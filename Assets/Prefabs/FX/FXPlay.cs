﻿using UnityEngine;

public class FXPlay : MonoBehaviour 
{
    ParticleSystem particleFX;

    void Start()
    {
        particleFX = GetComponent<ParticleSystem>();
    }


    public void PlayEffect()
    {
        particleFX.Play(true);
    } 

    public void StopEffect()
    {
        particleFX.Stop(true);
    }




    public void InvokedStopAfterPlay(float timeToStop)
    {
        PlayEffect();
        Invoke("StopEffect", timeToStop);
    }
}
