﻿using UnityEngine;

public class Trigger_DropLanes : MonoBehaviour 
{
	public GameObject laneController;

	public void DisplayLanes()
	{
		laneController.SetActive(true);
	}

	public void HideLanes()
	{
		laneController.SetActive(false);
	}
}
