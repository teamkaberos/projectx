﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Drop_Troops : MonoBehaviour , IDropHandler , IPointerEnterHandler,IPointerExitHandler
{
	Vector4 originalColor;
	Vector4 hoverColor = new Vector4 (1,0,0,0.4f);
	Image laneColor;

	public Button thisLaneButton;
	public GameObject laneTrigger;	//to reference the parent spawn button

	public Image hoverDisplay;

	void Start()
	{
		laneColor = GetComponent<Image>();
		originalColor = laneColor.color;
		laneTrigger = transform.parent.parent.gameObject;
	}
	public void OnPointerEnter(PointerEventData eventData)
	{
		if (DragAndDrop_Troop_Cards.troopBeingDragged != null)
		{
			hoverDisplay.color = hoverColor;
		}
	}
	public void OnPointerExit(PointerEventData eventData)
	{
		hoverDisplay.color = originalColor;
	}
	public void OnDrop(PointerEventData eventData)
	{
		if (DragAndDrop_Troop_Cards.troopBeingDragged != null)
		{
			laneTrigger.SetActive(false);
			DragAndDrop_Troop_Cards.troopBeingDragged.GetComponent<Button>().onClick.Invoke();
			thisLaneButton.onClick.Invoke();
			hoverDisplay.color = originalColor;
			DragAndDrop_Troop_Cards.troopBeingDragged = null;
		}
	}
}