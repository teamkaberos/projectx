﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class DragAndDrop_Troop_Cards : MonoBehaviour,IBeginDragHandler, IDragHandler, IEndDragHandler
{
	float timeRequired = 0.8f;
	float timer;
	bool isCounting = false;


	public static GameObject troopBeingDragged;
	public Transform startParent;
	Transform laneTrigger;
	Vector3 startPosition;

	void Start()
	{
		startParent = transform.parent;
		laneTrigger = transform.root;
	}
	public void OnBeginDrag (PointerEventData eventData)
	{
        // Check if the player is on tutorial
        if (Player.tutorial < Player.tutorialCap)
        {
            // Do some stuff here
        }

		troopBeingDragged = gameObject;
		laneTrigger.GetComponent<Trigger_DropLanes>().DisplayLanes();
		GetComponent<Image>().raycastTarget = false;
		startPosition = transform.position;
	}

	public void  OnDrag(PointerEventData eventData)
	{
		transform.position = Input.mousePosition;
	}

    public void AssignThis()
    {
        troopBeingDragged = gameObject;
    }

	public void  OnEndDrag(PointerEventData eventData)
	{
		troopBeingDragged = null;
		laneTrigger.GetComponent<Trigger_DropLanes>().HideLanes();
		GetComponent<Image>().raycastTarget = true;
		transform.position = startPosition;
	}
}
