﻿using UnityEngine;
using UnityEngine.UI;

public class AssignSlot : MonoBehaviour
{
    [HideInInspector] public int slotToAssignTo;
    GameObject glowObject;
    Button slotButton;

    public int assignedTroopNo;
    public int assignedTroopLevel;

    void Start()
    {
        glowObject = transform.GetChild(0).gameObject;
        slotButton = GetComponent<Button>();
        DragAndDrop.cardSelected += GlowInform;

        slotToAssignTo = int.Parse(name.Substring(name.Length - 1, 1));
        //this will happen if the players assigned units to the slots
    }

    public GameObject item
    {
        get
        {
            if (transform.childCount > 1)
            {
                return transform.GetChild(1).gameObject;
            }
            return null;
        }
    }

    #region toHide
    //	public void OnDrop (PointerEventData eventData)
    //	{
    //		//if the player did drag an object and placed over this slot
    //		if (DragAndDrop.beingSelected != null)
    //		{
    //			//This will only happen if this gameobject is dragged from the un-assigned area
    //			if(!DragAndDrop.beingSelected.GetComponent<DragAndDrop>().isAssigned)
    //			{	
    //				//Check first if player has enough points to assign the new unit
    //				if (Check_If_Assign_Can_Succeed() < DragAndDrop.beingSelected.GetComponent<DragAndDrop>().assignmentCost)
    //				{
    //					//do insufficient popup here
    //					UIManager.Instance.Not_Enough_Assign_Points();
    //					return;
    //				}
    //
    //				// and if this slot already has an item
    //				if (item)
    //				{
    //					Player.Instance.Calculate_Assignment_Points((item.GetComponent<DragAndDrop>().assignmentCost * -1));
    //					Destroy(item);
    //				}
    //				//Create a new card and assign it to the slot
    //				GameObject assignedCard = Instantiate(DragAndDrop.beingSelected,this.transform.position,Quaternion.identity,this.transform);
    //				assignedCard.GetComponent<Image>().raycastTarget = true;
    //				assignedCard.GetComponent<DragAndDrop>().isAssigned = true;
    //				assignedCard.GetComponent<DragAndDrop>().startParent = transform;
    //
    //				AssignTroop_To_Player_Troop_Slot(DragAndDrop.beingSelected.GetComponent<DragAndDrop>().assignedTroopID,
    //																			DragAndDrop.beingSelected.GetComponent<DragAndDrop>().assignmentCost);
    //			}
    //			else
    //			{
    //				// when the original point of the object being dragged is different from the one being hovered over now
    //				if (DragAndDrop.beingSelected.GetComponent<DragAndDrop>().startParent.transform != this.transform)
    //				{
    //					//check if there is already an item on that slot
    //					if (!item)
    //					{
    //						DragAndDrop.beingSelected.GetComponent<DragAndDrop>().startParent = this.transform;
    //						DragAndDrop.beingSelected.transform.position = this.transform.position;
    //					}
    //					//if there is an item
    //					else if (item)
    //					{
    //						// Area of troop cards swapping with each other
    //						//get the transform of Item
    //						Transform tempTransform = item.GetComponent<DragAndDrop>().startParent;
    //						//modifying the transform of Item
    //						item.GetComponent<DragAndDrop>().startParent = DragAndDrop.beingSelected.GetComponent<DragAndDrop>().startParent;
    //						item.transform.position = item.GetComponent<DragAndDrop>().startParent.position;
    //						item.transform.SetParent(item.GetComponent<DragAndDrop>().startParent);
    //
    //						DragAndDrop.beingSelected.GetComponent<DragAndDrop>().startParent = tempTransform;
    //						DragAndDrop.beingSelected.transform.position = DragAndDrop.beingSelected.GetComponent<DragAndDrop>().startParent.position;
    //						DragAndDrop.beingSelected.transform.SetParent(DragAndDrop.beingSelected.GetComponent<DragAndDrop>().startParent);
    //					}
    //				}
    //			}
    //		}
    //	}
    #endregion

    public void Create_Troop_Cards()
    {
        for (int x = 0; x < UnitManager.Instance.heroInfomation.Count; x++)
        {
            if (assignedTroopNo == UnitManager.Instance.heroInfomation[x].heroNo)
            {
                GameObject newCard = Instantiate(UnitManager.Instance.heroInfomation[x].dragAndDropCard, transform.position, Quaternion.identity, transform);

                newCard.GetComponent<DragAndDrop>().isAssigned = true;
                newCard.GetComponent<DragAndDrop>().thisToggle.interactable = true;

                newCard.GetComponent<DragAndDrop>().Assign();

                if (newCard.GetComponent<DragAndDrop>().assignedTroopID < 100)
                    newCard.GetComponent<DragAndDrop>().thisHeroCardsObtained.enabled = false;

                newCard.GetComponent<DragAndDrop>().startParent = transform;
                newCard.GetComponent<DragAndDrop>().draggingParent = transform.root.GetComponent<DragAndDrop_Transform_Organizer>().draggingParent;
            }
        }
    }

    public void GlowInform(bool isOn)
    {
        glowObject.SetActive(isOn);

        if (item)
        {
            if (item != DragAndDrop.beingSelected)
                item.GetComponent<DragAndDrop>().ToggleImageRaycast(isOn);
            //item.GetComponentInChildren<Toggle>().interactable = !isOn;
        }

        slotButton.interactable = isOn;

        if (isOn == false && transform.childCount <= 1)
            SetToEmpty();
    }

    public void SetToEmpty()
    {
        assignedTroopNo = 250;
        assignedTroopLevel = 0;
    }

    public void Assign_Selected_Troop()
    {
        //WHEN ASSIGNING TROOPS IN THE PREP HALLS
        if (DragAndDrop.beingSelected != null)
        {
            //This will only happen if this gameobject is dragged from the troop selection area
            if (!DragAndDrop.beingSelected.GetComponent<DragAndDrop>().isAssigned)
            {
                //Check with parent if there is already 2 of this card
                if (TroopAssign_Info.Instance.CheckHowManyOfThisUnitThereAre(DragAndDrop.beingSelected.GetComponent<DragAndDrop>().assignedTroopID))
                {
                    UIManager.Instance.HeroLimitReached();
                    DragAndDrop.beingSelected.GetComponent<DragAndDrop>().Selected(false);
                    return;
                }

                //if there is already an assigned unit in the selected slot
                if (item)
                {
                    // Check if player has enough points
                    if (Player.available_Assign_Points + item.GetComponent<DragAndDrop>().assignmentCost < DragAndDrop.beingSelected.GetComponent<DragAndDrop>().assignmentCost)
                    {
                        //do insufficient popup here
                        UIManager.Instance.Not_Enough_Assign_Points();
                        DragAndDrop.beingSelected.GetComponent<DragAndDrop>().Selected(false);
                        return;
                    }
                    //Check if replacable
                    if ((item.GetComponent<DragAndDrop>().assignmentCost + Player.available_Assign_Points) >= DragAndDrop.beingSelected.GetComponent<DragAndDrop>().assignmentCost)
                    {
                        Player.assigned_Assign_Points -= item.GetComponent<DragAndDrop>().assignmentCost;
                        Player.available_Assign_Points += item.GetComponent<DragAndDrop>().assignmentCost;
                        Destroy(item);
                    }
                }
                //if the selected slot is unassigned
                else
                {
                    // If player has insufficient points
                    if (Player.available_Assign_Points < DragAndDrop.beingSelected.GetComponent<DragAndDrop>().assignmentCost)
                    {
                        //do insufficient popup here
                        UIManager.Instance.Not_Enough_Assign_Points();
                        DragAndDrop.beingSelected.GetComponent<DragAndDrop>().Selected(false);
                        return;
                    }
                }

                //getting here means you can replace or assign the unit
                //therefore there is an instantiation of the card unit
                GameObject assignedCard = Instantiate(DragAndDrop.beingSelected, transform.position, Quaternion.identity, transform);
                assignedCard.transform.localScale = Vector3.one;
                assignedCard.GetComponent<Image>().raycastTarget = true;
                assignedCard.GetComponent<DragAndDrop>().isAssigned = true;
                assignedCard.GetComponent<DragAndDrop>().startParent = transform;
                if (assignedCard.GetComponent<DragAndDrop>().assignedTroopID < 100)
                {
                    assignedCard.GetComponent<DragAndDrop>().popupInfos.SetActive(false);
                    assignedCard.GetComponent<DragAndDrop>().thisHeroCardsObtained.enabled = false;
                }
                assignedCard.GetComponent<DragAndDrop>().Assign();
                assignedTroopNo = assignedCard.GetComponent<DragAndDrop>().assignedTroopID;
                if (assignedTroopNo < 100)
                {
                    for (int x = 0; x < UnitManager.Instance.heroInfomation.Count; x++)
                    {
                        if (UnitManager.Instance.heroInfomation[x].heroNo == assignedTroopNo)
                        {
                            assignedTroopLevel = UnitManager.Instance.heroInfomation[x].heroLvl;
                            break;
                        }
                    }
                }
                else
                    assignedTroopLevel = 0;
                AssignTroop_To_Player_Troop_Slot
                (assignedCard.GetComponent<DragAndDrop>().assignedTroopID, assignedCard.GetComponent<DragAndDrop>().assignmentCost);

                TroopAssign_Info.Instance.Assign_Points_Used();

                //Deselect the card
                //DragAndDrop.beingSelected.GetComponent<Toggle>().isOn = false;
                //assignedCard.GetComponent<DragAndDrop>().thisToggle.isOn = false;
            }
            //if players choose to rearrange troops
            else
            {
                //check if there is already an item on that slot
                //if there isn't anything
                if (!item)
                {
                    DragAndDrop.beingSelected.GetComponent<DragAndDrop>().startParent = transform;
                    DragAndDrop.beingSelected.GetComponent<DragAndDrop>().selectedParent = transform;
                    DragAndDrop.beingSelected.transform.position = transform.position;
                    DragAndDrop.beingSelected.transform.SetParent(transform);

                    assignedTroopNo = DragAndDrop.beingSelected.GetComponent<DragAndDrop>().assignedTroopID;
                    if (assignedTroopNo < 100)
                    {
                        for (int x = 0; x < UnitManager.Instance.heroInfomation.Count; x++)
                        {
                            if (UnitManager.Instance.heroInfomation[x].heroNo == assignedTroopNo)
                            {
                                assignedTroopLevel = UnitManager.Instance.heroInfomation[x].heroLvl;
                                break;
                            }
                            assignedTroopLevel = 0;
                        }
                    }
                }
                //if there is an item
                else
                {
                    // Area of troop cards swapping with each other
                    // get the transform of Item
                    Transform tempTransform = item.GetComponent<DragAndDrop>().startParent;
                    //modifying the transform of Item
                    item.GetComponent<DragAndDrop>().startParent = DragAndDrop.beingSelected.GetComponent<DragAndDrop>().startParent;
                    item.GetComponent<DragAndDrop>().selectedParent = DragAndDrop.beingSelected.GetComponent<DragAndDrop>().selectedParent;
                    item.transform.position = item.GetComponent<DragAndDrop>().startParent.position;
                    item.transform.SetParent(item.GetComponent<DragAndDrop>().startParent);

                    DragAndDrop.beingSelected.GetComponent<DragAndDrop>().startParent = tempTransform;
                    DragAndDrop.beingSelected.transform.position = DragAndDrop.beingSelected.GetComponent<DragAndDrop>().startParent.position;
                    DragAndDrop.beingSelected.GetComponent<DragAndDrop>().selectedParent = transform;
                    DragAndDrop.beingSelected.transform.SetParent(DragAndDrop.beingSelected.GetComponent<DragAndDrop>().startParent);
                }
                //Deselect the card
                DragAndDrop.beingSelected.GetComponent<Toggle>().isOn = false;
            }
        }
    }

    public int Get_Level_Of_Selected_Card()
    {
        for (int x = 0; x < UnitManager.Instance.heroInfomation.Count; x++)
        {
            if (UnitManager.Instance.heroInfomation[x].heroNo == DragAndDrop.beingSelected.GetComponent<DragAndDrop>().assignedTroopID)
            {
                return UnitManager.Instance.heroInfomation[x].heroLvl;
            }
        }
        return 0;
    }

    void AssignTroop_To_Player_Troop_Slot(int unitIDNo, int unitCost)
    {
        Player.assignedTroops[slotToAssignTo] = unitIDNo;

        Player.Instance.Calculate_Number_Of_Troops();

        if (Player.accountType == "Guest")
            Player.Save_Player_Assigned_Slots();
    }

    void SendAssignment()
    {
        //Send the assignment to server is the player is registered
        if (Player.accountType == "Registered")
        {
            assignedTroopLevel = Get_Level_Of_Selected_Card();
            //DataSender.Instance.SendTroop_And_AssignedSlot();
            //			(DragAndDrop.beingSelected.GetComponent<DragAndDrop>().assignedTroopID,
            //				assignedTroopLevel, string.Format ("Slot{0}",this.slotToAssignTo), string.Format ("Slot{0}Level",this.slotToAssignTo));
        }
    }

    void OnDestroy()
    {
        DragAndDrop.cardSelected -= GlowInform;
    }
}
