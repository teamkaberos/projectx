﻿using UnityEngine;
using UnityEngine.UI;

public class DragAndDrop : MonoBehaviour
{
    public Text thisHeroName;
	public Text thisHeroCardsObtained;
	public Toggle thisToggle;
	public Text thisCardCost;
	public Text thisCardLevel;

	public delegate void OnBeingSelected(bool isSelected);
	public static event OnBeingSelected cardSelected;

	public static GameObject beingSelected;
	public static int heroSlot_Selected_Is_AssignedTo;

	public bool isAssigned;		//this is to differentiate if the card is the assigner or assigned

	public int assignmentCost;
	public int assignedTroopID;
    public int thisCardsLevel;

	public Transform startParent;

	Vector3 startPosition;

	public Transform draggingParent;
	public GameObject popupInfos;
	public GameObject removeInfo;

	public Transform selectedParent;	//during assignment, to set parent as lowest sibling

	public bool forHeroLevelUp;


    // ==== For handling the Raycast upon selecting a card ==== \\
    [Header("Images Handling")]
    public Image[] allRaycastableImage;
    public Vector4[] colored;
    public Vector4[] tinted;

	#region Update dragging var
//	float timeRequired =1.28f;
//	float timer;
//	bool isCounting = false;
//
//	Image blocker;

//	public void StartCounter()
//	{
//		isCounting = true;
//		startPosition = transform.position;
//		if (isAssigned)
//		{
//			heroSlotAssignedTo = transform.parent.GetComponent<AssignSlot>().slotToAssignTo;
//		}
//	}
//	public void StopCounter()
//	{
//		isCounting = false;
//		blocker.enabled = true;
//		timer = 0;
//		if (!isAssigned)
//				Return_Where_You_Came_From();
//	}

//	void Update()
//	{
//		if (isCounting)
//		{
//			timer +=Time.deltaTime;
//			if(timer >= timeRequired)
//			{
//				beingDragged = gameObject;
//				transform.SetParent(draggingParent);
//				transform.position = Input.mousePosition;
//				GetComponent<Image>().raycastTarget = false;
//			}
//		}
//	} 

	#endregion

	void Start()
	{
        //thisCardLevel = transform.Find("Level").GetComponent<Text>();
        //thisCardCost = transform.Find("Cost").Find("CardCost").GetComponent<Text>();

        allRaycastableImage = GetComponentsInChildren<Image>();
        colored = new Vector4[allRaycastableImage.Length];
        tinted = new Vector4[allRaycastableImage.Length];
        for (int x = 0; x < allRaycastableImage.Length; x++)
        {
            colored[x] = allRaycastableImage[x].color;
            tinted[x] = new Vector4(colored[x].x, colored[x].y, colored[x].z, 0.3f);
        }

        for (int x = 0; x < UnitManager.Instance.heroInfomation.Count; x++)
        {
            if (assignedTroopID == UnitManager.Instance.heroInfomation[x].heroNo)
            {
                if (assignedTroopID < 100)
                {
                    if (UnitManager.Instance.heroInfomation[x].heroLvl >= 1)
                        assignmentCost = UnitManager.Instance.heroInfomation[x].heroAssignmentCost[UnitManager.Instance.heroInfomation[x].heroLvl - 1];
                    else
                        assignmentCost = UnitManager.Instance.heroInfomation[x].heroAssignmentCost[UnitManager.Instance.heroInfomation[x].heroLvl];
                }
                else
                    assignmentCost = 10;
            }
        }
        UpdateDisplayInfo();
	}


    public void Assign()
    {
        for (int x = 0; x < UnitManager.Instance.heroInfomation.Count; x++)
        {
            if (assignedTroopID == UnitManager.Instance.heroInfomation[x].heroNo)
            {
                if (assignedTroopID < 100)
                {
                    thisCardsLevel = UnitManager.heroLevels[x];
                    if (UnitManager.heroLevels[x] >= 1)
                        assignmentCost = UnitManager.Instance.heroInfomation[x].heroAssignmentCost[UnitManager.heroLevels[x] - 1];
                    else
                        assignmentCost = UnitManager.Instance.heroInfomation[x].heroAssignmentCost[UnitManager.heroLevels[x]];
                }
                else
                    assignmentCost = 10;
            }
        }

        if (isAssigned)
            selectedParent = transform.parent.transform;

        UpdateDisplayInfo();
    }


    public void UpdateDisplayInfo()
    {
        for (int x = 0; x < UnitManager.Instance.heroInfomation.Count; x++)
        {
            if (assignedTroopID == UnitManager.Instance.heroInfomation[x].heroNo)
            {
                if (assignedTroopID < 100)
                {
                    if (UnitManager.heroLevels[x] >= 1)
                    {
                        assignmentCost = UnitManager.Instance.heroInfomation[x].heroAssignmentCost[UnitManager.heroLevels[x] - 1];
                    }
                    else
                        assignmentCost = UnitManager.Instance.heroInfomation[x].heroAssignmentCost[UnitManager.heroLevels[x]];
                }
                else
                    assignmentCost = 10;
            }
        }

        thisCardCost.text = assignmentCost.ToString();


        if (assignedTroopID < 100)
        {
            thisCardLevel.text = string.Format("Level {0}", thisCardsLevel.ToString());
        }
    }

    public void ToggleCharacterInfo(bool isLeveled)
    {
        thisHeroName.gameObject.SetActive(isLeveled);
        thisCardLevel.gameObject.SetActive(isLeveled);
    }

	public void Selected(bool isOn)
	{
		//this is on the hero assignment page
		if (!forHeroLevelUp)
		{
			if (isOn)
			{
				transform.localScale = new Vector3 (1.2f, 1.2f,1.2f);
				beingSelected = gameObject;	
				if (!isAssigned)
                {
					transform.SetAsLastSibling();
				}
				else
				{
					selectedParent.SetAsLastSibling();
					removeInfo.SetActive(true);
                    Get_Slot_This_Card_Is_Assigned_To();
				}
			}
			else
			{
                transform.localScale = Vector3.one;
				beingSelected = null;
				if (isAssigned)
                    removeInfo.SetActive(false);
			}

            if (isAssigned != true && transform.root.name != "DragandDropCanvas")
            {
                popupInfos.SetActive(isOn);
            }
            cardSelected(isOn);
        }
        // And the ELSE part is for the Tavern
		else
		{
			if (isOn)
			{
				transform.localScale = new Vector3 (1.2f, 1.2f,1.2f);
                transform.SetAsLastSibling();
			}
			else
			{
				transform.localScale = Vector3.one;
                GetComponent<Toggle>().isOn = false;
			}
		}	
	}

	public void SendThisHeroNo(bool isOn)
	{
		UnitManager.Instance.SelectHeroToEvolve(assignedTroopID, thisToggle);
        Hero_Info_Assign.Instance.Recieve_Hero_Number(assignedTroopID, isOn, thisCardsLevel);
	}

	int Get_Slot_This_Card_Is_Assigned_To()
	{
		return startParent.GetComponent<AssignSlot>().slotToAssignTo;
	}

	bool Check_If_Unit_Can_Be_Unassigned()
	{
		int points_Available_After_Removing_ThisUnit = Player.available_Assign_Points + assignmentCost;
		
        if (points_Available_After_Removing_ThisUnit == Player.assign_Points_Limit)
			return false;
		return true;			
	}

    public void ToggleImageRaycast(bool toOn)
    {
        for (int x = 0; x < allRaycastableImage.Length; x++)
        {
            
            if (toOn)
                allRaycastableImage[x].color = tinted[x] ;
            else
                allRaycastableImage[x].color = colored[x] ;
            
            allRaycastableImage[x].raycastTarget = !toOn;
        }
    }

	public void Unassign_This_Unit()
	{
		if (!Check_If_Unit_Can_Be_Unassigned())
		{
			thisToggle.isOn = false;
			UIManager.Instance.Require_At_Least_One_Troop();
		}
		else
		{
            transform.parent.GetComponent<AssignSlot>().SetToEmpty();
            Unassign_Troop_From_Slot();
            Selected(false);
			DestroyThis();
		}
	}

	public void Unassign_Troop_From_Slot()
	{
		Player.assignedTroops[Get_Slot_This_Card_Is_Assigned_To()] = 250;

		Player.Instance.Calculate_Number_Of_Troops();

		TroopAssign_Info.Instance.Assign_Points_Used();

        Player.Instance.CheckAssignUnitResolved();

		if (Player.accountType == "Guest")
			Player.Save_Player_Assigned_Slots();
	}

	void DestroyThis()
	{
		Destroy(gameObject,0.01f);
	}
}
