﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Drag_Delay : MonoBehaviour 
{
	float timeRequired = 0.8f;
	float timer;
	bool isCounting = false;

	public Transform deck1;

	Image blocker;

	void Start()
	{
		blocker = GetComponent<Image>();
	}

	public void StartCounter()
	{
		isCounting = true;
	}
	public void StopCounter()
	{
		isCounting = false;
		blocker.enabled = true;
		timer = 0;
	}

	void Update()
	{
		if (isCounting)
		{
			timer +=Time.deltaTime;
			print (timer);
			if(timer >= timeRequired)
			{
				transform.position = Input.mousePosition;
			}
		}
	}
}
