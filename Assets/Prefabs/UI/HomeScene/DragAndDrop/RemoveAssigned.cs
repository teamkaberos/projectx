﻿using UnityEngine;
using UnityEngine.UI;

public class RemoveAssigned : MonoBehaviour 
{
    public Button assignmentClose;  //the button that allows players to close the assignment tab

    public GameObject pleaseRemoveTroopButton;  //the button to press when there is a troop assign error

    public GameObject removeUnits;
    public Text troopAssignError;

    bool assignmentPointError;  // if players assignment points exceeds, turn to true
    bool unitLimitError;    // if players units assigned exceeds limit count, turn to true


    void OnEnable()
    {
        if (Player.available_Assign_Points < 0 && TroopAssign_Info.Instance.Check_For_Troop_Limit() == true)
        {
            assignmentPointError = true;
            unitLimitError = true;

            assignmentClose.interactable = false;
            pleaseRemoveTroopButton.SetActive(true);

            removeUnits.SetActive(true);
            troopAssignError.text = "Available assignment points exceeded, Troop assignment count exceeded. Please re-assign your troops.";
        }
        else if (Player.available_Assign_Points < 0)
        {
            //set error to true
            assignmentPointError = true;

            assignmentClose.interactable = false;
            pleaseRemoveTroopButton.SetActive(true);

            removeUnits.SetActive(true);
            troopAssignError.text = "Troop assigned exceeds available assignment points. Please re-assign your troops.";
            return;
        }
        else if (TroopAssign_Info.Instance.Check_For_Troop_Limit() == true)
        {
            //set error to true
            unitLimitError = true;

            assignmentClose.interactable = false;
            pleaseRemoveTroopButton.SetActive(true);

            removeUnits.SetActive(true);
            troopAssignError.text = "Troop limit exceeds limit count. Please re-assign your troops.";
            return;
        }
        else
        {
            assignmentClose.interactable = true;
            pleaseRemoveTroopButton.SetActive(false);

            removeUnits.SetActive(false);
        }
    }

        

    void Start()
    {
        Player.assingmentPointsResolved += AssignmentPointResolved;
        Player.unitLimitResolved += TroopLimitResolve;
    }


    void AssignmentPointResolved()
    {
        assignmentPointError = false;
        CheckEnableClose();
    }
    void TroopLimitResolve()
    {
        unitLimitError = false;
        CheckEnableClose();
    }



    void CheckEnableClose()
    {
        if (assignmentPointError == false && unitLimitError == false)
        {
            enableClose();
        }
    }




    void enableClose()
    {
        assignmentClose.interactable = true;
    }






    void OnDestroy()
    {
        Player.assingmentPointsResolved -= CheckEnableClose;
        Player.unitLimitResolved -= CheckEnableClose;
    }
}
