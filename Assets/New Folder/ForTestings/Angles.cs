﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Angles : MonoBehaviour
{
    public Transform obj1;
    public Transform obj2;

    void Update()
    {
        Debug.Log(Vector2.Angle(obj1.position, obj2.position));
    }
}
