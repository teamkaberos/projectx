﻿using UnityEngine;
using UnityEngine.EventSystems;

public class FloorClose : MonoBehaviour, IPointerClickHandler
{
	public virtual void OnPointerClick(PointerEventData eventData)
	{
		bool toMove = CameraMovement.popup_Blocked;

		if (toMove == false)
		{
            if (Player.tutorial < Player.tutorialCap)
                return;
            UIManager.Instance.Slide_Up_Bar(false);
            BuildingManager.Instance.HideBuildingHighlight();
            if (BuildingManager.lastSelectedBuillding != null)
            {
                BuildingManager.lastSelectedBuillding.GetComponent<HomeBuildings>().clicked = false;
            }
		}
	}
}
