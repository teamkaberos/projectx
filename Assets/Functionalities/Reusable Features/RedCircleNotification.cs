﻿using UnityEngine;

public class RedCircleNotification : MonoBehaviour
{
    public GameObject redNotification;

    void OnEnable()
    {
        redNotification.SetActive(true);
    }

    void OnDisable()
    {
        redNotification.SetActive(false);
    }
}
