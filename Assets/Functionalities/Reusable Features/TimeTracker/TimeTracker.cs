﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TimeTracker : MonoBehaviour
{
    public static bool track;
    public static double timePlayed;     // keep track of how long the player is in the game

    void Start()
    {
        
        if (SceneManager.GetActiveScene().name.ToLower().Contains("login"))
        {
            track = false;
            timePlayed = 0;
        }
        if (SceneManager.GetActiveScene().name.ToLower().Contains("home"))
        {
            //do home scene stuff
            track = true;
        }
    }

    void Update()
    {
        if(track)
            timePlayed += Time.deltaTime;
    }
}
