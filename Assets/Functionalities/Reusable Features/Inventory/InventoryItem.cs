﻿using UnityEngine;


[CreateAssetMenu(fileName = "inventoryItem", menuName = "newInventoryItem")]
public class InventoryItem : ScriptableObject 
{
    public string itemName;
    public int itemValue;
    public string searchField;
    [TextArea(30,3)]
    public string itemDescription;
    public Sprite itemImage;
}
