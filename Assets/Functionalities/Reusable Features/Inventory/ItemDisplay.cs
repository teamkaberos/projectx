﻿using UnityEngine;
using UnityEngine.UI;

public class ItemDisplay : MonoBehaviour 
{
    public GameObject selectedItemDisplay;

    public Image selectedItemImage;
    public Text selectedItemName;
    public Text selectedItemDescriptionText;


    public Button[] inventoryItem;


    public void DisplayInfo(InventoryUpdater inv_item)
    {
        InventoryItem inv_itemSelected = inv_item.inventoryItem;

        if (!selectedItemDisplay.activeInHierarchy)
            selectedItemDisplay.SetActive(true);

        selectedItemImage.sprite = inv_itemSelected.itemImage;

        selectedItemName.text = inv_itemSelected.itemName.ToUpper();
        selectedItemDescriptionText.text = inv_itemSelected.itemDescription;
    }
}
