﻿using UnityEngine;
using UnityEngine.UI;
using System;

public class InventoryUpdater : MonoBehaviour
{
    Player player;
    Type playerType;

    public InventoryItem inventoryItem;
    //public Text itemName;

    string searchField;
    string itemDescrip;

    public Text itemValue;
    int amount;
    float itemAmount;


    public Image itemImage;

    System.Object playerObj;




    void Awake()
    {
        player = new Player();
        playerObj = player;
        playerType = player.GetType();

        itemImage.sprite = inventoryItem.itemImage;
        itemImage.preserveAspect = true;
        searchField = inventoryItem.searchField;
        itemDescrip = inventoryItem.itemDescription;
    }

    void OnEnable()
    {
        UpdateValueFromPlayer();
        DisplayValue();
    }
    void UpdateValueFromPlayer()
    {
        amount = (int)playerType.GetField(searchField).GetValue(playerObj);
        itemAmount = amount;
    }
    void DisplayValue()
    {
        itemValue.text = ParseValue(itemAmount);
    }
    string ParseValue(float amount)
    {
        if (amount > 1000)
        {
           
            if (amount > 1000000)
            {
                
                float Mamount = amount / 1000000;
                return string.Format("<color=#AA00FF>{0:0.##}M</color>", Mamount);
            }


            float Kamount = amount / 1000;
            return string.Format("<color=#FFFF00>{0:0.##}K</color>", Kamount);
        }

        return string.Format("<color=#FFFFFF>{0}</color>",amount);
    }
} 
