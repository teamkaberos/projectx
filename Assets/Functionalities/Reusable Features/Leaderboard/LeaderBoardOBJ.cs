﻿using UnityEngine;
using UnityEngine.UI;

public class LeaderBoardOBJ : MonoBehaviour
{
    public int rankNo;
    public string clanName;
    public int totalContributioinPoints;

    public string leaderID;

    public Image flagImage;
    public Text rankText;
    public Text clanNameText;
    public Text clanContributionPointsText;

    [Header("OBJ ranking display")]
    public Image nameImage;
    public Image pointsImage;


    Vector4 gold = new Vector4(1, 0.77f, 0, 1);
    Vector4 silver = new Vector4(1, 1, 1, 1);
    Vector4 bronze = new Vector4(0.75f, 0.5f, 0.28f, 1);


    public void SetName(string clanName_)
    {
        clanName = clanName_;
        clanNameText.text = clanName;
    }
    public void SetRank(int rank_)
    {
        rankNo = rank_;
        rankText.text = rank_.ToString();

        SetOBJColor();
    }
    public void SetPoints(int points_)
    {
        totalContributioinPoints = points_;
        clanContributionPointsText.text = string.Format("{0:n0}", points_);
    }
    public void SetFlag(Sprite flagImage_)
    {
        flagImage.sprite = flagImage_;
    }

    void SetOBJColor()
    {
        if (rankNo == 1)
        {
            nameImage.color = gold;
            pointsImage.color = gold;
        }
        else if (rankNo == 2)
        {
            nameImage.color = silver;
            pointsImage.color = silver;
        }
        else if (rankNo == 3)
        { 
            nameImage.color = bronze;
            pointsImage.color = bronze;
        }
        else
        {
            nameImage.enabled = false; 
        }
    }



    public void DestroyThisOBJ()
    {
        Destroy(gameObject, 0.2f);
    }
}
