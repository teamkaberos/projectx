﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LeaderBoard : MonoBehaviour
{
    public Button leaderboardButton;
    public GameObject leaderboardPanel;

    [Header("Clan Reference")]
    public Clan clanInfo;

    public int nValue;
    //public List <LeaderBoardOBJ> topNClans = new List<LeaderBoardOBJ>();  // list of top n clans in the leader board
    [HideInInspector] public List <GameObject> topNClanObj = new List<GameObject>();

    [Header("Instantiate")]
    public Transform leaderboardScroll;
    public GameObject leaderboardObjs;

    public struct LeaderboardInfo
    {
        public int rankInt_;
        public string clanID;
        public int contributionPoints;
        public string leaderID;
        public int clanMemberCount;
        public int flagInt;
    }

    [Space(12)]
    [Header("UI Stuff")]
    public GameObject responsePanel;
    public Text responseText;
    public Button responseButton;

    [Space(6)]
    public Text myClanName;
    public Text myClanPoints;
    public Text myClanRank;


    void Start()
    {
        leaderboardButton.onClick.AddListener(OpenLeaderboards);
    }

    void OpenLeaderboards()
    {
        myClanName.text = Clan.clanID;
        myClanPoints.text = string.Format("{0:n0}", EventHolderManager.playerClanProgression);
        myClanRank.text = "retrieving info . . .";


        RetrievingLeaderboard();
    }


    public void InteractableLeaderboard()
    {
        if (EventHolderManager.currentEventActivityStatus != EventHolderManager.EventLiveStatus.canceled)
            leaderboardButton.interactable = XcaliburEvent.eventIsOn;
        else
            leaderboardButton.interactable = false;
    }

    public void CheckIfClanWithinTopN()
    {
        if (!WithinTopN())
        NotInTopN();
    }
    bool WithinTopN()
    {
        for (int x = 0; x < topNClanObj.Count; x++)
        {
            if (Clan.clanID == topNClanObj[x].GetComponent<LeaderBoardOBJ>().clanName)
            {
                myClanRank.text = "Rank : " + topNClanObj[x].GetComponent<LeaderBoardOBJ>().rankNo + "";
                return true;
            }
        }
        return false;
    }
    void NotInTopN()
    {
        myClanRank.text = "Rank : Unranked";
    }



    public void GetTopNClans()
    {
        GameSparksManager.Instance.GetTopClans(nValue, this);
    }
    public void RemoveUnwantedOBJs()
    { 
        for(int x = 0; x< topNClanObj.Count; x++)
        {
            topNClanObj[x].GetComponent<LeaderBoardOBJ>().DestroyThisOBJ();
        }
        topNClanObj.Clear();
    }
    public void InsertInfo(LeaderboardInfo lbInfo)
    {
        GameObject lbOBJ = Instantiate(leaderboardObjs, leaderboardScroll.position, Quaternion.identity, leaderboardScroll);

        topNClanObj.Add(lbOBJ);

        LeaderBoardOBJ lbobjItem = lbOBJ.GetComponent<LeaderBoardOBJ>();

        lbobjItem.SetPoints(lbInfo.contributionPoints);
        lbobjItem.SetName(lbInfo.clanID);
        lbobjItem.SetFlag(clanInfo.ThisFlag(lbInfo.flagInt));
        lbobjItem.SetRank(lbInfo.rankInt_);
    }
    public void ArrangeAccordingToRank()
    {
        for (int rankLoopCheck = nValue; rankLoopCheck > 0; rankLoopCheck--)
        {
            for (int objLoopCheck = 0; objLoopCheck < topNClanObj.Count; objLoopCheck ++)
            {
                if (topNClanObj[objLoopCheck].GetComponent<LeaderBoardOBJ>().rankNo == nValue)
                {
                    topNClanObj[objLoopCheck].transform.SetAsFirstSibling();
                }
            }
        }
        CheckIfClanWithinTopN();
        RetrieveCompleted();
    }


    public void RetrievingLeaderboard()
    {
        RemoveUnwantedOBJs();

        GetTopNClans();
        InfoPopup(false, "Requesting from server", true);
        OpenLeaderBoardPanel();
    }

    public void RetrieveCompleted()
    {
        InfoPopup(true, "Retrieve Complete", false);
    }


    void OpenLeaderBoardPanel()
    {
        leaderboardPanel.SetActive(true);
    }

    public void InfoPopup(bool interactable, string textToDisplay, bool toBeSeen)
    {
        responseButton.interactable = interactable;
        responseText.text = textToDisplay;
        responsePanel.SetActive(toBeSeen);
    }

}
