﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class Siege : LoadedEnemy
{
    [HideInInspector]
    public DateTime timeOfAttack;
    [HideInInspector]
    public long lostGold;

	public Text timeOfAttackText;
    public Text goldLostText;


    void Start()
    {
        embarkToWar.onClick.AddListener(TakeRevenge);
    }

    void OnEnable()
    {
        UpdateOpponentInfo();
    }

    public override void UpdateOpponentInfo()
    {
        base.UpdateOpponentInfo();

        DateTime nowTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddMilliseconds(DataManager.initialLoginServerTime * 1000);
        TimeSpan diff = nowTime.Subtract(timeOfAttack);
        string lengthOfAttackedDuration = "";

        if (Mathf.FloorToInt(diff.Days) >= 1)
        {
            lengthOfAttackedDuration = Mathf.FloorToInt(diff.Days) + " days";
        }
        else if (Mathf.FloorToInt(diff.Hours) >= 1)
        {
            lengthOfAttackedDuration = Mathf.FloorToInt(diff.Hours) + " hrs";
        }
        else if (Mathf.FloorToInt(diff.Minutes) >= 1)
        {
            lengthOfAttackedDuration = Mathf.FloorToInt(diff.Minutes) + " mins";
        }
        else if (Mathf.FloorToInt(diff.Minutes) < 1)
        {
            lengthOfAttackedDuration = "Less than a min";
        }
        timeOfAttackText.text = string.Format("{0} ago", lengthOfAttackedDuration);




        if (lostGold > 0)
            goldLostText.text = string.Format("We were under seige by {0} and lost a total of {1:n0} GOLD.", playerName, lostGold);
        else if (lostGold <= 0)
            goldLostText.text = string.Format("We were under seige by {0} and successfully defended against the attack.", playerName, lostGold);
    }

    void TakeRevenge()
    {
        base.StoreEnemyInfo();
        LevelManager.Instance.BattleScene();
        LevelManager.Instance.StoreTotalGameTime();
    }

    public void DestroySeigeItem()
    {
        Destroy(gameObject, 0.1f); 
    }
}
