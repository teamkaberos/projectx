﻿using UnityEngine;
using UnityEngine.UI;

public class AccountDetails : MonoBehaviour 
{
    [Header("Inputs")]
    public InputField displayNameInput;
    public InputField oldPasswordInput;
    public InputField newPasswordInput;
    public InputField reenterNewPasswordInput;
    public Text emailDisplay;


    [Space(15)]
    [Header("Response Obj")]
    public GameObject tapBlockPanel;        //display this while waiting for the change details response to prevent further input
    public GameObject messagePopup;       //display this after recieving a response
    public Text messageText;                           //carries the response message
    public GameObject closeSuccessfulAccountUpdate;     //for closing the update acct panel if the action is successful

    [Space(15)]
    [Header("Buttons")]
    public Button changeAcctDetailsButton;          //to trigger the change acct details action
    public Button cancelButton;                            //to cancel the change acct details button
    public Button closeMessagePopup;                 //to close the message popup
    public Button successfulUpdateResponseButton;    //used to clear fields when the update is successful



    void Start()
    {
        ClearAllFields();                   //clear all input fields
        TapBlockToggle(false);        //turn off tap blocker
        DisplayPopup(false);            //turn off the popup

        changeAcctDetailsButton.onClick.AddListener(AttemptChangeAccountDetails);
        cancelButton.onClick.AddListener(ClearAllFields);
        successfulUpdateResponseButton.onClick.AddListener(ClearAllFields);
        closeMessagePopup.onClick.AddListener(delegate{ DisplayPopup(false); });
    }

    //Clear all input fields everytime the user closes the change account tab
    void ClearAllFields()
    {
        displayNameInput.text = "";
        oldPasswordInput.text = "";
        newPasswordInput.text = "";
        reenterNewPasswordInput.text = "";
        emailDisplay.text = Player.email;
    }

    // ==== PERFORM CHECKS HERE ==== \\

    // Check the displayname input
    bool usernameInputCheck()
    {
        if (displayNameInput.text == "")
            return true;
        if (displayNameInput.text.Length < 2)
            return false;
        return true;
    }
    bool oldPasswordCheck()
    {
        if (oldPasswordInput.text == "")
            return false;
        return true;
    }
    // Check if both passwords are holding the same string
    string newPasswordCheck()
    {
        if (newPasswordInput.text != reenterNewPasswordInput.text)
            return "New Passwords do not match";
        if (newPasswordInput.text.Length < 5)
            return "Please key in a password with a minimum of 5 characters";
        return "true";
    }

    void AttemptChangeAccountDetails()
    {
        if (!usernameInputCheck())
        {
            MessagePopup("Please enter a Display Name between 2-24 characters",false);
            return;
        }
        if (!oldPasswordCheck())
        {
            MessagePopup("Old password cannot be empty", false);
            return;
        }
        if (newPasswordCheck() != "true")
        {
            MessagePopup(newPasswordCheck(),false);
            return;
        }

        GameSparksManager.Instance.ChangePlayerDetails(displayNameInput.text,
                                                       oldPasswordInput.text, newPasswordInput.text, this);
        TapBlockToggle(true);
    }

    // Toggling the tap blocking panel to prevent further button taps until there is a response
    void TapBlockToggle(bool isOn)
    {
        tapBlockPanel.SetActive(isOn);
    }


    // ======== RESPONSE HANDLERS ======== \\
    public void MessagePopup(string responseMessage, bool outcome)
    {
        TapBlockToggle(false);
        messageText.text = responseMessage;
        DisplayPopup(true);

        closeSuccessfulAccountUpdate.SetActive(outcome);
    }
    void DisplayPopup(bool isOn)
    {
        messagePopup.SetActive(isOn);
    }
}
