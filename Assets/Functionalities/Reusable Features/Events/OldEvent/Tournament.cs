﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

public class Tournament : MonoBehaviour
{
    #region Singleton
    static Tournament _instance;
    public static Tournament Instance
    {
        get
        {
            return _instance;
        }
    }
    #endregion

    //  ==== FOR GS LOGING TEST ==== \\
    string usname = "RylaiCrestmaiden";
    string pasword = "1234";
    public Button loginButton;


    // ==== TOURNAMENTS INFO AREA ==== \\
    [System.Serializable]
    public class TournamentTemp
    {
        public string tournamentName;
        public int queueNo;
        public long startTime;
        public long endTime;
        public string status;
        public int season;
        public long timeLeftToLive;
        public long timeLeftToEnd;
    }
    public static List<TournamentTemp> ongoingTournaments = new List<TournamentTemp>();

    // ==== DISPLAY AREA ==== \\ 
    [HideInInspector] public List<EventButton> tournaments;         // The Reference to the Event buttons in the Event Panel
    // horizontal display
    [Header("Horizontal Display")]
    public GameObject eventOBJ;                         // OBJ that appears on the Event tab (this is the one that gets instantiated)
    public Transform eventPanel;                         // The Event panel that parents the eventOBJ
    [Header("Vertical Display")]
    public GameObject eventOBJ_V;                         // OBJ that appears on the Event tab (this is the one that gets instantiated)
    public Transform eventPanel_V;                         // The Event panel that parents the eventOBJ

    [Header("Event Info Popup")]
    public GameObject eventInfoPopup;               // The popup holding info of the selected event/tournament

    public GameObject[] tables;

    // ==== No ongoing Events display ==== \\
    public GameObject noEventsText;

    void Awake()
    {
        _instance = this;
    }

    void Start()
    {
        //loginButton.onClick.AddListener(delegate { GameSparksManager.Instance.ForTestGSLogin(usname, pasword); });

        ClearTournamentsInfo();
    }

    public void TogglePanel(bool isActive)
    {
        tables[0].SetActive(isActive);
        tables[1].SetActive(!isActive);
    }

    // ==== CREATE TOURNAMENTS ==== \\
    public void CreateTournamentTemp(string _tournamentName, int _queueNo, long _startTime, long _endTime,  string _status, int _season)
    {
        TournamentTemp newTemp = new TournamentTemp()
        {
            tournamentName = _tournamentName,
            queueNo = _queueNo,
            startTime = _startTime,
            endTime = _endTime,
            status = _status,
            season = _season,
        };
        if (newTemp.status == "inactive")
        {
            // if the event has not start
            newTemp.timeLeftToLive = _startTime - (DataManager.initialLoginServerTime);  //calculate the time till start
        }
        else if (newTemp.status == "live")
        {
            // if the event has already started
            newTemp.timeLeftToEnd = _endTime - (DataManager.initialLoginServerTime);  //calculate the time till end
        }
        ongoingTournaments.Add(newTemp);
    }

    public void TournamentsInit()
    {
        // Here is where the commands for the Tournament rearrangements, organization and rearrangements are issued
        RearrangeTournaments();
        CreateTournamentDisplay();
        CheckIfThereAreEvents();
        //CreateVerticalTournamentDisplay();
    }
    DateTime dateTimeStamp(long epochT)
    {
        return new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddSeconds(epochT);
    }
    void RearrangeTournaments()
    {
        List<TournamentTemp> ongoingTournamentsT = new List<TournamentTemp>();
        for (int x = 0; x < ongoingTournaments.Count; x++)
        {
            for (int y = ongoingTournaments.Count; y > 0; y--)
            {
                if (ongoingTournaments[x].queueNo == y)
                {
                    ongoingTournamentsT.Add(ongoingTournaments[x]);
                    break;
                }
            }
        }
        //ongoingTournaments = ongoingTournamentsT;
    }
    void CreateTournamentDisplay()
    {
        for (int x = 0; x < ongoingTournaments.Count; x++)
        {
            GameObject tourneyOBJ = Instantiate(eventOBJ, eventPanel.position, Quaternion.identity, eventPanel);
            EventButton eventInfo = tourneyOBJ.GetComponent<EventButton>();
            eventInfo.tournamentName = ongoingTournaments[x].tournamentName;
            eventInfo.startTime = dateTimeStamp(ongoingTournaments[x].startTime);
            eventInfo.endTime = dateTimeStamp(ongoingTournaments[x].endTime);
            eventInfo.status = ongoingTournaments[x].status;
            eventInfo.season = ongoingTournaments[x].season;
            eventInfo.timeToLive = ongoingTournaments[x].timeLeftToLive;
            eventInfo.timeToEnd = ongoingTournaments[x].timeLeftToEnd;
            eventInfo.DisplayInfo();
        }
    }
    void CheckIfThereAreEvents()
    {
        if (ongoingTournaments.Count > 0)
            noEventsText.SetActive(true);
        else
            noEventsText.SetActive(false);
    }
    void CreateVerticalTournamentDisplay()
    {
        for (int x = 0; x < ongoingTournaments.Count; x++)
        {
            GameObject tourneyOBJ = Instantiate(eventOBJ_V, eventPanel_V.position, Quaternion.identity, eventPanel_V);
            EventButton eventInfo = tourneyOBJ.GetComponent<EventButton>();
            eventInfo.tournamentName = ongoingTournaments[x].tournamentName;
            eventInfo.startTime = dateTimeStamp(ongoingTournaments[x].startTime);
            eventInfo.endTime = dateTimeStamp(ongoingTournaments[x].endTime);
            eventInfo.status = ongoingTournaments[x].status;
            eventInfo.season = ongoingTournaments[x].season;
            eventInfo.timeToLive = ongoingTournaments[x].timeLeftToLive;
            eventInfo.timeToEnd = ongoingTournaments[x].timeLeftToEnd;
            eventInfo.DisplayInfo();
        }
    }
    // ==== CREATE TOURNAMENTS END ==== \\


    // ==== UPDATE EVENTS ==== \\
    public void UpdateTournamentEvent(string eventName, int valueToIncrement)
    {
        // CHECK IF THE EVENT IS ACTIVE
        if (ActiveEvent(eventName))
        {
            // CHECK IF THERE ARE OTHER REQUIREMENTS ( EG NEED TO BE IN CLAN; LIMITED TIME ENTRIES )
            
        }
    }
    bool ActiveEvent(string eventName)
    {
        for (int x = 0; x < tournaments.Count; x++)
        {
            if (tournaments[x].tournamentName == eventName & tournaments[x].status == "live")
                return true;
        }
        return false;
    }

    // ==== UPDATE EVENTS END ==== \\


    // ==== CLEAR TOURNAMENTS ==== \\
    public void ClearTournamentsInfo()
    {
        // Destroy the gameObject first
        DestroyTournamentEventOBJs();
        // Then clear the nulled Lists
        ClearOnGointTournaments();
        ClearTournamentEventList();

    }
    void DestroyTournamentEventOBJs()
    {
        if (eventPanel.childCount > 0)
        {
            for (int x = 0; x < eventPanel.childCount; x++)
            {
                tournaments[x].DestroyThis();
            }
        }
    }
    void ClearOnGointTournaments()
    {
        ongoingTournaments.Clear();
    }
    void ClearTournamentEventList()
    {
        tournaments.Clear();
    }
}
