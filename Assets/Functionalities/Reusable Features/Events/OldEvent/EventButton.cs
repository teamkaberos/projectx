﻿using UnityEngine.UI;
using UnityEngine;
using System;

public class EventButton : MonoBehaviour 
{
    // ====== Info holder ====== \\
    public string tournamentName;
    public DateTime startTime;
    public DateTime endTime;
    public string status;
    public int season;
    public float timeToLive;
    public double timeToEnd;
    public int entries;


    // ====== UI Stuff ====== \\
    public Text nameDisplay;
    public Text startTimeDisplay;
    public Text toLiveTimeDisplay;
    public Text endTimeDisplay;
    public Text toEndTimeDisplay;
    public Text statusDisplay;
    public Text seasonDisplay;



    public void DisplayInfo()
    {
        nameDisplay.text = tournamentName;
        startTimeDisplay.text = "Start Date : \n" + StartTime() + UTC();
        endTimeDisplay.text = "End Date : \n" +  EndTime() + UTC();
        statusDisplay.text = string.Format ("<color=#FF0000>{0} </color> ",EventStatus().ToUpper());
        seasonDisplay.text = Season();
    }
    string StartTime()
    {
        return string.Format("{0}",startTime.ToString("d-M-yyyy HH:mm:ss"));
    }
    string EndTime()
    {
        return string.Format("{0}", endTime.ToString("d-M-yyyy HH:mm:ss"));
    }
    string EventStatus()
    {
        switch(status)
        {
            case "live":
                return "live now";
            case "inactive":
                return "stay tuned";
        }
        return "";
    }
    string Season()
    {
        return string.Format("Season : " + season);
    }
    string UTC()
    {
        return "<color=#00AA00> (UTC)</color>";
    }

    void Update()
    {
        if (status == "inactive")
        {
            timeToLive -= Time.deltaTime;
            toLiveTimeDisplay.text = "Countdown : " + FormatTime(timeToLive);
            toEndTimeDisplay.text = "Beginning soon";
            if(timeToLive <= 0)
            {
                // WHEN THE EVENT BECOMES LIVE
                timeToLive = 0;
                status = "live";
            }
        }
        else if (status == "live")
        {
            timeToEnd -= Time.deltaTime;
            toLiveTimeDisplay.text = "Ongoing";
            toEndTimeDisplay.text = "Ending in " + FormatTime((float)timeToEnd);
            if (timeToLive <= 0)
            {
                // WHEN THE EVENT BECOMES LIVE
                //timeToEnd = 0;
                //status = "over";
            }
        }
        else if (status == "over")
        {
            timeToLive = 0;
            timeToEnd = 0;
            toLiveTimeDisplay.text = "Event Over";
            toEndTimeDisplay.text = "Stay tuned";
        }
    }
    string FormatTime(float totalSeconds)
    {
        int hours = Mathf.FloorToInt (totalSeconds / 3600f);
        int min = Mathf.FloorToInt ((totalSeconds - (hours * 3600f)) / 60f);
        int seconds = Mathf.FloorToInt (totalSeconds - (hours * 3600f + min*60f));
        return string.Format("{0:00}:{1:00}:{2:00}", hours, min, seconds);
    }


    public void DestroyThis()
    {
        Destroy(gameObject);
    }
}
