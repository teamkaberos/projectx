﻿using UnityEngine;
using UnityEngine.UI;

public class EventInfo : MonoBehaviour
{
    public Text eventDescripName;
    public Text eventDescription;


    public void UpdateEventPopup(string eventName, string eventDescrip)
    {
        UpdateName(eventName);
        UpdateDescrip(eventDescrip);
    }
    void UpdateName(string eventName)
    {
        eventDescripName.text = eventName;
    }
    void UpdateDescrip(string eventDescrip)
    {
        eventDescription.text = eventDescrip;
    }
}
