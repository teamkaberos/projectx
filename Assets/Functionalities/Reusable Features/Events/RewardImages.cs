﻿using UnityEngine;

[CreateAssetMenu(fileName = "RewardImages", menuName = "RewardImages", order = 1)]
public class RewardImages : ScriptableObject
{
    [System.Serializable]
    public class RewardImageList
    {
        public EventType.RewardType rewardType;
        public Sprite rewardTypeImage;
        public string rewardTypeName;
    }

    public RewardImageList[] rewardImages;

    public Sprite ImageToReturn(EventType.RewardType typeOfReward)
    { 
        for (int x = 0; x < rewardImages.Length; x++)
        {
            if (typeOfReward == rewardImages[x].rewardType)
                return rewardImages[x].rewardTypeImage;
        }
        return null;
    }
    public string NameToReturn(EventType.RewardType typeOfReward)
    {
        for (int x = 0; x < rewardImages.Length; x++)
        {
            if (typeOfReward == rewardImages[x].rewardType)
                return rewardImages[x].rewardTypeName;
        }
        return "";
    }
}
