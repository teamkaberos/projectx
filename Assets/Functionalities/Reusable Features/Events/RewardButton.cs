﻿using UnityEngine;
using UnityEngine.UI;

public class RewardButton : MonoBehaviour
{
    [System.Serializable]
    public class RewardDisplay
    {
        public GameObject displayObj;
        public Image rewardImageType;
        public Text rewardAmount;
        public Text rewardType;
    }
    public EventType.RewardTier thisButtonRewards;
    public RewardDisplay[] thisTierRewards;

    public int rewardAvailability;
    public bool redeemed;

    int typeOfReward;   // { 0 = WeeklyRewards , 1 = ExcaliburEvent }

    // ======== UI Info Display ========= \\
    public Image rewardImage;
    public Text rewardTypeText;
    public Text rewardValueText;
    public Text rewardRequirementText;      // This will reflect "Redeemed" when the player collects this reward

    public Button collectThisRewardButton;

    // ===== Reward Redeemed ===== \\
    public GameObject redeemedBlocker;



    public void SetMeInteractable(bool isClaimable)
    {
        collectThisRewardButton.interactable = isClaimable;
    }

    public void UpdateButtonEventRewardInfo()
    {
        for (int x = 0; x < thisButtonRewards.rewardsOnTier.Length; x++)
        {
            thisTierRewards[x].rewardAmount.text = string.Format("{0:n0}x", thisButtonRewards.rewardsOnTier[x].amountOfEventReward);
            thisTierRewards[x].rewardType.text = thisButtonRewards.rewardsOnTier[x].typeOfEventRewardName;
            thisTierRewards[x].rewardImageType.sprite = thisButtonRewards.rewardsOnTier[x].eventRewardImage;
            thisTierRewards[x].displayObj.SetActive(true);
        }
    }
    public void DisplayAvailablility(int typeOfReward_, int rewardAvailability_)
    {
        typeOfReward = typeOfReward_;
        rewardAvailability = rewardAvailability_;

        DisplayInfo(typeOfReward);
    }

    public void DisplayInfo(int typeOfReward)
    {
        if (typeOfReward == 0)
            DisplayRewardRequirements();
        else if (typeOfReward == 1)
            DisplayRewardAvailability();            // if type == 1, means its excaliprize

        ListenerAssign();
    }
    void DisplayRewardRequirements()
    {
        // for event rewards, this shows the amount of points needed
        rewardRequirementText.text = string.Format("{0:n0}",rewardAvailability);
    }
    public void DisplayRewardAvailability()
    {
        // for Xcalibur event, this shows if the reward is available to be collected
        rewardRequirementText.text = AvailabilityConversion(rewardAvailability);
    }
    public void AssignToButton()
    {
        ListenerAssign(); 
    }
    protected virtual void ListenerAssign()
    {
        collectThisRewardButton.onClick.AddListener(AttemptRedeemReward);
    }

    protected virtual void AttemptRedeemReward()
    {
        // Hit this Button to attempt retrieving the reward from GS
        EventHolderManager.Instance.AttemptRewardRedemption(this);
    }
    public void DisplayClaimedReward()
    {
        RewardRedemption();
        DisplayMessage();
    }
    public void RewardRedemption()
    {
        for (int x = 0; x < thisButtonRewards.rewardsOnTier.Length; x++)
        {
            AddRewards(thisButtonRewards.rewardsOnTier[x].typeOfEventReward, x);
        }
    } 
    void DisplayMessage()
    {
        EventHolderManager.Instance.DisplayInfoPopup(FormatReceivedItems());
    }
    public string FormatReceivedItems()
    {
        string rewards = "Successfully received ";

        if (thisButtonRewards.rewardsOnTier.Length >= 1)
            rewards += string.Format("\n {0}x {1}", thisButtonRewards.rewardsOnTier[0].amountOfEventReward, thisButtonRewards.rewardsOnTier[0].typeOfEventRewardName);

        if (thisButtonRewards.rewardsOnTier.Length >= 2)
            rewards += string.Format("\n {0}x {1}", thisButtonRewards.rewardsOnTier[1].amountOfEventReward, thisButtonRewards.rewardsOnTier[1].typeOfEventRewardName);

        if (thisButtonRewards.rewardsOnTier.Length >= 3)
            rewards += string.Format("\n {0}x {1}", thisButtonRewards.rewardsOnTier[2].amountOfEventReward, thisButtonRewards.rewardsOnTier[2].typeOfEventRewardName);

        return rewards;
    }
    void AddRewards(EventType.RewardType rewardType, int rewardTypeNo)
    {
        if (rewardType == EventType.RewardType.gold)
            Player.Instance.AddGold(thisButtonRewards.rewardsOnTier[rewardTypeNo].amountOfEventReward);
        else if (rewardType == EventType.RewardType.threeMTimer)
        {
            Player.timer3 += thisButtonRewards.rewardsOnTier[rewardTypeNo].amountOfEventReward;
            DataManager.timer3Amount += thisButtonRewards.rewardsOnTier[rewardTypeNo].amountOfEventReward;
        }
        else if (rewardType == EventType.RewardType.fifteenMTimer)
        {
            Player.timer15 += thisButtonRewards.rewardsOnTier[rewardTypeNo].amountOfEventReward;
            DataManager.timer15Amount += thisButtonRewards.rewardsOnTier[rewardTypeNo].amountOfEventReward;
        }
        else if (rewardType == EventType.RewardType.thirtyMTmer)
        {
            Player.timer30 += thisButtonRewards.rewardsOnTier[rewardTypeNo].amountOfEventReward;
            DataManager.timer30Amount += thisButtonRewards.rewardsOnTier[rewardTypeNo].amountOfEventReward;
        }
        else if (rewardType == EventType.RewardType.sixtyMTimer)
        {
            Player.timer60 += thisButtonRewards.rewardsOnTier[rewardTypeNo].amountOfEventReward;
            DataManager.timer60Amount += thisButtonRewards.rewardsOnTier[rewardTypeNo].amountOfEventReward;
        }
        else if (rewardType == EventType.RewardType.diamond)
            Player.Instance.GainDiamond(thisButtonRewards.rewardsOnTier[rewardTypeNo].amountOfEventReward);
        else if (rewardType == EventType.RewardType.squareStone)
        {
            for (int x = 0; x < thisButtonRewards.rewardsOnTier[rewardTypeNo].amountOfEventReward; x++)
                Player.Instance.AddStone("0");
        }
        else if (rewardType == EventType.RewardType.circleStone)
        {
            for (int x = 0; x < thisButtonRewards.rewardsOnTier[rewardTypeNo].amountOfEventReward; x++)
                Player.Instance.AddStone("2");
        }
        else if (rewardType == EventType.RewardType.triangleStone)
        {
            for (int x = 0; x < thisButtonRewards.rewardsOnTier[rewardTypeNo].amountOfEventReward; x++)
                Player.Instance.AddStone("1");
        }
        else if (rewardType == EventType.RewardType.jackPacks)
        {
            Player.jackPack += 1;
        }
        else if (rewardType == EventType.RewardType.reputation)
            Player.Instance.AlterRepValue(true, thisButtonRewards.rewardsOnTier[rewardTypeNo].amountOfEventReward);
        else if (rewardType == EventType.RewardType.energyPack)
            Player.Instance.AddEnergPack(thisButtonRewards.rewardsOnTier[rewardTypeNo].amountOfEventReward);
        else if (rewardType == EventType.RewardType.xcalipoints)
            Player.Instance.AddExcalipoints(thisButtonRewards.rewardsOnTier[rewardTypeNo].amountOfEventReward);
        else if (rewardType == EventType.RewardType.allStones)
        {
            for (int x = 0; x < thisButtonRewards.rewardsOnTier[rewardTypeNo].amountOfEventReward; x++)
            {
                Player.Instance.AddStone("0");
                Player.Instance.AddStone("1");
                Player.Instance.AddStone("2");
            }
        }

        DataManager.Instance.PrepareDataForSparksPushing();

        GameSparksManager.Instance.SavePlayerData(GameSparksManager.playerId);
        GameSparksManager.Instance.SavePlayerExpendables(GameSparksManager.playerId);
        GameSparksManager.Instance.SaveInventoryStones();
        GameSparksManager.Instance.SaveInventoryPacksInfo();

        UIManager.Instance.Update_All();
    }




    public void SetRedeemed(bool isRedeemed)
    {
        redeemed = isRedeemed;
        CheckRedeemed();
    }
    protected virtual void CheckRedeemed()
    {
        redeemedBlocker.SetActive(redeemed);
        if (redeemed == true)
        {
            SetMeInteractable(false);
        }
    }


    // ===== XCALIREWARDS ===== \\ 
    public void SetAvailablility(int rewardAvailability_)
    {
        rewardAvailability = rewardAvailability_;
        DisplayRewardAvailability();
    }

    public void DestroyMe()
    {
        Destroy(gameObject, 0.5f);
    }

    // ===== ConvertValues ===== \\
    #region VALUE CONVERSION
    string AmountConvert(long costToConvert)
    {
        if (costToConvert < 1000)
            return costToConvert.ToString();
        if (costToConvert < 1000000)
            return kConvert(costToConvert);
        if (costToConvert < 1000000000)
            return mConvert(costToConvert);
        return bConvert(costToConvert);
    }
    string kConvert(long value)
    {
        return string.Format("{0}K", value / 1000);
    }
    string mConvert(long value)
    {
        return string.Format("{0}M", value / 1000000);
    }
    string bConvert(long value)
    {
        return string.Format("{0}B", value / 1000000000);
    }

    string AvailabilityConversion(int availType)
    {
        switch(availType)
        {
            case 1:
                return "Coming Next ";
            case 2:
                return "Available Now";
            case 3:
                return "ineligible";
            default:
                return "Unavailable";
        }
    }

    protected string RewardStatusConversion(int rwdStatus)
    {
        switch (rwdStatus)
        {
            case 1:
                return "Claimed";
            case 2:
                return "Unclaimed";
            case 3:
                return "Unavailable";
            default:
                return "Ineligible";
        }
    }
    #endregion
}
