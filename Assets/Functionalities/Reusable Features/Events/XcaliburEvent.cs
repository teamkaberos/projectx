﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class XcaliburEvent : MonoBehaviour
{
    public static bool eventIsOn;

    [Header("Excaliprize  Scriptables")]
    public XcaliprizeType[] availableXcaliprizeList;
    public XcaliprizeType currentXcaliprize;

    // ==== STATUS OF THE PRIZES FOR THE XCALIBUR EVENT ==== \\
    [Space(15)]
    [Header("Player Rewards Status")]
    public int[] playerRewardsStatusList;
    bool RewardClaimAvailability;                // Detemined by whether PLAYER is in Xcalibur-holding Clan

    [Space(15)]
    [Header("UI Displays")]
    // ==== FOR UPDATING THE UI OF XCALI EVENT ==== \\
    public GameObject xcaliprizePrefab;
    public GameObject xcaliprizeDisplayPanel;          // Xcali prize objs holder
    public Transform xcaliprizePanel;
    public List<XcaliRewardButton> xcaliprizeReward = new List<XcaliRewardButton>();
    public Transform frameCurrentAvailExcaliprize;
    int tobeFramed;


    [Space(15)]
    [Header("Response Handling")]
    public GameObject blockingPanel;        // Holds the Loading now info, and waits for Response from Cloud
    public GameObject responsePanel;       // Holds the Panel that displays the Response Message from Cloud
    public Text responseMessageText;       // Displays the Response Message from the Cloud


    [Space(15)]
    [Header("To Next Excaliprize")]
    public static float timeToNextAvailablePrize;
    int timeSlotNow;
    bool beginCountdown;
    public Text timeToNextAvailablePrizeText;
    public TimeSpan timeTillTheNextReward;
    public DateTime[] timesForAvailableRewards = new DateTime[5];     // For Checking if the reward is Available during the update


    void OnApplicationFocus(bool focus)
    {
        if (!focus)
            PlayerPrefs.SetString("currentTime", DateTime.Now.ToString());
        else
            CalculateTimeDifference();
    }
    void CalculateTimeDifference()
    {
        float differenceToAdd = (float) DateTime.Now.Subtract (DateTime.Parse(PlayerPrefs.GetString("currentTime"))).TotalSeconds ;
        timeToNextAvailablePrize -= differenceToAdd;
    }


    void Update()
    {
        if (beginCountdown)
        {
            timeToNextAvailablePrize -= Time.deltaTime;
            DisplayTimeToNextRewardText(ToNextRewardTime());
            if (timeToNextAvailablePrize <= 0)
            {
                timeToNextAvailablePrize = 21600f;
                xcaliprizeReward[tobeFramed].SetMeInteractable(false);
                SetTheNewAvailableReward();
                CheckForClaimedRewards();
                RetrievePlayerClaimedRewards();
                if (tobeFramed >= xcaliprizeReward.Count)
                {
                    StopCountdown();
                }
            }
        }
    }

    // ==== PREPERATION OF THE XCALIBUR EVENT PANEL ==== \\
    public void PrepareExcaliprizes(bool eventOn, int activeEvent)
    {
        eventIsOn = eventOn;

        // assign stuff regardless of event status
        AssignExcaliprize(activeEvent - 1);
        GetPrizeNamesAndRewards();
        CreatePrizeList();
        AssignRewardToButtons();

        if (!eventOn)
            FrameUpCurrentAvailable();

        // Do the Stuff if the event is ON
        PrepareRewardAvailableTimes(TimeMaster.loginTime);
        GetTimeToNextReward();
        // Do the Stuff if the event if OFF

        // only retrieve player's reward info if the event is off so player can claim rewards
        RetrievePlayerClaimedRewards();
    }
    void AssignExcaliprize(int excaliprizeSeason)
    {
        // FINDING THE CORRECT EXCALIPRIZE REWARD SYSTEM FOR THIS SEASON
        currentXcaliprize = availableXcaliprizeList[excaliprizeSeason];
    }
    void GetPrizeNamesAndRewards()
    {
        currentXcaliprize.PrepExcaliRewardList(); 
    }
    void CreatePrizeList()
    {
        //INSTANTIATING THE REWARD BUTTONS AND ADDING THEM TO THE LIST
        for (int x = 0; x < currentXcaliprize.xcalirewards.Length; x++)
        {
            xcaliprizeReward.Add(ExcaiprizeObj(xcaliprizePanel.position, xcaliprizePanel).GetComponent<XcaliRewardButton>());
            xcaliprizeReward[x].AssignXcaliburEvent(this);
        }
    }
    #region EXCALIPRIZE BUTTON CREATION
    GameObject ExcaiprizeObj(Vector3 position, Transform parent)
    {
        GameObject excaliprizeOBJ = Instantiate(xcaliprizePrefab, position, Quaternion.identity, parent);
        return excaliprizeOBJ;
    }
    #endregion
    void AssignRewardToButtons()
    {
        for (int x = 0; x < xcaliprizeReward.Count; x++)
        {
            xcaliprizeReward[x].thisButtonRewards = currentXcaliprize.xcalirewards[x];
            xcaliprizeReward[x].UpdateButtonEventRewardInfo();
            xcaliprizeReward[x].DisplayRewardAvailability();
            xcaliprizeReward[x].AssignToButton();
        }
    }


    // ==== Retrieval of Player claimed rewards ==== \\
    void RetrievePlayerClaimedRewards()
    {
        // Call retrieval from Cloud and response handled by CheckForClaimedRewards
        GameSparksManager.Instance.RetrievePlayerXcaliprizeInfo(this);
    }
    public void PlayerExcaliprizeRetrieved(string retrievedReward)
    {
        string[] rewards = retrievedReward.Split(',');

        playerRewardsStatusList = new int[rewards.Length];

        for (int x = 0; x < playerRewardsStatusList.Length; x++)
        {
            playerRewardsStatusList[x] = int.Parse(rewards[x]);
        }
        // HANDLE THE REWARDS BUTTON TOGGLES HERE
        CheckForClaimedRewards();
    }
    void CheckForClaimedRewards()
    {
        for (int x = 0; x < xcaliprizeReward.Count; x++)
        {
            if (x < tobeFramed)
            {
                if (playerRewardsStatusList[x] == 1)
                    xcaliprizeReward[x].SetRewardStatus(1);
                else
                    xcaliprizeReward[x].SetRewardStatus(2);

                xcaliprizeReward[x].SetRedeemed(true);
            }
            else if (x == tobeFramed)
            {
                if (playerRewardsStatusList[x] == 1)
                {
                    xcaliprizeReward[x].SetRewardStatus(1);
                    xcaliprizeReward[x].SetRedeemed(true);
                }
                else
                    SetCollectNow(tobeFramed);
            }
        }
    }


    // ==== ASSIGN THE CURRENT AVAILABLE REWARD ==== \\
    void FrameUpCurrentAvailable()
    {
        frameCurrentAvailExcaliprize.gameObject.SetActive(false);           // Hide the object first
        SetTheCurrentAvailableReward();
        MoreFrameToCurrentAvailable();                                      // Display the object after moving

        if (tobeFramed < xcaliprizeReward.Count)
            SetUpNextAvailable();
    }
    void SetTheCurrentAvailableReward()
    {
        DateTime nowTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddMilliseconds(DataManager.initialLoginServerTime * 1000);
        int dayToJump = (int)nowTime.DayOfWeek;
        int hourToJump = nowTime.Hour;

        tobeFramed = (dayToJump - 1) * 4 + Mathf.FloorToInt(hourToJump / 6);        
    }
    void SetTheNewAvailableReward()
    {
        tobeFramed += 1;
        MoreFrameToCurrentAvailable();
    }
    void MoreFrameToCurrentAvailable()
    {
        if (tobeFramed < xcaliprizeReward.Count)
        {
            frameCurrentAvailExcaliprize.position = xcaliprizeReward[tobeFramed].transform.position;
            frameCurrentAvailExcaliprize.SetParent(xcaliprizeReward[tobeFramed].transform);
            frameCurrentAvailExcaliprize.gameObject.SetActive(true);
        }
        else
        {
            frameCurrentAvailExcaliprize.gameObject.SetActive(false);
        }
    }
    void SetCollectNow(int buttonToBeSet)
    {
        if (EventHolderManager.xcaliburHolderHistory.Count > 0)
        {
            if (Clan.clanID == EventHolderManager.xcaliburHolderHistory[0].clanName)
            {
                xcaliprizeReward[buttonToBeSet].SetMeInteractable(true);
                xcaliprizeReward[buttonToBeSet].SetAvailablility(2);
            }
            else
            {
                xcaliprizeReward[buttonToBeSet].SetMeInteractable(false);
                xcaliprizeReward[buttonToBeSet].SetAvailablility(3);
            }
        }
    }
    void SetUpNextAvailable()
    {
        xcaliprizeReward[tobeFramed + 1].SetAvailablility(1);
    }


    // ==== RETRIEVAL OF REWARDS ==== \\
    public void AttemptRewardRetrieval(XcaliRewardButton xcaliButton)
    {
        // DisplayPopup to prevent further Action
        WaitingForResponse(true);
        // Set Collected prizes array
        SetRewardClaimed();
        // Update the UI on the Button
        xcaliButton.SetRewardAvailable(false);
        // Added on Xcaliprize buttons
        GameSparksManager.Instance.ClaimXcaliPrize( this , RewardString(), xcaliButton) ;
    }
    void SetRewardClaimed()
    {
        //for (int x = 0; x < playerRewardsStatusList.Length; x++)
            playerRewardsStatusList[tobeFramed] = 1;
    }
    public void SuccessfulRewardClaim(XcaliRewardButton xcaliButton)
    {
        xcaliButton.RewardRedemption();
        ResponsePanel(xcaliButton.FormatReceivedItems());
    }


    // ==== NEXT AVAILABLE REWARD ==== \\
    void PrepareRewardAvailableTimes(DateTime timeToBeUsed)
    {
        if (timeToBeUsed.Hour >= 18)
        {
            int hours = 23 - timeToBeUsed.Hour;
            int mins = 60 - timeToBeUsed.Minute;
            int seconds = 60 - timeToBeUsed.Second;

            timeTillTheNextReward = new TimeSpan(hours, mins, seconds);
        }
        else if (timeToBeUsed.Hour >= 12)
        {
            int hours = 17 - timeToBeUsed.Hour;
            int mins = 60 - timeToBeUsed.Minute;
            int seconds = 60 - timeToBeUsed.Second;

            timeTillTheNextReward = new TimeSpan(hours, mins, seconds);
        }
        else if (timeToBeUsed.Hour >= 6)
        {
            int hours = 11 - timeToBeUsed.Hour;
            int mins = 60 - timeToBeUsed.Minute;
            int seconds = 60 - timeToBeUsed.Second;

            timeTillTheNextReward = new TimeSpan(hours, mins, seconds);
        }
        else if (timeToBeUsed.Hour >= 0)
        {
            int hours = 5 - timeToBeUsed.Hour;
            int mins = 60 - timeToBeUsed.Minute;
            int seconds = 60 - timeToBeUsed.Second;

            timeTillTheNextReward = new TimeSpan(hours, mins, seconds);
        }

        timeToNextAvailablePrize = (float)timeTillTheNextReward.TotalSeconds;

        DisplayTimeToNextRewardText(ToNextRewardTime());
    }
    void GetTimeToNextReward()
    {
        if (tobeFramed < xcaliprizeReward.Count)
        {
            StartCountdown();
        }
        else
            DisplayTimeToNextRewardText("Reward redemption expired.");
    }
    public void UpdateToNextRewardTime()
    {
        //timeToNextAvailablePrize = (float)timesForAvailableRewards[timeSlotNow + 1].Subtract(TimeMaster.loginTime).TotalSeconds - Time.timeSinceLevelLoad;
    }
    void DisplayTimeToNextRewardText(string messageToDisplay)
    {
        timeToNextAvailablePrizeText.text = messageToDisplay;
    }
    void StartCountdown()
    {
        beginCountdown = true;
    }
    void StopCountdown()
    {
        beginCountdown = false;
        DisplayTimeToNextRewardText("Reward redemption expired.");
    }


    // ===== HIDING OF PANEL ===== \\
    public void HideXcaliprizePanel()
    {
        xcaliprizeDisplayPanel.SetActive(false);
    }


    // ==== DESTROY REWARDS ==== \\
    public void DestroyRewards()
    {
         for(int x = 0; x <xcaliprizeReward.Count; x++)
        {
            xcaliprizeReward[x].DestroyMe(); 
        }
        xcaliprizeReward.Clear();
    }


    // ==== RESPONSE DISPLAYS ==== \\
    public void WaitingForResponse(bool isWaiting)
    {
        blockingPanel.SetActive(isWaiting);
    }
    public void ResponsePanel(string responseMessage)
    {
        WaitingForResponse(false);

        responseMessageText.text = responseMessage;
        responsePanel.SetActive(true);
    }
    public void HideResponsePanel()
    {
        responsePanel.SetActive(false);
    }


    // ==== Reward Conversions ==== \\
    int RewardAvailable(string typeOfAvail)
    {     
        if (typeOfAvail == "coming")
            return 1;
        if (typeOfAvail == "available")
            return 2;
        return 0;
    }

    string RewardString()
    {
        string toReturn = "";
        for (int x = 0; x < playerRewardsStatusList.Length; x++)
        {
            if (x == playerRewardsStatusList.Length - 1)
            {
                toReturn += playerRewardsStatusList[x].ToString();
                break;
            }
            toReturn += playerRewardsStatusList[x].ToString() + ",";
        }
        return toReturn;
    }

    string ToNextRewardTime()
    {
        int hours = Mathf.FloorToInt(timeToNextAvailablePrize / 3600f);
        int minutes = Mathf.FloorToInt((timeToNextAvailablePrize - (hours * 3600)) / 60f);
        int seconds = Mathf.FloorToInt(timeToNextAvailablePrize - (hours * 3600) - (minutes * 60));

        string toReturn = "";
        toReturn = string.Format("Next reward available in : {0:00}:{1:00}:{2:00}", hours, minutes, seconds);

        return toReturn;
    }
}
