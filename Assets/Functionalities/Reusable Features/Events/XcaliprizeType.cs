﻿using UnityEngine;


[CreateAssetMenu(fileName = "Excaliprize", menuName = "Excaliprizes", order = 1)]
public class XcaliprizeType : ScriptableObject
{
    public RewardImages rewardImages;

    public EventType.RewardTier[] xcalirewards;        // the type and value of rewards if the Clan holds Excalibur


    public void PrepExcaliRewardList()
    {
        for (int x = 0; x < xcalirewards.Length; x++)
        {
            for (int y = 0; y < xcalirewards[x].rewardsOnTier.Length; y++)
            {
                xcalirewards[x].rewardsOnTier[y].GetRewardInfo(rewardImages);
                xcalirewards[x].rewardsOnTier[y].GetRewardName(rewardImages);
            }
        }
    }
}