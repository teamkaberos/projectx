﻿using UnityEngine;

[CreateAssetMenu(fileName = "EventType", menuName = "Event", order = 1)]
public class EventType : ScriptableObject
{
    public RewardImages rewardImages;
    // EVENTS SCRIPTABLES
    public enum RewardType
    {
        gold,
        threeMTimer,
        fifteenMTimer,
        thirtyMTmer,
        sixtyMTimer,
        diamond,
        squareStone,
        circleStone,
        triangleStone,
        reputation,
        energyPack,
        xcalipoints,
        allStones,
        jackPacks,
        queenPacks,
        kingPacks
    }
    [System.Serializable]
    public class RewardSort
    {
        public RewardType typeOfEventReward;
        public string typeOfEventRewardName;
        public int amountOfEventReward;
        public Sprite eventRewardImage;

        public void GetRewardInfo(RewardImages rewardImages)
        {
            eventRewardImage = rewardImages.ImageToReturn(typeOfEventReward);
        }
        public void GetRewardName(RewardImages rewardImages)
        {
            typeOfEventRewardName = rewardImages.NameToReturn(typeOfEventReward);
        }
    }
    [System.Serializable]
    public class RewardTier
    {
        public RewardSort[] rewardsOnTier; 
    }

    public string eventName;                            // Event Identifier
    public int eventInt;
    public string eventCondition;

    public RewardTier[] playerEventRewards;     // Array of rewards for solo players
    public int[] rewardRequirement;                 // Progress value that player has to meet to obtain said tier of reward

    public RewardTier[] clanEventRewards;       // Array of rewards when player is in a Clan
    public int[] clanRewardRequirements;         // Progresss value of the Clan to recieve said tier of reward

    public void PrepareRewardImages()
    {
        //for (int x = 0; x < playerEventRewards.l)
        PrepPlayerRewardsList();
        PrepClanRewardsList();
    }
    void PrepPlayerRewardsList()
    {
        for (int x =0; x < playerEventRewards.Length; x++)
        {
             for (int y = 0; y < playerEventRewards[x].rewardsOnTier.Length; y++)
            {
                playerEventRewards[x].rewardsOnTier[y].GetRewardInfo(rewardImages);
                playerEventRewards[x].rewardsOnTier[y].GetRewardName(rewardImages);
            }
        }
    }
    void PrepClanRewardsList()
    {
        for (int x = 0; x < clanEventRewards.Length; x++)
        {
            for (int y = 0; y < clanEventRewards[x].rewardsOnTier.Length; y++)
            {
                clanEventRewards[x].rewardsOnTier[y].GetRewardInfo(rewardImages);
                clanEventRewards[x].rewardsOnTier[y].GetRewardName(rewardImages);
            }
        }
    }

    public int[] RewardsCount()
    {
        return new int[playerEventRewards.Length];
    }
    public int[] ClanRewardsCount()
    {
        return new int[clanEventRewards.Length];
    }
}