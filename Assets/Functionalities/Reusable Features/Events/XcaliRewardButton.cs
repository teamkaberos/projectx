﻿using UnityEngine.UI;
using UnityEngine;

public class XcaliRewardButton : RewardButton
{
    [Space(10)]
    [Header("Xcalirewards Area")]
    public int rewardStatus;                // Whether it is claimed or unclaimed

    public GameObject claimedDisplay;   // To Display if this reward is claimed or unclaimed
    public Text rewardStatusText;           // To Display if this reward is claimed or unclaimed

    public XcaliburEvent xcaliburEvent;


    // ==== Info assignment ==== \\
    public void AssignXcaliburEvent(XcaliburEvent xcalievent)
    {
        xcaliburEvent = xcalievent;
    }
    protected override void ListenerAssign()
    {
        collectThisRewardButton.onClick.AddListener(delegate { xcaliburEvent.ResponsePanel("Attempting reward retrieval"); });
        collectThisRewardButton.onClick.AddListener ( delegate { xcaliburEvent.AttemptRewardRetrieval(this); });
    }

    // ==== Update Display on the UI ==== \\s
    public void SetRewardStatus(int rewardStatus_)
    {
        rewardStatus = rewardStatus_;
    }
    public void SetRewardStatusText()
    {
        rewardStatusText.text = RewardStatusConversion(rewardStatus);
    }
    protected override void CheckRedeemed()
    {
        if (rewardStatus == 1)
            claimedDisplay.SetActive(true);
        else if (rewardStatus == 2)
            claimedDisplay.SetActive(false);
        else if (rewardStatus == 3)
            claimedDisplay.SetActive(false);

        SetRewardStatusText();

        base.CheckRedeemed();
    }

    public void SetRewardAvailable(bool isAvailable)
    {
        collectThisRewardButton.interactable = isAvailable;
    }

    public void DestroyReward()
    {
        Destroy(gameObject, 0.1f);
    }

}
