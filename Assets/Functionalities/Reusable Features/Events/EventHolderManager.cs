﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EventHolderManager : MonoBehaviour
{
    // =============== CLOUD INFO ABOUT THE CURRENT EVENTS ================= \\
    /* These info does will not be changed by the players, it will only be changed
     * when the timer countdown hits 0 */

    // ================= PLAYER'S EVENT PROGRESSION INFO =============== \\
    [Space(10)]
    [Header("Player points")]
    public Text playerPointsAccumulatedText;        // Points accumulated display
    public static int playerProgression;                    // Points accumulated by the Player for this event in this season
    // ================= PLAYER'S CLAN EVENT PROGRESSION INFO =============== \\
    [Space(10)]
    [Header("Player Clan points")]
    public Text playerClanPointsAccumulatedText;        // Points accumulated display
    public static int playerClanProgression;                    // Points accumulated by the Player for this event in this season

    public static string clanWhenEventStarted;                // Used to track if a player has left the Clan before the event has ended

    public static string xcaliburHolder;                // Tracks which Clan is currently holding the Xcalibur


    public enum EventLiveStatus
    {
        live,
        unlive,
        canceled
    }
    public static EventLiveStatus currentEventActivityStatus;          // bool from server that will determine if the EVENTs are LIVE
    public static bool currentEventIsActive;                    // Checks if the Player can accumulate Points for the current event
    public static int currentActiveEvent;                        // Int of the current event
    public static int currentEventSeason;                      // Season of the event
    static int currentEventRunningDay;                        // Which day of the week is the event on
    static long timeTillNextEvent;                               // Countdown till the current event ends
    public static bool lastEventCanceled;                    // To disable the Xcalibur reward if there is a fault in the last event
    bool startCounting;                                              // Used to track if the event has expired if the day has passed

    [Space(10)]
    [Header("Timespan Display")]
    TimeSpan timespanDisplay;
    float timeToEventStatusChange;      // Time before the Event STARTS/ENDS
    bool isRetrieving;                            // To prevent multiple retrieval of Events from Cloud
    public Text durationOfEventText;    // shows start and end time of this event
    public Text leaderboardText;           // leaderboard display + duration of this leaderboard
    static long startTimeLong;
    static long endTimeLong;


    [Space(10)]
    [Header("Event Toggle")]
    public Toggle eventPanelToggle;      // refers to the same thing, but different component
    public GameObject eventToggle;      // refers to the same thing, but different component
    public Text hUDEventStatusNotice;

    // ======================= PLAYERS INFO =========================== \\
    [Serializable]
    public class MyRewards
    {
        public int[] rewardsRedemption;

        public string RedemptionToBeSaved()
        {
            string toBeSaved = "";
            for (int x = 0; x < rewardsRedemption.Length; x++)
            {
                if (x < rewardsRedemption.Length - 1)
                    toBeSaved += rewardsRedemption[x] + ",";
                else
                    toBeSaved += rewardsRedemption[x];
            }
            return toBeSaved;
        }
    }
    public static MyRewards myObtainedRewards;
    public static MyRewards myObtainedClanRewards;


    [Serializable]
    public class XcaliburHolder
    {
        public string clanName;       // the Clan that held the Xcalibur for this event
        public int eventType;           // the type of event this Xcalibur was attained
        public int season;                // the season this Xcalibur was attained
        public int objNo;                 // for keeping track of which is the lastest OBJ
        public int points;                // points achieved by the Clan who won this event
        public bool isCanceled;       // to disable Xcalibur rewards if there is a fault
    }
    public static List<XcaliburHolder> xcaliburHolderHistory = new List<XcaliburHolder>();

    // ================== GAME REQUIRED INFO FOR EVENTS ==================== \\
    [Space(10)]
    [Header("Events Scriptable")]
    public EventType[] allAvailableEvents;         /// Holder of all Scriptable Events
    public EventType currentActiveEventOBJ;    // Holder of the Active Scriptable Event


    // ==================== EVENTS HANDLERS =========================== \\
    [Space(10)]
    [Header("Reward Buttons")]
    public GameObject eventPanel;                               // Event Panel GameObject
    List<RewardButton> redeemPlayerRewardButtons = new List<RewardButton>();        // Reward Redemption buttons for Player
    List<RewardButton> redeemClanRewardButtons = new List<RewardButton>();        // Reward Redemption buttons for Clan

    public GameObject rewardButtonPrefab;                // The Reward Button Prefab to be instantiated
    public Transform playerScrollPanel;                       // Where to place the instantiated Buttons for Players rewards
    public Transform clanScrollPanel;                          // Where to place the instantiated Buttons for Players rewards
    public GameObject pScrollBlocker;                        // When the countdown time reaches 0, popup to block Collection
    public GameObject  clanScrollBlocker;                   // When the countdown time reaches 0, popup to block Collection
    public Text clanBlockerText;                                 // Displays Clan rewards info => eg if Player is in/not in CLAN

    [Space(10)]
    [Header("Excalibur Events")]
    public XcaliburEvent xcaliburEvent;                        

    [Space(10)]
    [Header("Event Info Text")]
    public Text eventName;
    public Text eventTimeCountdown;
    public Text eventConditionText;
    public GameObject eventConditionHeader;
    public Text lastEventXcaliburHolder;
    Vector4 fadedTextColor = new Vector4(1,1,0,0.5f);
    Vector4 fullTextColor = new Vector4(1,1,0,1);

    [Space(10)]
    [Header("Response Popup")]
    // popup OBJ and text
    public GameObject popupPanel;                 // Message Popup
    public Text responseHandlerText;              // Messages sent to the Player

    [Space(10)]
    [Header("Event Point Adding")]
    public static bool toAddEventPoint;
    public static string toCheckEvent = "none_To_Update";
    public static int pointsToAdd;

    [Space(10)]
    [Header("Test Buttons")]
    public Button GSLoginButton;
    public Button testSendButton;

    #region Singleton
    static EventHolderManager _instance;
    public static EventHolderManager Instance
    {
        get
        {
            return _instance;
        }
    }

    void Awake()
    {
        _instance = this;
    }
    #endregion

    void Start()
    {

        // For Tests
        //GSLoginButton.onClick.AddListener(GameSparksManager.Instance.ForEventTestGSLogin);
        CheckRewardButtons();
        //GetCurrentXcaliburHolder();
        GetCurrentEvent();
    }
    void Update()
    {
        if (startCounting)
        {
            if (currentEventActivityStatus == EventLiveStatus.live)
            {
                timeToEventStatusChange -= Time.deltaTime;

                if (timeToEventStatusChange <= 0)
                {
                    // make sure the time stays at 0
                    timeToEventStatusChange = 0;
                    RewardCollectionToggle(true);

                    if (!isRetrieving)
                    {
                        ToggleIsRetrieving(true);
                        // When the timer to change the status of an event hits 0, running = ended, ended = running
                        if (currentEventIsActive)
                        {
                            //means the event will end here, but Reward Buttons still exist for Players to Collect
                            xcaliburEvent.DestroyRewards();
                            CheckRewardButtons();
                            Invoke("GetCurrentEvent", 3f);
                        }
                        else if (!currentEventIsActive)
                        {
                            //means the event starts here, so remove the Old Buttons, and create new ones with new rewards
                            CheckRewardButtons();
                            Invoke("GetCurrentEvent", 3f);
                        }
                    }
                }
                DisplayEventTime();
            }
            else
            {
                DisplayEventCanceled();
            }
        }
    }


    // ==== Destroy any existing buttons in the Reward Collection Panel ===== \\
    void CheckRewardButtons()
    {
        CheckPlayerRewardButtons();
        CheckClanRewardButtons();
    }
    void CheckPlayerRewardButtons()
    {
        // If the PLAYER reward panel already has buttons, destroy them
        if (redeemPlayerRewardButtons.Count >= 1)
        {
            for (int x = 0; x < redeemPlayerRewardButtons.Count; x++)
                redeemPlayerRewardButtons[x].DestroyMe();
        }
        redeemPlayerRewardButtons.Clear();
    }
    void CheckClanRewardButtons()
    {
        // If the CLAN reward panel already has buttons, destroy them
        if (redeemClanRewardButtons.Count >= 1)
        {
            for (int x = 0; x < redeemClanRewardButtons.Count; x++)
                redeemClanRewardButtons[x].DestroyMe();
        }
        redeemClanRewardButtons.Clear();
    }

    // ==== Obtaining info about any current LIVE events ==== \\
    public void GetCurrentEvent()
    {
        GameSparksManager.Instance.RetrieveCurrentEvents();
        // Success response => CurrentEventsRetrieved && Attempts to Retrieve Players progression , Failed => EventsPopup
    }
    void GetCurrentXcaliburHolder()
    {
        GameSparksManager.Instance.RetrieveExcaliburHolder();
    }
    void RewardCollectionToggle(bool toBlock)
    {
        PlayerCollectionPanelToggle(toBlock);
        ClanCollectionPanelToggle(toBlock);
    }
    #region Collection Panels Toggle
    void PlayerCollectionPanelToggle(bool toBlock)
    {
        pScrollBlocker.SetActive(toBlock);
    }
    void SetClanBlockerText(string textToDisplay)
    {
        clanBlockerText.text = textToDisplay;
    }
    void ClanCollectionPanelToggle(bool toBlock)
    {
        clanScrollBlocker.SetActive(toBlock);
    }
    #endregion


    void StatusCheck(string status)
    {
        if (status.ToLower() == "unlive")
            currentEventActivityStatus = EventLiveStatus.unlive;
        else if (status.ToLower() == "canceled")
            currentEventActivityStatus = EventLiveStatus.canceled;
        else
            currentEventActivityStatus = EventLiveStatus.live;
    }
    string StatusUpdate()
    {
        if (currentEventActivityStatus == EventLiveStatus.unlive)
            return "Event has ended";
        if (currentEventActivityStatus == EventLiveStatus.canceled)
            return "Event has been canceled";
        return "Event is now Live";
    }
    void DisplayUpdate()
    {
        if (currentEventActivityStatus == EventLiveStatus.live)
        {
            // if the STATUS IS LIVE, (not Unlive or Canceled)
            if (currentEventIsActive)
                hUDEventStatusNotice.text = "<color=#00FF00>Live</color>";
            else
                hUDEventStatusNotice.text = "<color=#FF0000>Over</color>";
        }
        else
            hUDEventStatusNotice.text = "<color=#AAAAAA>Canceled</color>";
    }

    // WHEN THE SERVER RESPONDS WITH THE "GLOBAL EVENT COUNTER INFO", THIS INFO IS USED HERE FIRST
    public void CurrentEventsRetrieved(int activeEvent, int eventSeason, string eventActivity, int eventDay, string eventLiveStatus)
    {
        currentEventIsActive = IsEventActive(eventActivity);
        leaderboardText.gameObject.SetActive(IsEventActive(eventActivity));
        currentActiveEvent = activeEvent;
        //eventPanelToggle.interactable = currentEventIsActive;
        currentEventSeason = eventSeason;
        currentEventRunningDay = eventDay;
        currentActiveEventOBJ = CurrentEvent(activeEvent);
        timeToEventStatusChange = DifferenceTillEventStatucChange();

        StatusCheck(eventLiveStatus);

        SetRewardsRedemption();
        DisplayEventName();
        DisplayEventCondition();

        startCounting = true;

        PrepareExcaliprizes(currentEventIsActive, activeEvent);

        CreateRedeemableRewardsButton();
        EventProgressPointRetrieve();

        SetEventDurationDate();
        DisplayUpdate();
    }
    public void SetEventActivityFromServer(string eventActivityStatus, string eventLiveStatus)
    {
        // toggle the Xcalibur event LOADING SCREEN THE FIRST THING TO PREVENT MORE INTERACTION
        xcaliburEvent.WaitingForResponse(true);
        xcaliburEvent.HideResponsePanel();

        // set activity in GAME
        currentEventIsActive = IsEventActive(eventActivityStatus);

        StatusCheck(eventLiveStatus);

        //if (currentEventActivityStatus != EventLiveStatus.live)
        //    xcaliburEvent.HideXcaliprizePanel();
        //else
        //{
        //    xcaliburEvent.HideXcaliprizePanel();
        //}

        // turn off the XCALIBUR holder prize obj
        UpdateLastXcaliburHolder();                                            // Xcalibur button will be off if event is set to FALSE

        // then, turn off the popup and inform PLAYERs of the event status
        xcaliburEvent.ResponsePanel(StatusUpdate());
        DisplayUpdate();
    }
    void PrepareExcaliprizes(bool eventOn, int activeEvent)
    {
        xcaliburEvent.PrepareExcaliprizes(eventOn, activeEvent);
    }
    public void AttemptEventProgressPointRetrieval()
    {
        GameSparksManager.Instance.RetrieveEventContributionPoints();
    }
    public void PlayersProgressionRetrieved(int playerProgress, string initialClan, string[] rewards, string[] clanRewards)
    {
        playerProgression = playerProgress;
        clanWhenEventStarted = initialClan;

        myObtainedRewards.rewardsRedemption = RedeemedPlayerRewards(rewards);
        myObtainedClanRewards.rewardsRedemption = RedeemedClanRewards(clanRewards);

        DisplayPlayerPoints();

        CheckForEligibleToClaimPlayerRewards();

        CheckForPlayerRedeemedRewards();

        PlayerCollectionPanelToggle(false);

        // Check if the initial Clan player joined is the same as the current one
        if (Clan.clanID == "<currently_not_in_any_clan_yet>")
        {
            // If the player is not in a Clan now
            NotInClanProgressRetrieved();
        }
        else
        {
            // if PLAYER is in a CLAN RIGHT NOW
            if (Clan.clanID == initialClan)
            {
                // if this PLAYER is the extremely loyal one, let him get the CLAN contribution Points
                UpdateLastXcaliburHolder();
                GameSparksManager.Instance.RetrieveClanEventContributionPoints();
            }
            else
            {
                ClanProgressRetrieved(0);
            }
        }
    }
    public void ClanRejoinedCheck()
    {
        if (Clan.clanID == clanWhenEventStarted)
            GameSparksManager.Instance.RetrieveClanEventContributionPoints();
    }
    public void ClanProgressRetrieved(int clanPoints)
    {
        playerClanProgression = clanPoints;

        DisplayPlayClanPoints();
        CheckForEligibleToClaimClanRewards();

        CheckForClanRedeemedRewards();
        ClanCollectionPanelToggle(false);
        CheckForPendingPoints();

        ToggleIsRetrieving(false);
    }
    public void NotInClanProgressRetrieved()
    {
        playerClanProgression = 0;
        DisableClanEventButtonRedemption();
        DisplayPlayClanPoints();
        SetClanBlockerText("Currently not in a CLAN");
    }
    public void DisableClanOBJCollection()
    {
        ClanCollectionPanelToggle(true);
    }
    void DisableClanEventButtonRedemption()
    {
        for (int x = 0; x < redeemClanRewardButtons.Count; x++)
        {
            if (playerClanProgression >= redeemClanRewardButtons[x].rewardAvailability)
                redeemClanRewardButtons[x].SetMeInteractable(true);
            else
                redeemClanRewardButtons[x].SetMeInteractable(false);
        }
    }
   
    void SetEventDurationDate()
    {
        DateTime nowDate = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddMilliseconds(DataManager.initialLoginServerTime * 1000);
        TimeSpan difference = new TimeSpan();

        if (currentEventRunningDay >= 5)
            difference = new TimeSpan(2 + currentEventRunningDay - 5, 0, 0, 0);
        else
            difference = new TimeSpan(2 + currentEventRunningDay, 0, 0, 0);

        DateTime startDate = nowDate.Subtract(difference);
        TimeSpan duration = new TimeSpan(1, 0, 0, 0);
        DateTime endDate = startDate.Add(duration);

        startTimeLong = new DateTimeOffset(startDate.Year,startDate.Month,startDate.Day, 0,0,0, TimeSpan.Zero).ToUnixTimeSeconds();
        endTimeLong = new DateTimeOffset(endDate.Year, endDate.Month, endDate.Day, 0, 0, 0, TimeSpan.Zero).ToUnixTimeSeconds();


        durationOfEventText.text = string.Format("(UTC) \n {0}/{1} 00:00 - {2}/{3} 23:59", startDate.Day, startDate.Month, endDate.Day, endDate.Month);
        //leaderboardText.text = string.Format("Leaderboard \n (UTC) \n {0}/{1} 00:00 - {2}/{3} 23:59", startDate.Day, startDate.Month, endDate.Day, endDate.Month);

        if (currentEventIsActive != true)
            durationOfEventText.gameObject.SetActive(false);
        else
            durationOfEventText.gameObject.SetActive(true);
    }
    // Response handling when attempting to save info with GS.SaveEventProgress
    void ToggleIsRetrieving(bool isRetrievingNow)
    {
        // Check if the client is pulling info from the Cloud
        isRetrieving = isRetrievingNow;
    }

    int[] RedeemedPlayerRewards(string[] rewardString)
    {
        // to parse the rewards that are from the cloud
        int[] toReturn = new int[rewardString.Length];
        for (int x = 0; x < rewardString.Length; x++)
        {
            toReturn[x] = int.Parse(rewardString[x]);
        }

        int[] toBeUsed = new int[currentActiveEventOBJ.playerEventRewards.Length];
        for (int x = 0; x < toBeUsed.Length; x++)
        {
            if (x < toReturn.Length)
                toBeUsed[x] = toReturn[x];
            else
                toBeUsed[x] = 0;
        }

        return toBeUsed;
    }
    int[] RedeemedClanRewards(string[] rewardString)
    {
        // to parse the rewards that are from the cloud
        int[] toReturn = new int[rewardString.Length];
        for (int x = 0; x < rewardString.Length; x++)
        {
            toReturn[x] = int.Parse(rewardString[x]);
        }

        int[] toBeUsed = new int[currentActiveEventOBJ.clanEventRewards.Length];
        for (int x = 0; x < toBeUsed.Length; x++)
        {
            if (x < toReturn.Length)
                toBeUsed[x] = toReturn[x];
            else
                toBeUsed[x] = 0;
        }

        return toBeUsed;
    }

    bool IsEventActive(string eventActivity)
    {
        if (eventActivity == "true")
            return true;
        return false;
    }
    EventType CurrentEvent(int eventInt)
    {
        for (int x = 0; x < allAvailableEvents.Length; x++)
        {
            if (allAvailableEvents[x].eventInt == eventInt)
            {
                allAvailableEvents[x].PrepareRewardImages();
                return allAvailableEvents[x];
            }
        }
        return null;
    }
    string CurrentActiveEventName(string eventName_)
    {
        if (currentEventActivityStatus != EventLiveStatus.canceled)
        {
            if (currentEventIsActive)
                return eventName_ + " \n(Event Ongoing)";
            return eventName_ + " \n(Event Over)";
        }
        return eventName_ + " \n (Event Canceled)";
    }


    // ====== Holders of the Xcalibur ====== \\
    public void ClearOldList()
    {
        xcaliburHolderHistory.Clear();
    }
    public void AddToXcaliburHistoryList(string clanID, int objNo, int points, string isCanceled)
    {
        int season = Mathf.FloorToInt(objNo - 1) / 4;
        int eventType = objNo - (season * 4) + 1;

        XcaliburHolder history = new XcaliburHolder
        {
            clanName = clanID,
            eventType = eventType,
            objNo = objNo,
            season = season,
            points = points,
            isCanceled = bool.Parse(isCanceled)
        };

        xcaliburHolderHistory.Add(history);
    }
    public void UpdateLastXcaliburHolder()
    {
        if (GameSparksManager.playerDetailsObtained == false)
            return;

        for (int x = 0; x < xcaliburHolderHistory.Count; x++)
        {
            // Check if the previous event was canceled
            if (xcaliburHolderHistory[0].isCanceled == true)
            {
                DisplayTheCurrentHolder("Previous Event Canceled");
            }
            else
            {
                if (xcaliburHolderHistory[x].clanName != "<null>" & xcaliburHolderHistory[x].isCanceled != true)
                {
                    DisplayTheCurrentHolder(xcaliburHolderHistory[x].clanName);
                    break;
                }
                if (x == xcaliburHolderHistory.Count)
                    DisplayTheCurrentHolder("HoundsOfHell");
            }
        }
    }
    public void NoPreviousHolder()
    {
        DisplayTheCurrentHolder("No one wields Xcalibur");
    }
    public void DisplayTheCurrentHolder(string currentXcaliHolder)
    {
        if (Clan.clanJoinedTimeStampCheck == false)
            return;

        lastEventXcaliburHolder.text = currentXcaliHolder;

        if (currentEventActivityStatus == EventLiveStatus.live)
        {
            if (ChampionClanJoinedDuringExcaliburEvent())
                UIManager.Instance.XcaliburRewards(true);
            else
                UIManager.Instance.XcaliburRewards(false);
        }
        else if (currentEventActivityStatus == EventLiveStatus.canceled)
        {
            UIManager.Instance.XcaliburRewards(false);
        }
        else if (currentEventActivityStatus == EventLiveStatus.canceled)
            UIManager.Instance.XcaliburRewards(false);
    }
    bool ChampionClanJoinedDuringExcaliburEvent()
    {
        // if the player is currently is the latest clan holding the xcalibur
        if (Clan.clanID == xcaliburHolderHistory[0].clanName)
        {
            // if player is in the same clan as the current clan holding the xcalibur
            if (Clan.clanID == clanWhenEventStarted)
            {
                return true;
            }
            // if not the same clan, or if player did not login at least once during the event period,
            // we check the timestamp the player joined the clan
            if (Clan.clanID != clanWhenEventStarted)
            {
                if (Clan.clanJoinedTimeStamp > startTimeLong)
                {
                    return true;
                }
            }
        }
        return false;
    }

    // Resets all Reward Redemptions ( Can be used when the Day passes )
    void SetRewardsRedemption()
    {
        ClearRewardsClaimedList();
        AddRewardToList();
    }
    void ClearRewardsClaimedList()
    {
        myObtainedRewards = new MyRewards();
        myObtainedClanRewards = new MyRewards();
    }
    void AddRewardToList()
    {
        myObtainedRewards.rewardsRedemption = currentActiveEventOBJ.RewardsCount();
        myObtainedClanRewards.rewardsRedemption = currentActiveEventOBJ.ClanRewardsCount();
    }

    // Updates the UI to reflect the lastes VALUES
    void DisplayEventName()
    {
        if (currentEventIsActive) eventName.color = fullTextColor;
        else eventName.color = fadedTextColor;

        eventName.text = CurrentActiveEventName(currentActiveEventOBJ.eventName);
    }
    void DisplayEventTime()
    {
        // This will be called in the Update
        timespanDisplay = TimeSpan.FromSeconds(timeToEventStatusChange);

        if (currentEventIsActive)
        {
            if (timeToEventStatusChange > 0)
                eventTimeCountdown.text = "Ends in : " + TimeLeftFormat(timespanDisplay.Days, timespanDisplay.Hours, timespanDisplay.Minutes);
            else if (timeToEventStatusChange <= 0)
                eventTimeCountdown.text = string.Format("Tabulating Scores . . .");
        }
        else if (!currentEventIsActive)
        {
            if (timeToEventStatusChange > 0)
                eventTimeCountdown.text = "Rewards expires in : " + TimeLeftFormat(timespanDisplay.Days, timespanDisplay.Hours, timespanDisplay.Minutes);
            else if (timeToEventStatusChange <= 0)
                eventTimeCountdown.text = "Commencing in : " + TimeLeftFormat(timespanDisplay.Days, timespanDisplay.Hours, timespanDisplay.Minutes);
        }
    }
    string TimeLeftFormat(int days, int hours, int mins)
    {
        string toReturn = "";

        if (days >= 1)
            toReturn += days + "d ";
        if (hours >= 1)
            toReturn += hours + "h ";
        if (mins >= 1)
            toReturn += mins + "m";

        if (days < 1 & hours < 1 & mins < 1)
            toReturn = "less than a min";

        return toReturn;
    }
    void DisplayEventCondition()
    {
        if (currentEventActivityStatus != EventLiveStatus.canceled)
            eventConditionText.text = currentActiveEventOBJ.eventCondition;
        else
            eventConditionText.text = "Event Canceled";

        eventConditionHeader.gameObject.SetActive(currentEventIsActive);
    }
    void DisplayPlayerPoints()
    {
        playerPointsAccumulatedText.text = string.Format("Points : {0:n0}", playerProgression);
    }
    void DisplayPlayClanPoints()
    {
        playerClanPointsAccumulatedText.text = string.Format("Clan Points : {0:n0}", playerClanProgression);
    }

    void DisplayEventCanceled()
    {
        eventTimeCountdown.text = "Event Canceled";
        startCounting = false;
    }

    // Updates all Redeemable Eligibilities

    void CheckForEligibleToClaimPlayerRewards()
    {
        for (int x = 0; x < redeemPlayerRewardButtons.Count; x++)
        {
            if (playerProgression >= redeemPlayerRewardButtons[x].rewardAvailability)
                redeemPlayerRewardButtons[x].SetMeInteractable(true);
            else
                redeemPlayerRewardButtons[x].SetMeInteractable(false);
        }
    }
    void CheckForEligibleToClaimClanRewards()
    {
        if (Clan.clanID != clanWhenEventStarted)
            return;

        for (int x = 0; x < redeemClanRewardButtons.Count; x++)
        {
            if (playerClanProgression >= redeemClanRewardButtons[x].rewardAvailability)
                redeemClanRewardButtons[x].SetMeInteractable(true);
            else
                redeemClanRewardButtons[x].SetMeInteractable(false);
        }
    }
    void CheckForPlayerRedeemedRewards()
    {
        for (int x = 0; x < currentActiveEventOBJ.playerEventRewards.Length; x++)
        {
            if (myObtainedRewards.rewardsRedemption[x] == 0)
                redeemPlayerRewardButtons[x].SetRedeemed(false);
            else
                redeemPlayerRewardButtons[x].SetRedeemed(true);
        }
    }
    void CheckForClanRedeemedRewards()
    {
        for (int x = 0; x < currentActiveEventOBJ.clanEventRewards.Length; x++)
        {
            if (myObtainedClanRewards.rewardsRedemption[x] == 0)
                redeemClanRewardButtons[x].SetRedeemed(false);
            else
                redeemClanRewardButtons[x].SetRedeemed(true);
        }
    }

    // ==== Get Player's Event info ==== \\
    void AttemptRetrievalEventProgression()
    {
        GameSparksManager.Instance.SaveEventProgress();
    }

    // ==== Event Reward Redemption buttons ==== \\
    void CreateRedeemableRewardsButton()
    {
        for (int x = 0; x < currentActiveEventOBJ.playerEventRewards.Length; x++)
        {
            CreatePlayerProgressionEventButtons(x);
        }
        for (int x = 0; x < currentActiveEventOBJ.clanEventRewards.Length; x++)
        {
            CreateClanProgressionEventButtons(x);
        }
    }
    void CreatePlayerProgressionEventButtons(int no)
    {
        GameObject rewardBtn = Instantiate(rewardButtonPrefab, playerScrollPanel.position, Quaternion.identity, playerScrollPanel);
        RewardButton newRwdBtn = rewardBtn.GetComponent<RewardButton>();

        // Inject info fomr the scriptable object into the Reward Button

        newRwdBtn.thisButtonRewards = currentActiveEventOBJ.playerEventRewards[no];
        newRwdBtn.DisplayAvailablility(0, currentActiveEventOBJ.rewardRequirement[no]);
        newRwdBtn.UpdateButtonEventRewardInfo();

        redeemPlayerRewardButtons.Add(newRwdBtn);
    }
    void CreateClanProgressionEventButtons(int no)
    {
        GameObject rewardBtn = Instantiate(rewardButtonPrefab, clanScrollPanel.position, Quaternion.identity, clanScrollPanel);
        RewardButton newRwdBtn = rewardBtn.GetComponent<RewardButton>();

        // Inject info fomr the scriptable object into the Reward Button

        newRwdBtn.thisButtonRewards = currentActiveEventOBJ.clanEventRewards[no];
        newRwdBtn.DisplayAvailablility(0, currentActiveEventOBJ.clanRewardRequirements[no]);
        newRwdBtn.UpdateButtonEventRewardInfo();

        redeemClanRewardButtons.Add(newRwdBtn);
    }

    // ==== Update Reward Redemption ==== \\
    public void AttemptRewardRedemption(RewardButton rewardButton)
    {
        // Set the value in the array to "RETRIEVED" first so it is prevent from being tapped multiple times
        SetRedeemedValue(rewardButton, true);
        // Display the Redeemed Blocker
        CheckForPlayerRedeemedRewards();
        // Attempt to update the Cloud
        GameSparksManager.Instance.SaveEventProgress(rewardButton);
    }
    void SetRedeemedValue(RewardButton rewardButton, bool isRetrieved)
    {
        // isRetrieved is used to Toggle Interactable depending on the Response of Redeem Reward Event

        // SETTING THE PLAYER's OWN EVENTS REWARDS BUTTON TO REDEEMED STATE
        for (int x = 0; x < redeemPlayerRewardButtons.Count; x++)
        {
            if (rewardButton == redeemPlayerRewardButtons[x])
            {
                myObtainedRewards.rewardsRedemption[x] = isRetrieved ? 1 : 2;
                return;
            }
        }
        // SETTING THE PLAYER's CLAN EVENTS REWARDS BUTTON TO REDEEMED STATE
        for (int x = 0; x < redeemClanRewardButtons.Count; x++)
        {
            if (rewardButton == redeemClanRewardButtons[x])
            {
                myObtainedClanRewards.rewardsRedemption[x] = isRetrieved ? 1 : 2;
                return;
            }
        }
    }
    public void RedemptionOfRewardFailure(RewardButton rewardButton, string errorMessage)
    {
        // Un-set the Claimed value in the array
        SetRedeemedValue(rewardButton, false);
        // Hide the REDEEMed popup blocker
        CheckForPlayerRedeemedRewards();
        // Show the Error
        DisplayInfoPopup(errorMessage);
    }

    // ==== Event Accumulation ==== \\
    public void PendingContributionPointAdder(bool toAdd, string toCheckEvent_, int multipler)
    {
        toCheckEvent = toCheckEvent_;
        toAddEventPoint = toAdd;
        pointsToAdd = multipler;
    }
    void EventProgressPointRetrieve()
    {
       AttemptEventProgressPointRetrieval();
    }
    void CheckForPendingPoints()
    { 
        // IF there is a pending event to add
        if (toAddEventPoint)
        {
            AddContributionPoints(toCheckEvent);
            SetNonPendingUpdatePoints();
        } 
    }
    void SetNonPendingUpdatePoints()
    {
        toCheckEvent = "none_To_Update";
        toAddEventPoint = false;
    }
    public void AddContributionPoints(string condition)
    {
        // If the current event is not active or if Event status is canceled
        if (!currentEventIsActive || currentEventActivityStatus == EventLiveStatus.canceled)
        {
            AttemptEventProgressPointRetrieval();
            return;
        }
        if (currentActiveEventOBJ.eventCondition.ToLower().Contains(condition))
        {
            GameSparksManager.Instance.SaveEventProgress();
            if (Clan.clanID == clanWhenEventStarted)
            {
                GameSparksManager.Instance.SaveClanEventProgress();
                ClanManager.Instance.UpdatingClanMemberInfo(true);
                GameSparksManager.Instance.UpdatePlayerClanContribution(pointsToAdd);
            }
        }
    }

    // ==== EVENT Responses Display ==== \\

        // Large popup
    public void DisplayInfoPopup(string message)
    {
        SetMessage(message);
        ToggleResponsePanel(true);
    }
    void SetMessage(string message)
    {
        responseHandlerText.text = message;
    }
    void ToggleResponsePanel(bool isActive)
    {
        popupPanel.SetActive(isActive);
    }

    // ===== MISC CALCULATIONS ===== \\
    float DifferenceTillEventStatucChange()
    {
        // THIS FUNCTION WILL PERFORM ONLY ONCE, IT IS USED TO GET THE FLOAT DIFFERENCE TO THE NEXT EVENT STATUS UPDATE
        DateTime now = TimeMaster.loginTime;

        int hrDiff = now.Hour;
        int minDiff = now.Minute;
        int secDiff = now.Second;
        DateTime timeTo = new DateTime();

        if (currentEventIsActive)
        {
            timeTo = now.AddDays(7 - currentEventRunningDay).AddHours(-hrDiff).AddMinutes(-minDiff).AddSeconds(-secDiff);
            return (float)timeTo.Subtract(now).TotalSeconds;
        }
        timeTo = now.AddDays(5 - currentEventRunningDay).AddHours(-hrDiff).AddMinutes(-minDiff).AddSeconds(-secDiff);
        return (float)timeTo.Subtract(now).TotalSeconds;
    }
}

