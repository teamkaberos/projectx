﻿using UnityEngine.UI;
using UnityEngine;

public class InvokeInteractable : MonoBehaviour
{
    Button tapToClose;

    void Awake()
    {
        tapToClose = GetComponent<Button>();
        tapToClose.interactable = false;
    }

    void OnEnable()
    {
        Invoke("TriggerInteractable", 0.5f);
    }

    void TriggerInteractable()
    {
        tapToClose.interactable = true;
    }

    void OnDisable()
    {
        tapToClose.interactable = false;
    }
}
