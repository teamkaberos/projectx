﻿using UnityEngine;
using UnityEngine.UI;

public class VolumeSet : Settings 
{

    public Slider volumeSlider;
    public Button volumeUp;
    public Button volumeDown;

    public Button soundOn;
    public Button soundOff;

	// Use this for initialization
	public override void Start () 
    {
        volumeUp.onClick.AddListener(Increase_Volume);
        volumeDown.onClick.AddListener(Decrease_Volume);

        base.Start();

        Volume_Slider(volumeLevel);
	}


    void Increase_Volume()
    {
        volumeLevel += 0.1f;
        Volume_Slider(volumeLevel);
    }
    void Decrease_Volume()
    {
        volumeLevel -= 0.1f;
        Volume_Slider(volumeLevel);
    }


    void Volume_Slider(float newVolumeValue)
    {
        Cap_Volume();
        SetVolumeLevel(volumeLevel);
        volumeSlider.value = newVolumeValue;
    }

    void Cap_Volume()
    {
        if (volumeLevel < 0)
            volumeLevel = 0;
        if (volumeLevel > 1)
            volumeLevel = 1;
    }
}
