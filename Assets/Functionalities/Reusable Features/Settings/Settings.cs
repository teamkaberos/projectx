﻿using UnityEngine;

public class Settings : MonoBehaviour 
{
    [Range(0, 1)]
    public float volumeLevel = 1;

    public virtual void Start()
    {
        Get_Sound_Setting();
        SetVolumeLevel(volumeLevel);
    }


    void Get_Sound_Setting()
    {
        if (PlayerPrefs.HasKey("Volume_Setting"))
            volumeLevel = PlayerPrefs.GetFloat("Volume_Setting");
    }

    public void SetVolumeLevel(float volume_Level)
    {
        volumeLevel = volume_Level;
        AudioListener.volume = volumeLevel;
        SavePref();
    }


    void SavePref()
    {
        PlayerPrefs.SetFloat("Volume_Setting", volumeLevel);
    }

}
