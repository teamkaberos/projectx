﻿using UnityEngine;

public class TriggerPlayFX : MonoBehaviour
{
    public FXPlay[] fxToBePlayed;

  
    public void PlayFXNumber(int fxQueueNo)
    {
        fxToBePlayed[fxQueueNo].PlayEffect();
    }
    public void StopFXNumber(int fxQueueNo)
    {
        fxToBePlayed[fxQueueNo].StopEffect();
    }
}
