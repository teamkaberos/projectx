﻿using System.Collections;
using UnityEngine;
using System;

public class ConnectionChecker : MonoBehaviour 
{
	#region Singleton
	private static ConnectionChecker _instance;
	public static ConnectionChecker Instance
	{
		get
		{
			if (_instance = null)
			{
				GameObject go = new GameObject ("ConnectionChecker");
				go.AddComponent<ConnectionChecker> ();
			}
			return _instance;
		}
	}
	#endregion


}
