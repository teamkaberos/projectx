﻿using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class SendMessage : MonoBehaviour 
{
    [Header("redteam")]
    public string rusername;
    public string rpassword;

    public Button rloginButton;

    public TMP_InputField r_pid;
    public TMP_InputField r_plname;

    public Button rSendScriptMsg;

    public TMP_InputField rMessage;


    [Header("blueteam")]
    public string busername;
    public string bpassword;

    public Button bloginButton;

    public TMP_InputField b_pid;
    public TMP_InputField b_plname;

    public Button bSendScriptMsg;

    public TMP_InputField bMessage;




    void Start()
    {
        rloginButton.onClick.AddListener(RLogin);
        bloginButton.onClick.AddListener(BLogin);


        //rSendScriptMsg.onClick.AddListener(RSendMsg);
        //bSendScriptMsg.onClick.AddListener(BSendMsg);
    }

    public void RLogin()
    {
        GameSparksManager.Instance.ForTestGSLogin2(rusername, rpassword, "red", this);
    }
    public void BLogin()
    {
        GameSparksManager.Instance.ForTestGSLogin2(busername, bpassword, "blue", this);
    }

    public void FillInInfo(string id, string username, string color)
    {
        if (color == "red")
        {
            r_pid.text = id;
            r_plname.text = username;
        }

        if (color == "blue")
        {
            b_pid.text = id;
            b_plname.text = username;
        }
    }


    //void RSendMsg()
    //{
    //    GameSparksManager.Instance.PlayerSieged(b_pid.text);
    //}
    //void BSendMsg()
    //{
    //    GameSparksManager.Instance.PlayerSieged(r_pid.text);
    //}
}
