﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class MissionHolder : MonoBehaviour
{
    #region singleton
    static MissionHolder _instance;
    public static MissionHolder Instance
    {
        get
        {
            return _instance;
        }
    }
    #endregion

    public static bool isInit;

    public int[] missionsAssigned = new int[3];
    public int[] previousMissionAssigned = new int[3];
    public int[] missionProgress = new int[3];


    public List<int> missionInt = new List<int>();
    [HideInInspector]
    public int missionAmount = 3;

    public ScriptableMission missionZero;
    public ScriptableMission[] missionList;
    [Space(10)]

    public GameObject missionButtonToInstantiate;
    public Transform dailyMissionTransform;
    public MissionButton[] missionButtons;

    int missionThisRank;                                //used for calculating how many missions of this difficulty there are

    public GameObject missionNotificationRedOBJ;
    public Text missionNotificationText;

    [HideInInspector]
    public MissionButton speedUpMissionSlotSelected;    //to store that mission the player last selected
    public GameObject notification;
    public Button notificationConfirm;                  // in other words, the OK button for REFRESH MISSION
    public Text notificationText;


    int missionIntToReturn;                             //the int to return to the mission Int list, so the mission can be accessed again
    public static int speedUpCost;

    bool beginCountdown;



    void Awake()
    {
        _instance = this;
    }
    void Start()
    {
        for (int x = 0; x < missionButtons.Length; x++)
        {
            missionButtons[x].speedUpCostChanged += SpeedUpNotify;
            missionButtons[x].countdownCompleted += RemoveCleared;
            missionButtons[x].missionCompleted += MissionRewardAvailable;
        }
    }

    void OnDestroy()
    {
        for (int x = 0; x < missionButtons.Length; x++)
        {
            missionButtons[x].speedUpCostChanged -= SpeedUpNotify;
            missionButtons[x].countdownCompleted -= RemoveCleared;
            missionButtons[x].missionCompleted -= MissionRewardAvailable;
        }
    }
    public void LogoutReset()
    {
        isInit = false;
    }

    void Update()
    {
        if (beginCountdown)
        {
            for (int x = 0; x < missionAmount; x++)
            {
                if (missionButtons[x].scriptableMission != null)
                {
                    if (missionButtons[x].scriptableMission.missionState >= 1)
                    {
                        if (missionButtons[x].currentCooldownValue > 0)
                        {
                            missionButtons[x].currentCooldownValue -= Time.deltaTime;
                            if (Mathf.FloorToInt(missionButtons[x].currentCooldownValue) % 180f == 0)
                            {
                                missionButtons[x].CostChanged();
                            }

                            missionButtons[x].timeToNextMission.text = "Refreshes in " + missionButtons[x].TimeConverter(missionButtons[x].currentCooldownValue);
                            if (missionButtons[x].currentCooldownValue <= 0)
                            {
                                missionButtons[x].currentCooldownValue = -100;
                                missionButtons[x].CooldownCompleted();
                            }
                        }
                    }
                }
            }
        }
    }

    public void InitMissionsInt()
    {
        missionInt.Clear();
        for (int x = 1; x < missionList.Length; x++)
        {
            missionInt.Add(x);
            missionList[x].ResetMissions();
        }
    }

    public void DelegateMission(int _missionSlot, int _missionNo, int _missionProgress, int _missionState, float rfduration, bool init)
    {
        //Assign missions from the Player script
        missionsAssigned[_missionSlot] = _missionNo;
        if (_missionNo > missionList.Length - 1)
            missionsAssigned[_missionSlot] = 0;
        if (_missionNo != 0)
        {
            //Assign the cooldowns from the server
            missionButtons[_missionSlot].currentCooldownValue = rfduration;

            if (_missionNo == 1)

            // cap the value of the refresh duration
            if (missionButtons[_missionSlot].currentCooldownValue > missionButtons[_missionSlot].cooldownDuration)
                missionButtons[_missionSlot].currentCooldownValue = missionButtons[_missionSlot].cooldownDuration;

            //and check the remaining duration of the quest
            if (rfduration > 0)
                missionButtons[_missionSlot].Display_Cooldown_Stuff(true);
            else if (rfduration <= 0 && rfduration > -100)
                missionButtons[_missionSlot].Display_Cooldown_Stuff(false);

            for (int y = 0; y < missionInt.Count; y++)
            {
                if (_missionNo == missionInt[y])
                {
                    missionInt.RemoveAt(y);
                    break;
                }
            }
        }
        else
            GenerateMission(_missionSlot);

        for (int x = 0; x < missionList.Length; x++)
        {
            if (_missionNo == missionList[x].missionNumber)
            {
                missionList[x].missionProgress = _missionProgress;
                missionList[x].missionState = _missionState;
            }
        }

        missionProgress[_missionSlot] = _missionProgress;
    }

    void RemoveCleared()
    {
        //track how many mission are available for refreshing
        int tempClearedValue = 0;
        for (int x = 0; x < missionAmount; x++)
        {
            if (missionButtons[x].scriptableMission == null)
                return;
            //this is the condition for a mission to be refreshed
            if (missionButtons[x].scriptableMission.missionState == 1)
            {
                if (missionButtons[x].currentCooldownValue < 0)// && missionButtons[x].currentCooldownValue != -100.0f)
                {
                    tempClearedValue += 1;
                    ClearUnassignedMissions(missionButtons[x].scriptableMission.missionNumber);
                    missionsAssigned[x] = 0;
                    missionButtons[x].currentCooldownValue = -100;
                    missionIntToReturn = missionButtons[x].scriptableMission.missionNumber;

                    Player.playerMission[x] = 0;
                    Player.playerMissionProgress[x] = 0;
                    Player.playerMissionState[x] = 0;
                    Player.playerMissionRefreshTime[x] = 0;
                }
            }
        }
        AssignMissionToButtons();
        DisplayNewMission(tempClearedValue);
    }

    public void AssignMissionToButtons()
    {
        for (int x = 0; x < missionAmount; x++)
        {
            if (missionsAssigned[x] == 0)
            {
                int y = GenerateMission(x);
                missionButtons[x].scriptableMission = GetScriptable(y);
                missionsAssigned[x] = y;
                missionButtons[x].AssignValue();
                ResetClearedMission();
                ReturnClearedMission();
            }
            else
            {
                missionButtons[x].scriptableMission = GetScriptable(missionsAssigned[x]);
                missionButtons[x].AssignValue();
            }
        }
    }

    public void CheckIfWaitTimeHasCompleted(long timePassed)
    {
        for (int x = 0; x < missionAmount; x++)
        {
            if (missionButtons[x].scriptableMission != null)
            {
                if (missionButtons[x].scriptableMission.missionState == 1)
                    missionButtons[x].currentCooldownValue -= timePassed;
            }
        }
        RemoveCleared();
    }

    void DisplayNewMission(int newMissions)
    {
        missionNotificationText.text = newMissions.ToString();
        if (newMissions >= 1)
            missionNotificationRedOBJ.SetActive(true);
        else
            missionNotificationRedOBJ.SetActive(false);
    }
    void MissionRewardAvailable()
    {
        missionNotificationText.text = "!";
        missionNotificationRedOBJ.SetActive(true);
    }

    void ClearUnassignedMissions(int missionToClear)
    {
        for (int y = 0; y < missionList.Length; y++)
        {
            if (missionToClear == missionList[y].missionNumber)
            {
                missionList[y].missionProgress = 0;
                missionList[y].missionState = 0;
            }
        }
    }

    public void FillInTimeDifference()
    {
        if (!isInit)
        {
            isInit = true;

            long timeDifference = TimeMaster.Instance.CalculateTimeDifference(DataManager.initialLoginServerTime, DataManager.lastLogout);

            CheckIfWaitTimeHasCompleted(timeDifference);
        }
    }

    int GenerateMission(int slotNo)
    {
        missionThisRank = 0;

        //find how many missions are there of this rank
        for (int x = 0; x < missionInt.Count; x++)
        {
            if (missionList[missionInt[x]].missionDifficulty == slotNo)
            {
                missionThisRank += 1;
            }
        }

        int missionRand = Random.Range(1,missionThisRank+1);      // the queue number of the mission to return
        int missionToReturn = 100;//create an int to return

        for (int y = 0; y < missionInt.Count; y++)
        {
            if (missionList[missionInt[y]].missionDifficulty == slotNo)
            {
                if (missionThisRank == missionRand)
                {
                    missionToReturn = missionInt[y];
                    missionInt.RemoveAt(y);
                    break;
                }
                missionThisRank -= 1;
            }
        }
        Player.playerMission[slotNo] = missionToReturn;
        Player.playerMissionProgress[slotNo] = 0;

        return missionToReturn;
    }

    ScriptableMission GetScriptable(int scriptableNumber)
    {
        for (int x = 0; x < missionList.Length; x++)
        {
            if (missionList[x].missionNumber == scriptableNumber)
            {
                return missionList[x];
            }
        }
        return missionList[0];
    }
  
    void ReturnClearedMission()
    {
        for (int x = 0; x < missionInt.Count; x++)
        {
            if (missionInt[x] != missionIntToReturn)
            {
                if (x <= 0 & missionIntToReturn > 0)
                {
                    missionInt.Add(missionIntToReturn);
                }
                continue;
            }
        }
    }
    void ResetClearedMission()
    {
        for (int x = 0; x < missionList.Length; x++)
        {
            if (missionList[x].missionNumber == missionIntToReturn)
            {
                missionList[x].ResetMissions();
            }
        }
    }

    public void BeginMissionCountdown()
    {
        beginCountdown = true;
    }



    // === UPDATE THE MISSION AFTER BATTLE === \\
   public void UpdateMission(string objectToUpdate, int valueToUpdate)
    {
        // find the scriptable object to update
        for (int x = 0; x < missionList.Length; x++)
        {
            //find the mission numbers that are assigned to the player
            for (int y = 0; y < missionsAssigned.Length; y++)
            {
                //and check if these numbers match
                if (missionList[x].missionNumber == missionsAssigned[y])
                {
                    //if this mission is not completed
                    if (missionList[x].missionState == 0)
                    {
                        //when the correct mission is found
                        if (missionList[x].missionName.ToLower().Contains(objectToUpdate))
                        {
                            missionList[x].missionProgress += valueToUpdate;
                            Player.playerMissionProgress[y] += valueToUpdate;
                            DataManager.missionProgresses[y] += valueToUpdate;
                        }
                    }
                }
            }
        }
    }
   public void UpdateMissionValues()
    {
        for (int x = 0; x < missionAmount; x++)
        {
            missionButtons[x].AssignValue();
        }
    }


    public void NotifyPopup()
    {
        notification.SetActive(true);
        notificationConfirm.onClick.RemoveAllListeners();
    }

    public void SpeedUpNotify(float timeLeft, MissionButton button)
    {
        if (button == speedUpMissionSlotSelected)
        {
            speedUpCost = Mathf.CeilToInt(timeLeft / 180f);
            notificationText.text = string.Format("Refresh MISSION at {0} Diamond", speedUpCost);
            notificationConfirm.onClick.AddListener(SpeedUpMission);
            notificationConfirm.onClick.AddListener(speedUpMissionSlotSelected.GetComponent<MissionButton>().MissionRefreshed);
        }
        Check_If_Player_Can_Speed_Up();
    }

    void Check_If_Player_Can_Speed_Up()
    {
        if (Player.diamond < speedUpCost)
            notificationConfirm.interactable = false;
        else
            notificationConfirm.interactable = true;
    }

    void SpeedUpMission()
    {
        Player.Instance.SpendDiamond(Mathf.CeilToInt(speedUpMissionSlotSelected.currentCooldownValue / 180f));
        speedUpMissionSlotSelected.currentCooldownValue = 0.2f;
        DataManager.Instance.PrepareDataForSparksPushing();
        GameSparksManager.Instance.SaveAllMissions();
    }

    public void RemoveAssignedMission()
    {
        for (int x = 0; x < missionsAssigned.Length; x++)
        {
            missionsAssigned[x] = 0;
            missionProgress[x] = 0;
            missionButtons[x].ResetMission();
        }
    }
    public void ResetAllMissions()
    {
        for (int x = 0; x < missionList.Length; x++)
        {
            missionList[x].missionProgress = 0;
            missionList[x].missionState = 0;
        }
    }

}
