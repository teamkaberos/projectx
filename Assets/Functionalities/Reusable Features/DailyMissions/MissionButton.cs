﻿using UnityEngine;
using UnityEngine.UI;

public class MissionButton : MonoBehaviour
{
    public delegate void timeUpdate(float time, MissionButton thisButton);
    public event timeUpdate speedUpCostChanged;

    public delegate void cooldownComplete();
    public event cooldownComplete countdownCompleted;

    public delegate void MissionComplete();
    public event MissionComplete missionCompleted;

    public ScriptableMission scriptableMission;

    public float cooldownDuration;
    public float currentCooldownValue;
    public string missionDifficulty;

    int requiredAmount;
    int rewardValue;

    public Text missionName;
    public Text missionRequirements;
    public Text missionProgressText;
    public Text missionReward;

    public Text claimedOrAbandonedText;

    public Text timeToNextMission;

    public Image missionImage;
    public Image missionRewardImage;

    public Button claimReward;
    public Button dropMission;
    public Button refreshMission;

    public GameObject claimedPopup;

    public RectTransform originalSizeBar;
    public RectTransform visualProgressBar;

    void Start()
    {
        claimReward.onClick.AddListener(GetReward);
        dropMission.onClick.AddListener(DropMission);
        refreshMission.onClick.AddListener(RefreshMission);
    }

    public void CostChanged()
    {
        speedUpCostChanged(currentCooldownValue, this);
    }
    public void CooldownCompleted()
    {
        countdownCompleted();
    }

    public string TimeConverter(float timeLeft)
    {
        int hours = Mathf.FloorToInt(timeLeft / 3600f);
        int mins = Mathf.FloorToInt((timeLeft - (hours * 3600)) / 60f);
        if (mins >= 60)
        {
            mins = 0;
            hours += 1;
        }
        int seconds = Mathf.CeilToInt((timeLeft - ((hours * 3600) + (mins * 60))));
        if (seconds >= 60)
        {
            seconds = 0;
            mins += 1;
            if (mins >= 60)
            {
                mins = 0;
                hours += 1;
            }
        }

        return string.Format("{0:00}:{1:00}:{2:00}", hours, mins, seconds);
    }

    public void AssignValue()
    {
        missionName.text = scriptableMission.missionName;
        MissionText(scriptableMission.missionNumber);
        missionImage.sprite = scriptableMission.missionImage;
        missionImage.preserveAspect = true;

        missionRewardImage.sprite = scriptableMission.missionRewardImage;
        missionRewardImage.preserveAspect = true;

        CheckIfRewardClaimed();
    }

    void MissionText(int missionNo)
    {
        switch (missionNo)
        {
            case 0://empty ( awaiting for next)
                break;
            case 1://raid gold
                requiredAmount = 10000; //(int)(0.5f * Player.treasury_Storage_Cap);
                rewardValue = 8; //+ (2 * Player.treasuryLvl);
                CheckMissionProgress();
                break;
            case 2://get rep
                requiredAmount = 15; //(Player.castleLevel * 3) + 4;
                rewardValue = 10000; // (int)(Player.treasury_Storage_Cap * 0.2f);
                CheckMissionProgress();
                break;
            case 3://get excalipoints
                requiredAmount = 30; // (Player.recruitmentHall_Lvl * 4) + 12;
                rewardValue = 12;  // Player.recruitmentHall_Lvl * 3;
                CheckMissionProgress();
                break;
            case 4://raid gold 2
                requiredAmount = 15000; // (int)(0.85f * Player.treasury_Storage_Cap);
                rewardValue = 12; // + (3 * Player.treasuryLvl);
                CheckMissionProgress();
                break;
            case 5:
                /* OLD      //use mages
                requiredAmount = 3; //Player.castleLevel * 2;
                rewardValue = 1;  //Mathf.FloorToInt((Player.castleLevel + 1) / 2);
                CheckMissionProgress();
                break;
                */
                requiredAmount = 1;
                rewardValue = 5;
                CheckMissionProgress();
                break;
            case 6:
                /* OLD      //use  knights
                requiredAmount = 3; // Player.castleLevel * 2;
                rewardValue = 1;  //Mathf.FloorToInt((Player.castleLevel + 1) / 2);
                CheckMissionProgress();
                break;
                */
                requiredAmount = 2;
                rewardValue = 10;
                CheckMissionProgress();
                break;
            case 7:
                /* OLD      //use ranges
                requiredAmount = 3;  //Player.castleLevel * 2;
                rewardValue = 1;  //Mathf.FloorToInt((Player.castleLevel + 1) / 2);
                CheckMissionProgress();
                break;
                */
                requiredAmount = 3;
                rewardValue = 15;
                CheckMissionProgress();
                break;
            case 8:
                /* OLD //mage 3
                requiredAmount = 5; // Player.castleLevel * 4;
                rewardValue = 3;  //Mathf.FloorToInt((Player.castleLevel + 1) / 2) + 2;
                CheckMissionProgress();
                break;
                */
                requiredAmount = 1;
                rewardValue = 8;
                CheckMissionProgress();
                break;
            case 9:// raid gold 3
                requiredAmount = 20000;  // (int)(1.15f * Player.treasury_Storage_Cap);
                rewardValue = 16; // (4 * Player.treasuryLvl);
                CheckMissionProgress();
                break;
            case 10://get rep 2
                requiredAmount = 18; // (Player.castleLevel * 5) + 8;
                rewardValue = 15000;  //(int)(Player.treasury_Storage_Cap * 0.35f);
                CheckMissionProgress();
                break;
            case 11://get excalipoints 2
                requiredAmount = 40; // (Player.recruitmentHall_Lvl * 5) + 16;
                rewardValue = 18;  // Player.recruitmentHall_Lvl * 3;
                CheckMissionProgress();
                break;
                /*  //removed missions
            case 12://knight and mage
                requiredAmount = Player.castleLevel * 2;
                rewardValue = Player.castleLevel;
                CheckMissionProgress();
                break;
            case 13://archer and knight
                requiredAmount = Player.castleLevel * 2;
                rewardValue = Player.castleLevel;
                CheckMissionProgress();
                break;
            case 14://mage and archer
                requiredAmount = Player.castleLevel * 2;
                rewardValue = Player.castleLevel;
                CheckMissionProgress();
                break;
                */
            case 12:
                /* OLD      //mage 2
                requiredAmount = 4; // Player.castleLevel * 3;
                rewardValue = 2;  //Mathf.FloorToInt((Player.castleLevel + 1) / 2) + 1;
                CheckMissionProgress();
                break;
                */
                requiredAmount = 2;
                rewardValue = 16;
                CheckMissionProgress();
                break;
            case 13:
                /* OLD          //melee 2
                requiredAmount = 4;  // Player.castleLevel * 3;
                rewardValue = 2;  //Mathf.FloorToInt((Player.castleLevel + 1) / 2) + 1;
                CheckMissionProgress();
                break;
                */
                requiredAmount = 3;
                rewardValue = 24;
                CheckMissionProgress();
                break;
                // UNUSED MISSIONS NOW
                /*
            case 14://archer 2
                requiredAmount = 4; // Player.castleLevel * 3;
                rewardValue = 2;  //Mathf.FloorToInt((Player.castleLevel + 1) / 2) + 1;
                CheckMissionProgress();
                break;
            case 15://melee 3
                requiredAmount = 5;  // Player.castleLevel * 4;
                rewardValue = 3;  // Mathf.FloorToInt((Player.castleLevel + 1) / 2) + 2;
                CheckMissionProgress();
                break;
            case 16://archer 3
                requiredAmount = 5;  //Player.castleLevel * 4;
                rewardValue = 3; // Mathf.FloorToInt((Player.castleLevel + 1) / 2) + 2;
                CheckMissionProgress();
                break;
                */
        }
    }

    void CheckMissionProgress()
    {
        missionProgressText.text = string.Format("{0:n0} / {1:n0}", scriptableMission.missionProgress, requiredAmount);
        UpdateVisualDisplay();
        if (scriptableMission.missionProgress >= requiredAmount)
        {
             missionProgressText.text = "COMPLETED";
            if (scriptableMission.missionState == 0)
            {
                claimReward.interactable = true;
                missionCompleted();
            }
            else
                claimReward.interactable = false;
        }
        missionRequirements.text = scriptableMission.missionObjective.Replace("|x|", string.Format("{0:n0}",requiredAmount));
        missionReward.text = string.Format("{0:n0}", rewardValue);
    }


    public void Display_Cooldown_Stuff(bool display)
    {
        refreshMission.gameObject.SetActive(display);
    }


    void CheckIfRewardClaimed()
    {
        if (scriptableMission.missionState == 1)
        {
            if (scriptableMission.missionProgress < requiredAmount)
                claimedOrAbandonedText.text = "- ABANDONED -";
            else
                claimedOrAbandonedText.text = "- CLAIMED -";
            claimedPopup.SetActive(true);
        }
        else
        {
            MissionRefreshed();
            claimedPopup.SetActive(false);
        }
    }
    void UpdateVisualDisplay()
    {
        float current = scriptableMission.missionProgress;
        float needed = requiredAmount;

        float perc = current / needed * 100;
        if (perc >= 100)
            perc = 100;

        visualProgressBar.sizeDelta = new Vector2((originalSizeBar.sizeDelta.x * perc) / 100f, originalSizeBar.sizeDelta.y);
    }


    void GetReward()
    {
        if (scriptableMission.rewardType.ToLower().Contains("gold"))
            Player.Instance.AddGold(rewardValue);
        else if (scriptableMission.rewardType.ToLower().Contains("reputation"))
            Player.Instance.AlterRepValue(true, rewardValue);
        else if (scriptableMission.rewardType.ToLower().Contains("squarestone"))
            Player.squareStone += rewardValue;
        else if (scriptableMission.rewardType.ToLower().Contains("circlestone"))
            Player.circleStone += rewardValue;
        else if (scriptableMission.rewardType.ToLower().Contains("trianglestone"))
            Player.triangleStone += rewardValue;
        else if (scriptableMission.rewardType.ToLower().Contains("excalipoints"))
            Player.excalipoints += rewardValue;
        else if (scriptableMission.rewardType.ToLower().Contains("diamond"))
            Player.Instance.GainDiamond(rewardValue);


        scriptableMission.missionState = 1;
        //timeToNextMission.gameObject.SetActive(true);
        claimedOrAbandonedText.text = "- CLAIMED -";
        claimedPopup.SetActive(true);
        claimReward.interactable = false;
        refreshMission.gameObject.SetActive(true);

        currentCooldownValue = cooldownDuration;
        Player.playerMissionProgress[int.Parse(missionDifficulty) - 1] = 1;
        Player.playerMissionState[int.Parse(missionDifficulty) - 1] = 1;
        Player.playerMissionRefreshTime[int.Parse(missionDifficulty) - 1] = 1;

        UIManager.Instance.Update_All();
        
        DataManager.Instance.PrepareDataForSparksPushing();
        GameSparksManager.Instance.SavePlayerExpendables(GameSparksManager.playerId);
        GameSparksManager.Instance.SaveInventoryStones();
        GameSparksManager.Instance.SavePlayerData(GameSparksManager.playerId);
        GameSparksManager.Instance.SaveAllMissions();
    }
    void DropMission()
    {
        scriptableMission.missionState = 1;
        //timeToNextMission.gameObject.SetActive(true);
        claimedOrAbandonedText.text = "- ABANDONED -";
        claimedPopup.SetActive(true);
        claimReward.interactable = false;
        refreshMission.gameObject.SetActive(true);

        currentCooldownValue = cooldownDuration;

        DataManager.Instance.PrepareDataForSparksPushing();
        GameSparksManager.Instance.SaveAllMissions();
    }
    void RefreshMission()
    {
        MissionHolder.Instance.speedUpMissionSlotSelected = this;

        MissionHolder.Instance.NotifyPopup();

        speedUpCostChanged(currentCooldownValue,this);
    }
    public void MissionRefreshed()
    {
        refreshMission.gameObject.SetActive(false);
        claimedPopup.SetActive(false);
    }



    public void ResetMission()
    {
        scriptableMission = null;
    }
}
