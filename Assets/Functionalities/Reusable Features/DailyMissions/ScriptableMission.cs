﻿using UnityEngine;

[CreateAssetMenu(fileName = "Mission", menuName = "Mission", order = 1)]
public class ScriptableMission : ScriptableObject
{
    [Range(1, 200)]
    public int missionNumber;
    [Range(0, 2)]
    public int missionDifficulty;
    //to be implemented for different timings of missions


    public string missionName;
    [TextArea(3,10)]
    public string missionObjective;

    public int missionProgress;

    public string missionReward;

    public int missionState;
    //0 = clearing in progress
    //1 = completed
    //2 = abandoned
    //if 1, save time left to next mission

    public string rewardType;

    public Sprite missionImage;
    public Sprite missionRewardImage;

    public void ResetMissions()
    {
        missionProgress = 0;
    }
}

