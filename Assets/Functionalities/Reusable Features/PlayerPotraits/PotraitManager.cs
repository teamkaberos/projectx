﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PotraitManager : MonoBehaviour
{
    public delegate void PortraitChanged(string portraitID);
    public static event PortraitChanged PortraitHasChanged;

#region singleton
    static PotraitManager _instance;
    public static PotraitManager Instance
    {
        get
        {
            return _instance;
        }
    }
#endregion
    
    [Header("CLAN")]
    // ==== FOR CLAN ==== \\
    public Image clanMemberImage;

    [System.Serializable]
    public class PotraitControl
    {
        public string potraitID;
        public Button potraitButton;
		public Sprite potraitImage;
    }
    [Space(15)]
    [Header("PORTRAIT LIST")]
	public List<PotraitControl> PotraitsHolder = new List<PotraitControl>();

    [Space(15)]
    [Header("HOME SCENE AVATAR")]
    public Image homeSceneAvatarDisplay;



    void Awake()
    {
        _instance = this;
    }

	public void Assign_Player_Potrait()
	{
        for (int x = 0; x < PotraitsHolder.Count; x++)
		{
            if (Player.potraitID == PotraitsHolder[x].potraitID)
			{
				UIManager.Instance.playerAvatar.sprite = PotraitsHolder[x].potraitImage;
                homeSceneAvatarDisplay.sprite = PotraitsHolder[x].potraitImage;
                DataManager.portrait = PotraitsHolder[x].potraitID;
                GameSparksManager.Instance.SavePlayerDetails();
				break;
			}
        }
	}

    public Sprite PortraitSprite(string potraitID)
    {
        switch (potraitID)
        {
            case "h0001":
                return PotraitsHolder[0].potraitImage;
            case "h0002":
                return PotraitsHolder[1].potraitImage;
            case "h0003":
                return PotraitsHolder[2].potraitImage;
            case "h0004":
                return PotraitsHolder[3].potraitImage;
            case "h0005":
                return PotraitsHolder[4].potraitImage;
            case "h0006":
                return PotraitsHolder[5].potraitImage;
            case "h0007":
                return PotraitsHolder[6].potraitImage;
            case "h0008":
                return PotraitsHolder[7].potraitImage;
            default :
                return PotraitsHolder[0].potraitImage;
        }
    }

    public void ClanMemberAvatarDisplay(string avatarID)
    {
        clanMemberImage.sprite = PortraitSprite(avatarID);
    }
}
