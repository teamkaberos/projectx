﻿using UnityEngine;
using UnityEngine.UI;

public class PotraitAssign : MonoBehaviour 
{
    public Potrait potrait;

    public Text potraitName;
    public Image potraitImage;

    public string potraitID;

	// Use this for initialization
	void Awake ()
    {
        potraitName.text = potrait.name;
        potraitImage.sprite = potrait.artwork;

        potraitID = potrait.portraitID;
	}

    public void AssignToPlayer()
    {
        //store ID for player potrait
        Player.Instance.AssignPotraitID(potraitID);

        //change the image
        UIManager.Instance.playerAvatar.sprite = potraitImage.sprite;
        PotraitManager.Instance.homeSceneAvatarDisplay.sprite = potraitImage.sprite;

        DataManager.portrait = potraitID;
        GameSparksManager.Instance.SavePlayerDetails();
    }
}
