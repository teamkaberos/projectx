﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu (fileName = "New Potrait", menuName = "Potrait")]
public class Potrait : ScriptableObject 
{
    public new string name;
    public new string portraitID;

    public Sprite artwork;
}
