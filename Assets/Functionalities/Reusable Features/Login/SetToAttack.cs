﻿using UnityEngine;

public class SetToAttack : MonoBehaviour
{
    Animator anim;

    void Start()
    {
        anim = GetComponent<Animator>();
        StartTraining();
    }


    void StartTraining()
    {
        anim.SetBool("HomeLoop", true);
    }
}
