﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using GooglePlayGames;                  //Dont remove, only used when Android build
using GooglePlayGames.BasicApi;    //Dont remove, only used when Android build

public class Login : MonoBehaviour
{
    #region Singleton
    private static Login _instance;
    public static Login Instance
    {
        get
        {
            return _instance;
        }
    }
    #endregion
    [Header("For TESTINGs")]
    public bool forTestLogin;
    public string usernameintput;
    public string passwordinput;

    [Header("VERSION CHECKING")]
    public GameObject disableButtonsReasonDisplay;
    public Button loginButton_, registerButton_;

    [Header("ATTEMPTING CONNECTION")]
    public GameObject nowConnecting;

    [Header("Registration")]
    //For registration
    public InputField Username;
    public InputField Password;
    public InputField ConfirmPassword;
    public InputField Email;
    [Space(20)]
    [Header("Register attempts")]
    //For registration attempt
    public GameObject infoPopup;
    public Text infoText;
    public GameObject goToStoreButton;
    [Space(20)]
    [Header("Remembering Registered Users")]
    //Used for verifying users
    string username;
    string password;
    [Header("Registration")]
    //Used for registration
    public InputField loginUsernameInput;
    public InputField loginPasswordInput;
    public Button registerButton, registrationBackButton;
    public Text errorMessage;
    public GameObject registrationPanel;
    public GameObject errorPopup;
    public GameObject retryLoginPopup;
    public Button retryConnectionButton;    // this button is used if the player is not connected and wants to try conneccting to server again
    [Space(20)]
    [Header("Relogin")]
    public GameObject reloginPanel;
    public Button loginButton;
    public Button cancelLoginButton;
    [Space(20)]
    [Header("Guest Register")]
    public InputField Guest_Username;
    [Space(20)]
    [Header("Waiting")]
    //Used for loading/checking with DB
    public GameObject loadingPopup;
    public GameObject registerationButtons;
    public Text loggingInText;
    [Space(20)]
    [Header("Logged In")]
    public GameObject logged_In_Popup;

    public Button loginUser;
    public Button cancelLogin;

    DateTime currentTime;
    float timeDiff;

    public Text versionNo;

    protected string successCheck;

    public static bool versionChecked;  //to prevent the version check from firing multiple times if players logout

    public static bool wrongVersion, underMaintainance, notConnected;



    //Link to php scripts
    string[] playerInfoList;

    public string usernameT;

    #region if Android
#if UNITY_ANDROID
	PlayGamesClientConfiguration config = new PlayGamesClientConfiguration();
#endif
#endregion

    void Awake()
    {

#if UNITY_ANDROID
		PlayGamesPlatform.Activate();
#endif
    }

    void Start()
    {
        _instance = this;

        TimeMaster.loggedIn = 0;

        versionChecked = false;

        versionNo.text = "VERSION " + Player.gameVersion;

        if (SceneManager.GetActiveScene().name.Contains("Login"))
        {
            if (!versionChecked)
            {
                versionChecked = true;
                GameSparksManager.Instance.CheckGameVersion();
                LoadingPopupMessage("Checking version . . .", true);
            }
            else
            {
                AutoLogin(usernameintput, passwordinput);
             }
        }
    }

    public void GoToStore()
    {
#if UNITY_ANDROID
        Application.OpenURL("market://details?id=" + "com.kaberos.xcalibur");
    //https://play.google.com/store/apps/details?id=
#endif

#if UNITY_EDITOR_OSX
        Application.OpenURL("itms-apps:itunes.apple.com/app/Xcalibur/id1384807541");
#endif

#if UNITY_IOS
        Application.OpenURL("itms-apps:itunes.apple.com/us/app/xcalibur/id1384807541");
#endif
    }

    // ====== VERSION DIFFERENCE ====== \\
    public void VersionIsWrong()
    {
        wrongVersion = true;

        LoginEnable();
    }
    // ====== Connection error ====== \\
    public void Connectivity(bool isConnected)
    {
        if (isConnected == false)
        {
            disableButtonsReasonDisplay.GetComponent<Text>().text = "Error connecting to server.";
        }
        disableButtonsReasonDisplay.SetActive(!isConnected);

        loginButton_.interactable = isConnected;
        registerButton_.interactable = isConnected;

        nowConnecting.SetActive(!isConnected);

        notConnected = !isConnected;

        LoginEnable();
    }
    public void Maintainance()
    {
        underMaintainance = true;

        LoginEnable();
    }

    void LoginEnable()
    {
        if (!underMaintainance && !wrongVersion && !notConnected)
        {

            if (!versionChecked)
            {
                GameSparksManager.Instance.CheckGameVersion();
                LoadingPopupMessage("Checking version . . .", true);
            }

            loginButton_.interactable = true;
            registerButton_.interactable = true;
        }
        else
        {
            if (wrongVersion)
            {
                disableButtonsReasonDisplay.GetComponent<Text>().text = "Game client is out of date.";
                goToStoreButton.SetActive(true);
                disableButtonsReasonDisplay.SetActive(true);
            }
            if (underMaintainance)
            {
                disableButtonsReasonDisplay.GetComponent<Text>().text = "Server under maintainance.";
                disableButtonsReasonDisplay.SetActive(true);
            }
            loginButton_.interactable = false;
            registerButton_.interactable = false;
            nowConnecting.SetActive(notConnected);

            BackToLogin();
        }
    }

    void BackToLogin()
    {
        reloginPanel.SetActive(false);
        registrationPanel.SetActive(false);

        registerationButtons.SetActive(true);
    }



    //========== REGISTRATION ==========\\

    //Verifying if all fields are fiilled
    public void AttemptRegister()
    {
        if (InfoFilled())
        {
            LoadingPopupMessage("Creating account...",true);
            GameSparksManager.Instance.GameSparksRegister();
        }
        else
            InfoPopup(true);
    }
    bool InfoFilled()
    {
        if (Username.text.Length < 2)
        {
            infoText.text = "Username must be 2 - 24 characters long";
            return false;
        }
        if (Password.text == "")
        {
            infoText.text = "Please key in password";
            return false;
        }
        if (Password.text.Length < 5)
        {
            infoText.text = "Password too short";
            return false;
        }
        if (Password.text != ConfirmPassword.text)
        {
            infoText.text = "Passwords do not match";
            return false;
        }
        if (Email.text == "")
        {
            infoText.text = "Please insert valid email";
            return false;
        }
        return true;
    }

    // ========== LOGIN ==========\\

    //Verify if Login details are filled
    bool LoginNameFilled()
    {
        if (loginUsernameInput.text == "")
            return false;
        return true;
    }
    bool LoginPasswordFilled()
    {
        if (loginPasswordInput.text == "")
            return false;
        return true;
    }
    public void AttemptLogin()
    {
        if (LoginNameFilled() == false)
        {
            PopupMessage("Username field cannot be empty",true);
            return;
        }
        if (LoginPasswordFilled() == false)
        {
            PopupMessage("Password field cannot be empty",true);
            return;
        }
        LoadingPopupMessage("Logging in as " + loginUsernameInput.text,true);
        GameSparksManager.Instance.GameSparksLogin();
    }

    public void AutoLogin()
    {
        if (PlayerPrefs.HasKey("lastPlayerUsername"))
        {
            GameSparksManager.Instance.GameSparksLogin(PlayerPrefs.GetString("lastPlayerUsername")
                                                       , PlayerPrefs.GetString("lastPlayerPassword"));
                             
            ToggleLoginButtons(false);
            LoadingPopupMessage("Logging in as " + PlayerPrefs.GetString("lastPlayerUsername"),true);
        }
    }
    public void AutoLogin(string usname, string pswrod)
    {
        GameSparksManager.Instance.GameSparksLogin(usname, pswrod);

        ToggleLoginButtons(false);
        LoadingPopupMessage("Logging in as " + usname, true);
    }

    // ======= RESPONSE HANDLING ======= \\
    public void ReenableRegistrationButtons()
    {
        registerButton.interactable = true;
        registrationBackButton.interactable = true;
        loginUser.interactable = true;
        cancelLogin.interactable = true;
    }
    public void ReenableLoginButtons()
    {
        loginButton.interactable = true;
        cancelLoginButton.interactable = true;
    }
    public void ToggleLoginButtons(bool isOn)
    {
        registerationButtons.SetActive(isOn);
    }
    public void ToggleRetryConnectionButton(bool isOn)
    {
        retryLoginPopup.SetActive(isOn);
    }

    // the text to carry any response
    public void PopupMessage(string message, bool toDisplay)
    {
        DisplayMessage(message);
        InfoPopup(toDisplay);
    }

    void DisplayMessage(string message)
    {
        infoText.text = message;
    }
    void InfoPopup(bool toDisplay)
    {
        infoPopup.SetActive(toDisplay);
    }

    public void LoadingPopupMessage(string message, bool toDisplay)
    {
        LoadingText(message);
        LoadingPopup(toDisplay);
    }

    // the loading screen with rotating orbs
    void LoadingPopup(bool isLoading)
    {
        loadingPopup.SetActive(isLoading);
    }
    void LoadingText(string text)
    {
        loggingInText.text = text;
    }

    public void RememberMe(string lastUsername, string lastPassword)
    {
        PlayerPrefs.SetString("lastPlayerUsername", lastUsername);
        PlayerPrefs.SetString("lastPlayerPassword", lastPassword);
    }


#region if Android
#if UNITY_ANDROID
	void GooglePlayLogin()
	{
		PlayGamesPlatform.InitializeInstance(config);
		PlayGamesPlatform.DebugLogEnabled = true;
		PlayGamesPlatform.Activate();

		Social.localUser.Authenticate(successCheck => { Debug.Log(successCheck); });
	}
#endif
#endregion
}
