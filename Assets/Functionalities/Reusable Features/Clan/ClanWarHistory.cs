﻿using UnityEngine;

public class ClanWarHistory : OngoingWars
{
    [Header("War History Info")]
    // ==== War History Info ==== \\
    public int clanMemberDuringWar;
    public int targetClanMemberDuringWar;

    public override void DisplayWarStatus()
    {
        displayParticipation.interactable = false;
        warStatus.text = "War Ended";
    }

    public override void UpdateDisplay()
    {
        base.UpdateDisplay();

        countdownTimerDisplay.text = CheckOutcome();
    }

    string CheckOutcome()
    {
        if (ClanPointsAchieved() > TargetClanPointsAchieved())
        {
            countdownTimerDisplay.color = Color.green;
            return "VICTORIOUS";
        }
        if (TargetClanPointsAchieved() > ClanPointsAchieved())
        {
            countdownTimerDisplay.color = Color.red;
            return "DEFEATED";
        }
        countdownTimerDisplay.color = Color.grey;
        return "DRAW";
    }

    public override int ClanPointsAchieved()
    {
        playerPointCount = 50 - targetClanMemberDuringWar + clanParticipationCount;
        return playerPointCount;
    }
    public override string MyClanParticipationCount()
    {
        return string.Format("Participation : {0} / {1}", clanParticipationCount, clanMemberDuringWar);
    }
    public override string YourClanParticipationCount()
    {
        return string.Format("Participation : {0} / {1}", targetClanParticipationCount, targetClanMemberDuringWar);
    }
}