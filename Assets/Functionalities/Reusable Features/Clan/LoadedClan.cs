﻿using UnityEngine;
using UnityEngine.UI;

public class LoadedClan : MonoBehaviour 
{
    public Clan clan;

    [HideInInspector] public string 	clanName;
    [HideInInspector] public string 	clanLeaderName;
    [HideInInspector] public string clanLeaderID;

	// ------------ INFOS ------------ \\

	[HideInInspector] public string 	clanID;
	[HideInInspector] public int 	clanFlagID;
    [HideInInspector] public int clanMembers;
	[HideInInspector] public string 	clanNotice;

	// ---------- RESOURCES ---------- \\

    [HideInInspector] public int clanAvailableGold;
    [HideInInspector] public int clanAvailableGoldLimit;

    [HideInInspector] public int loadedClanOBJNumber;       // For GS Responses to return values to the correct Loaded Clan OBJ

    public Text clanName_text;
    public Text clanLeaderName_text;
    public Text clanMembers_Text;
    public Image clanFlag;

    public Button applyToClanButton;





    void Start()
    {
        applyToClanButton.onClick.AddListener(ApplyToClan);
    }






    public void GetTeamMemberCount()
    {
        GameSparksManager.Instance.ClanMemberCountRequest(clanName, loadedClanOBJNumber);
    }




    public void DisplayInfo()
    {
        if (ToDisplay())
            UpdateInfo();
        DisplayToggle(ToDisplay());
    }
    bool ToDisplay()
    {
        if (loadedClanOBJNumber < ClanManager.Instance.currentSearchLoadedCount)
            return true;
        return false;
    }
    void UpdateInfo()
    {
        clanName_text.text = clanName;
        clanID = clanName;
        clanLeaderName_text.text = clanLeaderName;
        clanFlag.sprite = clan.flagImages[clanFlagID];
        clanMembers_Text.text = string.Format("{0} / {1}", clanMembers, ClanManager.Instance.clanMaxMemberCount);
    }
    void DisplayFlag()
    {
        clanFlag.sprite = clan.flagImages[clanFlagID];
    }
    void DisplayToggle(bool setDisplay)
    {
        gameObject.SetActive(setDisplay);
    }




    public void ApplyToClan()
    {
        GameSparksManager.Instance.JoinClanRequest(clan, clanID, clanLeaderID);
    }
}
