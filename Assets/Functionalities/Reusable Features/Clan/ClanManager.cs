﻿using UnityEngine;
using UnityEngine.UI;

public class ClanManager : MonoBehaviour
{
    #region singleton
    private static ClanManager _instance;
    public static ClanManager Instance
    {
        get
        {
            return _instance;
        }
    }
    #endregion

    public enum RemoveFromClan
    {
        none,
        disband,
        kick 
    }

    [HideInInspector] public int clanMaxMemberCount = 50;
    [HideInInspector] public int clanTreasuryMaxStorageAmount = 500000;
    public Clan playerClanInfo;
    public static RemoveFromClan clanRemovalNotificaion;
    public static string oldClan;

    // ==== CREATE CLANS VARIABLES ==== \\
    [Header("Clan Creation")]
    public Button createClan;
    public GameObject createClanePopup;
    public Button attemptCreateClan;
    public InputField clanNameInput;

    [Space(20)]
    [Header("Clan Panels")]
    public GameObject clanListPanel;    // when Player is not in Clan
    public GameObject clanInfoPanel;    // when Player is in a Clan

    [Space(20)]
    [Header("Clan List Creation")]
    // ==== CLAN LIST CREATION ==== \\
    public Transform loadedClanOBJParent;
    public GameObject loadedClanOBJ;
    [HideInInspector] public LoadedClan[] loadedClanList;

    [Space(20)]
    [Header("Clan Search")]
    // ==== SEARCH FOR GUILD VARIABLES ==== \\
    public Button retrieveClans;
    [HideInInspector] public int clanToLoadCount = 10;      //How many Guild infos to load
    [HideInInspector] public int clanToLoadOffset = 0;
    [HideInInspector] public int currentSearchLoadedCount = 0;               // To track the number of Clans loaded in this search
    int requestCounter = 0;
    public InputField clanNameFilter;

    [Space(20)]
    [Header("Clan Message Handlers")]
    // ===== MESSAGE HANDLERS ===== \\
    public GameObject messagePop;
    public Button messagePopupClose;
    public Image messageBGColor;
    public Button clanRemovedAcknowledgedButton;
    public Text responseMessage;
    public Button leaveClanButton;

    [Space(20)]
    [Header("Clan Wars Outcome Handlers")]
    public GameObject clanOutcomePanel;
    public enum BattleOutcome { VICTORIOUS, DEFEATED, DRAW };
    public Text clanNameText, targetClanNameText, battleOutcomeText;
    public Button moveToHistory;
    public Button attemptSendNotifiedOfOutcome;

    [Space(20)]
    [Header("Member panel update")]
    public GameObject updateMemberBlockerPanel;

    [Space(20)]
    [Header("FOR TESTING")]
    // ==== FOR TESTING ==== \\
    public Button loginButton;

    string username = "RylaiCrestmaiden";

    string password = "1234";

    //string password = "teamKaberos2018";

    //string username = "Kaberos";

    //string username = "Garachomp";

    //string password = "Garachomp";



    void Awake()
    {
        _instance = this;
    }

    void Start()
    {
        ToggleCreateClan(false);
        ClearClanNameInput();

        createClan.onClick.AddListener(delegate { ToggleCreateClan(true); });
        createClan.onClick.AddListener(playerClanInfo.ResetFlag);  // Everytime the player hits the button, flag starts at 0
        attemptCreateClan.onClick.AddListener(AttemptCreate);

        //moveToHistory.onClick.AddListener(delegate { MoveWarToHistory(Enemy.clan); });

        // Add the function to the button
        retrieveClans.onClick.AddListener(GameSparksManager.Instance.ClanListRequest);

        leaveClanButton.onClick.AddListener(playerClanInfo.LeaveClan);

        // == TEST FUNCTION == \\
        loginButton.onClick.AddListener(delegate { DeactivateSelf(false); });


        clanRemovedAcknowledgedButton.onClick.AddListener(GameSparksManager.Instance.SavePlayerDetails);
        clanRemovedAcknowledgedButton.onClick.AddListener(RemovalAcknowledged);
        clanRemovedAcknowledgedButton.onClick.AddListener(DisableAcknowledgeButton);

        // Instantiate the loaded clan buttons
        CreateLoadedClanList();
    }


    // ==== DISABLE LOGIN BUTTON ==== \\
    public void DeactivateSelf(bool deactivate)
    {
        loginButton.interactable = deactivate;
    }

    void DisableAcknowledgeButton()
    {
        clanRemovedAcknowledgedButton.gameObject.SetActive(false);
    }



    // ===== CLAN REMOVAL/DISBAND NOTIFICATION ===== \\
    public void CheckForClanRemovalNotification()
    {
        if (clanRemovalNotificaion == RemoveFromClan.disband)
        {
            messageBGColor.color = new Vector4(0, 0, 0, 0.8f);
            InfoPopup(string.Format("Clan {0} has disbanded.", oldClan));

            SetPopupInteractable(false);
            clanRemovedAcknowledgedButton.gameObject.SetActive(true);
        }
        else if (clanRemovalNotificaion == RemoveFromClan.kick)
        {
            messageBGColor.color = new Vector4(0, 0, 0, 0.8f);
            InfoPopup(string.Format("You have been removed from Clan {0}.", oldClan));

            SetPopupInteractable(false);
            clanRemovedAcknowledgedButton.gameObject.SetActive(true);
        }
    }
    void RemovalAcknowledged()
    {
        TogglePopup(false);
        oldClan = "";
        clanRemovalNotificaion = RemoveFromClan.none;
        messageBGColor.color = new Vector4(0, 0, 0, 0.0039f);

        GameSparksManager.Instance.AcknowledgeRemovalFromClan(this);
    }


    // ==== TOGGLING BETWEEN IN AND OUT OF CLANS ==== \\
    public void ClanPanelToggle(bool inClan)
    {
        clanListPanel.SetActive(!inClan);
        clanInfoPanel.SetActive(inClan);
    }


    // ==== WHEN NOT IN CLAN ==== \\
    public void ToggleCreateClan(bool isOn)
    {
        ClearClanNameInput();
        createClanePopup.SetActive(isOn);
    }
    void AttemptCreate()
    {
        if (NameCheckPass())
        {
            SetPopupInteractable(false);
            Clan.clanFlag = SetSelectedFlag();
            InfoPopup("Loading . . .");
            GameSparksManager.Instance.CreateClanRequest();
        }
        else
        {
            InfoPopup("CLAN Name too short");
        }
    }
    bool NameCheckPass()
    {
        if (clanNameInput.text.Length < 2)
            return false;
        return true;
    }
    void ClearClanNameInput()
    {
        clanNameInput.text = "";
    }
    int SetSelectedFlag()
    {
        int toReturn = 0;
        for (int x = 0; x < playerClanInfo.flagImages.Length; x++)
        {
            if (playerClanInfo.currentlySelectedFlag.sprite == playerClanInfo.flagImages[x])
                toReturn = x;
        }
        return toReturn;
    }


    // === Create CLAN OBJ in CLAN LIST PANEL === \\
    void CreateLoadedClanList()
    {
        loadedClanList = new LoadedClan[10];
        for (int x = 0; x < clanToLoadCount; x++)
        {
            GameObject gameObj = Instantiate(loadedClanOBJ, loadedClanOBJParent.position, Quaternion.identity, loadedClanOBJParent);
            loadedClanList[x] = gameObj.GetComponent<LoadedClan>();
            loadedClanList[x].clan = playerClanInfo;
            loadedClanList[x].loadedClanOBJNumber = x;
            gameObj.SetActive(false);
        }
    }

    public void ClanListObtained(int i)
    {
        requestCounter = 0;
        currentSearchLoadedCount = i;
        for (int x = 0; x < i; x++)
        {
            GameSparksManager.Instance.ClanMemberCountRequest(loadedClanList[x].clanName, x);
        }
    }
    public void ClanMemeberCountObtained()
    {
        requestCounter += 1;
        if (requestCounter == currentSearchLoadedCount)
        {
            for (int x = 0; x < clanToLoadCount; x++)
            {
                loadedClanList[x].DisplayInfo();
            }
        }
    }


    // ==== WHEN IN CLAN ==== \\

    // === Create CLAN MEMBERS OBJ IN CLAN INFO PANEL ==== \\
    public void ClanInfoObtained(string requestType)
    {
        if (requestType == "join")
        {
            RetrieveMemberandClanInfo(requestType);
            EventHolderManager.Instance.ClanRejoinedCheck();
            GameSparksManager.Instance.SendJoinClanNotification();
        }
        else if (requestType == "create")
            Invoke("InvokedRetrieval", 0.3f);
    }
    void InvokedRetrieval()
    {
        RetrieveMemberandClanInfo("create");
    }
    void RetrieveMemberandClanInfo(string requestType)
    {
        playerClanInfo.CreateClanMembers();
        playerClanInfo.RequestClanInfos();
        if (requestType == "create")
            playerClanInfo.InitClanValues();

        //EventHolderManager.Instance.ClanProgressRetrieved(0);
    }
    // =================================\\


    // ======= INFO POPUP THAT CARRYS ANY ERROR / SUCCESS MESSAGE ======= \\
    public void InfoPopup(string message)
    {
        responseMessage.text = message;
        TogglePopup(true);

        if (message.ToLower().Contains("are you sure you want to leave"))
            ToggleLeaveClanButton(true);
    }
    public void SetPopupInteractable(bool touchable)
    {
        // if it is a LOADING notification message, popup should not close
        messagePopupClose.interactable = touchable;
    }
    public void TogglePopup(bool isOn)
    {
        messagePop.SetActive(isOn);
    }
    public void ToggleLeaveClanButton(bool isAttemptLeave)
    {
        leaveClanButton.gameObject.SetActive(isAttemptLeave);
    }

    public void UpdatingClanMemberInfo(bool toSet)
    {
        updateMemberBlockerPanel.SetActive(toSet);
    }
    // =================================\\


    // ===== CLAN WAR OUTCOME CARRIER ===== \\

    public void WarConcluded(string enemyClanID, int battleOutcome)
    {
        //

        // only handles the displaying of outcomes
        InsertClanNames(battleOutcome);
        // display the panel
        DisplayWarOutcomeToggle(true);
        // does the checks and calculations elsewhere
        
    }
    void InsertClanNames(int outcome)
    {
        clanNameText.text = Clan.clanID;
        targetClanNameText.text = Enemy.clan;
        
        battleOutcomeText.text = System.Enum.GetName(typeof(BattleOutcome), outcome);
    }
    void DisplayWarOutcomeToggle(bool isDisplayed)
    {
        clanOutcomePanel.SetActive(isDisplayed);
    }

    void MoveWarToHistory(string clanID)
    {
        //perform the moving of concluded war to the History panel
        for (int x = 0; x < playerClanInfo.ongoingWarsLoaded.Count; x++)
        {
            if (playerClanInfo.ongoingWarsLoaded[x].targetClanName == clanID)
            {
                // finding the correct OBJ to move

            }
        }
    }
    // =================================\\
}
