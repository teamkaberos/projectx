﻿using UnityEngine;
using UnityEngine.UI;

public class ClanWarParticipant : MonoBehaviour
{
    // ==== INFO HOLDER ==== \\
    public string participantName;
    public string participantID;

    // ==== DISPLAY UI INFO ==== \\ 
    public Text participantNameText;
    public GameObject participatedDisplay;

    public Button attemptAttackButton;



    public void DisplayParticipation(bool participated)
    {
        participatedDisplay.SetActive(participated);
    }
    public void UpdateDisplay()
    {
        participantNameText.text = participantName;
    }
    public void SetMyDisplay(bool setActive)
    {
        gameObject.SetActive(setActive);
    }
}