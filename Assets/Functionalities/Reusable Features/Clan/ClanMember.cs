﻿using UnityEngine;using UnityEngine.UI;


public class ClanMember : MonoBehaviour 
{
    // ==== CLAN REFERENCE ==== \\
    [HideInInspector]public Clan clan; //if the child member ever needs to send anything to the Clan class

    // ==== INFO STORAGE ==== \\
    public int victories = 100, losses = 100, battles = 200, heroesUnlocked = 1;
    public int[,] assignedUnits = new int[3,3];
    public long lastLogout;

    public string memberID;
    public string memberName;
    public string memberRank;
    public int memberRankNo;
    public int memberContribution;
    public int memberReputation;
    public int memberLv;
    public string memberPortrait;


    // ==== UI STUFF ==== \\
    public Text memberNameText, memberLVText, memberRankText, memberContributionText, memberReputationText;
    public Button memberInfoButton;




    public void AddPopupListener()
    {
        memberInfoButton.onClick.AddListener(delegate { clan.MemberDisplayPopup(this); });
    }
    public void DisplayPotrait()
    {
        PotraitManager.Instance.ClanMemberAvatarDisplay(memberPortrait);
    }
    public string VictoryCount()
    {
        if(battles > 0)
            return string.Format("{0:n0} / {1:n0} ({2:n0})%", victories, battles, ((victories * 100f) / battles));
        return "0 / 0";
    }
    public string HeroesUnlockedCount()
    {
        return string.Format("{0} / 15", heroesUnlocked);
    }
    public string LastSeen()
    {
        return TimeConvert(TimeMaster.Instance.CalculateTimeDifference(DataManager.initialLoginServerTime,lastLogout));
    }

    string TimeConvert(long timeDiff)
    {
        int days = Mathf.FloorToInt(timeDiff / 86400);
        if (days > 1)
            return string.Format("{0} days ago", days);
        
        int hours = Mathf.FloorToInt((timeDiff - (days * 86400)) / 3600);
        if (hours > 1)
            return string.Format("{0} hours ago", hours);
        
        int mins = Mathf.FloorToInt((timeDiff - (days * 86400) - (hours * 3600)) / 60);
        if (mins > 1)
            return string.Format("{0} minutes ago", mins);
        return "Less than a minutes ago";
    }

    public void UpdateMemberInfo()
    {
        memberNameText.text = memberName;
        memberLVText.text = "Lv." + memberLv;
        memberRankText.text = memberRank;
        memberContributionText.text = string.Format("{0:n0}", memberContribution);
        memberReputationText.text = string.Format("{0:n0}", memberReputation);
    }

    public void DestroyThis()
    {
        Destroy(gameObject);
    }
}