﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class OngoingWars : MonoBehaviour
{
    // ==== Ongoing war OBJ ==== \\
    public string status = "unknown";
    DateTime date;    // to track for next day

    public int clanParticipationCount;

    public string targetClanName;
    public string targetClanLeaderName;
    public int targetClanPoints;
    public int targetClanParticipationCount;
    public int targetClanMembers;
    public int targetClanWarVictories, targetClanWars;
    public int playerPointCount, targetPointCount;

    // ==== Ongoing War UI ==== \\
    [Header("Player Clan UI")]
    public Text playerClanNameText;
    public Text playerClanLeaderText;
    public Text playerClanMembersText;
    public Text playerClanPointsText;
    public Text playerClanVictoriesText;
    public Text playerPointCountText;
    [Header("Enemy Clan UI")]
    public Text targetClanNameText;
    public Text targetClanLeaderText;
    public Text targetClanMembersText;
    public Text targetClanPointsText;
    public Text targetClanVictoriesText;
    public Text targetPointCountText;

    [Header("DisplayStats")]
    public Button displayParticipation;

    [Header("Status Display")]
    public Text warStatus;
    public Text countdownTimerDisplay;




    void Start()
    {
        date = DateTime.UtcNow.Date;
    }
    void Update()
    {
        if (status == "pending" || status == "running")
            countdownTimerDisplay.text = string.Format("{0:00}:{1:00}:{2:00}" , TimeToTomorrow().Hours,TimeToTomorrow().Minutes,TimeToTomorrow().Seconds);
    }

    TimeSpan TimeToTomorrow()
    {
        TimeSpan timeLeft = GameSparksManager.Instance.TimeDifferenceToTomorrowUTC().Subtract(DateTime.UtcNow);
        return timeLeft;
    }

    public void CreatListener(Clan clan)
    {
        displayParticipation.onClick.RemoveAllListeners();      //Remove first in the case this gets call multiple times
        displayParticipation.onClick.AddListener(delegate { clan.DisplayParticipationStats(this); } ); 
    }

    public virtual void UpdateDisplay()
    {
        // == PLayer's Clan Stuff == \\
        playerClanNameText.text = Clan.clanID;
        playerClanLeaderText.text = Clan.leaderName;
        playerClanPointsText.text = "Clan Points : " + Clan.clanPoints;
        playerClanMembersText.text = MyClanParticipationCount();
        playerClanVictoriesText.text = string.Format("Battles : {0:n0} / {1:n0}", Clan.clanWarsVictories, Clan.clanWarsCount);
        playerPointCountText.text = string.Format("{0} / 50", ClanPointsAchieved());

        // == Target Clan Stuff == \\
        targetClanNameText.text = targetClanName;
        targetClanLeaderText.text = targetClanLeaderName;
        targetClanPointsText.text = "Clan Points : " + targetClanPoints;
        targetClanMembersText.text = YourClanParticipationCount();
        targetClanVictoriesText.text = string.Format("Battles : {0:n0} / {1:n0}", targetClanWarVictories, targetClanWars);
        targetPointCountText.text = string.Format("{0} / 50", TargetClanPointsAchieved());

        DisplayWarStatus();
    }

    public virtual void DisplayWarStatus()
    {
        if (status == "pending")
        {
            displayParticipation.interactable = false;
            warStatus.text = "War Begins in :";
            warStatus.color = Color.red;
        }
        else if (status == "running")
        {
            displayParticipation.interactable = true;
            warStatus.text = "War Ends in :";
            warStatus.color = Color.grey;
        }
    }
    public virtual int ClanPointsAchieved()
    {
        playerPointCount = 50 - targetClanMembers + clanParticipationCount;
        return playerPointCount;
    }
    public int TargetClanPointsAchieved()
    {
        targetPointCount = 50 - Clan.clanMembersID.Count + targetClanParticipationCount;
        return targetPointCount;
    }
    public virtual string MyClanParticipationCount()
    {
        return string.Format("Participation : {0} / {1}", clanParticipationCount, Clan.clanMembersID.Count);
    }
    public virtual string YourClanParticipationCount()
    {
        return string.Format("Participation : {0} / {1}", targetClanParticipationCount, targetClanMembers);
    }

    public void RemoveMe()
    {
        Destroy(gameObject, 0.2f);
    }
}