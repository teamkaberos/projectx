﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class Clan : MonoBehaviour
{
    // ------------ INFOS ------------ \\
    public static string clanID = "<currently_not_in_any_clan_yet>";    // Required to be declared as GS unable to save
    public static long clanJoinedTimeStamp;
    public static bool clanJoinedTimeStampCheck;    // check if the Clan joined time stamp has already been retrieved
    public static string clanIcon;
    public static int clanMemberLimit;
    public static string clanNotice;
    public static string leaderID;
    public static string leaderName;
    public static int clanFlag = 0;                           // the actual number of the flag
    public static int maxOfficerCount = 4;

    // ---------- CLAN RESOURCES ---------- \\
    public static int clanAvailableGold;
    public static int clanAvailableGoldLimit;

    // ---------- PLAYER RESOURCES ---------- \\
    public static int contributionPoints;
    public static int contributionIncrements = 5;   // How much to increment on every action that benefits CLAN
    public static int rank = -100;

    // ---------- CLAN WARS ------------ \\
    public static int clanWarsCount;
    public static int clanWarsVictories;
    public static int clanPoints;

    // ====== Will this BATTLE update Clan's Info ====== \\
    public static bool atWarEnemy;
    public static bool battleOutcome;
    public static int pointsToUpdate = 5;   // this is how much will be update on the cloud if the player wins this fight

    // ======== UI ======== \\
    public static List<string> clanMembersID = new List<string>();
    public static List<string> clanMembersName = new List<string>();
    public static List<string> clanMembersPortrait = new List<string>();
    public static List<int> clanMembersReputation = new List<int>();
    public static List<int> clanMembersRankValue = new List<int>();
    public static List<int> clanMembersContributions = new List<int>();
    public static List<int> clanMembersBattles = new List<int>();
    public static List<int> clanMembersVictories = new List<int>();
    public static List<int> clanMembersLosses = new List<int>();
    public static List<long> clanMembersLastLogout = new List<long>();

    public Text clanNameText;
    public Text clanNoticeText;
    public Text clanMembersText;
    public Text clanGoldStorageText;
    public Text clanWarsText;
    public Text clanPointsText;
    public Image clanCurrentFlag;

    public Toggle globalChatToggle;
    public ChatBoxContent clanChatBox;

    public GameObject retrievalInfoPopupBlocker;

    //====== CLAN MEMBER INFO DISPLAY ====== \\
    [HideInInspector] public string memberOnDisplay;
    [HideInInspector] public string memberOnDisplayName;
    [HideInInspector] public string memberOnDisplayRank;
    [HideInInspector] public int memberOnDisplayRankValue;
    [HideInInspector] public int memberOnDisplayContribution;
    [HideInInspector] string previousMemberOnDisplay;
    public Text memberNameText, memberRankText, memberContributionText, memberReputationText;
    public Text battlesText, heroesUnlockedText, memberLastSeenText;
    public Image potraitDisplay;
    public GameObject memberInfoPopup;
    public GameObject removeMember_Button;      // holds a button but refrenced as gameobject as setactive is used
    public GameObject sendGold_Button;               // holds a button but refrenced as gameobject as setactive is used
    public GameObject promoteMember_Button;    // holds a button but refrenced as gameobject as setactive is used
    public GameObject demoteMembner_Button;
    //====== CLAN MEMBER INFO DISPLAY END ====== \\


    // ===== EDIT CLAN INFO ===== \\
    [Space(15)]
    [Header("CREATION")]
    // ==== MEMBER OBJ CREATION ==== \\
    public GameObject clanMemberOBJ;
    public Transform clanMemberScroll;
    int distributionCounter = 0;        //For keeping track of how many members info are returned
    public List<ClanMember> clanMembers;
    // ==== MEMBER OBJ CREATION END ==== \\

    [Space(15)]
    [Header("CLAN FLAGS")]
    public Sprite[] flagImages;                       // The collection of sprite images
    public Image currentlySelectedFlag;         // When the player is creating a clan
    public Button[] nextFlag;                          // There is 2 instances where player can display the next flag
    public Button[] previousFlag;                   // There is 2 instances where player can display the previous flag
    public Button toggleChangeFlagPanel;
    public Button attemptChangeFlag;           // Used to attempt sending data to the Cloud
    [HideInInspector] public int tempFlagNo;                                       // used to toggle between all flags
    public Image currentChosenFlag;             // the selected flag for the CLAN (IF PLAYER IS ALREADY IN CLAN)
    public GameObject changeFlagPopup;      // Popup that holds the Change flag Panel
    public Text beSeenAsText;                       // The text that appears in the changeFlagPopup

    [Space(15)]
    [Header("CLAN NOTICE")]
    public Button setClanMessageButton;         // to toggle the setclan message popup
    public Button closeSetMessageButton;
    public InputField clanNoticeInputField;       // what do you want to notify your clan about
    public GameObject changeNoticePopup;    // the popup that holds the clanNotice input field
    public Button attemptChangeMessageButton;  // to send new message to the cloud
    public Text messageCountText;

    [Space(15)]
    [Header("STORAGE")]
    // ===== CLAN STORAGE ===== \\
    public InputField goldStorageValue;
    public Text goldLeftAmount;
    public Button storeGold;
    public Button attemptStorage;
    public GameObject storeGoldPopup;
    // ===== CLAN STORAGE END ===== \\

    [Space(15)]
    [Header("SEND GOLD")]
    // ===== MEMBER SEND GOLD ===== \\
    public Text goldSpaceAvailableDisplay;

    public Button sendGoldButton;       // this is the BUTTON on the player info popup
    public Button attemptSend;    // the button that activates the SEND TO PLAYER method
    public Text sendGoldText;     // to be changed between "Withdraw" and "Send" depending on Leader or not
    public InputField sendGoldAmount;
    public GameObject sendGoldPopup;

    public Text treasurySpaceAvailableDisplay;
    public Button withdrawGoldButton;       // only accessible to the leader
    public Button attemptWithdraw;
    public Text withdrawGoldText;
    public InputField withdrawGoldAmount;
    public GameObject withdrawGoldPopup;

    [HideInInspector] public int memberPossessedGold = -100;
    [HideInInspector] public long memberStorageGoldLimit = -100;
    // ===== MEMBER SEND GOLD ===== \\

    [Space(15)]
    [Header("PROMOTION")]
    // ====== MEMBER PROMOTION ====== \\
    public Button promoteButton;
    public Button attemptPromote;
    public Text promoteMemberText;
    public GameObject promotePopup;
    // ====== MEMBER PROMOTION END ====== \\
    [Header("DEMOTION")]
    public Button demoteButton;
    public Button attemptDemoteButton;
    public Text demoteMemberText;
    public GameObject demotePopup;
    // ====== MEMBER DEMOTION END ====== \\
    [Header("LEADERSHIP TRANSFER")]
    public Button transferLeadership;
    public Button attemptTransferLeadership;
    public Text transferLeaderText;
    public GameObject transferLeaderPopup;

    [Space(15)]
    [Header("REMOVE MEMBERS")]
    // ====== REMOVE MEMBER ====== \\
    public GameObject removeMemberPopup;
    public Text removeMemberText;
    public Button removeMember;
    public Button attemptRemove;
    // ====== REMOVE MEMBER END ====== \\

    [Space(15)]
    [Header("CLAN WARS")]

    [Header("NEW WARS")]
    // ====== CLAN WARS ====== \\
    public Button toggleClanWarsPanel;
    public Toggle newWarToggle, ongoingWarToggle, warHistoryToggle;
    // === SEARCH CLAN === \\
    public InputField clanTargetFilter;
    public Button searchTargetClans;
    public GameObject clanWarsPanel, newWarsPanel, ongoingWarsPanel, warHistoryPanel;
    [HideInInspector] public int clanemiesToLoad = 8;
    public GameObject targetClanOBJ;
    public Transform newWarsLoadedEnemyPanel;
    [HideInInspector] public List<TargetClan> targetClansLoaded = new List<TargetClan>();
    [HideInInspector] public List<string> targetClanNameList = new List<string>();
    [HideInInspector] public List<string> targetClanLeadersList = new List<string>();
    [HideInInspector] public List<int> targetClanPointList = new List<int>();
    [HideInInspector] public List<int> targetClanBattleCountList = new List<int>();
    [HideInInspector] public List<int> targetClanBattleVictoriesList = new List<int>();
    TargetClan targetToChallenge;
    public Button attemptConfirmDeclareWar;
    public GameObject declareWarConfirmationPopup;
    public Text confirmWarText;

    [Space(15)]
    [Header("ONGOING WARS")]
    // === ONGOING WARS WITH CLAN === \\
    public Transform ongoingClanWarsPanel;                                                  // Used for parenting and positioning OBJs
    public GameObject ongoingWarObj;
    [HideInInspector] public List<OngoingWars> ongoingWarsLoaded = new List<OngoingWars>();
    [HideInInspector] public List<string> ongoingWarTargetClanID = new List<string>();
    [HideInInspector] public List<string> ongoingWarTargetClanLeaderID = new List<string>();
    [HideInInspector] public List<string> ongoingWarsTargetClanLeaderName = new List<string>();
    // Enemy Clan Members
    [HideInInspector] public List<List<string>> ongoingWarsTargetClanMembersID = new List<List<string>>();
    [HideInInspector] public List<List<string>> ongoingWarsTargetClanMembersName = new List<List<string>>();
    // Enemy Clan List of participating members ID
    [HideInInspector] public List<List<string>> ongoingWarTargetClanMemberParticipation = new List<List<string>>();
    // Player Clan List of participating members ID
    [HideInInspector] public List<List<string>> ongoingWarClanMemberParticipation = new List<List<string>>();

    [HideInInspector] public List<int> ongoingWarClanPoints = new List<int>();
    [HideInInspector] public List<int> ongoingWarClanBattleCountList = new List<int>();
    [HideInInspector] public List<int> ongoingWarClanBattleVictories = new List<int>();

    [HideInInspector] public List<string> ongoingWarsStatusList = new List<string>();


    [Space(15)]
    [Header("Clan Participation")]
    public GameObject particiaptionPanel;
    public Text myClanNameText, targetClanNameText, myClanParticipationNumberText, targetClanParticipationNumberText;
    public Transform myClanParticipationPanel, targetClanParticipationPanel;
    public GameObject clanWarParticipantOBJ;
    public List<ClanWarParticipant> memberParticipantList = new List<ClanWarParticipant>();
    public List<ClanWarParticipant> targetMemberParticipantList = new List<ClanWarParticipant>();


    [Space(15)]
    [Header("WAR HISTORY")]
    // === WAR HISTORIES === \\
    public Transform clanWarHistoryPanel;                                                  // Used for parenting and positioning OBJs
    public GameObject clanHistoryOBJ;
    [HideInInspector] public List<ClanWarHistory> clanWarHistoryLoaded = new List<ClanWarHistory>();
    [HideInInspector] public List<string> endedClanWarTargetClanID = new List<string>();
    [HideInInspector] public List<string> endedClanWarTargetClanLeaderID = new List<string>();
    [HideInInspector] public List<string> endedClanWarTargetClanLeaderName = new List<string>();
    [HideInInspector] public List<int> endedClanWarTargetClanMemberCountDuringWar = new List<int>();
    [HideInInspector] public List<List<string>> endedClanWarTargetClanParticipationList = new List<List<string>>();

    [HideInInspector] public List<List<string>> endedClanWarClanMemberParticipationList = new List<List<string>>();
    [HideInInspector] public List<int> endedClanWarClanMemberCountDuringWar = new List<int>();


    // ====== CLAN WARS END====== \\
    [Space(15)]
    [Header("LEAVE CLAN")]
    // ===== LEAVE CLAN ===== \\
    public Button leaveClanButton;


    void Start()
    {
        // Listens to GSManager for member promotions
        GameSparksManager.GoldDropped += RecieveNotificationOfGoldSent;
        GameSparksManager.PromotionNotice += RecievePromotionNotification;
        GameSparksManager.DemotionNotice += RecieveDemotionNotification;
        GameSparksManager.GoldIncreased += RecieveNotificationOfTreasuryUpdate;
        GameSparksManager.MemberJoined += ReceiveNewClanMember;
        GameSparksManager.MemberRemovalNotice += MemberRemoved;
        GameSparksManager.ClaNotificationHasChanged += GetNewMessage;
        GameSparksManager.ClanLeaderHasChanged += LeadershipChanged;

        GameSparksManager.WarDeclared += AttemptRetrieveClanWars;
        GameSparksManager.WarPointsChanged += MemberParticipationUpdated;
        GameSparksManager.ClanFlagChanged += SetClanCurrentFlag;

        promoteButton.onClick.AddListener(DisplayPromotionInfo);
        attemptPromote.onClick.AddListener(AttemptPromote);

        demoteButton.onClick.AddListener(DisplayDemotionInfo);
        attemptDemoteButton.onClick.AddListener(AttemptDemote);

        toggleChangeFlagPanel.onClick.AddListener(delegate { SetTempFlagNo(tempFlagNo); });
        toggleChangeFlagPanel.onClick.AddListener(delegate { ToggleFlagChangePopup(true); });
        toggleChangeFlagPanel.onClick.AddListener(UpdateChangeFlagText);

        attemptChangeFlag.onClick.AddListener(AttemptSaveFlagChangeInfo);

        for (int x = 0; x < nextFlag.Length; x++)
        {
            nextFlag[x].onClick.AddListener(delegate { FlagSwitch(1); });
            previousFlag[x].onClick.AddListener(delegate { FlagSwitch(-1); });
        }

        storeGold.onClick.AddListener(delegate { ToggleStoreGoldPopup(true); });       // togglePopup
        goldStorageValue.onValueChanged.AddListener(StorageValueUpdate);            // this check amount is for storing GOLD
        attemptStorage.onClick.AddListener(AttemptDeposit);                                    // this is attempting to comm GS for storage

        withdrawGoldAmount.onValueChange.AddListener(WithdrawValueParseUpdateDisplay);

        sendGoldButton.onClick.AddListener(RetrieveSelectedPlayerRequiredInfos);
        attemptSend.onClick.AddListener(AttemptSend);

        removeMember.onClick.AddListener(DisplayRemoveConfirmation);
        attemptRemove.onClick.AddListener(AttemptRemove);

        toggleClanWarsPanel.onClick.AddListener(ToggleClanWarsPanel);
        toggleClanWarsPanel.onClick.AddListener(GetOngoingWars);
        newWarToggle.onValueChanged.AddListener(ToggleNewWarsPanel);
        ongoingWarToggle.onValueChanged.AddListener(ToggleOngoingWarsPanel);
        warHistoryToggle.onValueChanged.AddListener(ToggleWarHistoryPanel);
        searchTargetClans.onClick.AddListener(AttemptSearchTargetClans);

        attemptConfirmDeclareWar.onClick.AddListener(AttemptDeclareWar);

        leaveClanButton.onClick.AddListener(AttemptLeaveClan);

        TogglePopup(false);
        ClanParticipantCreation();
    }
    void OnDestroy()
    {
        GameSparksManager.GoldDropped -= RecieveNotificationOfGoldSent;
        GameSparksManager.PromotionNotice -= RecievePromotionNotification;
        GameSparksManager.GoldIncreased -= RecieveNotificationOfTreasuryUpdate;
        GameSparksManager.MemberJoined -= ReceiveNewClanMember;
        GameSparksManager.MemberRemovalNotice -= MemberRemoved;
        GameSparksManager.WarDeclared -= AttemptRetrieveClanWars;

        GameSparksManager.ClanFlagChanged -= SetNewClanFlag;
        GameSparksManager.DemotionNotice -= RecieveDemotionNotification;
        GameSparksManager.ClaNotificationHasChanged -= GetNewMessage;

        Clear();
    }

    void LoadingImageWhenRetrieving()
    {
        retrievalInfoPopupBlocker.SetActive(true);
    }

    // ====== CREATE CLAN STUFF ====== \\
    public void CreateClanInfoAsLeader()
    {
        rank = RankInt("Leader");
        contributionPoints = 0;
        clanWarsCount = 0;
        clanWarsVictories = 0;
    }
    public void InitClanValues()
    {
        DisplayName();
        DisplayNotice();
        DisplayFlag();
        DisplayMembers();
        ClanResourcesObtained();
        ClanWarsObtained();
        ClanPointsObtained();
    }
    // ====== CREATE CLAN STUFF END ====== \\


    // ====== FLAG SHUFFLING ====== \\
    void ToggleFlagChangePopup(bool isOn)
    {
        changeFlagPopup.SetActive(isOn);
    }
    public void SetTempFlagNo(int currentFlagNo)
    {
        tempFlagNo = currentFlagNo;
    }
    void UpdateChangeFlagText()
    {
        beSeenAsText.text = beSeenAsText.text.Replace("<your_clan_here>", clanID);
    }
    public void FlagSwitch(int numberToMove)
    {
        // number to move will be (+1) or (-1),
        tempFlagNo = CheckIfOverflow(tempFlagNo + numberToMove);
        currentlySelectedFlag.sprite = ThisFlag(tempFlagNo);
        currentChosenFlag.sprite = ThisFlag(tempFlagNo);
    }
    public void ResetFlag()
    {
        tempFlagNo = 0;
        currentlySelectedFlag.sprite = ThisFlag(tempFlagNo);
    }
    int CheckIfOverflow(int tempFlagValue)
    {
        if (tempFlagValue < 0)
            return flagImages.Length - 1;
        if (tempFlagValue > flagImages.Length - 1)
            return 0;
        return tempFlagValue;
    }
    public Sprite ThisFlag(int toggleFlags)
    {
        return flagImages[toggleFlags];
    }
    void AttemptSaveFlagChangeInfo()
    {
        if (clanFlag != tempFlagNo)
        {
            GameSparksManager.Instance.UpdateClanFlag(this);
            ClanManager.Instance.SetPopupInteractable(false);
            ClanManager.Instance.InfoPopup(string.Format("Updating {0} Clan Flag", clanID));
        }
        else
        {
            ClanManager.Instance.InfoPopup(string.Format("{0} is already known to carry this Flag", clanID));
        }
    }
    public void SetClanCurrentFlag(int clanCurrentFlag)
    {
        // Response after setting the new FLAG value
        ToggleFlagChangePopup(false);
        ClanManager.Instance.SetPopupInteractable(true);
        SetNewClanFlag(clanCurrentFlag);
    }
    void SetNewClanFlag(int newClanFlag)
    {
        ClanManager.Instance.InfoPopup(string.Format("{0} will henceforth fly under this Flag", clanID));
        clanFlag = newClanFlag;
        DisplayFlag();
    }
    // ====== FLAG SHUFFLING END ====== \\


    // ====== JOIN CLAN STUFF ====== \\
    public void CreateClanInfoAsMember()
    {
        rank = RankInt("Member");
        contributionPoints = 0;
    }
    public void ReceiveNewClanMember(string memberID, string memberName)
    {
        CreateNewMemberOBJ(memberID, memberName);
        AddMemberList();
        RequestNewMemberInfo();
        DisplayMembers();
    }
    void CreateNewMemberOBJ(string memberID, string memberName)
    {
        if (memberID != GameSparksManager.playerId)
        {
            clanMembersID.Add(memberID);
            clanMembersName.Add(memberName);
            GameObject newMem = Instantiate(clanMemberOBJ, clanMemberScroll.position, Quaternion.identity, clanMemberScroll);
            clanMembers.Add(newMem.GetComponent<ClanMember>());
            clanMembers[clanMembers.Count - 1].clan = this;
            clanMembers[clanMembers.Count - 1].AddPopupListener();
            clanMembers[clanMembers.Count - 1].memberName = memberName;
            clanMembers[clanMembers.Count - 1].memberID = memberID;
        }
    }
    void AddMemberList()
    {
        clanMembersPortrait.Add("h0000");
        clanMembersReputation.Add(-100);
        clanMembersRankValue.Add(-100);
        clanMembersContributions.Add(-100);
        clanMembersBattles.Add(-100);
        clanMembersVictories.Add(-100);
        clanMembersLosses.Add(-100);
        clanMembersLastLogout.Add(-100);
    }
    void RequestNewMemberInfo()
    {
        GameSparksManager.Instance.GetPlayerClanInfo(clanMembers[clanMembers.Count - 1].memberID, clanMembers.Count - 1, this);
        GameSparksManager.Instance.GetClanMemberData(clanMembers[clanMembers.Count - 1].memberID, clanMembers.Count - 1, this);
        GameSparksManager.Instance.GetClanMemberBattleLog(clanMembers[clanMembers.Count - 1].memberID, clanMembers.Count - 1, this);
        GameSparksManager.Instance.GetClanMemberDetails(clanMembers[clanMembers.Count - 1].memberID, clanMembers.Count - 1, this);
        GameSparksManager.Instance.GetClanMembersLastLogout(clanMembers[clanMembers.Count - 1].memberID, clanMembers.Count - 1, this);
    }
    // ====== JOIN CLAN STUFF END ====== \\


    // ====== CLAN MEMBERS STUFF ====== \\
    public void CreateClanMembers()
    {
        CleanUpList();
        CreateClanMemberOBJ();
        GenerateNewMemberList();
        CreateMemberList();
        RequestClanMembersInfo();
    }
    void CleanUpList()
    {
        for (int x = 0; x < clanMembersID.Count; x++)
        {
            for (int y = 0; y < clanMembersID.Count; y++)
            {
                if (clanMembersID[x] == clanMembersID[y] & x != y)
                {
                    clanMembersID.RemoveAt(y);
                    break;
                }
            }
        }
    }
    public void ClearMemberList()
    {
        clanMembersID.Clear();
        clanMembersName.Clear();
    }
    void CreateClanMemberOBJ()
    {
        clanMembers = new List<ClanMember>();
        for (int x = 0; x < clanMembersID.Count; x++)
        {
            GameObject cMem = Instantiate(clanMemberOBJ, clanMemberScroll.position, Quaternion.identity, clanMemberScroll);
            clanMembers.Add(cMem.GetComponent<ClanMember>());
            clanMembers[x].clan = this;
            clanMembers[x].AddPopupListener();
            clanMembers[x].memberName = clanMembersName[x];
            clanMembers[x].memberID = clanMembersID[x];
        }
    }
    void GenerateNewMemberList()
    {
        clanMembersReputation.Clear();
        clanMembersRankValue.Clear();
        clanMembersContributions.Clear();
        clanMembersPortrait.Clear();
        clanMembersBattles.Clear();
        clanMembersVictories.Clear();
        clanMembersLosses.Clear();
        clanMembersLastLogout.Clear();

        distributionCounter = 0;
    }
    void CreateMemberList()
    {
        for (int x = 0; x < clanMembersID.Count; x++)
        {
            clanMembersReputation.Add(-100);
            clanMembersRankValue.Add(-100);
            clanMembersContributions.Add(-100);
            clanMembersBattles.Add(-100);
            clanMembersVictories.Add(-100);
            clanMembersLosses.Add(-100);
            clanMembersPortrait.Add("h0001");
            clanMembersLastLogout.Add(-100);
        }
    }
    void RequestClanMembersInfo()
    {
        for (int z = 0; z < clanMembersID.Count; z++)
        {
            GameSparksManager.Instance.GetPlayerClanInfo(clanMembers[z].memberID, z, this);
            GameSparksManager.Instance.GetClanMemberData(clanMembers[z].memberID, z, this);
            GameSparksManager.Instance.GetClanMemberBattleLog(clanMembers[z].memberID, z, this);
            GameSparksManager.Instance.GetClanMemberDetails(clanMembers[z].memberID, z, this);
            GameSparksManager.Instance.GetClanMembersLastLogout(clanMembers[z].memberID, z, this);
        }
    }
    public void InsertPlayerInfo(int playerQueue, int rankValue, int contributionPoints_)
    {
        clanMembersContributions[playerQueue] = contributionPoints_;
        clanMembersRankValue[playerQueue] = rankValue;

        if (clanMembers[playerQueue].memberID == GameSparksManager.playerId)
        {
            contributionPoints = contributionPoints_;
            rank = rankValue;
        }

        clanMembers[playerQueue].memberContribution = contributionPoints_;
        clanMembers[playerQueue].memberRankNo = rankValue;
        clanMembers[playerQueue].memberRank = RankType(rankValue);

        CheckMemberInfo(playerQueue);
    }
    public void InsertPlayerDataInfo(int playerQueue, int repValue, int memLv)
    {
        clanMembersReputation[playerQueue] = repValue;
        clanMembers[playerQueue].memberReputation = repValue;
        clanMembers[playerQueue].memberLv = memLv;

        CheckMemberInfo(playerQueue);
    }
    public void InsertPlayerInfo(int playerQueue, int battles, int losses, int victories)
    {
        clanMembersBattles[playerQueue] = battles;
        clanMembersLosses[playerQueue] = losses;
        clanMembersVictories[playerQueue] = victories;

        clanMembers[playerQueue].battles = battles;
        clanMembers[playerQueue].victories = victories;
        clanMembers[playerQueue].losses = losses;

        CheckMemberInfo(playerQueue);
    }
    public void InsertPlayerInfo(int playerQueue, string portrait)
    {
        clanMembersPortrait[playerQueue] = portrait;

        clanMembers[playerQueue].memberPortrait = portrait;
        CheckMemberInfo(playerQueue);

    }
    public void InsertPlayerLastLogout(int playerQueue, long lastLog)
    {
        clanMembersLastLogout[playerQueue] = lastLog;

        clanMembers[playerQueue].lastLogout = lastLog;

        CheckMemberInfo(playerQueue);
    }
    void CheckMemberInfo(int playerQueueNo)
    {
        if (clanMembersContributions[playerQueueNo] != -100 & clanMembersReputation[playerQueueNo] != -100
            & clanMembersBattles[playerQueueNo] != -100 & clanMembersPortrait[playerQueueNo] != "h0000"
            & clanMembersLastLogout[playerQueueNo] != -100)
        {
            distributionCounter += 1;

            if (distributionCounter == clanMembers.Count)
                DistributeClanMembersInfo();
        }
    }

    void DistributeClanMembersInfo()
    {
        for (int x = 0; x < clanMembers.Count; x++)
        {
            clanMembers[x].memberContribution = clanMembersContributions[x];
            clanMembers[x].memberRank = RankType(clanMembersRankValue[x]);
            clanMembers[x].memberReputation = clanMembersReputation[x];
            clanMembers[x].UpdateMemberInfo();
        }
        ClanManager.Instance.ClanPanelToggle(true);
        ClanManager.Instance.TogglePopup(false);

        UpdateUIElements();
        WithdrawGoldButtonCheck();
        EditClanMessageCheck();
    }

    public void UpdateClanPoints()
    {
        for (int x = 0; x < clanMembers.Count; x++)
        {
            if (clanMembers[x].memberID == GameSparksManager.playerId)
            {
                clanMembers[x].memberContribution = contributionPoints;
                clanMembers[x].UpdateMemberInfo();
                break;
            }
        }
    }

    // ====== CLAN MEMBERS END ====== \\


    // ====== UPDATE PLAYER UI STUFF ======= \\
    void UpdateUIElements()
    {
        UIManager.Instance.UpdatePlayerInfo();
        UIManager.Instance.UpdatePlayerTabInfo();
    }
    public void WithdrawGoldButtonCheck()
    {
        if (LeaderCheck(GameSparksManager.playerId) == true || OfficerCheck(rank) == true)
        {
            withdrawGoldButton.gameObject.SetActive(true);
            withdrawGoldButton.onClick.AddListener(WithdrawGoldInfo);
            withdrawGoldButton.onClick.AddListener(delegate { ToggleWithdrawGoldPopup(true); });
            attemptWithdraw.onClick.AddListener(AttemptWithdrawGold);
        }
        else withdrawGoldButton.gameObject.SetActive(false);
    }
    public void TransferLeadershipButtonCheck()
    {
        if (LeaderCheck(GameSparksManager.playerId) == true && memberOnDisplay != GameSparksManager.playerId)
        {
            transferLeadership.gameObject.SetActive(true);
            transferLeadership.onClick.AddListener(TransferLeadershipInfo);
            attemptTransferLeadership.onClick.AddListener(AttemptTransferLeadership);
        }
        else transferLeadership.gameObject.SetActive(false);
    }
    // ====== UPDATE PLAYER UI STUFF END ======= \\


    // ====== CLAN INFO STUFF ====== \\
    public void RequestClanInfos()
    {
        RequestSecondaryInfo();
        RequestClanResources();
        RequestClanPoints();
    }
    void RequestSecondaryInfo()
    {
        //One OBJ for each clan so only need to request once
        GameSparksManager.Instance.GetClanInfo(clanID, this);
    }
    void RequestClanResources()
    {
        // One of this OBJ so only required to request once from the cloud
        GameSparksManager.Instance.GetClanResources(clanID, this);
    }
    void RequestClanWarsCount()
    {
        // One of this OBJ so only required to request once from the cloud
        GameSparksManager.Instance.GetClanResources(clanID, this);
    }
    void RequestClanPoints()
    {
        // One of this OBJ so only required to request once from the cloud
        GameSparksManager.Instance.GetClanPoints(clanID, this);
    }
    public void SecondaryInfoObtained()
    {
        DisplayName();
        DisplayFlag();
        DisplayNotice();
        DisplayMembers();
        ClanWarsObtained();
        ToggleChangeFlagButton();

        RearrangeOBJs();
        EventHolderManager.Instance.UpdateLastXcaliburHolder();

        //GameSparksManager.Instance.GetClanWarsHistory(clanID, this);
        //GameSparksManager.Instance.GetOnGoingWars(clanID, this);
    }
    void DisplayName()
    {
        clanNameText.text = clanID;
    }
    void DisplayFlag()
    {
        clanCurrentFlag.sprite = ThisFlag(clanFlag);
    }
    void DisplayNotice()
    {
        clanNoticeText.text = clanNotice;
    }
    void DisplayMembers()
    {
        clanMembersText.text = ClanMembers();
    }
    public void ClanResourcesObtained()
    {
        clanGoldStorageText.text = ClanResources();
    }
    public void ClanWarsObtained()
    {
        clanWarsText.text = ClanWars();
    }
    public void ClanPointsObtained()
    {
        clanPointsText.text = ClanPoints();
    }
    void ToggleChangeFlagButton()
    {
        if (LeaderCheck(GameSparksManager.playerId) == true || OfficerCheck(rank) == true)
            toggleChangeFlagPanel.gameObject.SetActive(true);
        else
            toggleChangeFlagPanel.gameObject.SetActive(false);
    }


    void EditClanMessageCheck()
    {
        if (LeaderCheck(GameSparksManager.playerId) == true || OfficerCheck(rank) == true)
        {
            setClanMessageButton.gameObject.SetActive(true);
            setClanMessageButton.onClick.AddListener(delegate { ToggleClanMessageChangePopup(true); });
            setClanMessageButton.onClick.AddListener(SetEmptyInputField);

            closeSetMessageButton.onClick.AddListener(CloseClanMessageChange);

            attemptChangeMessageButton.onClick.AddListener(SaveClanMessage);
        }
        else
        {
            setClanMessageButton.gameObject.SetActive(false);
        }
    }
    void CloseClanMessageChange()
    {
        ToggleClanMessageChangePopup(false);
        SetEmptyInputField();
    }
    public void ClanMessageCount(string messageText)
    {
        messageCountText.text = string.Format("{0} / 150", messageText.Length);
    }
    void ToggleClanMessageChangePopup(bool toShow)
    {
        changeNoticePopup.SetActive(toShow);
    }
    void SetEmptyInputField()
    {
        clanNoticeInputField.text = "";
    }
    void SaveClanMessage()
    {
        GameSparksManager.Instance.SaveClanMessage(clanNoticeInputField.text, this);
    }
    void GetNewMessage()
    {
        GameSparksManager.Instance.GetClanMessage(this);
    }
    public void MessageUpdateSuccess()
    {
        //clanNoticeText.text = clanNoticeInputField.text.Trim();
        ToggleClanMessageChangePopup(false);
        SetEmptyInputField();
        ClanManager.Instance.InfoPopup("Message Updated");
    }



    string ClanMembers()
    {
        return string.Format("{0} / 50", clanMembersID.Count);
    }
    string ClanResources()
    {
        return string.Format("Resources Available : \n {0:n0} / {1:n0}", clanAvailableGold, clanAvailableGoldLimit);
    }
    string ClanWars()
    {
        return string.Format("Clan Wars : \n {0:n0} / {1:n0}", clanWarsVictories, clanWarsCount);
    }
    string ClanPoints()
    {
        return string.Format("Clan Points : \n {0:n0}", clanPoints);
    }
    // ====== CLAN INFO END ======\\


    // ====== DISPLAY MEMBER INFO POPUP ====== \\
    public void MemberDisplayPopup(ClanMember clanMember)
    {
        FillInMemberDetails(clanMember);

        // IF LEADER
        if (rank == 0)
        {
            ToggleClanMembersButton(LeaderCheck(GameSparksManager.playerId));
        }
        else if (rank == 1)
        {
            if (memberOnDisplay != GameSparksManager.playerId)
                ToggleClanMembersButtonForOfficer(OfficerCheck(rank));
            else
            {
                ToggleSendGoldButton(false);
                TogglePromoteButton(false);
                ToggleDemoteButton(false);
                ToggleRemoveButton(false);
            }
        }
        else
        {
            ToggleSendGoldButton(false);
            TogglePromoteButton(false);
            ToggleRemoveButton(false);
        }

        ToggleTransferLeadershipButton();
        TogglePopup(true);
    }
    void FillInMemberDetails(ClanMember memberToDisplay)
    {
        memberNameText.text = "Name : " + memberToDisplay.memberName;
        memberRankText.text = "Rank : " + memberToDisplay.memberRank;
        memberReputationText.text = "Reputation : " + string.Format("{0:n0}", memberToDisplay.memberReputation);
        memberContributionText.text = "Contribution : " + string.Format("{0:n0}", memberToDisplay.memberContribution);
        battlesText.text = "Victories : " + memberToDisplay.VictoryCount();
        heroesUnlockedText.text = "Heroes : " + memberToDisplay.HeroesUnlockedCount();
        memberToDisplay.DisplayPotrait();
        if (memberOnDisplay == "")
        {
            // If the member on display isn't selected yet
            memberOnDisplay = memberToDisplay.memberID;
        }
        else if (memberOnDisplay != memberToDisplay.memberID)
        {
            previousMemberOnDisplay = memberOnDisplay;
            memberOnDisplay = memberToDisplay.memberID;
            ResetRetrievedValues();
        }
        memberOnDisplayName = memberToDisplay.memberName;
        memberOnDisplayRank = memberToDisplay.memberRank;
        memberOnDisplayRankValue = memberToDisplay.memberRankNo;
        memberOnDisplayContribution = memberToDisplay.memberContribution;
        memberLastSeenText.text = "Last seen : " + memberToDisplay.LastSeen();
    }
    public void TogglePopup(bool isOn)
    {
        memberInfoPopup.SetActive(isOn);
    }
    void ToggleClanMembersButton(bool isLeader)
    {
        ToggleSendGoldButton(isLeader);
        if (memberOnDisplay == leaderID)
            isLeader = false;
        TogglePromoteButton(isLeader);
        ToggleDemoteButton(isLeader);
        ToggleRemoveButton(isLeader);
    }
    void ToggleClanMembersButtonForOfficer(bool isOfficer)
    {
        // if the Member on display is also an officer
        if (memberOnDisplayRankValue > rank)
        {
            ToggleSendGoldButton(isOfficer);
            TogglePromoteButton(isOfficer);
            ToggleRemoveButton(isOfficer);
        }
        else
        {
            ToggleSendGoldButton(false);
            TogglePromoteButton(false);
            ToggleRemoveButton(false);
        }
    }
    void TogglePromoteButton(bool isLeader)
    {
        if (MemberCanBePromoted())
            promoteMember_Button.SetActive(isLeader);
        else
            promoteMember_Button.SetActive(false);
    }
    void ToggleDemoteButton(bool isLeader)
    {
        if (MemberCanBeDemoted())
            demoteMembner_Button.SetActive(isLeader);
        else
            demoteMembner_Button.SetActive(false);
    }
    bool MemberCanBePromoted()
    {
        if (memberOnDisplayRankValue >= 2)
            return true;
        return false;
    }
    bool MemberCanBeDemoted()
    {
        if (memberOnDisplayRankValue ==1)
            return true;
        return false;
    }
    void ToggleRemoveButton(bool isLeader)
    {
        removeMember_Button.SetActive(isLeader);
    }
    void ToggleSendGoldButton(bool isLeader)
    {
        sendGold_Button.SetActive(isLeader);
        if (LeaderCheck(memberOnDisplay))
            sendGold_Button.transform.GetComponentInChildren<Text>().text = "Withdraw";
        else
            sendGold_Button.transform.GetComponentInChildren<Text>().text = "Send";
    }
    void ToggleTransferLeadershipButton()
    {
        TransferLeadershipButtonCheck(); 
    }
    // ====== DISPLAY MEMBER END ======= \\


    // ======= STORE RSS TO CLAN ======= \\
    public void AttemptDeposit()
    {
        ClanManager.Instance.SetPopupInteractable(false);
        if (ToStoreAmount() <= 0)
        {
            ClanManager.Instance.InfoPopup("Minimum storage of 1 GOLD");
            ClanManager.Instance.SetPopupInteractable(true);
            return;
        }
        if (Player.gold < ToStoreAmount())
        {
            ClanManager.Instance.InfoPopup("You do not have that much");
            ClanManager.Instance.SetPopupInteractable(true);
            return;
        }
        ClanManager.Instance.InfoPopup("Storing GOLD");
        RetrieveLastestValue(0);
    }
    // === STORAGE CHECKS === \\
    public int ToStoreAmount()
    {

        if (goldStorageValue.text != "")
        {
            return int.Parse(goldStorageValue.text);
        }
        return 0;
    }
    int StorageSpaceAvailable()
    {
        return clanAvailableGoldLimit - clanAvailableGold;
    }
    void StorageValueUpdate(string newValue)
    {
        if (newValue != "")
        {
            if (!newValue.Contains("-"))
            {
                int amount = int.Parse(newValue);
                goldLeftAmount.text = string.Format("( {0} / {1:n0} )", goldStorgeAmount(amount), Player.gold);
            }
            else 
            {
                if (newValue == "-")
                {
                    goldStorageValue.text = "";
                    newValue = "";
                    return;
                }
                newValue = newValue.Remove(0, 1);

                goldStorageValue.text = newValue;
                int amount = int.Parse(newValue);
                goldLeftAmount.text = string.Format("( {0} / {1:n0} )", goldStorgeAmount(amount), Player.gold);
            }
        }
    }
    string goldStorgeAmount(int stringAmt)
    {
        if (stringAmt > Player.gold)
            return string.Format("<color=#FF0000>{0:n0}</color>", stringAmt);
        return string.Format("<color=#00FF00>{0:n0}</color>", stringAmt);
    }
    public void UpdateDone(int goldInTreasury, int goldLimit)
    {
        ClanResourcesObtained();
    }
    public void ToggleStoreGoldPopup(bool isOn)
    {
        goldLeftAmount.text = string.Format("( 0 / {0:n0} )", Player.gold);
        storeGoldPopup.SetActive(isOn);
        goldStorageValue.text = "";
    }
    public void RecieveNotificationOfTreasuryUpdate(string memberID, int difference, int goldInTreasury, string senderID)
    {
        string formatDifference = string.Format("{0:n0}", difference);
        ClanManager.Instance.InfoPopup(MemberName(memberID) + " just deposited " + formatDifference + " GOLD into the treasury");
        clanAvailableGold += difference;
        ClanResourcesObtained();

        // FOR THE PERSON WHO DEPOSIT
        if (memberID == GameSparksManager.playerId)
        {
            Player.Instance.SpendGold(difference);
            ClanManager.Instance.SetPopupInteractable(true);
            ClanManager.Instance.InfoPopup("GOLD deposit successful");
            DataManager.Instance.PrepareDataForSparksPushing();
            GameSparksManager.Instance.SavePlayerExpendables(GameSparksManager.playerId);
        }
    }
    // ======= STORE RSS TO CLAN END ======= \\


    // ======= SEND GOLD TO MEMBER ====== \\
    void ResetRetrievedValues()
    {
        memberPossessedGold = -100;
        memberStorageGoldLimit = -100;
    }
    void RetrieveSelectedPlayerRequiredInfos()
    {
        if (memberPossessedGold == -100 & memberStorageGoldLimit == -100)
        {
            GameSparksManager.Instance.GetClanMemberGoldInfo(memberOnDisplay, this);
            GameSparksManager.Instance.GetClanMemberTreasury(memberOnDisplay, this);
        }
        else
            InputDetails();
    }
    public void MemberGoldRetrieved(int goldRetrieved)
    {
        memberPossessedGold = goldRetrieved;
        RetrivalCheck();
    }
    public void MemberGoldLimitRetrieved(long goldLimitRetrieved)
    {
        memberStorageGoldLimit = goldLimitRetrieved;
        RetrivalCheck();
    }
    void RetrivalCheck()
    {
        if (memberPossessedGold != -100 & memberStorageGoldLimit != -100)
            InputDetails();
    }
    void InputDetails()
    {
        goldSpaceAvailableDisplay.text = string.Format("<size=40>{2} has in their possessions : </size> \n {0:n0} / {1:n0} ({3}%)",
                                                       memberPossessedGold, memberStorageGoldLimit, memberOnDisplayName, calculatePerc(0));
        ToggleSendGoldPopup(true);
    }

    string calculatePerc(int type)
    {
        if (type == 0)
        {
            float perc = (memberPossessedGold * 100) / memberStorageGoldLimit;
            return string.Format("{0:0.00}", perc);
        }
        if (type == 1)
        {
            float perc = (Player.gold * 100) / Player.treasury_Storage_Cap;
            return string.Format("{0:0.00}", perc);
        }
        return "<Type_not_found>";
    }
    public void ToggleSendGoldPopup(bool isOn)
    {
        if (LeaderCheck(memberOnDisplay))
            sendGoldText.text = "Withdraw";
        else
            sendGoldText.text = "Send";
        sendGoldPopup.SetActive(isOn);
        sendGoldAmount.text = "";
    }
    public int ToSendAmount()
    {
        if (sendGoldAmount.text != "")
            return int.Parse(sendGoldAmount.text);
        return 0;
    }
    public void AttemptSend()
    {
        if (ToSendAmount() <= 0)
        {
            ClanManager.Instance.InfoPopup("Minimum withdrawal of 1 GOLD");
            return;
        }
        if (ToSendAmount() > memberStorageGoldLimit - memberPossessedGold)
        {
            ClanManager.Instance.InfoPopup(memberOnDisplayName + " unable to store that much GOLD");
            return;
        }
        if (clanAvailableGold < ToSendAmount())
        {
            ClanManager.Instance.InfoPopup("Insufficient CLAN funds");
            return;
        }
        RetrieveLastestValue(1);
    }
    // ====== RECEIVE GOLD MEMBER ======= \\
    public void RecieveNotificationOfGoldSent(string memberID, int difference, int goldInTreasury, string senderID)
    {
        clanAvailableGold -= difference;
        ClanResourcesObtained();
        // memberID refers to the the person who withdrew the GOLD ,     sender refers to the leader or the officer

        if (senderID == GameSparksManager.playerId || memberID == GameSparksManager.playerId)
            UpdateMyGold(difference, memberID, senderID);
    }
    void UpdateMyGold(int difference, string memberID, string senderID)
    {
        // member refers to the person who is getting the gold, sender is the officer/leader

        // if i am the one recieving the GOLD
        if (memberID == senderID)
        {
            ClanManager.Instance.InfoPopup(string.Format("{0:n0} GOLD withdrawal successful.", difference));
            AlterMyGoldValue(difference);
        }
        else
        {
            if (senderID == GameSparksManager.playerId)
            {
                ClanManager.Instance.InfoPopup(string.Format("GOLD send successful."));
            }
            else if (memberID == GameSparksManager.playerId)
            {
                ClanManager.Instance.InfoPopup(string.Format("{0} sent you {1:n0} GOLD.", MemberName(senderID),difference));
            }
            else
            {
                ClanManager.Instance.InfoPopup(string.Format("{0} sent {2} {1:n0} GOLD.", MemberName(senderID), difference, MemberName(memberID)));
            }
        }
        ClanManager.Instance.SetPopupInteractable(true);
    }
    void AlterMyGoldValue(int difference)
    {
        Player.gold += difference;
        Player.Instance.LimitGold();
        GameSparksManager.Instance.SavePlayerExpendables(GameSparksManager.playerId);
        UIManager.Instance.UpdateCurrencyInfo();
    }
    // ======= SEND GOLD TO MEMBER END ====== \\

    // ====== Withdraw GOLD ====== \\
    void WithdrawGoldInfo()
    {
        treasurySpaceAvailableDisplay.text = string.Format("<size=40>You currently have : </size> \n {0:n0} / {1:n0} ({3}%)",
                                                       Player.gold, Player.treasury_Storage_Cap, Player.playerName, calculatePerc(1));
    }
    public void ToggleWithdrawGoldPopup(bool isOn)
    {
        withdrawGoldPopup.SetActive(isOn);
        withdrawGoldAmount.text = "";
    }
    bool WithdrawCheck()
    {
        if (ToWithdrawAmount() > 0)
        {
            if (Player.treasury_Storage_Cap - Player.gold < ToWithdrawAmount())
            {
                // if insufficient space in the treasury
                ClanManager.Instance.InfoPopup("Insufficient space in Treasury");
                ClanManager.Instance.SetPopupInteractable(true);
                return false;
            }
            if (clanAvailableGold < ToWithdrawAmount())
            {
                // if insufficient gold in clan storage
                ClanManager.Instance.InfoPopup("Insufficient GOLD in CLAN storage");
                ClanManager.Instance.SetPopupInteractable(true);
                return false;
            }
        }
        else
        {
            ClanManager.Instance.InfoPopup("Minimum withdrawal of 1 GOLD");
            ClanManager.Instance.SetPopupInteractable(true);
            return false;
        }
        return true;
    }
    void WithdrawValueParseUpdateDisplay(string newValue)
    { 
        if (newValue != "")
        {
            if (newValue.Contains("-"))
            { 
                if (newValue == "-")
                {
                    withdrawGoldAmount.text = "";
                    return;
                }
                newValue = newValue.Remove(0, 1);
                withdrawGoldAmount.text = newValue;
            }
        }
    }
    int ToWithdrawAmount()
    {
        if (withdrawGoldAmount.text != "")
        {
            return int.Parse(withdrawGoldAmount.text);
        }
        return 0;
    }
    public void AttemptWithdrawGold()
    {
        ClanManager.Instance.SetPopupInteractable(false);
        if (WithdrawCheck())
        {
            RetrieveLastestValue(2);
        }
    }
    // ====== Withdraw GOLD ====== \\



    //======== GOLD VALUE ALTERATION AREA ======== \\
    public void RetrieveLastestValue(int changeType)
    {
        GameSparksManager.Instance.CheckClanResource(clanID, changeType, this);
    }
    public void ChangeClanGoldValue(int changeType)
    {
        // changeType 0 == deposit       ||||     changeType1 == leader send     ||||    changeType2 == playerWithdrawal
        switch (changeType)
        {
            case 0:
                if (ToStoreAmount() > 0 && StorageSpaceAvailable() > 0)
                {
                    int valueToBeStored_ = 0;

                    if (ToStoreAmount() > StorageSpaceAvailable())
                        valueToBeStored_ = StorageSpaceAvailable();
                    else
                        valueToBeStored_ = ToStoreAmount();

                    GameSparksManager.Instance.SaveClanResources(valueToBeStored_, NewStorageAmount(valueToBeStored_), changeType, this);
                }
                else
                {
                    // IF not enough space
                    ClanManager.Instance.InfoPopup("Insufficient Storage space");
                    ClanManager.Instance.SetPopupInteractable(true);
                }
                break;
            case 1:
                if (ToSendAmount() < clanAvailableGold & ToSendAmount() <= memberStorageGoldLimit - memberPossessedGold)
                    GameSparksManager.Instance.SaveClanResources(ToSendAmount(), NewSentAmount(), changeType, this);
                break;

            case 2:
                GameSparksManager.Instance.SaveClanResources(ToWithdrawAmount(), NewWithdrawAmount(), changeType, this);
                break;
        }
    }
    int NewStorageAmount(int valueToBeStored_)
    {
        int availGold = clanAvailableGold + valueToBeStored_;
        if (valueToBeStored_ == clanAvailableGoldLimit)
            availGold = clanAvailableGoldLimit;
        return availGold;
    }
    int NewSentAmount()
    {
        return clanAvailableGold - ToSendAmount();
    }
    int NewWithdrawAmount()
    {
        return clanAvailableGold - ToWithdrawAmount();
    }
    //======== GOLD VALUE ALTERATION AREA END ======== \\




    // ======== MEMBER PROMOTION ======== \\
    public void DisplayPromotionInfo()
    {
        PromotionText();
        TogglePromotionPopup(true);
    }
    void PromotionText()
    {
        promoteMemberText.text = string.Format("{0} \n {1} TO {2}",
                                              memberOnDisplayName, memberOnDisplayRank, RankType(RankInt(memberOnDisplayRank) - 1));
    }
    void TogglePromotionPopup(bool isOn)
    {
        promotePopup.SetActive(isOn);
    }
    bool PromotionApproval()
    {
        int officerCount = 0;
        for (int x = 0; x < clanMembersID.Count; x++)
        {
            if (clanMembers[x].memberRankNo == 1)
            {
                officerCount += 1;
            }
        }

        if (officerCount >= maxOfficerCount)
            return false;
        return true;
    }
    void AttemptPromote()
    {
        //GS METHOD
        if (PromotionApproval())
            GameSparksManager.Instance.PromoteClanMember
                         (memberOnDisplay, memberOnDisplayContribution, RankInt(memberOnDisplayRank) - 1, this);
        else
            ClanManager.Instance.InfoPopup("Maximum number of Officers reached");
    }
    public void CompletePromotion(string memberID)
    {
        TogglePromotionPopup(false);
        TogglePopup(false);
        UpdateMemberInfo(memberID);

        RearrangeOBJs();
    }
    void UpdateMemberInfo(string memberID)
    {
        for (int x = 0; x < clanMembers.Count; x++)
        {
            if (clanMembers[x].memberID == memberID)
            {
                clanMembersRankValue[x] = 1;
                clanMembers[x].memberRankNo = 1;
                clanMembers[x].memberRank = RankType(clanMembersRankValue[x]);
                clanMembers[x].UpdateMemberInfo();
                break;
            }
        }
    }
    // ====== PROMOTED MEMBER ======= \\
    public void RecievePromotionNotification(string memberID)
    {
        UpdateMemberInfo(memberID);
        PromoteMe(memberID);
        WithdrawGoldButtonCheck();
    }
    void PromoteMe(string memberID)
    {
        if (GameSparksManager.playerId == memberID)
        {
            // handle event here if the promotion is you
            rank -= 1;
            ClanManager.Instance.InfoPopup("Congratulations! You just got promoted!");
            UpdateMyClanInfo();
        }
    }
    void UpdateMyClanInfo()
    {
        GameSparksManager.Instance.SavePlayerClanInfo(GameSparksManager.playerId);
    }
    // ======== MEMBER PROMOTION END ======== \\



    // ======= MEMBER DEMOTION ======== \\
    public void DisplayDemotionInfo()
    {
        DemotionText();
        ToggleDemotionPopup(true);
    }
    void DemotionText()
    {
        demoteMemberText.text = string.Format("{0} \n {1} TO {2}",
                                              memberOnDisplayName, memberOnDisplayRank, RankType(RankInt(memberOnDisplayRank) + 1));
    }
    void ToggleDemotionPopup(bool isOn)
    {
        demotePopup.SetActive(isOn);
    }
    void AttemptDemote()
    {
        //GS METHOD
        GameSparksManager.Instance.DemoteClanMember
                         (memberOnDisplay, memberOnDisplayContribution, RankInt(memberOnDisplayRank) + 1, this);
    }
    public void CompleteDemotion(string memberID)
    {
        ToggleDemotionPopup(false);
        TogglePopup(false);
        UpdateDemotion(memberID);

        RearrangeOBJs();
    }
    // ====== DEMOTED MEMBER ====== \\
    public void RecieveDemotionNotification(string memberID)
    {
        UpdateDemotion(memberID);
        DemoteMe(memberID);
    }
    void DemoteMe(string memberID)
    {
        if (GameSparksManager.playerId == memberID)
        {
            // handle event here if the promotion is you
            rank = 2;
            ClanManager.Instance.InfoPopup("Congratulations! You just got demoted!");
            UpdateMyClanInfo();
        }
    }
    void UpdateDemotion(string memberID)
    {
        for (int x = 0; x < clanMembers.Count; x++)
        {
            if (clanMembers[x].memberID == memberID)
            {
                clanMembersRankValue[x] = 2;
                clanMembers[x].memberRankNo = 2;
                clanMembers[x].memberRank = RankType(clanMembersRankValue[x]);
                clanMembers[x].UpdateMemberInfo();
                break;
            }
        }
    }



    // ======= LEADERSHIP TRANSFER ======= \\
    void TransferLeadershipInfo()
    {
        LeadershipTransferText();
        ToggleLeadershipTransferPopup(true);
    }
    void LeadershipTransferText()
    {
        transferLeaderText.text = string.Format("Transfer {0} ownership to {1}?", clanID, memberOnDisplayName);
    }
    public void ToggleLeadershipTransferPopup(bool isOn)
    {
        transferLeaderPopup.SetActive(isOn);
    }
    void AttemptTransferLeadership()
    {
        //GS METHOD
        GameSparksManager.Instance.TransferLeadership(memberOnDisplay, this);
    }


    void LeadershipChanged(string newLeaderID)
    {
        ClanManager.Instance.InfoPopup("Clan Leader has changed");

        for (int x = 0; x < clanMembers.Count; x++)
        {
            if (clanMembers[x].memberID == newLeaderID)
            {
                clanMembersRankValue[x] = 0;
                clanMembers[x].memberRankNo = 0;
                clanMembers[x].memberRank = RankType(clanMembersRankValue[x]);
                clanMembers[x].UpdateMemberInfo();
            }
            if (clanMembers[x].memberID == leaderID)
            {
                clanMembersRankValue[x] = 1;
                clanMembers[x].memberRankNo = 1;
                clanMembers[x].memberRank = RankType(clanMembersRankValue[x]);
                clanMembers[x].UpdateMemberInfo();
            }
        }

        leaderID = newLeaderID;
        if (newLeaderID == GameSparksManager.playerId)
            rank = 0;

        sendGoldButton.gameObject.SetActive(LeaderCheck(GameSparksManager.playerId));
        withdrawGoldButton.gameObject.SetActive(LeaderCheck(GameSparksManager.playerId));
        toggleChangeFlagPanel.gameObject.SetActive(LeaderCheck(GameSparksManager.playerId));
        withdrawGoldButton.gameObject.SetActive(LeaderCheck(GameSparksManager.playerId));

        RearrangeOBJs();

        // those affected are the Promoted Memeber and the leader
    }


    // ====== REMOVE MEMBER ======= \\
    public void DisplayRemoveConfirmation()
    {
        ConfimationText();
        ToggleRemoveMemberPopup(true);
    }
    void ConfimationText()
    {
        removeMemberText.text = string.Format("{0} \n {1}", memberOnDisplayName, memberOnDisplayRank);
    }
    void ToggleRemoveMemberPopup(bool isOn)
    {
        removeMemberPopup.SetActive(isOn);
    }
    void AttemptRemove()
    {
        // trigger GS method
        GameSparksManager.Instance.RemoveMemberNotification(clanID, memberOnDisplay, memberOnDisplayName, "kicked", this);
    }
    public void CompleteRemoval(string memberID)
    {
        ToggleRemoveMemberPopup(false);
        RemoveTeamMemberOBJ(memberID);
        ClearRemainingInfo();
        DisplayMembers();

        RearrangeOBJs();
    }
    void RemoveTeamMemberOBJ(string memberID)
    {
        for (int x = 0; x < clanMembers.Count; x++)
        {
            if (clanMembers[x].memberID == memberID)
            {
                clanMembersID.RemoveAt(x);
                clanMembersName.RemoveAt(x);
                clanMembersReputation.RemoveAt(x);
                clanMembersRankValue.RemoveAt(x);
                clanMembersContributions.RemoveAt(x);
                clanMembersBattles.RemoveAt(x);
                clanMembersVictories.RemoveAt(x);
                clanMembersLosses.RemoveAt(x);
                clanMembersPortrait.RemoveAt(x);
                clanMembersLastLogout.RemoveAt(x);
                clanMembers[x].DestroyThis();
                clanMembers.RemoveAt(x);
                break;
            }
        }
    }
    void ClearRemainingInfo()
    {
        TogglePopup(false);
        memberNameText.text = "";
        memberRankText.text = "";
        memberReputationText.text = "";
        memberContributionText.text = "";
        battlesText.text = "";
        heroesUnlockedText.text = "";
        potraitDisplay.sprite = PotraitManager.Instance.PotraitsHolder[0].potraitImage;
        memberOnDisplay = "";
        previousMemberOnDisplay = memberOnDisplay;
        memberLastSeenText.text = "";
    }
    // ====== MEMBER REMOVE NOTIFICATION ====== \\
    public void MemberRemoved(string memberID, string memberName)
    {
        DisplayRemovedMember(memberID);
        CheckIfIWasRemoved(memberID);
    }
    void DisplayRemovedMember(string memberID)
    {
        if (memberID == GameSparksManager.playerId)
            ClanManager.Instance.InfoPopup("You are no longer part of the CLAN");
        else
        {
            ClanManager.Instance.InfoPopup(MemberName(memberID) + " has left the CLAN");
            distributionCounter -= 1;
        }
    }
    void CheckIfIWasRemoved(string memberID)
    {
        if (memberID == GameSparksManager.playerId)
            LeftClan();
        else
            RemoveMemberInfo(memberID);
    }
    void RemoveMemberInfo(string memberID)
    {
        for (int x = 0; x < clanMembers.Count; x++)
        {
            if (clanMembers[x].memberID == memberID)
            {
                clanMembersID.RemoveAt(x);
                clanMembersName.RemoveAt(x);
                clanMembersReputation.RemoveAt(x);
                clanMembersRankValue.RemoveAt(x);
                clanMembersContributions.RemoveAt(x);
                clanMembersBattles.RemoveAt(x);
                clanMembersVictories.RemoveAt(x);
                clanMembersLosses.RemoveAt(x);
                clanMembersPortrait.RemoveAt(x);
                clanMembersLastLogout.RemoveAt(x);
                clanMembers[x].DestroyThis();
                clanMembers.RemoveAt(x);
                break;
            }
        }
    }
    // ====== REMOVE MEMBER END ======= \\



    // ===== REARRANGE CLAN MEMBERS OBJs ===== \\
    void RearrangeOBJs()
    {
        for (int x = 0; x < clanMembersID.Count; x++)
        {
            for (int y = 0; y < clanMembers.Count; y++)
            {
                // this is to put the leader as the first child
                if (clanMembersID[x] == clanMembers[y].memberID   &&   clanMembers[y].memberRankNo == 0)
                {
                    clanMembers[y].transform.SetAsFirstSibling();
                }
                if (clanMembersID[x] == clanMembers[y].memberID   &&   clanMembers[y].memberRankNo == 1)
                {
                    clanMembers[y].transform.SetSiblingIndex(1);
                }
            }
        }
    }



    // ====== CLAN WARS ====== \\
    void ToggleClanWarsPanel()
    {
        clanWarsPanel.SetActive(true);
    }
    void ToggleNewWarsPanel(bool isActive)
    {
        newWarsPanel.SetActive(isActive);
    }
    void ToggleOngoingWarsPanel(bool isActive)
    {
        ongoingWarsPanel.SetActive(isActive);
    }
    void ToggleWarHistoryPanel(bool isActive)
    {
        warHistoryPanel.SetActive(isActive);
    }
    void AttemptSearchTargetClans()
    {
        ClearTargetClanList();
        LoadAvailableClans();
        ClearTargeClanOBJs();
    }
    void ClearTargetClanList()
    {
        targetClanNameList.Clear();
        targetClanLeadersList.Clear();
        targetClanPointList.Clear();
        targetClanBattleCountList.Clear();
        targetClanBattleVictoriesList.Clear();
    }
    void ClearTargeClanOBJs()
    {
        for (int x = 0; x < targetClansLoaded.Count; x++)
        {
            targetClansLoaded[x].DestroyThis();
        }
        targetClansLoaded.Clear();
    }
    void LoadAvailableClans()
    {
        GameSparksManager.Instance.ObtainPotentialClanemies(this);
    }

    public void RetrieveClanemiesInfo()
    {
        CreateNewList();
        CreatClanemyOBJ();
        InsertClanInfoToOBJ();

        for (int x = 0; x < targetClanNameList.Count; x++)
        {
            GameSparksManager.Instance.GetClanemyClanInfo(targetClanNameList[x], x, this);
            GameSparksManager.Instance.GetClanemyClanPoints(targetClanNameList[x], x, this);
            GameSparksManager.Instance.GetClanemyMemebers(targetClanNameList[x], x, this);
        }
    }
    public bool TargetClanIDCheck(string clanID)
    {
        for (int x = 0; x < ongoingWarTargetClanID.Count; x++)
        {
            if (clanID == ongoingWarTargetClanID[x])
                return true;
        }
        return false;
    }
    void CreateNewList()
    {
        for (int x = 0; x < targetClanNameList.Count; x++)
        {
            targetClanPointList.Add(-1);
            targetClanBattleCountList.Add(-1);
            targetClanBattleVictoriesList.Add(-1);
        }
    }
    void CreatClanemyOBJ()
    {
        for (int x = 0; x < targetClanNameList.Count; x++)
        {
            GameObject targOBJ = Instantiate(targetClanOBJ, newWarsLoadedEnemyPanel.position, Quaternion.identity, newWarsLoadedEnemyPanel);
            TargetClan tc = targOBJ.GetComponent<TargetClan>();
            targetClansLoaded.Add(tc);
        }
    }
    void InsertClanInfoToOBJ()
    {
        for (int x = 0; x < targetClanNameList.Count; x++)
        {
            targetClansLoaded[x].clanName = targetClanNameList[x];
            targetClansLoaded[x].clanLeaderName = targetClanLeadersList[x];
            targetClansLoaded[x].clan = this;
        }
    }
    public void StoreRetrievedClanemiesClanInfo(int clanQueueInt, string clanIcon, int warCount, int victoryCount)
    {
        targetClansLoaded[clanQueueInt].clanIcon = clanIcon;
        targetClansLoaded[clanQueueInt].battleCount = warCount;
        targetClansLoaded[clanQueueInt].victoryCount = victoryCount;
        targetClansLoaded[clanQueueInt].UpdateDisplay();
    }
    public void StoreRetrievedClanemiesClanPoints(int clanQueueInt, int clanPoints)
    {
        targetClansLoaded[clanQueueInt].points = clanPoints;
        targetClansLoaded[clanQueueInt].UpdateDisplay();
    }
    public void StoreRetrievedClanemiesClanMembers(int clanQueueInt, List<string> memNameList, List<string> memIDList)
    {
        targetClansLoaded[clanQueueInt].clanMembersName = memNameList;
        targetClansLoaded[clanQueueInt].clanMembersID = memIDList;
        targetClansLoaded[clanQueueInt].UpdateDisplay();
    }

    // ==== UPDATING CLAN MEMBERS ON WARS ==== \\
    void AttemptRetrieveClanWars()
    {
        Invoke("InvokedRetrieval", 1f);
    }
    void InvokedRetrieval()
    {
        // Notify Clan member of the ongoing war
        ClanManager.Instance.InfoPopup("New War Declared!");
        // Remove ongoing wars OBJ first
        RemoveOngoingWarsOBJs();
        // Remove any info of all ongoing wars
        RemoveAllOngoingWarsInfo();
        // Invoke the Ongoing Wars Retrieval to ensure the info is iin the cloud already
        //GameSparksManager.Instance.GetOnGoingWars(clanID, this);
    }
    void InvokeRetrieve()
    {
        //GameSparksManager.Instance.GetOnGoingWars(clanID, this);
    }
    void CheckIfDeclaredWarTargetIsLoaded()
    {
        if (targetClansLoaded.Count > 0)
        {
            for (int x = 0; x < targetClansLoaded.Count; x++)
            {
                for (int y = 0; y < ongoingWarTargetClanID.Count; y++)
                {
                    if (ongoingWarTargetClanID[y] == targetClansLoaded[x].clanName)
                    {
                        // IF there is a Target clan of the same name
                        RemoveChallengedTarget(targetClansLoaded[y]);
                        break;
                    }
                }
            }
        }
    }
    void RemoveOngoingWarsOBJs()
    {
        for (int x = 0; x < ongoingWarsLoaded.Count; x++)
        {
            ongoingWarsLoaded[x].gameObject.SetActive(false);
            ongoingWarsLoaded[x].RemoveMe();
        }
    }
    void RemoveAllOngoingWarsInfo()
    {
        ongoingWarTargetClanID.Clear();
        ongoingWarTargetClanLeaderID.Clear();
        ongoingWarsTargetClanLeaderName.Clear();
        ongoingWarsTargetClanMembersID.Clear();
        ongoingWarsTargetClanMembersName.Clear();
        ongoingWarClanPoints.Clear();
        ongoingWarClanBattleCountList.Clear();
        ongoingWarClanBattleVictories.Clear();

        ongoingWarClanMemberParticipation.Clear();
        ongoingWarTargetClanMemberParticipation.Clear();

        ongoingWarsLoaded.Clear();
    }

    // ==== RETRIEVE WARS ==== \\
    void GetOngoingWars()
    {
        // Remove the actual OBJ in the menu
        RemoveOngoingWarsOBJs();
        // Remove whatever ongoing wars info there are
        RemoveAllOngoingWarsInfo();
        // And then attempt to retrieve the required info
        //GameSparksManager.Instance.GetOnGoingWars(clanID, this);
    }

    //==== Ongoing Wars info retrieval ==== \\
    public void InsertParticipationMemberList(List<string> myClanParticipation, List<string> myTargetClanParticipation)
    {
        List<string> clanPart = new List<string>();

        // Participation List for Player Clan
        ongoingWarClanMemberParticipation.Add(clanPart);
        // Participation List for Target Clan
        ongoingWarTargetClanMemberParticipation.Add(myTargetClanParticipation);


        if (myClanParticipation.Count > 0)
            ongoingWarClanMemberParticipation[ongoingWarClanMemberParticipation.Count - 1] = myClanParticipation;

        if (myTargetClanParticipation.Count > 0)
            ongoingWarTargetClanMemberParticipation[ongoingWarTargetClanMemberParticipation.Count - 1] = myTargetClanParticipation;
    }
    public void ObtainAdditionalClanInfo(int queueNo)
    {
        //members
        GameSparksManager.Instance.GetOngoingWarsClanMembers(ongoingWarTargetClanID[queueNo], queueNo, this);
        //points
        GameSparksManager.Instance.GetClanPointsForOngoingWarClans(ongoingWarTargetClanID[queueNo], queueNo, this);
        //battle victories
        GameSparksManager.Instance.GetOngoingWarsClanBattleStats(ongoingWarTargetClanID[queueNo], queueNo, this);

        ongoingWarTargetClanLeaderID.Add("-");
        ongoingWarsTargetClanLeaderName.Add("-");

        ongoingWarsTargetClanMembersID.Add(new List<string> { "-" });
        ongoingWarsTargetClanMembersName.Add(new List<string> { "-" });

        ongoingWarClanPoints.Add(-1);
        ongoingWarClanBattleVictories.Add(-1);
        ongoingWarClanBattleCountList.Add(-1);
    }
    public void InsertOngoingWarsTargetClanLeader(int queueNo, string leaderID, string leaderName)
    {
        ongoingWarTargetClanLeaderID[queueNo] = leaderID;
        ongoingWarsTargetClanLeaderName[queueNo] = leaderName;
        UpdateDisplay(queueNo);
    }
    public void InsertOngoingWarsTargetClanMembers(int queueNo, List<string> membersName, List<string> memberID)
    {
        ongoingWarsTargetClanMembersID[queueNo] = memberID;
        ongoingWarsTargetClanMembersName[queueNo] = membersName;
        UpdateDisplay(queueNo);
    }
    public void InsertOngoingWarClanPoints(int queueNo, int points)
    {
        ongoingWarClanPoints[queueNo] = points;
        UpdateDisplay(queueNo);
    }
    public void InsertOngoingWarClanBattleStats(int queueNo, int battleCount, int victoryCount)
    {
        ongoingWarClanBattleVictories[queueNo] = victoryCount;
        ongoingWarClanBattleCountList[queueNo] = battleCount;
        UpdateDisplay(queueNo);
    }
    void UpdateDisplay(int queueNo)
    {
        if (ongoingWarTargetClanID[queueNo] != "-" & ongoingWarsTargetClanMembersID[queueNo][0] != "-" & ongoingWarClanPoints[queueNo] != -1 & ongoingWarClanBattleCountList[queueNo] != -1)
            AllOngoingWarInfosObtained(queueNo);
    }
    void AllOngoingWarInfosObtained(int queueNo)
    {
        OngoingWars ongoingWar = CreateNewOngoingChallenge();
        InsertInfoToOngoingWarOBJ(ongoingWar, queueNo);
    }
    void InsertInfoToOngoingWarOBJ(OngoingWars ongoingWar, int queueNo)
    {
        ongoingWar.targetClanName = ongoingWarTargetClanID[queueNo];
        ongoingWar.targetClanLeaderName = ongoingWarsTargetClanLeaderName[queueNo];
        ongoingWar.targetClanPoints = ongoingWarClanPoints[queueNo];

        ongoingWar.status = ongoingWarsStatusList[queueNo];

        ongoingWar.targetClanMembers = ongoingWarsTargetClanMembersID[queueNo].Count;
        ongoingWar.targetClanWarVictories = ongoingWarClanBattleVictories[queueNo];
        ongoingWar.targetClanWars = ongoingWarClanBattleCountList[queueNo];

        ongoingWar.clanParticipationCount = ongoingWarClanMemberParticipation[queueNo].Count;
        ongoingWar.targetClanParticipationCount = ongoingWarTargetClanMemberParticipation[queueNo].Count;

        ongoingWar.CreatListener(this);
        ongoingWar.UpdateDisplay();
    }
   
        // ==== Clan Participation ==== \\
    void ClanParticipantCreation()
    {
        for (int x = 0; x < ClanManager.Instance.clanMaxMemberCount; x++)
        {
            GameObject myClanParticipant = Instantiate(clanWarParticipantOBJ, myClanParticipationPanel.position, Quaternion.identity, myClanParticipationPanel);
            ClanWarParticipant myCWP = myClanParticipant.GetComponent<ClanWarParticipant>();
            memberParticipantList.Add(myCWP);

            GameObject targetClanParticipant = Instantiate(clanWarParticipantOBJ, targetClanParticipationPanel.position, Quaternion.identity, targetClanParticipationPanel);
            ClanWarParticipant targetCWP = targetClanParticipant.GetComponent<ClanWarParticipant>();
            targetMemberParticipantList.Add(targetCWP);
        }
    }
    public void DisplayParticipationStats(OngoingWars ongoingWar)
    {
        int clanQueue = ClansAtWarDisplay(ongoingWar);
        GetClanMembersToDisplay(clanQueue);
        DisplayParticipationPanel();
    }
    int ClansAtWarDisplay(OngoingWars ongoingWar)
    {
        for (int x = 0; x < ongoingWarTargetClanID.Count; x++)  // Return AT target Clan ID List queue
        {
            // if  we find the correct clan name in the ongoing war list
            if (ongoingWar.targetClanName == ongoingWarTargetClanID[x])
            {
                myClanNameText.text = clanID;
                myClanParticipationNumberText.text = ongoingWar.MyClanParticipationCount();
                targetClanNameText.text = ongoingWar.targetClanName;
                targetClanParticipationNumberText.text = ongoingWar.YourClanParticipationCount();
                return x;
            }
        }
        return -1;
    }
    void GetClanMembersToDisplay(int clanQueueInt)
    {
        // ===== PLAYERS CLAN PARTICIPATION UPDATE ===== \\

        //Send the group of participants from PLayers Clan to the panel
        for (int y = 0; y < clanMembersName.Count; y++)
        {
            // Before inserting the name, check if it already exisits
            for (int u = 0; u < memberParticipantList.Count; u++)
            {
                if (memberParticipantList[u].participantName == clanMembersName[y])
                    continue;
                {
                    memberParticipantList[y].participantName = clanMembersName[y];
                    memberParticipantList[y].UpdateDisplay();

                    //Check if they took part in this particular war
                    if (ongoingWarClanMemberParticipation[clanQueueInt].Count > 0)
                    {
                        //Which means that that are participants in this particular war belonging to the player's clan
                        for (int z = 0; z < ongoingWarClanMemberParticipation[clanQueueInt].Count; z++)
                        {
                            if (clanMembersID[y] == ongoingWarClanMemberParticipation[clanQueueInt][z])
                            {
                                memberParticipantList[y].DisplayParticipation(true);
                                memberParticipantList[y].SetMyDisplay(true);
                            }
                            else
                                memberParticipantList[y].DisplayParticipation(false);
                        }
                    }
                }
            }
        }
        for (int y = clanMembersName.Count; y < ClanManager.Instance.clanMaxMemberCount; y++)
        {
            memberParticipantList[y].SetMyDisplay(false);
        }

        // ===== ENEMY CLAN PARTICIPATION UPDATE ===== \\
        for (int y = 0; y < ongoingWarsTargetClanMembersID[clanQueueInt].Count; y++)
        {
            targetMemberParticipantList[y].participantName = ongoingWarsTargetClanMembersName[clanQueueInt][y];
            targetMemberParticipantList[y].UpdateDisplay();
            if (ongoingWarTargetClanMemberParticipation[clanQueueInt].Count > 0)
            {
                for (int z = 0; z < ongoingWarTargetClanMemberParticipation[clanQueueInt].Count; z++)
                {
                    if (ongoingWarsTargetClanMembersID[clanQueueInt][y] == ongoingWarTargetClanMemberParticipation[clanQueueInt][z])
                    {
                        targetMemberParticipantList[y].DisplayParticipation(true);
                        targetMemberParticipantList[y].SetMyDisplay(true);
                    }
                    else
                        targetMemberParticipantList[y].DisplayParticipation(false);
                }
            }
        }
        for (int y = ongoingWarsTargetClanMembersID[clanQueueInt].Count; y < ClanManager.Instance.clanMaxMemberCount; y++)
        {
            targetMemberParticipantList[y].SetMyDisplay(false);
        }
    }
    void DisplayParticipationPanel()
    {
        particiaptionPanel.SetActive(true);
    }
    public void OngoingClanWarsInfoObtained()
    {
        for (int x = 0; x < ongoingWarTargetClanID.Count; x++)
        {
            ObtainAdditionalClanInfo(x);
        }
        CheckIfDeclaredWarTargetIsLoaded();
    }

    public void ConfirmDeclareWar(string clanID, TargetClan targetClan)
    {
        targetToChallenge = targetClan;
        confirmWarText.text = string.Format("Are you sure \n you want to declare War on \n {0}? ", clanID);
        WarConfirmationPopupToggle(true);
    }
    public void WarConfirmationPopupToggle(bool isActive)
    {
        declareWarConfirmationPopup.SetActive(isActive);
    }
    void AttemptDeclareWar()
    {
        GameSparksManager.Instance.DeclareClanWar(targetToChallenge, this);
    }

    // ==== Challenging Target ==== \\
    public void InsertChallengedClanInfo(TargetClan targetToAdd)
    {
        InsertNewWar(targetToAdd);
        UpdateNewOngoingWar(CreateNewOngoingChallenge(), targetToAdd);
        RemoveChallengedTarget(targetToAdd);
    }
    void InsertNewWar(TargetClan targetToAdd)
    {
        ongoingWarTargetClanID.Add(targetToAdd.clanName);
        ongoingWarTargetClanLeaderID.Add(targetToAdd.clanLeaderName);
        ongoingWarClanPoints.Add(targetToAdd.points);
    }
    void UpdateNewOngoingWar(OngoingWars newChallenge, TargetClan targetClan)
    {
        newChallenge.targetClanName = targetClan.clanName;
        newChallenge.targetClanLeaderName = targetClan.clanLeaderName;

        newChallenge.targetClanPoints = targetClan.points;

        newChallenge.targetClanMembers = targetClan.memberCount;

        newChallenge.targetClanWarVictories = targetClan.victoryCount;
        newChallenge.targetClanWars = targetClan.battleCount;

        newChallenge.playerPointCount = 50 - targetClan.memberCount;
        newChallenge.targetPointCount = 50 - clanMembersID.Count;

        newChallenge.status = "pending";

        newChallenge.CreatListener(this);
        newChallenge.UpdateDisplay();
    }
    void RemoveChallengedTarget(TargetClan targetToRemove)
    {
        for (int x = 0; x < targetClansLoaded.Count; x++)
        {
            if (targetToRemove == targetClansLoaded[x])
            {
                targetClanNameList.RemoveAt(x);
                targetClanLeadersList.RemoveAt(x);

                targetClanPointList.RemoveAt(x);
                targetClanBattleCountList.RemoveAt(x);
                targetClanBattleVictoriesList.RemoveAt(x);

                targetClansLoaded[x].DestroyThis();
                targetClansLoaded.RemoveAt(x);
            }
        }
    }
    OngoingWars CreateNewOngoingChallenge()
    {
        GameObject ogWar = Instantiate(ongoingWarObj, ongoingClanWarsPanel.position, Quaternion.identity, ongoingClanWarsPanel);
        OngoingWars ongoingWar = ogWar.GetComponent<OngoingWars>();
        ongoingWarsLoaded.Add(ongoingWar);

        // Perform the Check only when we're sure all the WAR info has been pulled so we can update the Latest values
        if (ongoingWarsLoaded.Count == ongoingWarTargetClanID.Count)
            CheckIfLastBattleWasOpposingClan();
        return ongoingWar;
    }


    // ==== Updating Player Clan Battle ==== \\
    void CheckIfLastBattleWasOpposingClan()
    {
        //for (int x = 0; x < ongoingWarTargetClanID.Count; x++)
        //{
            // This method is used for when the PLAYER's Clan Battle info is to be updated
            if (atWarEnemy & battleOutcome) //which means the player won a match with a target that they are at war with
            {
                // we need the clan leader's name in order to send a team message to them

                //FIND THE CLAN WE ARE AT WAR WITH
                for (int x = 0; x < ongoingWarTargetClanID.Count; x++)
                {
                    
                }

                //perform the Participation update on the Cloud here
                GameSparksManager.Instance.UpdateParticipation(Enemy.clan, this, CheckLeaderName());
                // Update the Player's contribution points to the Cloud
                GameSparksManager.Instance.SavePlayerClanInfo(GameSparksManager.playerId);
            }
        //}
    }
    string CheckLeaderName()
    {
        for (int x = 0; x < ongoingWarTargetClanID.Count; x++)
        {
            if (ongoingWarTargetClanID[x] == Enemy.clan)
            {
                return ongoingWarTargetClanLeaderID[x];
            }
        }
        return "";
    }
    public void MemberParticipationUpdated(string playerID, string enemyClanID) // When GS is updated on the Players participation
    {
        //insert into Clan participation list and update the display
        UpdateMemberParticipationAtWar(playerID, enemyClanID);
        //check if the clan has won the war
        CheckIfClanHasWonWar(enemyClanID);
    }
    // UpdateMemberParticipationAtWar is used when ANY Clan member has participated in the WAR
    void UpdateMemberParticipationAtWar(string playerID, string enemyClanID)
    {
             
        for (int x = 0; x < ongoingWarTargetClanID.Count; x++)
        {
            if (enemyClanID == ongoingWarTargetClanID[x])
            {
                // Before ADDing player to List
                if (ongoingWarClanMemberParticipation[x] != null)
                {
                    for (int y = 0; y < ongoingWarClanMemberParticipation[x].Count; y++)
                    {
                        if (ongoingWarClanMemberParticipation[x][x] == playerID)
                            break;
                        
                        if (y == ongoingWarClanMemberParticipation[x].Count)
                            ongoingWarClanMemberParticipation[x].Add(playerID);
                    }
                }
                else
                {
                    ongoingWarClanMemberParticipation[x] = new List<string>();
                    ongoingWarClanMemberParticipation[x].Add(playerID);
                }
                InsertInfoToOngoingWarOBJ(ongoingWarsLoaded[x], x);
            }
        }
    }
    void CheckIfClanHasWonWar(string enemyClanID)
    {
        for (int x = 0; x < ongoingWarTargetClanID.Count; x++)
        {
            if (enemyClanID == ongoingWarTargetClanID[x])
            {
                // do the checking here
                int participationCount = (50 - ongoingWarsTargetClanMembersName.Count + ongoingWarClanMemberParticipation[x].Count);
                if (participationCount >= 50)
                {
                    // in this Case the player wins
                    ClanManager.Instance.WarConcluded(enemyClanID, 0);

                    //Move this object into the CLAN WAR History
                    AddCurrentlyAtWarWithToHistory(enemyClanID);
                }
            }
        }
    }
    void AddCurrentlyAtWarWithToHistory(string enemyClanID)
    {
        endedClanWarTargetClanID.Add(enemyClanID);
        for (int x = 0; x < ongoingWarsLoaded.Count; x++)
        {
            if (ongoingWarsLoaded[x].targetClanName == enemyClanID)
            {
                endedClanWarTargetClanID.Add(ongoingWarTargetClanID[x]);

                // Add the required info into the history tab
                endedClanWarTargetClanMemberCountDuringWar.Add(ongoingWarsLoaded[x].targetClanParticipationCount);
                endedClanWarClanMemberCountDuringWar.Add(ongoingWarsLoaded[x].clanParticipationCount);

                //endedClanWarClanMemberParticipationList.Add(ongoingWarClanMemberParticipation[x]);
                //endedClanWarTargetClanParticipationList.Add(ongoingWarTargetClanMemberParticipation[x]);

                InsertWarHistoryParticipationList(ongoingWarClanMemberParticipation[x], ongoingWarTargetClanMemberParticipation[x]);

                // remove all the info in the lists and the actual obj
                ongoingWarTargetClanID.RemoveAt(x);
                ongoingWarTargetClanLeaderID.RemoveAt(x);
                ongoingWarsTargetClanLeaderName.RemoveAt(x);

                ongoingWarsTargetClanMembersID.RemoveAt(x);
                ongoingWarsTargetClanMembersName.RemoveAt(x);
                ongoingWarTargetClanMemberParticipation.RemoveAt(x);

                ongoingWarClanMemberParticipation.RemoveAt(x);

                ongoingWarClanPoints.RemoveAt(x);
                ongoingWarClanBattleCountList.RemoveAt(x);
                ongoingWarClanBattleVictories.RemoveAt(x);

                ongoingWarsStatusList.RemoveAt(x);

                ongoingWarsLoaded[x].RemoveMe();

                CreateSingleClanWarHistory();
            }
        }
    }

    public void InsertWarHistoryParticipationList(List<string> myClanParticipation, List<string> myTargetClanParticipation)
    {
        List<string> clanPart = new List<string>();

        // Participation List for Player Clan
        endedClanWarClanMemberParticipationList.Add(clanPart);
        // Participation List for Target Clan
        endedClanWarTargetClanParticipationList.Add(myTargetClanParticipation);

        if (myClanParticipation.Count > 0)
            endedClanWarClanMemberParticipationList[endedClanWarClanMemberParticipationList.Count - 1] = myClanParticipation;
        if (myTargetClanParticipation.Count > 0)
            endedClanWarTargetClanParticipationList[endedClanWarTargetClanParticipationList.Count - 1] = myTargetClanParticipation;
    } 
    public void ClanWarHistoryInfoObtained()
    {
        for (int x = 0; x < endedClanWarTargetClanID.Count; x++)
        {
            GameObject clanHistOBJ = Instantiate(clanHistoryOBJ, clanWarHistoryPanel.position, Quaternion.identity, clanWarHistoryPanel);
            ClanWarHistory clanWarHistory = clanHistOBJ.GetComponent<ClanWarHistory>();
            clanWarHistory.status = "ended";
            clanWarHistory.targetClanName = endedClanWarTargetClanID[x];

            clanWarHistory.clanMemberDuringWar = endedClanWarClanMemberCountDuringWar[x];
            clanWarHistory.targetClanMemberDuringWar = endedClanWarTargetClanMemberCountDuringWar[x];

            clanWarHistoryLoaded.Add(clanWarHistory);
            clanWarHistory.UpdateDisplay();
        }
    }
    void CreateSingleClanWarHistory()
    {
        // Used when the Clan has completed a war by max'ing out participation count
        int y = endedClanWarTargetClanID.Count - 1;
        GameObject clanHistOBJ = Instantiate(clanHistoryOBJ, clanWarHistoryPanel.position, Quaternion.identity, clanWarHistoryPanel);
        ClanWarHistory clanWarHistory = clanHistOBJ.GetComponent<ClanWarHistory>();
        clanWarHistory.status = "ended";
        clanWarHistory.targetClanName = endedClanWarTargetClanID[y];

        clanWarHistory.clanMemberDuringWar = endedClanWarClanMemberCountDuringWar[y];
        clanWarHistory.targetClanMemberDuringWar = endedClanWarTargetClanMemberCountDuringWar[y];

        clanWarHistoryLoaded.Add(clanWarHistory);
        clanWarHistory.UpdateDisplay();
    }
    // ====== CLAN WARS END ====== \\


    // ===== LEAVE CLAN ===== \\
    public void AttemptLeaveClan()
    {
        if (GameSparksManager.playerId == leaderID)
        {
            ClanManager.Instance.InfoPopup("Are you sure you want to leave " + clanID + "? All your members will be dropped." );
        }
        else
        {
            ClanManager.Instance.InfoPopup("Are you sure you want to leave " + clanID + "?");
        }
    }
    public void LeaveClan()
    {
        if (GameSparksManager.playerId != leaderID)
            GameSparksManager.Instance.RemoveMemberNotification(clanID, GameSparksManager.playerId, Player.playerName, "leave", this);
        else
            GameSparksManager.Instance.DropClanRequest(clanID, GameSparksManager.playerId, this);
    }
    public void LeftClan()
    {
        // Removing member List from the Panel
        RemoveClanMemberOBJ();
        ClanPanel();
        Clear();
        UpdateUIElements();

        //Removing Chat History of Clan
        ClearClanChatLists();
        ClearClanChatOBJs();


        EventHolderManager.Instance.NotInClanProgressRetrieved();
        EventHolderManager.Instance.DisableClanOBJCollection();

        UIManager.Instance.XcaliburRewards(false);

        DataManager.Instance.PrepareDataForSparksPushing();
        GameSparksManager.Instance.SavePlayerDetails();
        GameSparksManager.Instance.SavePlayerClanInfo(GameSparksManager.playerId);
    }
    void RemoveClanMemberOBJ()
    {
        for (int x = 0; x < clanMembersID.Count; x++)
        {
            clanMembers[x].DestroyThis();
        }
        distributionCounter -= 1;
    }
    void Clear()
    {
        clanID = "<currently_not_in_any_clan_yet>";
        clanIcon = "";
        clanMemberLimit = ClanManager.Instance.clanMaxMemberCount;
        clanNotice = "";
        leaderID = "";
        leaderName = "";
        clanAvailableGold = 0;

        rank = -100;
        contributionPoints = 0;

        clanPoints = 0;
        clanWarsCount = 0;
        clanWarsVictories = 0;

        clanMembersID.Clear();
        clanMembersName.Clear();
        clanMembersReputation.Clear();
        clanMembersRankValue.Clear();
        clanMembersContributions.Clear();
        clanMembersBattles.Clear();
        clanMembersVictories.Clear();
        clanMembersLosses.Clear();
        clanMembers.Clear();

        ongoingWarTargetClanID.Clear();
        ongoingWarTargetClanLeaderID.Clear();
        ongoingWarsTargetClanLeaderName.Clear();
        ongoingWarsTargetClanMembersID.Clear();
        ongoingWarsTargetClanMembersName.Clear();
        ongoingWarClanPoints.Clear();
        ongoingWarClanBattleCountList.Clear();
        ongoingWarClanBattleVictories.Clear();

        endedClanWarTargetClanID.Clear();
        endedClanWarTargetClanLeaderID.Clear();
        endedClanWarTargetClanLeaderName.Clear();
        endedClanWarClanMemberCountDuringWar.Clear();
        endedClanWarClanMemberParticipationList.Clear();
        endedClanWarTargetClanParticipationList.Clear();
        endedClanWarTargetClanMemberCountDuringWar.Clear();

        ongoingWarClanMemberParticipation.Clear();
        ongoingWarTargetClanMemberParticipation.Clear();

        clanFlag = 0;
        tempFlagNo = 0;
    }
    void ClanPanel()
    {
        ClanManager.Instance.ClanPanelToggle(false);
    }

    void ClearClanChatLists()
    {
        clanChatBox.ClearAllLists();
    }
    void ClearClanChatOBJs()
    {
        clanChatBox.ClearChatObjs();
    }
    void ToggleGlobalChat()
    {
        globalChatToggle.isOn = true;
    }

    // ===== LEAVE CLAN END ===== \\


    // ===== RESET CLAN INFO ===== \\
    public void ResetClanInfos()
    {
        if (clanID != "<currently_not_in_any_clan_yet>")
        {
            RemoveClanMemberOBJ();
            Clear();
        }
    }
    // ===== RESET CLAN INFO END ===== \\


    // ===== CLAN VALUES CONVERSION ===== \\
    public string RankType(int rankInt)
    {
        switch (rankInt)
        {
            case 0:
                return "Leader";
            case 1:
                return "Officers";
            case 2:
                return "Member";
            case 3:
                return "Member";
            default:
                return "Unassigned Rank";
        }
    }
    public static int RankInt(string rankType)
    {
        switch (rankType)
        {
            case "Leader":
                return 0;
            case "Officers":
                return 1;
            case "Member":
                return 2;
            default:
                return -100;
        }
    }


    // ==== MISC ==== \\
    bool LeaderCheck(string playerID)
    {
        if (playerID == leaderID)
            return true;
        return false;
    }
    bool OfficerCheck(int rankNo)
    {
        if (rankNo == 1)
            return true;
        return false;
    }
    string MemberName(string playerID)
    {
        string pName = "";
        for (int x = 0; x < clanMembers.Count; x++)
        {
            if (clanMembers[x].memberID == playerID)
                pName = clanMembers[x].memberName;
        }
        return pName;
    }
}
