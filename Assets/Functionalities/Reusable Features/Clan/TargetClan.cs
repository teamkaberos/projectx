﻿using UnityEngine.UI;
using UnityEngine;
using System.Collections.Generic;

public class TargetClan : MonoBehaviour
{
    // ==== INFO AREA ==== \\
    [HideInInspector] public string clanName = "-";
    [HideInInspector] public string leaderID = "-";
    [HideInInspector] public string clanLeaderName = "-";
    [HideInInspector] public string clanIcon = "-";
    [HideInInspector] public int points = -1;
    [HideInInspector] public int memberCount = -1;
    [HideInInspector] public int battleCount = -1;
    [HideInInspector] public int victoryCount = -1;
    [HideInInspector] public List<string> clanMembersName = new List<string>();
    [HideInInspector] public List<string> clanMembersID = new List<string>();
    [HideInInspector] public Clan clan;

    // ==== UI DISPLAY AREA ==== \\
    public Text clanNameText;
    public Text clanLeaderNameText;
    public Text clanMembersCount;
    public Text clanPointsText;
    public Text clanBattleCountsText;
    public Button declareWar;


    void Start()
    {
        declareWar.onClick.AddListener(DeclareWar);
    }

    public void UpdateDisplay()
    {
        memberCount = clanMembersID.Count;

        clanNameText.text = clanName;
        clanLeaderNameText.text = clanLeaderName;
        clanPointsText.text = "Clan Points : " + points;
        clanBattleCountsText.text = string.Format("War Counts : {0:n0} / {1:n0}", victoryCount, battleCount);
        clanMembersCount.text = string.Format("Members : {0} / 50", clanMembersID.Count);

        if (battleCount != -1 & victoryCount != -1 & points != -1 & clanMembersID.Count > 0)
            gameObject.SetActive(true);

        if (Clan.rank <= 1)
            declareWar.interactable = true;
        else
            declareWar.interactable = false;
    }

    void DeclareWar()
    {
        clan.ConfirmDeclareWar(clanName, this);
    }


    public void DestroyThis()
    {
        Destroy(gameObject, 0.03f);
    }
  
}






