﻿using UnityEngine;
using System.Collections.Generic;

public class ResourceObtain : MonoBehaviour 
{
    public GameObject resourceCollectedObj;                      //Object carrying the text that displays resource obtained

    public Transform resourceCollectedOBJHolder;               //Holder to store all instantiated resource collected obj

    [Range(5, 12)]
    public int resourceCollectedCounter = 5;
    List<GameObject> resourceCollectedObjList = new List<GameObject>();                  //The list that holds all accessible resource collected obj

    Vector3 displayPosition;

    void Start()
    {
        for (int x = 0; x < resourceCollectedCounter; x++)
        {
            GenerateResourceCollectedOBJ();
        }
        ListenToStoneCollected();
    }
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray))
            {
                displayPosition = Input.mousePosition;
            }
        }
    }

    private void OnDestroy()
    {
        BuildTimer.stoneCollect -= DisplayCollected;
    }

    // ===== OBJ CREATION PROCESS ===== \\
    void GenerateResourceCollectedOBJ()
    {
        GameObject obj = CreateRSSCollectOBJ();
        resourceCollectedObjList.Add(obj);
    }
    GameObject CreateRSSCollectOBJ()
    {
        GameObject obj = Instantiate(resourceCollectedObj, resourceCollectedOBJHolder.position, Quaternion.identity, resourceCollectedOBJHolder);
        return obj;
    }


    // ====== RSS GENERATORS LISTENER ======\\
    public void SubscribeToRSSGenerator(ResourceGenerate generatorID)
    {
        generatorID.ResourceAdded += DisplayCollected;
    }

    // ====== STONE COMPLETED LISTENER ====== \\
    void ListenToStoneCollected()
    {
        BuildTimer.stoneCollect += DisplayCollected;
    }



    // ===== USING ON CANVAS ===== \\
    void DisplayCollected(string rssType, int rssCollected)
    {
        string toDisplay = "";
        //check whether the TREASURY is full
        if (rssType.ToLower().Contains("gold"))
        {
            if (Player.gold >= Player.treasury_Storage_Cap)
                toDisplay = "Gold Storage already at limit";
            else
                toDisplay = string.Format("+ {0:n0} {1}", rssCollected, rssType);
        }
        else
        {
            toDisplay = string.Format("+ {0:n0} {1}", rssCollected, rssType);   //format what to display first
        }
        GameObject obj = GetInactive();                                                        //get an inactive display object
        EnableText(obj, toDisplay);                                                                //get and change the text
        MoveAndDisplayText(obj);                                                                      //move to target location
    }
    GameObject GetInactive()
    {
        for (int x = 0; x < resourceCollectedCounter; x++)
        {
            if (!resourceCollectedObjList[x].activeInHierarchy)
            {
                return resourceCollectedObjList[x];
            }
        }
        return null;
    }
    void EnableText(GameObject obj,string toDisplay)
    {
        obj.GetComponent<TextController>().SetText(toDisplay);
    }
    void MoveAndDisplayText(GameObject obj)
    {
        obj.transform.position = displayPosition;
        obj.SetActive(true);
    }



}
