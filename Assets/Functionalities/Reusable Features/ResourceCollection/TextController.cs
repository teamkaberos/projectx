﻿using UnityEngine;
using UnityEngine.UI;

public class TextController : MonoBehaviour 
{
    public Text rssObtainedText;


    public void SetText(string text)
    {
        rssObtainedText.text = text;
    }

    public void SetInactive()
    {
        gameObject.SetActive(false);
    }
}
