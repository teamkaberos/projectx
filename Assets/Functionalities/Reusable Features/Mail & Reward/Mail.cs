﻿using UnityEngine.UI;

public class Mail : Reward
{
    public delegate void OpenMail(string name, string title, string content, bool valueChange, Mail lastMail);
    public event OpenMail MailTapped;

	public Text senderText;
	public Text subjectText;

    Toggle OpenMailToggle;

 
    public void Mail_Deleted()
    {
        Destroy(gameObject);
    }
}
