﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class BaseMail : MonoBehaviour
{
    public delegate void MailDeleteToggled(bool toSet);
    public event MailDeleteToggled mailDeleteToggled;

    [Header("Mail Manager Ref")]
    public MailManager mailManager;


    [Header("Mail Info")]
    public string targetID;
    public string targetName;
    public string targetAvatar;
    public string mailSubject;
    public string mailBody;
    public long mailSentTime;
    public long expiresInTime;
    public float mailDurationInSeconds;


    [Space(15)]
    [Header("UI Info")]
    public Text targetNameText;
    public Image targetAvatarImage;
    public Text mailSubjectText;
    public Text mailBodyTextMinor;                               // Shortened display of the body text
    public Text mailSentTimeText;
    public Text expirationTimeText;
    public Image mailDisplayToggleImage;                    // The image that will be expanded upon tapping
    protected Vector2 originalMailImageSize;                // The original image size of the mail display toggle


    [Space(15)]
    [Header("Behaviour")]
    public Toggle mailOpenToggle;               // Toggle itself to display the message contents
    public float yHeightExpansionValue;


    [Space(15)]
    [Header("Delete Toggle")]
    public Toggle deleteCheckboxToggle;               // Toggle itself to display the message contents


    protected virtual void Start()
    {
        originalMailImageSize = mailDisplayToggleImage.rectTransform.sizeDelta;

        MailDeleteToggleListener();
        MailOpenListener();
    }

    protected virtual void OnEnable()
    {
        CalculateExpiryTimePassed();
    }

    void MailOpenListener()
    {
        mailOpenToggle.onValueChanged.AddListener(ToggleMail);
    }


    // =========== MAIL MANAGER REFERENCING ============= \\
    public void MailManage(MailManager mm)
    {
        mailManager = mm;
    }


    // ============ MAIL EXTENSION AREA ================\\
    protected virtual void ToggleMail(bool toSet)
    {
        if (toSet)
            ExpandMail();
        else
            CloseMail();
    }
    void ExpandMail()
    {
        mailDisplayToggleImage.rectTransform.sizeDelta = new Vector2(originalMailImageSize.x, yHeightExpansionValue);
    }
    void CloseMail()
    {
        mailDisplayToggleImage.rectTransform.sizeDelta = originalMailImageSize;
    }
    // ============ MAIL EXTENSION AREA ENDS =============\\


    // ============ UPDATE INFO AREA ==================\\
    public void InfoObtain(string recName, string recID, string avatar, long timeSent)
    {
        targetID = recID;
        targetName = recName;
        targetAvatar = avatar;
        mailSentTime = timeSent;
        expiresInTime = CalculateExpireTime(timeSent);
    }
    public void InfoObtain(string recID, long timeSent)
    {
        targetID = recID;
        mailSentTime = timeSent;
        expiresInTime = CalculateExpireTime(timeSent);
    }
    public void InfoObtain(string recName, string avatar)
    {
        targetName = recName;
        targetAvatar = avatar;
    }
    public void MessageInfoObtain(string messSubject, string messContent)
    {
        mailSubject = messSubject;
        mailBody = messContent;
    }

    public virtual void UpdateInfo()
    {
        targetNameText.text = "To : " + targetName;
        targetAvatarImage.sprite = PotraitManager.Instance.PortraitSprite(targetAvatar);
        mailSubjectText.text = "RE : " + mailSubject;

        mailBodyTextMinor.text = TextParse(mailBody);

        mailSentTimeText.text = TimeDifference();

        if (MailHasExpired())
        {
            ExpireMail();
        }
        else
            expirationTimeText.text = ExpirationTime();
    }

    void CalculateExpiryTimePassed()
    {
        expiresInTime -= (long)Time.timeSinceLevelLoad;
        UpdateInfo();
    }
    long CalculateExpireTime(long timeMailWasCreated)
    {
        DateTime epochStart = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        long UTCtimeNow = (long)(DateTime.UtcNow - epochStart).TotalSeconds;
        return UTCtimeNow - timeMailWasCreated;
    }
    bool MailHasExpired()
    {
        if (expiresInTime >= mailDurationInSeconds)
            return true;
        return false; 
    }
    string ExpirationTime()
    {
        string exp = "";
        double difference = mailDurationInSeconds - expiresInTime;
        int days = Mathf.FloorToInt( (float)(difference / 86400));
        int hours = Mathf.FloorToInt( (float)(difference - (days * 86400)) / 3600 );
        int mins = Mathf.FloorToInt( (float) (difference -((days*86400) + (hours * 3600)))/ 60);

        exp = mins + "mins";
        if (hours > 0)
            exp = hours + " hours " + exp;
        if (days > 0)
            exp = days + " days ";

        return "Expires in : " + exp;
    }
    string TextParse(string textToParse)
    {
        if (textToParse.Length > 15)
            return string.Format("{0} . . . ", textToParse.Remove(15, textToParse.Length - 15));
        
        return textToParse;
    }
    DateTime DateSent(long utcTime)
    {
        DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
        dtDateTime = dtDateTime.AddSeconds(utcTime);
        return dtDateTime;
    }
    // =========== UPDATE INFO AREA ENDS================\\



    // ==== TIME PARSER ==== \\

    string TimeDifference()
    {
        DateTime nowTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddMilliseconds(DataManager.initialLoginServerTime * 1000).AddSeconds(Time.timeSinceLevelLoad);
        DateTime dtMailSentTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddSeconds(mailSentTime);
        TimeSpan diff = nowTime.Subtract(dtMailSentTime);

        string timeSentText_ = "";

        if (Mathf.FloorToInt(diff.Days) >= 1)
        {
            timeSentText_ = Mathf.FloorToInt(diff.Days) + " day";
            if (Mathf.FloorToInt(diff.Days) > 1)
                timeSentText_ += "s";
        }
        else if (Mathf.FloorToInt(diff.Hours) >= 1)
        {
            timeSentText_ = Mathf.FloorToInt(diff.Hours) + " hr";
            if (Mathf.FloorToInt(diff.Hours) > 1)
                timeSentText_ += "s";
        }
        else if (Mathf.FloorToInt(diff.Minutes) >= 1)
        {
            timeSentText_ = Mathf.FloorToInt(diff.Minutes) + " min";
            if (Mathf.FloorToInt(diff.Minutes) > 1)
                timeSentText_ += "s";
        }
        else if (Mathf.FloorToInt(diff.Minutes) < 1)
        {
            timeSentText_ = "Less than a min";
        }
        return string.Format("{0} ago", timeSentText_);
    }

    // ==== TIME PARSER END ==== \\





    // ========== DELETE MAIL AREA ===============\\
    void MailDeleteToggleListener()
    {
        deleteCheckboxToggle.onValueChanged.AddListener(MailDeleteToggling);
    }
    void MailDeleteToggling(bool toSet)
    {
        mailDeleteToggled(toSet);
    }

    public virtual void ExpireMail()
    {
        GameSparksManager.Instance.DeleteExpiredMail(GameSparksManager.playerId, mailSentTime, this);
    }
    public void OutboxMailExpired()
    {
        mailManager.RemoveRawExpiredMailInfo(this, "outbox");
        DestroyThisMail();
    }
    public void InboxMailExpired()
    {
        mailManager.RemoveRawExpiredMailInfo(this, "inbox");
        DestroyThisMail();
    }

    public void DeleteMail()
    {
        mailManager.DeleteThisMail(this);
        DestroyThisMail();
    }
    public void DestroyThisMail()
    {
        Destroy(gameObject, 0.02f);
    }
    // ======== DELETE MAIL AREA END =============\\
}
