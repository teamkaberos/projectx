﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MailManager : MonoBehaviour
{
    #region Singleton
    static MailManager _instance;
    public static MailManager Instance
    {
        get
        {
            return _instance;
        }
    }

    void Awake()
    {
        _instance = this;
    }
    #endregion

    [Header("MAILS NOTIFICATIONS")]
    public GameObject newMailsNotification;
    public GameObject sentMailsNotification;
    public GameObject inboxNotify;

    [Space(10)]
    [Header("MAILS NOTIFICATIONS")]
    public Toggle inboxToggle;
    public Toggle outboxToggle;

    [Space(10)]
    [Header("EMPTY TABS DISPLAY")]
    public EmptyTabDisplay[] emptyTabsDisplay;

    [Serializable]
    public class PlayerDetails
    {
        public string playerID;
        public string playerDisplayName;
        public string playerDisplayAvatar;
        public string clanID;
    }
    [Header("MAIL COMMUNICATION TARGETS")]
    public List<PlayerDetails> playerDetailList = new List<PlayerDetails>();
    public int totalDifferentPlayerDetails;

    [Serializable]
    public class MailObject
    {
        public string recipID;
        public string senderID;
        public string subject;
        public string content;
        public long timeSent;
    }
    [Space(10)]
    [Header("         ****   RAW MAIL DETAILS   ****     ")]
    [Header("RAW **OUTBOX** Info")]
    public List<MailObject> rawOutboxListInfo = new List<MailObject>();
    [Space(10)]
    [Header("RAW **INBOX** Info")]
    public List<MailObject> rawInboxListInfo = new List<MailObject>();
    bool inboxChecked;
    bool outboxChecked;

    public static int numberOfMailsSendingToDelete;           // how many mails were sent to be deleted
    public static int numberOfMailsResponsesObtained;       // keep track of how many responses there should be when deleting
    [Space(10)]
    [Header("MAIL DELETION")]
    List<int> toBeDeletedIntList = new List<int>();
    public GameObject deleteMailsPanel;
    public GameObject confirmDeleteMailsPanel;
    public Button selectAllMailsButton;
    public GameObject selectAllImage;                    // to show if all items are selected
    public Button attemptDeleteMailsButton;



    [Space(10)]
    [Header("RESPONSE")]
    public GameObject responeOBJ;
    public Text responseText;

    [Space(10)]
    [Header("LOADING")]
    public GameObject loadingScreen;
    public Button loadingScreenTapToClose;

    // ========= MAIL SENDING AREA ========= \\
    [Space(10)]
    [Header("MAIL SENDING")]
    public InputField toRecipient;
    public InputField mailSubject;
    public InputField mailContent;

    public Text mailTextCount;

    public Button attemptSendMailButton;        // the button that will be tapped to send the mail



    bool CheckFields()
    {
        if (toRecipient.text == "")
        {
            DisplayEventHandling("Please input recipient.", true, true);
            return false;
        }
        if (toRecipient.text.ToLower() == Player.playerName.ToLower())
        {
            DisplayEventHandling("You can't send a mail to yourself", true, true);
            return false;
        }
        if (mailSubject.text == "")
        {
            DisplayEventHandling("Please input mail subject.", true, true);
            return false;
        }
        if (mailContent.text == "")
        {
            DisplayEventHandling("What do you want to say?", true, true);
            return false;
        }
        return true;
    }

    void AttemptSendMail()
    {
        if (CheckFields())
        {
            DisplayEventHandling("Sending mail to " + toRecipient.text.Trim(), true, true);

            if (PlayerIsLoadedCheckWithName(toRecipient.text))
                GameSparksManager.Instance.SendMail(LoadedPlayerID(toRecipient.text), mailSubject.text.Trim(), mailContent.text.Trim());
            else
                GameSparksManager.Instance.CheckIfUsernameExists(toRecipient.text.Trim().ToLower(), mailSubject.text.Trim(), mailContent.text.Trim());
        }
    }
    public void Mail_Text_Count(string input)
    {
        mailTextCount.text = string.Format("{0}/{1}", input.Length, mailContent.characterLimit);
    }

    public void MailSuccessfullySent(string recipientName, string recipientID, string recipientAvatar, long utcTime)
    {
        CreateSentMails(recipientName, recipientID, recipientAvatar, utcTime);
        SentMailsRawInfoUpdate(recipientID, mailSubject.text, mailContent.text, utcTime);
        ClearFields();
        ToggleSentMailsNotify(true);
    }
    public void ClearFields()
    {
        toRecipient.text = "";
        mailSubject.text = "";
        mailContent.text = "";
    }
    // ======== MAIL SENDING AREA END======== \\




    // ======== SENT MAIL AREA =========\\
    [Space(10)]
    [Header("MAIL SENDING")]
    public GameObject sentMailOBJ;                                              // the OBJ to be instantiated when there is a sent mail
    public Transform sentMailTransform;                                      // the Transform where the sentMailObj is to be childed
    public List<SentMail> mailsSent = new List<SentMail>();       // track record of mails sent by the players
    public ToggleGroup selectedSentMailDisplay;

    // === On receiving response when Player sends a Mail to another Player === \\
    public void CreateSentMails(string recipientName, string recipientID, string recipientAvatar, long utcTime)
    {
        // Create the MAIL SENT OBj
        GameObject mailObj = NewOutboxMailObj();
        mailObj.transform.SetAsFirstSibling();
        mailObj.GetComponent<Toggle>().group = selectedSentMailDisplay;

        SentMail newSentMail = mailObj.GetComponent<SentMail>();
        newSentMail.mailDeleteToggled += ToggleDeletePanel;
        newSentMail.MailManage(this);
        newSentMail.InfoObtain(recipientName, recipientID, recipientAvatar, utcTime);
        newSentMail.MessageInfoObtain(mailSubject.text.Trim(), mailContent.text.Trim());
        mailsSent.Add(newSentMail);

        newSentMail.UpdateInfo();
    }
    // === On Retrieving mails sent by PLAYER when scene loads === \\
    public void SentMailsRawInfoUpdate(string recipientID, string subject, string content, long sentTime)
    {
        MailObject newMailObject = new MailObject
        {
            recipID = recipientID,
            senderID = Player.playerName,
            subject = subject,
            content = content,
            timeSent = sentTime
        };

        rawOutboxListInfo.Insert(0,newMailObject);
    }

    public void GetRecipientDetails()
    {
        // Loop through the received
        for (int x = 0; x < rawOutboxListInfo.Count; x++)
        {
            if (x == 0)
            {
                GameSparksManager.Instance.GetEnemyDetails(rawOutboxListInfo[x].recipID, 0, "mail");
                totalDifferentPlayerDetails += 1;
            }
            else
            {
                if (x == rawOutboxListInfo.Count)
                    outboxChecked = true;

                for (int y = 0; y < x; y++)
                {
                    if (rawOutboxListInfo[x].recipID == rawOutboxListInfo[y].recipID)
                        break;
                    if (y == x - 1)
                    {
                        GameSparksManager.Instance.GetEnemyDetails(rawOutboxListInfo[x].recipID, x, "mail");
                        totalDifferentPlayerDetails += 1;
                    }
                }
            }
        }
    }
    public void NewPlayerDetailsRetrieved(string playerID, string playerName, string playerClanID, string portrait)
    {
        // ==== Adding to the LIST of loaded mail sending players
        PlayerDetails npDetail = new PlayerDetails
        {
            playerID = playerID,
            playerDisplayName = playerName,
            clanID = playerClanID,
            playerDisplayAvatar = portrait
        };
        playerDetailList.Add(npDetail);

        if (playerDetailList.Count < totalDifferentPlayerDetails)
        {
            if (outboxChecked == false || inboxChecked == false)
                return;
        }

        UpdateMails();
    }
    public void AddLoadedPlayer(string playerID, string playerName, string playerClanID, string portrait)
    {
        if (PlayerIsLoadedCheckWithName(playerName) || PlayerIsLoadedCheckWithID(playerID))
            return;
        PlayerDetails npDetail = new PlayerDetails
        {
            playerID = playerID,
            playerDisplayName = playerName,
            clanID = playerClanID,
            playerDisplayAvatar = portrait
        };
        playerDetailList.Add(npDetail);
    }
    GameObject NewOutboxMailObj()
    {
        GameObject newMailObj = Instantiate(sentMailOBJ, sentMailTransform.position, Quaternion.identity, sentMailTransform);
        return newMailObj;
    }
    string IDToName(string playerID)
    {
        string playerName = "";
        for (int x = 0; x < playerDetailList.Count; x++)
        {
            if (playerDetailList[x].playerID == playerID)
                playerName = playerDetailList[x].playerDisplayName;
        }
        return playerName;
    }
    string IDToAvatar(string playerID)
    {
        string avatar = "";
        for (int x = 0; x < playerDetailList.Count; x++)
        {
            if (playerDetailList[x].playerID == playerID)
                avatar = playerDetailList[x].playerDisplayAvatar;
        }
        return avatar;
    }
    // ======= SENT MAIL AREA END ========\\




    // ======= RECEIVED MAIL AREA ========\\
    [Space(10)]
    [Header("MAIL RECEIVING")]
    public GameObject receivedMailObj;
    public Transform inboxTransform;
    public List<ReceivedMail> inboxMails = new List<ReceivedMail>();
    public ToggleGroup selectedReceivedMailDisplay;

    void RetrieveMailInfo()
    {
        // this includes SENT and RECEIVED mails
        GameSparksManager.Instance.GetMail();
    }

    public void ReceivedMailsRawInfoUpdate(string recipientID, string subject, string content, long sentTime, string senderID)
    {
        MailObject newMailObject = new MailObject
        {
            recipID = recipientID,
            senderID = senderID,
            subject = subject,
            content = content,
            timeSent = sentTime
        };

        rawInboxListInfo.Insert(0, newMailObject);
    }
    public void GetSenderDetails()
    {
        // Loop through the received
        for (int x = 0; x < rawInboxListInfo.Count; x++)
        {
            if (x == 0)
            {
                // Check if the Player Details has been loaded before 
                if (PlayerIsLoadedCheckWithID(rawInboxListInfo[x].senderID))
                    break;
                GameSparksManager.Instance.GetEnemyDetails(rawInboxListInfo[x].senderID, 0, "mail");
                totalDifferentPlayerDetails += 1;
            }
            else
            {
                if (x == rawInboxListInfo.Count)
                    inboxChecked = true;

                for (int y = 0; y < x; y++)
                {
                    if (PlayerIsLoadedCheckWithID(rawInboxListInfo[x].senderID))
                        break;
                    if (rawInboxListInfo[x].senderID == rawInboxListInfo[y].senderID)
                        break;
                    if (y == x - 1)
                    {
                        GameSparksManager.Instance.GetEnemyDetails(rawInboxListInfo[x].senderID, x, "mail");
                        totalDifferentPlayerDetails += 1;
                    }
                }
            }
        }
    }
    GameObject NewInboxMailObj()
    {
        GameObject newMailObj = Instantiate(receivedMailObj, inboxTransform.position, Quaternion.identity, inboxTransform);
        return newMailObj;
    }

    // ====== RECIEVED MAIL AREA END =======\\



    // ==== MAIL OBJ CREATION ==== \\
    void UpdateMails()
    {
        for (int x = 0; x < rawOutboxListInfo.Count; x++)
            OutboxMailCreate(x);

        for (int x = 0; x < rawInboxListInfo.Count; x++)
            InboxMailCreate(x);
        
        SetNotifications();
    }
    void OutboxMailCreate(int mailNo)
    {
        GameObject mailObj = NewOutboxMailObj();
        mailObj.GetComponent<Toggle>().group = selectedSentMailDisplay;
        SentMail newSentMail = mailObj.GetComponent<SentMail>();

        newSentMail.mailDeleteToggled += ToggleDeletePanel;
        newSentMail.MailManage(this);
        mailsSent.Add(newSentMail);
        newSentMail.InfoObtain(
            IDToName(rawOutboxListInfo[mailNo].recipID),
            rawOutboxListInfo[mailNo].recipID,
            IDToAvatar(rawOutboxListInfo[mailNo].recipID),
            rawOutboxListInfo[mailNo].timeSent
        );

        newSentMail.MessageInfoObtain(rawOutboxListInfo[mailNo].subject, rawOutboxListInfo[mailNo].content);

        newSentMail.UpdateInfo();
    }
    void InboxMailCreate(int mailNo)
    {
        GameObject mailObj = NewInboxMailObj();
        mailObj.GetComponent<Toggle>().group = selectedReceivedMailDisplay;
        ReceivedMail newRecvMail = mailObj.GetComponent<ReceivedMail>();

        newRecvMail.mailDeleteToggled += ToggleDeletePanel;
        newRecvMail.MailManage(this);
        inboxMails.Add(newRecvMail);
        newRecvMail.InfoObtain(
            IDToName(rawInboxListInfo[mailNo].senderID),
            rawInboxListInfo[mailNo].senderID,
            IDToAvatar(rawInboxListInfo[mailNo].senderID),
            rawInboxListInfo[mailNo].timeSent
        );

        newRecvMail.MessageInfoObtain(rawInboxListInfo[mailNo].subject, rawInboxListInfo[mailNo].content);

        newRecvMail.UpdateInfo();
    }
    void InboxMailCreate(int mailNo, bool toSet)
    {
        GameObject mailObj = NewInboxMailObj();
        mailObj.transform.SetAsFirstSibling();
        mailObj.GetComponent<Toggle>().group = selectedReceivedMailDisplay;
        ReceivedMail newRecvMail = mailObj.GetComponent<ReceivedMail>();

        newRecvMail.mailDeleteToggled += ToggleDeletePanel;
        newRecvMail.MailManage(this);
        inboxMails.Add(newRecvMail);
        newRecvMail.InfoObtain(
            IDToName(rawInboxListInfo[mailNo].senderID),
            rawInboxListInfo[mailNo].senderID,
            IDToAvatar(rawInboxListInfo[mailNo].senderID),
            rawInboxListInfo[mailNo].timeSent
        );

        newRecvMail.MessageInfoObtain(rawInboxListInfo[mailNo].subject, rawInboxListInfo[mailNo].content);

        newRecvMail.UpdateInfo();
    }
    void SetNotifications()
    {
        CheckInboxMails();
        UpdateMailsButtonNotification();
    }

    void CheckInboxMails()
    {
        if (inboxTransform.childCount > 0)
        {
            for (int x = 0; x < rawInboxListInfo.Count; x++)
            {
                if (rawInboxListInfo[x].timeSent > DataManager.lastLogout)
                {
                    ToggleInboxNotify(true);
                    break;
                }
            }
        }
    }
    void UpdateMailsButtonNotification()
    {
        if (inboxNotify.activeInHierarchy)
            ToggleMailBoxNotify(true);
    }

    // ==== The toggling of the notifications ==== \\
    void ToggleInboxNotify(bool toset)
    {
        inboxNotify.SetActive(toset);
    }
    void ToggleMailBoxNotify(bool toset)
    {
        newMailsNotification.SetActive(toset);
    }
    void ToggleSentMailsNotify(bool toSet)
    {
        sentMailsNotification.SetActive(toSet);
    }
    // ==== MAIL OBJ CREATION END ==== \\



    // ===== PLAYER DETAILS LIST CHECKERS ===== \\
    bool PlayerIsLoadedCheckWithID(string playerID)
    {
        if (playerDetailList.Count >= 1)
        {
            for (int x = 0; x < playerDetailList.Count; x++)
            {
                if (playerID == playerDetailList[x].playerID)
                    return true;
            }
        }
        return false;
    }
    bool PlayerIsLoadedCheckWithName(string playerName)
    {
        if (playerDetailList.Count >= 1)
        {
            for (int x = 0; x < playerDetailList.Count; x++)
            {
                if (playerName.ToLower() == playerDetailList[x].playerDisplayName.ToLower())
                    return true;
            }
        }
        return false;
    }
    string LoadedPlayerID(string playerName)
    {
        string playerId = "";
        if (playerDetailList.Count >= 1)
        {
            for (int x = 0; x < playerDetailList.Count; x++)
            {
                if (playerName.ToLower() == playerDetailList[x].playerDisplayName.ToLower())
                    return playerDetailList[x].playerID;
            }
        }
        return playerId;
    }
    // ==== PLAYER DETAILS LIST CHECKERS END ==== \\



    // ===== MAIL DELETION AREA ===== \\
    public List<SentMail> toBeDeletedSentMails = new List<SentMail>();
    public List<ReceivedMail> toBeDeletedReceivedMails = new List<ReceivedMail>();
    public List<MailObject> toBeDeletedRawMailInfo = new List<MailObject>();

    void ToggleDeletePanel(bool toSet)
    {
        if (CheckWhichBoxToCheck() == "inbox")
        {
            CheckSelectAllToggle("inbox");
            if (InboxGroupHasMailSelected())
                deleteMailsPanel.SetActive(true);
            else
                deleteMailsPanel.SetActive(toSet);
        }
        if (CheckWhichBoxToCheck() == "outbox")
        {
            CheckSelectAllToggle("outbox");
            if (OutboxGroupHasMailSelected())
                deleteMailsPanel.SetActive(true);
            else
                deleteMailsPanel.SetActive(toSet);
        }
    }

    void SelectAllMails(string boxToCheck)
    {
        if (boxToCheck == "inbox")
        {
            for (int x = 0; x < inboxMails.Count; x++)
            {
                if (inboxMails[x].deleteCheckboxToggle.isOn == false)
                    inboxMails[x].deleteCheckboxToggle.isOn = true;
            }
        }
        if (boxToCheck == "outbox")
        {
            for (int x = 0; x < mailsSent.Count; x++)
            {
                if (mailsSent[x].deleteCheckboxToggle.isOn == false)
                    mailsSent[x].deleteCheckboxToggle.isOn = true;
            }
        }
    }
    void DeselectAllMails(string boxToCheck)
    {
        if (boxToCheck == "inbox")
        {
            for (int x = 0; x < inboxMails.Count; x++)
            {
                inboxMails[x].deleteCheckboxToggle.isOn = false;
            }
        }
        if (boxToCheck == "outbox")
        {
            for (int x = 0; x < mailsSent.Count; x++)
            {
                mailsSent[x].deleteCheckboxToggle.isOn = false;
            }
        }
    }
    string CheckWhichBoxToCheck()
    {
        if (inboxToggle.isOn)
            return "inbox";
        return "outbox";
    }

    // ===== CHECK IF MAIL IS SELECTED OR NOT ===== \\
    bool InboxGroupHasMailSelected()
    {
        for (int x = 0; x < inboxMails.Count; x++)
        {
            if (inboxMails[x].deleteCheckboxToggle.isOn == true)
                return true;
        }
        return false;
    }
    bool OutboxGroupHasMailSelected()
    {
        for (int x = 0; x < mailsSent.Count; x++)
        {
            if (mailsSent[x].deleteCheckboxToggle.isOn == true)
                return true;
        }
        return false;
    }
    bool InboxGroupHasMailNotSelected()
    {
        for (int x = 0; x < inboxMails.Count; x++)
        {
            if (inboxMails[x].deleteCheckboxToggle.isOn == false)
                return true;
        }
        return false;
    }
    bool OutboxGroupHasMailNotSelected()
    {
        for (int x = 0; x < mailsSent.Count; x++)
        {
            if (mailsSent[x].deleteCheckboxToggle.isOn == false)
                return true;
        }
        return false;
    }

    void CheckSelectAllToggle(string boxToCheck)
    {
        if (boxToCheck == "inbox")
        {
            for (int x = 0; x < inboxMails.Count; x++)
            {
                if (inboxMails[x].deleteCheckboxToggle.isOn == false)
                {
                    selectAllImage.SetActive(false);
                    return;
                }
            }
        }
        else if (boxToCheck == "outbox")
        {
            for (int x = 0; x < mailsSent.Count; x++)
            {
                if (mailsSent[x].deleteCheckboxToggle.isOn == false)
                {
                    selectAllImage.SetActive(false);
                    return;
                }
            }
        }
        selectAllImage.SetActive(true);
    }

    void ToggleAllSelectionMails()
    {
        if (CheckWhichBoxToCheck() == "inbox")
        {
            if (InboxGroupHasMailNotSelected())
                SelectAllMails("inbox");
            else
                DeselectAllMails("inbox");
        }
        else if (CheckWhichBoxToCheck() == "outbox")
        {
            if (OutboxGroupHasMailNotSelected())
                SelectAllMails("outbox");
            else
                DeselectAllMails("outbox");
        }
    }

    void AttemptDeleteSelectedMails()
    {
        DisplayEventHandling("Deleting Mail . . . ", true, false);
        if (CheckWhichBoxToCheck() == "inbox")
        {
            SetDeleteAmount("inbox");
            // delete inbox mail stuffs here
            for (int x = 0; x < inboxMails.Count; x++)
            {
                if (inboxMails[x].deleteCheckboxToggle.isOn == true)
                    GameSparksManager.Instance.DeleteMail(inboxMails[x].targetID, inboxMails[x].mailSentTime);
            }
            //for (int x = 0; x < inboxMails.Count; x++)
            //{
            //    if (inboxMails[x].deleteCheckboxToggle.isOn == true)
            //        GameSparksManager.Instance.DeleteMail(inboxMails[x].senderID, inboxMails[x].mailSentTime);
            //}

        }
        else if (CheckWhichBoxToCheck() == "outbox")
        {
            SetDeleteAmount("outbox");
            // delete outbox mail stuffs here
            for (int x = 0; x < mailsSent.Count; x++)
            {
                if (mailsSent[x].deleteCheckboxToggle.isOn == true)
                    GameSparksManager.Instance.DeleteMail(GameSparksManager.playerId, mailsSent[x].mailSentTime);
            }
        }
    }
    void SetDeleteAmount(string boxToTrack)
    {
        if (boxToTrack == "inbox")
        {
            for (int x = 0; x < inboxMails.Count; x++)
            {
                if (inboxMails[x].deleteCheckboxToggle.isOn == true)
                {
                    toBeDeletedReceivedMails.Add(inboxMails[x]);
                    toBeDeletedRawMailInfo.Add(rawInboxListInfo[x]);
                }
            }
        }
        else if (boxToTrack == "outbox")
        {
            for (int x = 0; x < mailsSent.Count; x++)
            {
                if (mailsSent[x].deleteCheckboxToggle.isOn == true)
                {
                    toBeDeletedSentMails.Add(mailsSent[x]);
                    toBeDeletedRawMailInfo.Add(rawOutboxListInfo[x]);
                }
            }
        }
    }
    public void MailDeleteResponse(string senderID, long timeStamp)
    {
        numberOfMailsResponsesObtained += 1;

        if (numberOfMailsResponsesObtained < toBeDeletedRawMailInfo.Count)
            return;

        RemoveRawMailInfo();
        ClearList();

        DisplayEventHandling("Mail deleted", true, true);
        confirmDeleteMailsPanel.SetActive(false);
        deleteMailsPanel.SetActive(false);

        EmptyTabCheck();
    }
    void RemoveRawMailInfo()
    {
        if (CheckWhichBoxToCheck() == "inbox")
        {
            for (int x = 0; x < toBeDeletedRawMailInfo.Count; x++)
            {
                for (int y = 0; y < rawInboxListInfo.Count; y++)
                {
                    if (toBeDeletedRawMailInfo[x] == rawInboxListInfo[y])
                    {
                        rawInboxListInfo.RemoveAt(y);
                        break;
                    }
                }
            }
            for (int x = 0; x < toBeDeletedReceivedMails.Count; x++)
            {
                for (int y = 0; y < inboxMails.Count; y++)
                {
                    if (toBeDeletedReceivedMails[x] == inboxMails[y])
                    {
                        inboxMails[y].DeleteMail();
                        break;
                    }
                }
            }
        }
        else if (CheckWhichBoxToCheck() == "outbox")
        {
            for (int x = 0; x < toBeDeletedRawMailInfo.Count; x++)
            {
                for (int y = 0; y < rawOutboxListInfo.Count; y++)
                {
                    if (toBeDeletedRawMailInfo[x] == rawOutboxListInfo[y])
                    {
                        rawOutboxListInfo.RemoveAt(y);
                        break;
                    }
                }
            }
            for (int x = 0; x < toBeDeletedSentMails.Count; x++)
            {
                for (int y = 0; y < mailsSent.Count; y++)
                {
                    if (toBeDeletedSentMails[x] == mailsSent[y])
                    {
                        mailsSent[y].DeleteMail();
                        break;
                    }
                }
            }
        }
    }
    public void RemoveRawExpiredMailInfo(BaseMail basemailInfoToExpire, string boxType)
    {
        if (boxType == "outbox")
        {
            for (int x = 0; x < mailsSent.Count; x++)
            {
                if (basemailInfoToExpire == mailsSent[x])
                    mailsSent.RemoveAt(x);
            }
            for (int x = 0; x < rawOutboxListInfo.Count; x++)
            {
                if (basemailInfoToExpire.mailSentTime == rawOutboxListInfo[x].timeSent)
                    rawOutboxListInfo.RemoveAt(x);
            }
        }
        else if (boxType == "inbox")
        {
            for (int x = 0; x < inboxMails.Count; x++)
            {
                // means the expired mail is from the inbox
                if (basemailInfoToExpire == inboxMails[x])
                    inboxMails.RemoveAt(x);
            }
            for (int x = 0; x < rawInboxListInfo.Count; x++)
            {
                if (basemailInfoToExpire.targetID == rawInboxListInfo[x].senderID)
                    rawInboxListInfo.RemoveAt(x);
            }
        }
    }
    public void DeleteThisMail(BaseMail mailToDelete)
    {
        if (CheckWhichBoxToCheck() == "inbox")
        {
            for (int x = 0; x < inboxMails.Count; x++)
            {
                if (inboxMails[x] == mailToDelete)
                {
                    inboxMails.RemoveAt(x);
                    break;
                }
            }
        }
        else if (CheckWhichBoxToCheck() == "outbox")
        {
            for (int x = 0; x < mailsSent.Count; x++)
            {
                if (mailsSent[x] == mailToDelete)
                {
                    mailsSent.RemoveAt(x);
                    break;
                }
            }
        }
        EmptyTabCheck();
    }
    void ClearList()
    {
        toBeDeletedReceivedMails.Clear();       // the mail OBJ of received
        toBeDeletedSentMails.Clear();              // the mail OBJ of sent
        toBeDeletedRawMailInfo.Clear();          // the raw info of the mail
    }
    void EmptyTabCheck()
    {
        for (int x = 0; x < emptyTabsDisplay.Length; x++)
        {
            emptyTabsDisplay[x].TriggerCheck();
        }
    }
    // ==== MAIL DELETION AREA END ==== \\


    void Start()
    {
        ListenToMailReceiver();
        RetrieveMailInfo();

        attemptSendMailButton.onClick.AddListener(AttemptSendMail);
        selectAllMailsButton.onClick.AddListener(ToggleAllSelectionMails);
        attemptDeleteMailsButton.onClick.AddListener(AttemptDeleteSelectedMails);

        inboxToggle.onValueChanged.AddListener(UpdateTime);
    }

    void ListenToMailReceiver()
    {
        GameSparksManager.MailIncoming += NewMailReceived;
    }

    void NewMailReceived(string senderID, string senderName, string subject, string content, long timeSent, string avatar, string clan)
    {
        AddLoadedPlayer(senderID, senderName, clan, avatar);
        ReceivedMailsRawInfoUpdate(GameSparksManager.playerId, subject, content, timeSent, senderID);
        InboxMailCreate(0, true);
        ToggleInboxNotify(true);
        UpdateMailsButtonNotification();
    }


    void UpdateTime(bool isOn)
    {
        if (isOn)
        {
            for (int x = 0; x < inboxMails.Count; x++)
                inboxMails[x].UpdateInfo();
            for (int y = 0; y < mailsSent.Count; y++)
                mailsSent[y].UpdateInfo();
        }
    }

    // ===== RESPONSE HANDLERS ====== \\
    public void DisplayEventHandling(string messageToDisplay,bool toSet, bool tapToSet)
    {
        SetResponseText(messageToDisplay);
        SetTappableToClose(tapToSet);
        ToggleResponseOBJ(toSet);
    }
    void SetTappableToClose(bool isInteractable)
    {
        loadingScreenTapToClose.interactable = isInteractable;
    }
    void SetResponseText(string notificationMessage)
    {
        responseText.text = notificationMessage;
    }
    void ToggleResponseOBJ(bool toSet)
    {
        responeOBJ.SetActive(toSet);
    }
    // ===== RESPONSE HANDLERS END ====== \\



    // ===== LOADING POPUPS ===== \\
    void ToggleLoadingScreen(bool toSet)
    {
        loadingScreen.SetActive(toSet);
    }
    // ===== LOADING POPUPS ===== \\
}
