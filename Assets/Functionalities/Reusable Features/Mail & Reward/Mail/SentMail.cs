﻿using UnityEngine;
using UnityEngine.UI;

public class SentMail : BaseMail
{
    [Header("---------- SENT MAIL SPECIFICS ---------")]

    [Header("Mail Info")]
    public string mailContent;       // raw mail body content info

    [Space(10)]
    [Header("Mail UI")]
    public Text mailBodyText;       // what was sent in this message
    public Text senderText;
    public Text recipientText;



    protected override void Start()
    {
        base.Start();
    }

    protected override void ToggleMail(bool toSet)
    {
        ToggleMailContentTextDisplay(toSet);
        base.ToggleMail(toSet);
    }

    void ToggleMailContentTextDisplay(bool toSet)
    {
        mailBodyText.gameObject.SetActive(toSet);
        senderText.gameObject.SetActive(toSet);
        recipientText.gameObject.SetActive(toSet);
    }

    public override void UpdateInfo()
    {
        base.UpdateInfo();

        mailBodyText.text = mailBody;

        senderText.text = "From : " + Player.playerName;
        recipientText.text = "To : " + targetName;
    }
}
