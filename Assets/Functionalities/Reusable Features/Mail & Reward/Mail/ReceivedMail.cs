﻿using UnityEngine;
using UnityEngine.UI;

public class ReceivedMail : SentMail
{
    [Header("---------- INBOX MAIL SPECIFICS ---------")]

    [Space(10)]
    [Header("Sender Info")]
    public string senderID;

    [Space(10)]
    [Header("Sender Info UI")]
    public Text senderNameText;


    public override void UpdateInfo()
    {
        base.UpdateInfo();

        targetNameText.text = "From : " + targetName;

        mailBodyText.text = mailBody;

        recipientText.text = "To : " + Player.playerName;
        senderText.text = "From : " + targetName;
    }
    public override void ExpireMail()
    {
        GameSparksManager.Instance.DeleteExpiredMail(targetID, mailSentTime, this);
    }
}
