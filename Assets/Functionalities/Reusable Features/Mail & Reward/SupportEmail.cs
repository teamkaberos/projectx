﻿using UnityEngine;

public class SupportEmail : MonoBehaviour
{
    public static bool URLOpened;

    string helpPageURL = "https://teamkaberos.wixsite.com/newmediaint/xcalibur-faq";



    public void Support_Email()
    {
        URLOpened = true;
        Application.OpenURL("mailto:support.xcalibur@kaberos.com");
    }

    void SendEmail()
    {
        string email = "me@example.com";
        string subject = MyEscapeURL("My Subject");
        string body = MyEscapeURL("My Body\r\nFull of non-escaped chars");
        Application.OpenURL("mailto:" + email + "?subject=" + subject + "&body=" + body);
    }
    string MyEscapeURL(string url)
    {
        return WWW.EscapeURL(url).Replace("+", "%20");
    }

    public void GameGuide()
    {
        URLOpened = true;
        //Application.OpenURL(" https://teamkaberos.wixsite.com/newmediaint");
        Application.OpenURL(helpPageURL);
    }
   

}
