﻿using UnityEngine;

public class EmptyTabDisplay : MonoBehaviour
{
    public Transform toBeCheckIfChildPresent;
    public GameObject toBeToggledObject;


    public void TriggerCheck()
    {
        toBeToggledObject.SetActive(HasChildCount());
    }

    void OnEnable()
    {
        TriggerCheck();
    }
    bool HasChildCount()
    {
        if (toBeCheckIfChildPresent.childCount >= 1)
            return false ;
        return true;
    }
}
