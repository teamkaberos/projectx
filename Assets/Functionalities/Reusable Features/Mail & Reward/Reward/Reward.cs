﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class Reward : MonoBehaviour 
{
    public delegate void RewardClaimed();
    public static event RewardClaimed rewardClaimed;

    ReceiveManager receiveManager;

    // ==== this mail's info ==== \\
    [HideInInspector] public string rewardID;
    [HideInInspector] public string rewardReason;
    [HideInInspector] public string rewardReceivedType;
    [HideInInspector] public int rewardAmountValue;
    [HideInInspector] public int rewardDaysLeft;

    // ==== this mail's display info ==== \\
    public GameObject rewardImageHolder;
    public Text rewardReasonText;
    public Text rewardAmountText;
    public Text rewardDurationText;

    bool timedReward;


    void OnEnable()
    {
        if (timedReward)
            UpdateTimeLeftInfo();
    }



    // ==== Reward OBJ Initializing ==== \\
    public virtual void InputValues(string rewardReason_, string rewardType_, int rewardValue_, string rewardID_, int rewardDaysLeft_)
	{
        rewardDaysLeft = rewardDaysLeft_;
        rewardID = rewardID_;
        rewardReason = rewardReason_;
        rewardReceivedType = rewardType_;
        rewardAmountValue = rewardValue_;

        timedReward = true;

    }
    public void CreateMyImage()
    {
        for (int x = 0; x < rewardImageHolder.transform.childCount; x++)
        {
            if (rewardImageHolder.transform.GetChild(x).name.ToLower().Contains(rewardReceivedType.ToLower()))
            {
                rewardImageHolder.transform.GetChild(x).gameObject.SetActive(true);
                break;
            }
        }
	}
    public void UpdateDisplayInfo()
    {
        rewardReasonText.text = rewardReason;
        rewardAmountText.text = ValueParser();
        UpdateTimeLeftInfo();
    }
    void UpdateTimeLeftInfo()
    {
        if (DateTime.UtcNow.Day == TimeMaster.loginTime.Day)
            rewardDurationText.text = TimeToNextDay();
        else
            GiveawayDestroy();
    }
    public void ButtonBehaviourListener()
    {
        GetComponent<Button>().onClick.AddListener(AttemptClaimGiveaway);
    }
    public void ReferenceReceiveManager(ReceiveManager rm)
    {
        receiveManager = rm;
    }

    string ValueParser()
    {
        if (rewardAmountValue > 1000)
        {
            if (rewardAmountValue > 1000000)
            {
                return string.Format("x {0:n0}m", rewardAmountValue / 1000000);
            }
            return string.Format("x {0:n0}k", rewardAmountValue / 1000);
        }
        return rewardAmountValue.ToString();
    }
    string TimeToNextDay()
    {
        DateTime currentUtc = DateTime.UtcNow;
        DateTime toSubtractFrom = DateTime.UtcNow.AddDays(1)
                                          .AddHours(-currentUtc.Hour)
                                          .AddMinutes(-currentUtc.Minute)
                                          .AddSeconds(-currentUtc.Second);

        TimeSpan timeToNextDay = toSubtractFrom.Subtract(currentUtc);

        if (rewardDaysLeft == 1)
            return string.Format("Expires in {0}h", timeToNextDay.Hours);
        return string.Format("Expires in {0}d {1}h", rewardDaysLeft - 1, timeToNextDay.Hours);
    }
    // ==== Reward OBJ Initializing End ==== \\

    
    
    // ==== Target Giveaways init ==== \\
    public void InputValues(ReceiveManager.TargetGiveaways targetGiveaways)
    {
        rewardID = targetGiveaways.rewardID;
        rewardReceivedType = targetGiveaways.rewardType;
        rewardAmountValue = targetGiveaways.rewardValue;

        rewardDurationText.gameObject.SetActive(false);
        rewardAmountText.text = ValueParser();

        CreateMyImage();
        RewardReasonDisplay();
        AddClaimListener();
    }
    void RewardReasonDisplay()
    {
        rewardReasonText.text = string.Format("Received {0:n0} {1}s from Admin.", rewardAmountValue, rewardReceivedType.ToUpper());
    }
    void AddClaimListener()
    {
        GetComponent<Button>().onClick.AddListener(AttemptClaimTargetedGift);
    }



    // ==== Giveaway OBJ reward retrieving ==== \\
    public void AttemptClaimGiveaway()
    {
        receiveManager.LoadingResponsePopup("Retrieving Giveaway reward...");
        receiveManager.AttemptClaimReward(this);
    }
    void AttemptClaimTargetedGift()
    {
        receiveManager.LoadingResponsePopup("Retrieving Giveaway reward...");
        receiveManager.AttemptClaimTargetedGift(this);
    }
    // ==== Giveaway OBJ reward retrieving END ==== \\

    public void ClaimThisTargetedGift()
    {
        receiveManager.TargetedGiveawaySuccess(this);
        GiveawayDestroy();
    }


    // ==== Giveaway OBJ reward retrieved ==== \\
    public void GiveawayDestroy()
    {
        Destroy(gameObject, 0.02f);
    }
    // ==== Giveaway OBJ reward retrieved END ==== \\

}
