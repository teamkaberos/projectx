﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class ReceiveManager : MonoBehaviour
{
    [System.Serializable]
    public class CurrentGiveawaysAvailable
    {
        public string rewardReason;
        public string rewardType;
        public string rewardID;
        public int rewardValue;
        public int rewardDaysLeft;
    }
    [System.Serializable]
    public struct TargetGiveaways
    {
        public string rewardType;
        public int rewardValue;
        public string rewardID;
    }

    [Header("UI OBJs")]
    public List<CurrentGiveawaysAvailable> currentGiveawaysAvailables;    // list of giveaways that are happening now
    public List<Reward> unclaimedRewards;                                               // list of rewards that are available for claiming
    public List<string> claimedRewardsID;                                                  // list of rewards that are already claimed

    public List<TargetGiveaways> currentAvailableGiveaways;


    [Space(15)]
    [Header("UI OBJs")]
    // ==== UI Displays ==== \\
    public List<GameObject> rewardOBJList;          // reference to the list of reward obj that is 
    public GameObject rewardObj;                         // the reward obj that is to be instantiated
    public Transform rewardPanel;                         // the reward panel that holds the instantiated reward obj 

    [Space(15)]
    [Header("Response Display")]
    // ==== Response Handles ==== \\
    public Text rewardReceivedText;                     // the text that will display what the player recieved
    public GameObject rewardReceivedPanel;        // the popup that displays the reward received text
    public GameObject loadingIndication;             // the spinning tri object that indicates loading

    [Header("Mails notification")]
    public GameObject newMailsNotification;
    public GameObject systemMailNotify;

    public static bool gmGiveawaysRetrieved;
    public static bool targetedGiveawaysRetrieved;





    void Start()
    {
        // Look in the cloud to see if there are any giveaways
        GameSparksManager.Instance.CheckGiveaways(this);                    // this is to check the available Giveaways
        GameSparksManager.Instance.GetTargetedRewards(this);

        GameSparksManager.TargetedMailIncoming += CreateRetrievedTargetedGiveaways;
    }
    void OnDestroy()
    {
        gmGiveawaysRetrieved = false;
        targetedGiveawaysRetrieved = false;
    }



    // ** global giveaways from dev ** \\
    public void AddCurrentGiveaways(string rewardReason_, string rewardType_, int rewardValue_, string rewardID_, int rewardDaysLeft_)
    {
        currentGiveawaysAvailables.Add(new CurrentGiveawaysAvailable
        {
            rewardReason = rewardReason_,
            rewardType = rewardType_,
            rewardValue = rewardValue_,
            rewardID = rewardID_,
            rewardDaysLeft = rewardDaysLeft_
        });
    }
    public void GiveawaysRetrieved()
    {
        // Because both giveaways uses this method to check whether they are claimed, we need a bool guard to ensure it only triggers once
        if (gmGiveawaysRetrieved == true && targetedGiveawaysRetrieved == true)
        {
            // after getting all the giveaways that are found in the cloud, we use the giveaway id to check if the player has claimed it before
            for (int x = 0; x < currentGiveawaysAvailables.Count; x++)
            {
                GameSparksManager.Instance.CheckClaimedGiveaways(this, currentGiveawaysAvailables[x].rewardID);
            }
        }
    }
    // ** targeted giveaways from dev ** \\
    public void CreateRetrievedTargetedGiveaways(string objID, string rewardType, int rewardValue)
    {
        GameObject rwdOBJ = Instantiate(rewardObj, rewardPanel.position, Quaternion.identity, rewardPanel);
        rwdOBJ.GetComponent<Reward>().InputValues(new TargetGiveaways {rewardID = objID,rewardType = rewardType, rewardValue = rewardValue});
        rwdOBJ.GetComponent<Reward>().ReferenceReceiveManager(this);

        rwdOBJ.transform.SetAsFirstSibling();

        NotificationToggle();
    }



    // ==== Giveaways checks are initialized ==== \\\\
    public void StoreRetrievedRewardsID(string IDClaimed)
    {
        claimedRewardsID.Add(IDClaimed);
    }



    // ==== Giveaways Initializing ====\\
    public void CreateRewardOBJ(string rewardID_)
    {
        for (int x = 0; x < currentGiveawaysAvailables.Count; x++)
        {
            if (rewardID_ == currentGiveawaysAvailables[x].rewardID)
            {
                UpdateRewardOBJ(InitRewardOBJ(), currentGiveawaysAvailables[x]);
                break;
            }
        }
        NotificationToggle();
    }
    Reward InitRewardOBJ()
    {
        // Creating a new rewardOBJ
        GameObject rwdOBJ = Instantiate(rewardObj, rewardPanel.position, Quaternion.identity, rewardPanel);

        // Add this gameobject to the list
        rewardOBJList.Add(rwdOBJ);

        // Get reference and add to list
        Reward thisReward = rwdOBJ.GetComponent<Reward>();
        unclaimedRewards.Add(thisReward);

        return thisReward;
    }
    void UpdateRewardOBJ(Reward newRwd, CurrentGiveawaysAvailable thisGiveaway)
    {
        newRwd.InputValues(thisGiveaway.rewardReason, thisGiveaway.rewardType, thisGiveaway.rewardValue, thisGiveaway.rewardID, thisGiveaway.rewardDaysLeft);
        newRwd.CreateMyImage();
        newRwd.UpdateDisplayInfo();
        newRwd.ButtonBehaviourListener();
        newRwd.ReferenceReceiveManager(this);
    }

    void NotificationToggle()
    {
        if (rewardPanel.childCount >= 1)
        {
            ToggleSystemMailNotify(true);
            ToggleNewMailNotification(true);
        }

    }
    void ToggleSystemMailNotify(bool toset)
    {
        systemMailNotify.SetActive(toset);
    }
    void ToggleNewMailNotification(bool toSet)
    {
        newMailsNotification.SetActive(true);
    }
    // ==== Giveaways Initializing End ====\\


    // ==== Giveaway Buttons behaviour ==== \\
    public void AttemptClaimReward(Reward rewardClaim)
    {
        GameSparksManager.Instance.ClaimGiveaway(this, rewardClaim);
    }

    public void AttemptClaimTargetedGift(Reward targetedGift)
    {
        GameSparksManager.Instance.ClaimTargetedGift(this, targetedGift);
    }


    public void GiveawayReceivedSuccess(Reward reward)
    {
        for (int x = 0; x < unclaimedRewards.Count; x++)
        {
            if (reward == unclaimedRewards[x])
            {
                // Reward the player with this giveaway
                AddReward(reward.rewardReceivedType, reward.rewardAmountValue);
                // destroy the instantiated obj,
                reward.GiveawayDestroy();

                // Remove reward from the Reward List
                unclaimedRewards.RemoveAt(x);
                break;
            }
        }
        // update the player's UI,
        UIManager.Instance.Update_All();

        // finally do the popup and notify the player of the retrieval
        GiveawayClaimedSuccess(reward);
    }
    public void TargetedGiveawaySuccess(Reward reward)
    {
        AddReward(reward.rewardReceivedType, reward.rewardAmountValue);
        UIManager.Instance.Update_All();
        GiveawayClaimedSuccess(reward);
    }
    void AddReward(string rewardType_, int rewardValue_)
    {
        if (rewardType_.Contains("diamond"))
            Player.Instance.GainDiamond(rewardValue_);
        else if (rewardType_.Contains("gold"))
            Player.Instance.AddGold(rewardValue_);
        else if (rewardType_.Contains("xcalipoint"))
            Player.Instance.ExcaliPoint_Calculator(true, rewardValue_);
        else if (rewardType_.Contains("reputation"))
            Player.Instance.AlterRepValue(true, rewardValue_);

        // Inform DataManager of the update
        DataManager.Instance.PrepareDataForSparksPushing();
        GameSparksManager.Instance.SavePlayerData(GameSparksManager.playerId);
        GameSparksManager.Instance.SavePlayerDetails();
        GameSparksManager.Instance.SavePlayerExpendables(GameSparksManager.playerId);
    }

    // ==== Giveaway Buttons behaviour End ==== \\



    // ==== Event Display Handling ==== \\
    public void LoadingResponsePopup(string responseMessage)
    {
        // Use this function if player attempts to load something from cloud
        SetTextToBeDisplayed(responseMessage);
        ToggleLoadingIndicator(true);
        ToggleMessageNotice(true);
    }
    public void ResponsePopup(string responseMessage)
    {
        // Use this function if player recieves response from cloud
        SetTextToBeDisplayed(responseMessage);
        ToggleLoadingIndicator(false);
        ToggleMessageNotice(true);
    }
    void GiveawayClaimedSuccess(Reward reward)
    {
        // Use this function if the Giveaway claim was successful
        GiveawayClaimed(reward.rewardReceivedType, reward.rewardAmountValue);
        ToggleLoadingIndicator(false);
        ToggleMessageNotice(true);
    }

    #region Event Handle Setters and Toggles
    void GiveawayClaimed(string rewardType_, int rewardAmount_)
    {
        SetTextToBeDisplayed(string.Format("Successfully claimed {0:n0} {1}", rewardAmount_, rewardType_.ToUpper()));
    }
    void SetTextToBeDisplayed(string whatIsGoingToBeDisplayed)
    {
        rewardReceivedText.text = whatIsGoingToBeDisplayed;
    }
    void ToggleLoadingIndicator(bool toDisplay)
    {
        loadingIndication.SetActive(toDisplay);
    }
    void ToggleMessageNotice(bool toDisplay)
    {
        rewardReceivedPanel.SetActive(toDisplay);
    }
    #endregion

}
