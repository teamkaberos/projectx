﻿using UnityEngine;
using UnityEngine.UI;

public class Interval_Rewards : MonoBehaviour
{
    #region singleton
    private static Interval_Rewards _instance;
	public static Interval_Rewards Instance
	{
		get
		{
			return _instance;
		}
	}
    #endregion

    // For setting how long the countdown timer will be
    [Range(0,12)]
	public int hours;
	[Range(0,60)]
	public int minutes;
	[Range(0,60)]
	public int seconds;

	public static float timeTillNextReward;

    public float ttnrDisplay;

    [HideInInspector]
	public bool rewardAvailable;


	public Text reward_Info_Text;               // For displaying whether the Reward is ready for collection or not
	public Text time_Remaining_Text;        // For displaying the remaining timer before the next reward is available
	public Button rewardButton;                 // For collecting the reward when readied
	public GameObject tapToClose;           // For closing the reward panel

	public Text reward_Obtained_Text;		// For displaying items the Player obtained when reward claimed
    public Image rewardObtainedImage;

	public GameObject Interval_Reward_Popup;    // The Interval Reward Panel

	public Animator animator;

    bool beginCountdown;

    static bool isInit;

    public GameObject redNotificationOBJ;


	void Awake()
	{
		_instance = this;
	}

    void Update()
    { 
        ttnrDisplay = timeTillNextReward;

        if (beginCountdown)
        {
            if (!rewardAvailable)
            {
                timeTillNextReward -= Time.deltaTime;

                DisplayText(Format_Remaining_Time(timeTillNextReward));

                if (timeTillNextReward <= 0f)
                {
                    Able_To_Collect();
                }
            }
        }
    }

    void OnDestroy()
    {
        SetInitialized(false); 
    }

    // This method returns the total value of the Time To Next Reward
    float Set_Cooldown_Timer()
    {
        // This is used  to calculate how long the interval reward will be ready
        float intervalDuration = seconds + (minutes * 60f) + (hours * 3600f);
        return intervalDuration;
    }

    public void Check_If_Interval_Reward_Ready()
    {
        //Calculate the total cooldown timer required for every interval as a float value
        float intervalRequired = Set_Cooldown_Timer();

        if ( Player.tutorial < Player.tutorialCap)
            return;

        // Get the time difference between the last claimed time and current server time
        if (!isInit)
        {
            long timeDifference = TimeMaster.Instance.CalculateTimeDifference(DataManager.initialLoginServerTime, DataManager.lastLogout);

            // ==== Cap duration ==== \\ in the case the time exceeds the actual duration
            if (timeTillNextReward > Set_Cooldown_Timer())
                timeTillNextReward = Set_Cooldown_Timer();

            timeTillNextReward -= timeDifference;
            if (timeTillNextReward <= 0)
                timeTillNextReward = 0.02f;

            if (intervalRequired > timeDifference)
            {
                Reward_Collected();
                Unable_To_Collect();
            }
            else if (intervalRequired <= timeDifference)
            {
                if (Player.tutorial >= Player.tutorialCap)
                {
                    Enable_Reward_Collection();
                    Able_To_Collect();
                }
            }
            SetInitialized( true );
        }
        beginCountdown = true;
	}

    // ==== Outcome of Check_If_Interval_Reward_Ready ==== \\
    public void Unable_To_Collect()
    {
        Reward_Collected();
        Disable_Reward_Collection_Button();
        Enable_Tap_To_Close_BG();
        ToggleChestOpen(true);
        ToggleNotificationPop(false);
    }
    public void Able_To_Collect()
    {
        Enable_Reward_Collection();
        DisplayRewardAvailable();
        Enable_Reward_Collection_Button();
        Disable_Tap_To_Close_BG();
        ToggleNotificationPop(true);
        //Interval_Reward_Pop(); 
        ToggleChestOpen(false);  
    }

	string Format_Remaining_Time(float remainingTime)
	{
		int hours = Mathf.FloorToInt(remainingTime/3600f);
		int minutes = Mathf.FloorToInt((remainingTime-(3600f*hours))/60f);
		int seconds = Mathf.FloorToInt(remainingTime - (3600f*hours) - (minutes * 60f));

		if (seconds >= 60)
			minutes += 1;
		if (minutes >= 60)
			hours += 1;

		return string.Format("Reward available in : {0:00}:{1:00}:{2:00}" , hours,minutes,seconds);
	}
	
    // == WHAT HAPPENS WHEN PLAYERS COLLECT REWARD == \\
	void Reward_Collected()
	{
		rewardAvailable = false;
	}
	void Disable_Reward_Collection_Button()
	{
		rewardButton.interactable = false;
	}
	void Display_Reward_Gotten()
	{
		reward_Obtained_Text.text = "Congratulations, you received " + LootManager.Instance.rewardGotten();
        rewardObtainedImage.sprite = LootManager.Instance.RewardGottenSprite();
		reward_Obtained_Text.gameObject.SetActive(true);
	}
	void Enable_Tap_To_Close_BG()
	{
		tapToClose.SetActive(true);
        DisplayRewardInfoText("Preparing your reward");
	}

    // == WHAT HAPPENS WHEN REWARD IS NOT COLLECTED == \\
	void Enable_Reward_Collection()
	{
		rewardAvailable = true;
	}
    void DisplayRewardAvailable()
    {
        DisplayText("Reward available");
    }
	void Enable_Reward_Collection_Button()
	{
		rewardButton.interactable = true;
	}
	void Disable_Tap_To_Close_BG()
	{
		tapToClose.SetActive(false);
        DisplayRewardInfoText("Tap to claim");
	}
	void ToggleNotificationPop(bool toSet)
    {
        redNotificationOBJ.SetActive(toSet);
    }
    void Interval_Reward_Pop()
	{
		Interval_Reward_Popup.SetActive(true);
	}

    // == CHEST ANIMATION == \\
    void ToggleChestOpen(bool isOpened)
	{
		animator.SetBool("ChestOpen",isOpened);
	}


    // === TO BE TAPPED ON THE CHEST THAT POPS UP === \\
    public void Collect_Interval_Reward()  
    {
        //This is to prevent multiple taps on the button
        //in the case where server hasn't return a value
        Disable_Reward_Collection_Button();
        DisplayText("Retrieving Reward");
        DisplayRewardInfoText("Reward Incoming");
        // Save the value into the server
        GameSparksManager.Instance.SaveIntervalReward(true);
    }


    // === CHECK WHETHER THE REWARD IS OBTAINED === \\
    public void IntervalRewardResultHandler(bool hasSucceeded)
    {
        if (hasSucceeded)
        {
            // not available to be collected
            LootManager.Instance.Get_Interval_Reward();

            ToggleNotificationPop(false);

            Reward_Collected();
            ToggleChestOpen(true);
            Display_Reward_Gotten();
            Invoke("Enable_Tap_To_Close_BG", 0.8f);
            timeTillNextReward = Set_Cooldown_Timer();

            GameSparksManager.Instance.SaveIntervalReward(false);
        }
        else
        {
            // reward is available
            Enable_Reward_Collection();
            Enable_Reward_Collection_Button();
        }
    }
	

    public void SetInitialized(bool isSet)
    {
        isInit = isSet;
    }

    // === Text formatting === \\
    void DisplayText(string textToDisplay)
    {
        time_Remaining_Text.text = textToDisplay;
    }
    void DisplayRewardInfoText(string textToDisplay)
    {
        reward_Info_Text.text = textToDisplay;
    }

	#region William's Badlands
	#endregion
}
