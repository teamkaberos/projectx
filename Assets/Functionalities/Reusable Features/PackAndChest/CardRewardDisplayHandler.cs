﻿using UnityEngine;
using UnityEngine.UI;

public class CardRewardDisplayHandler : MonoBehaviour
{
    public LootCardHandler[] packsRewards;
    public Button displayAll;                   // display all items
    public GameObject blocker;

    void OnEnable()
    {
        ToggleOpenAllButton(true);
        ToggleBlocker(false);
    }

    void Start()
    {
        displayAll.onClick.AddListener(OpenUnopened);
    }

    void OpenUnopened()
    {
        ToggleBlocker(true);
        for (int x = 0; x < packsRewards.Length; x++)
        {
            if (packsRewards[x].gameObject.activeInHierarchy)
            {
                packsRewards[x].OpenAllUnopened();
                break;
            }
        }
    }

    public void RevealComplete()
    {
        ToggleOpenAllButton(false);
        ToggleBlocker(false);
    }

    void ToggleOpenAllButton(bool toSet)
    {
        displayAll.gameObject.SetActive(toSet);
    }
    void ToggleBlocker(bool toSet)
    {
        blocker.SetActive(toSet);
    }
}
