﻿using UnityEngine;
using UnityEngine.UI;

public class PlayOpenCard : MonoBehaviour
{
    public delegate void NewCard(string heroName, Sprite heroImage);
    public static event NewCard NewCardReceived;

    public bool openState;
    public bool isNew;
    public string heroName;
    public int heroID;

    public Animator anim;
    public Image cardBackImage;
    public Image flash;
    public GameObject highlight;


    public void SetOpenState(bool isOpen)
    {
        // sets state to check if all has been opened
        openState = isOpen;
    }

    public void ToggleCard(bool toPlay)
    {
        // plays the animation
        anim.SetBool("OpenCard", toPlay);
    }


    public void CheckIfNew()
    {
        if (isNew == true)
        {
            NewCardReceived(heroName, UnitManager.Instance.heroInfomation[heroID].newHeroImage);
        }
    }


    public void ResetToBase()
    {
        isNew = false;
        cardBackImage.color = new Vector4(1, 1, 1, 1);
        flash.color = new Vector4(1, 1, 1, 0);
        highlight.SetActive(true);
    }
}
