﻿using UnityEngine;
using UnityEngine.UI;

public class LootCardHandler : MonoBehaviour 
{
    public NewItemObtained newItemObtained;

    public PlayOpenCard[] playOpencard;
	public GameObject[] cardBacks;
    public GameObject[] tapGlows;

    public Transform gameObjectHolder;

    [Space (15)]
    public CardRewardDisplayHandler openAll;

    public static bool openAllTriggered;




    /*
    void ShuffleCardsPosition()
    {
        for (int x = cardBacks.Length-1; x > 0 ; x--)
        {
            int toSet = Random.Range(0, cardBacks.Length);
            if (toSet == x)
            {
                if (toSet == cardBacks.Length)
                    toSet -= 1;
                else if (toSet == 0)
                    toSet += 1;
            }
            cardBacks[x].transform.SetSiblingIndex(toSet);
        }
    }
    void ReassignCardBacksAndGlows()
    {
        for (int x = 0; x < gameObjectHolder.childCount - 1; x++)
        {
            cardBacks[x] = gameObjectHolder.GetChild(x).gameObject;
            tapGlows[x] = cardBacks[x].transform.GetChild(4).gameObject;
        }
    }
    */

    void Start()
    {
        for (int x = 0; x < cardBacks.Length; x++)
        {
            cardBacks[x].GetComponentInChildren<Button>().onClick.AddListener(CheckIfAllOpened);
        }


        newItemObtained.ListenToResumeOpening(this);
    }


    public void OpenAllUnopened()
    {
        InvokeRepeating("OpenItem", 0.2f, 0.35f);
    }

    void OpenItem()
    { 
        for (int x = 0; x < playOpencard.Length; x++)
        {
            if (playOpencard[x].openState == false)
            { 
                cardBacks[x].GetComponentInChildren<Button>().onClick.Invoke();

                if (playOpencard[x].isNew)
                    StopOpening();
                break;
            }
            if (x == cardBacks.Length - 1)
                CancelInvoke("OpenItem");
        }
    } 
    void CheckIfAllOpened()
    {
        for (int x = 0; x < playOpencard.Length; x++)
        {
            if (playOpencard[x].openState == false)
                return;

            if (x == playOpencard.Length - 1)
            {
                openAll.RevealComplete();
            }
        }
    }

    public void StopOpening()
    {
        CancelInvoke("OpenItem");
    }

    public void ResumeOpening()
    {
        OpenAllUnopened();
    }

	void OnDisable()
    {
        openAllTriggered = false;
        ResetOpenedCards();
        for (int x = 0; x < cardBacks.Length; x++)
		{
			cardBacks[x].SetActive(true);
            cardBacks[x].transform.GetChild(0).gameObject.SetActive(true);
        }
    }



    void ResetOpenedCards()
    {
        for (int x = 0; x < playOpencard.Length; x++)
        {
            playOpencard[x].SetOpenState(false);
            playOpencard[x].ResetToBase();
        }
    }
}
