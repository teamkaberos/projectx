﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ComponentDescrip : MonoBehaviour 
{
    [Tooltip("this image is the one blocking players from tapping on the building")]
	public Image building_Blocker;

    public int thisStructureColliderNumber;

	[TextArea(3,10)]
	public string[] stuffToTeach;
	protected int stringPlayerIsAt = 0;			// this number is for jumping to next text

	public Text displayText;
    public GameObject next_Text_Button;

    [Tooltip("when the pointers are supposed to appear")]
    public int[] pointerAppears;

    [Tooltip("to hold lines directing what the tutorial is introducing")]
    public GameObject[] pointerDisplays;
   
    [Tooltip("at which string should the building blocker be disabled to allow tap")]
    public int[] enableStructureCollider;

    [Tooltip("at which string should the 'Next' button be hidden")]
    public int[] textBreak;

	public virtual void Start()
	{
		Display_Tutorial_Text();
        ColliderChecker();
	}

	void Set_To_Next_Text()
	{
		stringPlayerIsAt += 1;
	}
	void Display_Tutorial_Text()
	{
        if (stringPlayerIsAt <= stuffToTeach.Length - 1)
            displayText.text = stuffToTeach[stringPlayerIsAt].Replace("<insert_player_name_here>", Player.playerName);
	}

	public virtual void Check_If_Any_Text_Remaining()
	{
        for (int x = 0; x < textBreak.Length; x++)
        {
            if (stringPlayerIsAt == textBreak[x])
            {
                next_Text_Button.SetActive(false);
                Toggle_Raycast(false);
                break;
            }
            Toggle_Raycast(true);
            next_Text_Button.SetActive(true);
        }
	}
    public virtual void Check_If_Any_Pointers()
    {
        //hide all first
        for (int x = 0; x < pointerDisplays.Length; x++)
        {
            pointerDisplays[x].SetActive(false);
        }

        for (int x = 0; x < pointerDisplays.Length; x++)
        {
            if (stringPlayerIsAt == pointerAppears[x])
            {
                pointerDisplays[x].SetActive(true);
                break;
            }
        }
    }

	public virtual void DisplayNext()
	{
		Set_To_Next_Text();
		Display_Tutorial_Text();
		Check_If_Any_Text_Remaining();
        Check_If_Any_Pointers();
		Check_If_Tutorial_State_Completed();
        ColliderChecker();
	}

    public virtual void ColliderChecker()
    {
        if (enableStructureCollider.Length >= 1)
        {
            for (int x = 0; x < enableStructureCollider.Length; x++)
            {
                if (stringPlayerIsAt == enableStructureCollider[x])
                {
                    Tutorial.Instance.ToggleColliders(thisStructureColliderNumber, true);
                    break;
                }
                Tutorial.Instance.ToggleColliders(thisStructureColliderNumber, false);
            }
        }
    }

	protected virtual void Toggle_Raycast(bool isOn)
	{
        if (building_Blocker != null)
            building_Blocker.raycastTarget = isOn;      
	}

	public virtual void Check_If_Tutorial_State_Completed()
	{
		if (stringPlayerIsAt >= stuffToTeach.Length)
		{
            //Close all tutorial panels
            if (!SceneManager.GetActiveScene().name.Contains("Battle"))
            {
                Tutorial.Instance.focusGroups[Player.tutorial].SetActive(false);
                Tutorial.Instance.tutorialBlocker.SetActive(true);
            }

            Player.Instance.UpdateTutorialValue();

            Tutorial.Instance.Get_Updated_Tutorial_Value();
		}
	}
}
