﻿using UnityEngine;
using UnityEngine.UI;

public class HeroUpgrade_Descrip : ComponentDescrip 
{
	[Tooltip("which of the blockers should appear")]
	public GameObject[] focusBlocker;
	[Tooltip("the string number that the blockers should be appearing")]
	public int[] whereFocusBlockersAppear;
    public GameObject disabledTavern;
    public Button closeTavernTutorial;
    public Text closeLodge;

    public Button enterLodge;

	void OnEnable()
	{
		if (Player.tutorial == 6)
		{
			HomeBuildings.TutorialBuildingTapped_ += Toggle_Raycast;
			HomeBuildings.TutorialBuildingTapped += DisplayNext;
		}
    }

	public override void DisplayNext()
	{
		base.DisplayNext();

        DisplayGreyedTavern();
		ToggleEnterTavern();      
		CheckFocusBlockerToggle();
	}

    void DisplayGreyedTavern()
    {
        if (stringPlayerIsAt == 2)
            disabledTavern.SetActive(true);

        if (stringPlayerIsAt == 6)
        {
            SetTextColor();
            closeTavernTutorial.interactable = true;
        }
    }
    void ToggleEnterTavern()
	{
		if (stringPlayerIsAt == 7)
			enterLodge.interactable = false;
		else
			enterLodge.interactable = true;
	}

    public void Tutorial_Display_Next()
	{
		if (Player.tutorial == 6)
			DisplayNext();
	}

    void CheckFocusBlockerToggle()
	{
		for (int x = 0; x < whereFocusBlockersAppear.Length; x++)
		{
			if (whereFocusBlockersAppear[x] == stringPlayerIsAt)
			{
				focusBlocker[x].SetActive(true);
				break;
			}
			focusBlocker[x].SetActive(false);
		}
	}

    void SetTextColor()
    {
        closeLodge.color = Color.red;
    }

	void Unsub()
	{
		HomeBuildings.TutorialBuildingTapped_ -= Toggle_Raycast;
        HomeBuildings.TutorialBuildingTapped -= DisplayNext;
	}

	void OnDisable()
	{
		Unsub();
	}
}
