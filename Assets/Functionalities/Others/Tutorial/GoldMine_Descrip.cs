﻿using UnityEngine.UI;

public class GoldMine_Descrip : ComponentDescrip
{
    public ResourceUpdate goldmineUpgradeButton;
    public Button completeBuildingDiamond;

	void OnEnable()
	{
        ResourceGenerate.TutorialBuildingTapped += DisplayNext;
        ResourceGenerate.TutorialBuildingTapped_ += Toggle_Raycast;

        BuildTimer.buildingUnqueued += DisplayNext;

        ResourceUpdate.tutorialUpgrading += DisplayNext;

        ToggleCompleteDiamond(false);
	}

    void ToggleCompleteDiamond(bool toSet)
    {
        completeBuildingDiamond.interactable = toSet;
    }

	public override void Check_If_Any_Text_Remaining()
	{
        if (stringPlayerIsAt == 6)
            goldmineUpgradeButton.Toggle_Upgrade_Button();
        else if (stringPlayerIsAt == 8)
        {
            goldmineUpgradeButton.Toggle_Building_Complete_Button();
            ToggleCompleteDiamond(true);
        }

		base.Check_If_Any_Text_Remaining();
	}

    void Unsub_Event()
    {
        ResourceGenerate.TutorialBuildingTapped -= DisplayNext;
        ResourceGenerate.TutorialBuildingTapped_ -= Toggle_Raycast;

        BuildTimer.buildingUnqueued -= DisplayNext;

        ResourceUpdate.tutorialUpgrading -= DisplayNext;
    }

	void OnDisable()
	{
        Unsub_Event();
	}

	void OnDestroy()
	{
        Unsub_Event();
	}
}
