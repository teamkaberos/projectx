﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpeedUp_Descrip : ComponentDescrip 
{
    public Button diamond_Speed_Up_Button;

    public GameObject tap_To_Close_Blocker;
    public GameObject diamond_Speed_Up_Focus;
    public Button tutorial_Speed_Up_Blocker;

    public GameObject treasury_Tutorial_Popup;

	void OnEnable()
	{
		BuildTimer.tutorialSpeedingUp += DisplayNext;
		//BuildTimer.buildingCompleted += DisplayNext;
		BuildTimer.buildingUnqueued += DisplayNext;
    }

    public override void Check_If_Any_Text_Remaining()
    {
        if (this.stringPlayerIsAt == 3)
        {
            diamond_Speed_Up_Button.interactable = true;
            tap_To_Close_Blocker.SetActive(false);
            diamond_Speed_Up_Focus.SetActive(true);
        }
        if (this.stringPlayerIsAt ==4)
        {
            tutorial_Speed_Up_Blocker.interactable = false;
            diamond_Speed_Up_Focus.SetActive(false);
        }

        base.Check_If_Any_Text_Remaining();
    }

	public override void Check_If_Tutorial_State_Completed()
    {
        if (stringPlayerIsAt >= stuffToTeach.Length)
        {
            this.transform.parent.gameObject.SetActive(false);
            treasury_Tutorial_Popup.SetActive(true);
        }
    }

    private void OnDisable()
    {
        UnsubEvent();
    }

	private void OnDestroy()
    {
        UnsubEvent();
	}

    public void UnsubEvent()
    {
        BuildTimer.tutorialSpeedingUp -= DisplayNext;
        //BuildTimer.buildingCompleted -= DisplayNext;
        BuildTimer.buildingUnqueued -= DisplayNext;  
    }
}
