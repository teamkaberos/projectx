﻿using UnityEngine;
using UnityEngine.UI;

public class TroopAssign_Descrip : ComponentDescrip
{
    public delegate void OnTroopsAssigned();
    public static event OnTroopsAssigned troopsAssigned;

    public GameObject prepHall_Tutorial_Popup;

    public int[] tutorial_Focus;
    public GameObject[] tutorial_Focus_Blocker;

    public Image tap_To_Close_Blocker;

    public Button assignTroopButton;

	void OnEnable()
	{
        if (Player.tutorial == 3)
            DragAndDrop.cardSelected += DisplayNext_;
	}

	public override void Check_If_Any_Text_Remaining()
    {
        Trigger_Focus();
        base.Check_If_Any_Text_Remaining();
	}

    public void Button_Disaply_Next()
    {
        if (Player.tutorial == 3)
        {
            base.DisplayNext();
        }
    }

    void DisplayNext_(bool display)
    {
        if (stringPlayerIsAt >= 5)
        {
            tap_To_Close_Blocker.raycastTarget = false;
            Unsub();
        }
        base.DisplayNext();
    }

	public override void Check_If_Tutorial_State_Completed()
	{
        if (stringPlayerIsAt >= stuffToTeach.Length)
        {
            transform.parent.gameObject.SetActive(false);
            prepHall_Tutorial_Popup.SetActive(true);
            assignTroopButton.interactable = false;
            tap_To_Close_Blocker.raycastTarget = false;
            troopsAssigned();
        }
    }

	void Trigger_Focus()
    {
        for (int x = 0; x < tutorial_Focus.Length; x++)
        {
            if (stringPlayerIsAt == tutorial_Focus[x])
                tutorial_Focus_Blocker[x].SetActive(true);
            else
            {
                if (tutorial_Focus_Blocker[x].activeInHierarchy)
                {
                    tutorial_Focus_Blocker[x].SetActive(false);
                }
            }
        }

        for (int x = 0; x < tutorial_Focus.Length; x++)
        {
            if (stringPlayerIsAt == tutorial_Focus[x])
            {
                Toggle_Raycast(true);
                break;
            }
            Toggle_Raycast(false);
        }
    }

    void Unsub()
    {
        DragAndDrop.cardSelected -= DisplayNext_;    
    }

	void OnDestroy()
	{
        Unsub();
	}
    void OnDisable()
    {
        Unsub();
    }
}
