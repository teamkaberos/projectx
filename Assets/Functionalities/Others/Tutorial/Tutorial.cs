﻿using UnityEngine;
using UnityEngine.UI;

public class Tutorial : MonoBehaviour
{
    #region singleton
    static Tutorial _instance;
    public static Tutorial Instance
    {
        get
        {
            return _instance;
        }
    }
    #endregion

    public CameraMovement camMovement;

    public GameObject tutorialPopup;
    public GameObject tutorialBlocker;
    public GameObject[] focusBlocker;
    public GameObject[] focusGroups;

    public GameObject[] toDisable;

    public Collider[] structureColliders;

    public Text dialogueText;
    [TextArea(3, 10)]
    public string[] tutorials;

    int tutorialReached;

    void Awake()
    {
        _instance = this;
    }

    void Start()
    {
        camMovement = GameObject.FindGameObjectWithTag("CamControl").GetComponent<CameraMovement>();
    }

    public void Get_Updated_Tutorial_Value()
    {
        if (Player.tutorial == 5)
            Player.tutorial = 6;

        tutorialReached = Player.tutorial;

        if (Player.tutorial < Player.tutorialCap)
        {
            ToggleUIElements(false);
            DisableAllStructureColliders();
        }

        Check_Which_Tutorial_To_Show();

        GameSparksManager.Instance.SavePlayerDetails();
    }

    void Check_Which_Tutorial_To_Show()
    {
        if (tutorialReached == 0)
        {
            dialogueText.text = tutorials[0].Replace("<insert_player_name_here>", Player.playerName);
            tutorialBlocker.SetActive(true);
            tutorialPopup.SetActive(true);
            ForceSave();
        }
        else if (tutorialReached == 1)
        {
            dialogueText.text = tutorials[1];
            tutorialBlocker.SetActive(true);
            tutorialPopup.SetActive(true);
            ForceSave();
        }
        else if (tutorialReached == 2)
        {
            dialogueText.text = tutorials[2];
            tutorialBlocker.SetActive(true);
            tutorialPopup.SetActive(true);
            ForceSave();
        }
        else if (tutorialReached == 3)
        {
            dialogueText.text = tutorials[3];
            tutorialBlocker.SetActive(true);
            tutorialPopup.SetActive(true);
            ForceSave();
        }
        else if (tutorialReached == 4)
        {
            dialogueText.text = tutorials[4];
            tutorialBlocker.SetActive(true);
            tutorialPopup.SetActive(true);
            ForceSave();
        }
        else if (tutorialReached == 6)
        {
            dialogueText.text = tutorials[5];
            tutorialBlocker.SetActive(true);
            tutorialPopup.SetActive(true);
            ForceSave();
        }
        else if (tutorialReached == 7)
        {
            dialogueText.text = tutorials[6];
            tutorialBlocker.SetActive(true);
            tutorialPopup.SetActive(true);
            ForceSave();
        }
        else if (tutorialReached == 8)
        {
            dialogueText.text = tutorials[7];
            tutorialPopup.SetActive(true);
            ForceSave();
        }
        else
        {
            ToggleUIElements(true);
            EnableAllStructureColliders();
            return;
        }
    }

    void ForceSave()
    {
        DataManager.Instance.PrepareDataForSparksPushing();
        GameSparksManager.Instance.StoreAllData();
    }

    public void What_Tutorial_To_Show_Next()
    {
        if (tutorialReached == 0)   // GOLD MINE
        {
            tutorialPopup.SetActive(false);
            camMovement.LerpTowards_Building("Mine1", true);
            Invoke("Check_Focus_Group", camMovement.lerpTime);
            return;
        }
        else if (tutorialReached == 1)      //Gold MINE COMPLETED , Start TREASURY
        {
            tutorialPopup.SetActive(false);
            camMovement.LerpTowards_Building("TempTreasury", true);
            Invoke("Check_Focus_Group", camMovement.lerpTime);
            return;
        }
        else if (tutorialReached == 2)      //Treasury Completed , start BARRACKS
        {
            tutorialPopup.SetActive(false);
            camMovement.LerpTowards_Building("TempGuild", true);
            Invoke("Check_Focus_Group", camMovement.lerpTime);
            return;
        }
        else if (tutorialReached == 3)      //Barracks completed , start RECRUITMENT HALL
        {
            tutorialPopup.SetActive(false);
            camMovement.LerpTowards_Building("TempPrep", true);
            Invoke("Check_Focus_Group", camMovement.lerpTime);
            return;
        }
        else if (tutorialReached == 4)      //recruitment Hall Completed , Start Assignment
        {
            EnableUIButton(0);
            tutorialPopup.SetActive(false);
            Check_Focus_Group();
            return;
        }
        else if (tutorialReached == 5)      // ASSIGNEMTN COMPLETED , START BATTLES
        {

        }
        else if (tutorialReached == 6)		//TAVERN for heroes
        {
            tutorialPopup.SetActive(false);
            camMovement.LerpTowards_Building("TempHeroLodge", true);
            Invoke("Check_Focus_Group", camMovement.lerpTime);
            return;
        }
        else if (tutorialReached == 7)      // CASTLE
        {
            tutorialPopup.SetActive(false);
            camMovement.LerpTowards_Building("TempTownHall", true);
            Invoke("Check_Focus_Group", camMovement.lerpTime);
            return;
        }
        else if (tutorialReached == 8)
        {
            //THIS AREA IS FOR THE GIFT
            tutorialPopup.SetActive(false);
            Check_Focus_Group();
            return;
        }
        else
            return;
    }

    void EnableUIButton(int toEnable)
    {
        toDisable[toEnable].SetActive(true);
    }
    void ToggleUIElements(bool toggle)
    {
        for (int x = 0; x < toDisable.Length; x++)
        {
            toDisable[x].SetActive(toggle);
        }
    }

    void DisableAllStructureColliders()
    {
        for (int x = 0; x < structureColliders.Length; x++)
        {
            ToggleColliders(x, false);
        }
    }
    void EnableAllStructureColliders()
    {
        for (int x = 0; x < structureColliders.Length; x++)
        {
            ToggleColliders(x, true);
        }
    }

    public void ToggleColliders(int structure, bool setActive)
    {
        structureColliders[structure].enabled = setActive;
    }

    void Check_Focus_Group()
    {
        tutorialBlocker.SetActive(false);
        //focusBlocker[Player.tutorial].SetActive(true);
        focusGroups[Player.tutorial].SetActive(true);
        Debug.Log("Setting active " + Player.tutorial + " now.");
    }
}
