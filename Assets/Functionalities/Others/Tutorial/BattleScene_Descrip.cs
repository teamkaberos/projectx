﻿using UnityEngine;
using UnityEngine.UI;

public class BattleScene_Descrip : ComponentDescrip
{
    public Transform deck1;
    public Transform deck2;
    public DragAndDrop_Troop_Cards mageCard;
    public Button mageCardButton;
    public GameObject[] spawnButtons;

    public GameObject toDisplayNext;

    public int[] when_To_Invoke_Display_Next;
    public float[] invokedTimers;

    public int[] timeStopper_Counter;

    Vector4 shadedBlocker = new Vector4(0, 0, 0, 0.5f);
    Vector4 unshadedBlocker = new Vector4(0, 0, 0, 0.01f);



    new void Start()
    {
        if (Player.tutorial >= Player.tutorialCap)
            return;
        
        spawnButtons[0].SetActive(false);
        spawnButtons[1].SetActive(false);

        mageCard = deck2.GetChild(1).GetComponent<DragAndDrop_Troop_Cards>();
        mageCardButton = deck2.GetChild(1).GetComponent<Button>();

        SpawnButton.TroopDropped += DisplayNext;
    }

    void OnEnable()
    {
        Invoke("StopTime", 0.2f);
    }
    void Check_If_Time_Should_Stop_Or_Go()
    {
        for (int x = 0; x < timeStopper_Counter.Length; x++)
        {
            if (stringPlayerIsAt == timeStopper_Counter[x])
            {
                StopTime();
                break;
            }
            ContinueTime();
        }
    }

    void Invoke_Next_Display_Text()
    {
        for (int x = 0; x < when_To_Invoke_Display_Next.Length; x++)
        {
            if (stringPlayerIsAt == when_To_Invoke_Display_Next[x])
            {
                Invoke("DisplayNext", invokedTimers[x]);
            }
        }
    }

    public override void Check_If_Any_Text_Remaining()
    {
        base.Check_If_Any_Text_Remaining();

        Check_If_Time_Should_Stop_Or_Go();
        Invoke_Next_Display_Text();
        Check_If_Need_To_Disable();
    }
    protected override void Toggle_Raycast(bool isOn)
    {
        building_Blocker.raycastTarget = isOn;
    }

    public override void Check_If_Tutorial_State_Completed()
    {
        if (stringPlayerIsAt == 15)
        {
            Player.tutorial += 1;

            DataManager.Instance.PrepareDataForSparksPushing();
            GameSparksManager.Instance.SavePlayerDetails();
            GameSparksManager.Instance.SavePlayerData(GameSparksManager.playerId);
        }
    }

    void Check_If_Need_To_Disable()
    {
        if (stringPlayerIsAt == 8)
        {
            Toggle_Raycast(false);
            mageCard.enabled = false;
            mageCardButton.interactable = false;
            HideNextButton(false);
        }
        else if (stringPlayerIsAt == 10)
        {
            Toggle_Raycast(false);
            mageCard.enabled = false;
            mageCardButton.interactable = false;
            HideNextButton(false);
        }
        else if (stringPlayerIsAt == 12)
        {
            Toggle_Raycast(false);
            mageCard.enabled = true;
            mageCardButton.interactable = true;
            HideNextButton(false);
        }
        else
        {
            Toggle_Raycast(true);
            HideNextButton(true);
        }
    }

    void HideNextButton(bool isOn)
    {
        toDisplayNext.SetActive(isOn);
    }

	void StopTime()
    {
        building_Blocker.color = shadedBlocker;
        Time.timeScale = 0;
    }
    void ContinueTime()
    {
        building_Blocker.color = unshadedBlocker;
        Time.timeScale = 1;
    }
}
