﻿using UnityEngine;

public class Barracks_Descrip : ComponentDescrip
{
    public int[] thisStructureCollider;
    public int[] whenToHidePointer;

    public GameObject[] pointerToDisable;

    void OnEnable()
    {
        if (Player.tutorial == 2)
        {
            BuildTimer.buildingUnqueued += DisplayNext;

            BuildTimer.structureStartUpgrading += DisplayNext;
        }
    }

    public override void ColliderChecker()
    {
        for (int x = 0; x < enableStructureCollider.Length; x++)
        {
            if (stringPlayerIsAt == enableStructureCollider[x])
            {
                Tutorial.Instance.ToggleColliders(thisStructureCollider[x], true);
                //break;
            }
            else
                Tutorial.Instance.ToggleColliders(thisStructureCollider[x], false);
        }
    }

    void DeselectLastBarrack()
    {
        for (int x = 0; x < BuildingManager.Instance.myHomeBuildings_.Count; x++)
        {
            if (BuildingManager.Instance.myHomeBuildings_[x].clicked == true)
            {
                BuildingManager.Instance.myHomeBuildings_[x].SelectThisBuilding();
                DisablePointer();
            }
        }
    }
    public override void DisplayNext()
    {
        NewDisplayNext();
    }
    public string BarrackToGoTo(int tutorialLine)
    {
        if (tutorialLine == 0)
            return "TempRange";
        return "TempBunker";
    }

    public void DisablePointer()
    {
            for (int x = 0; x < pointerToDisable.Length; x++)
                pointerDisplays[x].SetActive(false);
    }

    void NewDisplayNext()
    {
        base.DisplayNext();
    }

    void OnDisable()
	{
        Unsub();
	}

	void OnDestroy()
	{
        Unsub();
	}

    void Unsub()
    {
        if (Player.tutorial == 2)
        {
            BuildTimer.buildingUnqueued -= DisplayNext;
            BuildTimer.structureStartUpgrading -= DisplayNext;
        }
    }
}
