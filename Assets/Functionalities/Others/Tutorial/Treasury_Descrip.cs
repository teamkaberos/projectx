﻿public class Treasury_Descrip : ComponentDescrip 
{
    public Treasury_Update treasuryUpgradeButton;

	void OnEnable()
	{
            HomeBuildings.TutorialBuildingTapped += DisplayNext;
            HomeBuildings.TutorialBuildingTapped_ += Toggle_Raycast;

            BuildTimer.buildingCompleted += DisplayNext;

            Treasury_Update.tutorialUpgrading += DisplayNext;

        if (stringPlayerIsAt == 4)
            DisplayNext();
	}

    public override void Check_If_Any_Text_Remaining()
    {
        if (stringPlayerIsAt == 3)
        {
            treasuryUpgradeButton.Toggle_Upgrade_Button();
        }

        base.Check_If_Any_Text_Remaining();
    }

    void Unsub()
    {
        HomeBuildings.TutorialBuildingTapped -= DisplayNext;
        HomeBuildings.TutorialBuildingTapped_ -= Toggle_Raycast;

        BuildTimer.buildingCompleted -= DisplayNext;

        Treasury_Update.tutorialUpgrading -= DisplayNext;
    }

	void OnDisable()
	{
        Unsub();
	}
	void OnDestroy()
    {
        Unsub();
    }
}
