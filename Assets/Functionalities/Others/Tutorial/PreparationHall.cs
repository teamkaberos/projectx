﻿using UnityEngine;
using UnityEngine.UI;

public class PreparationHall : ComponentDescrip
{
    public GameObject troopAssign_Tutorial;
    public GameObject prevent_Tutorial_Close;

    public Button assignTroop;

	private void OnEnable()
	{
        if (Player.tutorial == 3)
        {
            HomeBuildings.TutorialBuildingTapped += DisplayNext;
            HomeBuildings.TutorialBuildingTapped_ += Toggle_Raycast;

            BuildTimer.buildingCompleted += DisplayNext;
            BuildTimer.structureStartUpgrading += DisplayNext;
            BuildTimer.buildingUnqueued += DisplayNext;

            TroopAssign_Descrip.troopsAssigned += DisplayNext;
        }
	}

    public void TriggerTutorials()
    {
        if (Player.tutorial == 3 & stringPlayerIsAt == 5)
        {
            troopAssign_Tutorial.SetActive(true);
            prevent_Tutorial_Close.SetActive(true);
            gameObject.SetActive(false);
        }
    }

    void Unsub()
    {
        HomeBuildings.TutorialBuildingTapped -= DisplayNext;
        HomeBuildings.TutorialBuildingTapped_ -= Toggle_Raycast;

        BuildTimer.buildingCompleted -= DisplayNext;
        BuildTimer.structureStartUpgrading -= DisplayNext;
        BuildTimer.buildingUnqueued -= DisplayNext;

        TroopAssign_Descrip.troopsAssigned -= DisplayNext;
    }
	private void OnDisable()
	{
        Unsub();
	}
	private void OnDestroy()
	{
        Unsub();
	}


}
