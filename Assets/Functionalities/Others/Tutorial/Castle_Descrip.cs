﻿using UnityEngine.UI;

public class Castle_Descrip : ComponentDescrip 
{
    public Button completeBuilding;
    public Button completeBuildingDiamond;

    void OnEnable()
    {
        if (Player.townHallLevel < 1)
        {
            HomeBuildings.TutorialBuildingTapped += DisplayNext;
            HomeBuildings.TutorialBuildingTapped_ += Toggle_Raycast;

            completeBuilding.interactable = false;

            BuildTimer.buildingUnqueued += DisplayNext;

            TownHall_Update.tutorialBuild += DisplayNext;
        }
        ToggleCompleteDiamond(false);
    }

    void ToggleCompleteDiamond(bool toSet)
    {
        completeBuildingDiamond.interactable = toSet;
    }

    public override void Check_If_Any_Pointers()
    {
        if (Player.townHallLevel < 1)
        {
            if (stringPlayerIsAt == 6)
            {
                completeBuilding.interactable = true;
                ToggleCompleteDiamond(true);
            }
        }
        base.Check_If_Any_Pointers();
    }


    void Unsub()
    {
        HomeBuildings.TutorialBuildingTapped -= DisplayNext;
        HomeBuildings.TutorialBuildingTapped_ -= Toggle_Raycast;

        BuildTimer.buildingUnqueued -= DisplayNext;

        TownHall_Update.tutorialBuild -= DisplayNext;
    }

    void OnDisable()
    {
        Unsub();
    }
    void OnDestroy()
    {
        Unsub();
    }
}
