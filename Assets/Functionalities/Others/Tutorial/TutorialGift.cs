﻿public class TutorialGift : ComponentDescrip
{
    public NewItemObtained newItemObtained;

	public override void DisplayNext()
	{
		base.DisplayNext();
	}

	public override void Check_If_Tutorial_State_Completed()
	{
		GetReward();
		base.Check_If_Tutorial_State_Completed();
	}

    void GetReward()
	{
		UnitManager.Instance.UnlockHero(13);
        UnitManager.Instance.GainHeroCard(13);

        newItemObtained.DisplayReward("Goblin", UnitManager.Instance.heroInfomation[13].newHeroImage);

		Player.gold += 10000;
		Player.Instance.LimitGold();

		UIManager.Instance.UpdateCurrencyInfo();

        DataManager.Instance.PrepareDataForSparksPushing();

        GameSparksManager.Instance.SaveHeroInfo(13);
        GameSparksManager.Instance.SavePlayerExpendables(GameSparksManager.playerId);
        GameSparksManager.Instance.SaveLastLogout();
	}
}
