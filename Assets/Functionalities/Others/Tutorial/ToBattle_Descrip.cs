﻿using UnityEngine;
using UnityEngine.UI;

public class ToBattle_Descrip : ComponentDescrip
{
    public delegate void tutorial_Enemy_Selected();
    public static event tutorial_Enemy_Selected selectedEnemy;

    public int[] focus_Display;
    public GameObject[] to_Battle_Tutorial_Focus;

    public Button cancelButton;

    void OnEnable()
    {
        LoadedEnemy.enemySelected += DisplayNext;
    }

    public override void Check_If_Any_Text_Remaining()
    {
        if (stringPlayerIsAt == 2)
        {
            selectedEnemy?.Invoke();
        }
        for (int x = 0; x < focus_Display.Length; x++)
        {
            if (stringPlayerIsAt == focus_Display[x])
                to_Battle_Tutorial_Focus[x].SetActive(true);
            else
                to_Battle_Tutorial_Focus[x].SetActive(false);
        }
        base.Check_If_Any_Text_Remaining();
    }

    public void Button_Display_Next()
    {
        if (Player.tutorial == 4)
        {
            cancelButton.interactable = false;
            base.DisplayNext();
        }
    }

    void OnDestroy()
    {
        LoadedEnemy.enemySelected -= DisplayNext;
    }
}
