﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
	#region Singleton

	private static AudioManager _instance;
	public static AudioManager Instance
	{
		get
		{ 
			if (_instance == null) 
			{
				GameObject go = new GameObject ("AudioManager");
				go.AddComponent<AudioManager> ();
			}
			return _instance;
		}
	}

	#endregion

	AudioSource aSource;

	void Awake()
	{
		_instance = this;
		aSource = GetComponent<AudioSource> ();
	}

	// ------------------------------------------ HOME SCENE ------------------------------------------ \\





	// ================================================================================================= \\





									  // INTENTIONALLY LEFT BLANK \\





	// ----------------------------------------- BATTLE SCENE ----------------------------------------- \\

	public AudioClip knightAttack;
	public AudioClip archerAttack;
	public AudioClip wizardAttack;

	public AudioClip dragonFire;
	public AudioClip landMines;
	public AudioClip seige;

	public AudioClip dragonGrowl;
	public AudioClip seigeBoomz;

	public AudioClip selectionSFX;
	public AudioClip deselectionSFX;

	public AudioClip victoryJingle;
	public AudioClip defeatJingle;

	void StopBGM()
	{
		aSource.Stop();
	}

	public void KnightAttack()
	{
		aSource.PlayOneShot (knightAttack);
	}
	public void ArcherAttack()
	{
		aSource.PlayOneShot (archerAttack);
	}
	public void WizardAttack()
	{
		aSource.PlayOneShot (wizardAttack);
	}

	public void SeigeAttack()
	{
		aSource.PlayOneShot(seige);
	}
	public void SeigeBoomz()
	{
		aSource.PlayOneShot(seigeBoomz);
	}
	public void LandMines()
	{
		aSource.PlayOneShot(landMines);
	}
	public void DragonFire()
	{
		aSource.PlayOneShot(dragonFire);
	}
	public void DragonGrowl()
	{
		aSource.PlayOneShot(dragonGrowl);
	}

	public void SelectionSFX()
	{
		aSource.PlayOneShot(selectionSFX);
	}

	public void VictoryJingle()
	{
		StopBGM();
		aSource.PlayOneShot(victoryJingle);
	}
	public void DefeatJingle()
	{
		StopBGM();
		aSource.PlayOneShot(defeatJingle);
	}
	// =================================================================================================== \\

}
