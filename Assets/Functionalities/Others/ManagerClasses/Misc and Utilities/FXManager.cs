﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FXManager : MonoBehaviour 
{
	#region singleton
	private static FXManager _instance;
	public static FXManager Instance
	{
		get
		{
			return _instance;
		}
	}
	#endregion

	public Transform enemyCastle;
    public Transform playerCastle;
	public Transform fxKeeper;		//Parent for all VFX in GAME scene

	public Transform[] troopVFX;	// 1 - Hasted ; 2 - Slowed ; 3 - Frozen ; etc etc
	public Transform [] castleVFX;	// 1 - Ballista attack

	public List<Transform> hasteKeeper;
	public List<Transform> slowKeeper;
	public List<Transform> frostKeeper;

	public GameObject wizardBang;
	public GameObject archerBang;
	public GameObject knightBang;
	public GameObject landMineFX;

    public GameObject succubusBang;

	public GameObject archerBangEnemy;
	public GameObject archerArrowParticles;

    public List<Transform> jiBaBoom;
    public List<Transform> jiBaBoomE;

	void Start()
	{
		_instance = this;

		CreateTroopVFX();
		CreateCastleVFX();
	}

	//Create HASTE VFX
	void CreateTroopVFX()
	{
		for (int x = 0;x<8;x++)
		{
			//Creating the FX
			Transform hasteFX = Instantiate (troopVFX[0], fxKeeper.position, Quaternion.identity);
			Transform earthFX = Instantiate (troopVFX[1], fxKeeper.position, Quaternion.identity);
			Transform iceFX = Instantiate (troopVFX[2], fxKeeper.position, Quaternion.identity);

			// ====== Just add the FX here if we ever have more ===== //

			//Parenting the FX
			hasteFX.SetParent(fxKeeper);
			earthFX.SetParent(fxKeeper);
			iceFX.SetParent(fxKeeper);

			//Adding the FX to List for easy access
			hasteKeeper.Add(hasteFX);
			slowKeeper.Add(earthFX);
			frostKeeper.Add(iceFX);

			//Hiding the FX
			hasteFX.gameObject.SetActive(false);
			earthFX.gameObject.SetActive(false);
			iceFX.gameObject.SetActive(false);
		}
	}
	void CreateCastleVFX()
	{
		for (int x = 0; x<4; x++)
		{
			//creating the FX
			Transform ballistaAttack = Instantiate (castleVFX[0], enemyCastle.position, Quaternion.identity);

			//Parenting the FX
			ballistaAttack.SetParent(enemyCastle);

			//Adding FX to List for easy access
			jiBaBoom.Add(ballistaAttack);

			//Hiding the FX
			ballistaAttack.gameObject.SetActive(false);
		}
        for (int x = 0; x < 4; x++)
        {
            //creating the FX
            Transform ballistaAttack = Instantiate(castleVFX[0], playerCastle.position, Quaternion.identity);

            //Parenting the FX
            ballistaAttack.SetParent(playerCastle);

            //Adding FX to List for easy access
            jiBaBoomE.Add(ballistaAttack);

            //Hiding the FX
            ballistaAttack.gameObject.SetActive(false);
        }
	}

	public void Haste(Transform hastedUnit)
	{
		for (int x = 0; x < hasteKeeper.Count; x++)
		{
			if(!hasteKeeper[x].gameObject.activeInHierarchy)
			{
				hasteKeeper[x].transform.position = hastedUnit.position;
				hasteKeeper[x].SetParent(hastedUnit);
				hasteKeeper[x].gameObject.SetActive(true);
				break;
			}
		}
	}
	public void Slooww(Transform sloowwedUnit)
	{
		for (int x = 0; x < slowKeeper.Count; x++)
		{
			if(!slowKeeper[x].gameObject.activeInHierarchy)
			{
				slowKeeper[x].transform.position = new Vector3 (sloowwedUnit.position.x,sloowwedUnit.position.y + 100, sloowwedUnit.position.z);
				slowKeeper[x].SetParent(sloowwedUnit);
				slowKeeper[x].gameObject.SetActive(true);
				break;
			}
		}
	}
	public void UnHaste(Transform windFX)
	{
		//Move the windFX away
		windFX.position = fxKeeper.position;
		windFX.SetParent(fxKeeper);
		windFX.gameObject.SetActive(false);
	}
	public void UnSlooww(Transform gravityFX)
	{
		gravityFX.position = fxKeeper.position;
		gravityFX.SetParent(fxKeeper);
		gravityFX.gameObject.SetActive(false);
	}
		
	public void BallistaBoomz()
	{
		for (int x = 0; x < jiBaBoom.Count; x++)
		{
			if (!jiBaBoom[x].gameObject.activeInHierarchy)
			{
				jiBaBoom[x].gameObject.SetActive(true);
				break;
			}
		}
	}
    public void BallistaBoomzE()
    {
        for (int x = 0; x < jiBaBoomE.Count; x++)
        {
            if (!jiBaBoomE[x].gameObject.activeInHierarchy)
            {
                jiBaBoomE[x].gameObject.SetActive(true);
                break;
            }
        }
    }
}
