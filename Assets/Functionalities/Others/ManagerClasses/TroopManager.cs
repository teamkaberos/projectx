﻿using System.Collections.Generic;
using UnityEngine;

public class TroopManager : MonoBehaviour
{
    #region Singleton
    static TroopManager _instance;
    public static TroopManager Instance
    {
        get
        {
            return _instance;
        }
    }
    #endregion

    public string myRace;
    string enemyRace;

    public Transform[] unitSet;         //accessing the stored race units in gameManager
    public Transform[] eUnitSet;        //accessing the stored enemy race units in gameManager

    public List<Transform> enemyMelee;  //Used for spawnning
    public List<Transform> enemyRange;  //Used for spawnning
    public List<Transform> enemyCaster; //Used for spawnning

    public List<Transform> playerHero;          //Used for spawnning
    public List<Transform> playerHeroIllu;

    public List<Transform> all_Players_Units;
    public List<Transform> all_Enemy_Units;

    public Transform unitKeeper;
    public Transform heroKeeper;
    public Transform illuKeeper;

    public static int pUnitIndex;
    public static string pHeroToBeSpawned;
    public static int pLocIndex;

    public static int eUnitIndex;
    public static int eLocIndex;

    Transform toBeSpawned;
    Transform heroToBeSpawned;
    Transform eToBeSpawned;

    void Awake()
    {
        _instance = this;

        RemoveUnassignedSlots();
    }

    void Start()
    {
        //Check first for players race and groups
        //CheckRace();
        //and instantiate units accordingly
        //		CreateUnits();  //< Previous unit creation>
        New_Create_Enemy_Units();
    }

    void CheckRace()
    {
        myRace = (string)Player.Instance.GetType().GetField("race").GetValue(Player.Instance);
        unitSet = (Transform[])GameManager.Instance.GetType().GetField(myRace + "Units").GetValue(GameManager.Instance);

        enemyRace = (string)Enemy.Instance.GetType().GetField("raceLoaded").GetValue(Enemy.Instance);
        eUnitSet = (Transform[])GameManager.Instance.GetType().GetField(enemyRace.ToLower() + "UnitsE").GetValue(GameManager.Instance);
    }

    public void RemoveUnassignedSlots()
    {
        for (int x = 0; x < Enemy.troopsAssigned.Count; x++)
        {
            if (Enemy.troopsAssigned[x] == 250)
            {
                Enemy.troopsAssigned.RemoveAt(x);
                Enemy.troopsAssignedLevels.RemoveAt(x);
            }
        }
    }

    public void New_Create_Enemy_Units()
    {
        //Do this procedure twice to create 4 of each unit
        for (int i = 0; i < 4; i++)
        {
            //declare an int to store the troop no
            int troopToCreate = 250;
            //loop through all assigned enemy troops

            for (int x = 0; x < Enemy.troopsAssigned.Count; x++)
            {
                for (int y = 0; y < GameManager.Instance.troopInfo.Count; y++)
                {
                    if (Enemy.troopsAssigned[x] < 100)
                    {
                        if ((Enemy.troopsAssigned[x]) == GameManager.Instance.troopInfo[y].troopNo)
                        {
                            troopToCreate = y;
                            break;
                        }
                    }
                    else
                    {
                        if ((Enemy.troopsAssigned[x] + 10) == GameManager.Instance.troopInfo[y].troopNo)
                        {
                            troopToCreate = y;
                            break;
                        }
                    }
                }

                Transform newEnemyTroopToCreate = Instantiate(GameManager.Instance.troopInfo[troopToCreate].troopUnit, unitKeeper.position, Quaternion.identity);
                newEnemyTroopToCreate.SetParent(unitKeeper);
                newEnemyTroopToCreate.gameObject.AddComponent<Rigidbody>().useGravity = false;

                if (Enemy.troopsAssigned[x] < 100)
                {
                    // If the created troop is a hero
                    newEnemyTroopToCreate.GetComponent<Hero>().Assign_Enemy_Hero_ID_And_Level(Enemy.troopsAssigned[x], Enemy.troopsAssignedLevels[x]);
                    if (troopToCreate == 12)
                    {
                        // this is for ninja
                        Transform newNinIllusion = Instantiate(GameManager.Instance.heroesAbility[0], unitKeeper.position, Quaternion.identity);
                        Transform newNinIllusion1 = Instantiate(GameManager.Instance.heroesAbility[0], unitKeeper.position, Quaternion.identity);
                        newNinIllusion.SetParent(unitKeeper);
                        newNinIllusion1.SetParent(unitKeeper);
                        newNinIllusion.gameObject.AddComponent<Rigidbody>().useGravity = false;
                        newNinIllusion1.gameObject.AddComponent<Rigidbody>().useGravity = false;
                        all_Enemy_Units.Add(newNinIllusion);
                        all_Enemy_Units.Add(newNinIllusion1);
                        newNinIllusion.gameObject.SetActive(false);
                        newNinIllusion1.gameObject.SetActive(false);
                    }
                }
                all_Enemy_Units.Add(newEnemyTroopToCreate);
                newEnemyTroopToCreate.gameObject.SetActive(false);

                if (newEnemyTroopToCreate.GetComponent<Unit>().unitIndex < 100)
                    newEnemyTroopToCreate.GetComponent<Unit>().shieldedCanvas.transform.localEulerAngles = new Vector3(0, 180, 0);
            }
        }
    }

    public void New_Create_Player_Units(int troopIDNo)
    {
        for (int x = 0; x < 4; x++)
        {
            int troopToCreate = 250;
            for (int y = 0; y < GameManager.Instance.troopInfo.Count; y++)
            {
                if (troopIDNo == GameManager.Instance.troopInfo[y].troopNo)
                {
                    troopToCreate = y;
                    break;
                }
            }
            Transform newTroop = Instantiate(GameManager.Instance.troopInfo[troopToCreate].troopUnit, unitKeeper.position, Quaternion.identity);
            newTroop.SetParent(unitKeeper);
            newTroop.gameObject.GetComponent<Unit>().isFriendlyUnit = true;
            newTroop.gameObject.AddComponent<Rigidbody>().useGravity = false;
            if (GameManager.Instance.troopInfo[troopToCreate].troopNo < 100) // which means this unit is a hero unit
            {
                newTroop.GetComponent<Hero>().MyHeroIdentityNumber(troopToCreate);

            }

            if (troopIDNo == 12)
            {
                Transform newNinIllusion = Instantiate(GameManager.Instance.heroesAbility[0], unitKeeper.position, Quaternion.identity);
                Transform newNinIllusion1 = Instantiate(GameManager.Instance.heroesAbility[0], unitKeeper.position, Quaternion.identity);
                newNinIllusion.SetParent(unitKeeper);
                newNinIllusion1.SetParent(unitKeeper);
                newNinIllusion.gameObject.GetComponent<Unit>().isFriendlyUnit = true;
                newNinIllusion1.gameObject.GetComponent<Unit>().isFriendlyUnit = true;
                newNinIllusion.gameObject.AddComponent<Rigidbody>().useGravity = false;
                newNinIllusion1.gameObject.AddComponent<Rigidbody>().useGravity = false;
                all_Players_Units.Add(newNinIllusion);
                all_Players_Units.Add(newNinIllusion1);
                newNinIllusion.gameObject.SetActive(false);
                newNinIllusion1.gameObject.SetActive(false);
            }
            all_Players_Units.Add(newTroop);
            newTroop.gameObject.SetActive(false);
        }
    }

    public void CheckSpawnTroopAndLoc(int i)
    {
        //Check the unit to be spawned
        pUnitIndex = int.Parse(GameManager.storedUnitIndex);
        pLocIndex = i;
    }
    public void CheckSpawnHeroAndLoc(int i)
    {
        pHeroToBeSpawned = GameManager.storedHeroID;
        pLocIndex = i;
    }


    public void New_SpawnHeroOnLane()
    {
        for (int i = 0; i < all_Players_Units.Count; i++)
        {
            if (all_Players_Units[i].name.Contains(pHeroToBeSpawned) && all_Players_Units[i].gameObject.activeInHierarchy != true)
            {
                heroToBeSpawned = all_Players_Units[i];
                break;
            }
        }
        //Get the locator
        heroToBeSpawned.SetParent(Player.Instance.unitLocs[pLocIndex]);
        //Put the unit there
        heroToBeSpawned.transform.position = Player.Instance.unitLocs[pLocIndex].position;

        //Activate unit
        heroToBeSpawned.gameObject.SetActive(true);

        // Set visibility of the PANTHER HERO
        if (heroToBeSpawned.name.Contains("Invis"))
        {
            heroToBeSpawned.GetComponent<Unit>().Panther_Invis(true);
        }

        //Assign units to their lane numbers
        heroToBeSpawned.GetComponent<Unit>().myLoc = pLocIndex;
        //Check for buffs and debuffs
        BuffDebuffChecker();
        //Activate Ability
        heroToBeSpawned.gameObject.GetComponent<Hero>().HeroSummon(pLocIndex);
        //Remove unit from variable
        heroToBeSpawned = null;
    }
    public void New_SpawnUnitsOnLane()
    {
        for (int x = 0; x < all_Players_Units.Count; x++)
        {
            if (pUnitIndex == all_Players_Units[x].GetComponent<Unit>().unitIndex && all_Players_Units[x].gameObject.activeInHierarchy != true)
            {
                toBeSpawned = all_Players_Units[x];
                break;
            }
        }
        //Get the locator that will be "spawned" on
        toBeSpawned.SetParent(Player.Instance.unitLocs[pLocIndex]);
        //Put unit to be spawned there
        toBeSpawned.transform.position = Player.Instance.unitLocs[pLocIndex].position;
        //Assign unit to their lane number
        toBeSpawned.GetComponent<Unit>().myLoc = pLocIndex;
        //Check for buffs and debuffs
        BuffDebuffChecker();
        //Activate unit
        toBeSpawned.gameObject.SetActive(true);
        //Remove unit from variable
        toBeSpawned = null;
    }


    public void New_Spawn_Enemy_Units_On_Lane(int unitID, int spawnLane)
    {
        unitID += 10;
        eLocIndex = 1;

        for (int x = 0; x < all_Enemy_Units.Count; x++)
        {
            if (unitID == all_Enemy_Units[x].GetComponent<Unit>().unitIndex && all_Enemy_Units[x].gameObject.activeInHierarchy != true)
            {
                eToBeSpawned = all_Enemy_Units[x];
                break;
            }
        }
        //Get the locator that will be "spawned" on
        eToBeSpawned.SetParent(Enemy.Instance.spawnLocs[spawnLane]);
        //Put unit to be spawned there
        eToBeSpawned.transform.position = Enemy.Instance.spawnLocs[spawnLane].position;
        //Activate unit
        eToBeSpawned.gameObject.SetActive(true);
        //Assign the lane number to the units
        eToBeSpawned.GetComponent<Unit>().myLoc = spawnLane;
        //Check if there are any buffs/debuffs on enemy lane
        BuffDebuffChecker();
        //Nullify enemy to be spawned;
        eToBeSpawned = null;
    }
    public void New_Spawn_Enemy_Heroes_On_Lane(int unitID, int spawnLane)
    {
        eLocIndex = spawnLane;
        for (int x = 0; x < all_Enemy_Units.Count; x++)
        {
            if (unitID == all_Enemy_Units[x].GetComponent<Unit>().unitIndex && all_Enemy_Units[x].gameObject.activeInHierarchy != true && all_Enemy_Units[x].GetComponent<Unit>().isFriendlyUnit == false)
            {
                eToBeSpawned = all_Enemy_Units[x];
                break;
            }
        }
        //Get the locator
        eToBeSpawned.SetParent(Enemy.Instance.spawnLocs[spawnLane]);
        //Put the unit there
        eToBeSpawned.transform.position = Enemy.Instance.spawnLocs[spawnLane].position;

        // Set visibility of the PANTHER HERO
        if (eToBeSpawned.name.Contains("Invis"))
            eToBeSpawned.GetComponent<Unit>().Panther_Invis(false);
        
        //Activate unit
        eToBeSpawned.gameObject.SetActive(true);
        //Assign units to their lane numbers
        eToBeSpawned.GetComponent<Unit>().myLoc = spawnLane;
        //Check for buffs and debuffs
        BuffDebuffChecker();
        //Activate Ability
        eToBeSpawned.gameObject.GetComponent<Hero>().HeroSummon(spawnLane);
        //Remove unit from variable
        eToBeSpawned = null;
    }


    public void BuffDebuffChecker()
    {
        if (GameManager.pHasted[pLocIndex] >= 1)
        {
            if (toBeSpawned != null)
            {
                toBeSpawned.GetComponent<Unit>().Hasted(pLocIndex);
            }
            if (heroToBeSpawned != null)
            {
                heroToBeSpawned.GetComponent<Unit>().Hasted(pLocIndex);
            }
        }
        if (GameManager.pSlowed[pLocIndex] >= 1)
        {
            if (toBeSpawned != null)
            {
                toBeSpawned.GetComponent<Unit>().Slowed(pLocIndex);
            }
            if (heroToBeSpawned != null)
            {
                heroToBeSpawned.GetComponent<Unit>().Slowed(pLocIndex);
            }
        }

        if (GameManager.eHasted[eLocIndex] >= 1)
        {
            if (eToBeSpawned != null)
            {
                eToBeSpawned.GetComponent<Unit>().Hasted(eLocIndex);
            }
        }
        if (GameManager.eSlowed[eLocIndex] >= 1)
        {
            if (eToBeSpawned != null)
            {
                eToBeSpawned.GetComponent<Unit>().Slowed(eLocIndex);
            }
        }

        if (GameManager.pFrozenLane[pLocIndex] == 1)
        {
            if (toBeSpawned != null)
            {
                toBeSpawned.GetComponent<Unit>().Hasted(pLocIndex);
            }
            if (heroToBeSpawned != null)
            {
                heroToBeSpawned.GetComponent<Unit>().Frozen(pLocIndex);
            }
        }
        if (GameManager.eFrozenLane[eLocIndex] == 1)
        {
            if (eToBeSpawned != null)
            {
                eToBeSpawned.GetComponent<Unit>().Frozen(eLocIndex);
            }
        }
    }


    public void Reset_Hero_Ability(Transform unit)
    {
        if (unit.name.Contains ("Hero"))
            ResetHeroAbilities(unit);
    }
    public void KillUnits(Transform unit, int currentLoc)
	{
		//When units die, go back to respective keeper
		unit.SetParent(unitKeeper);
		//Put units position back to keeper
		unit.position = unitKeeper.position;
		//Set unit to NotDead
		unit.GetComponent<Unit>().isDead = false;
        //Return collider center
        unit.GetComponent<Unit>().SetCollider();
        //Make units movable
        unit.GetComponent<Unit>().CheckMovementSpeed(currentLoc);
        // Remove units shield
        unit.GetComponent<Unit>().ClearShield();
		//Disable unit
		unit.gameObject.SetActive(false);
	}
	void ResetHeroAbilities(Transform unit)
	{
		if (unit.name.Contains("Tanker"))
            unit.GetComponent<Hero>().TankerDebuff();
		else if (unit.name.Contains("Haste1"))
			unit.GetComponent<Hero>().HasteDebuff();
		else if (unit.name.Contains("Haste3"))
			unit.GetComponent<Hero>().HastetresDebuff();
		else if (unit.name.Contains("Snail1"))
			unit.GetComponent<Hero>().SnailDebuff();
		else if (unit.name.Contains("Snail3"))
			unit.GetComponent<Hero>().SnailtresDebuff();
		else if (unit.name.Contains("Teleporter"))
            unit.GetComponent<Hero>().TeleporterDebuff();
		else if (unit.name.Contains("Ninja"))
			unit.GetComponent<Hero>().NinjaDebuff();
        else if (unit.name.Contains("LaneChange"))
            unit.GetComponent<Hero>().LaneChangeDebuff();
		unit.GetComponent<Hero>().DeactivateAbility();
        unit.GetComponent<Hero>().HeroDisengaged();
        if (unit.GetComponent<ActiveSkills>() != null)
            unit.GetComponent<ActiveSkills>().ResetCounter();
	}
}
