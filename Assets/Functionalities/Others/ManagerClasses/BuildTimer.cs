﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class BuildTimer : MonoBehaviour
{
    #region singleton
    static BuildTimer _instance;
    public static BuildTimer Instance
    {
        get
        {
            return _instance;
        }
    }
    #endregion

    // ====== DELEGATES ====== \\
    public delegate void OnBuildingUpgrade();
    public static event OnBuildingUpgrade structureStartUpgrading;

    public delegate void OnBuildingComplete();
    public static event OnBuildingComplete buildingCompleted;
    public static event OnBuildingComplete buildingUnqueued;

    public delegate void OnSpeedUpTapped();
    public static event OnSpeedUpTapped tutorialSpeedingUp;
    public static event OnSpeedUpTapped speedUpTapped;

    public delegate void OnSpeedUpComplete();
    public static event OnSpeedUpComplete speedUpFilled;

    public delegate void OnStoneComplete(string stoneType, int amount);
    public static event OnStoneComplete stoneCollect;


    // ====== VARIABLES ====== \\
    public static List<float> unitMyGroup = new List<float>();
    public static List<string> queuedMyGroup = new List<string>();

    public static List<float> unitMySupport = new List<float>();
    public static List<string> queuedMySupport = new List<string>();

    public static List<float> buildingTimer = new List<float>();
    public static List<string> queuedBuilding = new List<string>();
    float speedupTimer0;

    public static float earthstone0_Build_Time = 28800f;
    public static float earthstone1_Build_Time = 28800f;
    public static float speedupEarthStone0;
    public static float speedupEarthStone1;

    public static float windStone0_Build_Time = 28800f;
    public static float windStone1_Build_Time = 28800f;
    public static float speedupWindStone0;
    public static float speedupWindStone1;

    public static float flameStone0_Build_Time = 28800f;
    public static float flameStone1_Build_Time = 28800f;
    public static float speedupFlameStone0;
    public static float speedupFlameStone1;

    public static string queuedStone0;
    public static string queuedStone1;
    public static string queuedStone2;
    public static string queuedStone3;
    public static string queuedStone4;
    public static string queuedStone5;

    public static float currentBuildingProgress;
    public static string currentBuildingTimeLeft;

    public static GameObject lastBuildingPopup;
    public GameObject[] buildingGameObjects;

    public static bool buildingIsCompleted;
    static string toBeCanceled;                 //To be sent by the cancel button to cancel the correct structure/unit
    static string toBeSpedUp;                   //To be sent by the Haste button to speed up current structure/unit build
    static int speedUpCost;                     //Diamond cost to be used for speeding up structure/unit
    int temp3usage;
    int temp15usage;
    int temp30usage;
    int temp60usage;

    // ===--- Used for displaying remaining timers on UI ---=== //
    [Header("Units")]
    public Text myGroupRemainingTime;                   // The text to be displayed on UI
    public Image myGroupBuildProgression;           // The visual progress of group building
    public Text mySupportRemainingTime;             // The text to be displayed on UI
    public Image mySupportBuildProgression;     // The visual progress of group building
    [Header("Mines")]
    public Text myMinesRemainingTime;           // Text displaying remaining time on UI
    public Image myMinesBuildProgression;       // Visual progress of structure building - mines
    public Image myMinesSpeedUpProgression;
    public float mineBuildingTimer;                    // Actual timer needed to complete the structure
    [Header("Mills")]
    public Text myMillsRemainingTime;               // Text displaying remaining time on UI
    public Image myMillsBuildProgression;             // Visual progress of structure building - mills
    public float millBuildingTimer;                                             // Actual timer needed to complete the structure
    [Header("Bunker")]
    public Text myBunkerRemainingTime;
    public Image myBunkerBuildProgression;
    public Image myBunkerSpeedupProgression;
    public float bunkerBuildingTimer;
    [Header("Range")]
    public Text myRangesRemainingTime;
    public Image myRangesBuildProgression;
    public Image myRangeSpeedUpProgression;
    public float rangesBuildingTimer;
    [Header("Guild")]
    public Text myGuildRemainingTime;
    public Image myGuildBuildProgression;
    public Image myGuildSpeedupProgression;
    public float guildBuildingTimer;
    [Header("Recruitment Hall")]
    public Text rHallRemainingTime;
    public Image rHallBuildProgression;
    public Image rHallSpeedupProgression;
    public float rHallBuildingTimer;
    [Header("Town Hall")]
    public Text tHallRemainingTime;
    public Image tHallBuildProgression;
    public Image tHallSpeedupProgression;
    public float tHallBuildingTimer;
    [Header("Treasury")]
    public Text treasuryRemainingTime;
    public Image treasuryBuildProgression;
    public Image treasurySpeedupProgression;
    public float treasuryBuildingTimer;
    [Header("Forge")]
    public Text forgeRemainingTime;
    public Image forgeBuildProgression;
    public Image forgeSpeedUpProgression;
    public float forgeBuildingTimer;
    [Space(25)]
    [Header("Stones")]
    //public Text[] stone_RemainingTime;

    public Image[] stone_BuildProgression;
    public Image[] stone_SpeedupProgress;
    public float[] stone_BuildingTimer = new float[6];
    public static bool[] isBuildingStone = new bool[6];

    [Space(25)]
    [Header("Stone Remaining Timers")]
    public Text earth0stoneRemainingTime;
    public Text earth1stoneRemainingTime;
    public Text flame0stoneRemainingTime;
    public Text flame1stoneRemainingTime;
    public Text wind0stoneRemainingTime;
    public Text wind1stoneRemainingTime;

    public Text speedupThreeM;
    public Text speedupFifteenM;
    public Text speedupThirtyM;
    public Text speedupSixtyM;

    [Header("Speed up popup")]
    public GameObject confirmSpendDiamondPopup;
    public GameObject mainSpeedupPopup;     // the speed up popup that displays the timers
    public Text displaySpendDiamondAmount;
    public Button spendDiamondButton;   // the button that the player uses to spend diamond on speed up
    bool confirmDiamondUsage;   // used to determine if the player wants to spend diamond if speed up cost is over 1

    bool allowBuild;


    // FUNCTIONS
    void Start()
    {
        _instance = this;
        allowBuild = false;
        Set_SpeedUp_Values();
    }

    void Update()
    {
        StructureBuildProgress();
        StoneBuildProgress();
    }

    // === allow build === \\
    public void StartBuilding()
    {
        allowBuild = true; ;
    }


    // Resert all the stones speed up bar values
    void Set_SpeedUp_Values()
    {
        speedupEarthStone0 = Player.stoneBuildDuration;
        speedupEarthStone1 = Player.stoneBuildDuration;
        speedupWindStone0 = Player.stoneBuildDuration;
        speedupWindStone1 = Player.stoneBuildDuration;
        speedupFlameStone0 = Player.stoneBuildDuration;
        speedupFlameStone1 = Player.stoneBuildDuration;
    }
    public void CapStoneResearchDuration()
    {
        if (earthstone0_Build_Time > Player.stoneBuildDuration)
            earthstone0_Build_Time = Player.stoneBuildDuration;

        if (earthstone1_Build_Time > Player.stoneBuildDuration)
            earthstone1_Build_Time = Player.stoneBuildDuration;

        if (windStone0_Build_Time > Player.stoneBuildDuration)
            windStone0_Build_Time = Player.stoneBuildDuration;

        if (windStone1_Build_Time > Player.stoneBuildDuration)
            windStone1_Build_Time = Player.stoneBuildDuration;

        if (flameStone0_Build_Time > Player.stoneBuildDuration)
            flameStone0_Build_Time = Player.stoneBuildDuration;

        if (flameStone1_Build_Time > Player.stoneBuildDuration)
            flameStone1_Build_Time = Player.stoneBuildDuration;
    }

    // Calculating the progress for player Structures
    void StructureBuildProgress()
    {
        // ===--- IF PLAYER HAS A STRUCTURE BUILD ONGOING ---=== //
        if (buildingTimer.Count != 0)
        {
            Calculate_SpeedUp_Cost();
            // FLOAT STORED WILL BE REDUCED OVER TIME

            if (buildingTimer[0] > 0)
                buildingTimer[0] -= Time.deltaTime;
            else if (buildingTimer[0] < 0)
                buildingTimer[0] = 0;

            if (toBeSpedUp == "building")
                speedupTimer0 = buildingTimer[0] - speedUpAmount();
            else
                speedupTimer0 = buildingTimer[0];

            // ===-- Formatting the float for display --===//
            int hours = Mathf.FloorToInt(buildingTimer[0] / 3600);
            int minutes = Mathf.FloorToInt((buildingTimer[0] - (hours * 3600)) / 60f);
            if (minutes == 60)
            {
                hours += 1;
                minutes = 0;
            }
            int seconds = Mathf.CeilToInt(buildingTimer[0] - (hours * 3600) - (minutes * 60));
            if (seconds == 60)
            {
                minutes += 1;
                if (minutes == 60)
                {
                    hours += 1;
                    minutes = 0;
                }
                seconds = 0;
            }
            string niceTimer = string.Format("{0:00}:{1:00}:{2:00}", hours, minutes, seconds);
            currentBuildingTimeLeft = niceTimer;
            // ===--- IF IT IS A MINE ---===//

            if (queuedBuilding[0].Contains("Mine") && buildingIsCompleted != true)
            {
                if (Player.mine_Upgrading_Now == Player.mine_That_Player_Last_Tapped_On)
                {
                    // ===-- Updating Display on text and image --=== //
                    myMinesRemainingTime.text = niceTimer;
                    myMinesBuildProgression.fillAmount = (mineBuildingTimer - buildingTimer[0]) / mineBuildingTimer;
                    myMinesSpeedUpProgression.fillAmount = (mineBuildingTimer - speedupTimer0) / mineBuildingTimer;
                    currentBuildingProgress = (mineBuildingTimer - buildingTimer[0]) / mineBuildingTimer;
                    if (buildingTimer[0] <= 0)
                    {
                        buildingTimer[0] = 0;
                        myMinesRemainingTime.text = "Completed!";
                        myMinesBuildProgression.fillAmount = 1;
                        myMinesSpeedUpProgression.fillAmount = 0;

                        UIManager.Instance.Display_Building_Complete_Button("Mine");
                        buildingIsCompleted = true;
                        if (toBeSpedUp == "building")
                            Cancel_Speed_Up();
                        buildingCompleted();
                    }
                }
            }
            // ===--- IF IT IS A BUNKER ---===//
            else if (queuedBuilding[0].Contains("Bunker") && buildingIsCompleted != true)
            {
                // ===-- Updating Display on text and image --=== //
                myBunkerRemainingTime.text = niceTimer;
                myBunkerBuildProgression.fillAmount = (bunkerBuildingTimer - buildingTimer[0]) / bunkerBuildingTimer;
                myBunkerSpeedupProgression.fillAmount = (bunkerBuildingTimer - speedupTimer0) / bunkerBuildingTimer;
                currentBuildingProgress = (bunkerBuildingTimer - buildingTimer[0]) / bunkerBuildingTimer;
                if (buildingTimer[0] <= 0)
                {
                    buildingTimer[0] = 0;
                    myBunkerRemainingTime.text = "Completed!";
                    myBunkerBuildProgression.fillAmount = 1;
                    myBunkerSpeedupProgression.fillAmount = 0;

                    UIManager.Instance.Display_Building_Complete_Button("Bunker");
                    buildingIsCompleted = true;
                    if (toBeSpedUp == "building") Cancel_Speed_Up();
                    buildingCompleted();
                }
            }
            // ===--- IF IT IS A RANGED ---===//
            else if (queuedBuilding[0].Contains("Range") && buildingIsCompleted != true)
            {
                // ===-- Updating Display on text and image --=== //
                myRangesRemainingTime.text = niceTimer;
                myRangesBuildProgression.fillAmount = (rangesBuildingTimer - buildingTimer[0]) / rangesBuildingTimer;
                myRangeSpeedUpProgression.fillAmount = (rangesBuildingTimer - speedupTimer0) / rangesBuildingTimer;
                currentBuildingProgress = (rangesBuildingTimer - buildingTimer[0]) / rangesBuildingTimer;
                if (buildingTimer[0] <= 0)
                {
                    buildingTimer[0] = 0;
                    myRangesRemainingTime.text = "Completed!";
                    myRangesBuildProgression.fillAmount = 1;
                    myRangeSpeedUpProgression.fillAmount = 0;

                    UIManager.Instance.Display_Building_Complete_Button("Range");
                    buildingIsCompleted = true;
                    if (toBeSpedUp == "building") Cancel_Speed_Up();
                    buildingCompleted();
                }
            }
            // ===--- IF IT IS A GUILD ---=== //
            else if (queuedBuilding[0].Contains("Guild") && buildingIsCompleted != true)
            {
                // ===-- Updating Display on text and image --=== //
                myGuildRemainingTime.text = niceTimer;
                myGuildBuildProgression.fillAmount = (guildBuildingTimer - buildingTimer[0]) / guildBuildingTimer;
                myGuildSpeedupProgression.fillAmount = (guildBuildingTimer - speedupTimer0) / guildBuildingTimer;
                currentBuildingProgress = (guildBuildingTimer - buildingTimer[0]) / guildBuildingTimer;
                if (buildingTimer[0] <= 0)
                {
                    buildingTimer[0] = 0;
                    myGuildRemainingTime.text = "Completed!";
                    myGuildBuildProgression.fillAmount = 1;
                    myGuildSpeedupProgression.fillAmount = 0;

                    UIManager.Instance.Display_Building_Complete_Button("Guild");
                    buildingIsCompleted = true;
                    if (toBeSpedUp == "building") Cancel_Speed_Up();
                    buildingCompleted();
                }
            }
            // ===--- IF IT IS A RECRUITMENT HALL ---=== //
            else if (queuedBuilding[0].Contains("RecruitmentHall") && buildingIsCompleted != true)
            {
                // ===-- Updating Display on text and image --=== //
                rHallRemainingTime.text = niceTimer;
                rHallBuildProgression.fillAmount = (rHallBuildingTimer - buildingTimer[0]) / rHallBuildingTimer;
                rHallSpeedupProgression.fillAmount = (rHallBuildingTimer - speedupTimer0) / rHallBuildingTimer;
                currentBuildingProgress = (rHallBuildingTimer - buildingTimer[0]) / rHallBuildingTimer;
                if (buildingTimer[0] <= 0)
                {
                    buildingTimer[0] = 0;
                    rHallRemainingTime.text = "Completed!";
                    rHallBuildProgression.fillAmount = 1;
                    rHallSpeedupProgression.fillAmount = 0;

                    UIManager.Instance.Display_Building_Complete_Button("RecruitmentHall");
                    buildingIsCompleted = true;
                    if (toBeSpedUp == "building") Cancel_Speed_Up();
                    buildingCompleted();
                }
            }
            // ===--- IF IT IS A TOWN HALL ---=== //
            else if (queuedBuilding[0].Contains("TownHall") && buildingIsCompleted != true)
            {
                // ===-- Updating Display on text and image --=== //
                tHallRemainingTime.text = niceTimer;
                tHallBuildProgression.fillAmount = (tHallBuildingTimer - buildingTimer[0]) / tHallBuildingTimer;
                tHallSpeedupProgression.fillAmount = (tHallBuildingTimer - speedupTimer0) / tHallBuildingTimer;
                currentBuildingProgress = (tHallBuildingTimer - buildingTimer[0]) / tHallBuildingTimer;
                if (buildingTimer[0] <= 0)
                {
                    buildingTimer[0] = 0;
                    tHallRemainingTime.text = "Completed!";
                    tHallBuildProgression.fillAmount = 1;
                    tHallSpeedupProgression.fillAmount = 0;

                    UIManager.Instance.Display_Building_Complete_Button("TownHall");
                    buildingIsCompleted = true;
                    if (toBeSpedUp == "building") Cancel_Speed_Up();
                    buildingCompleted();
                }
            }
            // ===--- IF IT IS A TREASURY ---=== //
            else if (queuedBuilding[0].Contains("Treasury") && buildingIsCompleted != true)
            {
                if (Player.tutorial < Player.tutorialCap)
                {
                    if (buildingTimer[0] <= 0.8f && buildingTimer[0] > 0.2f)
                    {
                        buildingTimer[0] = 0.44f;
                        return;
                    }
                }
                // ===-- Updating Display on text and image --=== //
                treasuryRemainingTime.text = niceTimer;
                treasuryBuildProgression.fillAmount = (treasuryBuildingTimer - buildingTimer[0]) / treasuryBuildingTimer;
                treasurySpeedupProgression.fillAmount = (treasuryBuildingTimer - speedupTimer0) / treasuryBuildingTimer;
                currentBuildingProgress = (treasuryBuildingTimer - buildingTimer[0]) / treasuryBuildingTimer;
                if (buildingTimer[0] <= 0)
                {
                    buildingTimer[0] = 0;
                    treasuryRemainingTime.text = "Completed!";
                    treasuryBuildProgression.fillAmount = 1;
                    treasurySpeedupProgression.fillAmount = 0;

                    UIManager.Instance.Display_Building_Complete_Button("Treasury");
                    buildingIsCompleted = true;
                    if (toBeSpedUp == "building") Cancel_Speed_Up();
                    buildingCompleted();
                }
            }
            // ===--- IF IT IS A FORGE ---=== //
            else if (queuedBuilding[0].Contains("Forge") && buildingIsCompleted != true)
            {
                // ===-- Updating Display on text and image --=== //
                forgeRemainingTime.text = niceTimer;
                forgeBuildProgression.fillAmount = (forgeBuildingTimer - buildingTimer[0]) / forgeBuildingTimer;
                forgeSpeedUpProgression.fillAmount = (forgeBuildingTimer - speedupTimer0) / forgeBuildingTimer;
                currentBuildingProgress = (forgeBuildingTimer - buildingTimer[0]) / forgeBuildingTimer;
                if (buildingTimer[0] <= 0)
                {
                    buildingTimer[0] = 0;
                    forgeRemainingTime.text = "Completed!";
                    forgeBuildProgression.fillAmount = 1;
                    forgeSpeedUpProgression.fillAmount = 0;

                    UIManager.Instance.Display_Building_Complete_Button("Forge");
                    buildingIsCompleted = true;
                    if (toBeSpedUp == "building") Cancel_Speed_Up();
                    buildingCompleted();
                }
            }
        }
    }
    void StoneBuildProgress()
    {
        // EARTH STONES
        if (isBuildingStone[0] == true)
        {
            Calculate_SpeedUp_Cost();
            earthstone0_Build_Time -= Time.deltaTime;
            if (toBeSpedUp == "stone0")
                speedupEarthStone0 = earthstone0_Build_Time - speedUpAmount();
            else
                speedupEarthStone0 = earthstone0_Build_Time;

            // ===-- Formatting the float for display --===//
            int hours = Mathf.FloorToInt(earthstone0_Build_Time / 3600);
            int minutes = Mathf.FloorToInt((earthstone0_Build_Time - (hours * 3600)) / 60f);
            if (minutes == 60)
            {
                hours += 1;
                minutes = 0;
            }
            int seconds = Mathf.CeilToInt(earthstone0_Build_Time - (hours * 3600) - (minutes * 60));
            if (seconds == 60)
            {
                minutes += 1;
                if (minutes == 60)
                {
                    hours += 1;
                    minutes = 0;
                }
                seconds = 0;
            }
            string niceTimer = string.Format("{0:00}:{1:00}", hours, minutes);

            //stone_RemainingTime[0].text = string.Format("{0} ({1})", niceTimer, GetMyStone(queuedStone0));
            stone_BuildProgression[0].fillAmount = (Player.stoneBuildDuration - earthstone0_Build_Time) / Player.stoneBuildDuration;
            stone_SpeedupProgress[0].fillAmount = (Player.stoneBuildDuration - speedupEarthStone0) / Player.stoneBuildDuration;

            earth0stoneRemainingTime.text = niceTimer + " Left";

            if (earthstone0_Build_Time <= 0)
            {
                earthstone0_Build_Time = 0;
                stone_BuildProgression[0].fillAmount = 1;
                //stone_RemainingTime[0].text = "Ready for collection";

                stone_SpeedupProgress[0].fillAmount = 0;
                UIManager.Instance.stone_SpeedUp[0].SetActive(false);
                UIManager.Instance.popupManage[8].GetComponent<Barrack_Update>().ToggleStoneCompletNotification();

                BuildingManager.Instance.myHomeBuildings_[2].SetNotificationType(1);
                BuildingManager.Instance.myHomeBuildings_[2].ToggleNotification(true);

                //display building complete
                if (toBeSpedUp == "stone0")
                    Cancel_Speed_Up();
            }
        }
        if (isBuildingStone[3] == true)
        {
            Calculate_SpeedUp_Cost();
            earthstone1_Build_Time -= Time.deltaTime;
            if (toBeSpedUp == "stone3")
                speedupEarthStone1 = earthstone1_Build_Time - speedUpAmount();
            else
                speedupEarthStone1 = earthstone1_Build_Time;

            // ===-- Formatting the float for display --===//
            int hours = Mathf.FloorToInt(earthstone1_Build_Time / 3600);
            int minutes = Mathf.FloorToInt((earthstone1_Build_Time - (hours * 3600)) / 60f);
            if (minutes == 60)
            {
                hours += 1;
                minutes = 0;
            }
            int seconds = Mathf.CeilToInt(earthstone1_Build_Time - (hours * 3600) - (minutes * 60));
            if (seconds == 60)
            {
                minutes += 1;
                if (minutes == 60)
                {
                    hours += 1;
                    minutes = 0;
                }
                seconds = 0;
            }
            string niceTimer = string.Format("{0:00}:{1:00}", hours, minutes);

            earth1stoneRemainingTime.text = niceTimer + " Left";

            //stone_RemainingTime[0].text = string.Format("{0} ({1})", niceTimer, GetMyStone(queuedStone0));
            stone_BuildProgression[3].fillAmount = ((Player.stoneBuildDuration - earthstone1_Build_Time) / Player.stoneBuildDuration);
            stone_SpeedupProgress[3].fillAmount = (Player.stoneBuildDuration - speedupEarthStone1) / Player.stoneBuildDuration;
            if (earthstone1_Build_Time <= 0)
            {
                earthstone1_Build_Time = 0;
                stone_BuildProgression[3].fillAmount = 1;
                //stone_RemainingTime[0].text = "Ready for collection";
                stone_SpeedupProgress[3].fillAmount = 0;
                UIManager.Instance.stone_SpeedUp[3].SetActive(false);
                UIManager.Instance.popupManage[8].GetComponent<Barrack_Update>().ToggleStoneCompletNotification();

                BuildingManager.Instance.myHomeBuildings_[2].SetNotificationType(1);
                BuildingManager.Instance.myHomeBuildings_[2].ToggleNotification(true);

                //display building complete
                if (toBeSpedUp == "stone3") Cancel_Speed_Up();
            }
        }

        // WIND STONES
        if (isBuildingStone[1] == true)
        {
            Calculate_SpeedUp_Cost();
            windStone0_Build_Time -= Time.deltaTime;
            if (toBeSpedUp == "stone1")
                speedupWindStone0 = windStone0_Build_Time - speedUpAmount();
            else
                speedupWindStone0 = windStone0_Build_Time;

            // ===-- Formatting the float for display --===//
            int hours = Mathf.FloorToInt(windStone0_Build_Time / 3600);
            int minutes = Mathf.FloorToInt((windStone0_Build_Time - (hours * 3600)) / 60f);
            if (minutes == 60)
            {
                hours += 1;
                minutes = 0;
            }
            int seconds = Mathf.CeilToInt(windStone0_Build_Time - (hours * 3600) - (minutes * 60));
            if (seconds == 60)
            {
                minutes += 1;
                if (minutes == 60)
                {
                    hours += 1;
                    minutes = 0;
                }
                seconds = 0;
            }
            string niceTimer = string.Format("{0:00}:{1:00}", hours, minutes);

            wind0stoneRemainingTime.text = niceTimer + " Left";

            //stone_RemainingTime[1].text = string.Format("{0} ({1})", niceTimer, GetMyStone(queuedStone1));
            stone_BuildProgression[1].fillAmount = ((Player.stoneBuildDuration - windStone0_Build_Time) / Player.stoneBuildDuration);
            stone_SpeedupProgress[1].fillAmount = ((Player.stoneBuildDuration - speedupWindStone0) / Player.stoneBuildDuration);
            if (windStone0_Build_Time <= 0)
            {
                windStone0_Build_Time = 0;
                stone_BuildProgression[1].fillAmount = 1;
                //stone_RemainingTime[1].text = "Ready for collection";
                UIManager.Instance.stone_SpeedUp[1].SetActive(false);
                UIManager.Instance.popupManage[2].GetComponent<Barrack_Update>().ToggleStoneCompletNotification();

                BuildingManager.Instance.myHomeBuildings_[3].SetNotificationType(1);
                BuildingManager.Instance.myHomeBuildings_[3].ToggleNotification(true);

                stone_SpeedupProgress[1].fillAmount = 0;
                //display building complete
                if (toBeSpedUp == "stone1") Cancel_Speed_Up();
            }
        }
        if (isBuildingStone[4] == true)
        {
            Calculate_SpeedUp_Cost();
            windStone1_Build_Time -= Time.deltaTime;
            if (toBeSpedUp == "stone4")
                speedupWindStone1 = windStone1_Build_Time - speedUpAmount();
            else
                speedupWindStone1 = windStone1_Build_Time;

            // ===-- Formatting the float for display --===//
            int hours = Mathf.FloorToInt(windStone1_Build_Time / 3600);
            int minutes = Mathf.FloorToInt((windStone1_Build_Time - (hours * 3600)) / 60f);
            if (minutes == 60)
            {
                hours += 1;
                minutes = 0;
            }
            int seconds = Mathf.CeilToInt(windStone1_Build_Time - (hours * 3600) - (minutes * 60));
            if (seconds == 60)
            {
                minutes += 1;
                if (minutes == 60)
                {
                    hours += 1;
                    minutes = 0;
                }
                seconds = 0;
            }
            string niceTimer = string.Format("{0:00}:{1:00}", hours, minutes);

            wind1stoneRemainingTime.text = niceTimer + " Left";

            //stone_RemainingTime[1].text = string.Format("{0} ({1})", niceTimer, GetMyStone(queuedStone1));
            stone_BuildProgression[4].fillAmount = ((Player.stoneBuildDuration - windStone1_Build_Time) / Player.stoneBuildDuration);
            stone_SpeedupProgress[4].fillAmount = ((Player.stoneBuildDuration - speedupWindStone1) / Player.stoneBuildDuration);
            if (windStone1_Build_Time <= 0)
            {
                windStone1_Build_Time = 0;
                stone_BuildProgression[4].fillAmount = 1;
                //stone_RemainingTime[1].text = "Ready for collection";
                UIManager.Instance.stone_SpeedUp[4].SetActive(false);
                UIManager.Instance.popupManage[2].GetComponent<Barrack_Update>().ToggleStoneCompletNotification();

                BuildingManager.Instance.myHomeBuildings_[3].SetNotificationType(1);
                BuildingManager.Instance.myHomeBuildings_[3].ToggleNotification(true);

                stone_SpeedupProgress[4].fillAmount = 0;
                //display building complete
                if (toBeSpedUp == "stone4") Cancel_Speed_Up();
            }
        }

        // FLAME STONES
        if (isBuildingStone[2] == true)
        {
            Calculate_SpeedUp_Cost();
            flameStone0_Build_Time -= Time.deltaTime;
            if (toBeSpedUp == "stone2")
                speedupFlameStone0 = flameStone0_Build_Time - speedUpAmount();
            else
                speedupFlameStone0 = flameStone0_Build_Time;

            // ===-- Formatting the float for display --===//
            int hours = Mathf.FloorToInt(flameStone0_Build_Time / 3600);
            int minutes = Mathf.FloorToInt((flameStone0_Build_Time - (hours * 3600)) / 60f);
            if (minutes == 60)
            {
                hours += 1;
                minutes = 0;
            }
            int seconds = Mathf.CeilToInt(flameStone0_Build_Time - (hours * 3600) - (minutes * 60));
            if (seconds == 60)
            {
                minutes += 1;
                if (minutes == 60)
                {
                    hours += 1;
                    minutes = 0;
                }
                seconds = 0;
            }
            string niceTimer = string.Format("{0:00}:{1:00}", hours, minutes);

            flame0stoneRemainingTime.text = niceTimer + " Left";

            //stone_RemainingTime[2].text = string.Format("{0} ({1})", niceTimer, GetMyStone(queuedStone2));
            stone_BuildProgression[2].fillAmount = ((Player.stoneBuildDuration - flameStone0_Build_Time) / Player.stoneBuildDuration);
            stone_SpeedupProgress[2].fillAmount = ((Player.stoneBuildDuration - speedupFlameStone0) / Player.stoneBuildDuration);
            if (flameStone0_Build_Time <= 0)
            {
                flameStone0_Build_Time = 0;
                stone_BuildProgression[2].fillAmount = 1;
                //stone_RemainingTime[2].text = "Ready for collection";
                UIManager.Instance.stone_SpeedUp[2].SetActive(false);
                UIManager.Instance.popupManage[7].GetComponent<Barrack_Update>().ToggleStoneCompletNotification();

                BuildingManager.Instance.myHomeBuildings_[4].SetNotificationType(1);
                BuildingManager.Instance.myHomeBuildings_[4].ToggleNotification(true);

                stone_SpeedupProgress[2].fillAmount = 0;
                //display building complete
                if (toBeSpedUp == "stone2") Cancel_Speed_Up();
            }
        }
        if (isBuildingStone[5] == true)
        {
            Calculate_SpeedUp_Cost();
            flameStone1_Build_Time -= Time.deltaTime;
            if (toBeSpedUp == "stone5")
                speedupFlameStone1 = flameStone1_Build_Time - speedUpAmount();
            else
                speedupFlameStone1 = flameStone1_Build_Time;

            // ===-- Formatting the float for display --===//
            int hours = Mathf.FloorToInt(flameStone1_Build_Time / 3600);
            int minutes = Mathf.FloorToInt((flameStone1_Build_Time - (hours * 3600)) / 60f);
            if (minutes == 60)
            {
                hours += 1;
                minutes = 0;
            }
            int seconds = Mathf.CeilToInt(flameStone1_Build_Time - (hours * 3600) - (minutes * 60));
            if (seconds == 60)
            {
                minutes += 1;
                if (minutes == 60)
                {
                    hours += 1;
                    minutes = 0;
                }
                seconds = 0;
            }
            string niceTimer = string.Format("{0:00}:{1:00}", hours, minutes);

            flame1stoneRemainingTime.text = niceTimer + " Left";

            //stone_RemainingTime[2].text = string.Format("{0} ({1})", niceTimer, GetMyStone(queuedStone2));
            stone_BuildProgression[5].fillAmount = ((Player.stoneBuildDuration - flameStone1_Build_Time) / Player.stoneBuildDuration);
            stone_SpeedupProgress[5].fillAmount = ((Player.stoneBuildDuration - speedupFlameStone1) / Player.stoneBuildDuration);
            if (flameStone1_Build_Time <= 0)
            {
                flameStone1_Build_Time = 0;
                stone_BuildProgression[5].fillAmount = 1;
                //stone_RemainingTime[2].text = "Ready for collection";
                UIManager.Instance.stone_SpeedUp[5].SetActive(false);
                UIManager.Instance.popupManage[7].GetComponent<Barrack_Update>().ToggleStoneCompletNotification();

                BuildingManager.Instance.myHomeBuildings_[4].SetNotificationType(1);
                BuildingManager.Instance.myHomeBuildings_[4].ToggleNotification(true);

                stone_SpeedupProgress[5].fillAmount = 0;
                //display building complete
                if (toBeSpedUp == "stone5") Cancel_Speed_Up();
            }
        }
    }


    // ==  Utilities == \\

    // Add a stone build to the Players Stone Upgrade List
    public void StoneQueue(int stoneType)
    {
        //check first which slot to add to
        //int queuedNo = CheckWhichStoneSlotToBuildOn(stoneType);

        if (stoneType == 0)
        {
            earthstone0_Build_Time = Player.stoneBuildDuration - 1;
            speedupEarthStone0 = earthstone0_Build_Time;
            queuedStone0 = stoneType.ToString();
        }
        else if (stoneType == 1)
        {
            windStone0_Build_Time = Player.stoneBuildDuration - 1;
            speedupWindStone0 = windStone0_Build_Time;
            queuedStone1 = stoneType.ToString();
        }
        else if (stoneType == 2)
        {
            flameStone0_Build_Time = Player.stoneBuildDuration - 1;
            speedupFlameStone0 = flameStone0_Build_Time;
            queuedStone2 = stoneType.ToString();
        }
        else if (stoneType == 3)
        {
            earthstone1_Build_Time = Player.stoneBuildDuration - 1;
            speedupEarthStone1 = earthstone1_Build_Time;
            queuedStone3 = "0";
        }
        else if (stoneType == 4)
        {
            windStone1_Build_Time = Player.stoneBuildDuration - 1;
            speedupWindStone1 = windStone1_Build_Time;
            queuedStone4 = "1";
        }
        else if (stoneType == 5)
        {
            flameStone1_Build_Time = Player.stoneBuildDuration - 1;
            speedupFlameStone1 = flameStone1_Build_Time;
            queuedStone5 = "2";
        }
        IsBuildingStone(stoneType);

        DataManager.Instance.PrepareDataForSparksPushing();

        GameSparksManager.Instance.SaveStonesUnderUpgrade();

        GameSparksManager.Instance.SavePlayerExpendables(GameSparksManager.playerId);
    }
    public void IsBuildingStone(int stoneType)
    {
        isBuildingStone[stoneType] = true;
        UIManager.Instance.stone_Collect[stoneType].SetActive(true);
    }
    // Remove the stone that is being completed
    public void StoneUnQueue(int stoneSlot)
    {
        // ==== KNIGHT STONES ==== \\
        if (stoneSlot == 0)
        {
            Player.Instance.AddStone(queuedStone0);
            stone_BuildProgression[0].fillAmount = 0;
            //stone_RemainingTime[0].text = "Awaiting research";

            stoneCollect("EarthStone", 1);

            isBuildingStone[0] = false;
            queuedStone0 = null;

            earthstone0_Build_Time = 28800f;
            UIManager.Instance.popupManage[8].GetComponent<Barrack_Update>().GetUpdated();
        }
        if (stoneSlot == 3)
        {
            Player.Instance.AddStone(queuedStone3);
            stone_BuildProgression[3].fillAmount = 0;
            //stone_RemainingTime[0].text = "Awaiting research";

            stoneCollect("EarthStone", 1);

            isBuildingStone[3] = false;
            queuedStone3 = null;

            earthstone1_Build_Time = 28800f;
            UIManager.Instance.popupManage[8].GetComponent<Barrack_Update>().GetUpdated();
        }

        // ==== ARCHER STONES ==== \\
        if (stoneSlot == 1)
        {
            Player.Instance.AddStone(queuedStone1);
            stone_BuildProgression[1].fillAmount = 0;
            //stone_RemainingTime[1].text = "Awaiting research";

            stoneCollect("WindStone", 1);

            isBuildingStone[1] = false;
            queuedStone1 = null;

            windStone0_Build_Time = 28800f;
            UIManager.Instance.popupManage[2].GetComponent<Barrack_Update>().GetUpdated();
        }
        if (stoneSlot == 4)
        {
            Player.Instance.AddStone(queuedStone4);
            stone_BuildProgression[4].fillAmount = 0;
            //stone_RemainingTime[1].text = "Awaiting research";

            stoneCollect("WindStone", 1);

            isBuildingStone[4] = false;
            queuedStone4 = null;

            windStone1_Build_Time = 28800f;
            UIManager.Instance.popupManage[2].GetComponent<Barrack_Update>().GetUpdated();
        }

        // ==== MAGE STONES ==== \\
        if (stoneSlot == 2)
        {
            Player.Instance.AddStone(queuedStone2);
            stone_BuildProgression[2].fillAmount = 0;
            //stone_RemainingTime[2].text = "Awaiting research";

            stoneCollect("FlameStone", 1);

            isBuildingStone[2] = false;
            queuedStone2 = null;

            flameStone0_Build_Time = 28800f;
            UIManager.Instance.popupManage[7].GetComponent<Barrack_Update>().GetUpdated();
        }
        if (stoneSlot == 5)
        {
            Player.Instance.AddStone(queuedStone5);
            stone_BuildProgression[5].fillAmount = 0;
            //stone_RemainingTime[2].text = "Awaiting research";

            stoneCollect("FlameStone", 1);

            isBuildingStone[5] = false;
            queuedStone5 = null;

            flameStone1_Build_Time = 28800f;
            UIManager.Instance.popupManage[7].GetComponent<Barrack_Update>().GetUpdated();
        }

        //Toggle_BuildStones_Button();

        UIManager.Instance.UpdateTavernInfos();
        DataManager.Instance.PrepareDataForSparksPushing();
        GameSparksManager.Instance.SaveStonesUnderUpgrade();
        GameSparksManager.Instance.SavePlayerExpendables(GameSparksManager.playerId);

        if (Player.accountType == "Guest")
            Player.SavePrefs();
    }
    // Add a Structure to the Structure upgrade List
    public void BuildingQueue(string qType)
    {
        if (Player.premiumAccount != true)
        {
            if (buildingTimer.Count < 1)
            {
                // random number to avoid null reference exception
                float formula = 0;
                switch (qType)
                {
                    case "Bunker":
                        bunkerBuildingTimer = formula;
                        if (Player.tutorial < Player.tutorialCap)
                            structureStartUpgrading();
                        break;

                    case "Range":
                        rangesBuildingTimer = formula;
                        if (Player.tutorial < Player.tutorialCap)
                            structureStartUpgrading();
                        break;

                    case "Guild":
                        guildBuildingTimer = formula;
                        if (Player.tutorial < Player.tutorialCap)
                            structureStartUpgrading();
                        break;

                    case "RecruitmentHall":
                        rHallBuildingTimer = formula;
                        if (Player.tutorial < Player.tutorialCap)
                            structureStartUpgrading();
                        break;

                    case "TownHall":
                        tHallBuildingTimer = formula;
                        break;

                    case "Treasury":
                        treasuryBuildingTimer = formula;
                        break;

                    case "Forge":
                        forgeBuildingTimer = formula;
                        break;

                    case "Mine":
                        Player.mine_Upgrading_Now = Player.mine_That_Player_Last_Tapped_On;     //Storing the mine that the player is upgrading 
                        mineBuildingTimer = formula;
                        BuildingManager.Instance.StartUpgading_RSS_Building(qType);
                        break;
                        //              case "Mill":
                        //                  Player.mill_Upgrading_Now = Player.mill_That_Player_Last_Tapped_On;         //Storing the mill the player is upgrading
                        //                  formula = Player.mills_Upgrade_Duration[Player.mill_That_Player_Last_Tapped_On];
                        //                  millBuildingTimer = formula;
                        //                  Player.gold -= Player.millsCost[Player.mine_That_Player_Last_Tapped_On];
                        //                  BuildingManager.Instance.StartUpgading_RSS_Building(qType);
                        //                  break;
                }
                // Deduct upgrade cost
                Player.Instance.SpendGold(Player.Instance.upgradeCost(UpgradingBuildingNo(qType)));
                // Add building to the queue
                queuedBuilding.Add(qType);
                // Add build time to the building timer
                buildingTimer.Add(Player.Instance.UpgradingBuildingLevel(qType));
                speedupTimer0 = buildingTimer[0];
                //Show scaffold here
                BuildingManager.lastSelectedBuillding.GetComponent<HomeBuildings>().ShowScaffold();
                // ==== Update UI stuffs here ==== \\
                UIManager.Instance.Display_Upgrading_Now_Info();
                UIManager.Instance.UpdateCurrencyInfo();
                UIManager.Instance.Update_Building_LevelInfo();
                UIManager.Instance.UpdatePlayerTabInfo();

                // ==== UPDATE GS ==== \\
                if (Player.tutorial >= Player.tutorialCap)
                {
                    DataManager.Instance.PrepareDataForSparksPushing();
                    GameSparksManager.Instance.SaveBuildingUnderUpgrade();
                    GameSparksManager.Instance.SavePlayerExpendables(GameSparksManager.playerId);
                }
            }
        }
    }
    // Remove the Structure fromt the Structure upgrading List
    public void BuildingUnqueue(string buildingType)
    {
        speedupTimer0 = 0;
        int currentLevel = 0;
        switch (buildingType)
        {
            case "Bunker":
                Player.Instance.UpgradeBunker();
                currentLevel = Player.bunkerLv;
                if (Player.tutorial >= Player.tutorialCap)
                {
                    DataManager.Instance.PrepareDataForSparksPushing();
                    GameSparksManager.Instance.SaveBarracks(1);
                }
                break;
            case "Range":
                Player.Instance.UpgradeRange();
                currentLevel = Player.rangeLv;
                if (Player.tutorial >= Player.tutorialCap)
                {
                    DataManager.Instance.PrepareDataForSparksPushing();
                    GameSparksManager.Instance.SaveBarracks(2);
                }
                break;
            case "Guild":
                Player.Instance.UpgradeGuild();
                currentLevel = Player.guildLv;
                if (Player.tutorial >= Player.tutorialCap)
                {
                    DataManager.Instance.PrepareDataForSparksPushing();
                    GameSparksManager.Instance.SaveBarracks(0);
                }
                break;
            case "RecruitmentHall":
                Player.Instance.UpgradeRecruitmentHall();
                currentLevel = Player.recruitmentHall_Lvl;
                if (Player.tutorial >= Player.tutorialCap)
                {
                    DataManager.Instance.PrepareDataForSparksPushing();
                    GameSparksManager.Instance.SaveStructurePreparationHall();
                }
                break;
            case "TownHall":
                Player.Instance.UpgradeTownHall();
                currentLevel = Player.townHallLevel;
                if (Player.tutorial >= Player.tutorialCap)
                {
                    DataManager.Instance.PrepareDataForSparksPushing();
                    GameSparksManager.Instance.SaveStructureTownHall();
                }
                break;
            case "Treasury":
                Player.Instance.UpgradeTreasury();
                currentLevel = Player.treasuryLvl;
                if (Player.tutorial >= Player.tutorialCap)
                {
                    DataManager.Instance.PrepareDataForSparksPushing();
                    GameSparksManager.Instance.SaveStructureTreasury();
                }
                break;
            case "Forge":
                Player.Instance.UpgradeForge();
                currentLevel = Player.forgeLevel;
                BuildingManager.Instance.Update_Forge_Building();
                if (Player.tutorial >= Player.tutorialCap)
                {
                    DataManager.Instance.PrepareDataForSparksPushing();
                    GameSparksManager.Instance.SaveStructureForge();
                }
                break;
            case "Mine":
                //Add int to Mine upgrades
                Player.Instance.UpgradeMine();
                currentLevel = Player.mines[Player.mine_Upgrading_Now];
                // Allow RSS building generation
                BuildingManager.Instance.FinishUpgrading_RSS_Building(buildingType);
                // Check to see if home screen mesh is correctly updated
                BuildingManager.Instance.goldMine[Player.mine_Upgrading_Now].Update_RSS_Building_Model();
                BuildingManager.Instance.goldMine[Player.mine_Upgrading_Now].DisplayGoblins();
                if (Player.tutorial >= Player.tutorialCap)
                {
                    DataManager.Instance.PrepareDataForSparksPushing();
                    GameSparksManager.Instance.SaveAllStructureMines();
                }
                break;
        }
        //Hide scaffold here
        buildingUnqueued();
        BuildingManager.Instance.MeshUpdate();
        queuedBuilding.RemoveAt(0);
        buildingTimer.RemoveAt(0);
        //Enable all upgrade buttons
        UIManager.Instance.Enable_All_Upgrade_Buttons();
        UIManager.Instance.Update_Building_LevelInfo();
        //Disable the active one if leveling requirement not met
        UIManager.Instance.Disable_Further_Upgrade();
        UIManager.Instance.Enable_SpeedUp_Buttons();
        UIManager.Instance.NotifyStructureCompletion(buildingType, currentLevel);
        buildingIsCompleted = false;
        DataManager.Instance.PrepareDataForSparksPushing();
        GameSparksManager.Instance.SaveBuildingUnderUpgrade(0, 0);
        GameSparksManager.Instance.SaveInventoryTimers();
    }
    // For send the ID of the structure that is being upgraded currently
    public int UpgradingBuildingNo(string nameOfBuilding)
    {
        if (nameOfBuilding == null)
            return 0;
        if (nameOfBuilding.ToLower().Contains("townhall"))
            return 1;
        if (nameOfBuilding.ToLower().Contains("bunker"))
            return 2;
        if (nameOfBuilding.ToLower().Contains("range"))
            return 3;
        if (nameOfBuilding.ToLower().Contains("guild"))
            return 4;
        if (nameOfBuilding.ToLower().Contains("treasury"))
            return 5;
        if (nameOfBuilding.ToLower().Contains("mine"))
        {
            return (Player.mine_Upgrading_Now + 6);
        }
        if (nameOfBuilding.ToLower().Contains("forge"))
            return 11;
        if (nameOfBuilding.ToLower().Contains("recruitment"))
            return 12;
        return 100;
    }

    // === SPEED UP AREA === \\
    public void To_Be_Sped_Up(string typeToBeSped)
    {
        toBeSpedUp = typeToBeSped;                          // Register the OBJ that is going to be sped up
        UIManager.Instance.UpdateSpeedUpInfo();       // Display speed up timers the player has  
        Calculate_SpeedUp_Cost();                               // Display the required amount of diamond for the OBJ that is to be sped up
        UpdateTimerUsage();
    }
    public void Calculate_SpeedUp_Cost()
    {
        // Only applicable for diamond speed ups
        // This method will fetch the remaining time on the OBJ that is to be sped up
        // and calculate how much diamond will be need to complete it
        if (toBeSpedUp == "building")
        {
            int nearestMin = (int)Math.Round(buildingTimer[0] / 180f);      //round time to nearest minute
            speedUpCost = nearestMin * 3;                                                 //Caluculate how much diamond will be needed
        }
        else if (toBeSpedUp == "stone0")
        {
            int nearestMin = (int)Math.Round(earthstone0_Build_Time / 180f);
            speedUpCost = nearestMin * 3;
        }
        else if (toBeSpedUp == "stone1")
        {
            int nearestMin = (int)Math.Round(windStone0_Build_Time / 180f);
            speedUpCost = nearestMin * 3;
        }
        else if (toBeSpedUp == "stone2")
        {
            int nearestMin = (int)Math.Round(flameStone0_Build_Time / 180f);
            speedUpCost = nearestMin * 3;
        }
        else if (toBeSpedUp == "stone3")
        {
            int nearestMin = (int)Math.Round(earthstone1_Build_Time / 180f);
            speedUpCost = nearestMin * 3;
        }
        else if (toBeSpedUp == "stone4")
        {
            int nearestMin = (int)Math.Round(windStone1_Build_Time / 180f);
            speedUpCost = nearestMin * 3;
        }
        else if (toBeSpedUp == "stone5")
        {
            int nearestMin = (int)Math.Round(flameStone1_Build_Time / 180f);
            speedUpCost = nearestMin * 3;
        }
        UIManager.Instance.Display_Speed_Up_Cost(speedUpCost, toBeSpedUp);
    }
    float speedUpAmount()
    {
        // For calcuating the amount that will be sped up in the timer bar
        float speedUpValue = (temp3usage * 180) + (temp15usage * 900) + (temp30usage * 1800) + (temp60usage * 3600);
        return speedUpValue;
    }
    public void AutoCalculateSpeedUp()
    {
        RestoreSpeedUpTimers();
        ClearTempUsage();
        //Declare a float to store the time we need
        float OBJCompletionTimeLeft = 0;
        //Get the time left of the OBJ that is being upgraded
        switch (toBeSpedUp)
        {
            case "building":
                OBJCompletionTimeLeft = buildingTimer[0];
                break;
            case "stone0":
                OBJCompletionTimeLeft = earthstone0_Build_Time;
                break;
            case "stone1":
                OBJCompletionTimeLeft = windStone0_Build_Time;
                break;
            case "stone2":
                OBJCompletionTimeLeft = flameStone0_Build_Time;
                break;
            case "stone3":
                OBJCompletionTimeLeft = earthstone1_Build_Time;
                break;
            case "stone4":
                OBJCompletionTimeLeft = windStone1_Build_Time;
                break;
            case "stone5":
                OBJCompletionTimeLeft = flameStone1_Build_Time;
                break;
            default:
                OBJCompletionTimeLeft = 0;
                break;
        }
        // Do calculations for 60min timers
        for (int x = 0; x < Player.timer60; x++)
        {
            if (OBJCompletionTimeLeft > 3600f)   // Which means its more than 60mins
            {
                Speed_Up_Build(60);
                OBJCompletionTimeLeft -= 3600f;
            }
            else
                break;
        }
        for (int x = 0; x < Player.timer30; x++)
        {
            if (OBJCompletionTimeLeft > 1800f)   // Which means its more than 60mins
            {
                Speed_Up_Build(30);
                OBJCompletionTimeLeft -= 1800f;
            }
            else
                break;
        }
        for (int x = 0; x < Player.timer15; x++)
        {
            if (OBJCompletionTimeLeft > 900f)   // Which means its more than 60mins
            {
                Speed_Up_Build(15);
                OBJCompletionTimeLeft -= 900f;
            }
            else
                break;
        }
        for (int x = 0; x < Player.timer3; x++)
        {
            if (OBJCompletionTimeLeft > 180f)   // Which means its more than 60mins
            {
                Speed_Up_Build(3);
                OBJCompletionTimeLeft -= 180f;
            }
            else if (OBJCompletionTimeLeft <= 180f & OBJCompletionTimeLeft > 0f)
            {
                Speed_Up_Build(3);
                OBJCompletionTimeLeft = 0;
                break;
            }
        }
        speedUpTapped();
    }
    // Update and display the speed up timer bar based on which timer the Player Pressed
    public void ConfirmSpendDIamond()
    {
        confirmDiamondUsage = true;
        SpeedUpConfirmationPopup(false);
        spendDiamondButton.onClick.Invoke();
    }
    void DisplayDiamondRequired()
    {
        displaySpendDiamondAmount.text = string.Format("{0:n0} Diamonds required", speedUpCost);
    }
    void SpeedUpConfirmationPopup(bool toDisplay)
    {
        confirmSpendDiamondPopup.SetActive(toDisplay);
    }
    void SpeedUpPopUpToggle(bool toDisplay)
    {
        mainSpeedupPopup.SetActive(toDisplay);
    }

    void SpendDiamondWarning()
    {
        DisplayDiamondRequired();
        SpeedUpConfirmationPopup(true);
    }

    public void Speed_Up_Build(int speedUps)
    {
        // 1 = using diamonds
        // 15 = 15 mins speed up
        // 30 = 30 mins speed up
        //Called when Player hits on the "Yes" to speed up building process
        //Check if the player has enough diamonds
        if (speedUps == 1)
        {
            if (speedUpCost >= 1 & confirmDiamondUsage != true)
            {
                // popup warning and stop the code progress
                SpendDiamondWarning();
                return;
            }
            if (Player.diamond >= speedUpCost)
            {
                //Straight away removes all time
                Player.Instance.SpendDiamond(speedUpCost);
                confirmDiamondUsage = false;
                if (toBeSpedUp == "building")
                {
                    //Send the remaining time to be deducted
                    Cancel_Speed_Up();
                    buildingTimer[0] = 0.2f;
                }
                else if (toBeSpedUp == "stone0")
                {
                    Cancel_Speed_Up();
                    earthstone0_Build_Time = 0.2f;
                }
                else if (toBeSpedUp == "stone1")
                {
                    Cancel_Speed_Up();
                    windStone0_Build_Time = 0.2f;
                }
                else if (toBeSpedUp == "stone2")
                {
                    Cancel_Speed_Up();
                    flameStone0_Build_Time = 0.2f;
                }
                else if (toBeSpedUp == "stone3")
                {
                    Cancel_Speed_Up();
                    earthstone1_Build_Time = 0.2f;
                }
                else if (toBeSpedUp == "stone4")
                {
                    Cancel_Speed_Up();
                    windStone1_Build_Time = 0.2f;
                }
                else if (toBeSpedUp == "stone5")
                {
                    Cancel_Speed_Up();
                    flameStone1_Build_Time = 0.2f;
                }

                if (Player.tutorial >= Player.tutorialCap)
                {
                    DataManager.Instance.PrepareDataForSparksPushing();

                    GameSparksManager.Instance.SavePlayerExpendables(GameSparksManager.playerId);

                    GameSparksManager.Instance.SaveBuildingUnderUpgrade();
                    GameSparksManager.Instance.SaveStonesUnderUpgrade();
                }

                if (Player.tutorial == 1)
                    tutorialSpeedingUp();
            }
            else if (Player.diamond < speedUpCost)
            {
                //Insufficient diamond pop up
                UIManager.Instance.Notification_Pop_Up("diamonds", 1);
            }
            SpeedUpPopUpToggle(false);
        }
        else
        {
            //Find the field of timer which the player has selected
            int amountLeft = (int)Player.Instance.GetType().GetField("timer" + speedUps).GetValue(Player.Instance);
            //and check if there are any remaining amount of it
            if (amountLeft >= 1)
            {
                Player.Instance.GetType().GetField("timer" + speedUps).SetValue(Player.Instance, amountLeft - 1);
                StoreTempSpeedUp(speedUps);
                UpdateTimerUsage();
                float timeReduction = speedUps * 60;
                if (toBeSpedUp == "building")
                {
                    if (speedupTimer0 <= timeReduction)
                    {
                        speedupTimer0 = 0.02f;
                        speedUpFilled();
                    }
                    else if (speedupTimer0 > timeReduction)
                    {
                        speedupTimer0 -= timeReduction;
                    }
                }
                else if (toBeSpedUp == "stone0")
                {
                    if (speedupEarthStone0 <= timeReduction)
                    {
                        speedupEarthStone0 = 0.01f;
                        speedUpFilled();
                    }
                    else if (speedupEarthStone0 > timeReduction)
                    {
                        speedupEarthStone0 -= timeReduction;
                    }
                }
                else if (toBeSpedUp == "stone1")
                {
                    if (speedupWindStone0 <= timeReduction)
                    {
                        speedupWindStone0 = 0.01f;
                        speedUpFilled();
                    }
                    else if (speedupWindStone0 > timeReduction)
                    {
                        speedupWindStone0 -= timeReduction;
                    }
                }
                else if (toBeSpedUp == "stone2")
                {
                    if (speedupFlameStone0 <= timeReduction)
                    {
                        speedupFlameStone0 = 0.01f;
                        speedUpFilled();
                    }
                    else if (speedupFlameStone0 > timeReduction)
                    {
                        speedupFlameStone0 -= timeReduction;
                    }
                }
                else if (toBeSpedUp == "stone3")
                {
                    if (speedupEarthStone1 <= timeReduction)
                    {
                        speedupEarthStone1 = 0.01f;
                        speedUpFilled();
                    }
                    else if (speedupEarthStone1 > timeReduction)
                    {
                        speedupEarthStone1 -= timeReduction;
                    }
                }
                else if (toBeSpedUp == "stone4")
                {
                    if (speedupWindStone1 <= timeReduction)
                    {
                        speedupWindStone1 = 0.01f;
                        speedUpFilled();
                    }
                    else if (speedupWindStone1 > timeReduction)
                    {
                        speedupWindStone1 -= timeReduction;
                    }
                }
                else if (toBeSpedUp == "stone5")
                {
                    if (speedupFlameStone1 <= timeReduction)
                    {
                        speedupFlameStone1 = 0.01f;
                        speedUpFilled();
                    }
                    else if (speedupFlameStone1 > timeReduction)
                    {
                        speedupFlameStone1 -= timeReduction;
                    }
                }

                UIManager.Instance.UpdateSpeedUpInfo();
                Calculate_SpeedUp_Cost();
            }
            else if (amountLeft < 1)
            {
                string insufficient = speedUps + "m Timers";
                UIManager.Instance.Notification_Pop_Up(insufficient, 1);
            }
        }
    }
    // To store how many of each individual speed ups the player used
    void StoreTempSpeedUp(int typeOfSpeedup)
    {
        // Store how many of each timers the player intends to use
        if (typeOfSpeedup == 3)
            temp3usage += 1;
        else if (typeOfSpeedup == 15)
            temp15usage += 1;
        else if (typeOfSpeedup == 30)
            temp30usage += 1;
        else if (typeOfSpeedup == 60)
            temp60usage += 1;

    }
    void UpdateTimerUsage()
    {
        speedupThreeM.text = string.Format("Apply :\n{0}", temp3usage);
        speedupFifteenM.text = string.Format("Apply :\n{0}", temp15usage);
        speedupThirtyM.text = string.Format("Apply :\n{0}", temp30usage);
        speedupSixtyM.text = string.Format("Apply :\n{0}", temp60usage);
    }


    // Outcome of Speed up
    public void ApplyTimer()
    {
        if (toBeSpedUp == "building")
        {
            buildingTimer[0] = speedupTimer0;
            if (buildingTimer[0] <= 0)
                buildingTimer[0] = 0.2f;
        }
        else if (toBeSpedUp == "stone0")
        {
            earthstone0_Build_Time = speedupEarthStone0;
            if (earthstone0_Build_Time <= 0)
                earthstone0_Build_Time = 0.2f;
        }
        else if (toBeSpedUp == "stone3")
        {
            earthstone1_Build_Time = speedupEarthStone1;
            if (earthstone1_Build_Time <= 0)
                earthstone1_Build_Time = 0.2f;
        }
        else if (toBeSpedUp == "stone1")
        {
            windStone0_Build_Time = speedupWindStone0;
            if (windStone0_Build_Time <= 0)
                windStone0_Build_Time = 0.2f;
        }
        else if (toBeSpedUp == "stone4")
        {
            windStone1_Build_Time = speedupWindStone1;
            if (windStone1_Build_Time <= 0)
                windStone1_Build_Time = 0.2f;
        }
        else if (toBeSpedUp == "stone2")
        {
            flameStone0_Build_Time = speedupFlameStone0;
            if (flameStone0_Build_Time <= 0)
                flameStone0_Build_Time = 0.2f;
        }
        else if (toBeSpedUp == "stone5")
        {
            flameStone1_Build_Time = speedupFlameStone1;
            if (flameStone1_Build_Time <= 0)
                flameStone1_Build_Time = 0.2f;
        }

        ClearTempUsage();

        if (Player.tutorial >= Player.tutorialCap)
        {
            DataManager.Instance.PrepareDataForSparksPushing();

            GameSparksManager.Instance.SaveInventoryTimers();
            GameSparksManager.Instance.SavePlayerExpendables(GameSparksManager.playerId);

            GameSparksManager.Instance.SaveBuildingUnderUpgrade();
            GameSparksManager.Instance.SaveStonesUnderUpgrade();
        }
    }
    public void Cancel_Speed_Up()
    {
        RestoreSpeedUpTimers();
        ClearTempUsage();
        NullifyToBeSpedUp();
    }

    // Clear the amount of stored Temp Timer usage and set the OBJ to be sped up to Null
    void ClearTempUsage()
    {
        temp3usage = 0;
        temp15usage = 0;
        temp30usage = 0;
        temp60usage = 0;
    }
    // In the Case where the players decided to cancel the Speed Up
    void RestoreSpeedUpTimers()
    {
        Player.timer3 += temp3usage;
        Player.timer15 += temp15usage;
        Player.timer30 += temp30usage;
        Player.timer60 += temp60usage;
    }
    void NullifyToBeSpedUp()
    {
        toBeSpedUp = null;
    }
    // === SPEED UP AREA END === \\


    public void LoadScaffold(int buildingSlot)
    {
        if (Player.accountType == "Guest")
        {
            if (PlayerPrefs.HasKey("_BuildingOneUpgraded"))
            {
                string building = (PlayerPrefs.GetString("_BuildingOneUpgraded"));
                for (int x = 0; x < buildingGameObjects.Length; x++)
                {
                    if (!building.Contains("Mine"))
                    {
                        if (buildingGameObjects[x].name.Contains(building))
                        {
                            buildingGameObjects[x].GetComponent<HomeBuildings>().DisplayProgressionBuild();
                            buildingGameObjects[x].GetComponent<HomeBuildings>().ShowScaffold();
                            break;
                        }
                    }
                    else
                    {
                        string goldMineUp = (PlayerPrefs.GetInt("_GoldMineUpgrading")).ToString();
                        if (buildingGameObjects[x].name.Contains(goldMineUp))
                        {
                            buildingGameObjects[x].GetComponent<HomeBuildings>().ShowScaffold();
                            buildingGameObjects[x].GetComponent<HomeBuildings>().DisplayProgressionBuild();
                            buildingGameObjects[x].GetComponent<ResourceGenerate>().UpgradeRSSBuilding(false);
                            break;
                        }
                    }
                }
            }
        }
        else if (Player.accountType == "Registered")
        {
            string building = queuedBuilding[buildingSlot];
            if (building.Contains("Mine"))
            {
                building = building + UIManager.Instance.goldMineConverter(Player.mine_Upgrading_Now);
            }
            for (int x = 0; x < buildingGameObjects.Length; x++)
            {
                // IF its not a mine
                if (!building.Contains("Mine"))
                {
                    if (buildingGameObjects[x].name.Contains(building))
                    {
                        buildingGameObjects[x].GetComponent<HomeBuildings>().ShowScaffold();
                        buildingGameObjects[x].GetComponent<HomeBuildings>().DisplayProgressionBuild();
                        break;
                    }
                }
                // if the building being upgraded is a mine
                else
                {
                    if (buildingGameObjects[x].name.Contains(building))
                    {

                        buildingGameObjects[x].GetComponent<HomeBuildings>().ShowScaffold();
                        buildingGameObjects[x].GetComponent<HomeBuildings>().DisplayProgressionBuild();
                        buildingGameObjects[x].GetComponent<ResourceGenerate>().UpgradeRSSBuilding(false);
                        break;
                    }
                }
            }
        }
    }

    // Reset Values when the player Logs Out of this account,so the value does not get brought over
    public void LogOutReset()
    {
        for (int x = 0; x < isBuildingStone.Length; x++)
        {
            isBuildingStone[x] = false;
        }
        earthstone0_Build_Time = 0;
        earthstone1_Build_Time = 0;
        windStone0_Build_Time = 0;
        windStone1_Build_Time = 0;
        flameStone0_Build_Time = 0;
        flameStone1_Build_Time = 0;
        queuedStone0 = "";
        queuedStone1 = "";
        queuedStone2 = "";
        queuedStone3 = "";
        queuedStone4 = "";
        queuedStone5 = "";
    }


    //  ======= FOR GUESTS ======= \\


    /*
    public void LoadTimer()
    {
        // ---------- FOR UNITS ---------- \\ 
        //      int groupArrayCount = PlayerPrefs.GetInt ("GroupArrayCount");
        //      if (groupArrayCount > 0 & unitMyGroup.Count != 0)
        //      {
        //          for (int i = 0; i < groupArrayCount; i++)
        //          {
        //              unitMyGroup [i] = (PlayerPrefs.GetFloat ("GroupBuildTimer" + i));
        //              queuedMyGroup [i] = (PlayerPrefs.GetString ("QueuedGroups" + i));
        //          }
        //      }
        //      else if(groupArrayCount > 0 & unitMyGroup.Count == 0)
        //      {
        //          for (int i = 0; i < groupArrayCount; i++)
        //          {
        //              unitMyGroup.Add (PlayerPrefs.GetFloat ("GroupBuildTimer" + i));
        //              queuedMyGroup.Add(PlayerPrefs.GetString ("QueuedGroups" + i));
        //          }
        //      }

        // ---------- FOR BUILDINGS ---------- \\
        //If there is a building that was saved
        //      if (PlayerPrefs.HasKey("_BuildingOneUpgraded"))
        //      {
        //          Debug.Log (PlayerPrefs.GetString("_BuildingOneUpgraded"));
        //          for (int x = 0; x < UIManager.Instance.buildingPopUps.Length; x++)
        //          {
        //              if (UIManager.Instance.buildingPopUps[x].name.Contains(PlayerPrefs.GetString("_BuildingOneUpgraded")))
        //              {                   
        //                  UIManager.Instance.buildingPopUps[x].GetComponent<ProgressBarHandler>().theProgressBar.SetActive(true);
        //              }
        //          }
        //      }

        // === For Stones === \\
        if (PlayerPrefs.HasKey("_myStoneZeroSlot"))
        {
            queuedStone0 = PlayerPrefs.GetString("_myStoneZeroSlot");
            earthstone0_Build_Time = PlayerPrefs.GetFloat("_mySlotZeroStoneTimer");
        }

        if (PlayerPrefs.HasKey("_myStoneOneSlot"))
        {
            queuedStone1 = PlayerPrefs.GetString("_myStoneOneSlot");
            windStone0_Build_Time = PlayerPrefs.GetFloat("_mySlotOneStoneTimer");
        }

        if (PlayerPrefs.HasKey("_myStoneTwoSlot"))
        {
            queuedStone2 = PlayerPrefs.GetString("_myStoneTwoSlot");
            flameStone0_Build_Time = PlayerPrefs.GetFloat("_mySlotTwoStoneTimer");
        }
        //      queuedStone1 = PlayerPrefs.GetString("_myStoneOneSlot");
        //      queuedStone2 = PlayerPrefs.GetString("_myStoneTwoSlot");

        //       Retrieve info on how many buildings-in-progress was saved
        int arrayCountBuilding = PlayerPrefs.GetInt("ArrayCountBuilding");
        if (arrayCountBuilding > 0 & buildingTimer.Count != 0)
        {
            for (int i = 0; i < arrayCountBuilding; i++)
            {
                buildingTimer[i] = (PlayerPrefs.GetFloat("BuildingTimer" + i));
                queuedBuilding[i] = (PlayerPrefs.GetString("QueuedBuilding" + i));
                if (queuedBuilding[i].Contains("Mine"))
                {
                    Player.mine_Upgrading_Now = PlayerPrefs.GetInt("_GoldMineUpgrading");
                }
            }
        }
        else if (arrayCountBuilding > 0 & buildingTimer.Count == 0)
        {
            for (int i = 0; i < arrayCountBuilding; i++)
            {
                buildingTimer.Add(PlayerPrefs.GetFloat("BuildingTimer" + i));
                queuedBuilding.Add(PlayerPrefs.GetString("QueuedBuilding" + i));
            }
        }
        
        LoadScaffold(0);
        ImBack();
    }
    public void ImBack()
    {
        //For structures
        if (buildingTimer.Count != 0)
        {
            for (int i = 0; i <= buildingTimer.Count - 1; i++)
            {
                float timePassedTwo = TimeMaster.timePassed;
                float toSubtract = Math.Min(timePassedTwo, buildingTimer[i]);
                buildingTimer[i] -= toSubtract;
                timePassedTwo -= toSubtract;

                if (timePassedTwo == 0)
                    break;
            }
            if (queuedBuilding[0].Contains("Mine"))
                mineBuildingTimer = PlayerPrefs.GetFloat("_BuildingOneDuration");
            else if (queuedBuilding[0].Contains("Bunker"))
                bunkerBuildingTimer = PlayerPrefs.GetFloat("_BuildingOneDuration");
            else if (queuedBuilding[0].Contains("Range"))
                rangesBuildingTimer = PlayerPrefs.GetFloat("_BuildingOneDuration");
            else if (queuedBuilding[0].Contains("Guild"))
                guildBuildingTimer = PlayerPrefs.GetFloat("_BuildingOneDuration");
            else if (queuedBuilding[0].Contains("RecruitmentHall"))
                rHallBuildingTimer = PlayerPrefs.GetFloat("_BuildingOneDuration");
            else if (queuedBuilding[0].Contains("TownHall"))
                tHallBuildingTimer = PlayerPrefs.GetFloat("_BuildingOneDuration");
            else if (queuedBuilding[0].Contains("Treasury"))
                treasuryBuildingTimer = PlayerPrefs.GetFloat("_BuildingOneDuration");
            else if (queuedBuilding[0].Contains("Forge"))
                forgeBuildingTimer = PlayerPrefs.GetFloat("_BuildingOneDuration");
        }
    }
    public void SaveTimer()
    {
        // ---------- FOR UNITS ---------- \\
        // Get the length of the array, because timer.Count equals to 0 before loading
        // For Melee units
        //      PlayerPrefs.SetInt ("GroupArrayCount", unitMyGroup.Count);
        //      for (int i = 0; i < unitMyGroup.Count; i++)
        //      {
        //          PlayerPrefs.SetFloat ("GroupBuildTimer" + i, unitMyGroup [i]);
        //          PlayerPrefs.SetString ("QueuedGroups" + i, queuedMyGroup [i]);
        //      }
        // ================================= \\

        // ----------FOR STONES----------- \\
        if (isBuildingStone[0] == true && !PlayerPrefs.HasKey("_mySlotZeroStoneTimer"))
        {
            PlayerPrefs.SetString("_myStoneZeroSlot", queuedStone0);
            PlayerPrefs.SetFloat("_mySlotZeroStoneTimer", earthstone0_Build_Time);
        }

        if (isBuildingStone[1] == true && !PlayerPrefs.HasKey("_mySlotOneStoneTimer"))
        {
            PlayerPrefs.SetString("_myStoneOneSlot", queuedStone1);
            PlayerPrefs.SetFloat("_mySlotOneStoneTimer", windStone0_Build_Time);
        }

        if (isBuildingStone[2] == true && !PlayerPrefs.HasKey("_mySlotTwoStoneTimer"))
        {
            PlayerPrefs.SetString("_myStoneTwoSlot", queuedStone2);
            PlayerPrefs.SetFloat("_mySlotTwoStoneTimer", flameStone0_Build_Time);
        }

        //      PlayerPrefs.SetString("_myStoneOneSlot", queuedStone1);
        //      PlayerPrefs.SetString("_myStoneTwoSlot", queuedStone2);

        // ---------- FOR BUILDINGS ---------- \\
        //Save number of buildings that is being built
        PlayerPrefs.SetInt("ArrayCountBuilding", buildingTimer.Count);
        for (int i = 0; i < buildingTimer.Count; i++)
        {
            PlayerPrefs.SetFloat("BuildingTimer" + i, buildingTimer[i]);
            PlayerPrefs.SetString("QueuedBuilding" + i, queuedBuilding[i]);
        }
        if (Player.accountType == "Guest")
            Player.SavePrefs();
        // ===================================== \\
    }
*/


    // ---- TESTING PURPOSE ---- \\

    #region Badlands

    #endregion
}
