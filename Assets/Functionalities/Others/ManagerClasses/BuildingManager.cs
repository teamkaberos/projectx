﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingManager : MonoBehaviour
{
    #region singleton
    // This class will be in charge of everything upgradable or buildable
    private static BuildingManager _instance;
    public static BuildingManager Instance
    {
        get
        {
            return _instance;
        }
    }
    void Awake()
    {
        _instance = this;
    }

    #endregion
    public static bool structInit, stoneInit, rssGenInit;

    // ==== STRUCTURE VALUES ==== \\
    int tempTownHallLV, tempPrepHallLv, tempBunkerLv, tempRangeLv, tempGuildLv, tempTreasuryLv, tempForgeLv, tempCastleLv;   // Local Values for structures
    int[] tempMineLv = new int[5];                             // To allow for reusable codes so they don't disturb those that will be sent to GS

    int tempAssignmentCost = 100;

    int tempTownHallCost = 3000;
    float tempTownHallDuration = 30f;

    int tempBunkerCost = 1000;
    int tempRangeCost = 1000;
    int tempGuildCost = 1000;
    float tempBunkerDuration = 30f;
    float tempRangeDuration = 30f;
    float tempGuildDuration = 30f;

    int tempTreasuryCost = 1000;
    int tempTreasuryStorageCap = 20000;
    float tempTreasuryDuration = 30f;

    int[] tempForgeCost = { 25000, 150000, 1000000, 0 };

    //float tempPrepHallDuration = 5f;
    float tempShufflingTimer = 13.2f;

    //float tempForgeDuration = 5f;

    int[] tempMineCost = { 1000, 1000, 1000, 1000, 1000 };
    float[] tempMineDuration = { 5, 5, 5, 5, 5 };
    int[] tempMineRate = { 650, 650, 650, 650, 650 };

    List<int> upgradeInProgressID;
    List<float> upgradeInProgressTime;

    // === The 3D OBJ of the structures in Home Scene === \\
    public GameObject[] myHomeBuildings;
    [HideInInspector] public List<HomeBuildings> myHomeBuildings_ = new List<HomeBuildings>();
    int[] barracksNo = { 2, 3, 4 };

    // Storage of the last 3D OBJ selected/tapped by the player
    public static GameObject lastSelectedBuillding;

    public ResourceObtain rssObtainedDisplayCanvas;     //for display the obtained RSS
    public ResourceGenerate[] goldMine;
    public ResourceGenerate[] lumberMill;

    public bool[] thisPopup;


    void Start()
    {
        for (int x = 0; x < myHomeBuildings.Length; x++)
        {
            myHomeBuildings_.Add(myHomeBuildings[x].GetComponent<HomeBuildings>());
        }

        RssCollectDelegate();
        ResetLocalValues();
        //Invoke("DistributeStructureLevels", 1.5f);
    }
    void OnDestroy()
    {
        //LogOutReset();
        ClearUpgradeInProgress();
    }

    void RssCollectDelegate()
    {
        //This is for the RSS collection display on the generators
        for (int x = 0; x < 5; x++)
        {
            rssObtainedDisplayCanvas.SubscribeToRSSGenerator(goldMine[x]);
        }
    }
    void ResetLocalValues()
    {
        // Set temp values to original values to enable Structure loop to take place;
        tempTownHallLV = 0;
        tempPrepHallLv = 0;
        tempBunkerLv = 0;
        tempRangeLv = 0;
        tempGuildLv = 0;
        tempTreasuryLv = 0;
        tempForgeLv = 0;
        for (int x = 0; x < tempMineLv.Length; x++)
        {
            tempMineLv[x] = 0;
            tempMineCost[x] = 1000;
            tempMineDuration[x] = 5;
            tempMineRate[x] = 650;
        }
        tempCastleLv = 1;

        tempTownHallCost = 3000;
        tempTownHallDuration = 30f;

        tempBunkerCost = 1000;
        tempRangeCost = 1000;
        tempGuildCost = 1000;
        tempBunkerDuration = 30f;
        tempRangeDuration = 30f;
        tempGuildDuration = 30f;

        tempTreasuryCost = 1000;
        tempTreasuryStorageCap = 20000;
        tempTreasuryDuration = 30f;

        tempTreasuryDuration = 30f;

        //tempPrepHallDuration = 5f;
        tempShufflingTimer = 12.2f;

        tempAssignmentCost = 100;
        //tempForgeDuration = 5f;
    }
    public void DistributeStructureLevels()
    {
        UpgradeTownHall();
        UpgradePreparationHall();
        UpgradeBunker();
        UpgradeRange();
        UpgradeGuild();
        UpgradeTreasury();
        UpgradeForge();
        UpgradeRSS();
        UpgradeCastle();
        MeshUpdate();

        UIManager.Instance.Update_Building_LevelInfo();
        UIManager.Instance.UpdatePlayerInfo();
        UIManager.Instance.UpdateCurrencyInfo();


        if (!structInit) CheckForUpgradeInProgress();
        if (!stoneInit) CheckForStoneBuildInProgress();
        if (!rssGenInit) FillInRssGenerated();
    }

    public void BuildingLevelUp()
    {
        Player.castleLevel = 1;
        Player.townHallLevel = 1;
        Player.bunkerLv = 1;
        Player.rangeLv = 1;
        Player.guildLv = 1;
        Player.recruitmentHall_Lvl = 1;
        Player.treasuryLvl = 1;
        Player.treasury_Storage_Cap = 27000;


        tempTreasuryStorageCap = 27000;
        tempShufflingTimer = 13.2f;

        for (int x = 0; x < Player.mines.Length; x++)
        {
            Player.mines[x] = 1;
        }
        DistributeStructureLevels();


        Player.tutorial = 9;

        Player.excalipoints = Random.Range(12784, 457573);

        int randLv = Random.Range(3, 8);

        Player.my_Matches = Random.Range(randLv * 124, 255);
        Player.my_Victories = Random.Range(Mathf.FloorToInt(Player.my_Matches * 0.4f), Player.my_Matches);
        Player.my_Losses = Player.my_Matches - Player.my_Victories;

        Player.reputation = (Mathf.FloorToInt(Player.my_Victories * 0.85f) * 3) + Random.Range(480, 645) + 100;

        Player.townHallLevel = (randLv * 3);
        Player.bunkerLv = (randLv * 3) + Random.Range(1, 4);
        Player.rangeLv = (randLv * 3) + Random.Range(1, 4);
        Player.guildLv = (randLv * 3) + Random.Range(1, 4);
        Player.recruitmentHall_Lvl = randLv * 3;
        Player.treasuryLvl = (randLv * 3) + Random.Range(1, 4);

        for (int x = 0; x < Player.mines.Length; x++)
        {
            if (x <= randLv - 1)
                Player.mines[x] = (randLv * 3) + Random.Range(1, 4);
            else
                Player.mines[x] = 0;
        }

        DistributeStructureLevels();

        for (int x = 0; x < randLv; x++)
            Player.Instance.Check_If_Castle_Upgradable();


        DataManager.Instance.PrepareDataForSparksPushing();

        GameSparksManager.Instance.StoreAllData();

        GameSparksManager.Instance.SavePlayerData(GameSparksManager.playerId);
        GameSparksManager.Instance.SavePlayerDetails();

        GameSparksManager.Instance.SaveLastLogout();
    }


    // ==== STRUCTURE UPGRADES AREA ==== \\
    public void UpgradeTownHall()
    {
        for (int x = 0; x < Player.townHallLevel; x++)
        {
            tempTownHallLV += 1;
            tempTownHallCost = CompoundValueCalculator(tempTownHallCost, Player.townHallMultiplier);
            tempTownHallDuration = DurationMultiplier(tempTownHallLV);
        }
        Player.townHallCost = tempTownHallCost;
        Player.townHall_Upgrade_Duration = tempTownHallDuration;
        Player.Instance.Check_If_Castle_Upgradable();
    }
    public void UpgradePreparationHall()
    {
        for (int x = 0; x < Player.recruitmentHall_Lvl; x++)
        {
            tempPrepHallLv += 1;
            tempShufflingTimer -= 0.2f;
        }
        Player.shufflingTime = tempShufflingTimer;
    }
    public void UpgradeBunker()
    {
        for (int x = 0; x < Player.bunkerLv; x++)
        {
            tempBunkerLv += 1;
            tempBunkerCost = CompoundValueCalculator(tempBunkerCost, Player.barrackMultiplier);
            tempBunkerDuration = DurationMultiplier(tempBunkerLv);
        }
        Player.bunkerCost = tempBunkerCost;
        Player.bunker_Upgrade_Duration = tempBunkerDuration;
    }
    public void UpgradeRange()
    {
        for (int x = 0; x < Player.rangeLv; x++)
        {
            tempRangeLv += 1;
            tempRangeCost = CompoundValueCalculator(tempRangeCost, Player.barrackMultiplier);
            tempRangeDuration = DurationMultiplier(tempRangeLv);
        }
        Player.rangeCost = tempRangeCost;
        Player.range_Upgrade_Duration = tempRangeDuration;
    }
    public void UpgradeGuild()
    {
        for (int x = 0; x < Player.guildLv; x++)
        {
            tempGuildLv += 1;
            tempGuildCost = CompoundValueCalculator(tempGuildCost, Player.barrackMultiplier);
            tempGuildDuration = DurationMultiplier(tempGuildLv);
        }
        Player.guildCost = tempGuildCost;
        Player.guild_Upgrade_Duration = tempGuildDuration;
    }
    public void UpgradeTreasury()
    {
        for (int x = 0; x < Player.treasuryLvl; x++)
        {
            tempTreasuryLv += 1;
            //Debug.Log(tempTreasuryStorageCap);
            tempTreasuryCost = CompoundValueCalculator(tempTreasuryCost, Player.treasuryMultiplier);
            tempTreasuryStorageCap = CompoundValueCalculator(tempTreasuryStorageCap, Player.treasury_Storage_Multiplier);
            tempTreasuryDuration = DurationMultiplier(tempTreasuryLv);
        }
        Player.treasuryCost = tempTreasuryCost;
        Player.treasury_Storage_Cap = tempTreasuryStorageCap;
        Player.treasury_Upgrade_Duration = tempTreasuryDuration;
    }
    public void UpgradeForge()
    {
        for (int x = 0; x < Player.forgeLevel; x++)
            tempForgeLv += 1;
        Player.forge_UpgradeCost = tempForgeCost[Player.forgeLevel];
        Update_Forge_Building();
    }
    public void UpgradeRSS()
    {
        //Loop through all the mines
        for (int x = 0; x < Player.mines.Length; x++)
        {
            // And increase the Temp Mine Level values according to the respective mine Level
            // Within this loop, the values of the X mine is altered . . .
            for (int y = 0; y < Player.mines[x]; y++)
            {
                tempMineLv[x] += 1;
                tempMineCost[x] = CompoundValueCalculator(tempMineCost[x], Player.rssGenMultiplier);
                tempMineDuration[x] = DurationMultiplier(tempMineLv[x]);
                if (Player.mines[x] == 0)
                    tempMineRate[x] = 0;
                else
                    tempMineRate[x] = CompoundValueCalculator(tempMineRate[x], Player.rssGenRateMultiplier);
            }
            //  . . . and sent to the Player for updating
            if (Player.mines[x] >= 1)
            {
                Player.minesCost[x] = tempMineCost[x];
                Player.mines_Upgrade_Duration[x] = tempMineDuration[x];
                Player.minesRate[x] = tempMineRate[x];
                Player.minesCap[x] = tempMineRate[x] * Player.fillUpRateMultiplier;
            }
            else
            {
                Player.minesCost[x] = 1000;
                Player.mines_Upgrade_Duration[x] = 5;
                Player.minesRate[x] = 0;
                Player.minesCap[x] = 0;
            }
        }
        Assign_RSSGenerator_Level();
    }
    public void UpgradeCastle()
    {
        for (int x = 0; x < Player.castleLevel; x++)
        {
            tempCastleLv += 1;
            tempAssignmentCost += Player.assignmentIncrement;
        }
        Player.assign_Points_Limit = tempAssignmentCost;
        Player.available_Assign_Points = tempAssignmentCost;
    }
    public void MeshUpdate()
    {
        for (int x = 0; x < myHomeBuildings_.Count; x++)
        {
            if (x < 6 || x > 10)
            {
                myHomeBuildings_[x].DisplayBuilding(x);
            }
        }
    }

    // ==== STRUCTURE VALUES CALCULATOR ==== \\
    int CompoundValueCalculator(int baseValue, float multiplier)
    {
        int compoundValue = Mathf.FloorToInt(baseValue * multiplier);
        return compoundValue;
    }
    float DurationMultiplier(int structureLevel)
    {
        // All structures upgrade duration are the same except for preparation halls and forge
        // so using this will allow the code to be reused
        if (structureLevel >= 1 && structureLevel <= 6)
            return structureLevel * 1500;
        if (structureLevel >= 7 && structureLevel <= 15)
            return structureLevel * 4500;
        if (structureLevel >= 16 && structureLevel <= 21)
            return structureLevel * 13500;
        if (structureLevel >= 22 && structureLevel <= 29)
            return structureLevel * 22500;
        return 0;
    }
    public float DurationRequest(int structureLevel)
    {
        return DurationMultiplier(structureLevel);
    }


    // ==== BATTLE POINTS FULL NOTIFICATION ==== \\
    public void DisplayBattlePointsNotification()
    {
        for (int x = 0; x < barracksNo.Length; x++)
        {
            myHomeBuildings_[barracksNo[x]].CheckFullBattlePointNotification();
        }
    }


    // Identifying the Structure saved in the GS data and changing them into a string
    public string Convert_Building_ID_To_Name(int buildingID)
    {
        if (buildingID == 0)
            return null;
        if (buildingID == 1)
            return "TownHall";
        if (buildingID == 2)
            return "Bunker";
        if (buildingID == 3)
            return "Range";
        if (buildingID == 4)
            return "Guild";
        if (buildingID == 5)
            return "Treasury";
        if (buildingID == 6)
        {
            Player.mine_Upgrading_Now = 0;
            Player.mine_That_Player_Last_Tapped_On = 0;
            return "Mine";
        }
        if (buildingID == 7)
        {
            Player.mine_Upgrading_Now = 1;
            Player.mine_That_Player_Last_Tapped_On = 1;
            return "Mine";
        }
        if (buildingID == 8)
        {
            Player.mine_Upgrading_Now = 2;
            Player.mine_That_Player_Last_Tapped_On = 2;
            return "Mine";
        }
        if (buildingID == 9)
        {
            Player.mine_Upgrading_Now = 3;
            Player.mine_That_Player_Last_Tapped_On = 3;
            return "Mine";
        }
        if (buildingID == 10)
        {
            Player.mine_Upgrading_Now = 4;
            Player.mine_That_Player_Last_Tapped_On = 4;
            return "Mine";
        }
        if (buildingID == 11)
            return "Forge";
        if (buildingID == 12)
            return "RecruitmentHall";

        return null;
    }


    // ==-- RSS BUILDING UPGRADES AREA -- == //
    public void StartUpgading_RSS_Building(string buildingType)
    {
        //		if (buildingType == "Mill")
        //		{
        //			lumberMill[Player.mill_That_Player_Last_Tapped_On].GetComponent<ResourceGenerate>().UpgradeRSSBuilding(true);
        //		}
        if (buildingType == "Mine")
        {
            goldMine[Player.mine_That_Player_Last_Tapped_On].UpgradeRSSBuilding(true);
        }
    }
    public void FinishUpgrading_RSS_Building(string buildingType)
    {
        //		if (buildingType == "Mill")
        //		{
        //			lumberMill[Player.mill_Upgrading_Now].GetComponent<ResourceGenerate>().UpgradeCompleteRSSBuilding();
        //			lumberMill[Player.mill_That_Player_Last_Tapped_On].GetComponent<ResourceGenerate>().generatorLvl = Player.mills[Player.mill_That_Player_Last_Tapped_On];
        //			lumberMill[Player.mill_That_Player_Last_Tapped_On].GetComponent<ResourceGenerate>().generationRate = Player.millsRate[Player.mill_That_Player_Last_Tapped_On];
        //			lumberMill[Player.mill_That_Player_Last_Tapped_On].GetComponent<ResourceGenerate>().generatorCap = Player.millsCap[Player.mill_That_Player_Last_Tapped_On];
        //		}
        if (buildingType == "Mine")
        {
            goldMine[Player.mine_Upgrading_Now].UpgradeCompleteRSSBuilding();
            goldMine[Player.mine_That_Player_Last_Tapped_On].generatorLvl = Player.mines[Player.mine_That_Player_Last_Tapped_On];
            goldMine[Player.mine_That_Player_Last_Tapped_On].generationRate = Player.minesRate[Player.mine_That_Player_Last_Tapped_On];
            goldMine[Player.mine_That_Player_Last_Tapped_On].generatorCap = Player.minesCap[Player.mine_That_Player_Last_Tapped_On];
        }
    }

    public void Assign_RSSGenerator_Level()
    {
        for (int x = 0; x < goldMine.Length; x++)
        {
            // --- GOLD MINE INFO ---
            goldMine[x].generatorLvl = Player.mines[x];
            goldMine[x].generationRate = Player.minesRate[x];
            goldMine[x].generatorCap = Player.minesCap[x];
            goldMine[x].currentResourceValue = Player.minesCurrentValue[x];
            goldMine[x].Update_RSS_Building_Model();
            goldMine[x].DisplayGoblins();
        }
    }


    // ++++=== INITIAL LOGIN CHECKS ===++++ \\
    void CheckForUpgradeInProgress()
    {
        structInit = true;
        long timeDifference = TimeMaster.Instance.CalculateTimeDifference(DataManager.initialLoginServerTime, DataManager.lastLogout);
        //first building
        if (BuildTimer.queuedBuilding.Count > 0)
        {
            float buildingTime = Player.Instance.UpgradingBuildingLevel(BuildTimer.queuedBuilding[0]);  // returns the full duration of the structure being upgraded

            // In case the upgrade duration exceeds the actual duration required, cap the upgrade duration for the structure
            if (BuildTimer.buildingTimer[0] > buildingTime)
                BuildTimer.buildingTimer[0] = buildingTime;

            buildingTime = BuildTimer.buildingTimer[0];
            buildingTime -= timeDifference;

            if (buildingTime <= 0.3f) BuildTimer.buildingTimer[0] = 0.5f;
            else if (buildingTime > 0.3f) BuildTimer.buildingTimer[0] = buildingTime;

            BuildTimer.Instance.LoadScaffold(0);
        }
    }
    void CheckForStoneBuildInProgress()
    {
        stoneInit = true;
        long timeDifference = TimeMaster.Instance.CalculateTimeDifference(DataManager.initialLoginServerTime, DataManager.lastLogout);   //Get time difference from server

        if (BuildTimer.earthstone0_Build_Time >= 0 & BuildTimer.earthstone0_Build_Time < 28800f)
        {
            BuildTimer.queuedStone0 = "0";
            BuildTimer.Instance.IsBuildingStone(0);
            BuildTimer.earthstone0_Build_Time -= timeDifference;
            if (BuildTimer.earthstone0_Build_Time <= 0)
                BuildTimer.earthstone0_Build_Time = 0.2f;
        }
        if (BuildTimer.earthstone1_Build_Time >= 0 & BuildTimer.earthstone1_Build_Time < 28800f)
        {
            BuildTimer.queuedStone3 = "0";
            BuildTimer.Instance.IsBuildingStone(3);
            BuildTimer.earthstone1_Build_Time -= timeDifference;
            if (BuildTimer.earthstone1_Build_Time <= 0)
                BuildTimer.earthstone1_Build_Time = 0.2f;
        }


        if (BuildTimer.windStone0_Build_Time >= 0 & BuildTimer.windStone0_Build_Time < 28800f)
        {
            BuildTimer.queuedStone1 = "1";
            BuildTimer.Instance.IsBuildingStone(1);
            BuildTimer.windStone0_Build_Time -= timeDifference;
            if (BuildTimer.windStone0_Build_Time <= 0)
                BuildTimer.windStone0_Build_Time = 0.2f;
        }
        if (BuildTimer.windStone1_Build_Time >= 0 & BuildTimer.windStone1_Build_Time < 28800f)
        {
            BuildTimer.queuedStone4 = "1";
            BuildTimer.Instance.IsBuildingStone(4);
            BuildTimer.windStone1_Build_Time -= timeDifference;
            if (BuildTimer.windStone1_Build_Time <= 0)
                BuildTimer.windStone1_Build_Time = 0.2f;
        }


        if (BuildTimer.flameStone0_Build_Time >= 0 & BuildTimer.flameStone0_Build_Time < 28800f)
        {
            BuildTimer.queuedStone2 = "2";
            BuildTimer.Instance.IsBuildingStone(2);
            BuildTimer.flameStone0_Build_Time -= timeDifference;
            if (BuildTimer.flameStone0_Build_Time <= 0)
                BuildTimer.flameStone0_Build_Time = 0.2f;
        }
        if (BuildTimer.flameStone1_Build_Time >= 0 & BuildTimer.flameStone1_Build_Time < 28800f)
        {
            BuildTimer.queuedStone5 = "2";
            BuildTimer.Instance.IsBuildingStone(5);
            BuildTimer.flameStone1_Build_Time -= timeDifference;
            if (BuildTimer.flameStone1_Build_Time <= 0)
                BuildTimer.flameStone1_Build_Time = 0.2f;
        }
    }
    void FillInRssGenerated()
    {
        rssGenInit = true;

        long timeDifference = TimeMaster.Instance.CalculateTimeDifference(DataManager.initialLoginServerTime, DataManager.lastLogout);   //Get time difference from server

        for (int x = 0; x < goldMine.Length; x++)
        {
            if (Player.mines[x] > 1)
                goldMine[x].Update_Current_Value(timeDifference);
        }
    }

    public void LogOutReset()
    {
        structInit = false;
        stoneInit = false;
        rssGenInit = false;
    }
    void ClearUpgradeInProgress()
    {
        BuildTimer.queuedBuilding.Clear();
        BuildTimer.buildingTimer.Clear();
    }

    // Update the Forge Mesh in the Home Area
    public void Update_Forge_Building()
    {
        myHomeBuildings[11].GetComponent<ForgeBuilding>().CheckLevel();
    }

    // ===== HIDE PLAYER BUILDING SELECTION ===== \\
    public void HideBuildingHighlight()
    {
        for (int x = 0; x < myHomeBuildings.Length; x++)
        {
            myHomeBuildings[x].GetComponent<HomeBuildings>().selectedDisplay.SetActive(false);
        }
    }

    public void CheckWhichPopUp(HomeBuildings theSelectedOne)
    {
        //Used for mini pop up infos
        for (int x = 0; x < myHomeBuildings_.Count; x++)
        {
            if (myHomeBuildings_[x] != theSelectedOne)
                myHomeBuildings_[x].clicked = false;
        }
    }

    // ====== CAMERA MOVEMENT ====== \\
    public void Switch_Last_Selected()
    {
        if (lastSelectedBuillding != null)
            lastSelectedBuillding.GetComponent<HomeBuildings>().clicked = false;
    }

    // ===== Placed in the Collect Gold button on the mini popup ===== \\
    public void CollectGold()
    {
        goldMine[Player.mine_That_Player_Last_Tapped_On].CollectResources();
    }

    // ===== For turning off the Rotating Diamond on the Home Scene ===== \\
    public void Turn_Off_Mine_Complete()
    {
        for (int x = 0; x < 5; x++)
        {
            goldMine[x].buildingComplete_Pop_Up.SetActive(false);
        }
    }


    #region NOT-IN-USE

    public void StructuresPopUpInfo()
    {
        lastSelectedBuillding.GetComponent<HomeBuildings>().BuildingPopUps();
    }

    #endregion
}
