﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using UnityEngine.Purchasing;

//// Deriving the Purchaser class from IStoreListener enables it to receive messages from Unity Purchasing.
//public class CashPurchases : MonoBehaviour, IStoreListener
//{
//	public static CashPurchases CashPurchases_S;

//	private static IStoreController m_StoreController;          // The Unity Purchasing system.
//	private static IExtensionProvider m_StoreExtensionProvider; // The store-specific Purchasing subsystems.

//	#if UNITY_EDITOR 
//	public static string Product_500k_Gold = "gold500k";   
//	public static string Product_3_Lives = "lives3";
//	#elif UNITY_ANDROID
//	public static string Product_500k_Gold = "gold500k";   
//	public static string Product_3_Lives = "lives3";
//	#elif UNITY_IOS
//	public static string Product_500k_Gold = "com.NewMediaInc.TappyToons.gold500k";   
//	public static string Product_3_Lives = "com.NewMediaInc.TappyToons.lives3";
//	#endif

//	void Awake()
//	{
//		CashPurchases_S = this;
//	}

//	void Start()
//	{
//		// If we haven't set up the Unity Purchasing reference
//		if (m_StoreController == null)
//		{
//			// Begin to configure our connection to Purchasing
//			InitializePurchasing();
//		}
//	}
//	public void InitializePurchasing() 
//	{
//		if (IsInitialized())
//		{
//			// ... we are done here.
//			return;
//		}

//		var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

//		builder.AddProduct(Product_500k_Gold, ProductType.Consumable);
//		builder.AddProduct(Product_3_Lives, ProductType.Consumable);

//		UnityPurchasing.Initialize(this, builder);
//	}
//	private bool IsInitialized()
//	{
//		return m_StoreController != null && m_StoreExtensionProvider != null;
//	}

//	public void Buy500kGold()
//	{
//		BuyProductID(Product_500k_Gold);
//  	}
//	public void Buy3Lives()
//	{
//		BuyProductID(Product_3_Lives);
//	}
////	public void Buy2ndFamiliarSlot()
////	{
////		BuyProductID(Product_2nd_Fam_Slot);
////	}

//	void BuyProductID(string productId)
//	{
//		if (IsInitialized())
//		{
//			Product product = m_StoreController.products.WithID(productId);

//			if (product != null && product.availableToPurchase)
//			{
//				Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
//				m_StoreController.InitiatePurchase(product);
//			}
//			else
//			{ 
//				Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
//			}
//		}
//		else
//		{
//			Debug.Log("BuyProductID FAIL. Not initialized.");
//		}
//	}

//		// Restore purchases previously made by this customer. Some platforms automatically restore purchases, like Google. 
//	// Apple currently requires explicit purchase restoration for IAP, conditionally displaying a password prompt.
//	public void RestorePurchases()
//	{
//		// If Purchasing has not yet been set up ...
//		if (!IsInitialized())
//		{
//			// ... report the situation and stop restoring. Consider either waiting longer, or retrying initialization.
//			Debug.Log("RestorePurchases FAIL. Not initialized.");
//			return;
//		}

//		// If we are running on an Apple device ... 
//		if (Application.platform == RuntimePlatform.IPhonePlayer || 
//			Application.platform == RuntimePlatform.OSXPlayer)
//		{
//			// ... begin restoring purchases
//			Debug.Log("RestorePurchases started ...");

//			// Fetch the Apple store-specific subsystem.
//			var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
//			// Begin the asynchronous process of restoring purchases. Expect a confirmation response in 
//			// the Action<bool> below, and ProcessPurchase if there are previously purchased products to restore.
//			apple.RestoreTransactions((result) => {
//				// The first phase of restoration. If no more responses are received on ProcessPurchase then 
//				// no purchases are available to be restored.
//				Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
//			});
//		}
//		// Otherwise ...
//		else
//		{
//			// We are not running on an Apple device. No work is necessary to restore purchases.
//			Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
//		}
//	}
		
//	public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
//	{
//		// Purchasing has succeeded initializing. Collect our Purchasing references.

//		// Overall Purchasing system, configured with products for this application.
//		m_StoreController = controller;
//		// Store specific subsystem, for accessing device-specific store features.
//		m_StoreExtensionProvider = extensions;
//  	}
//	public void OnInitializeFailed(InitializationFailureReason error)
//	{
//		// Purchasing set-up has not succeeded. Check error for reason. Consider sharing this reason with the user.
//		Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
//	}

//	public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args) 
//	{
//		if (String.Equals(args.purchasedProduct.definition.id, Product_500k_Gold, StringComparison.Ordinal))
//		{
//			PlayerS.munneh += 500000;
//			Debug.Log ("You've obtained 500k Gold!");
//		}
//		else if (String.Equals(args.purchasedProduct.definition.id, Product_3_Lives, StringComparison.Ordinal))
//		{
//			PlayerS.playerLives += 3;
//			Debug.Log("You've got 3 more lives!");
//		}
//		// Or ... an unknown product has been purchased by this user. Fill in additional products here....
//		else 
//		{
//			Debug.Log(string.Format("ProcessPurchase: FAIL. Unrecognized product: '{0}'", args.purchasedProduct.definition.id));
//		}

//		// Return a flag indicating whether this product has completely been received, or if the application needs 
//		// to be reminded of this purchase at next app launch. Use PurchaseProcessingResult.Pending when still 
//		// saving purchased products to the cloud, and when that save is delayed. 
//		return PurchaseProcessingResult.Complete;
//	}


//	public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
//	{
//		// A product purchase attempt did not succeed. Check failureReason for more detail. Consider sharing 
//		// this reason with the user to guide their troubleshooting actions.
//		Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
//	}
//}