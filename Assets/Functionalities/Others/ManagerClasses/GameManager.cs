﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public delegate void OnBuffValueChange();
    public static event OnBuffValueChange whenBuffValueChange;

    #region Singleton

    static GameManager _instance;
    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
            {
                GameObject gameManager = new GameObject("Game Manager");
                gameManager.AddComponent<GameManager>();
            }
            return _instance;
        }
    }

    #endregion

    // Uses string instead of int, because int cannot be null (can only be 0, which might cause problem)
    public static string storedUnitIndex;
    public static string storedSupportIndex;
    public static string storedHeroID;

    [Space(20)]
    [Header("Hero Units")]
    public Transform[] heroesAbility;

    public static GameObject[][] grids = new GameObject[3][];

    [Space(20)]
    [Header("Unit Cards")]
    public GameObject squareButtons;
    public GameObject triangleButtons;
    public GameObject circleButtons;

    [System.Serializable]
    public class Troops
    {
        public string name;
        public int troopNo;
        public Transform troopUnit;
        public GameObject troopCard;
    }
    public List<Troops> troopInfo = new List<Troops>();

    [Space(20)]
    [Header("Game Info")]
    public GameObject[] laneAGrids;
    public GameObject[] laneBGrids;
    public GameObject[] laneCGrids;

    public ColliderUnit[] laneAColliderUnits;
    public ColliderUnit[] laneBColliderUnits;
    public ColliderUnit[] laneCColliderUnits;

    public GameObject[] playerLocs;
    public GameObject[] enemyLocs;

    public GameObject tutorial_Popup;

    bool hasWon;
    public GameObject gameOverScreen;
    public GameObject victoryScreen;

    public GameObject garrison;
    public GameObject singleGarrison;

    // For determining which instance of UnitButton is to be put on cooldown
    public static UnitButton toBeDisabled;

    public static GameObject canvas;

    public static Color defaultGray;
    public static Color defaultGreen;

    public static bool stunnable;

    public static bool onVictory;
    public static bool onDefeat;

    // --- For FX --- \\
    public GameObject wizardBang;
    public GameObject archerBang;
    public GameObject knightBang;

    public GameObject landMineFX;

    public GameObject archerBangEnemy;
    public GameObject archerArrowParticles;

    // --- Hero Abilities --- //
    public static bool soulEnhanced;
    public static int soulChargeValue;

    public static int[] pHasted = new int[3];
    public static int[] pSlowed = new int[3];
    public static float[] pBlocked = new float[3];
    public static int[] pFrozenLane = new int[3];
    public static int[] pMinosSummoned = new int[3];

    public static int[] eHasted = new int[3];
    public static int[] eSlowed = new int[3];
    public static float[] eBlocked = new float[3];
    public static int[] eFrozenLane = new int[3];
    public static int[] eMinosSummoned = new int[3];

    public GameObject InfoText;
    public Text infoText_Text;

    public GameObject[] playerLaneFlames;
    public GameObject[] enemyLaneFlames;

    [Tooltip("Appears when player's lane is hasted")]
    public FXPlay[] playerHaste;
    [Tooltip("Appears when a player's lane is slowed")]
    public FXPlay[] playerSlow;
    [Tooltip("Appears when a player's lane gets blocked")]
    public FXPlay[] playerBlocked;
    [Tooltip("Appears when a player attempts to spawn on a blocked lane")]
    public FXPlay[] playerBlockedObject;     //the spark that appears when player tries to spawn on a blocked lane
    [Tooltip("Appears when a player's healer passes the enemy lines")]
    public FXPlay[] playerHeal;
    [Tooltip("Appears when a player summons Minotaur")]
    public FXPlay[] playerMinosBlock;
    public Animator[] playerMinosAnimation;

    [Tooltip("Appears when enemy's lane is hasted")]
    public FXPlay[] enemyHaste;
    [Tooltip("Appears when a enemy's lane is slowed")]
    public FXPlay[] enemySlow;
    [Tooltip("Appears when a enemy's lane gets blocked")]
    public FXPlay[] enemyBlocked;
    [Tooltip("Appears when a enemy attempts to spawn on a blocked lane")]
    public FXPlay[] enemyBlockedObject;    //the spark that appears when enemy tries to spawn on a blocked lane
    [Tooltip("Appears when a enemy's healer passes the enemy lines")]
    public FXPlay[] enemyHeal;
    [Tooltip("Appears when a player summons Minotaur")]
    public FXPlay[] enemyMinosBlock;
    public Animator[] enemyMinosAnimation;


    // **** Number of times to create HERO FX Obj **** \\
    int timesToCreate = 10;

    [Space(12)]
    [Header("Genie Stuff")]
    public GameObject genieBurstEffect; // the effect to instantiate
    List<FXPlay> genieBurstEffectList = new List<FXPlay>();
    int genieBurstToBePlayed;     // to loop the number that will be played
    public Transform genieFXHolder;

    [Space(12)]
    [Header("Teleporter Stuff")]
    public GameObject teleportFromFX;                    // the Teleport **FROM** effect;
    public GameObject teleportToFX;                // the Teleport **TO** effect
    List<FXPlay> teleportedBurstFromFX = new List<FXPlay>();
    List<FXPlay> teleportedToBurstFX = new List<FXPlay>();
    int teleportToBePlayed;                               // to loop the int of the FX that will be played
    public Transform teleporterFXHolder;         // the transform to 

    public StageTimer stageTime;


    // =============== \\


    void Awake()
    {
        defaultGreen = new Color(0.449f, 1f, 0.544f);
        _instance = this;
        canvas = GameObject.Find("UI Canvas");
        storedUnitIndex = null;

        //  -- Grid Arrays Assignment --  \\

        grids[0] = new GameObject[20];
        grids[1] = new GameObject[20];
        grids[2] = new GameObject[20];

        grids[0] = laneAGrids;
        grids[1] = laneBGrids;
        grids[2] = laneCGrids;

        CacheColliderUnits();
        ResetLaneBuffs();
        ResetStoredInfo();
        ResetSoulCharge();

        CreateGenieEffect();
        CreateTeleportedEffect();

        // ----------------------------- \\
        defaultGray = new Color(0.243f, 0.232f, 0.232f);
    }

    void Update()
    {
        BlockTimer();
    }

    void CacheColliderUnits()
    {
        laneAColliderUnits = new ColliderUnit[laneAGrids.Length];
        laneBColliderUnits = new ColliderUnit[laneBGrids.Length];
        laneCColliderUnits = new ColliderUnit[laneCGrids.Length];

        for (int x = 0; x < laneAGrids.Length; x++)
        {
            laneAColliderUnits[x] = laneAGrids[x].GetComponent<ColliderUnit>();
            laneBColliderUnits[x] = laneBGrids[x].GetComponent<ColliderUnit>();
            laneCColliderUnits[x] = laneCGrids[x].GetComponent<ColliderUnit>();
        }
    }

    void ResetLaneBuffs()
    {
        //Resets all BUFFS and DEBUFFS
        for (int x = 0; x < 3; x++)
        {
            // Values in lanes = values for haste boost
            pHasted[x] = 0;
            pSlowed[x] = 0;
            eBlocked[x] = 0;
            eHasted[x] = 0;
            eSlowed[x] = 0;
            pFrozenLane[x] = 0;
            eFrozenLane[x] = 0;
            pMinosSummoned[x] = 0;
            eMinosSummoned[x] = 0;
        }

        if (Player.tutorial == 4)
        {
            tutorial_Popup.SetActive(true);
        }
    }

    public void LaneBuffChanged()
    {
        whenBuffValueChange();
    }

    // ==== BUFF BEHAVIOUR ==== \\
    public void Haste_Lane(int laneToHaste, bool isFriendly, int abilityLevel)
    {
        //Check whether Player or Enemy Haste
        if (isFriendly)
        {
            //Check to see if the lane is already hasted
            //if it is hasted and Haste level is same/lower, then ignore
            if (pHasted[laneToHaste] >= abilityLevel * 10)
                return;
            //Otherwise set the lane's Haste value to the new Haste value
            pHasted[laneToHaste] = (abilityLevel * 10);
            //Get all the units in the Hasted Lane
            //and Haste them
            Unit[] units = playerLocs[laneToHaste].GetComponentsInChildren<Unit>();
            foreach (Unit playerUnit in units)
                playerUnit.Hasted(laneToHaste);
        }
        else
        {
            //Check to see if the lane is already hasted
            //if it is hasted and Haste level is same/lower, then ignore
            if (eHasted[laneToHaste] >= abilityLevel * 10)
                return;
            //Otherwise set the lane's Haste value to the new Haste value
            eHasted[laneToHaste] = (abilityLevel * 10);
            //Get all the units in the Hasted Lane
            //and Haste them
            Unit[] units = enemyLocs[laneToHaste].GetComponentsInChildren<Unit>();
            foreach (Unit playerUnit in units)
                playerUnit.Hasted(laneToHaste);
        }
        HasteVisual(isFriendly);
    }
    public void UnHaste_Lane(int laneToUnhaste, bool isFriendly)
    {
        //Check whether Player or Enemy UnHaste
        if (isFriendly)
        {
            pHasted[laneToUnhaste] = 0;

            Unit[] units = playerLocs[laneToUnhaste].GetComponentsInChildren<Unit>();
            foreach (Unit playerUnit in units)
                playerUnit.Unhasted(laneToUnhaste);
        }
        else
        {
            eHasted[laneToUnhaste] = 0;

            Unit[] units = enemyLocs[laneToUnhaste].GetComponentsInChildren<Unit>();
            foreach (Unit playerUnit in units)
                playerUnit.Unhasted(laneToUnhaste);
        }
    }
    public void Reapply_Haste(bool isFriendly, Hero toDisregard)
    {
        //Check friendly or enemy units
        if (isFriendly)
        {
            //Loop through all 3 lanes and check
            for (int x = 0; x < 3; x++)
            {
                //Get all the friendly Heroes in this lane
                Hero[] heroes = playerLocs[x].GetComponentsInChildren<Hero>();
                foreach (Hero heroUnit in heroes)
                {
                    //Make sure the hero that is being checked isn't
                    //the one that is killed
                    if (heroUnit != toDisregard)
                    {
                        //and check if they are haste units
                        if (Haste_Hero_Check(heroUnit))
                        {
                            //if they are, apply the Haste again
                            heroUnit.HeroSummon(x);
                        }
                    }
                }
            }
        }
        else
        {
            //Loop through all 3 lanes and check
            for (int x = 0; x < 3; x++)
            {
                //Get all the friendly Heroes in this lane
                Hero[] heroes = enemyLocs[x].GetComponentsInChildren<Hero>();
                foreach (Hero heroUnit in heroes)
                {
                    //Make sure the hero that is being checked isn't
                    //the one that is killed
                    if (heroUnit != toDisregard)
                    {
                        //and check if they are haste units
                        if (Haste_Hero_Check(heroUnit))
                        {
                            //if they are, apply the Haste again
                            heroUnit.HeroSummon(x);
                        }
                    }
                }
            }
        }
        HasteVisual(isFriendly);
    }
    bool Haste_Hero_Check(Hero unitToCheck)
    {
        if (unitToCheck.heroType.ToLower().Contains("haste") & unitToCheck.GetComponent<Unit>().isDead == false)
            return true;
        return false;
    }

    public void Slow_Lane(int laneToSlow, bool isFriendly, int abilityLevel)
    {
        //Check whether Player or Enemy Slow
        if (isFriendly)
        {
            //Check to see if the lane is already slowed
            //if it is Slowed and Slow level is same/lower, then ignore
            if (eSlowed[laneToSlow] >= abilityLevel * 10)
                return;
            //Otherwise set the lane's Slow value to the new Slow value
            eSlowed[laneToSlow] = (abilityLevel * 10);
            //Get all the units in the Slowed Lane
            //and Slow them
            Unit[] units = enemyLocs[laneToSlow].GetComponentsInChildren<Unit>();
            foreach (Unit playerUnit in units)
                playerUnit.Slowed(laneToSlow);
        }
        else
        {
            //Check to see if the lane is already slowed
            //if it is slowed and Slow level is same/lower, then ignore
            if (pSlowed[laneToSlow] >= abilityLevel * 10)
                return;
            //Otherwise set the lane's Haste value to the new Haste value
            pSlowed[laneToSlow] = (abilityLevel * 10);
            //Get all the units in the Slowed Lane
            //and Slow them
            Unit[] units = playerLocs[laneToSlow].GetComponentsInChildren<Unit>();
            foreach (Unit playerUnit in units)
                playerUnit.Slowed(laneToSlow);
        }
        SlowVisual(isFriendly);
    }
    public void UnSlow_Lane(int laneToUnslow, bool isFriendly)
    {
        //Check whether Player or Enemy UnSlow
        if (isFriendly)
        {
            eSlowed[laneToUnslow] = 0;

            Unit[] units = enemyLocs[laneToUnslow].GetComponentsInChildren<Unit>();
            foreach (Unit playerUnit in units)
                playerUnit.Unslowed(laneToUnslow);
        }
        else
        {
            pSlowed[laneToUnslow] = 0;

            Unit[] units = playerLocs[laneToUnslow].GetComponentsInChildren<Unit>();
            foreach (Unit playerUnit in units)
                playerUnit.Unslowed(laneToUnslow);
        }
    }
    public void Reapply_Slow(bool isFriendly, Hero toDisregard)
    {
        //Check friendly or enemy units
        if (isFriendly)
        {
            //Loop through all 3 lanes and check
            for (int x = 0; x < 3; x++)
            {
                //Get all the friendly Heroes in this lane
                Hero[] heroes = playerLocs[x].GetComponentsInChildren<Hero>();
                foreach (Hero heroUnit in heroes)
                {
                    //Make sure the hero that is being checked isn't
                    //the one that is killed
                    if (heroUnit != toDisregard)
                    {
                        //and check if they are haste units
                        if (Slow_Hero_Check(heroUnit))
                        {
                            //if they are, apply the Haste again
                            heroUnit.HeroSummon(x);
                        }
                    }
                }
            }
        }
        else
        {
            //Loop through all 3 lanes and check
            for (int x = 0; x < 3; x++)
            {
                //Get all the friendly Heroes in this lane
                Hero[] heroes = enemyLocs[x].GetComponentsInChildren<Hero>();
                foreach (Hero heroUnit in heroes)
                {
                    //Make sure the hero that is being checked isn't
                    //the one that is killed
                    if (heroUnit != toDisregard)
                    {
                        //and check if they are haste units
                        if (Slow_Hero_Check(heroUnit))
                        {
                            //if they are, apply the Haste again
                            heroUnit.HeroSummon(x);
                        }
                    }
                }
            }
        }
        SlowVisual(isFriendly);
    }
    bool Slow_Hero_Check(Hero unitToCheck)
    {
        if (unitToCheck.heroType.ToLower().Contains("snail"))
            return true;
        return false;
    }

    public void Block_Lane(int laneToBeBlocked, bool isFriendly, int abilityLevel)
    {
        if (isFriendly)
            eBlocked[laneToBeBlocked] = (2 * abilityLevel) + 4;
        else
            pBlocked[laneToBeBlocked] = (2 * abilityLevel) + 4;

        BlockVisual(laneToBeBlocked, isFriendly, true);
    }

    public void MinosBlock(int laneToBlock, bool isFriendly)
    {
        if (isFriendly)
        {
            pMinosSummoned[laneToBlock] = 1;
        }
        else if (!isFriendly)
        {
            eMinosSummoned[laneToBlock] = 1;
        }

        MinosBlockingVisuals(laneToBlock, isFriendly, true);
    }
    public void MinosUnblock(int laneToUnblock, bool isFriendly)
    {
        if (isFriendly)
        {
            pMinosSummoned[laneToUnblock] = 0;
        }
        else if (!isFriendly)
        {
            eMinosSummoned[laneToUnblock] = 0;
        }

        MinosBlockingVisuals(laneToUnblock, isFriendly, false);
    }

    public void ShieldUnitAhead(int laneInt, bool isFriendly, int shieldAmount)
    {
        //do shield for friendlys
        if (isFriendly)
        {
            //check how many units there are in the lane
            if (playerLocs[laneInt].transform.childCount >= 2)
            {
                bool friendlyShielded = false;
                Unit toBeShielded;

                for (int x = playerLocs[laneInt].transform.childCount - 2; x >= 0; x--)
                {
                    toBeShielded = playerLocs[laneInt].transform.GetChild(x).GetComponent<Unit>();
                    if (toBeShielded.isDead == false && toBeShielded.shielded == false)
                    {
                        friendlyShielded = true;
                        toBeShielded.Shielded(shieldAmount);
                        break;
                    }
                }
                if (friendlyShielded == false)
                {
                    // which means there is another unit ahead that has a shield or dead
                    toBeShielded = playerLocs[laneInt].transform.GetChild(playerLocs[laneInt].transform.childCount - 1).GetComponent<Unit>();
                    toBeShielded.Shielded(1);
                }
            }
            // if there is no units ahead
            else if (playerLocs[laneInt].transform.childCount <= 1)
            {
                // For shielding self
                Unit toBeShielded = playerLocs[laneInt].transform.GetChild(0).GetComponent<Unit>();
                toBeShielded.Shielded(1);
            }
        }
        else if (!isFriendly)
        {
            if (enemyLocs[laneInt].transform.childCount >= 2)
            {
                bool unfriendlyShielded = false;
                Unit toBeShielded;

                for (int x = enemyLocs[laneInt].transform.childCount - 2; x >= 0; x--)
                {
                    toBeShielded = enemyLocs[laneInt].transform.GetChild(x).GetComponent<Unit>();
                    if (toBeShielded.isDead == false && toBeShielded.shielded == false)
                    {
                        unfriendlyShielded = true;
                        toBeShielded.Shielded(shieldAmount);
                        break;
                    }
                }

                if (unfriendlyShielded == false)
                {
                    // which means there is another unit ahead that has a shield or dead
                    toBeShielded = enemyLocs[laneInt].transform.GetChild(enemyLocs[laneInt].transform.childCount - 1).GetComponent<Unit>();

                    toBeShielded.Shielded(1);
                }
            }
            // if there is no units ahead
            else if (enemyLocs[laneInt].transform.childCount <= 1)
            {
                // For shielding self
                Unit toBeShielded = enemyLocs[laneInt].transform.GetChild(0).GetComponent<Unit>();

                toBeShielded.Shielded(1);
            }
        }
    }

    void BlockTimer()
    {
        for (int x = 0; x < 3; x++)
        {
            if (pBlocked[x] >= 0)
            {
                pBlocked[x] -= Time.deltaTime;

                if (pBlocked[x] <= 0)
                    BlockVisual(x, false, false);
            }

            if (eBlocked[x] >= 0)
            {
                eBlocked[x] -= Time.deltaTime;

                if (eBlocked[x] <= 0)
                    BlockVisual(x, true, false);
            }
        }
    }


    void CreateGenieEffect()
    {
        for (int x = 0; x < timesToCreate; x++)
        {
            GameObject fx = Instantiate(genieBurstEffect, genieFXHolder.position, Quaternion.Euler(90f, 0f, 0f), genieFXHolder);
            genieBurstEffectList.Add(fx.GetComponent<FXPlay>());
        }
    }
    public void PlayGenieFX(Transform genie)
    {
        genieBurstToBePlayed += 1;
        if (genieBurstToBePlayed > 9)
            genieBurstToBePlayed = 0;
        genieBurstEffectList[genieBurstToBePlayed].transform.position = genie.position;
        genieBurstEffectList[genieBurstToBePlayed].PlayEffect();
    }


    void CreateTeleportedEffect()
    {
        for (int x = 0; x < timesToCreate; x++)
        {
            GameObject fxBurstFrom = Instantiate(teleportFromFX, teleporterFXHolder.position, Quaternion.Euler(90f, 0f, 0f), teleporterFXHolder);
            GameObject fxBurstTo = Instantiate(teleportToFX, teleporterFXHolder.position, Quaternion.Euler(90f, 0f, 0f), teleporterFXHolder);
            teleportedToBurstFX.Add(fxBurstTo.GetComponent<FXPlay>());
            teleportedBurstFromFX.Add(fxBurstFrom.GetComponent<FXPlay>());
        }
    }
    public void PlayBlinkFromEffect(Transform teleporter)
    {
        teleportToBePlayed += 1;
        if (teleportToBePlayed > 9)
            teleportToBePlayed = 0;

        teleportedToBurstFX[teleportToBePlayed].transform.position = teleporter.position;
        teleportedToBurstFX[teleportToBePlayed].PlayEffect();
    }
    public void PlayBlinkToEffect(Transform teleporter)
    {
        teleportedBurstFromFX[teleportToBePlayed].transform.position = teleporter.position;
        teleportedBurstFromFX[teleportToBePlayed].PlayEffect();
    }



    // ==== FX ==== \\
    public void BlockVisual(int laneBlocked, bool isFriendly, bool startEnd)
    {
        //if blocker belongs to the player
        if (isFriendly)
        {
            //display the block effect on enemy side
            if (startEnd)
                enemyBlocked[laneBlocked].PlayEffect();
            else
                enemyBlocked[laneBlocked].StopEffect();
        }
        else
        {
            if (startEnd)
                playerBlocked[laneBlocked].PlayEffect();
            else
                playerBlocked[laneBlocked].StopEffect();
        }
    }
    public void MinosBlockingVisuals(int laneBlock, bool isFriendly, bool startEnd)
    {
        if (isFriendly)
        {
            if (startEnd)
            {
                playerMinosAnimation[laneBlock].SetBool("MinosOnLane", true);
                playerMinosBlock[laneBlock].PlayEffect();
            }
            else
            {
                playerMinosAnimation[laneBlock].SetBool("MinosOnLane", false);
                playerMinosBlock[laneBlock].StopEffect();
            }
        }
        else if (!isFriendly)
        {
            if (startEnd)
            {
                enemyMinosAnimation[laneBlock].SetBool("MinosOnLane", true);
                enemyMinosBlock[laneBlock].PlayEffect();
            }
            else
            {
                enemyMinosAnimation[laneBlock].SetBool("MinosOnLane", false);
                enemyMinosBlock[laneBlock].StopEffect();
            }
        }
    }


    // FX that displays when a unit is attempted to be summoned on a blocked lane
    public bool LaneIsBlocked(int laneCheck, bool isFriendly)
    {
        if (isFriendly)
        {
            if (pBlocked[laneCheck] > 0.01f)
            {
                if (InfoText.activeInHierarchy)
                    InfoText.GetComponent<Animator>().Play(0);
                else
                    InfoText.SetActive(true);
                Block_Bolt(laneCheck, isFriendly);
                return true;
            }
        }
        else
        {
            if (eBlocked[laneCheck] > 0.01f)
            {
                //if (InfoText.activeInHierarchy)
                //    InfoText.GetComponent<Animator>().Play(0);
                //else
                //InfoText.SetActive(true);
                //Block_Bolt(laneCheck, isFriendly);
                return true;
            }
        }
        return false;
    }
    public bool LaneIsMinosBlocked(int laneInt, bool isFriendly)
    {
        if (isFriendly)
        {
            if (pMinosSummoned[laneInt] == 1)
            {
                if (InfoText.activeInHierarchy)
                    InfoText.GetComponent<Animator>().Play(0);
                else
                    InfoText.SetActive(true);

                //Block_Bolt(laneInt, isFriendly);
                return true;
            }
        }
        else if (!isFriendly)
        {
            if (eMinosSummoned[laneInt] == 1)
            {
                //if (InfoText.activeInHierarchy)
                //    InfoText.GetComponent<Animator>().Play(0);
                //else
                //InfoText.SetActive(true);

                //Block_Bolt(laneInt, isFriendly);
                return true;
            }
        }
        return false;
    }

    //Block visual refers to the rotating rune
    void Block_Bolt(int laneBolted, bool isFriendly)
    {
        if (isFriendly)
            playerBlockedObject[laneBolted].PlayEffect();
        else
            enemyBlockedObject[laneBolted].PlayEffect();
    }


    public void SlowVisual(bool isFriendly)
    {
        //if slower is player's unit
        if (isFriendly)
        {
            for (int x = 0; x < 3; x++)
            {
                //display slow on enemy's side
                if (eSlowed[x] > 1)
                    enemySlow[x].PlayEffect();
                else
                    enemySlow[x].StopEffect();
            }
        }
        else
        {
            for (int x = 0; x < 3; x++)
            {
                if (pSlowed[x] > 1)
                    playerSlow[x].PlayEffect();
                else
                    playerSlow[x].StopEffect();
            }
        }
    }

    public void HasteVisual(bool isFriendly)
    {
        //if the haste belongs to player
        if (isFriendly)
        {
            for (int x = 0; x < 3; x++)
            {
                //display the player's haste effect
                if (pHasted[x] > 1)
                    playerHaste[x].PlayEffect();
                else
                    playerHaste[x].StopEffect();
            }
        }
        else
        {
            for (int x = 0; x < 3; x++)
            {
                if (eHasted[x] > 1)
                    enemyHaste[x].PlayEffect();
                else
                    enemyHaste[x].StopEffect();
            }
        }
    }

    public void HealVisual(bool isFriendly, int laneNo)
    {
        if (isFriendly)
            playerHeal[laneNo].PlayEffect();
        else
            enemyHeal[laneNo].PlayEffect();
    }


    void ResetStoredInfo()
    {
        storedUnitIndex = null;
        storedSupportIndex = null;
        storedHeroID = null;
    }

    void ResetSoulCharge()
    {
        soulEnhanced = false;
        soulChargeValue = 0;
    }

    public void BattleOutcome(bool battleWon)
    {
        hasWon = battleWon;
        Clan.battleOutcome = battleWon;
        //Pull enemy values from cloud
        GameSparksManager.Instance.GetPlayerData(EnemyLoader.selectedEnemyID);

        //Only needs to be called if the player wins
        if (battleWon)
        {
            if (EventHolderManager.Instance.currentActiveEventOBJ.eventCondition.ToLower().Contains("battles"))
                EventHolderManager.Instance.PendingContributionPointAdder(true, "battles", Mathf.FloorToInt(stageTime.timer));

            GameSparksManager.Instance.GetPlayerExpendables(EnemyLoader.selectedEnemyID, this);
        }
        else
            GameSparksManager.Instance.UpdateRaidedEnemyInfo(EnemyLoader.selectedEnemyID, 0, hasWon);
    }

    public void UpdateRaidedInfo()
    {
        GameSparksManager.Instance.UpdateRaidedEnemyInfo(EnemyLoader.selectedEnemyID, Mathf.FloorToInt(Enemy.gold / 2), hasWon);
    }


    #region STOREROOM

    // --- For Defender Testing --- \\

    public static string storedUnitIndexDefender;
    public static string storedSupportIndexDefender;

    public static UnitButtonDefender toBeDisabledDefender;
    public static GameObject previousUnitButtonDefender;
    public static GameObject previousSupButtonDefender;

    // ============================= \\

    //	public void SpawnLaneHint(int unitType, int laneIndex)
    //	{
    //		GameObject hint = Instantiate (hintIcons [unitType], new Vector3 (laneHints [laneIndex].transform.position.x, laneHints [laneIndex].transform.position.y, laneHints [laneIndex].transform.position.z), Quaternion.identity);
    //		hint.transform.SetParent (laneHints [laneIndex].transform);
    //		hint.transform.SetAsFirstSibling ();
    //		hint.transform.localScale = Vector3.one;
    //	}


    #endregion

}
