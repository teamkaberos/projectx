﻿//using System.Collections;
//using UnityEngine;
//using UnityEngine.SceneManagement;

//public class DataSender : MonoBehaviour
//{
//	static DataSender _instance;
//	public static DataSender Instance
//	{
//		get
//		{
//			return _instance;
//		}
//	}

//	void Awake()
//	{
//		_instance = this;
//	}
		
//	// BUILDING STORAGE
//	string forSlotStorage;
//	string forServerTime;
//	string forSlotToSaveObj;
//	string forObjUpgraded;

//	public void Send_Tutorial_State()
//	{
//		StartCoroutine("_Send_Tutorial_State");
//	}
//	IEnumerator _Send_Tutorial_State()
//	{
//		WWWForm form = new WWWForm();

//		form.AddField("playeruuid", Player.playerUniqueID);
//		form.AddField("playertutorialstate", Player.tutorial);
//		form.AddField("playergold", Player.gold);
//		form.AddField("mine4lv", Player.mines[4]);
//		form.AddField("treasurylv", Player.treasuryLvl);
//		form.AddField("bunkerlv", Player.bunkerLv);
//		form.AddField("rangelv", Player.rangeLv);
//		form.AddField("guildlv", Player.guildLv);
//		form.AddField("recruitmenthalllv", Player.recruitmentHall_Lvl);

//		WWW www = new WWW(URLList.update_Tutorial_State_URL, form);

//		yield return www;
//	}

//	public void Send_Mail_To_Player(string userName, string subject, string mailContent)
//	{
//		StartCoroutine(_Send_Mail_To_Player(userName, subject, mailContent));
//	}
//	IEnumerator _Send_Mail_To_Player(string userName, string subject, string mailContent)
//	{
//		WWWForm form = new WWWForm();

//		form.AddField("playeruuid", Player.playerUniqueID);
//		form.AddField("mailsubject", subject);
//		form.AddField("mailcontent", mailContent);

//		WWW www = new WWW(URLList.send_Mail_To_Players, form);

//		yield return www;

//		Debug.Log(www.text);
//	}

//	public void SendTroop_And_AssignedSlot()
//	{
//		StartCoroutine("_SendTroopAssignmentInfo");

//		//		WWWForm form = new WWWForm ();
//		//		form.AddField("troopNumberPost", troopID);
//		//		form.AddField("troopSlotPost", slotNumber);
//		//
//		//		form.AddField("troopLevelPost", troopLevel);
//		//		form.AddField("troopSlotLevelPost", slotNumberLevel);
//		//
//		//		form.AddField("playerID",Player.playerUniqueID);
//		//
//		//		WWW www = new WWW (troopAssignmentSlotURL, form);
//	}
//	IEnumerator _SendTroopAssignmentInfo()
//	{
//		WWWForm form = new WWWForm();
//		form.AddField("playerid", Player.playerUniqueID);
//		form.AddField("shuffleTimer", Player.shufflingTime.ToString());

//		form.AddField("slot0Post", Player.assignedTroops[0]);
//		form.AddField("slot0LevelPost", TroopAssign_Info.Instance.DropSlots[0].GetComponent<AssignSlot>().assignedTroopLevel);

//		form.AddField("slot1Post", Player.assignedTroops[1]);
//		form.AddField("slot1LevelPost", TroopAssign_Info.Instance.DropSlots[1].GetComponent<AssignSlot>().assignedTroopLevel);

//		form.AddField("slot2Post", Player.assignedTroops[2]);
//		form.AddField("slot2LevelPost", TroopAssign_Info.Instance.DropSlots[2].GetComponent<AssignSlot>().assignedTroopLevel);

//		form.AddField("slot3Post", Player.assignedTroops[3]);
//		form.AddField("slot3LevelPost", TroopAssign_Info.Instance.DropSlots[3].GetComponent<AssignSlot>().assignedTroopLevel);

//		form.AddField("slot4Post", Player.assignedTroops[4]);
//		form.AddField("slot4LevelPost", TroopAssign_Info.Instance.DropSlots[4].GetComponent<AssignSlot>().assignedTroopLevel);

//		form.AddField("slot5Post", Player.assignedTroops[5]);
//		form.AddField("slot5LevelPost", TroopAssign_Info.Instance.DropSlots[5].GetComponent<AssignSlot>().assignedTroopLevel);

//		form.AddField("slot6Post", Player.assignedTroops[6]);
//		form.AddField("slot6LevelPost", TroopAssign_Info.Instance.DropSlots[6].GetComponent<AssignSlot>().assignedTroopLevel);

//		form.AddField("slot7Post", Player.assignedTroops[7]);
//		form.AddField("slot7LevelPost", TroopAssign_Info.Instance.DropSlots[7].GetComponent<AssignSlot>().assignedTroopLevel);

//		form.AddField("slot8Post", Player.assignedTroops[8]);
//		form.AddField("slot8LevelPost", TroopAssign_Info.Instance.DropSlots[8].GetComponent<AssignSlot>().assignedTroopLevel);

//		WWW www = new WWW(URLList.troopAssignmentSlotURL, form);

//		yield return www;

//		if (DragAndDrop.beingSelected != null)
//		{
//			DragAndDrop.beingSelected.GetComponent<DragAndDrop>().Selected(false);
//		}
//	}

//	public void Send_Stone_Purchase_Slot()
//	{
//		StartCoroutine("_Send_Stone_Purchase_Slot");
//	}
//	IEnumerator _Send_Stone_Purchase_Slot()
//	{
//		WWWForm form = new WWWForm();
//		form.AddField("playerid", Player.playerUniqueID);
//		form.AddField("flameslotno", Player.earthStoneBuildSlot);
//		form.AddField("windslotno", Player.windStoneBuildSlots);
//		form.AddField("thunderslotno", Player.flameStoneBuildSlot);

//		WWW www = new WWW(URLList.newPurchaseStoneBuildSlotURL, form);

//		yield return www;

//		Debug.Log(www.text);
//	}

//	public void LogTime(string serverTime, bool isLogin)
//	{
//		string toLog;

//		WWWForm form = new WWWForm();

//		form.AddField("playerid", Player.playerUniqueID);
//		form.AddField("servertime", serverTime);

//		if (isLogin)
//			toLog = "lastlogin";
//		else
//			toLog = "lastlogout";

//		form.AddField("lastlog", toLog);

//		WWW www = new WWW(URLList.Login_Logout_TimeStampURL, form);
//	}
		
//	public void ResendBuildingTime(string newTimer)
//	{
//		WWWForm form = new WWWForm();
//		form.AddField("PlayerID", Player.playerUniqueID);
//		form.AddField("buildingTimeStampPost", newTimer);
//		form.AddField("buildingUpgrading", BuildTimer.queuedBuilding[0]);

//		WWW www = new WWW(URLList.timeStampURL, form);

//		Debug.Log("Building Time Restamped");
//	}

//	public void SendStoneBuildingTime(int slot)
//	{
//		WWWForm form = new WWWForm();
//		form.AddField("PlayerID", Player.playerUniqueID);
//		if (slot == 0)
//		{
//			form.AddField("stoneUpgrading", BuildTimer.queuedStone0);

//			// This function is the one where it executes the data sending
//			WWW www = new WWW(URLList.stoneTimeStampURL, form);
//		}
//		else if (slot == 1)
//		{
//			form.AddField("stoneUpgrading", BuildTimer.queuedStone1);

//			// This function is the one where it executes the data sending
//			WWW www = new WWW(URLList.stone1TimeStampURL, form);
//		}
//		else if (slot == 2)
//		{
//			form.AddField("stoneUpgrading", BuildTimer.queuedStone2);

//			// This function is the one where it executes the data sending
//			WWW www = new WWW(URLList.stone2TimeStampURL, form);
//		}
//		Debug.Log("Stone Time Stamped");
//	}

//	public void SendBuildingTime()
//	{
//		StartCoroutine("_SendBuildingTime");
//	}
//	IEnumerator _SendBuildingTime()
//	{
//        if (Player.tutorial > 7)
//        {
//            WWWForm form = new WWWForm();

//            form.AddField("playerid", Player.playerUniqueID);
//            if (BuildTimer.queuedBuilding.Count >= 1)
//            {
//                form.AddField("building0Upgrade", BuildTimer.Instance.UpgradingBuildingNo(BuildTimer.queuedBuilding[0]));
//                form.AddField("building0TimeLeft", BuildTimer.buildingTimer[0].ToString());
//            }
//            else if (BuildTimer.queuedBuilding.Count < 1)
//            {
//                form.AddField("building0Upgrade", "null");
//                form.AddField("building0TimeLeft", "null");
//            }

//            if (BuildTimer.isBuildingStone[0] == true)
//            {
//                string stoneUpgrading = BuildTimer.queuedStone0;
//                form.AddField("stone0upgrade", stoneUpgrading);
//                form.AddField("stone0timeleft", BuildTimer.earthstone0_Build_Time.ToString());
//            }
//            else if (BuildTimer.isBuildingStone[0] == false)
//            {
//                form.AddField("stone0upgrade", "null");
//                form.AddField("stone0timeleft", "null");
//            }

//            if (BuildTimer.isBuildingStone[1] == true)
//            {
//                string stoneUpgrading = BuildTimer.queuedStone1;
//                form.AddField("stone1upgrade", stoneUpgrading);
//                form.AddField("stone1timeleft", BuildTimer.windStone0_Build_Time.ToString());
//            }
//            else if (BuildTimer.isBuildingStone[1] == false)
//            {
//                form.AddField("stone1upgrade", "null");
//                form.AddField("stone1timeleft", "null");
//            }

//            if (BuildTimer.isBuildingStone[2] == true)
//            {
//                string stoneUpgrading = BuildTimer.queuedStone2;
//                form.AddField("stone2upgrade", stoneUpgrading);
//                form.AddField("stone2timeleft", BuildTimer.flameStone0_Build_Time.ToString());
//            }
//            else if (BuildTimer.isBuildingStone[2] == false)
//            {
//                form.AddField("stone2upgrade", "null");
//                form.AddField("stone2timeleft", "null");
//            }
				
//            if (BuildTimer.isBuildingStone[3] == true)
//            {
//                string stoneUpgrading = BuildTimer.queuedStone3;
//                form.AddField("stone3upgrade", stoneUpgrading);
//                form.AddField("stone3timeleft", BuildTimer.earthstone1_Build_Time.ToString());
//            }
//            else if (BuildTimer.isBuildingStone[3] == false)
//            {
//                form.AddField("stone3upgrade", "null");
//                form.AddField("stone3timeleft", "null");
//            }

//            if (BuildTimer.isBuildingStone[4] == true)
//            {
//                string stoneUpgrading = BuildTimer.queuedStone4;
//                form.AddField("stone4upgrade", stoneUpgrading);
//                form.AddField("stone4timeleft", BuildTimer.windStone1_Build_Time.ToString());
//            }
//            else if (BuildTimer.isBuildingStone[4] == false)
//            {
//                form.AddField("stone4upgrade", "null");
//                form.AddField("stone4timeleft", "null");
//            }

//            if (BuildTimer.isBuildingStone[5] == true)
//            {
//                string stoneUpgrading = BuildTimer.queuedStone2;
//                form.AddField("stone5upgrade", stoneUpgrading);
//                form.AddField("stone5timeleft", BuildTimer.flameStone1_Build_Time.ToString());
//            }
//            else if (BuildTimer.isBuildingStone[5] == false)
//            {
//                form.AddField("stone5upgrade", "null");
//                form.AddField("stone5timeleft", "null");
//            }

//			WWW www = new WWW(URLList.allUpgradingURL, form);

//            yield return www;
//        }
//	}

//	public void ClearBuildingTime()
//	{
//        if (Player.tutorial >= Player.tutorialCap)
//			StartCoroutine("_ClearBuildingTime");
//	}
//	IEnumerator _ClearBuildingTime()
//	{
//		WWWForm form = new WWWForm();
//		form.AddField("playerid", Player.playerUniqueID);
//		form.AddField("slottostamp", "building0upgrading");
//		form.AddField("objtostamp", "building0timeleft");

//		WWW www = new WWW(URLList.clearTimeStampURL, form);

//		yield return www;
//	}

//	public void ClearStoneBuildingTime(int whichSlot)
//	{
//		StartCoroutine(_ClearStoneBuildingTime(whichSlot));
//	}
//	IEnumerator _ClearStoneBuildingTime(int whichSlot)
//	{
//		string slotToClear = string.Format("stone{0}timeleft", whichSlot);
//		string timeslotToClear = string.Format("stone{0}upgrading", whichSlot);

//		WWWForm form = new WWWForm();
//		form.AddField("playerid", Player.playerUniqueID);
//		form.AddField("slottostamp", slotToClear);
//		form.AddField("objtostamp", timeslotToClear);

//		WWW www = new WWW(URLList.clearTimeStampURL, form);

//		yield return www;
//	}

//	public void UpdateBuildingLevels()
//	{
//		if (Player.tutorial >= 8)
//		{
//			WWWForm form = new WWWForm();
//			form.AddField("playerid", Player.playerUniqueID);
//			form.AddField("shuffletimer", Player.shufflingTime.ToString());
//			form.AddField("castleLv", Player.castleLevel);
//			form.AddField("townhallLv", Player.townHallLevel);
//			form.AddField("treasuryLv", Player.treasuryLvl);
//			form.AddField("rhallLv", Player.recruitmentHall_Lvl);
//			form.AddField("bunkerLv", Player.bunkerLv);
//			form.AddField("rangeLv", Player.rangeLv);
//			form.AddField("guildLv", Player.guildLv);
//			form.AddField("goldmine0Lv", Player.mines[0]);
//			form.AddField("goldmine1Lv", Player.mines[1]);
//			form.AddField("goldmine2Lv", Player.mines[2]);
//			form.AddField("goldmine3Lv", Player.mines[3]);
//			form.AddField("goldmine4Lv", Player.mines[4]);
//			form.AddField("forgeLv", Player.forgeLevel);

//			// This function is the one where it executes the data sending
//			WWW www = new WWW(URLList.strcture_Update_Send_URL, form);
//		}
//	}
//	public void UpdatePlayerCurrencyLevels()
//	{
//		if (Player.tutorial >= 8)
//		{
//			WWWForm form = new WWWForm();
//			form.AddField("playerid", Player.playerUniqueID);
//			form.AddField("reputation", Player.reputation);
//			form.AddField("gold", Player.gold);
//			form.AddField("diamond", Player.diamond);
//			form.AddField("token", Player.tokens);
//			form.AddField("circlestone", Player.circleStone);
//			form.AddField("trianglestone", Player.triangleStone);
//			form.AddField("squarestone", Player.squareStone);
//			form.AddField("circlebattle", Player.circleBattles);
//			form.AddField("trianglebattle", Player.triangleBattles);
//			form.AddField("squarebattle", Player.squareBattles);
//			form.AddField("mybattles", Player.my_Matches);
//			form.AddField("myvictories", Player.my_Victories);
//			form.AddField("timer3", Player.timer3);
//			form.AddField("timer15", Player.timer15);
//			form.AddField("timer30", Player.timer30);
//			form.AddField("timer60", Player.timer60);
//			form.AddField("jackpack", Player.jackPack);
//			form.AddField("queenpack", Player.queenPack);
//			form.AddField("kingpack", Player.kingPack);
//			form.AddField("bronzechest", Player.bronzeChest);
//			form.AddField("silverchest", Player.silverChest);
//			form.AddField("goldchest", Player.goldChest);
//			form.AddField("mypotraitid", Player.potraitID);
//			form.AddField("excalipoints", Player.excalipoints);
//            form.AddField("energy", Player.playerCurrentEnergy);
//            form.AddField("energyrechargetime", Player.currentEnergyRechargeTime.ToString());
//            form.AddField("energypacks", Player.energyPacks.ToString());

//			WWW www = new WWW(URLList.expandables_Update_Send_URL, form);
//		}
//	}
//    /*
//	public void Send_Hero_Attained_Info()
//	{
//		StartCoroutine("_Send_Hero_Attained_Info");
//	}
//	IEnumerator _Send_Hero_Attained_Info()
//	{
//		WWWForm form = new WWWForm();

//		form.AddField("playerid", Player.playerUniqueID);

//		form.AddField("tanklvl", UnitManager.Instance.heroInfomation[0].heroLvl);
//		form.AddField("tankamount", UnitManager.Instance.heroInfomation[0].cardAmount);

//		form.AddField("hastelvl", UnitManager.Instance.heroInfomation[1].heroLvl);
//		form.AddField("hasteamount", UnitManager.Instance.heroInfomation[1].cardAmount);

//		form.AddField("haste3lvl", UnitManager.Instance.heroInfomation[2].heroLvl);
//		form.AddField("haste3amount", UnitManager.Instance.heroInfomation[2].cardAmount);

//		form.AddField("snaillvl", UnitManager.Instance.heroInfomation[3].heroLvl);
//		form.AddField("snailamount", UnitManager.Instance.heroInfomation[3].cardAmount);

//		form.AddField("snail3lvl", UnitManager.Instance.heroInfomation[4].heroLvl);
//		form.AddField("snail3amount", UnitManager.Instance.heroInfomation[4].cardAmount);

//		form.AddField("healerlvl", UnitManager.Instance.heroInfomation[5].heroLvl);
//		form.AddField("healeramount", UnitManager.Instance.heroInfomation[5].cardAmount);

//		form.AddField("blockerlvl", UnitManager.Instance.heroInfomation[6].heroLvl);
//		form.AddField("blockeramount", UnitManager.Instance.heroInfomation[6].cardAmount);

//		form.AddField("refresherlvl", UnitManager.Instance.heroInfomation[7].heroLvl);
//		form.AddField("refresheramount", UnitManager.Instance.heroInfomation[7].cardAmount);

//		form.AddField("teleporterlvl", UnitManager.Instance.heroInfomation[8].heroLvl);
//		form.AddField("teleporteramount", UnitManager.Instance.heroInfomation[8].cardAmount);

//		form.AddField("lanechangerlvl", UnitManager.Instance.heroInfomation[9].heroLvl);
//		form.AddField("lanechangeramount", UnitManager.Instance.heroInfomation[9].cardAmount);

//		form.AddField("cooldownerlvl", UnitManager.Instance.heroInfomation[10].heroLvl);
//		form.AddField("cooldowneramount", UnitManager.Instance.heroInfomation[10].cardAmount);

//		form.AddField("soulchargerlvl", UnitManager.Instance.heroInfomation[11].heroLvl);
//		form.AddField("soulchargeramount", UnitManager.Instance.heroInfomation[11].cardAmount);

//		form.AddField("ninjalvl", UnitManager.Instance.heroInfomation[12].heroLvl);
//		form.AddField("ninjaamount", UnitManager.Instance.heroInfomation[12].cardAmount);

//		form.AddField("speederlvl", UnitManager.Instance.heroInfomation[13].heroLvl);
//		form.AddField("speederamount", UnitManager.Instance.heroInfomation[13].cardAmount);

//		form.AddField("morpherlvl", UnitManager.Instance.heroInfomation[14].heroLvl);
//		form.AddField("morpheramount", UnitManager.Instance.heroInfomation[14].cardAmount);

//		form.AddField("freezerlvl", UnitManager.Instance.heroInfomation[15].heroLvl);
//		form.AddField("freezeramount", UnitManager.Instance.heroInfomation[15].cardAmount);

//		form.AddField("invisibilitylvl", UnitManager.Instance.heroInfomation[16].heroLvl);
//		form.AddField("invisibilityamount", UnitManager.Instance.heroInfomation[16].cardAmount);

//		WWW www = new WWW(URLList.store_Hero_ValuesURL, form);

//		yield return www;
//	}
//*/
//    public void GetNewSeiges()
//	{
//		StartCoroutine(_GetNewSeiges());
//	}
//	IEnumerator _GetNewSeiges()
//	{
//		WWWForm form = new WWWForm();

//		form.AddField("playerid", Player.playerUniqueID);

//		yield return null;
//	}
		
//	//public void SendUpdateSeiges()
//	//{
//	//	WWWForm form = new WWWForm();

//	//	int slotsToSave = Player.playerAttackedListID.Count;

//	//	if (slotsToSave >= 5)
//	//	{
//	//		form.AddField("atkid4", Player.playerAttackedListID[slotsToSave]);
//	//		form.AddField("atktime4", Player.playerAttackedTime[slotsToSave]);
//	//		form.AddField("atkgold4", Player.playerAttackedGoldLost[slotsToSave]);
//	//		slotsToSave -= 1;
//	//	}

//	//	if (slotsToSave >= 4)
//	//	{
//	//		form.AddField("atkid3", Player.playerAttackedListID[slotsToSave]);
//	//		form.AddField("atktime3", Player.playerAttackedTime[slotsToSave]);
//	//		form.AddField("atkgold3", Player.playerAttackedGoldLost[slotsToSave]);
// //           slotsToSave -= 1;
//	//	}

//	//	if (slotsToSave >= 3)
//	//	{
//	//		form.AddField("atkid2", Player.playerAttackedListID[slotsToSave]);
//	//		form.AddField("atktime2", Player.playerAttackedTime[slotsToSave]);
//	//		form.AddField("atkgold2", Player.playerAttackedGoldLost[slotsToSave]);
// //           slotsToSave -= 1;
//	//	}

//	//	if (slotsToSave >= 2)
//	//	{
//	//		form.AddField("atkid1", Player.playerAttackedListID[slotsToSave]);
//	//		form.AddField("atktime1", Player.playerAttackedTime[slotsToSave]);
//	//		form.AddField("atkgold1", Player.playerAttackedGoldLost[slotsToSave]);
// //           slotsToSave -= 1;
//	//	}

//	//	if (slotsToSave >= 1)
//	//	{
//	//		form.AddField("atkid0", Player.playerAttackedListID[slotsToSave]);
//	//		form.AddField("atktime0", Player.playerAttackedTime[slotsToSave]);
//	//		form.AddField("atkgold0", Player.playerAttackedGoldLost[slotsToSave]);
// //           slotsToSave -= 1;
//	//		WWW www = new WWW(URLList.getNewSeigesURL, form);
//	//	}
//	//}
		
	
//    public void UpdateMission()
//    {
//        WWWForm form = new WWWForm();
//        form.AddField("uuid",Player.playerUniqueID);

//        form.AddField("mission1", MissionHolder.Instance.missionsAssigned[0]);
//        form.AddField("mission1progress", MissionHolder.Instance.missionButtons[0].scriptableMission.missionProgress);
//        form.AddField("mission1state", MissionHolder.Instance.missionButtons[0].scriptableMission.missionState.ToString());
//        form.AddField("mission1refreshduration", MissionHolder.Instance.missionButtons[0].currentCooldownValue.ToString());
        
//        form.AddField("mission2", MissionHolder.Instance.missionsAssigned[1]);
//        form.AddField("mission2progress", MissionHolder.Instance.missionButtons[1].scriptableMission.missionProgress);
//        form.AddField("mission2state", MissionHolder.Instance.missionButtons[1].scriptableMission.missionState.ToString());
//        form.AddField("mission2refreshduration", MissionHolder.Instance.missionButtons[1].currentCooldownValue.ToString());
        
//        form.AddField("mission3", MissionHolder.Instance.missionsAssigned[2]);
//        form.AddField("mission3progress", MissionHolder.Instance.missionButtons[2].scriptableMission.missionProgress);
//        form.AddField("mission3state", MissionHolder.Instance.missionButtons[2].scriptableMission.missionState.ToString());
//        form.AddField("mission3refreshduration", MissionHolder.Instance.missionButtons[2].currentCooldownValue.ToString());

//		WWW www = new WWW(URLList.SendMissionsURL, form);
//    }

//    public void LogOut()
//    {
//        WWWForm form = new WWWForm();
//        form.AddField ("playerid", Player.playerUniqueID);

//		WWW www = new WWW(URLList.logOutURL, form);
//    }

//	#region William's Badlands

//	public void SendBuildingTime(string slotToSaveTime, string serverTime, string slotToSaveObj, string objUpgraded)
//	{
//	       if (Player.tutorial >= 8)
//	       {
//	           forSlotStorage = slotToSaveTime;
//	           forServerTime = serverTime;
//	           forSlotToSaveObj = slotToSaveObj;
//	           forObjUpgraded = objUpgraded;
	
//	           StartCoroutine("_Send_Building_Time");         
//	       }
//	}

//		IEnumerator _Send_Building_Time()
//		{
//			WWWForm form = new WWWForm();
//			form.AddField("playerid", Player.playerUniqueID);               //for identification
//			form.AddField("slottostamp", forSlotStorage);                   //which slot is being used for time store
//			form.AddField("timestamp", forServerTime);                      //server time that is being stored
//			form.AddField("objtostamp", forSlotToSaveObj);              //which slot to store the upgrade
//			form.AddField("objbeingbuilt", forObjUpgraded);             //which object is being upgraded
	
//			// This function is the one where it executes the data sending
//			WWW www = new WWW(URLList.timeStampURL, form);
	
//			yield return www;
//		}


//	 public void Send_Struct_BuildingTime()
//	 {
//		 StartCoroutine(_SendStructBuildTime());
//	 }
//	 IEnumerator _SendStructBuildTime()
//	 {
//		 WWWForm form = new WWWForm();


//		 form.AddField("playerid", Player.playerUniqueID);
//		 form.AddField("struc0", BuildTimer.Instance.UpgradingBuildingNo(BuildTimer.queuedBuilding[0]));
//		 form.AddField("struc0buildtime", BuildTimer.buildingTimer[0].ToString());

//		WWW www = new WWW(URLList.strcture_Update_Send_URL, form);

//		 yield return www;
//	 }
	 

//	#endregion
//}
