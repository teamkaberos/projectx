﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnitManager : MonoBehaviour
{
    public delegate void UnitEvolved();
    public static event UnitEvolved UnitHasEvolved;

    public delegate void NewUnitEvolvedObtained(string heroName, Sprite newHeroImage);
    public static event NewUnitEvolvedObtained NewUnitEvolvedWasObtained;


    #region singleton
    static UnitManager _instance;
	public static UnitManager Instance
	{
		get
		{
			return _instance;
		}
	}
    #endregion

    public enum UnitRarity { Uncommon, Rare, Epic, Legendary };
    public enum UnitShape { Square, Circle, Triangle, Neutral };

    [System.Serializable]
	public class Unit_Info
	{
		public string heroName;
		public string heroRace;
		public int heroNo;
		public int heroIsUnlocked;
		public int heroLvl;
        public int[] heroAssignmentCost = {10,20,0,0,0};
		public int cardAmount;
        public int heroRarity;             //used to determine hero card costs
        public UnitRarity rarity;
		public int heroWeight;           //rarity
        public UnitShape unitShape;
		public Sprite heroImage;
        public Sprite newHeroImage;     // image with no grey bar
		public GameObject dragAndDropCard;
	}

	[SerializeField] public static int[] cardsRequiredToRankUp = {0,10,20,40,80,0};
	[SerializeField] public static int[] stonesRequiredToRankUp = {0,10,20,30,40,0};
    [SerializeField] public static bool sufficientGold;
	[SerializeField] public static bool sufficientCards;
	[SerializeField] public static bool sufficientStones;
    [SerializeField] public static bool sufficientPlayerLevel;


    public static int availableHeroes = 15;
	public int heroAmount = 17;
	public static int[] heroUnlocked = new int[17];
	public static int[] heroLevels = new int[17];
	public static int[] heroCardsAmount = new int[17];
	public float[] heroTimers;

	public List<Unit_Info> heroInfomation = new List <Unit_Info>();
	public GameObject[] heroAssignmentTab;
	public GameObject[] heroInfoTab;

	public static string heroToEvolveNow;
	public static int intOfHeroToEvolve;
	public GameObject heroEvoPopup;
	Toggle lastToggleSelected;
	public Button evolveHeroButton;
	public Text heroToEvolveDisplay;
	public Text heroEvoCardsRequirement;
	public Text heroEvoStonesRequirement;
    public Text heroEvoGoldRequirement;
    public Text heroEvoLevelRequirement;
    string stoneToDeduct;
    public GameObject stonesMetTick;
    public GameObject cardsMetTick;
    public GameObject goldMetTick;
    public GameObject playerLevelMetTick;

	int heroToPurchase;
	int heroToPurchase_Card_Owned;  //How many card of this hero does this player have

    int totalEvolveChance;

    [HideInInspector]
    public int[] CardCost = {100,300,800,1500,3000};
    [HideInInspector]
    public int[] cardUpgrade_GoldCost = {5000, 10000, 25000, 50000, 100000};  //how much gold is required in order to upgrade cards

    public Text requiredStonesText;
    public Text requiredCardsText;
    public Text requiredGoldText;
    public Text requiredCastleLevelText;
    public GameObject[] lockedImage;

    [Space(10)]
    [Header("Fanciful stuff")]
    public NewItemObtained newItemObtained;


    void Start()
	{
		_instance = this;
		if (Player.firstTimePlaying != 0)
		{
            if (Player.accountType == "Guest")
                LoadHeroInfo();
		}
		else 
		{
			if (Player.accountType == "Guest")
				SaveHeroInfo();
		}
        //Invoke("AssignNumbers", 1.5f);
	}

	public int TotalHeroAmount()
	{
        return availableHeroes;
	}

    // Used in the Training Grounds
    public void EvolveUnit_(string evoType)
    {
        // THIS IS USED FOR EVOLVING SQUARES / CIRCLES / TRIANGLES
        totalEvolveChance = 0;

        for (int x = 0; x < heroInfomation.Count; x++)
        {
            if (heroInfomation[x].unitShape.ToString() == evoType & heroInfomation[x].rarity <= HeroLevelCheck())
            {
                //calculate total weight of heroes
                totalEvolveChance += heroInfomation[x].heroWeight;
            }
        }

        int randUnit = Random.Range(0, totalEvolveChance);
        //and loop through all units that are of the evolved type
        for (int y = 0; y < heroInfomation.Count; y++)
        {
            if (heroInfomation[y].unitShape.ToString() == evoType || heroInfomation[y].unitShape.ToString() == "Neutral")
            {
                if (heroInfomation[y].rarity <= HeroLevelCheck())
                {
                    // If the rolled number doesnt belong to this hero
                    if (randUnit > heroInfomation[y].heroWeight)
                    {
                        randUnit -= heroInfomation[y].heroWeight;
                    }

                    // If the rolled number falls on this hero's weight
                    else if (randUnit <= heroInfomation[y].heroWeight)
                    {
                        //save got hero info here
                        if (heroUnlocked[Instance.heroInfomation[y].heroNo] != 1)
                        {
                            //get the hero unit here
                            heroUnlocked[heroInfomation[y].heroNo] = 1;
                            heroLevels[heroInfomation[y].heroNo] = 1;

                            Player.heroCardAmounts[heroInfomation[y].heroNo] += 1;
                            GainHeroCard(heroInfomation[y].heroNo);

                            UnlockHero(heroInfomation[y].heroNo);

                            NewUnitEvolvedWasObtained(heroInfomation[y].heroName, heroInfomation[y].newHeroImage);
                        }
                        else
                        {
                            //get the hero card here
                            Player.heroCardAmounts[heroInfomation[y].heroNo] += 1;

                            UIManager.Instance.ProgressNotification(heroInfomation[y].heroRace, false);
                            GainHeroCard(heroInfomation[y].heroNo);

                        }
                        Clear_Required_For_Evolutions(evoType);

                        DataManager.Instance.PrepareDataForSparksPushing();
                        GameSparksManager.Instance.SaveAllHeroInfos();
                        GameSparksManager.Instance.SaveUnitBattleCount();
                        GameSparksManager.Instance.SaveInventoryStones();
                        UnitHasEvolved();
                        break;
                    }
                }
            }
        }
    }
    UnitRarity HeroLevelCheck()
    {
        if (Player.castleLevel <= 2)
            return UnitRarity.Uncommon;
        if (Player.castleLevel == 3 || Player.castleLevel == 4)
            return UnitRarity.Rare;
        if (Player.castleLevel == 5)
            return UnitRarity.Epic;
        return UnitRarity.Legendary;
    }
    void Clear_Required_For_Evolutions(string evoType)
    {
        if (evoType == "Square")
        {
            Player.squareStone -= 1;
            Player.squareBattles = 0;       //TODO initial script is "squareBattles = 0"
        }
        else if (evoType == "Circle")
        {
            Player.circleStone -= 1;
            Player.circleBattles = 0;           //TODO initial script is "circleBattles = 0"
        }
        else if (evoType == "Triangle")
        {
            Player.triangleStone -= 1;
            Player.triangleBattles = 0;     //TODO initial script is "triangleBattles = 0"
        }
    }

	void RandomDisplay()
	{
		int rand = Random.Range(0,16);
	}

    public int HeroesUnlockedCount()
    {
        int y = 0;
        for (int x = 0; x < heroInfomation.Count; x++)
        {
            if (heroInfomation[x].heroIsUnlocked == 1)
            {
                y += 1;
            }
        }
        return y;
    }

	public void UnlockHero(int unlockedHeroID)
	{
		if (heroUnlocked[unlockedHeroID] < 1)
		{
			heroUnlocked[unlockedHeroID] = 1;
            heroLevels[unlockedHeroID] = 1;
			heroInfomation[unlockedHeroID].heroLvl = 1;
		}
        DistributeStats();
	}

	public void SaveHeroInfo()
	{
		for (int x = 0; x < heroCardsAmount.Length; x++)
		{
			PlayerPrefs.SetInt("HeroUnlocked"+x,heroUnlocked[x]);
			PlayerPrefs.SetInt("HeroLevels"+x,heroLevels[x]);
			PlayerPrefs.SetInt("HeroCardsObtained"+x,heroCardsAmount[x]);
		}
	}
	public void LoadHeroInfo()
	{
		for (int y = 0; y < heroCardsAmount.Length; y ++)
		{
			heroUnlocked[y] = PlayerPrefs.GetInt("HeroUnlocked"+y);
			heroLevels[y] = PlayerPrefs.GetInt("HeroLevels"+y);
			heroCardsAmount[y] = PlayerPrefs.GetInt("HeroCardsObtained"+y);
		}
	}

	public void GainHeroCard(int heroID)
	{
		heroCardsAmount[heroID] += 1;
	}

	public void Assign_Player_Cards_Values()
	{
		for (int x = 0; x < heroAmount; x++)
		{
			if (heroInfomation[x].heroNo == x)
			{
				heroInfomation[x].heroIsUnlocked = heroUnlocked[x];
				heroInfomation[x].heroLvl = heroLevels[x];
				heroInfomation[x].cardAmount = heroCardsAmount[x];
			}
		}
	}

	public void AssignNumbers()
    {
        for (int x = 0; x < Player.heroAmount; x++)
        {
            heroInfomation[x].heroLvl = heroLevels[x];
            heroInfomation[x].cardAmount = heroCardsAmount[x];
            heroInfomation[x].heroIsUnlocked = heroUnlocked[x];
        }

        DistributeStats();
  	}

    public void DistributeStats()
    {
		//loop through all the manually keyed in info for the heroes
        //This is for the cards in the tavern
		for (int x = 0; x < heroInfomation.Count ; x++)
		{
			//then loop through all the tabs
			for (int y = 0; y < heroInfoTab.Length; y++)
			{
				//find if there is a matching hero no
				if (heroInfomation[x].heroNo == heroInfoTab[y].GetComponent<DragAndDrop>().assignedTroopID && heroInfomation[x].heroNo < 100)
				{
                    heroInfoTab[y].GetComponent<DragAndDrop>().ToggleCharacterInfo(true);
                    //find out if the hero is already unlocked
                    if (heroUnlocked[heroInfomation[x].heroNo] == 1)
                    {
                        //Assign to the heroInfolist so it is visible
                        heroInfomation[x].heroIsUnlocked = 1;
                        heroInfomation[x].cardAmount = heroCardsAmount[x];

                        //make the hero selectable
                        heroInfoTab[y].GetComponent<DragAndDrop>().thisToggle.interactable = true;
                        heroInfoTab[y].GetComponent<DragAndDrop>().thisCardsLevel = heroLevels[heroInfomation[x].heroNo];

                        heroInfoTab[y].GetComponent<DragAndDrop>().thisHeroCardsObtained.enabled = true;
                        heroInfoTab[y].GetComponent<DragAndDrop>().Assign();

                        if (heroInfomation[x].heroLvl < 5)
                        {
                            heroInfoTab[y].GetComponent<DragAndDrop>().thisHeroCardsObtained.text = string.Format("{0}/{1}",
                            heroCardsAmount[heroInfomation[x].heroNo], cardsRequiredToRankUp[heroLevels[heroInfomation[x].heroNo]]);
                        }
                        else
                        {
                            heroInfoTab[y].GetComponent<DragAndDrop>().thisHeroCardsObtained.text = string.Format("{0}/MAX",
                            heroCardsAmount[heroInfomation[x].heroNo]);
                        }
                    }
                    else if (heroUnlocked[heroInfomation[x].heroNo] == 0)
					{
                        heroInfoTab[y].GetComponent<DragAndDrop>().ToggleCharacterInfo(false);
                        heroInfoTab[y].GetComponent<DragAndDrop>().thisToggle.interactable = false;
						heroInfoTab[y].GetComponent<DragAndDrop>().thisHeroCardsObtained.enabled = false;
					}
				}
			}
            //This is for the cards in the prep halls
			for (int y = 0; y < heroAssignmentTab.Length; y++)
			{
				//find if there is a matching hero no
				if (heroInfomation[x].heroNo == heroAssignmentTab[y].GetComponent<DragAndDrop>().assignedTroopID && heroInfomation[x].heroNo < 100)
				{
					//find out if the hero is already unlocked
					if (heroUnlocked[heroInfomation[x].heroNo] == 1)
                    {
						//make the hero selectable
						heroAssignmentTab[y].GetComponent<DragAndDrop>().thisToggle.interactable = true;
						//display the hero's level & name
//						if (heroLevels[heroInfomation[x].heroNo]  == 0)
//							heroAssignmentTab[y].GetComponent<DragAndDrop>().thisHeroName.text = "" + heroInfomation[x].heroName;
//						else 
//							heroAssignmentTab[y].GetComponent<DragAndDrop>().thisHeroName.text = heroLevels[heroInfomation[x].heroNo] + " " + heroInfomation[x].heroName;
						//display the amount of cards the player has of this hero
						heroAssignmentTab[y].GetComponent<DragAndDrop>().thisHeroCardsObtained.enabled = true;

                        heroAssignmentTab[y].GetComponent<DragAndDrop>().Assign();
//					heroAssignmentTab[y].GetComponent<DragAndDrop>().thisHeroCardsObtained.text  = string.Format("{0}/{1}",
//						heroCardsAmount[heroInfomation[x].heroNo], cardsRequiredToRankUp[heroLevels[heroInfomation[x].heroNo]]);
					}
					//keep it uninteractable if not unlocked
					else if (heroUnlocked[heroInfomation[x].heroNo] == 0)
                    {
						heroAssignmentTab[y].GetComponent<DragAndDrop>().thisToggle.interactable = false;
						heroAssignmentTab[y].GetComponent<DragAndDrop>().thisHeroCardsObtained.enabled = false;
					}
				}
			}
		}
	}
		
    //This method opens up the hero popup tab
	public void SelectHeroToEvolve(int heroToEvolveNo, Toggle toggleToDisable)
	{
		heroToPurchase = heroToEvolveNo;
		//Check which hero PLayer is evolving
		for (int x = 0; x < heroInfomation.Count; x++)
		{
            //if hero is found
            if (heroInfomation[x].heroNo == heroToEvolveNo)
            {
                lastToggleSelected = toggleToDisable;
                intOfHeroToEvolve = x;

                //Check what are the requirements
                int requiredCards = cardsRequiredToRankUp[heroInfomation[x].heroLvl] - heroCardsAmount[x];
                heroToPurchase_Card_Owned = heroCardsAmount[x];

                for (int y = 0; y < 4; y++)
                {
                    lockedImage[y].SetActive(true);
                    if (heroInfomation[x].heroLvl > y + 1)
                        lockedImage[y].SetActive(false);
                }

                // ************* FOR SPEEDER HEROES ************* \\
                if (heroInfomation[x].heroName.Contains("Speeder"))
                {
                    if (heroInfomation[x].heroLvl < 3)
                    {
                        if (heroInfomation[x].heroLvl < (Player.castleLevel / 2) + 1)
                        {
                            heroEvoLevelRequirement.text = "<color=#00FF00>Sufficient Level</color>";
                            ToggleTick("level", true);
                            sufficientPlayerLevel = true;
                        }
                        else
                        {
                            heroEvoLevelRequirement.text = string.Format("<color=#FF0000>Level up to evolve Hero</color>", requiredCards);
                            ToggleTick("level", false);
                            sufficientPlayerLevel = false;
                        }
                        requiredCastleLevelText.text = string.Format("Level Required : {0} / {1}", Player.castleLevel, heroInfomation[x].heroLvl * 2);

                        // CHECK FOR THIS HERO'S CARD AMOUNT HERE
                        if (requiredCards <= 0)
                        {
                            heroEvoCardsRequirement.text = "<color=#00FF00>Sufficient cards</color>";
                            ToggleTick("card", true);
                            sufficientCards = true;
                        }
                        else
                        {
                            heroEvoCardsRequirement.text = string.Format("<color=#FF0000>You require {0} more Cards</color>", requiredCards);
                            ToggleTick("card", false);
                            sufficientCards = false;
                        }
                        requiredCardsText.text = string.Format("Cards Required :  {0:n0} / {1:n0}", heroCardsAmount[x], cardsRequiredToRankUp[heroInfomation[x].heroLvl]);

                        //heroEvoStonesRequirement.gameObject.SetActive(false);
                        ToggleTick("stone", true);

                        requiredStonesText.text = "Neutral Hero does not require stones for evolution";
                        sufficientStones = true;


                        // =============== GOLD CHECK AREA FOR HERO EVOLUTION ================= \\
                        // here is checking for the gold upgrade cost of the current hero selected
                        if (Player.gold >= cardUpgrade_GoldCost[heroInfomation[x].heroLvl])
                        {
                            heroEvoGoldRequirement.text = string.Format("<color=#00FF00>Sufficient Gold</color>");
                            ToggleTick("gold", true);
                            sufficientGold = true;
                        }
                        else
                        {
                            heroEvoGoldRequirement.text = string.Format("<color=#FF0000>Requires {0:n0} more to Evolve</color>",
                                                                        cardUpgrade_GoldCost[heroInfomation[x].heroLvl] - Player.gold);
                            ToggleTick("gold", false);
                            sufficientGold = false;
                        }
                        requiredGoldText.text = string.Format("Gold Required :  {0:n0} / {1:n0} ", Player.gold, cardUpgrade_GoldCost[heroInfomation[x].heroLvl]);

                        if (sufficientCards == true && sufficientStones == true && sufficientGold == true && sufficientPlayerLevel == true)
                            evolveHeroButton.interactable = true;
                        else
                            evolveHeroButton.interactable = false;
                    }
                    else
                    {
                        ToggleTick("stone", false);
                        ToggleTick("gold", false);
                        ToggleTick("card", false);
                        ToggleTick("level", false);

                        requiredCastleLevelText.text = string.Format("- Hero already at MAX Level -");
                        requiredCardsText.text = string.Format("- Hero already at MAX Level -");
                        requiredGoldText.text = string.Format("- Hero already at MAX Level -");
                        requiredStonesText.text = string.Format("- Hero already at MAX Level -");

                        heroEvoCardsRequirement.text = "<color=#FFFFFF>Hero at MAX Level</color>";
                        heroEvoStonesRequirement.text = "<color=#FFFFFF>Hero at MAX Level</color>";
                        heroEvoGoldRequirement.text = "<color=#FFFFFF>Hero at MAX Level</color>";
                        heroEvoLevelRequirement.text = "<color=#FFFFFF>Hero at MAX Level</color>";

                        evolveHeroButton.interactable = false;
                    }
                }

                // ************* FOR REGULAR HERO ************* \\
                else
                {
                    if (heroInfomation[x].heroLvl <= 4)
                    {
                        // CHECK IF THE HERO IS EVOLVABLE BASED ON PLAYER'S LEVEL

                        // formula : heroLevelCap = 1 + (playerLevel / 2)

                        if (heroInfomation[x].heroLvl < (Player.castleLevel / 2) + 1)
                        {
                            heroEvoLevelRequirement.text = "<color=#00FF00>Sufficient Level</color>";
                            ToggleTick("level", true);
                            sufficientPlayerLevel = true;
                        }
                        else
                        {
                            heroEvoLevelRequirement.text = string.Format("<color=#FF0000>Level up to evolve Hero</color>", requiredCards);
                            ToggleTick("level", false);
                            sufficientPlayerLevel = false;
                        }
                        requiredCastleLevelText.text = string.Format("Level Required :  {0:n0} / {1:n0}", Player.castleLevel, heroInfomation[x].heroLvl * 2);

                        // CHECK FOR THIS HERO'S CARD AMOUNT HERE
                        if (requiredCards <= 0)
                        {
                            heroEvoCardsRequirement.text = "<color=#00FF00>Sufficient cards</color>";
                            ToggleTick("card", true);
                            sufficientCards = true;
                        }
                        else
                        {
                            heroEvoCardsRequirement.text = string.Format("<color=#FF0000>You require {0} more Cards</color>", requiredCards);
                            ToggleTick("card", false);
                            sufficientCards = false;
                        }
                        requiredCardsText.text = string.Format("Cards Required :  {0:n0} / {1:n0}", heroCardsAmount[x], cardsRequiredToRankUp[heroInfomation[x].heroLvl]);


                        // CHECK FOR STONES HERE; SEARCH FOR GOBLIN FIRST BECAUSE HIS EVOLUTION DOESNT NEED STONES
                        if (!heroInfomation[x].heroName.Contains("Speeder"))
                        {
                            heroEvoStonesRequirement.gameObject.SetActive(false);
                            //check for stones
                            if (heroInfomation[x].unitShape == UnitShape.Square)
                            {
                                //check for square stones amount
                                if (Player.squareStone >= stonesRequiredToRankUp[heroInfomation[x].heroLvl])
                                {
                                    sufficientStones = true;
                                    stoneToDeduct = "square";
                                    heroEvoStonesRequirement.text = "<color=#00FF00>Sufficient Earth Stones</color>";
                                    ToggleTick("stone", true);
                                }
                                else
                                {
                                    sufficientStones = false;
                                    heroEvoStonesRequirement.gameObject.SetActive(true);
                                    heroEvoStonesRequirement.text = string.Format("<color=#FF0000>Requires {0} more Earth Stones</color>", stonesRequiredToRankUp[heroInfomation[x].heroLvl] - Player.squareStone);
                                    ToggleTick("stone", false);
                                }
                                requiredStonesText.text = string.Format("Earth Stones Required :  {0:n0} / {1:n0}", Player.squareStone, stonesRequiredToRankUp[heroInfomation[x].heroLvl]);
                            }
                            else if (heroInfomation[x].unitShape == UnitShape.Circle)
                            {
                                //check for square stones amount
                                if (Player.circleStone >= stonesRequiredToRankUp[heroInfomation[x].heroLvl])
                                {
                                    sufficientStones = true;
                                    stoneToDeduct = "circle";
                                    heroEvoStonesRequirement.text = "<color=#00FF00>Sufficient Fire Stones</color>";
                                    ToggleTick("stone", true);
                                }
                                else
                                {
                                    sufficientStones = false;
                                    heroEvoStonesRequirement.gameObject.SetActive(true);
                                    heroEvoStonesRequirement.text = string.Format("<color=#FF0000>Requires {0} more Flame Stones</color>", stonesRequiredToRankUp[heroInfomation[x].heroLvl] - Player.circleStone);
                                    ToggleTick("stone", false);
                                }
                                requiredStonesText.text = string.Format("Flame Stones Required :  {0:n0} / {1:n0}", Player.circleStone, stonesRequiredToRankUp[heroInfomation[x].heroLvl]);
                            }
                            else if (heroInfomation[x].unitShape == UnitShape.Triangle)
                            {
                                //check for square stones amount
                                if (Player.triangleStone >= stonesRequiredToRankUp[heroInfomation[x].heroLvl])
                                {
                                    sufficientStones = true;
                                    stoneToDeduct = "triangle";
                                    heroEvoStonesRequirement.text = "<color=#00FF00>Sufficient Wind Stones</color>";
                                    ToggleTick("stone", true);
                                }
                                else
                                {
                                    sufficientStones = false;
                                    heroEvoStonesRequirement.gameObject.SetActive(true);
                                    heroEvoStonesRequirement.text = string.Format("<color=#FF0000>Requires {0} more Wind Stones</color>", stonesRequiredToRankUp[heroInfomation[x].heroLvl] - Player.triangleStone);
                                    ToggleTick("stone", false);
                                }
                                requiredStonesText.text = string.Format("Wind Stones Required : {0:n0} / {1:n0}", Player.triangleStone, stonesRequiredToRankUp[heroInfomation[x].heroLvl]);
                            }
                        }
                        else
                        {
                            //heroEvoStonesRequirement.gameObject.SetActive(false);
                            ToggleTick("stone", true);
                            requiredStonesText.text = "Neutral Hero does not require stones for evolution";
                            sufficientStones = true;
                        }


                        // =============== GOLD CHECK AREA FOR HERO EVOLUTION ================= \\
                        // here is checking for the gold upgrade cost of the current hero selected
                        if (Player.gold >= cardUpgrade_GoldCost[heroInfomation[x].heroLvl])
                        {
                            heroEvoGoldRequirement.text = string.Format("<color=#00FF00>Sufficient Gold</color>");
                            ToggleTick("gold", true);
                            sufficientGold = true;
                        }
                        else
                        {
                            heroEvoGoldRequirement.text = string.Format("<color=#FF0000>Requires {0:n0} more to Evolve</color>",
                                                                        cardUpgrade_GoldCost[heroInfomation[x].heroLvl] - Player.gold);
                            ToggleTick("gold", false);
                            sufficientGold = false;
                        }
                        requiredGoldText.text = string.Format("Gold Required : {0:n0} / {1:n0} ", Player.gold, cardUpgrade_GoldCost[heroInfomation[x].heroLvl]);

                        if (sufficientCards == true && sufficientStones == true && sufficientGold == true && sufficientPlayerLevel == true)
                            evolveHeroButton.interactable = true;
                        else
                            evolveHeroButton.interactable = false;
                    }
                    else
                    {
                        ToggleTick("stone", false);
                        ToggleTick("gold", false);
                        ToggleTick("card", false);
                        ToggleTick("level", false);

                        requiredCastleLevelText.text = string.Format("- Hero already at MAX Level -");
                        requiredCardsText.text = string.Format("- Hero already at MAX Level -");
                        requiredGoldText.text = string.Format("- Hero already at MAX Level -");
                        requiredStonesText.text = string.Format("- Hero already at MAX Level -");

                        heroEvoCardsRequirement.text = "<color=#FFFFFF>Hero at MAX Level</color>";
                        heroEvoStonesRequirement.text = "<color=#FFFFFF>Hero at MAX Level</color>";
                        heroEvoGoldRequirement.text = "<color=#FFFFFF>Hero at MAX Level</color>";
                        heroEvoLevelRequirement.text = "<color=#FFFFFF>Hero at MAX Level</color>";

                        evolveHeroButton.interactable = false;
                    }
                }
                
                Hero_Info_Assign.Instance.Display_Card_Cost_Values(CardCost[heroInfomation[x].heroRarity]);
                Hero_Info_Assign.Instance.Check_ExcaliPoints_Button(CardCost[heroInfomation[x].heroRarity]);
                break;
            }
		}
	}

    public void ToggleTick(string setType, bool toSet)
    { 
        if (setType == "card")
        {
            cardsMetTick.SetActive(toSet);
            heroEvoCardsRequirement.gameObject.SetActive(!toSet);
        }

        if (setType == "stone")
        {
            stonesMetTick.SetActive(toSet);
            heroEvoStonesRequirement.gameObject.SetActive(!toSet);
        }

        if (setType == "gold")
        {
            goldMetTick.SetActive(toSet);
            heroEvoGoldRequirement.gameObject.SetActive(!toSet);
        }

        if (setType == "level")
        {
            playerLevelMetTick.SetActive(toSet);
            heroEvoLevelRequirement.gameObject.SetActive(!toSet);
        }
    }

	public void Purchase_Card_With_Excalipoints(int timesToPurchase)
	{
        for (int x = 0; x < heroInfomation.Count; x++)
        {
            if (intOfHeroToEvolve == heroInfomation[x].heroNo)
            {
                //add the number of cards for player
                heroCardsAmount[heroInfomation[x].heroNo] += 1 * timesToPurchase;
                break;
            }
        }
        for (int x = 0; x < heroInfomation.Count; x++)
        {
            if (heroInfoTab[x].GetComponent<DragAndDrop>().assignedTroopID == intOfHeroToEvolve)
            {
                heroInfoTab[x].GetComponent<DragAndDrop>().SendThisHeroNo(true);
                break;
            }
        }
        for (int x = 0; x < heroInfomation.Count; x++)
        {
            if (heroInfomation[x].heroNo == intOfHeroToEvolve)
            //spend the amount of points
            {
                Player.Instance.SpendExcalipoints(CardCost[heroInfomation[x].heroRarity] * timesToPurchase);
                Hero_Info_Assign.Instance.Check_ExcaliPoints_Button(CardCost[heroInfomation[x].heroRarity]);
                break;
            }
        }

		//update on the ui
		DistributeStats();

        Hero_Info_Assign.Instance.UpdateExcalipointsAmount();
		UIManager.Instance.UpdateTavernInfos();
        UIManager.Instance.Update_All();

        DataManager.Instance.PrepareDataForSparksPushing();

        GameSparksManager.Instance.SavePlayerExpendables(GameSparksManager.playerId);
	}  

	public void EvolveHero()
	{
		//Activate some animation here

		//Deducting the amount of cards belonging to the hero that is being upgraded
		heroCardsAmount[intOfHeroToEvolve] -= cardsRequiredToRankUp[heroLevels[intOfHeroToEvolve]];
		//Deducting the amount of stones used for evolving the hero
		Player.Instance.SpendEvolutionStones(stoneToDeduct, stonesRequiredToRankUp[heroLevels[intOfHeroToEvolve]]);

        Player.Instance.SpendGold(cardUpgrade_GoldCost[heroLevels[intOfHeroToEvolve]]);

		heroLevels[intOfHeroToEvolve] += 1;
		Player.heroLevels[intOfHeroToEvolve] += 1;
		for (int x = 0; x < heroInfomation.Count; x++)
		{
			if (heroInfomation[x].heroNo == intOfHeroToEvolve)
			{
				heroInfomation[x].heroLvl = heroLevels[intOfHeroToEvolve];

                UIManager.Instance.ProgressNotification(heroInfomation[x].heroRace, heroLevels[intOfHeroToEvolve]);
                 
				break;
			}
		}


		Untoggle();
		DistributeStats();


        TroopAssign_Info.Instance.Reassign(intOfHeroToEvolve);
        TroopAssign_Info.Instance.UpdateAssignedTroopValues();

        GameSparksManager.Instance.SaveHeroInfo(intOfHeroToEvolve);

		if (Player.accountType =="Guest")
		{
			SaveHeroInfo();
		}
	}

	void Untoggle()
	{
		if (lastToggleSelected != null)
		    lastToggleSelected.isOn = false;
	}

    public void ResetHeroInfo()
    {
        for (int x = 0; x < Player.heroAmount; x++)
        {
            heroInfomation[x].heroLvl = 0;
            heroInfomation[x].cardAmount = 0;
            heroInfomation[x].heroIsUnlocked = 0;
        }
    }
}