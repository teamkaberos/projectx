﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    #region Singleton
    private static UIManager _instance;
    public static UIManager Instance
    {
        get
        {
            return _instance;
        }
    }
    #endregion

    [Header("Home Scene")]
    public Text nameDisplay;
    public Text availableGroups;
    public Text clanDisplay;
    public Text reputationDisplay;
    public Text townHallLevelDisplay;
    public Text goldDisplay;
    public Text excaliburPoints;
    public Image goldFilledDisplay;                                             //Green Filled bar that will appear on the UI
    public Text lumberDisplay;
    public Text diamondDisplay;
    public Text tokenDisplay;
    public Text playerEnergy;
    public Text castleLvlDisplay;

    public Transform UIBuildings_Pop_Up;                            //Parent canvas that holds transform for all child UI buildings

    public Toggle[] heroSlots;
    public Image[] heroSlotImage;

    public GameObject insufficientEnergyButton;
    public GameObject insufficientEnergyPopup;
    public Text remainingEnergyPack;
    public Button useEnergyPackButton;
    public GameObject addEnergyButton;

    public GameObject excaliburHolderRewardsButton;

    public Toggle clanChatToggle;

    #region The Player Info Tab Pop up
    [Header("PlayerTab")]
    public Image playerAvatar;
    public Text myRepRequired;
    public Text playerName_;
    public Text playerClan_;
    public Text playerRace_;
    public Text playerUUID_;
    public Text clientVersion_;
    public Text playerRep_;
    public Text playerBattles_;
    public Text playerVictories_;
    public Text playerGold_;
    public Text playerDiamonds_;
    public Text playerTokens_;
    public Text myHeroesOwned;
    public Text myExcalipoints_;
    public Text playerDragons_;
    public Text playerSeige_;
    public Text playerRSSBuilding_;
    public Text playerCastleLevel_;
    public Text playerMeleeBattles;
    public Text playerRangeBattles;
    public Text playerCasterBattles;

    int toGreenCounter = 0;                                        //Used to count how many greens there will be
    int goldMineChecked = 0;                                      //Used to mark the last checked mine
    int goldMineRestrict = 0;                                       //Used to check how many goldmines are at the max

    public Button townHallButton_;
    public Button[] goldMineButton_;
    public Text[] goldMineButtonText_;
    public Button rHallButton_;
    public Button bunkerButton_;
    public Button guildButton_;
    public Button rangeButton_;
    public Button forgeButton_;
    //	public Button tavernButton_;
    public Button treasuryButton_;

    public Image treasuryUpgradable_;
    public Image rHallUpgradable_;
    public Image goldMineUpgradable_;
    public Image[] goldMineUpgraded_;
    public Image[] goldMineUpgradedLong_;
    public Image meleeUpgrade_;
    public Image rangeUpgrade_;
    public Image guildUpgrade_;
    #endregion

    [Space(30)]
    [Header("Mail")]
    public GameObject mail_Notif_Toggle;
    public GameObject rewardRecieve;                //the button that will appear in the mail box for players to tap
    public GameObject mailRecieve;
    public Transform inboxSlot;
    public Text inboxAmount;                                        //value that will display amount in the inbox
    public GameObject rewardClaimed;
    public Text rewardClaimCheck;

    [Space(30)]
    [Header("Seige")]
    public GameObject siegedPopup;
    public Text siegedValue;
    public Transform seigedInbox;
    public FXPlay siegedFires;
    public GameObject seigeRevengeInfo;
    public GameObject revengeChildPop;  //pop up if there are seiges
    public Text revengeChildAmount;        //how many seiges are there

    public Image seigedButtonImage;
    public Image matchMakeButtonImage;
    public Vector4 originalColor = new Vector4(1, 1, 1, 1);
    public Vector4 backColor = new Vector4(0.5f, 0.5f, 0.5f, 1);


    [Space(30)]
    [Header("PVP Popup")]
    public Text pCLevel;
    public Text pFlags;
    public Text pShuffleTime;
    public Text pGroups;

    public Toggle secondDeck_Toggle;                                // For disabling ibefore the player unlocks
    public Button troopBuild_Button;                                    // For the building of troops
    public Button mini_TroopBuild_Button;                   // for the minipop up
    public Button supportBuild_Button;
    public Button mini_SupportBuild_Button;

    public Toggle mineToggle;
    public Toggle dragonToggle;
    public Toggle seigeToggle;

    public Text pHeroLevels;
    [Space(30)]
    public GameObject[] buildingPopUps;                         //For enabling when building completes
    [Space(30)]
    public GameObject[] progressBar;                                    //To be disabled when players hasten or cancel structures
    public Button[] hastenBuild;                                                //Buttons for hasting structure buildings
    [Space(30)]
    [Header("Mini PopUps Bottom Screen")]
    public GameObject[] popupManage;                                //To hide small building infos
    public GameObject[] miniBuildingComplete;
    [Space(30)]
    [Header("SpeedingUp")]
    public GameObject speedUp_Popup;                                // Speed up pop up for structures
    public Text timer3_Left;
    public Text timer15_Left;
    public Text timer30_Left;
    public Text timer60_Left;
    public Text diamondSpend;
    public Text speedUpWith;
    [Space(30)]
    public Button[] upgradeButton;                                              //All the ACTUAL upgrade buttons
                                                                                //	public Button[] checkRSSButton;										//the buttons that will check before invoking the ACTUAL build button
    public Text speedUp_Cost_Text_Display;                      //Text display for speed up cost
    public Button speed_Up_with_Diamond_Button; //button that finalizes the speedup with button                                        //Check if player has enough levels to unlock
    [Space(30)]

    [Header("Barracks Info")]
    public Text meleeHallsLvDisplay;
    public Text bunker_GoldCost;
    public Text bunker_LevelUpDuration;
    public Text bunker_Requirement_Display;

    public Text rangedHallsLvDisplay;
    public Text ranged_GoldCost;
    public Text ranged_LevelUpDuration;
    public Text ranged_Requirement_Display;

    public Text wizardHallsLvDisplay;
    public Text wizard_GoldCost;
    public Text wizard_LevelUpDuration;
    public Text wizard_Requirement_Display;

    [Space(30)]
    [Header("Tavern Info")]
    public Text flameStones;
    public Text thunderStones;
    public Text windStones;
    public Text excalipoints;
    public Text sqStoneBattles;
    public Text crStoneBattles;
    public Text trStoneBattles;

    public Text heroEvolutionInfo;


    public Text packsAvailable;
    public Text chestsAvailable;
    public Text purchasedContent;
    public Text purchasedContent2;
    public Button tavernPurchase_Item_Button;
    public Button jackButton;
    public Text jacksAvailable;
    public Button queenButton;
    public Text queensAvailable;
    public Button kingButton;
    public Text kingsAvailable;
    public Button bronzeChestButton;
    public Text bronzeChestAvailable;
    public Button silverChestButton;
    public Text silverChestAvailable;
    public Button goldChestButton;
    public Text goldChestAvailable;
    public Button[] beginResearch_Button;

    public GameObject[] stone_Collect;
    public GameObject[] stone_SpeedUp;
    public Button close_Reward_Display_Button;
    int beforeCloseButtonGetsEnabled;                                                           //check if all cards are opened
    int assignValue = 0;                                                                                    //Change according to cards that are going to be assigned

    public PlayOpenCard[] jackOpenCard;
    public PlayOpenCard[] queenOpenCard;
    public PlayOpenCard[] kingOpenCard;

    public Text[] jackPackRewards_Display;
    public Text[] queenPackRewards_Display;
    public Text[] kingPackRewards_Display;

    public Image[] jackPackRewards_DisplayImage;
    public Image[] queenPackRewards_DisplayImage;
    public Image[] kingPackRewards_DisplayImage;

    [Space(30)]
    public Text minesAvail;
    public Text dragonsAvail;
    public Text seigesAvail;
    public Text sUnitsInQueue;
    [Space(30)]
    [Header("Recruitment Hall Info")]
    public Text recruitmentHall_Lvl_Display;
    public Text groupCost_Display;
    public Text new_GroupCost_Display;
    public Text recruitmentHall_Cost_Display;
    public Text recruitmentHall_BuildDuration;
    public Text recruitmentHall_BuildLimit_Display;
    public Text recruitmentHall_New_BuildLimit_Display;
    public Text recruitmentHall_Requirement_Display;
    public Text current_Shuffle_Time_Display;
    public Text new_Shuffle_Time_Display;
    public Text remaining_Assignment_Points;
    public GameObject troopAssignmentError;
    public Text troopAssignErrorMessage;
    public Text groupsInQueue;
    [Space(30)]
    [Header("Town Hall Info")]
    public Text townHall_Lvl_Display;
    public Text townHall_Cost_Display;
    public Text townHall_Upgrade_Duration_Display;
    public Text old_Flags_Amount;
    public Text new_Flags_Amount;
    public Text old_HeroSlots_Amount;
    public Text new_HeroSlots_Amount;
    public Text old_Group_Limit;
    public Text new_Group_Limit;
    public Text old_Deck_Amount;
    public Text new_Deck_Amount;
    [Space(30)]
    [Header("Treasury Info")]
    public Text treasury_Lvl_Display;
    public Text treasury_Cost_Display;
    public Text treasury_Upgrade_Duration_Display;
    public Text treasury_Current_Storage_Value_Display;
    public Text treasury_New_Storage_Value_Display;
    public Text treasury_Current_Protection_Value_Display;
    public Text treasury_New_Protection_Value_Display;
    public Text treasury_Requirement_Display;
    [Space(30)]
    [Header("Forge Info")]
    public Text forge_Level;
    public Text support_Current_Level;
    public Text support_New_Level;
    public Text forge_Cost;
    public Text forge_Requirement_Display;
    public Text forge_Duration_Display;

    [Space(30)]
    [Header("Mills Info")]
    public GameObject mills_Pop_Up;
    public Text mills_Level_UP_Display;
    public Text lumberMill_Current_Gen_Rate;           //CurrentGeneration rate
    public Text lumberMill_Next_Gen_Rate;              //nxt lvl gen rate
    public Text lumberMill_Current_Cap_Value;          //Current storage cap
    public Text lumberMill_Next_Cap_Value;             //nexrt lvl storage cap
    public Text mill_GoldCost;                         //Cost to upgrade with lumber
    public Text mill_LevelUpDuration;                  //Duration to upgrade
    public Text mill_Requirement_Display;
    [Space(30)]
    [Header("Mines Info")]
    public GameObject mines_Pop_Up;
    public Text mines_Level_UP_Display;
    public Text gMines_Current_Gen_Rate;                //CurrentGeneration rate
    public Text gMines_Next_Gen_Rate;                   //nxt lvl gen rate
    public Text gMines_Current_Cap_Value;               //Current storage cap
    public Text gMines_Next_Cap_Value;                  //nexrt lvl storage cap
    public Text mines_GoldCost;                         //Cost to upgrade with lumber
    public Text mine_LevelUpDuration;                   //Duration to upgrade
    public Text mine_Requirement_Display;
    public Text current_Popup_RSSInfo;                  //Small rss info popup

    string newBuildingDuration;                         //Used to hold the converted float duration to string in xx:yy:zz format

    [Space(30)]
    public GameObject loadingScreen;
    public GameObject battleLoadingScreen;              //The loading screen for going into battle
    public GameObject criteria_Not_Met_Pop_Up;          //Pop up that informs player they have insufficient resource
    public Text criteria_Not_Met_Text_Display;          //To be filled in the type of resource that player lacks
    [Space(30)]
    public GameObject castleLvUp_Pop_Up;                //Popup when player lvls up
    public Text castleLv_Attained_Text;                 //Text to display player's level
    public Text buildingCompletedNotif;                 //Text to display on building complete

    Animator anim;
    Animator chatBoxAnim;
    [Space(30)]

    public ProgressDisplayPanel progressDisplay;        // the Class
    public GameObject progressDisplayPanel;             // the GameObject
    public Text itemToDisplayText;

    public GameObject chatCanvas;

    public Toggle clanButton;
    public GameObject clanInfoButton;
    public Toggle sUnitFarmToggle;
    public GameObject sUnitFarmInfoToggle;

    public GameObject accountDetailPanel;


    [Space(35)]
    [Header("Main Interface Buttons")]
    public Transform buttonsPanel;
    public Transform blockObject;


    [Space(35)]
    [Header("Shared")]


    [Space(35)]
    [Header("Battle Scene")]

    public GameObject[] playerFlags;
    public GameObject damageFlash;
    public GameObject hitFlash;

    public Text[] myCardsInfo;
    public Text sUnitsNotify;

    public Text timerDisplay;

    public GameObject[] heroButton;
    public Transform fakeCardBack1;
    public Transform fakeCardBack2;
    public GameObject deckBack1;
    public GameObject deckBack2;

    //unit blocking variables
    public Transform deck1UnitBlockingDial;
    public Transform deck1CurrentCardPos;
    public Transform deck1lastCardPos;
    public Transform deck2UnitBlockingDial;
    public Transform deck2CurrentCardPos;
    public Transform deck2lastCardPos;

    public GameObject[] soul_Charges_Display;

    public GameObject[] heroCards;

    public GameObject supportBlocker;

    public GameObject recievedDiamond_Display;      //the gameobject to setactive if player gets diamond
    public Text receivedGold;
    public Text receivedDiamond;
    public Text receivedT3;
    public Text receivedT15;
    public Text receivedT30;
    public Text receivedT60;
    public GameObject t3display;
    public GameObject t15display;
    public GameObject t30display;
    public GameObject t60display;

    public Button retreatButton;
    public GameObject gameOverBackButton;
    public GameObject victoryBackButton;

    //===================================//
    //MONODEVELOP//
    //===================================//

    void Awake()
    {
        _instance = this;
    }

    void Start()
    {
        if (SceneManager.GetActiveScene().name.ToLower().Contains("home"))
        {
            anim = GameObject.Find("MainCanvas").GetComponent<Animator>();

            chatBoxAnim = chatCanvas.GetComponent<Animator>();

            excaliburHolderRewardsButton.SetActive(false);
        }
        else if (SceneManager.GetActiveScene().name.Contains("Battle"))
        {
            DisplayName();
            CheckCastleLevel();
            DisplayFlag();

            if (Player.forgeLevel < 0)
            {
                // SET forgeLevel requirement to 1 so the blocker will be active at the correct time
                supportBlocker.SetActive(true);
            }
            else if (Player.forgeLevel >= 1)
            {
                supportBlocker.SetActive(false);
            }

            if (Player.tutorial < Player.tutorialCap)
                retreatButton.interactable = false;
            else
                retreatButton.interactable = true;
        }
    }

    void Update()
    {
        if (SceneManager.GetActiveScene().name.Contains("Home"))
        {
            if (Player.playerCurrentEnergy < Player.maxEnergy)
            {
                Player.currentEnergyRechargeTime -= Time.deltaTime;
                playerEnergy.text = string.Format("(+1 Energy in {0}) {1}/{2}", EnergyRechargeTime(Player.currentEnergyRechargeTime),
                                                  Player.playerCurrentEnergy, Player.maxEnergy);

                if (!addEnergyButton.activeInHierarchy)
                    addEnergyButton.SetActive(true);


                if (Player.currentEnergyRechargeTime <= 0)
                {
                    Player.Instance.RechargeEnergy(1);
                    insufficientEnergyButton.SetActive(false);
                    if (Player.playerCurrentEnergy == Player.maxEnergy)
                    {
                        Update_Energy();
                        addEnergyButton.SetActive(false);
                    }
                    Player.currentEnergyRechargeTime = Player.energyRechargeTime;
                }
            }
            else
            {
                if (Player.playerCurrentEnergy > Player.maxEnergy)
                {
                    Player.playerCurrentEnergy = Player.maxEnergy;
                    addEnergyButton.SetActive(false);
                    Player.currentEnergyRechargeTime = Player.energyRechargeTime;
                }

                if (addEnergyButton.activeInHierarchy)
                    addEnergyButton.SetActive(false);
            }
        }
    }

    //================= HOME SCENE ====================//

    public void Update_All()
    {
        UpdatePlayerInfo();
        UpdateCurrencyInfo();
        Update_Building_LevelInfo();
        Check_If_Allowed_PVP();
    }

    // ====== VALUES UPDATING ====== \\
    public void Update_Energy()
    {
        playerEnergy.text = string.Format("{0}/{1}", Player.playerCurrentEnergy, Player.maxEnergy);
    }

    public void UpdatePlayerInfo()
    {
        nameDisplay.text = "" + Player.playerName;
        reputationDisplay.text = string.Format("{0:n0}", Player.reputation);
        castleLvlDisplay.text = "LV. " + Player.castleLevel;
        if (Clan.clanID != "<currently_not_in_any_clan_yet>")
            clanDisplay.text = "" + Clan.clanID;
        else
            clanDisplay.text = "";
    }
    public void UpdatePlayerTabInfo()
    {
        playerName_.text = "Name : " + Player.playerName;
        playerCastleLevel_.text = "Player Level : " + Player.castleLevel;
        if (Clan.clanID != "<currently_not_in_any_clan_yet>")
            playerClan_.text = "Clan : " + Clan.clanID;
        else
            playerClan_.text = "Clan : n/a";
        playerRace_.text = "Race : " + Player.race;
        playerUUID_.text = "Xcalibur UUID : " + GameSparksManager.playerId;
        playerRep_.text = string.Format("Reputation : {0:n0}", Player.reputation);
        clientVersion_.text = string.Format("Client Version : {0}", Player.gameVersion);
        playerBattles_.text = "Matches : " + Player.my_Matches;
        if (Player.my_Matches >= 1)
            playerVictories_.text = string.Format("Victories : {0} ({1:F2}%)", Player.my_Victories, Player.Instance.victoryPercentage());
        else
            playerVictories_.text = string.Format("Victories : {0} (0.00%)", Player.my_Victories);
        playerGold_.text = string.Format("Gold : {0:n0}", Player.gold);
        playerDiamonds_.text = string.Format("Diamonds : {0:n0}", Player.diamond);
        playerTokens_.text = string.Format("Tokens : {0:n0}", Player.tokens);
        myHeroesOwned.text = string.Format("Heroes Owned : {0} / {1}", UnitManager.Instance.HeroesUnlockedCount(), UnitManager.Instance.TotalHeroAmount());
        myExcalipoints_.text = string.Format("Xcalipoints : {0:n0}", Player.Instance.excalipoint_Value());
        myRepRequired.text = string.Format("Rep to next PLAYER LEVEL : {0}", Player.Instance.RepRequiredToNextCastleLevel());
        /*playerDragons_.text =  "Dragons : " + Player.dragons;
        playerSeige_.text =  "Seige : " + Player.seige;*/
        playerRSSBuilding_.text = "Unlocked generators : " + Player.resourceBuildings_Unlock_Counter[Player.castleLevel];/*
        playerMeleeBattles.text = "Melee Battles : " + Player.squareBattles;
        playerRangeBattles.text = "Ranged Battles : " + Player.triangleBattles;
        playerCasterBattles.text = "Caster Battles : " + Player.circleBattles;*/
    }
    public void Update_PlayerTab_Buttons()
    {
        goldMineChecked = 0;
        goldMineRestrict = 0;
        toGreenCounter = 0;

        #region barraks
        //check barracks first
        if (Player.bunkerLv >= (Player.castleLevel * 3 + 3) || Player.bunkerLv >= Player.barracksLvlCap)
            bunkerButton_.interactable = false;
        else
            bunkerButton_.interactable = true;
        if (Player.bunkerLv > Player.recruitmentHall_Lvl)
            meleeUpgrade_.color = Color.green;
        else
            meleeUpgrade_.color = Color.red;

        //then ranged
        if (Player.rangeLv >= (Player.castleLevel * 3 + 3) || Player.rangeLv >= Player.barracksLvlCap)
            rangeButton_.interactable = false;
        else
            rangeButton_.interactable = true;
        if (Player.rangeLv > Player.recruitmentHall_Lvl)
            rangeUpgrade_.color = Color.green;
        else
            rangeUpgrade_.color = Color.red;

        //and finally casters
        if (Player.guildLv >= (Player.castleLevel * 3 + 3) || Player.guildLv >= Player.barracksLvlCap)
            guildButton_.interactable = false;
        else
            guildButton_.interactable = true;
        if (Player.guildLv > Player.recruitmentHall_Lvl)
            guildUpgrade_.color = Color.green;
        else
            guildUpgrade_.color = Color.red;
        #endregion

        if (Player.recruitmentHall_Lvl >= Player.guildLv || Player.recruitmentHall_Lvl >= Player.rangeLv || Player.recruitmentHall_Lvl >= Player.bunkerLv || Player.recruitmentHall_Lvl >= Player.recruitmentHallLvlCap)
            rHallButton_.interactable = false;
        else if (Player.recruitmentHall_Lvl < Player.guildLv && Player.recruitmentHall_Lvl < Player.rangeLv && Player.recruitmentHall_Lvl < Player.bunkerLv)
            rHallButton_.interactable = true;
        if (Player.recruitmentHall_Lvl > Player.townHallLevel)
            rHallUpgradable_.color = Color.green;
        else if (Player.recruitmentHall_Lvl <= Player.townHallLevel)
            rHallUpgradable_.color = Color.red;


        for (int i = 0; i < Player.resourceBuildings_Unlock_Counter[Player.castleLevel]; i++)
        {
            //enable all upgrade buttons
            goldMineButton_[i].interactable = true;
            //disable all upgradable
            goldMineUpgraded_[i].color = Color.red;
            //set unlocked text to goldmine
            goldMineButtonText_[i].text = "Gold Mine";

            if (i < 4)
                goldMineUpgradedLong_[i].color = Color.red;
        }
        //check whos upgradable
        for (int i = 0; i < Player.resourceBuildings_Unlock_Counter[Player.castleLevel]; i++)
        {
            for (int y = 0; y < Player.mines.Length; y++)
            {
                if ((goldMineConverter(y) - 1) == i)
                {
                    if (Player.mines[y] > Player.townHallLevel)
                    {
                        goldMineRestrict += 1;
                        goldMineUpgraded_[i].color = Color.green;
                        if (i >= 1)
                            goldMineUpgradedLong_[i - 1].color = Color.green;
                    }
                    if (Player.mines[y] >= (Player.castleLevel * 3 + 3) || Player.mines[y] >= Player.rssBuildingLvCap)
                    {
                        goldMineButton_[i].interactable = false;
                        goldMineButtonText_[i].text = "Gold Mine (Maxed)";
                    }
                }
            }
        }
        if (goldMineRestrict >= Player.resourceBuildings_Unlock_Counter[Player.castleLevel])
            goldMineUpgradable_.color = Color.green;
        else
            goldMineUpgradable_.color = Color.red;

        // ==-- Treasury Check --== //
        if (Player.treasuryLvl >= (Player.castleLevel * 3 + 3) || Player.treasuryLvl >= Player.treasuryLvlCap)
            treasuryButton_.interactable = false;
        else
            treasuryButton_.interactable = true;
        if (Player.treasuryLvl > Player.townHallLevel)
            treasuryUpgradable_.color = Color.green;
        else
            treasuryUpgradable_.color = Color.red;

        //==-- TownHall Check --==//
        if (treasuryUpgradable_.color == Color.green && rHallUpgradable_.color == Color.green && goldMineUpgradable_.color == Color.green)
        {
            if (Player.townHallLevel >= Player.townHallLevelCap)
                townHallButton_.interactable = false;
            else
                townHallButton_.interactable = true;
        }
        else
            townHallButton_.interactable = false;

        //==-- Forge Check --==//
        if (Player.castleLevel >= ((Player.forgeLevel * 2) + 2))
        {
            if (Player.forgeLevel < 3)
                forgeButton_.interactable = true;
        }
        else
            forgeButton_.interactable = false;
    }
    public int goldMineConverter(int mineNumber)
    {
        if (mineNumber == 0)
            return 3;
        if (mineNumber == 1)
            return 4;
        if (mineNumber == 2)
            return 2;
        if (mineNumber == 3)
            return 5;
        if (mineNumber == 4)
            return 1;
        return 0;
    }
    public void UpdateCurrencyInfo()
    {
        goldDisplay.text = string.Format("{0:n0} / {1:n0}", Player.gold, Player.treasury_Storage_Cap);
        goldFilledDisplay.rectTransform.sizeDelta = new Vector2(Player.Instance.Treasury_Filled_Perc() * 5.4f, goldFilledDisplay.rectTransform.sizeDelta.y);
        excaliburPoints.text = string.Format("{0:n0}", Player.Instance.excalipoint_Value());
        //      lumberDisplay.text = string.Format("{0:n0}/{1:n0}", Player.gold, Player.lumberCap);
        diamondDisplay.text = string.Format("{0:n0}", Player.diamond);
        playerEnergy.text = string.Format("{0}/{1}", Player.playerCurrentEnergy, Player.maxEnergy);
        tokenDisplay.text = string.Format("{0:n0}", Player.tokens);
    }

    public void UpdateTavernInfos()
    {
        //update stone values
        flameStones.text = string.Format("{0:n0}", Player.squareStone);
        thunderStones.text = string.Format("{0:n0}", Player.circleStone);
        windStones.text = string.Format("{0:n0}", Player.triangleStone);

        sqStoneBattles.text = string.Format("Battles : {0}/{1}", Player.squareBattles, Player.evolvementRequiredBattles);
        crStoneBattles.text = string.Format("Battles : {0}/{1}", Player.circleBattles, Player.evolvementRequiredBattles);
        trStoneBattles.text = string.Format("Battles : {0}/{1}", Player.triangleBattles, Player.evolvementRequiredBattles);

        excalipoints.text = string.Format("{0:n0}", Player.Instance.excalipoint_Value());
    }
    public void UpdateHeroInfo(string textToParse)
    {
        string[] words = textToParse.Split(',');
        heroEvolutionInfo.text = string.Format("Evolve <color=#{2}>{0}</color> for a chance to obtain a <color=#{2}>{1}</color> HERO.", words[0], words[1], words[2]);
        //heroEvolutionInfo.text.Replace
    }

    public void UpdateAvailableLoot()
    {
        jacksAvailable.text = string.Format("{0:n0} available", Player.jackPack);
        if (Player.jackPack <= 0)
            jackButton.interactable = false;
        else
            jackButton.interactable = true;

        queensAvailable.text = string.Format("{0:n0} available", Player.queenPack);
        if (Player.queenPack <= 0)
            queenButton.interactable = false;
        else
            queenButton.interactable = true;

        kingsAvailable.text = string.Format("{0:n0} available", Player.kingPack);
        if (Player.kingPack <= 0)
            kingButton.interactable = false;
        else
            kingButton.interactable = true;


        bronzeChestAvailable.text = Player.bronzeChest + " available";
        if (Player.bronzeChest <= 0)
            bronzeChestButton.interactable = false;
        else
            bronzeChestButton.interactable = true;

        silverChestAvailable.text = Player.silverChest + " available";
        if (Player.silverChest <= 0)
            silverChestButton.interactable = false;
        else
            silverChestButton.interactable = true;

        goldChestAvailable.text = Player.goldChest + " available";
        if (Player.goldChest <= 0)
            goldChestButton.interactable = false;
        else
            goldChestButton.interactable = true;
    }

    public void UpdateAvailableSUnits()
    {
        minesAvail.text = Player.landmines + "";
        dragonsAvail.text = Player.dragons + "";
        seigesAvail.text = Player.seige + "";
    }
    public void UpdateAvailableAssignPoints()
    {
        remaining_Assignment_Points.text = string.Format("Available Assignment Points : {0}", Player.available_Assign_Points);
    }
    public void UpdateSpeedUpInfo()
    {
        timer3_Left.text = "x " + Player.timer3;
        timer15_Left.text = "x " + Player.timer15;
        timer30_Left.text = "x " + Player.timer30;
        timer60_Left.text = "x " + Player.timer60;
    }
    // ====== VALUES UPDATING END ====== \\

    // ====== HOME INTERFACE BUTTONS ====== \\
    public void SetBlockToBottom()
    {
        blockObject.SetSiblingIndex(buttonsPanel.childCount - 2);
    }
    public void SetClanChatToggle(bool isOn)
    {
        clanChatToggle.interactable = isOn;
        if (isOn == false)
            clanChatToggle.isOn = false;

    }
    public void XcaliburRewards(bool isHolder)
    {
        if (Player.tutorial < Player.tutorialCap)
            excaliburHolderRewardsButton.SetActive(false);
        else
        {
            if (EventHolderManager.currentEventActivityStatus != EventHolderManager.EventLiveStatus.canceled)
            {
                if (EventHolderManager.clanWhenEventStarted == EventHolderManager.xcaliburHolderHistory[0].clanName)
                {
                    excaliburHolderRewardsButton.SetActive(!XcaliburEvent.eventIsOn);
                }
                else
                    excaliburHolderRewardsButton.SetActive(false);
            }
            else if (EventHolderManager.currentEventActivityStatus == EventHolderManager.EventLiveStatus.canceled)
                excaliburHolderRewardsButton.SetActive(false);
        }
    }
    // ====== HOME INTERFACE BUTTONS END ====== \\

    // ======= LISTENERS ======= \\
    public void ToggleMailNotification(bool toDeduct)
    {
        int mailsLeft = inboxSlot.childCount;

        if (toDeduct)
            mailsLeft -= 1;

        if (inboxSlot.childCount < 100)
            inboxAmount.text = mailsLeft + "";
        else if (inboxSlot.childCount >= 100)
            inboxAmount.text = mailsLeft + "+";

        if (inboxSlot.childCount > 1)
            mail_Notif_Toggle.SetActive(true);
        else
            mail_Notif_Toggle.SetActive(false);
    }
    public void RewardClaimed(string notify)
    {
        rewardClaimed.SetActive(true);
        rewardClaimCheck.text = notify;
    }

    public void DisplaySeiged(int timesAttacked, string typeOfAttack)
    {
        if (timesAttacked < 1)
            return;
        switch (typeOfAttack)
        {
            case "away":
                siegedValue.text = string.Format("The Castle was attacked {0} while you were gone. Tap on revenge to view details.", TimesAttacked(timesAttacked));
                break;
            case "home":
                siegedValue.text = "Preposterous! Attacked on your watch!";
                break;
            default:
                siegedValue.text = "Not even a fly . . .";
                break;
        }
        siegedPopup.SetActive(true);
        siegedFires.InvokedStopAfterPlay(10);
    }
    string TimesAttacked(int times)
    {
        if (times == 1)
            return "once";
        if (times == 2)
            return "twice";
        return times + " times";
    }
    // ======= LISTENERS END ======= \\

    // ======= LEVEL RESTRICTED ====== \\
    public void ChatCanvasDisplay()
    {
        if (Player.tutorial < Player.tutorialCap)
            chatCanvas.SetActive(false);
        else
            chatCanvas.SetActive(true);
    }
    public void ClanToggleDisplay()
    {
        if (Player.reputation < 120)
        {
            clanButton.interactable = false;
            clanInfoButton.SetActive(true);
        }
        else
        {
            clanButton.interactable = true;
            clanInfoButton.SetActive(false);
        }
    }
    public void SetSUnitToggleDisplay()
    {
        sUnitFarmToggle.interactable = Player.castleLevel >= 2;
        sUnitFarmInfoToggle.SetActive(Player.castleLevel < 2);
    }
    // ======= LEVEL RESTRICTED  END====== \\

    void Check_If_Allowed_PVP()
    {
        if (Player.playerCurrentEnergy <= 0)
        {
            insufficientEnergyButton.SetActive(true);
        }
    }

    public void ToggleTab(int tabNo)
    {
        if (tabNo == 0)
        {
            seigedButtonImage.color = backColor;
            matchMakeButtonImage.color = originalColor;
        }
        else if (tabNo == 1)
        {
            seigedButtonImage.color = originalColor;
            matchMakeButtonImage.color = backColor;
        }
    }

    public void Toggle_Tavern_Purchase_Button(bool newValue)
    {
        tavernPurchase_Item_Button.interactable = newValue;
    }
    public void AssignTavern_Reward_Names(string rewardName, int rewardValue, Sprite rewardIcon, string textColor, bool isNew, int heroID)
    {
        if (assignValue <= 0)
        {
            assignValue = rewardValue;
            beforeCloseButtonGetsEnabled = rewardValue;
        }

        if (rewardValue == 6)
        {
            jackOpenCard[assignValue - 1].isNew = isNew;
            jackOpenCard[assignValue - 1].heroID = heroID;
            jackOpenCard[assignValue - 1].heroName = rewardName;
            jackPackRewards_Display[assignValue - 1].text = string.Format("<color={0}>{1}</color>", textColor, rewardName);
            jackPackRewards_DisplayImage[assignValue - 1].sprite = rewardIcon;
            assignValue -= 1;
            return;
        }

        if (rewardValue == 8)
        {
            queenOpenCard[assignValue - 1].isNew = isNew;
            queenOpenCard[assignValue - 1].heroID = heroID;
            queenOpenCard[assignValue - 1].heroName = rewardName;
            queenPackRewards_Display[assignValue - 1].text = string.Format("<color={0}>{1}</color>", textColor, rewardName);
            queenPackRewards_DisplayImage[assignValue - 1].sprite = rewardIcon;
            assignValue -= 1;
            return;
        }

        if (rewardValue == 10)
        {
            kingOpenCard[assignValue - 1].isNew = isNew;
            kingOpenCard[assignValue - 1].heroID = heroID;
            kingOpenCard[assignValue - 1].heroName = rewardName;
            kingPackRewards_Display[assignValue - 1].text = string.Format("<color={0}>{1}</color>", textColor, rewardName);
            kingPackRewards_DisplayImage[assignValue - 1].sprite = rewardIcon;
            assignValue -= 1;
            return;
        }
    }
    public void Enable_Reward_Display_Close_Button()
    {
        beforeCloseButtonGetsEnabled -= 1;
        if (beforeCloseButtonGetsEnabled == 1)
        {
            close_Reward_Display_Button.interactable = true;
        }
    }


    // ========== MINI POPUP INFOS ========== \\
    public void HideMiniInfo()
    {
        for (int x = 0; x < popupManage.Length; x++)
        {
            //Disable
            popupManage[x].SetActive(false);
        }
    }
    public void Lerp_To_Building_MiniPopup()
    {
        if (BuildTimer.queuedBuilding.Count >= 1)
        {
            for (int x = 0; x < popupManage.Length; x++)
            {
                if (popupManage[x].name.Contains(BuildTimer.queuedBuilding[0]))
                {
                    popupManage[x].SetActive(true);
                    break;
                }
            }
        }
    }
    public void DisplayMiniBuildingCompletePopUp(string buildingCompleted)
    {
        for (int x = 0; x < miniBuildingComplete.Length; x++)
        {
            if (miniBuildingComplete[x].name.Contains(buildingCompleted))
            {
                miniBuildingComplete[x].SetActive(true);
            }
        }
    }
    public void CheckWhetherToSlide()
    {
        for (int x = 0; x < BuildingManager.Instance.myHomeBuildings_.Count; x++)
        {
            if (BuildingManager.Instance.myHomeBuildings_[x].clicked == true)
            {
                //BuildingManager.Instance.myHomeBuildings_[x].SelectThisBuilding();
                Slide_Up_Bar(false);
            }
        }
    }
    public void Slide_Up_Bar(bool newValue)
    {
        anim.SetBool("Building_Slideup", newValue);
        chatBoxAnim.SetBool("Close", newValue);
    }
    // ========== MINI POPUP INFOS END========== \\

    // ======= ACCOUNT DETAILS PANEL ======== \\
    public bool AccountDetailsActive()
    {
        if (accountDetailPanel.activeInHierarchy)
            return true;
        return false;
    }
    // ======= ACCOUNT DETAILS PANEL END ======== \\

    void TimeFormatting(float duration)
    {
        //Used to format time to 00:00:00 for display in the UI
        int days = Mathf.FloorToInt(duration / 86400);
        int hours = Mathf.FloorToInt((duration - (days * 86400)) / 3600f);
        int minutes = Mathf.FloorToInt((duration - (days * 86400) - (hours * 3600)) / 60f);
        int seconds = Mathf.CeilToInt(duration - (days * 86400) - (hours * 3600) - (minutes * 60));
        if (duration > 0.1f)
        {
            newBuildingDuration = seconds + "s build time";
            if (duration >= 60f)
            {
                newBuildingDuration = minutes + "m " + seconds + "s build time";
                if (duration >= 3600f)
                {
                    newBuildingDuration = hours + "h " + minutes + "m " + seconds + "s build time";
                    if (duration >= 86400f)
                    {
                        newBuildingDuration = days + "d " + hours + "h " + minutes + "m " + seconds + "s build time";
                    }
                }
            }
        }
        //		newBuildingDuration = string.Format ("{0}h {1:00}:{2:00}", hours, minutes, seconds);
    }
    string EnergyRechargeTime(float time)
    {
        float minutes = Mathf.FloorToInt(time / 60f);
        float seconds = Mathf.FloorToInt(time - (minutes * 60));

        return string.Format("{0:00}:{1:00}", minutes, seconds);
    }
    public void Update_Building_LevelInfo()
    {
        //To Be Used when any building gets upgraded
        #region TROOP BUILDINGS
        // == BUNKER ==//
        meleeHallsLvDisplay.text = "Level " + Player.bunkerLv + " -> " + (Player.bunkerLv + 1);
        if (Player.gold < Player.bunkerCost)
        {
            bunker_GoldCost.color = Color.red;
            bunker_GoldCost.text = string.Format("{0:n0} /n (Requires {1:n0} more)", AmountConvert(Player.bunkerCost), AmountConvert(Player.bunkerCost - Player.gold));
        }
        else
        {
            bunker_GoldCost.color = Color.green;
            bunker_GoldCost.text = string.Format("{0:n0} / {1:n0}", AmountConvert(Player.gold), AmountConvert(Player.bunkerCost));
        }
        if (Player.bunkerLv >= (Player.castleLevel * 3 + 3))
        {
            bunker_Requirement_Display.text = "Please attain PLAYER Level " + (Player.castleLevel + 1);
            bunker_Requirement_Display.color = Color.red;
        }
        else
        {
            bunker_Requirement_Display.text = "Level requirement met";
            bunker_Requirement_Display.color = Color.green;
        }
        TimeFormatting(Player.bunker_Upgrade_Duration);
        bunker_LevelUpDuration.text = newBuildingDuration;
        if (Player.bunkerLv >= Player.barracksLvlCap)
        {
            bunker_GoldCost.color = Color.red;
            bunker_Requirement_Display.color = Color.red;
            bunker_LevelUpDuration.color = Color.red;
            bunker_GoldCost.text = string.Format("- Level Maxed -");
            bunker_Requirement_Display.text = string.Format("- Level Maxed -");
            bunker_LevelUpDuration.text = string.Format("- Level Maxed -");
            meleeHallsLvDisplay.text = "Level " + Player.barracksLvlCap + " (Maxed)";
        }

        // == RANGED == //
        rangedHallsLvDisplay.text = "Level " + Player.rangeLv + " -> " + (Player.rangeLv + 1);
        if (Player.gold < Player.rangeCost)
        {
            ranged_GoldCost.color = Color.red;
            ranged_GoldCost.text = string.Format("{0:n0} (Requires {1:n0} more)", AmountConvert(Player.rangeCost), AmountConvert(Player.rangeCost - Player.gold));
        }
        else
        {
            ranged_GoldCost.color = Color.green;
            ranged_GoldCost.text = string.Format("{0:n0} / {1:n0}", AmountConvert(Player.gold), AmountConvert(Player.rangeCost));
        }
        if (Player.rangeLv >= (Player.castleLevel * 3 + 3))
        {
            ranged_Requirement_Display.text = "Please attain PLAYER Level " + (Player.castleLevel + 1);
            ranged_Requirement_Display.color = Color.red;
        }
        else
        {
            ranged_Requirement_Display.text = "Level requirement met";
            ranged_Requirement_Display.color = Color.green;
        }
        TimeFormatting(Player.range_Upgrade_Duration);
        ranged_LevelUpDuration.text = newBuildingDuration;
        if (Player.rangeLv >= Player.barracksLvlCap)
        {
            ranged_GoldCost.color = Color.red;
            ranged_Requirement_Display.color = Color.red;
            ranged_LevelUpDuration.color = Color.red;
            ranged_GoldCost.text = string.Format("- Level Maxed -");
            ranged_Requirement_Display.text = string.Format("- Level Maxed -");
            ranged_LevelUpDuration.text = string.Format("- Level Maxed -");
            rangedHallsLvDisplay.text = "Level " + Player.barracksLvlCap + " (Maxed)";
        }

        // == GUILD == //
        wizardHallsLvDisplay.text = "Level " + Player.guildLv + " -> " + (Player.guildLv + 1);
        if (Player.gold < Player.guildCost)
        {
            wizard_GoldCost.color = Color.red;
            wizard_GoldCost.text = string.Format("{0:n0} (Requires {1:n0} more)", AmountConvert(Player.guildCost), AmountConvert(Player.guildCost - Player.gold));
        }
        else
        {
            wizard_GoldCost.color = Color.green;
            wizard_GoldCost.text = string.Format("{0:n0} / {1:n0}", AmountConvert(Player.guildCost), AmountConvert(Player.gold));
        }
        if (Player.guildLv >= (Player.castleLevel * 3 + 3))
        {
            wizard_Requirement_Display.text = "Please attain PLAYER level " + (Player.castleLevel + 1);
            wizard_Requirement_Display.color = Color.red;
        }
        else
        {
            wizard_Requirement_Display.text = "Level requirement met";
            wizard_Requirement_Display.color = Color.green;
        }
        TimeFormatting(Player.guild_Upgrade_Duration);
        wizard_LevelUpDuration.text = newBuildingDuration;
        if (Player.guildLv >= Player.barracksLvlCap)
        {
            wizard_GoldCost.color = Color.red;
            wizard_Requirement_Display.color = Color.red;
            wizard_LevelUpDuration.color = Color.red;
            wizard_GoldCost.text = string.Format("- Level Maxed -");
            wizard_Requirement_Display.text = string.Format("- Level Maxed -");
            wizard_LevelUpDuration.text = string.Format("- Level Maxed -");
            wizardHallsLvDisplay.text = "Level " + Player.barracksLvlCap + " (Maxed)";
        }
        #endregion

        #region RESOURCE BUILDING

        //		mills_Level_UP_Display.text = "Level " + Player.mills[Player.mill_That_Player_Last_Tapped_On] + " -> " + (Player.mills[Player.mill_That_Player_Last_Tapped_On] +1);
        //		lumberMill_Current_Gen_Rate.text = Player.millsRate[Player.mill_That_Player_Last_Tapped_On] + "/h";
        //		lumberMill_Next_Gen_Rate.text = ">   " + Mathf.FloorToInt(Player.millsRate[Player.mill_That_Player_Last_Tapped_On] + 5);
        //		lumberMill_Current_Cap_Value.text = Player.millsCap[Player.mill_That_Player_Last_Tapped_On] + "";
        //		lumberMill_Next_Cap_Value.text = ">   " + Mathf.FloorToInt(Player.millsCap[Player.mill_That_Player_Last_Tapped_On] + 50000);
        //		if (Player.gold < Player.millsCost[Player.mill_That_Player_Last_Tapped_On] )
        //		{
        //			mill_lumberCost.color = Color.red;
        //			mill_lumberCost.text = string.Format ("{0} (Requires {1} more)", Player.millsCost[Player.mill_That_Player_Last_Tapped_On],Player.millsCost[Player.mill_That_Player_Last_Tapped_On] - Player.gold);
        //		}
        //		else
        //		{
        //			mill_lumberCost.text = string.Format("{0} / {1}", Player.millsCost[Player.mill_That_Player_Last_Tapped_On],Player.gold);
        //			mill_lumberCost.color = Color.white;
        //		}
        //		if (Player.mills[Player.mill_That_Player_Last_Tapped_On] >= (Player.castleLevel*3))
        //		{
        //			mill_Requirement_Display.text = "Please attain castle level " + (Mathf.FloorToInt(Player.mills[Player.mill_That_Player_Last_Tapped_On]/3)+1);
        //			mill_Requirement_Display.color = Color.red;
        //		}
        //		else
        //		{
        //			mill_Requirement_Display.text =  "Level requirement met";
        //			mill_Requirement_Display.color = Color.white;
        //		}
        //		TimeFormatting (Player.mills_Upgrade_Duration[Player.mill_That_Player_Last_Tapped_On]);
        //		mill_LevelUpDuration.text = newBuildingDuration;

        //==-- GOLD MINE INFOS --==//
        mines_Level_UP_Display.text = "Level " + Player.mines[Player.mine_That_Player_Last_Tapped_On] + " -> " + (Player.mines[Player.mine_That_Player_Last_Tapped_On] + 1);
        gMines_Current_Gen_Rate.text = string.Format("{0:n0} /h", Player.minesRate[Player.mine_That_Player_Last_Tapped_On]);
        gMines_Current_Cap_Value.text = string.Format("{0:n0}", Player.minesCap[Player.mine_That_Player_Last_Tapped_On]);
        if (Player.mines[Player.mine_That_Player_Last_Tapped_On] == 0)
        {
            gMines_Current_Gen_Rate.text = "0/h";
            gMines_Current_Cap_Value.text = "0";
            gMines_Next_Gen_Rate.text = "> 1044/h";
            gMines_Next_Cap_Value.text = "> 8,384 ";
        }
        else
        {
            gMines_Next_Gen_Rate.text = "> " + string.Format("{0:n0} /h", Mathf.FloorToInt(Player.minesRate[Player.mine_That_Player_Last_Tapped_On] * 1.15f));
            gMines_Next_Cap_Value.text = "> " + string.Format("{0:n0}", Mathf.FloorToInt(Player.minesCap[Player.mine_That_Player_Last_Tapped_On] * 1.15f));
        }
        if (Player.gold < Player.minesCost[Player.mine_That_Player_Last_Tapped_On])
        {
            mines_GoldCost.text = string.Format("{0:n0} (Requires {1:n0} more)", AmountConvert(Player.minesCost[Player.mine_That_Player_Last_Tapped_On]),
                                                  AmountConvert(Player.minesCost[Player.mine_That_Player_Last_Tapped_On] - Player.gold));
            mines_GoldCost.color = Color.red;
        }
        else
        {
            mines_GoldCost.text = string.Format("{0:n0} / {1:n0}", AmountConvert(Player.minesCost[Player.mine_That_Player_Last_Tapped_On])
                                                , AmountConvert(Player.gold));
            mines_GoldCost.color = Color.green;
        }
        if (Player.mines[Player.mine_That_Player_Last_Tapped_On] >= (Player.castleLevel * 3 + 3))
        {
            mine_Requirement_Display.text = "Please attain PLAYER level " + (Player.castleLevel + 1);
            mine_Requirement_Display.color = Color.red;
        }
        else if (Player.mines[Player.mine_That_Player_Last_Tapped_On] == 0 && Player.minesBuilt >= Player.resourceBuildings_Unlock_Counter[Player.castleLevel])
        {
            mine_Requirement_Display.text = string.Format("Attain castle level {0} to unlock mine", Player.castleLevel + 1);
            mine_Requirement_Display.color = Color.red;
        }
        else
        {
            mine_Requirement_Display.text = "Level requirement met";
            mine_Requirement_Display.color = Color.green;
        }
        TimeFormatting(Player.mines_Upgrade_Duration[Player.mine_That_Player_Last_Tapped_On]);
        mine_LevelUpDuration.text = newBuildingDuration;
        if (Player.mines[Player.mine_That_Player_Last_Tapped_On] >= Player.rssBuildingLvCap)
        {
            mines_GoldCost.color = Color.red;
            mine_Requirement_Display.color = Color.red;
            mine_LevelUpDuration.color = Color.red;
            mines_GoldCost.text = string.Format("- Level Maxed -");
            mine_Requirement_Display.text = string.Format("- Level Maxed -");
            mine_LevelUpDuration.text = string.Format("- Level Maxed -");
            mines_Level_UP_Display.text = "Level " + Player.rssBuildingLvCap + " (Maxed)";
            gMines_Current_Gen_Rate.text = string.Format("{0:n0} /h", Player.minesRate[Player.mine_That_Player_Last_Tapped_On]);
            gMines_Current_Cap_Value.text = string.Format("{0:n0}", Player.minesCap[Player.mine_That_Player_Last_Tapped_On]);
            gMines_Next_Gen_Rate.text = "";
            gMines_Next_Cap_Value.text = "";
        }
        #endregion

        #region RECRUITMENTHALL
        recruitmentHall_Lvl_Display.text = "Level " + Player.recruitmentHall_Lvl + " -> " + (Player.recruitmentHall_Lvl + 1);
        groupCost_Display.text = Player.groupCost + "";
        new_GroupCost_Display.text = Player.groupCost + "";
        if (Player.gold < Player.recruitmentHall_Cost)
        {
            recruitmentHall_Cost_Display.color = Color.red;
            recruitmentHall_Cost_Display.text = string.Format("{0:n0} (Require {1:n0} more) ", Player.recruitmentHall_Cost, Player.recruitmentHall_Cost - Player.gold);
        }
        else
        {
            recruitmentHall_Cost_Display.color = Color.green;
            recruitmentHall_Cost_Display.text = string.Format("{0:n0} / {1:n0}", Player.recruitmentHall_Cost, Player.gold);
        }

        if (Player.recruitmentHall_Lvl >= (Player.castleLevel * 3 + 3))
        {
            recruitmentHall_Requirement_Display.text = "Please attain PLAYER level " + (Player.castleLevel + 1);
            recruitmentHall_Requirement_Display.color = Color.red;
        }
        else
        {
            recruitmentHall_Requirement_Display.text = "Level requirement met";
            recruitmentHall_Requirement_Display.color = Color.green;
        }
        TimeFormatting(Player.recruitment_Upgrade_Duration);
        current_Shuffle_Time_Display.text = string.Format("{0:0.00}s", Player.shufflingTime);
        new_Shuffle_Time_Display.text = string.Format("> {0:0.00}s", (Player.shufflingTime - 0.2f));
        recruitmentHall_BuildDuration.text = newBuildingDuration;
        recruitmentHall_BuildLimit_Display.text = Player.recruitment_Limit + "";
        recruitmentHall_New_BuildLimit_Display.text = string.Format("> {0}", (Player.recruitment_Limit + 5));

        if (Player.recruitmentHall_Lvl >= Player.recruitmentHallLvlCap)
        {
            recruitmentHall_Cost_Display.color = Color.red;
            recruitmentHall_Requirement_Display.color = Color.red;
            recruitmentHall_BuildDuration.color = Color.red;

            recruitmentHall_Cost_Display.text = string.Format("- Level Maxed -");
            recruitmentHall_Requirement_Display.text = string.Format("- Level Maxed -");
            recruitmentHall_BuildDuration.text = string.Format("- Level Maxed -");

            recruitmentHall_Lvl_Display.text = "Level " + Player.recruitmentHallLvlCap + " (Maxed)";

            new_Shuffle_Time_Display.text = "";
        }

        #endregion

        #region TOWN HALL
        townHallLevelDisplay.text = "Level " + Player.townHallLevel + " -> " + (Player.townHallLevel + 1);
        if (Player.gold < Player.townHallCost)
        {
            townHall_Cost_Display.text = string.Format("{0:n0} (Require {1:n0} more) ", AmountConvert(Player.townHallCost), AmountConvert(Player.townHallCost - Player.gold));
            townHall_Cost_Display.color = Color.red;
        }
        else
        {
            townHall_Cost_Display.text = string.Format("{0:n0} / {1:n0}", AmountConvert(Player.townHallCost), AmountConvert(Player.gold));
            townHall_Cost_Display.color = Color.green; ;
        }
        #region Town Hall Info
        old_Flags_Amount.text = Player.castleMaxHealth + "";
        if (Player.castleLevel == 2)
            new_Flags_Amount.text = "> 4";
        else if (Player.castleLevel == 4)
            new_Flags_Amount.text = "> 5";
        else
            new_Flags_Amount.text = "- " + Player.castleMaxHealth;

        old_Deck_Amount.text = Player.decksAvailable + "";
        if (Player.castleLevel == 6)
            new_Deck_Amount.text = "2";
        else
            new_Deck_Amount.text = "- " + Player.decksAvailable;

        TimeFormatting(Player.townHall_Upgrade_Duration);
        townHall_Upgrade_Duration_Display.text = newBuildingDuration;

        #endregion

        if (Player.townHallLevel >= Player.townHallLevelCap)
        {
            townHallLevelDisplay.text = "Level " + Player.townHallLevelCap + " (Maxed)";

            townHall_Cost_Display.color = Color.red;
            townHall_Upgrade_Duration_Display.color = Color.red;


            townHall_Cost_Display.text = string.Format("- Level Maxed -");
            townHall_Upgrade_Duration_Display.text = string.Format("- Level Maxed -");

            new_Flags_Amount.text = "";
        }
        #endregion

        #region TREASURY
        treasury_Lvl_Display.text = "Level " + Player.treasuryLvl + " -> " + (Player.treasuryLvl + 1);
        if (Player.gold < Player.treasuryCost)
        {
            treasury_Cost_Display.color = Color.red;
            treasury_Cost_Display.text = string.Format("{0:n0} (Requires {1:n0} more)", AmountConvert(Player.treasuryCost), AmountConvert((Player.treasuryCost - Player.gold)));
        }
        else
        {
            treasury_Cost_Display.color = Color.green;
            treasury_Cost_Display.text = string.Format("{0:n0} / {1:n0}", AmountConvert(Player.treasuryCost), AmountConvert(Player.gold));
        }
        TimeFormatting(Player.treasury_Upgrade_Duration);
        treasury_Upgrade_Duration_Display.text = newBuildingDuration;
        treasury_Current_Storage_Value_Display.text = string.Format("{0:n0}", Player.treasury_Storage_Cap);
        treasury_New_Storage_Value_Display.text = string.Format("> {0:n0}", Player.treasury_Storage_Cap * Player.treasury_Storage_Multiplier);
        treasury_Current_Protection_Value_Display.text = string.Format("{0:0.##}%", Player.treasury_Protection_Value);
        treasury_New_Protection_Value_Display.text = string.Format("> {0:0.##}%", Player.treasury_Protection_Value + 0.43f);
        if (Player.treasuryLvl >= Player.castleLevel * 3 + 3)
        {
            treasury_Requirement_Display.text = "Please attain PLAYER LEVEL " + (Player.castleLevel + 1);
            treasury_Requirement_Display.color = Color.red;
        }
        else
        {
            treasury_Requirement_Display.text = "Level requirement met";
            treasury_Requirement_Display.color = Color.green;
        }
        if (Player.treasuryLvl >= Player.treasuryLvlCap)
        {
            treasury_Cost_Display.color = Color.red;
            treasury_Requirement_Display.color = Color.red;
            treasury_Upgrade_Duration_Display.color = Color.red;

            treasury_Cost_Display.text = string.Format("- Level Maxed -");
            treasury_Requirement_Display.text = string.Format("- Level Maxed -");
            treasury_Upgrade_Duration_Display.text = string.Format("- Level Maxed -");
            treasury_New_Storage_Value_Display.text = "";

            treasury_Lvl_Display.text = "Level " + Player.treasuryLvl + " (Maxed)";

            new_Shuffle_Time_Display.text = "";
        }
        #endregion

        #region FORGE
        if (Player.forgeLevel < Player.forgeCap)
        {
            forge_Level.text = "Level " + Player.forgeLevel + " -> " + (Player.forgeLevel + 1);
            support_Current_Level.text = "" + Player.forgeLevel;
            support_New_Level.text = "-> " + (Player.forgeLevel + 1);
            if (Player.gold < Player.forge_UpgradeCost)
            {
                forge_Cost.color = Color.red;
                forge_Cost.text = string.Format("{0:n0} (Requires {1:n0} more)", AmountConvert(Player.forge_UpgradeCost), AmountConvert(Player.forge_UpgradeCost - Player.gold));
            }
            else
            {
                forge_Cost.color = Color.green;
                forge_Cost.text = string.Format("{0} / {1}", AmountConvert(Player.forge_UpgradeCost), AmountConvert(Player.gold));
            }
            if (Player.castleLevel < (Player.forgeLevel * 2 + 2))
            {
                forge_Requirement_Display.color = Color.red;
                forge_Requirement_Display.text = "Please attain castle level " + (Player.forgeLevel * 2 + 2);
            }
            else
            {
                forge_Requirement_Display.color = Color.green;
                forge_Requirement_Display.text = "Level requirement met";
            }
            TimeFormatting(Player.forge_UpgradeDuration);
            forge_Duration_Display.text = "" + newBuildingDuration;
        }
        else if (Player.forgeLevel >= Player.forgeCap)
        {
            forge_Level.text = "Level 3 (Max Level)";
            support_Current_Level.text = "3";
            support_New_Level.text = "-";
            forge_Cost.color = Color.red;
            forge_Cost.text = string.Format("- Level Maxed -");
            forge_Requirement_Display.color = Color.red;
            forge_Requirement_Display.text = string.Format("- Level Maxed -");
            forge_Duration_Display.color = Color.red;
            forge_Duration_Display.text = string.Format("- Level Maxed -");
        }
        #endregion

        #region TAVERN
        UpdateTavernInfos();
        #endregion
    }

    public void Update_PVP_Info()
    {
        PlayerFlags();                      // to display hero flags
        PlayerHeroSlots();              // to unlock hero slots
        PlayerShufflingTime();      // to calculate shuffle time
                                    //		Check_SUnits_Preferences();
    }

    public void RSS_Building_Upgrade_Pops(int generatorNo, string generatorType)
    {
        //Pop up display for RSS building
        if (generatorType == "mine")
        {
            mines_Pop_Up.SetActive(true);
            mines_Level_UP_Display.text = Player.mines[generatorNo] + " -> " + (Player.mines[generatorNo] + 1);
        }
    }

    public void Allow_Upgrade()
    {
        //scroll through all the upgrade buttons
        for (int x = 0; x < upgradeButton.Length; x++)
        {
            //and search for the only active in heirarchy one
            if (upgradeButton[x].gameObject.activeInHierarchy)
            {
                upgradeButton[x].onClick.Invoke();
                break;
            }
        }
    }

    public void DisplayTimerBar(GameObject timerBar)
    {
        // Displays the timer progress bar when player upgrades a structure
        timerBar.SetActive(true);
        // Disable the upgrade buttons for all buildings
        // So the buildings cannot be upgraded simultaneously
        // Check if players upgraded building or build troops
        if (BuildTimer.buildingTimer.Count >= 1)
        {
            Disable_Current_Upgrade_Buttons();
        }
    }

    public void Disable_Current_Upgrade_Buttons()
    {
        for (int x = 0; x < upgradeButton.Length; x++)
        {
            upgradeButton[x].interactable = false;
        }
    }
    public void Enable_All_Upgrade_Buttons()
    {
        for (int x = 0; x < upgradeButton.Length; x++)
        {
            upgradeButton[x].interactable = true;
        }
    }
    public void Display_Building_Complete_Button(string buildingType)
    {
        for (int x = 0; x < buildingPopUps.Length; x++)
        {
            if (buildingPopUps[x].name.Contains(buildingType))
            {
                buildingPopUps[x].GetComponent<ProgressBarHandler>().OnBuildingCompleted(buildingType);
                break;
            }
        }
        DisplayMiniBuildingCompletePopUp(buildingType);
    }
    public void Hide_All_Building_Popups()
    {
        for (int x = 0; x < buildingPopUps.Length; x++)
        {
            buildingPopUps[x].SetActive(false);
        }
    }
    public void Display_Upgrading_Now_Info()
    {
        for (int x = 0; x < buildingPopUps.Length; x++)
        {
            if (buildingPopUps[x].activeInHierarchy)
            {
                buildingPopUps[x].GetComponent<ProgressBarHandler>().Inform_Upgrade_In_Progress();
                break;
            }
        }
    }
    public void Disable_Further_Upgrade()
    {
        for (int x = 0; x < buildingPopUps.Length; x++)
        {
            if (buildingPopUps[x].activeInHierarchy)
            {
                buildingPopUps[x].GetComponent<ProgressBarHandler>().Check_If_Building_Upgrade_Available();
                break;
            }
        }
    }

    public void Hide_Current_Progress_Bar()
    {
        for (int x = 0; x < progressBar.Length; x++)
        {
            if (progressBar[x].activeInHierarchy)
            {
                progressBar[x].SetActive(false);
            }
        }
    }

    public void Display_Speed_Up_Cost(int cost, string type)
    {
        speedUpWith.text = string.Format("Spend diamonds to complete {0} immediately?", type);
        speedUp_Cost_Text_Display.text = cost + "";
        if (Player.diamond > cost)
        {
            speedUp_Cost_Text_Display.color = Color.green;
            speed_Up_with_Diamond_Button.interactable = true;
        }
        else if (Player.diamond < cost)
        {
            speedUp_Cost_Text_Display.color = Color.red;
            speed_Up_with_Diamond_Button.interactable = false;
        }
    }

    public void Enable_SpeedUp_Buttons()
    {
        for (int x = 0; x < hastenBuild.Length; x++)
        {
            hastenBuild[x].interactable = true;
        }
    }
    public void Disable_SpeedUp_Buttons()
    {
        for (int x = 0; x < hastenBuild.Length; x++)
        {
            hastenBuild[x].interactable = false;
        }
    }

    public void Hide_SpeedUp_PopUp()
    {
        // disable speedup popups if there is nothing to hasten anymore
        speedUp_Popup.SetActive(false);
    }

    public void Notification_Pop_Up(string criteria_Used, int typeToBeDisplayed)
    {
        //1 = Not enough resources; ie Not enough lumber
        //2 = Attain Castle level
        //3 = Storage full
        //4 = Upgrade building
        //5 = Building at max level already
        if (typeToBeDisplayed == 1)
            criteria_Not_Met_Text_Display.text = "Insufficient " + criteria_Used;
        else if (typeToBeDisplayed == 2)
            criteria_Not_Met_Text_Display.text = "Attain PLAYER LEVEL " + (Player.castleLevel + 1);
        else if (typeToBeDisplayed == 3)
            criteria_Not_Met_Text_Display.text = "Storage full";
        else if (typeToBeDisplayed == 5)
            criteria_Not_Met_Text_Display.text = criteria_Used + " already at max level.";
        else if (typeToBeDisplayed == 6)
            criteria_Not_Met_Text_Display.text = criteria_Used + "";

        criteria_Not_Met_Pop_Up.SetActive(true);
    }
    public void Not_Enough_Assign_Points()
    {
        SetPopup("Insufficient Assignment Points");
    }
    public void Require_At_Least_One_Troop()
    {
        SetPopup("At least one troop needs to be assigned");
    }
    public void HeroLimitReached()
    {
        SetPopup("A Hero can only be assigned twice");
    }
    void SetPopup(string message)
    {
        troopAssignmentError.SetActive(true);
        troopAssignErrorMessage.text = message;
    }

    public void HideUIBuildings()
    {
        for (int x = 0; x < UIBuildings_Pop_Up.childCount; x++)
        {
            if (UIBuildings_Pop_Up.GetChild(x).gameObject.activeInHierarchy)
            {
                UIBuildings_Pop_Up.GetChild(x).gameObject.SetActive(false);
                break;
            }
        }
    }
    public void Castle_Level_Up_Popup()
    {
        castleLvUp_Pop_Up.SetActive(true);
        castleLv_Attained_Text.text = "You have reached PLAYER LEVEL" + Player.castleLevel + "!";
    }

    public void NotifyStructureCompletion(string buildingCompleted, int structLv)
    {
        ProgressNotification(ParseName(buildingCompleted), structLv);
    }
    string ParseName(string oldbuildingName)
    {
        if (oldbuildingName.ToLower() == "townhall")
            return "Castle";
        if (oldbuildingName.ToLower() == "range")
            return "Archer Training Ground";
        if (oldbuildingName.ToLower() == "guild")
            return "Mage Training Ground";
        if (oldbuildingName.ToLower() == "bunker")
            return "Knight Training Ground";
        if (oldbuildingName.ToLower() == "recruitmenthall")
            return "Preperation Hall";
        return oldbuildingName;
    }
    public void ProgressNotification(string heroName, bool isNew)
    {
        if (!isNew)
            itemToDisplayText.text = string.Format("You got a {0} card", heroName);
        else
            itemToDisplayText.text = string.Format("You've obtained {0}", heroName);
        progressDisplayPanel.SetActive(true);
    }
    public void ProgressNotification(string itemLeveledUp, int currentLv)
    {
        itemToDisplayText.text = string.Format("{0} is now Lv{1}", itemLeveledUp.ToUpper(), currentLv);
        progressDisplayPanel.SetActive(true);
    }

    // ---- ARMY INFO ---- //

    // ---- 2ND DECK INFO ---- //
    public void AllowSecondDeck()
    {
        if (Player.castleLevel >= 7)
        {
            secondDeck_Toggle.interactable = true;
        }
    }
    public void ToggleDeckSelection(bool newValue)
    {
        //toggle for if player has second deck or not
        if (newValue == true)
            Player.secondDeckEnabled = true;
        else
            Player.secondDeckEnabled = false;
    }
    // ---- HERO INFO ----//
    public void SendMySelectedHeroSlot(int heroSlot)
    {
        Player.selectedHeroSlot = heroSlot;
    }
    public void SendMyHeroType(string heroType)
    {
        Player.heroType[Player.selectedHeroSlot] = heroType;
    }
    public void SendMyHeroNumber(int heroNumber)
    {
        Player.heroNo[Player.selectedHeroSlot] = heroNumber;
    }
    public void DisplayHeroName(bool newValue)
    {
        if (newValue == true)
        {
            Player.Instance.equippedHero[Player.selectedHeroSlot].text = Player.heroType[Player.selectedHeroSlot];
            for (int x = 0; x < UnitManager.Instance.heroInfomation.Count; x++)
            {
                if (UnitManager.Instance.heroInfomation[x].heroName == Player.heroType[Player.selectedHeroSlot])
                {
                    Player.heroNo[Player.selectedHeroSlot] = UnitManager.Instance.heroInfomation[x].heroNo;
                    heroSlotImage[Player.selectedHeroSlot].sprite = UnitManager.Instance.heroInfomation[x].heroImage;
                }
            }
        }
        else if (newValue == false)
        {
            Player.Instance.equippedHero[Player.selectedHeroSlot].text = "No Hero Selected";
            Player.heroType[Player.selectedHeroSlot] = null;
            Player.heroNo[Player.selectedHeroSlot] = 200;
        }
    }

    public void Update_Energy_Pack()
    {
        remainingEnergyPack.text = string.Format("Refill ({0} left)", Player.energyPacks);
        if (Player.energyPacks > 0)
            useEnergyPackButton.interactable = true;
        else
            useEnergyPackButton.interactable = false;
        insufficientEnergyPopup.SetActive(true);
    }
    public void Refill_Energy()
    {
        Player.energyPacks -= 1;
        Player.Instance.RechargeEnergy(8);
        insufficientEnergyButton.SetActive(false);
        insufficientEnergyPopup.SetActive(false);
        Player.currentEnergyRechargeTime = Player.energyRechargeTime;

        Update_All();
    }

    public void PackInfo(string pack)
    {
        purchasedContent.text = pack;
    }
    public void PackInfo2(string info)
    {
        purchasedContent2.text = info;
    }
    public void SetFontSize(int size)
    {
        purchasedContent.fontSize = size;
    }

    // === Loading Screen == //
    public void DisableLoading()
    {
        // The screen for loading when pulling enemy info
        loadingScreen.SetActive(false);
    }
    public void Enable_Battle_Loading_Screen()
    {
        battleLoadingScreen.SetActive(true);
    }
    public void Disable_Battle_Loading_Screen()
    {
        battleLoadingScreen.SetActive(false);
    }

    // ---- Player Info ----//
    public void IncreaseGroup()
    {
        Player.Instance.CheckGroupLimit();
        if (Player.equippedGroups < Player.groupLimit)
            Player.equippedGroups += 1;
        PlayerGroups();
    }
    public void DecreaseGroup()
    {
        if (Player.equippedGroups > 2)
            Player.equippedGroups -= 1;
        PlayerGroups();
    }
    public void IncreaseCastleLevel()
    {
        Player.castleLevel += 1;
        if (Player.castleLevel > 10)
        {
            Player.castleLevel = 10;
        }
        UpdatePlayerInfo();
        if (Player.accountType == "Guest")
            Player.SavePrefs();
    }
    public void DecreaseCastleLevel()
    {
        Player.castleLevel -= 1;
        if (Player.castleLevel < 1)
        {
            Player.castleLevel = 1;
        }
        UpdatePlayerInfo();
        if (Player.accountType == "Guest")
            Player.SavePrefs();
    }

    void PlayerHeroSlots()
    {
        if (Player.castleLevel >= 0)
        {
            for (int x = 0; x < heroSlots.Length; x++)
            {
                heroSlots[x].interactable = false;
            }

            if (Player.castleLevel >= 2)
            {
                heroSlots[0].interactable = true;

                if (Player.castleLevel >= 5)
                {
                    heroSlots[1].interactable = true;

                    if (Player.castleLevel >= 7)
                    {
                        heroSlots[2].interactable = true;
                    }
                }
            }
        }
    }
    void PlayerShufflingTime()
    {
        pShuffleTime.text = string.Format("{0:0.00}", Player.shufflingTime);
    }
    void PlayerGroups()
    {
        // For Unlocking the troop slots as the player levels up
        pGroups.text = Player.equippedGroups.ToString();
    }
    void PlayerFlags()
    {
        if (Player.castleLevel <= 10)
        {
            pFlags.text = "5";

            if (Player.castleLevel <= 4)
            {
                pFlags.text = "4";

                if (Player.castleLevel <= 2)
                {
                    pFlags.text = "3";
                }
            }
        }
    }


    // +=== VALUE CONVERTERS ==== \\
    public string AmountConvert(long costToConvert)
    {
        if (costToConvert < 1000)
            return costToConvert.ToString();
        //if (costToConvert < 1000000)
        return kConvert(costToConvert);
        //if (costToConvert < 1000000000)
        //return mConvert(costToConvert);
        //return bConvert(costToConvert);
    }
    string kConvert(long value)
    {
        return string.Format("{0:n0} K", value / 1000);
    }
    string mConvert(long value)
    {
        return string.Format("{0:n0} M", value / 1000000);
    }
    string bConvert(long value)
    {
        return string.Format("{0:n0} B", value / 1000000000);
    }

    //===================================//

    //BATTLE SCENE//

    //===================================//
    void DisplayName()
    {
        nameDisplay.text = Player.playerName;
    }

    public void CheckCastleLevel()
    {
        if (Player.castleLevel <= 10)
        {
            Player.castleHealth = 5;
            Player.castleMaxHealth = 5;

            if (Player.castleLevel <= 4)
            {
                Player.castleHealth -= 1;
                Player.castleMaxHealth -= 1;

                if (Player.castleLevel <= 2)
                {
                    Player.castleHealth -= 1;
                    Player.castleMaxHealth -= 1;
                }
            }
        }
    }

    public void AssignHero()
    {
        for (int x = 0; x < Player.Instance.equippedHero.Length; x++)
        {
            if (Player.heroType[x] != null)
            {
                //for every equipped hero
                for (int y = 0; y < heroCards.Length; y++)
                {
                    if (heroCards[y].name.Contains(Player.heroType[x]))
                    {
                        heroButton[x].SetActive(true);
                        heroCards[y].transform.position = heroButton[x].transform.position;
                        heroButton[x] = heroCards[y];
                        heroButton[x].GetComponent<UnitButton>().InitialLockHeroButton();
                    }
                }
                //				Player.Instance.equippedHero[x].text = Player.heroType[x];
                //				heroButton[x].GetComponent<UnitButton>().unitCode = Player.heroType[x];
            }
        }
        GlobalCooldown_HeroButton_Disasble();
    }

    public void GlobalCooldown_HeroButton_Disasble()
    {
        for (int x = 0; x < heroButton.Length; x++)
        {
            if (Player.heroType[x] != null)
            {
                heroButton[x].GetComponent<Button>().interactable = false;
                heroButton[x].GetComponent<UnitButton>().RevokeSpawnning(SpawnBlockerBarManager.Instance.cdTime + 0.05f);
            }
        }
    }
    public void LockAllHeroCards()
    {
        for (int x = 0; x < heroButton.Length; x++)
        {
            if (Player.heroType[x] != null)
            {
                heroButton[x].GetComponent<UnitButton>().SpawnBoolToTrue();
            }
        }
    }

    public void SUnitsNotify()
    {
        switch (DeploySupportUnits.sUnitSelected.unitType)
        {
            case SUnitType.SUnit.Mines:
                sUnitsNotify.text = "Mines Deployed";
                break;
            case SUnitType.SUnit.Ballista:
                sUnitsNotify.text = "Ballista Fired";
                break;
            case SUnitType.SUnit.Flames:
                sUnitsNotify.text = "Dragon Summoned";
                break;
        }
        sUnitsNotify.gameObject.SetActive(true);
        Invoke("DisableSUnitsNotify", 1.5f);
    }
    public void DisableSUnitsNotify()
    {
        sUnitsNotify.gameObject.SetActive(false);
    }
    public void Display_Soul_Charges()
    {
        for (int i = 0; i < Player.soulCharges; i++)
        {
            soul_Charges_Display[i].SetActive(true);
        }
    }
    public void Hide_Soul_Charges()
    {
        //Set inactive the last active soul
        soul_Charges_Display[Player.soulCharges - 1].SetActive(false);
    }

    public void FakeCardsLaying()
    {
        if (Player.deck1Cards < 4)
        {
            for (int x = 0; x < Player.deck1Cards - 2; x++)
            {
                fakeCardBack1.GetChild(x).gameObject.SetActive(true);
            }
        }
        if (Player.secondDeckEnabled)
        {
            for (int x = 0; x < Player.deck2Cards - 2; x++)
            {
                fakeCardBack2.GetChild(x).gameObject.SetActive(true);
            }
        }
    }
    public void DeckThinning()
    {
        if (Player.deck1Cards < 5)
        {
            for (int i = 0; i < fakeCardBack1.childCount - 1; i++)
            {
                if (fakeCardBack1.GetChild(i).gameObject.activeInHierarchy == true)
                {
                    fakeCardBack1.GetChild(i).gameObject.SetActive(false);
                    break;
                }
            }
        }
        //Second deck area
        if (Player.secondDeckEnabled)
        {
            if (Player.deck2Cards < 3)
            {
                fakeCardBack2.GetChild(fakeCardBack2.childCount - 1).gameObject.SetActive(false);
            }
        }
    }

    public void ChangeBlockerPosition()
    {
        if (Player.secondDeckEnabled != true)
        {
            if (Player.deck1Cards == 0)
            {
                deck1UnitBlockingDial.position = deck1lastCardPos.position;
            }
            else
            {
                deck1UnitBlockingDial.position = deck1CurrentCardPos.position;
            }
        }
        else
        {
            if (Player.deck1Cards == 0)
            {
                deck1UnitBlockingDial.position = deck1lastCardPos.position;
            }
            else
            {
                deck1UnitBlockingDial.position = deck1CurrentCardPos.position;
            }
            if (Player.deck2Cards == 0)
            {
                deck2UnitBlockingDial.position = deck2lastCardPos.position;
            }
            else
            {
                deck2UnitBlockingDial.position = deck2CurrentCardPos.position;
            }
        }
    }

    public void Player_Shuffling_Timer_Display(float timeLeft)
    {
        timerDisplay.text = string.Format("{0:0.00}", timeLeft);
    }

    public void DisplayFlag()
    {
        for (int i = 0; i < Player.castleMaxHealth; i++)
        {
            if (i <= Player.castleHealth && !playerFlags[i].activeInHierarchy)
            {
                playerFlags[i].SetActive(true);
                if (i == Player.castleHealth - 1)
                {
                    break;
                }
            }
        }
    }
    public void WaitForHideFlag()
    {
        Invoke("HideFlag", 0.5f);
    }
    public void HideFlag()
    {
        playerFlags[Player.castleHealth].SetActive(false);
        damageFlash.SetActive(true);
        Invoke("HideDamageFlash", 1);
    }
    public void HideEnemyFlag()
    {
        if (Enemy.Instance.castleHealth >= 0)
            Enemy.Instance.flags[Enemy.Instance.castleHealth].gameObject.SetActive(false);
        hitFlash.SetActive(true);
        Invoke("HideHitFlash", 2);
    }
    void HideDamageFlash()
    {
        damageFlash.SetActive(false);
    }
    void HideHitFlash()
    {
        hitFlash.SetActive(false);
    }

    public void UpdateRemainingCards()
    {
        //If player only has 1 deck equipped
        myCardsInfo[0].text = Player.deck1Cards + "/" + Player.deck1InitialCards;
        //if player has 2 decks equipped
        if (Player.secondDeckEnabled == true)
        {
            myCardsInfo[1].text = Player.deck2Cards + "/" + Player.deck2InitialCards;
        }
    }
    public void DisplayDeckBack()
    {
        deckBack1.SetActive(true);
        if (Player.secondDeckEnabled)
        {
            deckBack2.SetActive(true);
        }
    }
    public void HideDeckBack()
    {
        deckBack1.SetActive(false);
        if (Player.secondDeckEnabled)
        {
            deckBack2.SetActive(false);
        }
    }

    public void ToggleBackToHomeSceneButton()
    {
        gameOverBackButton.SetActive(true);
        victoryBackButton.SetActive(true);
    }

    public void Display_Obtained_Rewards(int diamondAmt, int goldAmt, int t3, int t15, int t30, int t60)
    {
        receivedDiamond.text = diamondAmt + " DIAMONDS";
        receivedGold.text = string.Format("{0:n0} GOLD", goldAmt);

        receivedT3.text = t3 + "x 3m timers";
        receivedT15.text = t15 + "x 15m timers";
        receivedT30.text = t30 + "x 30m timers";
        receivedT60.text += t60 + "x 60m timers";


        if (diamondAmt >= 1)
            recievedDiamond_Display.SetActive(true);
        else
            recievedDiamond_Display.SetActive(false);
        if (t3 >= 1)
            t3display.SetActive(true);
        else
            t3display.SetActive(false);

        if (t15 >= 1)
            t15display.SetActive(true);
        else
            t15display.SetActive(false);

        if (t30 >= 1)
            t30display.SetActive(true);
        else
            t30display.SetActive(false);

        if (t60 >= 1)
            t60display.SetActive(true);
        else
            t60display.SetActive(false);

        GameManager.Instance.victoryScreen.SetActive(true);
    }
}
