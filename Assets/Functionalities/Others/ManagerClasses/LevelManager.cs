﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour 
{
    public delegate void SUnitSceneLoaded();
    public static event SUnitSceneLoaded ChallengeReady;

	#region Singleton

	private static LevelManager _instance;
	public static LevelManager Instance
	{
		get
		{
			if (_instance == null) 
			{
				GameObject levelManager = new GameObject ("LevelManager");
				levelManager.AddComponent<LevelManager> ();
			}
			return _instance;
		}
	}
		
	#endregion

	public GameObject loadingBar;
	public RectTransform loadingProgressBar;
	public Text loadingText;
	public GameObject tapToContinue;
	AsyncOperation loadLevel;


    [Space(14)]
    [Header("SUnits Area")]
    public GameObject sUnitLoadingScreen;
    public Text sUnitLoadingScreenText;
    public bool loadingSUnitScene;
    bool unnotified = true;
    public Button loadSUnitChallenge;


    bool readyForWar;
	float timer;

	void Awake()
	{
		if (_instance == null) 
		{
			_instance = this;
		}
        if (SceneManager.GetActiveScene().name.Contains("Splash"))
        {
            Invoke("LoginScene", 6.5f);
        }
	}

    void Start()
    {
        loadingSUnitScene = false;
        if (SceneManager.GetActiveScene().name.Contains("Home"))
            loadSUnitChallenge.onClick.AddListener(AllowSceneActivation);
    }

    public void SplashScreen()
    {
        SceneManager.LoadScene("0_Splash"); 
    }

	public void LoginScene()
	{
		StartCoroutine("LoadAsyncLoginScreen");
        Player.Instance.ResetTotalGamePlayTime();
	}

	public void HomeScene()
	{
        // When the player returns home
		StartCoroutine ("LoadAsyncHomeScreen");
        Time.timeScale = 1;
	}

    public void BattleScene()
	{
        // When Player goes into the battle scene
        if (Player.playerCurrentEnergy >= 1)
        {
            Player.Instance.UseEnergy(1);
            TimeMaster.Instance.SaveCountdownTimers();
            //if (Player.accountType == "Registered")
                //DataSender.Instance.Store_LastAmount_Of_Value_In_RSS_Generator();

            StartCoroutine("LoadAsyncBattleScene");

            UIManager.Instance.Enable_Battle_Loading_Screen();
        }
        else
            UIManager.Instance.Notification_Pop_Up("Energy", 1);
	}

    public void SUnitFarm(SUnitType.SUnit sUnitType)
    {
        SUnitChallenge.sUnitToAdd = sUnitType;

        if (sUnitType == SUnitType.SUnit.Mines)
        {
            loadingSUnitScene = true;
            StartCoroutine("LoadMineScene");
            sUnitLoadingScreenText.text = string.Format("Approaching {0} area", sUnitType.ToString());
            sUnitLoadingScreen.SetActive(true);
        }
        if (sUnitType == SUnitType.SUnit.Ballista)
        {
            loadingSUnitScene = true;
            StartCoroutine("LoadBallistaScene");
            sUnitLoadingScreenText.text = string.Format("Approaching {0} area", sUnitType.ToString());
            sUnitLoadingScreen.SetActive(true);
        }
    }


    public void StoreTotalGameTime()
    {
        Player.Instance.StoreTotalGamePlayTime();
    }

	IEnumerator LoadAsyncLoginScreen()
	{
		loadLevel = SceneManager.LoadSceneAsync ("1_Login");
		yield return loadLevel;
	}

	IEnumerator LoadAsyncHomeScreen()
	{
		loadLevel = SceneManager.LoadSceneAsync ("2_Home");
		yield return loadLevel;
	}

	IEnumerator LoadAsyncBattleScene()
	{
		loadLevel = SceneManager.LoadSceneAsync ("3_Battle");
		loadLevel.allowSceneActivation = false;
		yield return loadLevel;
	}

    IEnumerator LoadMineScene()
    {
        loadLevel = SceneManager.LoadSceneAsync("4_MinesFarm");
        loadLevel.allowSceneActivation = false;
        yield return loadLevel;
    }
    IEnumerator LoadBallistaScene()
    {
        loadLevel = SceneManager.LoadSceneAsync("6_BallistaFarm");
        loadLevel.allowSceneActivation = false;
        yield return loadLevel;
    }

    public void PvP()
	{
		SceneManager.LoadSceneAsync ("MockupPvP");
	}

	public void TapToContinue()
	{
        Player.my_Matches += 1;
        AllowSceneActivation();

    }

    void AllowSceneActivation()
    {
        loadLevel.allowSceneActivation = true;
    }

	void CheckProgressBar()
	{
		if(SceneManager.GetActiveScene().name.Contains("Home"))
		{
			if (loadLevel != null)
			{
				if (loadLevel.progress == 0.9f)
				{
					Invoke("ToWar",1.5f);
				}
			}
		}
	}
	void CheckText()
	{
		if(SceneManager.GetActiveScene().name.Contains("Home"))
		{
            if (GameSparksManager.allObtained == true)
            {
                if (loadLevel != null && !readyForWar)
                {
                    timer += Time.deltaTime;
                    if (timer <= 0.3f)
                    {
                        loadingText.text = "Rallying Troops...";
                    }
                    else if (timer <= 0.8f)
                    {
                        loadingText.text = "Building Spells...";
                    }
                    else if (timer <= 1.4f)
                    {
                        loadingText.text = "Getting in position...";
                    }
                    else if (timer <= 1.9f)
                    {
                        loadingText.text = "Ready for war..";
                    }
                }
            }
		}
	}

	void ToWar()
	{
		loadingText.text = "Battle";
		loadingText.color = Color.red;
		tapToContinue.SetActive(true);
		loadingBar.SetActive(false);
		readyForWar = true;
	}

	void Update()
	{
		CheckProgressBar();
		CheckText();

        if (loadingSUnitScene == true)
        {
            if (loadLevel.progress == 0.9f & unnotified)
            {
                loadSUnitChallenge.interactable = true;
                unnotified = false;
            }
        }
	}
}
