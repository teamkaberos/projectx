﻿using UnityEngine;

namespace ValueConverter 
{
    public class ResourceConvert : MonoBehaviour
    {
        // =========== TIME CONVERSION ============ \\
        public void ConvertUnix(long unixValue)
        {
            int days = Mathf.FloorToInt(unixValue / 86400);
            int hours = Mathf.FloorToInt(unixValue - DaySeconds(days) / 3600);
            int minutes = Mathf.FloorToInt(unixValue - DaySeconds(days) - HourSeconds(hours) / 60);
            int seconds = Mathf.FloorToInt(unixValue - DaySeconds(days) - HourSeconds(hours) - MinuteSeconds(minutes));
        }
        int DaySeconds(int days)
        {
            return days * 86400;
        }
        int HourSeconds(int hours)
        {
            return hours * 3600;
        }
        int MinuteSeconds(int minutes)
        {
            return minutes * 60;
        }


        // =========== COST CONVERSION ============= \\
        public string AmountConvert(long costToConvert)
        {
            if (costToConvert < 1000)
                return costToConvert.ToString();
            if (costToConvert < 1000000)
                return kConvert(costToConvert);
            if (costToConvert < 1000000000)
                return mConvert(costToConvert);
            return bConvert(costToConvert);
        }
        string kConvert(long value)
        {
            return string.Format("{0:00} K", value / 1000);
        }
        string mConvert(long value)
        {
            return string.Format("{0:00} M", value / 1000000);
        }
        string bConvert(long value)
        {
            return string.Format("{0:00} B", value / 1000000000);
        }
    }

    public class GuildRanking :MonoBehaviour
    {
        public string RankType(int rankInt)
        {
            switch(rankInt)
            {
                case 0:
                    return "Leader";
                case 1:
                    return "Captain";
                case 2:
                    return "Lieutanent";
                case 3:
                    return "Cadet";
                default:
                    return "Unassigned Rank";
            }
        }
        public int RankInt(string rankType)
        {
            switch (rankType)
            {
                case "Leader":
                    return 0;
                case "Captain":
                    return 1;
                case "Lieutanent":
                    return 2;
                case "Cadet":
                    return 3;
                default:
                    return -100;
            }
        }
    }
}
