﻿using UnityEngine;
using System;

public class TimeMaster : MonoBehaviour
{
    #region Singleton

    private static TimeMaster _instance;
    public static TimeMaster Instance
    {
        get
        {
            if (_instance == null)
            {
                GameObject go = new GameObject("TimeMaster");
                go.AddComponent<TimeMaster>();
            }
            return _instance;
        }
    }

    #endregion

    // ====================================== FROM SERVER ====================================== \\

    public float timeDiff;
    public float disconnectPlayer = 10;

    public static float timePassed;

    public static int loggedIn = 0;

    public GameObject popupBlock;
    public GameObject scrollStopper;

    public bool chatLoaded; // check if all chat messages has been loaded
    public bool dataDistributed;    // check if all data has been distributed

    public static DateTime loginTime;

    void Awake()
    {
        if (loggedIn == 0)
            loginTime = DateTime.UtcNow;
    }

    void Start()
    {
        _instance = this;
        BlockerToggle(true);

        if (loggedIn == 0)
        {
            BlockerToggle(true);
            Remove_Local_Time_Saves();
            SetLoggedIn();
            //Invoke("CheckSceneOrigins", 1.5f);
        }
        else
        {
            Invoke("Set_Progress_Time_To_Last_Local_Save", 1.3f);
        }
    }

    void SetLoggedIn()
    {
        loggedIn = 1;
    }

    public long CalculateTimeDifference(long timeNow, long timeBefore)
    {
        return timeNow - timeBefore;
    }

    public void CheckSceneOrigins()
    {
        BlockerToggle(false);

        if (loggedIn == 0)  // If the value is 1, meaning it is loading from the battle scene
        {
            //LoadFromServerAction();
        }
    }

    public void DataDistributed()
    {
        dataDistributed = true;
        ToggleLoadingCheck();
    }
    public void ChatLoaded()
    {
        chatLoaded = true;
        ToggleLoadingCheck();
    }
    void ToggleLoadingCheck()
    {
        if (!chatLoaded)
            return;
        if (!dataDistributed)
            return;

        BlockerToggle(false);
        BuildTimer.Instance.StartBuilding();
    }


    // ===== AREA FOR LOCAL TIME STORAGE ===== \\
    public void SaveCountdownTimers()
    {
        Get_Current_InBuilding_Progress();
        Get_Current_RSS_Amount();
        Get_Remaining_Time_For_Interval_Reward();
        Get_Energy_RechargeTime_Left();
        Get_Mission_Remaining_Time();
        Get_Xcalireward_Remaining_Time();
        Get_Current_Device_Time();
    }

    #region Save Countdown Timer methods
    // Storing the progress of the any current on-going build progress
    void Get_Current_InBuilding_Progress()
    {
        if (BuildTimer.queuedBuilding.Count > 1)
        {
            PlayerPrefs.SetFloat("_myLocalStructureTimerSave", BuildTimer.buildingTimer[0]);
            PlayerPrefs.SetString("_myLocalStructureThatIsUpgradingNow", BuildTimer.queuedBuilding[0]);
        }

        if (BuildTimer.isBuildingStone[0] == true)
        {
            PlayerPrefs.SetFloat("_myLocalStone0TimerSave", BuildTimer.earthstone0_Build_Time);
            PlayerPrefs.SetString("_myLocalStone0TypeSave", BuildTimer.queuedStone0);
        }
        if (BuildTimer.isBuildingStone[1] == true)
        {
            PlayerPrefs.SetFloat("_myLocalStone1TimerSave", BuildTimer.windStone0_Build_Time);
            PlayerPrefs.SetString("_myLocalStone1TypeSave", BuildTimer.queuedStone1);
        }
        if (BuildTimer.isBuildingStone[2] == true)
        {
            PlayerPrefs.SetFloat("_myLocalStone2TimerSave", BuildTimer.flameStone0_Build_Time);
            PlayerPrefs.SetString("_myLocalStone2TypeSave", BuildTimer.queuedStone2);
        }
        if (BuildTimer.isBuildingStone[3] == true)
        {
            PlayerPrefs.SetFloat("_myLocalStone3TimerSave", BuildTimer.earthstone1_Build_Time);
            PlayerPrefs.SetString("_myLocalStone3TypeSave", BuildTimer.queuedStone3);
        }
        if (BuildTimer.isBuildingStone[4] == true)
        {
            PlayerPrefs.SetFloat("_myLocalStone4TimerSave", BuildTimer.windStone1_Build_Time);
            PlayerPrefs.SetString("_myLocalStone4TypeSave", BuildTimer.queuedStone4);
        }
        if (BuildTimer.isBuildingStone[5] == true)
        {
            PlayerPrefs.SetFloat("_myLocalStone5TimerSave", BuildTimer.flameStone1_Build_Time);
            PlayerPrefs.SetString("_myLocalStone5TypeSave", BuildTimer.queuedStone5);
        }
    }
    // Soring the amount of  RSS the generator has
    void Get_Current_RSS_Amount()
    {
        for (int x = 0; x < Player.mines.Length; x++)
        {
            PlayerPrefs.SetFloat("_mineCurrentValue" + x, BuildingManager.Instance.goldMine[x].currentResourceValue);
        }
    }
    //Storing the time left for interval rewards
    void Get_Remaining_Time_For_Interval_Reward()
    {
        PlayerPrefs.SetFloat("_remainingTimeForIntervalReward", Interval_Rewards.timeTillNextReward);
    }
    //Get the amount of energy the player has left
    //Get the time left before the current key completes charging
    void Get_Energy_RechargeTime_Left()
    {
        PlayerPrefs.SetInt("energyLeft", Player.playerCurrentEnergy);
        PlayerPrefs.SetFloat("energychargetime", Player.currentEnergyRechargeTime);
    }
    //Get the remaining time for daily missions
    void Get_Mission_Remaining_Time()
    {
        for (int x = 0; x < MissionHolder.Instance.missionAmount; x++)
        {
            if (MissionHolder.Instance.missionButtons[x].scriptableMission.missionState == 1)
            {
                PlayerPrefs.SetFloat(string.Format("mission{0}TimeLeft", x), MissionHolder.Instance.missionButtons[x].currentCooldownValue);
            }
        }
    }
    void Get_Xcalireward_Remaining_Time()
    {
        PlayerPrefs.SetFloat("xcalirewardTime", XcaliburEvent.timeToNextAvailablePrize);
    }
    // Storing the current device time to be used later
    void Get_Current_Device_Time()
    {
        PlayerPrefs.SetString("_myLocalLastSavedTimer", DateTime.Now.ToString());
    }
    #endregion
    // On coming back to home scene, set the remaining time to how much it was before player enters battle
    public void Set_Progress_Time_To_Last_Local_Save()
    {
        #region structure upgrades
        if (PlayerPrefs.HasKey("_myLocalStructureTimerSave"))
        {
            BuildTimer.buildingTimer[0] = (PlayerPrefs.GetFloat("_myLocalStructureTimerSave"));
            BuildTimer.queuedBuilding[0] = (PlayerPrefs.GetString("_myLocalStructureThatIsUpgradingNow"));
        }
        #endregion

        #region stoneupgrade

        //if (BuildTimer.isBuildingStone[0] == true)
        //{
        //    BuildTimer.earthstone0_Build_Time = PlayerPrefs.GetFloat("_myLocalStone0TimerSave");
        //    Debug.Log(BuildTimer.earthstone0_Build_Time);
        //    BuildTimer.queuedStone0 = PlayerPrefs.GetString("_myLocalStone0TypeSave");
        //}
        //if (BuildTimer.isBuildingStone[1] == true)
        //{
        //    BuildTimer.windStone0_Build_Time = PlayerPrefs.GetFloat("_myLocalStone1TimerSave");
        //    BuildTimer.queuedStone1 = PlayerPrefs.GetString("_myLocalStone1TypeSave");
        //}
        //if (BuildTimer.isBuildingStone[2] == true)
        //{
        //    BuildTimer.flameStone0_Build_Time = PlayerPrefs.GetFloat("_myLocalStone2TimerSave");
        //    BuildTimer.queuedStone2 = PlayerPrefs.GetString("_myLocalStone2TypeSave");
        //}
        //if (BuildTimer.isBuildingStone[3] == true)
        //{
        //    BuildTimer.earthstone1_Build_Time = PlayerPrefs.GetFloat("_myLocalStone3TimerSave");
        //    BuildTimer.queuedStone3 = PlayerPrefs.GetString("_myLocalStone3TypeSave");
        //}
        //if (BuildTimer.isBuildingStone[4] == true)
        //{
        //    BuildTimer.windStone1_Build_Time = PlayerPrefs.GetFloat("_myLocalStone4TimerSave");
        //    BuildTimer.queuedStone4 = PlayerPrefs.GetString("_myLocalStone4TypeSave");
        //}
        //if (BuildTimer.isBuildingStone[5] == true)
        //{
        //    BuildTimer.flameStone1_Build_Time = PlayerPrefs.GetFloat("_myLocalStone5TimerSave");
        //    BuildTimer.queuedStone5 = PlayerPrefs.GetString("_myLocalStone5TypeSave");
        //}
        #endregion

        #region mines value
        for (int x = 0; x < Player.mines.Length; x++)
        {
            BuildingManager.Instance.goldMine[x].currentResourceValue = PlayerPrefs.GetFloat("_mineCurrentValue" + x);
        }
        #endregion

        #region interval reward time left
        Interval_Rewards.timeTillNextReward = PlayerPrefs.GetFloat("_remainingTimeForIntervalReward");
        #endregion

        #region energyRecharge
        Player.playerCurrentEnergy = PlayerPrefs.GetInt("energyLeft");
        Player.currentEnergyRechargeTime = PlayerPrefs.GetFloat("energychargetime");
        #endregion

        #region missionTimeRemaining
        for (int x = 0; x < MissionHolder.Instance.missionAmount; x++)
        {
            if (PlayerPrefs.HasKey(string.Format("mission{0}TimeLeft", x)))
            {
                MissionHolder.Instance.missionButtons[x].currentCooldownValue = PlayerPrefs.GetFloat(string.Format("mission{0}TimeLeft", x));
                MissionHolder.Instance.missionButtons[x].Display_Cooldown_Stuff(true);
            }
        }
        #endregion

        #region XcaliburRewardTime
        XcaliburEvent.timeToNextAvailablePrize = PlayerPrefs.GetFloat("xcalirewardTime");
        #endregion


        Invoke("Assign_New_Values", 0.2f);
    }

    // Distributing the new values after calculating the time difference
    public void Assign_New_Values()
    {
        float localTimeDifference = Calculate_The_Time_Difference();

        #region buildingStuff
        if (BuildTimer.queuedBuilding.Count > 0)
        {
            //get the upgrade duration
            float buildingTime = Player.Instance.UpgradingBuildingLevel(BuildTimer.queuedBuilding[0]);
            buildingTime = BuildTimer.buildingTimer[0];
            buildingTime -= localTimeDifference;

            if (buildingTime <= 0.3f) BuildTimer.buildingTimer[0] = 0.5f;
            else if (buildingTime > 0.3f) BuildTimer.buildingTimer[0] = buildingTime;

            BuildTimer.Instance.LoadScaffold(0);
        }

        if (BuildTimer.isBuildingStone[0] == true)
        {
            BuildTimer.earthstone0_Build_Time -= localTimeDifference;
            UIManager.Instance.stone_Collect[0].SetActive(true);
            if (BuildTimer.earthstone0_Build_Time <= 0f)
                BuildTimer.earthstone0_Build_Time = 0.2f;
        }
        else if (BuildTimer.isBuildingStone[0] == false)
        {
            UIManager.Instance.stone_Collect[0].SetActive(false);
        }


        if (BuildTimer.isBuildingStone[1] == true)
        {
            BuildTimer.windStone0_Build_Time -= localTimeDifference;
            UIManager.Instance.stone_Collect[1].SetActive(true);
            if (BuildTimer.windStone0_Build_Time <= 0f)
                BuildTimer.windStone0_Build_Time = 0.2f;
        }
        else if (BuildTimer.isBuildingStone[1] == false)
        {
            UIManager.Instance.stone_Collect[1].SetActive(false);
        }


        if (BuildTimer.isBuildingStone[2] == true)
        {
            BuildTimer.flameStone0_Build_Time -= localTimeDifference;
            UIManager.Instance.stone_Collect[2].SetActive(true);
            if (BuildTimer.flameStone0_Build_Time <= 0f)
                BuildTimer.flameStone0_Build_Time = 0.2f;
        }
        else if (BuildTimer.isBuildingStone[2] == false)
        {
            UIManager.Instance.stone_Collect[2].SetActive(false);
        }


        if (BuildTimer.isBuildingStone[3] == true)
        {
            BuildTimer.earthstone1_Build_Time -= localTimeDifference;
            UIManager.Instance.stone_Collect[3].SetActive(true);
            if (BuildTimer.earthstone1_Build_Time <= 0f)
                BuildTimer.earthstone1_Build_Time = 0.2f;
        }
        else if (BuildTimer.isBuildingStone[3] == false)
        {
            UIManager.Instance.stone_Collect[3].SetActive(false);
        }


        if (BuildTimer.isBuildingStone[4] == true)
        {
            BuildTimer.windStone1_Build_Time -= localTimeDifference;
            UIManager.Instance.stone_Collect[4].SetActive(true);
            if (BuildTimer.windStone1_Build_Time <= 0f)
                BuildTimer.windStone1_Build_Time = 0.2f;
        }
        else if (BuildTimer.isBuildingStone[4] == false)
        {
            UIManager.Instance.stone_Collect[4].SetActive(false);
        }


        if (BuildTimer.isBuildingStone[5] == true)
        {
            BuildTimer.flameStone1_Build_Time -= localTimeDifference;
            UIManager.Instance.stone_Collect[5].SetActive(true);
            if (BuildTimer.flameStone1_Build_Time <= 0f)
                BuildTimer.flameStone1_Build_Time = 0.2f;
        }
        else if (BuildTimer.isBuildingStone[5] == false)
        {
            UIManager.Instance.stone_Collect[5].SetActive(false);
        }
        #endregion

        for (int x = 0; x < Player.mines.Length; x++)
        {
            BuildingManager.Instance.goldMine[x].Update_Current_Value((int)localTimeDifference);
        }

        if (Player.tutorial >= Player.tutorialCap)
        {
            //interval rewards are only available after completion of tutorial
            Interval_Rewards.Instance.rewardAvailable = false;
            Interval_Rewards.timeTillNextReward -= localTimeDifference;
            Interval_Rewards.Instance.Unable_To_Collect();

            if (Interval_Rewards.timeTillNextReward < 0.0f)
            {
                Interval_Rewards.Instance.rewardAvailable = true;
                Interval_Rewards.timeTillNextReward = 0.02f;
                Interval_Rewards.Instance.Able_To_Collect();
            }
        }

        //if PLayer has below maximum energy
        if (Player.playerCurrentEnergy < Player.maxEnergy)
        {
            //and the recharge time is lesser than the local time difference
            if (Player.currentEnergyRechargeTime <= localTimeDifference)
            {
                //give players an additional energy
                //and calculate the current recharge time
                Player.Instance.RechargeEnergy(1);
                Player.currentEnergyRechargeTime += Player.currentEnergyRechargeTime - localTimeDifference;
            }
            else
                Player.currentEnergyRechargeTime -= localTimeDifference;
        }

        for (int x = 0; x < MissionHolder.Instance.missionAmount; x++)
        {
            if (PlayerPrefs.HasKey(string.Format("mission{0}TimeLeft", x)))
            {
                if (MissionHolder.Instance.missionButtons[x].currentCooldownValue > localTimeDifference)
                    MissionHolder.Instance.missionButtons[x].currentCooldownValue -= localTimeDifference;
                else
                    MissionHolder.Instance.missionButtons[x].currentCooldownValue = 0.02f;
            }
        }

        XcaliburEvent.timeToNextAvailablePrize -= localTimeDifference;
        Remove_Local_Time_Saves();
    }

    public void BlockerToggle(bool toShow)
    {
        popupBlock.SetActive(toShow);
        scrollStopper.SetActive(toShow);
    }
    public float Calculate_The_Time_Difference()
    {
        if (PlayerPrefs.HasKey("_myLocalLastSavedTimer"))
        {
            DateTime battleStartTime = DateTime.Parse(PlayerPrefs.GetString("_myLocalLastSavedTimer"));
            TimeSpan localTimeDifference = DateTime.Now.Subtract(battleStartTime);
            return (float)localTimeDifference.TotalSeconds;
        }
        return 0;
    }
    void Remove_Local_Time_Saves()
    {
        if (PlayerPrefs.HasKey("_myLocalLastSavedTimer"))
        {
            //Remove structure local save times
            PlayerPrefs.DeleteKey("_myLocalStructureTimerSave");
            PlayerPrefs.DeleteKey("_myLocalStructureThatIsUpgradingNow");

            //Remove stone local save times
            PlayerPrefs.DeleteKey("_myLocalStone0TimerSave");
            PlayerPrefs.DeleteKey("_myLocalStone0TypeSave");

            PlayerPrefs.DeleteKey("_myLocalStone1TimerSave");
            PlayerPrefs.DeleteKey("_myLocalStone1TypeSave");

            PlayerPrefs.DeleteKey("_myLocalStone2TimerSave");
            PlayerPrefs.DeleteKey("_myLocalStone2TypeSave");

            PlayerPrefs.DeleteKey("_myLocalStone3TimerSave");
            PlayerPrefs.DeleteKey("_myLocalStone3TypeSave");

            PlayerPrefs.DeleteKey("_myLocalStone4TimerSave");
            PlayerPrefs.DeleteKey("_myLocalStone4TypeSave");

            PlayerPrefs.DeleteKey("_myLocalStone5TimerSave");
            PlayerPrefs.DeleteKey("_myLocalStone5TypeSave");

            //Remove local gold mine values
            for (int x = 0; x < Player.mines.Length; x++)
            {
                PlayerPrefs.DeleteKey("_mineCurrentValue" + x);
            }

            //Remove local interval reward time 
            PlayerPrefs.DeleteKey("_remainingTimeForIntervalReward");

            //Remove energy local timer
            PlayerPrefs.DeleteKey("energyLeft");
            PlayerPrefs.DeleteKey("energychargetime");

            for (int x = 0; x < MissionHolder.Instance.missionAmount; x++)
            {
                PlayerPrefs.DeleteKey(string.Format("mission{0}TimeLeft", x));
            }

            PlayerPrefs.DeleteKey("_myLocalLastSavedTimer");
        }
    }
}