﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Purchasing;


public class ShopManager : MonoBehaviour, IStoreListener
{
    public enum PurchaseItem
    {
        gold,
        diamond
    }
    public PurchaseItem purchasedItem;

    public ChatBoxContent systemChat;

    public int repAlterValue;

    static IStoreController m_StoreController;
    static IExtensionProvider m_StoreExtensionProvider;

    static string Buy_50_Diamonds = "50_diamonds"; 
    static string Buy_250_Diamonds = "250_diamonds";
    static string Buy_500_Diamonds = "500_diamonds";
    static string Buy_800_Diamonds = "800_diamonds";
    static string Buy_2000_Diamonds = "2000_diamonds";
    static string Buy_4000_Diamonds = "4000_diamonds";
    static string Buy_6000_Diamonds = "6000_diamonds";

    static string Buy_600_Diamonds_Package = "600_diamond_package";
    static string Buy_1500_Diamonds_Package = "1500_diamond_package";
    static string Buy_3500_Diamonds_Package = "3500_diamond_package";
    static string Buy_Starter_Pack = "starter_pack";

    static string Buy_EarthStone_ResearchSlot = "slot_flamestone";          // slot_flame is for the old Knights (Earth now)
    static string Buy_FlameStone_ResearchSlot = "slot_thunderstone";      // slot_thunder is for the old Mage (Flame now)
    static string Buy_WindStone_ResearchSlot = "slot_windstone";            // slot_wind is for archer


    [Header("Diamond Purchase Buttons")]
    public Button Diamond_50_Button;
    public Button Diamond_250_Button;
    public Button Diamond_500_Button;
    public Button Diamond_800_Button;
    public Button Diamond_2000_Button;
    public Button Diamond_4000_Button;
    public Button Diamond_6000_Button;
    public Button Diamond_600_Package_Button;
    public Button Diamond_1500_Package_Button;
    public Button Diamond_3500_Package_Button;
    public Button Start_Package_Button;
    public Button SlotPurchase_FlameStone;
    public Button SlotPurchase_EarthStone;
    public Button SlotPurchase_WindStone;
    public Button Timer_Package_Button;
    public Button Purchase_Jack_Pack;
    public Button Purchase_Queen_Pack;
    public Button Purchase_King_Pack;

    [Space(15)]
    [Header("Button Tabs")]
    public Toggle misc_Tab_Toggle;
    public Toggle diamond_Tab_Toggle;
    public Toggle bundle_Tab_Toggle;
    public Toggle gold_Tab_Toggle;


    [Space(15)]
    [Header("Gold Purchase Buttons")]
    public Button[] purchaseGold;
    int[] goldCost = { 10, 20, 50, 100, 150, 200, 300 };
    [Space(15)]
    [Header("Timer Purchase Buttons")]
    public Button[] purchaseTimer;
    int[] timersCost = { 135, 637, 1200, 2250 };
    int timerPackageCost = 800;
    [Space(15)]
    [Header("Packs Purchase Buttons")]
    public GameObject contentPopup;
    public Text contentText, packTypeText;
    public Button jackContentButton, queenContentButton, kingContentButton;
    int[] packPurchaseCost = { 80, 200, 500 };
    [Space(15)]
    [Header("Stone Bulk Purchase")]
    int stoneBulk_PurchaseCost = 2000;
    [Space(15)]
    [Header("Energy Bulk Purchase")]
    public Button[] purchase_Energy_Bulk_Button;
    int[] energy_Bulk_Purchase_Cost = { 70 };
    int[] energy_Bulk_Content = { 10 };

    [Space(15)]
    [Header("Energy Purchase")]
    int purchase_Energy_Cost = 8;


    static string toPurchase;
    static string rssUsed;

    [Space(15)]
    [Header("Stones Purchase Buttons")]
    public Button flameStoneSlot;
    public Button windStoneSlot;
    public Button earthStoneSlot;
    public Text flameStoneSlotCost;
    public Text windStoneSlotCost;
    public Text earthStoneSlotCost;
    public Barrack_Update bunkerUpdate;
    public Barrack_Update rangeUpdate;
    public Barrack_Update casterUpdate;
    string stoneSlotCost = "$1.48";

    [Space(15)]
    [Header("Purchase Popup")]
    public GameObject popupPanel;
    public Text confirmPurchaseText;
    public Button confirmPurchaseButton;
    string toReplaceText = "";

    [Space(15)]
    [Header("Purchased Result")]
    public GameObject purchaseResultPopup;
    public Text purchaseResultText;

    [Space(15)]
    [Header("DIamondExchange")]
    public Button[] exchangeButtonList;



    void Start()
    {
        toReplaceText = confirmPurchaseText.text;

         AddProducts();

        Diamond_50_Button.onClick.AddListener(Purchase50Diamond);
        Diamond_250_Button.onClick.AddListener(Purchase250Diamond);
        Diamond_500_Button.onClick.AddListener(Purchase500Diamond);
        Diamond_800_Button.onClick.AddListener(Purchase800Diamond);
        Diamond_2000_Button.onClick.AddListener(Purchase2000Diamond);
        Diamond_4000_Button.onClick.AddListener(Purchase4000Diamond);
        Diamond_6000_Button.onClick.AddListener(Purchase6000Diamond);

        Diamond_600_Package_Button.onClick.AddListener(Purchase_600_Diamond_Package_Deal);
        Diamond_1500_Package_Button.onClick.AddListener(Purchase_1500_Diamond_Package_Deal);
        Diamond_3500_Package_Button.onClick.AddListener(Purchase_3500_Diamond_Package_Deal);

        Start_Package_Button.onClick.AddListener(Purchase_Starter_Pack);

        Purchase_Jack_Pack.onClick.AddListener(AttemptPurchaseJackPack);
        Purchase_Queen_Pack.onClick.AddListener(AttemptPurchaseQueenPack);
        Purchase_King_Pack.onClick.AddListener(AttemptPurchaseKingPack);

        jackContentButton.onClick.AddListener(JackContent);
        queenContentButton.onClick.AddListener(QueenContent);
        kingContentButton.onClick.AddListener(KingContent);

        Timer_Package_Button.onClick.AddListener(AttemptPurchaseTimerPackage);

        SlotPurchase_FlameStone.onClick.AddListener(Purchase_Flame_StoneSlot);
        SlotPurchase_EarthStone.onClick.AddListener(Purchase_Earth_StoneSlot);
        SlotPurchase_WindStone.onClick.AddListener(Purchase_Wind_StoneSlot);
    }

    void AddProducts()
    {
        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

        builder.AddProduct(Buy_50_Diamonds, ProductType.Consumable);
        builder.AddProduct(Buy_250_Diamonds, ProductType.Consumable);
        builder.AddProduct(Buy_500_Diamonds, ProductType.Consumable);
        builder.AddProduct(Buy_800_Diamonds, ProductType.Consumable);
        builder.AddProduct(Buy_2000_Diamonds, ProductType.Consumable);
        builder.AddProduct(Buy_4000_Diamonds, ProductType.Consumable);
        builder.AddProduct(Buy_6000_Diamonds, ProductType.Consumable);

        builder.AddProduct(Buy_WindStone_ResearchSlot, ProductType.NonConsumable);
        builder.AddProduct(Buy_FlameStone_ResearchSlot, ProductType.NonConsumable);
        builder.AddProduct(Buy_EarthStone_ResearchSlot, ProductType.NonConsumable);

        builder.AddProduct(Buy_Starter_Pack, ProductType.NonConsumable);

        builder.AddProduct(Buy_600_Diamonds_Package, ProductType.Consumable);
        builder.AddProduct(Buy_1500_Diamonds_Package, ProductType.Consumable);
        builder.AddProduct(Buy_3500_Diamonds_Package, ProductType.Consumable);

        UnityPurchasing.Initialize(this, builder);

    }

    bool isInitialized()
    {
        return m_StoreController != null && m_StoreExtensionProvider != null;
    }

    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        m_StoreController = controller;
        m_StoreExtensionProvider = extensions;

        CheckIfItemPurchased();
    }
    public void OnInitializeFailed(InitializationFailureReason error)
    {
        Debug.Log(error + "FAILED TO INIT");
    }

    void CheckIfItemPurchased()
    {
        Product product = m_StoreController.products.WithID(Buy_Starter_Pack);
    }

    // ======= UI BUTTONS TAB TRIGGERING ====== \\
    public void ToggleGoldTab()
    {
        gold_Tab_Toggle.isOn = true;
    }
    public void TogglePackagesTab()
    {
        bundle_Tab_Toggle.isOn = true;
    }
    public void ToggleDiamondTab()
    {
        diamond_Tab_Toggle.isOn = true;
    }
    public void ToggleGameTab()
    {
        misc_Tab_Toggle.isOn = true;
    }


    // ======= CONFIRM PURCHASE POPUP INFO ====== \\
    void ClearConfirmPurchaseListeners()
    {
        confirmPurchaseText.text = toReplaceText;
        confirmPurchaseButton.onClick.RemoveAllListeners();
    }


    // ============= *** PRODUCTS PURCHASE AREA *** =========== \\

    //======= GOLD PURCHASE =======\\
    public void AttemptPurchaseGold(int goldAmount)
    {
        ClearConfirmPurchaseListeners();
        confirmPurchaseButton.onClick.AddListener(delegate { PurchaseGold(goldAmount); });
        DisplayConfirmation(string.Format("{0:n0} GOLD",goldAmount), purchaseGoldCost(goldAmount).ToString());
    }
    void PurchaseGold(int goldAmount)
    {
        if (Player.diamond >= purchaseGoldCost(goldAmount))
        {
            Player.gold += goldAmount;
            if (Player.gold >= Player.treasury_Storage_Cap)
                Player.gold = Player.treasury_Storage_Cap;

            Player.Instance.SpendDiamond(purchaseGoldCost(goldAmount));

            if (Player.accountType == "Guest")
                Player.SavePrefs();

            DataManager.Instance.PrepareDataForSparksPushing();
            GameSparksManager.Instance.SavePlayerExpendables(GameSparksManager.playerId);

            SuccessPopup(string.Format("{0:n0} GOLD", goldAmount));
            UIManager.Instance.UpdateCurrencyInfo();
        }
        else
            FailedPopup("Insufficient DIAMONDS");
    }
    //======= DIAMOND PURCHASE =======\\
    public void Purchase50Diamond()
    {
        BuyProductID(Buy_50_Diamonds);
    }
    public void Purchase250Diamond()
    {
        BuyProductID(Buy_250_Diamonds);
    }
    public void Purchase500Diamond()
    {
        BuyProductID(Buy_500_Diamonds);
    }
    public void Purchase800Diamond()
    {
        BuyProductID(Buy_800_Diamonds);
    }
    public void Purchase2000Diamond()
    {
        BuyProductID(Buy_2000_Diamonds);
    }
    public void Purchase4000Diamond()
    {
        BuyProductID(Buy_4000_Diamonds);
    }
    public void Purchase6000Diamond()
    {
        BuyProductID(Buy_6000_Diamonds);
    }
    // ====== DIAMOND EXCHANGE ====== \\
    public void AttemptExchangeDiamondWithGold(int diamondValue)
    {
        ClearConfirmPurchaseListeners();
        confirmPurchaseButton.onClick.AddListener(delegate { PurchaseDiamond(diamondValue); });
        DisplayConfirmation(string.Format("{0:n0} Diamonds", diamondValue), string.Format("{0:n0} GOLD", ExchangedDiamondAmount(diamondValue)) ,0);
    }
    void PurchaseDiamond(int diamondValue)
    {
        if (Player.gold >= ExchangedDiamondAmount(diamondValue))
        {
            Player.gold -= ExchangedDiamondAmount(diamondValue);

            DataManager.Instance.PrepareDataForSparksPushing();

            GameSparksManager.Instance.AttemptShopPurchase(this, PurchaseItem.diamond, diamondValue);
        }
        else
            FailedPopup("Insufficient GOLD");
    }
    // ======= PACKAGE PURCHASE =======\\
    void Purchase_600_Diamond_Package_Deal()
    {
        BuyProductID(Buy_600_Diamonds_Package);
    }
    void Purchase_1500_Diamond_Package_Deal()
    {
        BuyProductID(Buy_1500_Diamonds_Package);
    }
    void Purchase_3500_Diamond_Package_Deal()
    {
        BuyProductID(Buy_3500_Diamonds_Package);
    }
    void Purchase_Starter_Pack()
    {
        BuyProductID(Buy_Starter_Pack);
    }
    //======= ENERGY PURCHASE =======\\
    public void Purchase_8_Energy()
    {
        Player.playerCurrentEnergy += 8;
        Player.Instance.SpendDiamond(purchase_Energy_Cost);
        Check_Bulk_Energy_Purchase();

        DataManager.Instance.PrepareDataForSparksPushing();
        GameSparksManager.Instance.SavePlayerData(GameSparksManager.playerId);
        GameSparksManager.Instance.SavePlayerExpendables(GameSparksManager.playerId);
    }
    //======= STONE SLOT PURCHASE =======\\
    void Purchase_Flame_StoneSlot()
    {
        BuyProductID(Buy_FlameStone_ResearchSlot);
    }
    void Purchase_Wind_StoneSlot()
    {
        BuyProductID(Buy_WindStone_ResearchSlot);
    }
    void Purchase_Earth_StoneSlot()
    {
        BuyProductID(Buy_EarthStone_ResearchSlot);
    }
    //======= STONE BULK PURCHASE =======\\
    public void AttemptPurchaseStoneBulk(int stoneValue)
    {
        ClearConfirmPurchaseListeners();
        confirmPurchaseButton.onClick.AddListener(delegate { Purchase_Stone_Bulk(stoneValue); });
        DisplayConfirmation(StonePurchaseCheck(stoneValue),stoneBulk_PurchaseCost.ToString());
    }
    public void Purchase_Stone_Bulk(int stoneValue)
    {
        if (Player.diamond >= stoneBulk_PurchaseCost)
        {
            string stoneType = StonePurchaseCheck(stoneValue);
            if (stoneValue == 0)
                Player.squareStone += 10;
            else if (stoneValue == 1)
                Player.triangleStone += 10;
            else if (stoneValue == 2)
                Player.circleStone += 10;

            Player.Instance.SpendDiamond(stoneBulk_PurchaseCost);
            DataManager.Instance.PrepareDataForSparksPushing();

            GameSparksManager.Instance.SavePlayerExpendables(GameSparksManager.playerId);
            GameSparksManager.Instance.SaveInventoryStones();

            SuccessPopup(string.Format("10x {0}", stoneType));
        }
        else
            FailedPopup("Insufficient DIAMONDS");
    }
    string StonePurchaseCheck(int stoneID)
    {
        if (stoneID == 0)
            return "EarthStones";
        if (stoneID == 1)
            return "WindStones";
        if (stoneID == 2)
            return "FlameStones";
        return "";
    }
    //======= ENERGY PURCHASE =======\\
    public void AttemptPurchaseEnergyBulk(int bulkValue)
    {
        ClearConfirmPurchaseListeners();
        confirmPurchaseButton.onClick.AddListener( delegate { Purchase_Energy_Bulk(bulkValue); } );
        DisplayConfirmation("1 ENERGY PACK", energy_Bulk_Purchase_Cost[bulkValue].ToString());
    }
    void Purchase_Energy_Bulk(int bulkValue)
    {
        if (Player.diamond >= energy_Bulk_Purchase_Cost[bulkValue])
        {
            Player.energyPacks += energy_Bulk_Content[bulkValue];

            Player.Instance.SpendDiamond(energy_Bulk_Purchase_Cost[bulkValue]);
            SuccessPopup(string.Format("{0} Energy Packs", energy_Bulk_Content[bulkValue]));

            DataManager.Instance.PrepareDataForSparksPushing();
            GameSparksManager.Instance.SaveInventoryEnergyPacks();
        }
        else
            FailedPopup("Insufficient DIAMONDS");
    }
    //======= TIMERS PURCHASE =======\\
    public void AttemptPurchaseTimer(string purchaseType)
    {
        ClearConfirmPurchaseListeners();
        confirmPurchaseButton.onClick.AddListener( delegate { PurchaseTimers(purchaseType); } );
        DisplayConfirmation("50x " + purchaseType, purchaseTimerCost(purchaseType).ToString() );
    }
    public void PurchaseTimers(string purchaseType)
    {
        if (Player.diamond >= purchaseTimerCost(purchaseType))
        {
            if (purchaseType == "3min")
                Player.timer3 += 50;
            else if (purchaseType == "15min")
                Player.timer15 += 50;
            else if (purchaseType == "30min")
                Player.timer30 += 50;
            else if (purchaseType == "60min")
                Player.timer60 += 50;

            Player.Instance.SpendDiamond(purchaseTimerCost(purchaseType));

            DataManager.Instance.PrepareDataForSparksPushing();
            GameSparksManager.Instance.SavePlayerExpendables(GameSparksManager.playerId);
            GameSparksManager.Instance.SaveInventoryTimers();

            SuccessPopup("50x " + purchaseType + " Timers");
        }
        else 
            FailedPopup("Insufficient DIAMONDS");
            
    }
    int purchaseTimerCost(string timerType)
    {
        if (timerType == "3min")
            return 135;
        if (timerType == "15min")
            return 637;
        if (timerType == "30min")
            return 1200;
        if (timerType == "60min")
            return 2250;
        return 100;
    }

    void AttemptPurchaseTimerPackage()
    {
        ClearConfirmPurchaseListeners();
        confirmPurchaseButton.onClick.AddListener(Purchase_Timer_Package);
        DisplayConfirmation("Timer Package", "800");
    }
    void Purchase_Timer_Package()
    {
        if (Player.diamond >= 800)
        {
            Player.timer3 += 10;
            Player.timer15 += 10;
            Player.timer30 += 10;
            Player.timer60 += 10;

            Player.Instance.SpendDiamond(800);

            DataManager.Instance.PrepareDataForSparksPushing();
            GameSparksManager.Instance.SavePlayerExpendables(GameSparksManager.playerId);
            GameSparksManager.Instance.SaveInventoryTimers();

            SuccessPopup("Timer Package");
        }
        else
            FailedPopup("Insufficient DIAMONDS");
    }
    //======= PACKS PURCHASE =======\\
    void AttemptPurchaseJackPack()
    {
        ClearConfirmPurchaseListeners();
        confirmPurchaseButton.onClick.AddListener(Purchase_JackPack);
        DisplayConfirmation("1x Jack Pack", "80");
    }
    void Purchase_JackPack()
    {
        if (Player.diamond >= 80)
        {
            Player.jackPack += 1;

            Player.Instance.SpendDiamond(80);

            DataManager.Instance.PrepareDataForSparksPushing();
            GameSparksManager.Instance.SavePlayerExpendables(GameSparksManager.playerId);
            GameSparksManager.Instance.SaveInventoryPacksInfo();

            SuccessPopup("Jack Pack");
        }
        else
            FailedPopup("Insufficient DIAMONDS");
    }

    void AttemptPurchaseQueenPack()
    {
        ClearConfirmPurchaseListeners();
        confirmPurchaseButton.onClick.AddListener(Purchase_QueenPack);
        DisplayConfirmation("1x Queen Pack", "200");
    }
    void Purchase_QueenPack()
    {
        if (Player.diamond>=200)
        {
            Player.queenPack += 1;

            Player.Instance.SpendDiamond(200);

            DataManager.Instance.PrepareDataForSparksPushing();
            GameSparksManager.Instance.SavePlayerExpendables(GameSparksManager.playerId);
            GameSparksManager.Instance.SaveInventoryPacksInfo();

            SuccessPopup("Queen Pack");
        }
        else
            FailedPopup("Insufficient DIAMONDS");
    }

    void AttemptPurchaseKingPack()
    {
        ClearConfirmPurchaseListeners();
        confirmPurchaseButton.onClick.AddListener(Purchase_KingPack);
        DisplayConfirmation("1x King Pack", "500");
    }
    void Purchase_KingPack()
    {
        if (Player.diamond>= 500)
        {
            Player.kingPack += 1;

            Player.Instance.SpendDiamond(500);

            DataManager.Instance.PrepareDataForSparksPushing();
            GameSparksManager.Instance.SavePlayerExpendables(GameSparksManager.playerId);
            GameSparksManager.Instance.SaveInventoryPacksInfo();

            SuccessPopup("King Pack");
        }   
        else
            FailedPopup("Insufficient DIAMONDS");
    }

    void JackContent()
    {
        packTypeText.text = "JACK PACK";
        contentText.text = "Contains 6 random items";
        contentPopup.SetActive(true);
    }
    void QueenContent()
    {
        packTypeText.text = "QUEEN PACK";
        contentText.text = "Contains 8 random items + Guaranteed 1 Rare Card";
        contentPopup.SetActive(true);
    }
    void KingContent()
    {
        packTypeText.text = "KING PACK";
        contentText.text = "Contains 10 random items + Guaranteed 2 Rare Card + Guaranteed 1 Epic Card";
        contentPopup.SetActive(true);
    }

    #region SAMPLE PURCHASE
    public void BuyConsumable()
    {
        // Buy the consumable product using its general identifier. Expect a response either 
        // through ProcessPurchase or OnPurchaseFailed asynchronously.
        BuyProductID("Consumable_Product_ID");
    }
    public void BuyNonConsumable()
    {
        // Buy the non-consumable product using its general identifier. Expect a response either 
        // through ProcessPurchase or OnPurchaseFailed asynchronously.
        BuyProductID("NonConsumable_Product_ID");
    }
    public void BuySubscription()
    {
        // Buy the subscription product using its the general identifier. Expect a response either 
        // through ProcessPurchase or OnPurchaseFailed asynchronously.
        // Notice how we use the general product identifier in spite of this ID being mapped to
        // custom store-specific identifiers above.
        BuyProductID("Subscription_Product_ID");
    }
    #endregion

    // ============= *** OBTAIN AND PURCHASE PRODUCT RESULT *** =========== \\
    void BuyProductID(string productId)
    {
        if (isInitialized())
        {
            Player.purchasingItem = true;

            Product product = m_StoreController.products.WithID(productId);

            if (product != null && product.availableToPurchase)
            {
                m_StoreController.InitiatePurchase(product);
            }
        }
    }
    void Result()
    {
        
    }

    // ============= *** BUTTONS CHECK AREA *** ============== \\

    // ======= PURCHASE BUTTONS CHECK =======\\
    public void CheckPurchaseButton()
    {
        //CheckPurchaseGold();
        //CheckPurchaseTimer();
        CheckStoneSlot();
        //CheckPackPurchase();
        //CheckTimerPackage();
        //Check_Bulk_Energy_Purchase();
        PurchaseStarterPackCheck();
    }
    // ======= STONE SLOT BUTTONS CHECK =======\\
    public void CheckStoneSlot()
    {
        if (Player.earthStoneBuildSlot == 2)
            earthStoneSlot.interactable = false;

        if (Player.windStoneBuildSlots == 2)
            windStoneSlot.interactable = false;

        if (Player.flameStoneBuildSlot == 2)
            flameStoneSlot.interactable = false;

        StoneSlotCost();
    }
    public void StoneSlotCost()
    {
        if (Player.earthStoneBuildSlot <= 1)
            earthStoneSlotCost.text = stoneSlotCost;
        else if (Player.earthStoneBuildSlot == 2)
            flameStoneSlotCost.text = "Slot Purchased";


        if (Player.windStoneBuildSlots <= 1)
            windStoneSlotCost.text = stoneSlotCost;
        else if (Player.earthStoneBuildSlot == 2)
            windStoneSlotCost.text = "Slot Purchased";


        if (Player.flameStoneBuildSlot <= 1)
            flameStoneSlotCost.text = stoneSlotCost;
        else if (Player.earthStoneBuildSlot == 2)
            earthStoneSlotCost.text = "Slot Purchased";
    }
    // ======= GOLD PURCHASE BUTTONS CHECK =======\\
    void CheckPurchaseGold()
    {
        for (int x = 0; x < goldCost.Length; x++)
        {
            if (Player.diamond < goldCost[x])
                purchaseGold[x].interactable = false;
            else
                purchaseGold[x].interactable = true;
        }
    }
    int purchaseGoldCost(int goldAmount)
    {
        if (goldAmount == 10000)
            return 10;
        if (goldAmount == 20000)
            return 20;
        if (goldAmount == 50000)
            return 50;
        if (goldAmount == 105000)
            return 100;
        if (goldAmount == 165000)
            return 150;
        if (goldAmount == 250000)
            return 200;
        if (goldAmount == 450000)
            return 300;
        return 0;
    }
    int ExchangedDiamondAmount(int diamondAmount)
    {
        if (diamondAmount == 10)
            return 100000;
        if (diamondAmount == 100)
            return 1000000;
        return 0;
    }
    // ======= TIMERS PURCHASE CHECK =======\\
    void CheckPurchaseTimer()
    {
        for (int x = 0; x < timersCost.Length; x++)
        {
            if (Player.diamond < timersCost[x])
                purchaseTimer[x].interactable = false;
            else
                purchaseTimer[x].interactable = true;
        }
    }
    void CheckTimerPackage()
    {
        if (Player.diamond < timerPackageCost)
            Timer_Package_Button.interactable = false;
        else
            Timer_Package_Button.interactable = true;
    }
    // ======= PACKS PURCHASE CHECK ======= \\
    void CheckPackPurchase()
    {
        if (Player.diamond < packPurchaseCost[0])
            Purchase_Jack_Pack.interactable = false;
        else
            Purchase_Jack_Pack.interactable = true;

        if (Player.diamond < packPurchaseCost[1])
            Purchase_Queen_Pack.interactable = false;
        else
            Purchase_Queen_Pack.interactable = true;
        
        if (Player.diamond < packPurchaseCost[2])
            Purchase_King_Pack.interactable = false;
        else
            Purchase_King_Pack.interactable = true;
    }
    // ======= ENERGY BULK PURCHASE ======= \\
    void Check_Bulk_Energy_Purchase()
    {
        for (int x = 0; x < purchase_Energy_Bulk_Button.Length; x++)
        {
            if (Player.diamond < energy_Bulk_Purchase_Cost[x])
                purchase_Energy_Bulk_Button[x].interactable = false;
            else
                purchase_Energy_Bulk_Button[x].interactable = true;
        }
    }
    // =======
    public void PurchaseStarterPackCheck()
    {
        if (Player.starterPackPurchase == 0)
            Start_Package_Button.interactable = true;
        else if (Player.starterPackPurchase == 1)
            Start_Package_Button.interactable = false;
    }


    // ======= PURCHASE OUTCOME ======= \\
    void DiamondPurchased(int diamondValue)
    {
        Player.diamond += diamondValue;

        SuccessPopup(string.Format("{0} Diamonds", diamondValue));
    }
    void PackagePurchased(int typeOfPackage)
    {
        switch(typeOfPackage)
        {
            case 1:
                //600 Diamond package
                Player.diamond += 600;
                Player.jackPack += 3;
                SuccessPopup("Value Package 1");
                break;
            case 2:
                //1500 Diamond package
                Player.diamond += 1500;
                Player.jackPack += 3;
                Player.queenPack += 1;
                SuccessPopup("Value Package 2");
                break;
            case 3:
                //3500 Diamond package
                Player.excalipoints += 1500;
                Player.diamond += 3500;
                Player.jackPack += 3;
                Player.queenPack += 2;
                Player.kingPack += 1;
                SuccessPopup("Value Package 3");
                break;
        }

        DataManager.Instance.PrepareDataForSparksPushing();
        GameSparksManager.Instance.SavePlayerExpendables(GameSparksManager.playerId);
        GameSparksManager.Instance.SaveInventoryPacksInfo();
    }
    void StarterPackPurchased()
    {
        Player.excalipoints += 1000;
        Player.diamond += 3000;
        Player.jackPack += 3;
        Player.queenPack += 2;
        Player.kingPack += 1;
        Player.starterPackPurchase = 1;
        PurchaseStarterPackCheck();
        SuccessPopup("Starter Pack");
        Start_Package_Button.interactable = false;
        GameSparksManager.Instance.SaveStarterPackPurchase();
    }
    void Purchase_Stone_Build_Slot(int stoneType)
    {
        string stoneSlot = "";
        if (stoneType == 0)
        {
            Player.earthStoneBuildSlot = 2;
            DataManager.bunkerStoneSlot = Player.earthStoneBuildSlot;
            bunkerUpdate.GetUpdated();
            GameSparksManager.Instance.SaveBarracks(1);
            stoneSlot = "Earth Stone Research Slot";
        }
        if (stoneType == 1)
        {
            Player.windStoneBuildSlots = 2;
            DataManager.rangeStoneSlot = Player.windStoneBuildSlots;
            rangeUpdate.GetUpdated();
            GameSparksManager.Instance.SaveBarracks(2);
            stoneSlot = "Wind Stone Research Slot";
        }
        if (stoneType == 2)
        {
            Player.flameStoneBuildSlot = 2;
            DataManager.guildStoneSlot = Player.flameStoneBuildSlot;
            casterUpdate.GetUpdated();
            GameSparksManager.Instance.SaveBarracks(0);
            stoneSlot = "Flame Stone Research Slot";
        }
        CheckStoneSlot();
        SuccessPopup(stoneSlot);
    }

    public void DiamondExchangeOutcome(int diamondValue)
    {
        Player.Instance.GainDiamond(diamondValue);
        SuccessPopup(string.Format("{0:n0} Diamonds",diamondValue));

        GameSparksManager.Instance.SavePlayerExpendables(GameSparksManager.playerId);
    }

    void SuccessPopup(string typeOfPackage)
    {
        purchaseResultText.text = string.Format("You've successfully obtained {0}", typeOfPackage);
        purchaseResultPopup.SetActive(true);
        systemChat.DisplayMessageInChatbox("Successfully obtained " + typeOfPackage);

        if (Player.accountType == "Guest")
            Player.SavePrefs();

        UIManager.Instance.UpdateCurrencyInfo();
        UIManager.Instance.UpdateTavernInfos();
    }
    void FailedPopup(string failReason)
    {
        purchaseResultText.text = failReason + "";
        purchaseResultPopup.SetActive(true);
    }


    // ===== PURCHASE POPUP DISPLAY ===== \\
    void DisplayConfirmation(string typeOfPurchase, string amountToPurchase)
    {
        confirmPurchaseText.text = confirmPurchaseText.text.Replace("purchase_product", typeOfPurchase);
        confirmPurchaseText.text = confirmPurchaseText.text.Replace("product_cost", amountToPurchase + " DIAMONDS");

        popupPanel.SetActive(true);
    }
    void DisplayConfirmation(string typeOfPurchase, string amountToPurchase, int type)
    {
        confirmPurchaseText.text = confirmPurchaseText.text.Replace("purchase_product", typeOfPurchase);
        confirmPurchaseText.text = confirmPurchaseText.text.Replace("product_cost", amountToPurchase);

        popupPanel.SetActive(true);
    }

    public void Unbuying()
    {
        // this bool is used to determine if the player's 
        Player.purchasingItem = false;
    }


    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e)
    {
        if (string.Equals(e.purchasedProduct.definition.id, Buy_50_Diamonds, System.StringComparison.Ordinal))
            DiamondPurchased(50);
        else if (string.Equals(e.purchasedProduct.definition.id, Buy_250_Diamonds, System.StringComparison.Ordinal))
            DiamondPurchased(250);
        else if (string.Equals(e.purchasedProduct.definition.id, Buy_500_Diamonds, System.StringComparison.Ordinal))
            DiamondPurchased(500);
        else if (string.Equals(e.purchasedProduct.definition.id, Buy_800_Diamonds, System.StringComparison.Ordinal))
            DiamondPurchased(800);
        else if (string.Equals(e.purchasedProduct.definition.id, Buy_2000_Diamonds, System.StringComparison.Ordinal))
            DiamondPurchased(2000);
        else if (string.Equals(e.purchasedProduct.definition.id, Buy_4000_Diamonds, System.StringComparison.Ordinal))
            DiamondPurchased(4000);
        else if (string.Equals(e.purchasedProduct.definition.id, Buy_6000_Diamonds, System.StringComparison.Ordinal))
            DiamondPurchased(6000);

        else if (string.Equals(e.purchasedProduct.definition.id, Buy_EarthStone_ResearchSlot, System.StringComparison.Ordinal))
            Purchase_Stone_Build_Slot(0);
        else if (string.Equals(e.purchasedProduct.definition.id, Buy_WindStone_ResearchSlot, System.StringComparison.Ordinal))
            Purchase_Stone_Build_Slot(1);
        else if (string.Equals(e.purchasedProduct.definition.id, Buy_FlameStone_ResearchSlot, System.StringComparison.Ordinal))
            Purchase_Stone_Build_Slot(2);

        else if (string.Equals(e.purchasedProduct.definition.id, Buy_600_Diamonds_Package, System.StringComparison.Ordinal))
            PackagePurchased(1);
        else if (string.Equals(e.purchasedProduct.definition.id, Buy_1500_Diamonds_Package, System.StringComparison.Ordinal))
            PackagePurchased(2);
        else if (string.Equals(e.purchasedProduct.definition.id, Buy_3500_Diamonds_Package, System.StringComparison.Ordinal))
            PackagePurchased(3);
        else if (string.Equals(e.purchasedProduct.definition.id, Buy_Starter_Pack, System.StringComparison.Ordinal))
            StarterPackPurchased();

        return PurchaseProcessingResult.Complete;
    }
    public void OnPurchaseFailed(Product i, PurchaseFailureReason p)
    {
        if (p == PurchaseFailureReason.PurchasingUnavailable)
            FailedPopup("Purchasing unavailable");
        if (p == PurchaseFailureReason.ExistingPurchasePending)
            FailedPopup("Purchase pending");
        if (p == PurchaseFailureReason.ProductUnavailable)
            FailedPopup("Product unavailable");
        if (p == PurchaseFailureReason.SignatureInvalid)
            FailedPopup("Invalid signature");
        if (p == PurchaseFailureReason.UserCancelled)
            FailedPopup("Purchase cancelled");
        if (p == PurchaseFailureReason.PaymentDeclined)
            FailedPopup("Payment declined");
        if (p == PurchaseFailureReason.DuplicateTransaction)
            FailedPopup("Duplicate purcahse");
        if (p == PurchaseFailureReason.Unknown)
            FailedPopup("Unknown Error");
    }

    #region NOT IN USE
    public void PurchaseReputation()
    {
        Player.Instance.AlterRepValue(true, repAlterValue);
        if (Player.accountType == "Guest")
            Player.SavePrefs();
        Player.Instance.Check_If_Castle_Upgradable();
        UIManager.Instance.UpdatePlayerInfo();
    }
    public void PurchaseSupports()
    {
        Player.landmines += 5;
        if (Player.landmines >= 999)
            Player.landmines = 999;
        Player.dragons += 5;
        if (Player.dragons >= 999)
            Player.dragons = 999;
        Player.seige += 5;
        if (Player.seige >= 999)
            Player.seige = 999;

        if (Player.accountType == "Guest")
            Player.SavePrefs();
    }
    public void PurchaseGroups()
    {
        Player.myGroups += 10;
        if (Player.myGroups >= 99)
            Player.myGroups = 99;

        if (Player.accountType == "Guest")
            Player.SavePrefs();
    }
    public void PurchaseHeroCards()
    {
        for (int y = 0; y < UnitManager.heroCardsAmount.Length; y++)
        {
            UnitManager.heroCardsAmount[y] += 20;
            UnitManager.Instance.DistributeStats();

            UnitManager.Instance.SaveHeroInfo();
        }
    }
    public void PurchaseEvolutionStones()
    {
        Player.circleStone += 20;
        Player.triangleStone += 20;
        Player.squareStone += 20;
        if (Player.accountType == "Guest")
            Player.SavePrefs();
    }
    public void Purchase_Battle_Encounter()
    {
        Player.squareBattles += Player.evolvementRequiredBattles;
        Player.circleBattles += Player.evolvementRequiredBattles;
        Player.triangleBattles += Player.evolvementRequiredBattles;
        if (Player.accountType == "Guest")
            Player.SavePrefs();
    }
    public void ConfirmPack_Purchase()
    {
        //TODO  set packs purchases back to +1
        if (toPurchase.Contains("Jack"))
        {
            Player.jackPack += 5;
            //Player.Instance.SpendGold(lastPurchasedPrice);
        }
        else if (toPurchase.Contains("Queen"))
        {
            Player.queenPack += 5;
            //Player.Instance.SpendGold(lastPurchasedPrice);
        }
        else if (toPurchase.Contains("King"))
        {
            Player.kingPack += 5;
            //Player.Instance.SpendDiamond(lastPurchasedPrice);
        }

        if (toPurchase.Contains("BronzeChest"))
        {
            Player.bronzeChest += 5;
            //Player.Instance.SpendDiamond(lastPurchasedPrice);
        }
        else if (toPurchase.Contains("SilverChest"))
        {
            Player.silverChest += 5;
            //Player.Instance.SpendDiamond(lastPurchasedPrice);
        }
        else if (toPurchase.Contains("GoldChest"))
        {
            Player.goldChest += 5;
            //Player.Instance.SpendDiamond(lastPurchasedPrice);
        }
    }
    #endregion
}
