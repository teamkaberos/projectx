﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DataManager : MonoBehaviour
{
    public delegate void DataObtained();
    public static event DataObtained DataDistributed;
    static DataManager _instance;
    public static DataManager Instance
    {
        get
        {
            return _instance;
        }
    }

    #region Raw Data

    // There are the datas that are pushed / pulled directly to / from Gamesparks
    // The DataManager class is responsible for both converting raw data into useful data, or other way around
    // Data are only passed into DataManager class when Gamesparks method is called

    // ----- Player Data ----- \\

    public static string clanID;
    public static string portrait;
    public static string race;
    public static int tutorialState;

    // ----- Clan ----- \\

    public static int contributionPoints;
    public static int rank;

    // ----- Expendables ----- \\

    public static int reputation;
    public static int excalipoints;
    public static int gold;
    public static int diamond;
    public static int token;

    public static int squareStones;
    public static int circleStones;
    public static int triangleStones;

    public static int timer3Amount;
    public static int timer15Amount;
    public static int timer30Amount;
    public static int timer60Amount;

    public static int jackPack;
    public static int queenPack;
    public static int kingPack;

    public static int bronzeChest;
    public static int silverChest;
    public static int goldChest;

    public static int energyPacks;

    // ----- Structures ----- \\

    public static int castleLv;
    public static int townHallLv;
    public static int treasuryLv;
    public static int treasuryStorageLimit;
    public static int preparationHallLv;
    public static int bunkerLv;
    public static int bunkerStoneSlot;
    public static int rangeLv;
    public static int rangeStoneSlot;
    public static int guildLv;
    public static int guildStoneSlot;
    public static int forgeLv;

    public static int[] minesLv = new int[5];
    public static int[] minesValue = new int[5];

    // ----- Mission ----- \\

    public static int[] missionIDs = new int[3];
    public static int[] missionProgresses = new int[3];
    public static float[] missionRefreshTimes = new float[3];
    public static int[] missionStates = new int[3];


    // ----- Upgrade States ----- \\

    public static int buildingUnderUpgrade; // Using the building's index to identify which building is currently being upgraded. Only one upgrade at the same time is allowed.
    public static float buildingTimeLeft;       // Time left for the structure being upgraded to completion

    public static int[] stonesUnderUpgrade = new int[6];    // Same as above, except that you can upgrade 6 stones at the same time

    // ----- Heroes ----- \\

    public static int[] heroIsUnlocked = new int[17];
    public static int[] heroLevels = new int[17];
    public static int[] cardAmount = new int[17];

    // ----- Battle ----- \\

    public static int energy;
    public static float energyRefreshTime;

    public static float shuffleTime;

    public static int casterBattleCount;
    public static int meleeBattleCount;
    public static int rangeBattleCount;

    public static int battles;
    public static int losses;
    public static int victories;

    // setNo x slotNo
    public static int[,] assignedUnitsSlot = new int[3, 3];
    public static int[,] assignedUnitsLevel = new int[3, 3];

    public List<RevengeOpponent> revengeList = new List<RevengeOpponent>();

    // ----- Others ----- \\

    static bool logged;

    public static long lastLogout;
    public static float intervalRewardRefreshTime;

    static bool newPlayerSaved;

    #endregion

    #region Processed Data

    // Only for shared usage
    public static long initialLoginServerTime = -100;
    public static long currentServerTime;

    #endregion

    void Start()
    {
        _instance = this;

        if (SceneManager.GetActiveScene().name.ToLower().Contains("login"))
        {
            logged = false;
        }
        else if (SceneManager.GetActiveScene().name.ToLower().Contains("home"))
        {
            if (GameSparksManager.newPlayer == true)
            {
                if (newPlayerSaved == false)
                {
                    // this sends the default values and creates the initial OBJ to be stored in GS
                    Invoke("PrepareNewPlayerStoreData", 1.4f);
                    Invoke("NewPlayerStoreData", 1.6f);
                    UIManager.Instance.ChatCanvasDisplay();
                    UIManager.Instance.ClanToggleDisplay();
                    UIManager.Instance.SetSUnitToggleDisplay();

                    newPlayerSaved = true;  // to ensure it only saves once
                }
                else
                {
                    Invoke("DistributeAllData", 1.2f);
                }
            }
        }

        if (logged == false)
        {
            GameSparksManager.InformationsReceived += DistributeAllData;
            GameSparksManager.SiegeReceived += DistributeDataMinor;
        }
        else if (logged == true && GameSparksManager.allObtained == true)
            Invoke("DistributeAllData", 1.2f);

        // dependant on DistributeAllDate() Player.cs(454),(455), BuildingManager.cs(70), MissionHolder.cs(63), TroopAssign_Info(40)
        // TimeMaster(76), Tutorial(41), UnitManager(90), TimeMaster.SetProgressToLastLocalSave(80)
    }

    void PrepareNewPlayerStoreData()
    {
        TroopAssign_Info.Instance.New_AssignTroop();
        UnitManager.Instance.AssignNumbers();
        Player.Instance.UpdatePlayerMission();

        PrepareDataForSparksPushing();
    }
    void NewPlayerStoreData()
    {
        GameSparksManager.Instance.StoreAllData();

        TroopAssign_Info.Instance.SaveTroop();

        Tutorial.Instance.Get_Updated_Tutorial_Value();

        BuildingManager.Instance.DistributeStructureLevels();

        TimeMaster.Instance.CheckSceneOrigins();
    }
    public bool LoggedCheck()
    {
        return logged;
    }
    public void PrepareDataForSparksPushing()
    {
        clanID = Clan.clanID;
        portrait = Player.potraitID;
        race = Player.race;
        tutorialState = Player.tutorial;

        reputation = Player.reputation;
        excalipoints = Player.excalipoints;
        gold = Player.gold;
        diamond = Player.diamond;
        token = Player.tokens;

        casterBattleCount = Player.circleBattles;
        meleeBattleCount = Player.squareBattles;
        rangeBattleCount = Player.triangleBattles;

        squareStones = Player.squareStone;
        circleStones = Player.circleStone;
        triangleStones = Player.triangleStone;

        timer3Amount = Player.timer3;
        timer15Amount = Player.timer15;
        timer30Amount = Player.timer30;
        timer60Amount = Player.timer60;

        jackPack = Player.jackPack;
        queenPack = Player.queenPack;
        kingPack = Player.kingPack;

        bronzeChest = Player.bronzeChest;
        silverChest = Player.silverChest;
        goldChest = Player.goldChest;

        energyPacks = Player.energyPacks;

        castleLv = Player.castleLevel;
        townHallLv = Player.townHallLevel;
        treasuryLv = Player.treasuryLvl;
        treasuryStorageLimit = Player.treasury_Storage_Cap;
        preparationHallLv = Player.recruitmentHall_Lvl;
        bunkerLv = Player.bunkerLv;
        rangeLv = Player.rangeLv;
        guildLv = Player.guildLv;
        forgeLv = Player.forgeLevel;

        minesLv = Player.mines;

        if (BuildTimer.queuedBuilding.Count > 0)    // Building under upgrade's data will not be saved if there is nothing in the building queue list
        {
            buildingUnderUpgrade = BuildTimer.Instance.UpgradingBuildingNo(BuildTimer.queuedBuilding[0]);
            buildingTimeLeft = BuildTimer.buildingTimer[0];
        }
        else                                        // Save the values as 0
        {
            buildingUnderUpgrade = 0;
            buildingTimeLeft = 0;
        }

        for (int i = 0; i < Player.heroAmount; i++)
        {
            heroIsUnlocked[i] = Player.heroUnlocked[i];
            heroLevels[i] = Player.heroLevels[i];
            cardAmount[i] = Player.heroCardAmounts[i];
        }

        energy = Player.playerCurrentEnergy;
        energyRefreshTime = Player.currentEnergyRechargeTime;

        shuffleTime = Player.shufflingTime;

        for (int i = 0; i < 3; i++)
        {
            missionIDs[i] = MissionHolder.Instance.missionsAssigned[i];
            if (missionIDs[i] != 0)
            {
                missionProgresses[i] = MissionHolder.Instance.missionButtons[i].scriptableMission.missionProgress;
                missionStates[i] = MissionHolder.Instance.missionButtons[i].scriptableMission.missionState;
                missionRefreshTimes[i] = MissionHolder.Instance.missionButtons[i].currentCooldownValue;
            }
        }

        battles = Player.my_Matches;
        losses = Player.my_Losses;
        victories = Player.my_Victories;

        contributionPoints = Clan.contributionPoints;
        rank = Clan.rank;

        intervalRewardRefreshTime = Interval_Rewards.timeTillNextReward;
    }

    public void DistributeAllData()
    {
        logged = true;

        //Clan.clanID = clanID;
        Player.potraitID = portrait;
        Player.race = race;
        Player.tutorial = tutorialState;

        Player.reputation = reputation;
        Player.excalipoints = excalipoints;
        Player.gold = gold;
        Player.diamond = diamond;
        Player.tokens = token;

        Player.squareStone = squareStones;
        Player.circleStone = circleStones;
        Player.triangleStone = triangleStones;

        Player.earthStoneBuildSlot = bunkerStoneSlot;
        Player.windStoneBuildSlots = rangeStoneSlot;
        Player.flameStoneBuildSlot = guildStoneSlot;

        Player.circleBattles = casterBattleCount;
        Player.squareBattles = meleeBattleCount;
        Player.triangleBattles = rangeBattleCount;

        Player.timer3 = timer3Amount;
        Player.timer15 = timer15Amount;
        Player.timer30 = timer30Amount;
        Player.timer60 = timer60Amount;

        Player.jackPack = jackPack;
        Player.queenPack = queenPack;
        Player.kingPack = kingPack;

        Player.bronzeChest = bronzeChest;
        Player.silverChest = silverChest;
        Player.goldChest = goldChest;

        Player.energyPacks = energyPacks;

        Player.castleLevel = castleLv;
        Player.townHallLevel = townHallLv;
        Player.treasuryLvl = treasuryLv;
        Player.recruitmentHall_Lvl = preparationHallLv;
        Player.bunkerLv = bunkerLv;
        Player.rangeLv = rangeLv;
        Player.guildLv = guildLv;
        //Player.forgeLevel = forgeLv;

        Player.mines = minesLv;
        Player.minesCurrentValue = minesValue;

        if (BuildTimer.queuedBuilding.Count == 0)
        {
            if (BuildingManager.Instance.Convert_Building_ID_To_Name(buildingUnderUpgrade) != null)
            {
                BuildTimer.queuedBuilding.Add(BuildingManager.Instance.Convert_Building_ID_To_Name(buildingUnderUpgrade));
                BuildTimer.buildingTimer.Add(buildingTimeLeft);
            }
        }

        for (int i = 0; i < Player.heroAmount; i++)
        {
            Player.heroUnlocked[i] = heroIsUnlocked[i];
            Player.heroLevels[i] = heroLevels[i];
            Player.heroCardAmounts[i] = cardAmount[i];
        }

        for (int i = 0; i < 3; i++)
        {
            Player.assignedTroops[i] = assignedUnitsSlot[0, i];
        }
        for (int i = 0; i < 3; i++)
        {
            Player.assignedTroops[i + 3] = assignedUnitsSlot[1, i];
        }
        for (int i = 0; i < 3; i++)
        {
            Player.assignedTroops[i + 6] = assignedUnitsSlot[2, i];
        }

        Player.playerCurrentEnergy = energy;
        if (Player.playerCurrentEnergy < 0)
            Player.playerCurrentEnergy = 0;
        Player.currentEnergyRechargeTime = energyRefreshTime;

        for (int i = 0; i < 3; i++)
        {
            Player.playerMission[i] = missionIDs[i];
            Player.playerMissionProgress[i] = missionProgresses[i];
            Player.playerMissionState[i] = missionStates[i];
            Player.playerMissionRefreshTime[i] = missionRefreshTimes[i];
        }


        Player.my_Matches = battles;
        Player.my_Losses = losses;
        Player.my_Victories = victories;

        Interval_Rewards.timeTillNextReward = intervalRewardRefreshTime;

        DistributoToGame();
    }

    public void DistributoToGame()
    {
        BuildingManager.Instance.DistributeStructureLevels();
        BuildingManager.Instance.DisplayBattlePointsNotification();

        Player.Instance.UpdatePlayerMission();
        MissionHolder.Instance.FillInTimeDifference();

        UnitManager.Instance.AssignNumbers();
        TroopAssign_Info.Instance.New_AssignTroop();

        Tutorial.Instance.Get_Updated_Tutorial_Value();

        EnemyLoader.Instance.SiegeEnemiesDisplay("away");

        UIManager.Instance.ChatCanvasDisplay();
        UIManager.Instance.ClanToggleDisplay();
        UIManager.Instance.SetSUnitToggleDisplay();

        Interval_Rewards.Instance.Check_If_Interval_Reward_Ready();
        Player.Instance.UpdatePlayerEnergy();
        PotraitManager.Instance.Assign_Player_Potrait();

        ClanManager.Instance.CheckForClanRemovalNotification();

        TimeMaster.Instance.DataDistributed();
        Player.Instance.StoreAllData();
        DataDistributed?.Invoke();
    }


    // This happens only when the player gets sieged
    public void DistributeDataMinor()
    {
        Player.reputation = reputation;
        Player.gold = gold;
        DataDistributed?.Invoke();
        UIManager.Instance.Update_All();
        TimeMaster.Instance.BlockerToggle(false);
    }

    #region Data Processing

    public void Reset()
    {
        clanID = "<currently_not_in_any_clan_yet>";
        portrait = "h0001";
        race = "human";
        tutorialState = 0;

        contributionPoints = 0;
        rank = -3;

        reputation = 100;
        excalipoints = 0;
        gold = 18000;
        diamond = 200;
        token = 25;

        squareStones = 0;
        circleStones = 0;
        triangleStones = 0;

        timer3Amount = 0;
        timer15Amount = 0;
        timer30Amount = 0;
        timer60Amount = 0;

        jackPack = 0;
        queenPack = 0;
        kingPack = 0;

        bronzeChest = 0;
        silverChest = 0;
        goldChest = 0;

        energyPacks = 0;

        castleLv = 0;
        townHallLv = 0;
        treasuryLv = 0;
        treasuryStorageLimit = 20000;
        preparationHallLv = 0;
        bunkerLv = 0;
        bunkerStoneSlot = 1;
        rangeLv = 0;
        rangeStoneSlot = 1;
        guildLv = 0;
        guildStoneSlot = 1;
        forgeLv = 0;

        minesLv = new int[5];
        minesValue = new int[5];

        missionIDs = new int[3];
        missionProgresses = new int[3];
        missionRefreshTimes = new float[3];
        missionStates = new int[3];

        stonesUnderUpgrade = new int[6];    // Same as above, except that you can upgrade 6 stones at the same time

        heroIsUnlocked = new int[17];
        heroLevels = new int[17];
        cardAmount = new int[17];

        energy = 8;

        energyRefreshTime = 0;

        shuffleTime = 12;

        casterBattleCount = 0;
        meleeBattleCount = 0;
        rangeBattleCount = 0;

        battles = 0;
        losses = 0;
        victories = 0;

        assignedUnitsSlot = new int[3, 3];
        assignedUnitsLevel = new int[3, 3];

        revengeList = new List<RevengeOpponent>();
        logged = false;

        lastLogout = 0;
        intervalRewardRefreshTime = 0;
    }

    #endregion
}

// Stores the information of the opponent in the revenge list
public class RevengeOpponent
{
    public string attackerID;
    public string defenderID;
    public int goldLost;
    public string timeOfAttack;
}