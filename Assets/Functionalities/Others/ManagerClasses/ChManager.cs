﻿using UnityEngine;

public class ChManager : MonoBehaviour 
{
    public int goldAdd;
    public int diamondAdd;
    public int excalipointsAdd;

    public int castleLevelToBecome;

    public int repToBecome;


    public void Ch()
    {
        int rand = castleLevelToBecome;
        DataManager.castleLv = castleLevelToBecome;


        DataManager.townHallLv = Random.Range(castleLevelToBecome*3 , (castleLevelToBecome+1)*3 + 1);
        DataManager.treasuryLv = Random.Range(castleLevelToBecome * 3, (castleLevelToBecome + 1) * 3 + 1);
        DataManager.preparationHallLv = Random.Range(castleLevelToBecome * 3, (castleLevelToBecome + 1) * 3 + 1);
        DataManager.bunkerLv = Random.Range(castleLevelToBecome * 3, (castleLevelToBecome + 1) * 3 + 1);
        DataManager.rangeLv = Random.Range(castleLevelToBecome * 3, (castleLevelToBecome + 1) * 3 + 1);
        DataManager.guildLv = Random.Range(castleLevelToBecome * 3, (castleLevelToBecome + 1) * 3 + 1);
        DataManager.forgeLv = Random.Range(castleLevelToBecome * 3, (castleLevelToBecome + 1) * 3 + 1);

        for (int x = 0; x < 5; x++)
        {
            DataManager.minesLv[x] = Random.Range(castleLevelToBecome * 3, (castleLevelToBecome + 1) * 3 + 1);
        }

    }

    void Update()
    {
        
    }
}
