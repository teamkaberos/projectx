﻿using System.Collections.Generic;
using UnityEngine;

public class LootManager : MonoBehaviour 
{
    public delegate void NewHeroReceivedFromIntervalReward(string heroName, Sprite heroImage);
    public static event NewHeroReceivedFromIntervalReward NewHeroFromIntervalReward;

    #region singleton
    private static LootManager _instance;
	public  static LootManager Instance
	{
		get
		{
			return _instance;
		}
    }
    void Awake()
    {
        _instance = this;
    }

    #endregion

    public enum ItemType { consumable, heroCard };
    public enum CardRarity { Uncommon, Rare, Epic, Legendary };
    public enum DropAreas { intervalReward, jackPack, queenPack, kingPack }

    public ChatBoxContent systemChat;

    [System.Serializable]
	public class Dropables
	{
		public string name;
        public Sprite lootImage;
        public ItemType itemType;
        public CardRarity cardRarity;
        public DropAreas[] eligibleForDrop;
        public bool checkRequired;
        public string textColor;
		public int itemWeight;
		public int itemGrade;
	}

	public List <Dropables> dropTable = new List <Dropables> ();
	List<Dropables> rareDrops = new List <Dropables>();
    List<Dropables> epicDrops = new List<Dropables>();

    List<Dropables> rareCards = new List<Dropables>();
    List<Dropables> epicCards = new List<Dropables>();
    List<Dropables> legendaryCards = new List<Dropables>();

    public static int rareAndAboveCounter;
    public static int epicAndAboveCounter;

	bool isAssigning;
	int cardAssigning;

	public static string rewardRolled;
    public static Sprite rewardGottenSprite;

    public static bool newHeroReceived;
    public static int newHeroID;



	void Start()
	{
		//Compile all rare cards here
		for (int x = 0; x<dropTable.Count; x++)
		{
			if (dropTable[x].itemGrade >=3)
			{
                rareDrops.Add(dropTable[x]);

                if (dropTable[x].itemGrade >= 4)
                    epicDrops.Add(dropTable[x]);
			}

            if (dropTable[x].itemGrade == 3)
            {
                if (dropTable[x].itemType == ItemType.heroCard)
                    rareCards.Add(dropTable[x]);
            }

            if (dropTable[x].itemGrade == 4)
            {
                if (dropTable[x].itemType == ItemType.heroCard)
                    epicCards.Add(dropTable[x]);
            }

            if (dropTable[x].itemGrade == 5)
            {
                if (dropTable[x].itemType == ItemType.heroCard)
                    legendaryCards.Add(dropTable[x]);
            }
		}
	}

	public void OpenLoot(string typeOfLoot)
	{
		Player.Instance.DeductLoot(typeOfLoot);
	}

	public void Get_Interval_Reward()
	{
        CalculateIntervalRewardLoot();
	}

    void CalculateIntervalRewardLoot()
    {
        //Get all the items the player is eligible for 
        int lootNumber = 0;
        newHeroReceived = false;

        for (int x = 0; x < dropTable.Count; x++)
        {
            if (dropTable[x].checkRequired != true)
            {
                for (int y = 0; y < dropTable[x].eligibleForDrop.Length; y++)
                {
                    if (dropTable[x].eligibleForDrop[y] == DropAreas.intervalReward)
                    {
                        lootNumber += dropTable[x].itemWeight;
                        break;
                    }
                }
            }
            // checks if the item is required to be checked by hero level 
            // and if the card is available to be calculated based on hero level
            else if (dropTable[x].checkRequired == true & dropTable[x].cardRarity <= HeroLevelCheck())
            {
                for (int y = 0; y < dropTable[x].eligibleForDrop.Length; y++)
                {
                    if (dropTable[x].eligibleForDrop[y] == DropAreas.intervalReward)
                    {
                        lootNumber += dropTable[x].itemWeight;
                        break;
                    }
                }
            }
        }

        int randItem = Random.Range(0, lootNumber);

        for (int y = 0; y < dropTable.Count; y++)
        {
            // if number falls in here
            if (randItem <= dropTable[y].itemWeight)
            {
                Reward(dropTable[y].name);
                if (newHeroReceived == true)
                    NewHeroFromIntervalReward(dropTable[y].name, UnitManager.Instance.heroInfomation[newHeroID].newHeroImage);
                break;
            }
            randItem -= dropTable[y].itemWeight;
        }

        UIManager.Instance.UpdatePlayerTabInfo();
        UIManager.Instance.UpdatePlayerInfo();
        UIManager.Instance.Update_All();
        UnitManager.Instance.DistributeStats();
        UnitManager.Instance.SaveHeroInfo();
    }

    CardRarity HeroLevelCheck()
    {
        if (Player.castleLevel <= 2)
            return CardRarity.Uncommon;
        if (Player.castleLevel == 3 || Player.castleLevel == 4)
            return CardRarity.Rare;
        if (Player.castleLevel == 5)
            return CardRarity.Epic;
        return CardRarity.Legendary;
    }
	public void OpenChest(int typeOfChest)
	{
		Player.jackPack+=1;
		Player.heroTimer15+=3;
		Player.timer3 += 20;
		if (typeOfChest > 0)
		{
			Player.queenPack+=1;
			Player.heroTimer15+=2;
			Player.heroTimer30+=2;
			Player.timer3 += 20;
			Player.timer15+=15;
			if (typeOfChest > 1)
			{
				Player.kingPack+= 1;
				Player.heroTimer15+=5;
				Player.heroTimer30+=3;
				Player.heroTimer60+=3;
				Player.timer3+= 20;
				Player.timer15+=15;
				Player.timer30+=10;
			}
		}
		if (Player.accountType == "Guest")
			Player.SavePrefs();
	}

	public void CalculateLoot(int contentAmount)
	{
		// reset counter values everytime 
		// player opens a loot crate
		rareAndAboveCounter = 0;
		epicAndAboveCounter = 0;
        
		//any cards provides players a chance to get a card of any rarity
		for (int x = 0; x < contentAmount ; x ++)
		{
			//reset item total item weight value on every loop
			int totalItemWeight = 0;
            // add all weights together
            for (int y = 0; y < dropTable.Count; y++)
            {
                for(int z = 0; z < dropTable[y].eligibleForDrop.Length; z++)
                {
                    if (EligibilityCheck(contentAmount, dropTable[y].eligibleForDrop[z]) == true)
                    {
                        totalItemWeight += dropTable[y].itemWeight;
                        break;
                    }
                }
            }

			int dropChance = Random.Range(0 , totalItemWeight);
			// Loop through all the items and see which item player got based on roll
			for (int z = 0; z < dropTable.Count; z++)
			{
				//Check the pack player is currently opening now
				if (contentAmount == 8)	//If this is Queen Pack
				{
					if (x == (contentAmount-1) && rareAndAboveCounter < 1)
					{
                        ForceRareCard(contentAmount);
						break;
					}
				}
				if (contentAmount == 10) //If this is King Pack
				{
					if (x >= (contentAmount-3))
					{
						if (rareAndAboveCounter < 2)
						{
                            ForceRareCard(contentAmount);
							break;
						}
						if (epicAndAboveCounter < 1)
						{
                            ForceEpicCard(contentAmount);
							break;
						}
					}
				}
				// if items falls in this category
				if (dropChance <= dropTable[z].itemWeight)
				{
                    if (dropTable[z].itemType == ItemType.heroCard & dropTable[z].itemGrade == 3)
                        rareAndAboveCounter += 1;
                    if (dropTable[z].itemType == ItemType.heroCard & dropTable[z].itemGrade >= 4)
                    {
                        rareAndAboveCounter += 1;
                        epicAndAboveCounter += 1;
                    }

                    newHeroReceived = false;
					//give this item to player
					Reward (dropTable[z].name);
                    UIManager.Instance.AssignTavern_Reward_Names(dropTable[z].name,
                        contentAmount,dropTable[z].lootImage,dropTable[z].textColor, newHeroReceived, newHeroID);

					break;
				}
				dropChance -= dropTable[z].itemWeight;
			}
		}

		UIManager.Instance.UpdatePlayerTabInfo();
        UIManager.Instance.UpdateCurrencyInfo();
		UIManager.Instance.UpdatePlayerInfo();
        UIManager.Instance.Update_All();

        UnitManager.Instance.DistributeStats();
		UnitManager.Instance.SaveHeroInfo();

        DataManager.Instance.PrepareDataForSparksPushing();
        GameSparksManager.Instance.StorePackRewards();
	}

    bool EligibilityCheck(int contentAmount, DropAreas dropArea)
    {
        if (contentAmount == 6 && dropArea == DropAreas.jackPack)
            return true;
        else if (contentAmount == 8 && dropArea == DropAreas.queenPack)
            return true;
        else if (contentAmount == 10 && dropArea == DropAreas.kingPack)
            return true;
        return false;
    }

	//Force Rare/Epic Drops for Queen/King packs
	public void ForceRare(int contentAmount)
	{
		int rareWeight = 0;
		//add all rare item weights
		for (int y = 0 ; y < rareDrops.Count ; y ++)
		{
			rareWeight += rareDrops[y].itemWeight;
		}

		int rareDropChance = Random.Range (0,rareWeight);

		for (int x = 0; x < rareDrops.Count; x++)
		{
			if (rareDropChance <= rareDrops[x].itemWeight)
			{
				//give rare item to player here
				Reward(rareDrops[x].name);
                UIManager.Instance.AssignTavern_Reward_Names(rareDrops[x].name,
                    contentAmount,rareDrops[x].lootImage,rareDrops[x].textColor, newHeroReceived, newHeroID);
                rareAndAboveCounter += 1;
				return;
			}
			rareDropChance -= rareWeight;
		}
	}
    public void ForceRareCard(int contentAmount)
    {
        int rareCardWeight = 0;
        List<Dropables> rareAndEpics = new List<Dropables>();

        //add all rare item weights
        for (int y = 0; y < rareCards.Count; y++)
        {
            rareCardWeight += rareCards[y].itemWeight;
            rareAndEpics.Add(rareCards[y]);
        }
        for (int y = 0; y < epicCards.Count; y++)
        {
            rareCardWeight += epicCards[y].itemWeight;
            rareAndEpics.Add(epicCards[y]);
        }

        int rareDropChance = Random.Range(0, rareCardWeight);

        for (int x = 0; x < rareAndEpics.Count; x++)
        {
            if (rareDropChance <= rareAndEpics[x].itemWeight)
            {
                //give rare item to player here
                Reward(rareAndEpics[x].name);
                UIManager.Instance.AssignTavern_Reward_Names(rareAndEpics[x].name,
                    contentAmount, rareAndEpics[x].lootImage, rareAndEpics[x].textColor, newHeroReceived, newHeroID);
                rareAndAboveCounter += 1;
                return;
            }
            rareDropChance -= rareAndEpics[x].itemWeight;
        }
    }
	public void ForceEpic(int contentAmount)
	{
		int epicWeight = 0;
		//add all rare item weights
		for (int y = 0 ; y < epicDrops.Count ; y ++)
		{
			epicWeight += epicDrops[y].itemWeight;
		}

		int epicDropChance = Random.Range (0,epicWeight);

		for (int x = 0; x < epicDrops.Count; x++)
		{
			if (epicDropChance <= epicDrops[x].itemWeight)
			{
				//give rare item to player here
				Reward(epicDrops[x].name);
                UIManager.Instance.AssignTavern_Reward_Names(epicDrops[x].name,
                    contentAmount,epicDrops[x].lootImage, epicDrops[x].textColor, newHeroReceived, newHeroID);
                epicAndAboveCounter += 1;
				return;
			}
			epicDropChance -= epicWeight;
		}
    }
    public void ForceEpicCard(int contentAmount)
    {
        int epicCardWeight = 0;
        List<Dropables> epicAndLegendaries = new List<Dropables>();

        //add all rare item weights
        for (int y = 0; y < epicCards.Count; y++)
        {
            epicCardWeight += epicCards[y].itemWeight;
            epicAndLegendaries.Add(epicCards[y]);
        }
        for (int y = 0; y < legendaryCards.Count; y++)
        {
            epicCardWeight += legendaryCards[y].itemWeight;
            epicAndLegendaries.Add(legendaryCards[y]);
        }

        int epicDropChance = Random.Range(0, epicCardWeight);

        for (int x = 0; x < epicAndLegendaries.Count; x++)
        {
            if (epicAndLegendaries[x].itemType != ItemType.heroCard)
                continue;
            if (epicDropChance <= epicAndLegendaries[x].itemWeight)
            {
                //give rare item to player here
                Reward(epicAndLegendaries[x].name);
                UIManager.Instance.AssignTavern_Reward_Names(epicAndLegendaries[x].name,
                    contentAmount, epicAndLegendaries[x].lootImage, epicAndLegendaries[x].textColor, newHeroReceived, newHeroID);
                rareAndAboveCounter += 1;
                epicAndAboveCounter += 1;
                return;
            }
            epicDropChance -= epicAndLegendaries[x].itemWeight;
        }
    }

  
    // The Reward method is in charge of adding to the Player's info and then saving to the Cloud
	public void Reward(string reward)
	{
		rewardRolled = reward;
        for (int x = 0; x < dropTable.Count; x++)
        {
            if (dropTable[x].name == reward)
            {
                rewardGottenSprite = dropTable[x].lootImage;
                break;
            }
        }
		switch (reward)
		{
		case "Earth Stone":
			    Player.squareStone += 1;
                DataManager.squareStones += 1;
                break;
		case "Flame Stone":
			    Player.circleStone += 1;
                DataManager.circleStones += 1;
                break;
		case "Wind Stone":
			Player.triangleStone += 1;
                DataManager.triangleStones += 1;
			break;
		case "15 Reputation":
                Player.Instance.AlterRepValue(true, 15);
			break;
		case "20x 3min Timers":
			Player.timer3 += 20;
                UIManager.Instance.UpdateSpeedUpInfo();
			break;
		case "5x 15min Timers":
                Player.timer15 += 5;
                UIManager.Instance.UpdateSpeedUpInfo();
			break;
		case "3x 30min Timers":
                Player.timer30 += 3;
                UIManager.Instance.UpdateSpeedUpInfo();
			break;
		case "2x 60min Timers":
                Player.timer60 += 2;
                UIManager.Instance.UpdateSpeedUpInfo();
			break;
        case "10 Diamond":
                Player.Instance.GainDiamond(10);
            break;
        case "20 Diamond":
                Player.Instance.GainDiamond(20);
                break;
        case "30 Diamond":
                Player.Instance.GainDiamond(30);
                break;
        case "15 Excalipoints":
                Player.Instance.ExcaliPoint_Calculator(true, 15);
                break;
        case "50 Excalipoints":
                Player.Instance.ExcaliPoint_Calculator(true, 50);
                break;
        case "80 Excalipoints":
                Player.Instance.ExcaliPoint_Calculator(true, 80);
                break;
        case "120 Excalipoints":
                Player.Instance.ExcaliPoint_Calculator(true, 120);
                break;
		case "3 JackPack":
			Player.jackPack += 3;
			break;
		case "2 QueenPack":
			Player.queenPack += 2;
			break;
		case "1 KingPack":
			Player.kingPack += 1;
			break;
		case "Minotaur":
            UnitManager.heroCardsAmount[0] += 1;
                if (UnitManager.heroUnlocked[UnitManager.Instance.heroInfomation[0].heroNo] != 1)
                {
                    Player.heroUnlocked[UnitManager.Instance.heroInfomation[0].heroNo] = 1;
                    Player.heroLevels[UnitManager.Instance.heroInfomation[0].heroNo] = 1;
                    UnitManager.Instance.UnlockHero(UnitManager.Instance.heroInfomation[0].heroNo);
                    newHeroReceived = true;
                }
                newHeroID = 0;
                break;
		case "Tigera":
                UnitManager.heroCardsAmount[1] += 1;

                if (UnitManager.heroUnlocked[UnitManager.Instance.heroInfomation[1].heroNo] != 1)
                {
                    Player.heroUnlocked[UnitManager.Instance.heroInfomation[1].heroNo] = 1;
                    Player.heroLevels[UnitManager.Instance.heroInfomation[1].heroNo] = 1;
                    UnitManager.Instance.UnlockHero(UnitManager.Instance.heroInfomation[1].heroNo);
                    newHeroReceived = true;
                }
                newHeroID = 1;
                break;
		case "Tigerex":
                UnitManager.heroCardsAmount[2] += 1;

                if (UnitManager.heroUnlocked[UnitManager.Instance.heroInfomation[2].heroNo] != 1)
                {
                    Player.heroUnlocked[UnitManager.Instance.heroInfomation[2].heroNo] = 1;
                    Player.heroLevels[UnitManager.Instance.heroInfomation[2].heroNo] = 1;
                    UnitManager.Instance.UnlockHero(UnitManager.Instance.heroInfomation[2].heroNo);
                    newHeroReceived = true;
                }
                newHeroID = 2;
                break;
		case "Golem":
                UnitManager.heroCardsAmount[3] += 1;

                if (UnitManager.heroUnlocked[UnitManager.Instance.heroInfomation[3].heroNo] != 1)
                {
                    Player.heroUnlocked[UnitManager.Instance.heroInfomation[3].heroNo] = 1;
                    Player.heroLevels[UnitManager.Instance.heroInfomation[3].heroNo] = 1;
                    UnitManager.Instance.UnlockHero(UnitManager.Instance.heroInfomation[3].heroNo);
                    newHeroReceived = true;
                }
                newHeroID = 3;
                break;
		case "Griolem":
                UnitManager.heroCardsAmount[4] += 1;

                if (UnitManager.heroUnlocked[UnitManager.Instance.heroInfomation[4].heroNo] != 1)
                {
                    Player.heroUnlocked[UnitManager.Instance.heroInfomation[4].heroNo] = 1;
                    Player.heroLevels[UnitManager.Instance.heroInfomation[4].heroNo] = 1;
                    UnitManager.Instance.UnlockHero(UnitManager.Instance.heroInfomation[4].heroNo);
                    newHeroReceived = true;
                }
                newHeroID = 4;
                break;
		case "Priestess":
                UnitManager.heroCardsAmount[5] += 1;

                if (UnitManager.heroUnlocked[UnitManager.Instance.heroInfomation[5].heroNo] != 1)
                {
                    Player.heroUnlocked[UnitManager.Instance.heroInfomation[5].heroNo] = 1;
                    Player.heroLevels[UnitManager.Instance.heroInfomation[5].heroNo] = 1;
                    UnitManager.Instance.UnlockHero(UnitManager.Instance.heroInfomation[5].heroNo);
                    newHeroReceived = true;

                }
                newHeroID = 5;
                break;
		case "Angel":
                UnitManager.heroCardsAmount[6] += 1;

                if (UnitManager.heroUnlocked[UnitManager.Instance.heroInfomation[6].heroNo] != 1)
                {
                    Player.heroUnlocked[UnitManager.Instance.heroInfomation[6].heroNo] = 1;
                    Player.heroLevels[UnitManager.Instance.heroInfomation[6].heroNo] = 1;
                    UnitManager.Instance.UnlockHero(UnitManager.Instance.heroInfomation[6].heroNo);
                    newHeroReceived = true;

                }
                newHeroID = 6;
                break;
		case "DarkKnight":
                UnitManager.heroCardsAmount[7] += 1;

                if (UnitManager.heroUnlocked[UnitManager.Instance.heroInfomation[7].heroNo] != 1)
                {
                    Player.heroUnlocked[UnitManager.Instance.heroInfomation[7].heroNo] = 1;
                    Player.heroLevels[UnitManager.Instance.heroInfomation[7].heroNo] = 1;
                    UnitManager.Instance.UnlockHero(UnitManager.Instance.heroInfomation[7].heroNo);

                }
                newHeroID = 7;
                break;	
		case "Dark Elf":
                UnitManager.heroCardsAmount[8] += 1;

                if (UnitManager.heroUnlocked[UnitManager.Instance.heroInfomation[8].heroNo] != 1)
                {
                    Player.heroUnlocked[UnitManager.Instance.heroInfomation[8].heroNo] = 1;
                    Player.heroLevels[UnitManager.Instance.heroInfomation[8].heroNo] = 1;
                    UnitManager.Instance.UnlockHero(UnitManager.Instance.heroInfomation[8].heroNo);
                    newHeroReceived = true;

                }
                newHeroID = 8;
                break;
		case "Genie":
                UnitManager.heroCardsAmount[9] += 1;

                if (UnitManager.heroUnlocked[UnitManager.Instance.heroInfomation[9].heroNo] != 1)
                {
                    Player.heroUnlocked[UnitManager.Instance.heroInfomation[9].heroNo] = 1;
                    Player.heroLevels[UnitManager.Instance.heroInfomation[9].heroNo] = 1;
                    UnitManager.Instance.UnlockHero(UnitManager.Instance.heroInfomation[9].heroNo);
                    newHeroReceived = true;

                }
                newHeroID = 9;
                break;
		case "Succubus":
                UnitManager.heroCardsAmount[10] += 1;

                if (UnitManager.heroUnlocked[UnitManager.Instance.heroInfomation[10].heroNo] != 1)
                {
                    Player.heroUnlocked[UnitManager.Instance.heroInfomation[10].heroNo] = 1;
                    Player.heroLevels[UnitManager.Instance.heroInfomation[10].heroNo] = 1;
                    UnitManager.Instance.UnlockHero(UnitManager.Instance.heroInfomation[10].heroNo);
                    newHeroReceived = true;

                }
                newHeroID = 10;
                break;
		case "Werewolf":
                UnitManager.heroCardsAmount[11] += 1;

                if (UnitManager.heroUnlocked[UnitManager.Instance.heroInfomation[11].heroNo] != 1)
                {
                    Player.heroUnlocked[UnitManager.Instance.heroInfomation[11].heroNo] = 1;
                    Player.heroLevels[UnitManager.Instance.heroInfomation[11].heroNo] = 1;
                    UnitManager.Instance.UnlockHero(UnitManager.Instance.heroInfomation[11].heroNo);
                    newHeroReceived = true;

                }
                newHeroID = 11;
                break;
		case "Ninja":
                UnitManager.heroCardsAmount[12] += 1;

                if (UnitManager.heroUnlocked[UnitManager.Instance.heroInfomation[12].heroNo] != 1)
                {
                    Player.heroUnlocked[UnitManager.Instance.heroInfomation[12].heroNo] = 1;
                    Player.heroLevels[UnitManager.Instance.heroInfomation[12].heroNo] = 1;
                    UnitManager.Instance.UnlockHero(UnitManager.Instance.heroInfomation[12].heroNo);
                    newHeroReceived = true;
                }
                newHeroID = 12;
                break;
		case "Goblin":
                UnitManager.heroCardsAmount[13] += 1;

                if (UnitManager.heroUnlocked[UnitManager.Instance.heroInfomation[13].heroNo] != 1)
                {
                    Player.heroUnlocked[UnitManager.Instance.heroInfomation[13].heroNo] = 1;
                    Player.heroLevels[UnitManager.Instance.heroInfomation[13].heroNo] = 1;
                    UnitManager.Instance.UnlockHero(UnitManager.Instance.heroInfomation[13].heroNo);
                    newHeroReceived = true;

                }

                newHeroID = 13;
                break;
    			//TODO morpher not available yet
                //	case "Morpher":
                //	HeroManager.heroCardsAmount[14] += 1;
                //break;
		case "Naga":
                UnitManager.heroCardsAmount[15] += 1;

                if (UnitManager.heroUnlocked[UnitManager.Instance.heroInfomation[15].heroNo] != 1)
                {
                    Player.heroUnlocked[UnitManager.Instance.heroInfomation[15].heroNo] = 1;
                    Player.heroLevels[UnitManager.Instance.heroInfomation[15].heroNo] = 1;
                    UnitManager.Instance.UnlockHero(UnitManager.Instance.heroInfomation[15].heroNo);
                    newHeroReceived = true;

                }
                newHeroID = 15;
                break;
		case "Panthera":
                UnitManager.heroCardsAmount[16] += 1;

                if (UnitManager.heroUnlocked[UnitManager.Instance.heroInfomation[16].heroNo] != 1)
                {
                    Player.heroUnlocked[UnitManager.Instance.heroInfomation[16].heroNo] = 1;
                    Player.heroLevels[UnitManager.Instance.heroInfomation[16].heroNo] = 1;
                    UnitManager.Instance.UnlockHero(UnitManager.Instance.heroInfomation[16].heroNo);
                    newHeroReceived = true;

                }
                newHeroID = 16;
                break;
		}
        systemChat.DisplayMessageInChatbox("Successfully obtained " + reward);
        DataManager.Instance.PrepareDataForSparksPushing();
	}

	public string rewardGotten()
	{
		return rewardRolled;
	}
    public Sprite RewardGottenSprite()
    {
        return rewardGottenSprite;
    }
//	public void Distribute_Reward_Display(string rewardName, int rewardsAwarded)
//	{
//
//		//check which pack was awarded
//		if (rewardsAwarded == 10)//in the case this is the King Pack
//		{
//			cardAssigning = 10;
//
//		}
//	}
}
