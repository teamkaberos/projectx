﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Enemy : BaseManager
{
    public delegate void CastleDamage();
    public static event CastleDamage onCastleDestroy;

    public delegate void EnemyCastleDamage();
    public static event EnemyCastleDamage enemyHit;
    public static event EnemyCastleDamage enemyDead;

    #region Singleton

    private static Enemy _instance;
    public static Enemy Instance
    {
        get
        {
            if (_instance == null)
            {
                GameObject enemyManager = new GameObject("EnemyCastle");
                enemyManager.AddComponent<Enemy>();
            }
            return _instance;
        }
    }

    #endregion

    [Space(20)]
    [Header("Enemy Display Info")]


    // --- For storing information of loaded enemy --- \\
    public static string nameLoaded = "OPEN_AI";
    public static string race = "human";          //always in lowercase   1)human  2)undead
    public static int castleLevel = 1;
    public static int reputation = 9999;
    public static string clan = "<currently_not_in_any_clan_yet>";
    public static int gold;
    public int castleHealth = 3;
    public int castleMaxHealth;
    public float spawnInterval = 3.5f;       //Timing between each units spawn
    public float aiReactionTime = 4f;       // reaction time for AI
    int spawnCounts = 0;
    public int[] enemyArray;                     //storing type of units; 0 = melee ; 1 = ranged ; 2 = casters;
    bool isShuffling;

    public int energy;
    public int energyRefreshTime;
    public int diamond;
    public int excalipoints;


    public Text nameDisplay;
    public Text enemyAmountDisplay;
    public Text reputationDisplay;
    public Text castleLevelDisplay;
    public Text shufflingTimerDisplay;



    [Space(20)]
    // =========================== \\
    [Header("Flags")]
    public Transform[] flags;
    [Header("Enemies")]
    public Transform[] enemyPrefabs;
    [Header("SpawnLocators")]
    public Transform[] spawnLocs;

    bool stillUnderAttack;

    public int[] randomLoc;
    public int locIndex;
    int randomSpawn;

    public bool manual_Turn_On;

    // For getting the correct lane ID not restricted to the order of the objects from the order in the array
    public static int newLocIndex;

    public static List<int> troopsAssigned = new List<int>();
    public static List<int> troopsAssignedLevels = new List<int>();

    static List<int> tempAssignedTroopList = new List<int>();
    static List<int> tempAssignedTroopsLevelList = new List<int>();

    public static int lumberLoaded;

    public static float shuffleTiming = 15f;                                                                //Value for when enemy decks are shuffling *DO NOT USE THIS FOR TIMER*
    public static float shuffleReduction;                                     //Time that will be check when enemy deck begins shuffling
    public static float shuffleTimeForDisplay;                                                                  //To display the above timer
    // ================================================ \\

    public int[] manualTroopAssignment;
    public int[] manualTroopAssignedLevel;


    // ====== SUPPORT UNITS REQUIRED ====== \\
    public Text soulsDisplay;
    public int mySouls = 0;
    float summonInterval = 5f;                          // to determine how often the ENEMY will check for threats
    bool summonedSUnits;                                // to determine if ENEMY deployed sunits
    [Header("Mines Check")]
    int[] lanesClosest = { -100, -100, -100 };
    int nearThreats = -1;                                           // To determine how many mines to Plant
    readonly int closeThreshold;                                // How far should a unit be to be considered a Threat
    [Header("Dragon Check")]
    int[] lanesTotalHostileUnits = { 0, 0, 0 };
    int massThreats = -1;                                          // To determine how mnay lanes to fire
    readonly int massThreshold = 2;                         // How many units should there be in a lane to be considered a Threat
    [Header("Seige Check")]
    int tries = 2;



    void Awake()
    {
        _instance = this;
        if (SceneManager.GetActiveScene().name == "3_Battle")
        {
            nameDisplay.text = nameLoaded;
            enemyAmountDisplay.text = "" + troopsAssigned.Count;
            shuffleTimeForDisplay = shuffleTiming;
            shufflingTimerDisplay.text = "" + shuffleTimeForDisplay;
        }
    }

    new void Start()
    {
        #region for manual test
        if (manual_Turn_On)
        {
            for (int x = 0; x < manualTroopAssignment.Length; x++)
            {
                if (manualTroopAssignment[x] != 250)
                {
                    troopsAssigned.Add(manualTroopAssignment[x]);
                    troopsAssignedLevels.Add(manualTroopAssignedLevel[x]);
                }
            }
            enemyAmountDisplay.text = "" + troopsAssigned.Count;
        }
        #endregion
       
        if (Player.tutorial >= Player.tutorialCap)
        {
            // Regular Enemy Spawns
            New_Shuffle_Enemies();
            InvokeRepeating("NewEnemySpawn", spawnInterval + aiReactionTime , spawnInterval + aiReactionTime);
            //InvokeRepeating("AttemptDeploySupportUnits", 8f, summonInterval + aiReactionTime);
        }
        else if (Player.tutorial == 4)
        {
            // Tutorial Enemy Spawns
            Forced_Enemy_Shuffle();
            InvokeRepeating("NewEnemySpawn", 1f, 11f);
        }
        ResetSoul();
        AssignFlags();
    }

    void Forced_Enemy_Shuffle()
    {
        troopsAssigned[0] = 101;
        troopsAssigned[1] = 102;
        troopsAssigned[2] = 100;
    }

	void New_Shuffle_Enemies()
	{
		//Start shuffling the cards in the temp shuffle list
		int times_To_Shuffle = tempAssignedTroopList.Count;
		for (int x = 0; x < times_To_Shuffle; x++)
		{
			System.Random rnd = new System.Random();
			int r = rnd.Next(0,troopsAssigned.Count);

			tempAssignedTroopList.Add(troopsAssigned[r ]);
			tempAssignedTroopsLevelList.Add(troopsAssignedLevels[r]);

			troopsAssigned.RemoveAt(r);
			troopsAssignedLevels.RemoveAt(r);
		}
		for (int y = 0; y < times_To_Shuffle; y ++)
		{
			troopsAssigned.Add(tempAssignedTroopList[y]);
			troopsAssignedLevels.Add(tempAssignedTroopsLevelList[y]);
		}
	}

	void NewEnemySpawn()
	{
		int spawnLane = ShuffleEnemyLocators();
        //do the actual spawning here
        //but check if its a hero or regular unit first
        if (spawnLane < 3)
        {
            if (troopsAssigned[spawnCounts] >= 100)
                TroopManager.Instance.New_Spawn_Enemy_Units_On_Lane(troopsAssigned[spawnCounts], spawnLane);
            else
                TroopManager.Instance.New_Spawn_Enemy_Heroes_On_Lane(troopsAssigned[spawnCounts], spawnLane);

    		//increase the spawncounts
    		spawnCounts += 1;
            // if spawncounts is same as the number of troops the enemy brought into battle, the counter resets
            if (spawnCounts == troopsAssigned.Count)
            {
                if (Player.tutorial == 4)
                {
                    CancelInvoke("NewEnemySpawn");
                    return;
                }
                isShuffling = true;
                spawnCounts = 0;
                CancelInvoke("NewEnemySpawn");
                InvokeRepeating("NewEnemySpawn", shuffleTiming + aiReactionTime, spawnInterval + aiReactionTime);
            }
		}
	}

	int ShuffleEnemyLocators()
	{
        // tutorial enemy
        if (Player.tutorial == 4)
            return 1;

        List<int> freeLanes = new List<int>();
        for (int x = 0; x < 3; x++)
        {
            //check if the lane is able to spawn a unit
            if (GameManager.Instance.LaneIsBlocked(x, false) != true && GameManager.Instance.LaneIsMinosBlocked(x, false) != true)
                freeLanes.Add(x);
        }

        if (freeLanes.Count >= 1)
        {
            locIndex = Random.Range(0, freeLanes.Count);
            return freeLanes[locIndex];
        }

        return 100;
	}

	public void SetNewShuffleTime()
	{
		shuffleTimeForDisplay = shuffleTiming - shuffleReduction;
		shufflingTimerDisplay.text =  string.Format("{0:00}",shuffleTimeForDisplay);
	}

    public void CancelSpawning()
    {
        CancelInvoke("NewEnemySpawn");
    }

	void Update()
	{
		if (isShuffling)
		{
			shuffleTimeForDisplay-=Time.deltaTime;
			if (shuffleTimeForDisplay <= 0)
			{
				shuffleTimeForDisplay = shuffleTiming;
				isShuffling = false;
			}
			shufflingTimerDisplay.text = string.Format("{0:00}",shuffleTimeForDisplay);
		}
	}

    // --- PLAYERDATA STUFFS --- \\

    // -------------------------------- BATTLE SCENE -------------------------------- \\

    //	Basically to Damage the enemy castle
    public void Damage(int dmgAmt)
	{
		//This is when the player hits and damages the enemies castle
		castleHealth -= dmgAmt;
        enemyHit();

        if (castleHealth < 0)
            return;

		// When the health reaches 0...
		if (castleHealth == 0) 
		{
			onCastleDestroy();
			enemyDead();

            Player.Instance.Battles_Entered_Count(true);
            AudioManager.Instance.VictoryJingle();
            GameManager.Instance.BattleOutcome(true);
            CancelSpawning();

			Player.Instance.RepValue(true);
		}
	}

	void AssignFlags()
	{
		castleHealth = 0;
		castleMaxHealth = 0;

        if (castleLevel <= 2)
		{
            if (Player.tutorial == 4)
            {
                castleHealth = 1;
                castleMaxHealth = 1;
            }
            else
            {
                castleHealth = 3;
                castleMaxHealth = 3;
            }
		}
		else if (castleLevel == 3 || castleLevel == 4)
		{
			castleHealth = 4;
			castleMaxHealth = 4;
		}
		else if (castleLevel >= 5)
		{
			castleHealth = 5;
			castleMaxHealth = 5;
		}

		Assign_My_Flags();
	}
    void ResetSoul()
    {
        mySouls = 0;
        UpdateSouls();
    }

    public void IncreaseSouls()
    {
        mySouls += 1;
        if (mySouls > Player.soulChargeCap)
            mySouls = Player.soulChargeCap;
        UpdateSouls();
    }
    public void SpendSouls(int cost)
    {
        mySouls -= cost;
        UpdateSouls();
    }
    void UpdateSouls()
    {
        soulsDisplay.text = "Souls : " + mySouls;
    }

    void AttemptDeploySupportUnits()
    {
        // DEAL AN INSTANT BLOW IF THE PLAYER HAS ONLY 1 HEALTH LEFT
        if (Player.castleHealth == 1 && mySouls >= Player.seigeRequire[2] && castleLevel >= 6)
        {
            // === INSTANT WIN FORMULA === \\
            summonedSUnits = true;
            FXManager.Instance.BallistaBoomzE();
            Player.Instance.Damage(1);
            UIManager.Instance.HideFlag();
            return;
            // === INSTANT WIN FORMULA END === \\
        }
        // ==== GET THE CLOSEST UNITS ON EACH LANE =====\\
        for (int x = GameManager.Instance.laneAColliderUnits.Length - 2 ; x > closeThreshold ; x--)
        {
            if (GameManager.Instance.laneAColliderUnits[x].playerUnit != null)
            {
                lanesClosest[0] = x;
                break;
            }
        }
        for (int x = GameManager.Instance.laneBColliderUnits.Length - 2; x > closeThreshold; x--)
        {
            if (GameManager.Instance.laneBColliderUnits[x].playerUnit != null)
            {
                lanesClosest[1] = x;
                break;
            }
        }
        for (int x = GameManager.Instance.laneCColliderUnits.Length - 2; x > closeThreshold; x--)
        {
            if (GameManager.Instance.laneCColliderUnits[x].playerUnit != null)
            {
                lanesClosest[2] = x;
                break;
            }
        }
        // ==== FIND OUT HOW MANY LANES HAS THREAT ==== \\
        if (lanesClosest[0] != -100)
            nearThreats += 1;
        if (lanesClosest[1] != -100)
            nearThreats += 1;
        if (lanesClosest[2] != -100)
            nearThreats += 1;
        if (lanesClosest[0] == -100 & lanesClosest[1] == -100 & lanesClosest[2] == -100)
        {
        }
        // ==== GET THE AMOUNT OF UNITS ON EACH LANE ==== \\
        foreach( GameObject units in GameManager.Instance.laneAGrids)
        {
            if (units != null)
            {
                ColliderUnit unit = units.GetComponent<ColliderUnit>();
                if (unit.playerUnit != null)
                {
                    lanesTotalHostileUnits[0] += 1;
                }
            }
        }
        foreach (GameObject units in GameManager.Instance.laneBGrids)
        {
            if (units != null)
            {
                ColliderUnit unit = units.GetComponent<ColliderUnit>();
                if (unit.playerUnit != null)
                {
                    lanesTotalHostileUnits[1] += 1;
                }
            }
        }
        foreach (GameObject units in GameManager.Instance.laneCGrids)
        {
            if (units != null)
            {
                ColliderUnit unit = units.GetComponent<ColliderUnit>();
                if (unit.playerUnit != null)
                {
                    lanesTotalHostileUnits[2] += 1;
                }
            }
        }
        // ==== FIND OUT HOW MANY LANES HAS MORE THAN 3 UNITS ==== \\
        if (lanesTotalHostileUnits[0] >= massThreshold)
            massThreats += 1;
        if (lanesTotalHostileUnits[1] >= massThreshold)
            massThreats += 1;
        if (lanesTotalHostileUnits[2] >= massThreshold)
            massThreats += 1;
        if (lanesTotalHostileUnits[0] == 0 & lanesTotalHostileUnits[1] == 0 & lanesTotalHostileUnits[2] == 0)
        {
            // if there isn't a lane with mass threats
        }
        // ==== DEPLOY S-UNITS AREA ==== \\

        // ==== FIND OUT HOW MANY LAND MINES ENEMY CAN USE ==== \\
       
        for (int x = Player.minesRequire.Length; x > 0; x--)
        {
            if (nearThreats >= 0 && summonedSUnits != true)
            {
                if (mySouls >= Player.minesRequire[nearThreats])
                {
                    //find out which lane is the closest
                    if (nearThreats == 1)
                    {
                        if (castleLevel >= 2)
                        {
                            int toPlant = lanesClosest.Max();
                            int lane = lanesClosest.ToList().IndexOf(toPlant);
                            string lane_ = "";
                            if (lane == 0)
                                lane_ = "A";
                            else if (lane == 1)
                                lane_ = "B";
                            else if (lane == 2)
                                lane_ = "C";

                            toPlant += 1;       // plant on the next

                            GameManager.Instance.laneBColliderUnits[toPlant].SummonSingleGarrison(lane_ + toPlant.ToString(), Player.minesRequire[0], 1, "enemy");
                            summonedSUnits = true;
                            SpendSouls(Player.minesRequire[0]);
                            break;
                        }
                    }
                    if (nearThreats == 2)
                    {
                        if (castleLevel >= 4)
                        {

                            int toPlant = lanesClosest.Max();
                            toPlant += 1;
                            //FIND WHICH 2 LANES HAS UNITS
                            if (lanesClosest[0] > closeThreshold & lanesClosest[1] > closeThreshold)
                            {
                                // Lane A & B
                                GameManager.Instance.laneAColliderUnits[toPlant].SummonSingleGarrison("A"+toPlant.ToString(), Player.minesRequire[1], 2, "enemy");
                            }
                            if (lanesClosest[0] > closeThreshold & lanesClosest[2] > closeThreshold)
                            {
                                // Lane A & C
                                GameManager.Instance.laneCColliderUnits[toPlant].SummonSingleGarrison("C" + toPlant.ToString(), Player.minesRequire[1], 2, "enemy");
                            }
                            if (lanesClosest[1] > closeThreshold & lanesClosest[2] > closeThreshold)
                            {
                                // Lane B & C
                                GameManager.Instance.laneBColliderUnits[toPlant].SummonSingleGarrison("B" + toPlant.ToString(), Player.minesRequire[1], 2, "enemy");
                            }
                            summonedSUnits = true;
                            SpendSouls(Player.minesRequire[1]);
                            break;
                        }
                    }
                    if (nearThreats == 3)
                    {
                        if (castleLevel >= 6)
                        {
                            int toPlant = lanesClosest.Max();
                            toPlant += 1;
                            GameManager.Instance.laneBColliderUnits[toPlant].SummonSingleGarrison("B"+toPlant.ToString(), Player.minesRequire[2], 3, "enemy");
                            summonedSUnits = true;
                            SpendSouls(Player.minesRequire[1]);
                            break;
                        }
                    }    
                }
            }
        }
        for (int y = Player.dragsRequire.Length; y > 0; y--)
        {
            if (summonedSUnits != true)
            {
                if (massThreats >= 0)
                {
                    if (mySouls >= Player.dragsRequire[massThreats])
                    {
                        if (massThreats == 0)
                        {
                            if (castleLevel >= 2)
                            {
                                // if there is only 1 lane with a threat detected
                                summonedSUnits = true;
                                SpendSouls(Player.dragsRequire[0]);
                                int laneIndex = lanesTotalHostileUnits.ToList().IndexOf(lanesTotalHostileUnits.Max());
                                if (laneIndex == 0)
                                    GameManager.Instance.laneAColliderUnits[0].SummonDragon("A01", Player.dragsRequire[0], 1, "enemy");
                                if (laneIndex == 1)
                                    GameManager.Instance.laneBColliderUnits[0].SummonDragon("B01", Player.dragsRequire[0], 1, "enemy");
                                if (laneIndex == 2)
                                    GameManager.Instance.laneCColliderUnits[0].SummonDragon("C01", Player.dragsRequire[0], 1, "enemy");
                                break;
                            }
                        }
                        if (massThreats == 1)
                        {
                            if (castleLevel >= 4)
                            {
                                // FIND WHICH 2 LANES TO SUMMON DRAGON
                                summonedSUnits = true;
                                SpendSouls(Player.dragsRequire[1]);
                                if (lanesTotalHostileUnits[0] >= massThreshold & lanesTotalHostileUnits[1] >= massThreshold)
                                {
                                    //lane A && B
                                    GameManager.Instance.laneAColliderUnits[0].SummonDragon("A01", Player.dragsRequire[1], 2, "enemy");
                                }
                                if (lanesTotalHostileUnits[0] >= massThreshold & lanesTotalHostileUnits[2] >= massThreshold)
                                {
                                    //lane A && C
                                    GameManager.Instance.laneCColliderUnits[0].SummonDragon("C01", Player.dragsRequire[1], 2, "enemy");
                                }
                                if (lanesTotalHostileUnits[1] >= massThreshold & lanesTotalHostileUnits[2] >= massThreshold)
                                {
                                    //lane B && C
                                    GameManager.Instance.laneBColliderUnits[0].SummonDragon("B01", Player.dragsRequire[1], 2, "enemy");
                                }
                                break;
                            }
                        }
                        if (massThreats == 2)
                        {
                            if (castleLevel >= 6)
                            {
                                summonedSUnits = true;
                                SpendSouls(Player.dragsRequire[2]);
                                GameManager.Instance.laneBColliderUnits[0].SummonDragon("B01", Player.dragsRequire[2], 3, "enemy");
                                break;
                            }
                        }
                        massThreats -= 1;
                    }
                }
            }
        }

        // Siege summon
        if (summonedSUnits != true)
        {
            int chance = Random.Range(0, 100);
            for (int z = Player.seigeRequire.Length; z > 0; z--)
            {
                if (mySouls >= Player.seigeRequire[tries])
                {
                    if (castleLevel >= 6)
                    {
                        //attempt the most expensive seige first
                        summonedSUnits = true;
                        Player.Instance.Damage(1);
                        UIManager.Instance.HideFlag();
                        break;
                    }
                    if (castleLevel >= 4)
                    {
                        if (chance <= 32)
                        {
                            summonedSUnits = true;
                            Player.Instance.Damage(1);
                            UIManager.Instance.HideFlag();
                            break;
                        }
                    }
                    if (castleLevel >= 2)
                    {
                        if (chance <= 12)
                    {
                            summonedSUnits = true;
                            Player.Instance.Damage(1);
                            UIManager.Instance.HideFlag();
                            break;
                        }
                    }
                    FXManager.Instance.BallistaBoomzE();
                }
            }
        }

        ResetLanesThreatValues();
        if (summonedSUnits)
        {
            SUnitsCooldown();
        }
    }
    void ResetLanesThreatValues()
    {
        for (int x = 0; x < 3; x++)
        {
            lanesClosest[x] = -100;
            lanesTotalHostileUnits[x] = 0;
        }
        nearThreats = -1;
        massThreats = -1;
        tries = 0;
    }
    void SUnitsCooldown()
    {
        summonedSUnits = false;
        CancelInvoke("AttemptDeploySupportUnits");
        InvokeRepeating("AttemptDeploySupportUnits", 6f, summonInterval + aiReactionTime);
    }



    public void Check_Health()
	{
		if (castleHealth > castleMaxHealth)
			castleHealth = castleMaxHealth;
		if (castleHealth <= 0)
			castleHealth = 0;

		Assign_My_Flags();
	}

	void Assign_My_Flags()
	{
		for (int i = 0; i < castleHealth; i++) 
		{
			flags [i].gameObject.SetActive (true);
		}
	}

//	 When a unit reaches a castle
	void OnTriggerEnter(Collider other)
	{
		if (name.Contains ("Castle") & stillUnderAttack == false) 
		{
			// ENEMY CASTLE DAMAGE
			if (other.GetComponent<BaseUnit> ().isFriendlyUnit == true & gameObject.GetComponent<BaseUnit> ().isFriendlyUnit == false)
			{
				if (SceneManager.GetActiveScene ().name == "3_Battle") 
				{
					stillUnderAttack = true;
					Damage (1);
					Enemy enemyCastle = GetComponent<Enemy> ();
					
                    if (enemyCastle != null)
					{
						UIManager.Instance.HideEnemyFlag();
					}

					Hero hero = other.GetComponent<Hero> ();
					if (hero != null)
					{
						if (hero.heroType == "Speeder")
						{
							Damage(1);
							if (enemyCastle != null)
							{
								UIManager.Instance.HideEnemyFlag();
							}
						}
					}
					stillUnderAttack = false;
                    other.GetComponent<Unit>().FadingOutUnit(2.2f);
                }
			}
		}
	}

	void OnTriggerExit(Collider other)
	{
		//if (GetComponent<BaseUnit> ().isFriendlyUnit != other.GetComponent<Unit> ().isFriendlyUnit)
		//{
		//	stillUnderAttack = false;
		//	other.GetComponent<Unit>().FadingOutUnit(2.2f);
		//}
	}
	// ================================================================= BATTLE SCENE END

		

    #region not in use anymore
    /*
    void EnemySpawn()
    {
        Transform tempEnemy;
        randomSpawn = Random.Range (0, 3);
        tempEnemy = Instantiate (GameManager.Instance.undeadUnits [enemyArray[spawnCounts]], 
                                                spawnLocs [randomLoc [randomSpawn]].transform.position,
                                                transform.rotation) as Transform;
        tempEnemy.SetParent (spawnLocs [randomLoc[randomSpawn]]);
//      tempEnemy.localScale = Vector3.one;
//      tempEnemy.localScale = new Vector3 (tempEnemy.localScale.x, tempEnemy.localScale.y, tempEnemy.localScale.z);
        GetNewLocIndex (spawnLocs [randomLoc[locIndex]]);

        int unitType = tempEnemy.GetComponent<Unit> ().unitIndex;
//      GameManager.Instance.SpawnLaneHint (unitType, newLocIndex);

        // == SIZING OF SPAWNED UNITS
        if (tempEnemy.gameObject.GetComponent<Unit> ().unitClass == "Wizard") 
        {
            tempEnemy.transform.localScale = new Vector3 (1f, 1f, 1f);
        }
        if (tempEnemy.gameObject.GetComponent<Unit> ().unitClass == "Archer") 
        {
            tempEnemy.transform.localScale = new Vector3 (1.2f, 1.2f, 1.2f);
        }
        if (tempEnemy.gameObject.GetComponent<Unit> ().unitClass == "Knight") 
        {
            tempEnemy.transform.localScale = new Vector3 (1.5f, 1.5f, 1.5f);
        }
        tempEnemy.gameObject.AddComponent<Rigidbody> ().useGravity = false;

//      locIndex += 1;          
//      if (locIndex >= 3) 
//      {
//          locIndex = 0;
//      }

        spawnCounts += 1;

        //When enemy runs out of equipped units
        if (spawnCounts == enemyUnitsEquippedLoaded)
        {
            spawnCounts = 0;
            CancelInvoke("EnemySpawn");
            isShuffling = true;
            InvokeRepeating ("EnemySpawn", shuffleTiming, spawnInterval);
        }
    }
    */
    //  void ShuffleLocators()
    //  {
    //      for (int i = 0; i < randomLoc.Length; i++)
    //      {
    //          int tmp = randomLoc [i];
    //          int r = Random.Range (i, randomLoc.Length);
    //          randomLoc [i] = randomLoc [r];
    //          randomLoc [r] = tmp;
    //      }
    //  }
    //  public void GetNewLocIndex(Transform locs)
    //  {
    //      newLocIndex = int.Parse(locs.gameObject.name.Substring(8, 1));
    //  }
    #endregion


}
