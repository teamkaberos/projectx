﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;

public class Player : BaseUnit
{
    // FOR FUTURE USE
    // Can be used to handle FX that would be called whenever a castle is damaged
    // e.g: Flag deduction, animations, etc.
    public delegate void CastleDamage();
    public static event CastleDamage onCastleDestroy;

    public delegate void PlayerCastleDamage();
    public static event PlayerCastleDamage playerHit;
    public static event PlayerCastleDamage playerDead;

    delegate void OnStructureLevelUp();
    OnStructureLevelUp onStructureLevelUp;

    public delegate void TroopAssignmentError();
    public static event TroopAssignmentError assingmentPointsExceeded;
    public static event TroopAssignmentError assingmentPointsResolved;
    public static event TroopAssignmentError unitLimitExceeded;
    public static event TroopAssignmentError unitLimitResolved;

    public delegate void TutorialStatus();
    public static event TutorialStatus tutorialComplete;

    public delegate void PlayerLevelUp(int currentLV);
    public static event PlayerLevelUp levelUp;

    public delegate void OnEventTriggered();
    public static event OnEventTriggered SoulChargeAltered;
    public static event OnEventTriggered SUnitAltered;

    #region Singleton
    // The singleton..
    private static Player _instance;
    // ..and its accessor. 
    // Use Player.Instance when using anything from this script.
    // e.g: Player.Instance.health, Player.Instance.MyMethod();
    public static Player Instance
    {
        get
        {
            // If the singleton instance is not present on the scene..
            //			if (_instance == null) 
            //			{
            //				// ..creates a game object to attach the singleton to.
            //				GameObject player = new GameObject ("PlayerCastle");
            //				player.AddComponent<Player> ();
            //				print ("Created a new instance.");
            //			}
            // Retrieves the static instance of the private class and returns it to the public class
            return _instance;
        }
    }
    #endregion

    [Header("Home Scene")]

    // --- PLAYERDATA STUFFS --- \\

    //=== PLAYER INFO===\\
    public static string playerName = "";
    public static string playerUniqueID = "";
    public static string accountType = "Registered";
    public static int reputation = 100;             //Base value is 100
    public static string race = "human";        // to be keyed in lowercase
    public static int tutorial = 0;
    public static int tutorialCap = 9;
    public static long lastLogout = 0;
    public static string email = "";
    public static string clan;
    public static string potraitID = "h0001";

    public static int starterPackPurchase = 0;

    public static int[] castleHealth_ = { 3, 3, 3, 4, 4, 5, 5, 5, 5, 5, 5 };

    //== FOR ANY MAINTENANCE OR UPDATES ==\\
    public static readonly string gameVersion = "1.1.60.001";

    //=== ENERGIES === \\
    public static int energyPacks = 0;
    public static readonly int maxEnergy = 8;
    public static readonly float energyRechargeTime = 300f;
    public static int playerCurrentEnergy = 8;
    public static float currentEnergyRechargeTime = 0;

    public static bool purchasingItem;

    public static string timesAttacked;
    public static int gold_Lost = 0;
    public static string goldLost;

    public static int[] playerMission = new int[3];
    public static int[] playerMissionProgress = new int[3];
    public static int[] playerMissionState = new int[3];
    public static float[] playerMissionRefreshTime = { -100, -100, -100 };

    public static int myGroups = 0;
    public static int groupCost = 300;
    public static int decksAvailable = 1;
    public static int groupLimit = 10;
    public static int heroSlots = 0;
    public static int my_Matches = 0;
    public static int my_Victories = 0;
    public static int my_Losses = 0;
    public static float my_Win_Percentage = 0f;
    public int[] repRequired = { 100, 200, 350, 550, 850, 1200, 1600, 2150, 2700, 3400 };
    public static int earthStoneBuildSlot = 1;
    public static int windStoneBuildSlots = 1;
    public static int flameStoneBuildSlot = 1;

    public static string lastIntervalRewardClaimed;

    public static int jackPack;
    public static int queenPack;
    public static int kingPack;
    public static int bronzeChest;
    public static int silverChest;
    public static int goldChest;

    // =========================== \\
    public static int townHallLevel = 0;
    public static int townHallLevelCap = 30;
    public static int townHallCost = 3000;
    public static float townHallMultiplier = 1.42f;
    public static float townHall_Upgrade_Duration = 30f;
    public static int castleLevel = 1;
    public static int castleMaxLevel = 10;
    public static int castleMaxHealth = 3;
    public static int castleHealth = 3;

    public static int treasuryLvl = 0;
    public static int treasuryLvlCap = 30;
    public static int treasuryCost = 2000;
    public static float treasuryMultiplier = 1.42f;
    public static float treasury_Upgrade_Duration = 30f;
    public static int treasury_Storage_Cap = 20000;
    public static float treasury_Storage_Multiplier = 1.35f;
    public static float treasury_Protection_Value = 6;

    public static int timer3 = 0;
    public static int timer15 = 0;
    public static int timer30 = 0;
    public static int timer60 = 0;

    public static int barracksLvlCap = 30;
    public static float barrackMultiplier = 1.42f;
    public static int bunkerLv = 0;
    public static int bunkerCost = 2000;
    public static float bunker_Upgrade_Duration = 30f;
    public static int rangeLv = 0;
    public static int rangeCost = 2000;
    public static float range_Upgrade_Duration = 30f;
    public static int guildLv = 0;
    public static int guildCost = 2000;
    public static float guild_Upgrade_Duration = 30f;

    public static float stoneBuildDuration = 28800f;
    public static int squareStone = 0;
    public static int squareBattles = 0;
    bool squareAdded;
    public static int triangleStone;
    public static int triangleBattles;
    bool triangleAdded;
    public static int circleStone;
    public static int circleBattles;
    public static int evolvementRequiredBattles = 10;
    bool circleAdded;

    public static int recruitmentHallLvlCap = 30;
    public static int recruitmentHall_Lvl = 0;
    public static int recruitment_Limit = 10;
    public static int recruitmentHall_Cost = 1200;
    public static float recruitment_Upgrade_Duration = 5f;
    public Button assignUnitButton;
    public static int assigned_Assign_Points;           //Check for how many assign points are used
    public static int available_Assign_Points = 100;            //Check for how many assign points are left
    public static int assign_Points_Limit = 100;                    //Max value of points player will have ; used to check if number is max when removing units
    public static int assignmentIncrement = 8;

    public static int forgeLevel = 0;
    public static int forgeCap = 3;
    public static int forge_UpgradeCost = 25000;
    public static int forge_Built_Limit = 20;
    public static float forge_UpgradeDuration = 5f;

    public static int tavernLevel = 0;
    public static int heroTimer15 = 0;
    public static int heroTimer30 = 0;
    public static int heroTimer60 = 0;
    public static int heroTimer120 = 0;

    public static int heroAmount = 17;
    // Size: 17
    public static int[] heroLevels = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    // Size: 17
    public static int[] heroCardAmounts = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    // Size: 17
    public static int[] heroUnlocked = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

    public static int[] heroAssigned = new int[9];

    public static int recruitmentCost = 300;
    public static int supportUnits_Cost = 500;

    public static int[] resourceBuildings_Unlock_Counter = { 1, 1, 2, 3, 4, 5, 5, 5, 5, 5, 5 };

    public static int rssBuildingLvCap = 30;
    public static int resourceBuildingsUnlockedCounter = 1;			//Used to track how many rss generators there are
    public static float rssGenMultiplier = 1.33f;
    public static float rssGenRateMultiplier = 1.25f;
    public static int fillUpRateMultiplier = 3;    //number of hours it takes to fill up the RSS generators
    public static int[] mills = new int[5];
    public static int[] millsRate = { 1044, 1044, 1044, 1044, 1044 };
    public static int[] millsCap = new int[5];
    public static int[] millsCost = { 1000, 1000, 1000, 1000, 1000 };
    public static int[] millsCurrentValue = new int[5];
    public static float[] mills_Upgrade_Duration = new float[5];
    public static int mill_That_Player_Last_Tapped_On;
    public static int mill_Upgrading_Now = 255;
    public static int minesBuilt;
    public static int[] mines = { 0, 0, 0, 0, 0 };
    public static int[] minesRate = { 650, 650, 650, 650, 650 };
    public static int[] minesCap = new int[5];
    public static int[] minesCost = { 1000, 1000, 1000, 1000, 1000 };
    public static int[] minesCurrentValue = new int[5];
    public static float[] mines_Upgrade_Duration = new float[5];
    public static int mine_That_Player_Last_Tapped_On = 0;
    public static int mine_Upgrading_Now = 255;
    public static bool sufficientRSS;                               //To determine what value to use when spending currencies

    public static int landmines = 0;                                    //Amount of landmines player has
    public static int seige = 0;                                                //Amount of seige player has
    public static int dragons = 0;                                      //Amount of dragon player has

    //	public static int lumber = 50000;
    public static int gold = 18000;
    [SerializeField]
    public static int diamond = 200;
    public static int tokens = 25;

    public static int goldCap = 18000;                      //Starting value not determined
                                                            //public static int lumberCap = 50000;					//Starting value not determined
                                                            // ======================= \\

    public static int excalipoints = 0;

    float updateAccInterval = 120f;    // Change this value to detemine how long in between to save player's data
    float updateAccTimer = 120f;


    // --- Misc --- \\
    public static bool premiumAccount;
    public static int firstTimePlaying = 0;

    // ============= \\

    [Space(35)]
    [Header("Shared")]

    [SerializeField] public Text[] equippedHero;

    //	[SerializeField] public static int[][] heroInfo = new int[16][];			//Hero abilityType and HeroInfo [0]Level	[1]AbilityLvl	[2]heroRace

    public Text framerate;


    [Space(35)]

    [Header("Battle Scene")]
    //for SpawnButton

    public Transform[] unitLocs;
    public int getLocIndex;

    public static int equippedGroups = 3;
    public int[] manualAssignment;
    public static int number_Of_Troops_Assigned = 3;										//The actual number of troops player brings
    public static int[] assignedTroops = { 100, 101, 102, 250, 250, 250, 250, 250, 250 };				//250
    public static int[] defaultAssignedTroops = { 100, 101, 102, 250, 250, 250, 250, 250, 250 };

    public static int[] heroNo = { 200, 200, 200 };             //Equipped hero
    public static string[] heroType = new string[3];                                    //Type of the hero to be displayed
    public static int selectedHeroSlot;
    public static int heroInitialCDTime = 30;

    public static int myRemainingCards;                 //The amount of cards below the first; ie if 3 groups, remaining cards = 11

    bool stillUnderAttack;
    public static bool isShuffling;
    public static float shufflingTimer;                                                         //Time that will be displayed on the battle UI
    public static float shufflingTime = 12.2f;                                              //Time it takes to shuffle the deck
    public static float shuffleReduction;                                                       //Time that will be checked when deck begins shuffling
    public static float shuffleIncrement;														//Time that will be checked together with reduction

    public static bool secondDeckEnabled = true;
    public static int deck1Cards;
    public static int deck1InitialCards;                        //Original value for first deck if player has second deck enabled
    public static int deck2Cards;
    public static int deck2InitialCards;                        //Original value for second deck if player has second deck enabled
    public static string[] equippedUnits;
    public static int[] equippedSupports = new int[3];
    public static int[] minesRequire = { 15, 15, 15 };     // Values are above the max soul charge value so players cant use
    public static int[] dragsRequire = { 15, 15, 15 };
    //public static int[] minesRequire = { 2, 3, 4 };   // Original requirements for S-Unit mines
    //public static int[] dragsRequire = { 3, 5, 8 };   // Original requirements for S-Unit drags
    public static int[] seigeRequire = { 1, 2, 3 };
    Transform deckAssignedTo;                               //For randomising between both decks
    public List<GameObject> troopsArrangement;
    public Transform deck1;
    public Transform deck2;
    public GameObject theSecondDeck;                        //Whole secondDeck gameobject
    int cc = 0;                                                                                     //Used to determine how many cards a deck has, so reshuffling can begin
    public Transform deck1CurrentCard;                  //Position of the first card of the first deck
    public Transform deck2CurrentCard;
    public GameObject[] supportUnitButtons;             //Array of the support buttons

    public static int soulCharges = 0;
    public static int soulChargeCap = 10;
    public GameObject[] medals;

    #region For Testing

    public float squareCDFormula;           //For home buildings
    public float triangleCDFormula;     //For home buildings
    public float circleCDFormula;           //For home buildings

    #endregion
    static bool isInit;
    public static float totalTimeInGame;
    static bool calculatedTimeAway;
    static float timeAwayFromGame;

    // ====================================================================== VARIABLES END \\

    void Awake()
    {
        Application.targetFrameRate = 150;
        timeAwayFromGame = 0;
        calculatedTimeAway = false;

        #region Singleton init
        if (_instance == null)
        {
            _instance = this;
        }
        if (castleLevel == 0)
        {
            castleLevel = 1;
        }
        #endregion
        //		if (accountType == "Guest")
        //		{
        firstTimePlaying = PlayerPrefs.GetInt("FirstTimePlaying");
        if (firstTimePlaying != 0)
        {
            CheckGroupLimit();
            CheckHeroSlots();
        }
        else if (firstTimePlaying == 0)
        {
            CheckGroupLimit();
            CheckHeroSlots();
            firstTimePlaying = 1;
            PlayerPrefs.SetInt("FirstTimePlaying", firstTimePlaying);

            if (accountType == "Guest")
            {
                SavePrefs();
                SaveBuildingInfo();
            }
        }

        if (SceneManager.GetActiveScene().name == "3_Battle" || SceneManager.GetActiveScene().name == "MockupPvP")
        {
            // ----=== Resetting the amount of troops player has ====---- //
            Reset_ParticipationCount();
            //Manual_Troop_Assignment();
            Set_Shuffle_Timer();
            Update_Shuffling_Time();
            Calculate_Number_Of_Troops();
            ResetDeckCounts();

            // ----=== Displaying the second deck and laying out the Cards on UI ===---- //
            InitializeDecks();
            UIManager.Instance.FakeCardsLaying();

            //		UIManager.Instance.AssignHero();							//Assign hero names to buttons, and initializing the cooldown
            UIManager.Instance.UpdateRemainingCards();
            UIManager.Instance.Display_Soul_Charges();

            CreateTroopButtons();
            ShuffleTroops();
            DistributeTroops();
            DisplaySecondCard();                        //	Moving of the first card to a side
        }
        else
        {
            //Reset player charges to 0 so if player goes into battle again, it wont bring over from previous match
            soulCharges = 0;
        }
    }

    void Start()
    {
        if (SceneManager.GetActiveScene().name.Contains("Home"))
        {
            //TODO this area is only for registered players who has info on the server
            //if (tutorial < tutorialCap)
            //{
            //    PotraitManager.Instance.Assign_Player_Potrait();
            //}
            if (accountType == "Registered")
            {
                Time.timeScale = 1f;

                if (TimeMaster.loggedIn == 0)
                    isInit = true;
                else
                    isInit = false;

                if (isInit == false)
                {
                    //Invoke("UpdatePlayerMission", 1.5f);
                    //Invoke("UpdatePlayerEnergy", 1.5f);
                }
            }
            isShuffling = false;
        }
    }

    void Update()
    {
        if (isShuffling)
        {
            shufflingTimer -= Time.deltaTime;
            if (shufflingTimer <= 0)
                shufflingTimer = 0;
            Update_Shuffling_Time();
        }

        if (SceneManager.GetActiveScene().name.ToLower().Contains("home"))
        {
            if (GameSparksManager.playerId != "" && tutorial >= tutorialCap)
            {
                updateAccTimer -= Time.deltaTime;
                if (updateAccTimer < 0f)
                {
                    StoreAllData();
                    updateAccTimer = updateAccInterval;
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.B))
        {
            //UpdateData();
        }
    }
    void UpdateData()
    {
        castleLevel = 8;
        townHallLevel = 23;
        forgeLevel = 3;
        treasuryLvl = 24;
        bunkerLv = 24;
        rangeLv = 24;
        guildLv = 25;
        reputation = 74632;
        recruitmentHall_Lvl = 24;
        diamond = 9348421;
        squareStone = 2853;
        circleStone = 28957;
        triangleStone = 8493;
        mines[0] = 24;
        mines[1] = 24;
        mines[2] = 24;
        mines[3] = 25;
        mines[4] = 25;
        tutorial = 8;
        DataManager.Instance.PrepareDataForSparksPushing();
        GameSparksManager.Instance.StoreAllData();
    }

    public void ResetTotalGamePlayTime()
    {
        totalTimeInGame = 0;
    }
    public void StoreTotalGamePlayTime()
    {
        totalTimeInGame += Time.timeSinceLevelLoad;

    }

    public float Calculate_The_Time_Difference()
    {
        if (calculatedTimeAway == false)
        {
            DateTime awayStartTime = DateTime.Parse(PlayerPrefs.GetString("FocusTimeCounter"));
            TimeSpan localTimeDifference = DateTime.Now.Subtract(awayStartTime);

            calculatedTimeAway = true;

            return (float)localTimeDifference.TotalSeconds;
        }
        return 0;
    }


    void OnApplicationFocus(bool focus)
    {
        if (tutorial >= tutorialCap)
        {
            if (!focus)
            {
                // The frame where player minimizes the app
                if (SceneManager.GetActiveScene().name.Contains("Home"))
                {
                    TimeMaster.Instance.SaveCountdownTimers();

                    calculatedTimeAway = false;
                    PlayerPrefs.SetString("FocusTimeCounter", DateTime.Now.ToString());
                }

                GameSparksManager.Instance.SaveLastLogout();
            }
            else
            {
                //When the player returns and if player is at home scene
                if (SceneManager.GetActiveScene().name.Contains("Home") && GameSparksManager.Instance.AmReturningHome() == false)
                {
                    //find the difference in time
                    timeAwayFromGame = Calculate_The_Time_Difference();

                    TimeMaster.Instance.Assign_New_Values();

                    DataManager.Instance.PrepareDataForSparksPushing();
                    StoreAllData();

                    GameSparksManager.Instance.SaveLastLogout();

                    if (timeAwayFromGame > 5f && purchasingItem != true && Input_Resize.chatFocus != true && SupportEmail.URLOpened != true)
                    {
                        BuildingManager.Instance.LogOutReset();
                        MissionHolder.Instance.LogoutReset();
                        LevelManager.Instance.SplashScreen();
                    }
                    //reset the email bool
                    if (SupportEmail.URLOpened == true)
                        SupportEmail.URLOpened = false;
                }
            }
        }
    }
    void OnApplicationPause(bool paused)
    {
        if (tutorial < tutorialCap)
            return;
        if (paused)
        {
            if (SceneManager.GetActiveScene().name.Contains("Home"))
            {
                TimeMaster.Instance.SaveCountdownTimers();

                calculatedTimeAway = false;
                PlayerPrefs.SetString("FocusTimeCounter", DateTime.Now.ToString());
            }
            GameSparksManager.Instance.SaveLastLogout();
        }
        else
        {
            //if player is at home scene
            if (SceneManager.GetActiveScene().name.Contains("Home") && GameSparksManager.Instance.AmReturningHome() == false)
            {
                //find the difference in time
                timeAwayFromGame = Calculate_The_Time_Difference();

                TimeMaster.Instance.Assign_New_Values();

                DataManager.Instance.PrepareDataForSparksPushing();
                StoreAllData();

                GameSparksManager.Instance.SaveLastLogout();

                if (timeAwayFromGame > 5f && purchasingItem != true && Input_Resize.chatFocus != true && SupportEmail.URLOpened != true)
                {
                    BuildingManager.Instance.LogOutReset();
                    MissionHolder.Instance.LogoutReset();
                    LevelManager.Instance.SplashScreen();
                }
                if (SupportEmail.URLOpened == true)
                    SupportEmail.URLOpened = false;
            }
        }
    }

    void OnApplicationQuit()
    {
        if (tutorial < tutorialCap)
            return;

        if (SceneManager.GetActiveScene().name.Contains("Battle"))
        {
            reputation -= 3;
            CheckRepValue();
        }

        if (DataManager.Instance.LoggedCheck() == true)
        {
            GameSparksManager.Instance.SaveLastLogout();
        }

        StoreAllData();
    }

    public void DisconnectPlayer()
    {
        //Reset();
        //LevelManager.Instance.SplashScreen();
    }


    public static void SetSUnitValues(int val, SUnitType.SUnit unitToAdd)
    {
        switch (unitToAdd)
        {
            case SUnitType.SUnit.Ballista:
                seige = val;
                break;
            case SUnitType.SUnit.Flames:
                dragons = val;
                break;
            case SUnitType.SUnit.Mines:
                landmines = val;
                break;
        }
    }
    public static void UpdateSUnitsAmount(SUnitType.SUnit unitType)
    {
        if (unitType == SUnitType.SUnit.Ballista)
            seige -= 1;
        else if (unitType == SUnitType.SUnit.Flames)
            dragons -= 1;
        else if (unitType == SUnitType.SUnit.Mines)
            landmines -= 1;
        SUnitAltered?.Invoke();
    }
    public static int SUnitAnount(SUnitType.SUnit unitType)
    {
        int toReturn = 0;
        switch (unitType)
        {
            case SUnitType.SUnit.Ballista:
                toReturn = seige;
                break;
            case SUnitType.SUnit.Flames:
                toReturn = dragons;
                break;
            case SUnitType.SUnit.Mines:
                toReturn = landmines;
                break;
        }
        return toReturn;
    }

    // -------------------------------- BATTLE SCENE -------------------------------------- \\

    void Manual_Troop_Assignment()
    {
        for (int x = 0; x < assignedTroops.Length; x++)
        {
            assignedTroops[x] = manualAssignment[x];
        }
    }

    void Set_Shuffle_Timer()
    {
        shufflingTimer = shufflingTime - shuffleReduction + shuffleIncrement;
    }
    void Update_Shuffling_Time()
    {
        UIManager.Instance.Player_Shuffling_Timer_Display(shufflingTimer);
    }

    void ResetDeckCounts()
    {
        //Check if player has 1 or 2 decks
        if (!secondDeckEnabled)
        {
            deck1Cards = number_Of_Troops_Assigned;
            deck1InitialCards = deck1Cards;
        }
        else if (secondDeckEnabled)
        {
            //Set max value of cards to be assigned to first deck
            deck1Cards = (number_Of_Troops_Assigned + 1) / 2;
            deck1InitialCards = deck1Cards;

            deck2Cards = number_Of_Troops_Assigned - deck1Cards;
            deck2InitialCards = deck2Cards;
        }
    }
    void Reset_ParticipationCount()
    {
        squareAdded = false;
        circleAdded = false;
        triangleAdded = false;
    }
    void InitializeDecks()
    {
        if (secondDeckEnabled)
        {
            theSecondDeck.gameObject.SetActive(true);
        }
    }
    //New CreateTroopButtons
    void CreateTroopButtons()
    {
        for (int x = 0; x < assignedTroops.Length; x++)
        {
            // if assigned slot is not empty
            if (assignedTroops[x] != 250)
            {
                //search for the corresponding card in GameManager
                for (int y = 0; y < GameManager.Instance.troopInfo.Count; y++)
                {
                    // if the same troop is found
                    if (GameManager.Instance.troopInfo[y].troopNo == assignedTroops[x])
                    {
                        //Instantiate cards here
                        GameObject button = Instantiate(GameManager.Instance.troopInfo[y].troopCard, deck1.position, Quaternion.identity, deck1);
                        StoreTroopButton(button);
                        if (GameManager.Instance.troopInfo[y].troopNo < 100)    //which means its a hero
                        {
                            for (int z = 0; z < UnitManager.Instance.heroInfomation.Count; z++)
                            {
                                if (GameManager.Instance.troopInfo[y].troopNo == UnitManager.Instance.heroInfomation[z].heroNo)
                                {
                                    button.GetComponent<UnitButton>().CardLevel(UnitManager.Instance.heroInfomation[z].heroLvl);
                                }
                            }
                        }
                        TroopManager.Instance.New_Create_Player_Units(GameManager.Instance.troopInfo[y].troopNo);
                    }
                }
            }
        }
    }
    void ShuffleTroops()
    {
        if (tutorial == 4)
            return;
        //else
        //{
        List<GameObject> shuffledCards = new List<GameObject>();
        int times_To_Loop = troopsArrangement.Count;
        for (int i = 0; i < times_To_Loop; i++)
        {
            System.Random rnd = new System.Random();
            int r = rnd.Next(0, troopsArrangement.Count);

            shuffledCards.Add(troopsArrangement[r]);
            troopsArrangement.RemoveAt(r);
        }
        for (int x = 0; x < times_To_Loop; x++)
        {
            troopsArrangement.Add(shuffledCards[x]);
        }
    }
    void DistributeTroops()
    {
        if (!secondDeckEnabled)
        {
            for (int x = 0; x < troopsArrangement.Count; x++)
            {
                OrganizeCards(troopsArrangement[x], 0);
                ScaleTroopButton(troopsArrangement[x]);
            }
        }
        if (secondDeckEnabled)
        {

            deck1Cards = deck1InitialCards;
            deck2Cards = deck2InitialCards;
            int assignNo = 0;
            for (int x = 0; x < troopsArrangement.Count; x++)
            {
                if (deck1Cards > 0)
                {
                    deckAssignedTo = deck1;
                    deck1Cards -= 1;
                    assignNo = 0;
                }
                else
                {
                    deckAssignedTo = deck2;
                    deck2Cards -= 1;
                    assignNo = 1;
                }
                OrganizeCards(troopsArrangement[x], assignNo);
                ScaleTroopButton(troopsArrangement[x]);
            }

            deck1Cards = deck1InitialCards;
            deck2Cards = deck2InitialCards;
        }
    }

    void OrganizeCards(GameObject troopButton, int deckAssignedTo)
    {
        if (deckAssignedTo == 0)
        {
            troopButton.transform.SetParent(deck1);
            troopButton.transform.SetAsFirstSibling();
            troopButton.transform.position = deck1.position;
        }
        else if (deckAssignedTo == 1)
        {
            troopButton.transform.SetParent(deck2);
            troopButton.transform.SetAsFirstSibling();
            troopButton.transform.position = deck2.position;
        }
    }
    void ScaleTroopButton(GameObject troopButton)
    {
        troopButton.transform.localScale = Vector3.one;
    }
    void StoreTroopButton(GameObject troopButton)
    {
        troopsArrangement.Add(troopButton);
    }

    public float CurrentShufflingTimer()
    {
        float currentShuffleTime = shufflingTime - shuffleReduction + shuffleIncrement;
        return currentShuffleTime;
    }

    public void DisplaySecondCard()
    {
        UIManager.Instance.ChangeBlockerPosition();

        //If player only has 1 deck
        if (!secondDeckEnabled)
        {
            // In the case where there is no more second card beneath
            if (cc == number_Of_Troops_Assigned)
            {
                //reset the counter
                cc = 0;
                //Reduce the deck count
                DeductDeck1Count();
                //Update amount on UI
                UIManager.Instance.UpdateRemainingCards();
                //Activate the shuffling animation
                UIManager.Instance.DisplayDeckBack();
                //Set to shuffing, and assign shuffling time
                isShuffling = true;
                shufflingTimer = shufflingTime - shuffleReduction + shuffleIncrement;
                shuffleReduction = 0;
                shuffleIncrement = 0;
                //Begin the shuffling cooldown
                Invoke("WaitForDeckCooldown", shufflingTimer);

                return;
            }
            if (cc < number_Of_Troops_Assigned)
            {
                if (GameManager.toBeDisabled != null)
                    DeductDeck1Count();
            }
        }
        //If player has 2 decks
        else if (secondDeckEnabled)
        {
            if (cc == number_Of_Troops_Assigned)
            {
                //reset the counter
                cc = 0;
                //Reduce deck count
                DeductDeck1Count();
                DeductDeck2Count();
                //Update amount on UI
                UIManager.Instance.UpdateRemainingCards();
                //Activate the shuffling animation
                UIManager.Instance.DisplayDeckBack();
                //Set to shuffing, and assign shuffling time
                isShuffling = true;
                shufflingTimer = shufflingTime - shuffleReduction + shuffleIncrement;
                shuffleReduction = 0;
                shuffleIncrement = 0;
                //Begin the shuffling cooldown
                Invoke("WaitForDeckCooldown", shufflingTimer);
                //if the deck has no more cards
                //				if (deck1Cards <= 0 && deck2Cards <= 0)
                //				{	
                //					//reset the value of cards in deck1 to original value
                //					ResetDeckCounts();
                //				}
                return;
            }
            if (cc < number_Of_Troops_Assigned)
            {
                //Check and deduct the value from the deck of the last card spawned
                if (GameManager.toBeDisabled != null)
                {
                    if (GameManager.toBeDisabled.transform == deck1.GetChild(0).transform)
                    {
                        DeductDeck1Count();
                    }
                    if (GameManager.toBeDisabled.transform == deck2.GetChild(0).transform)
                    {
                        DeductDeck2Count();
                    }
                }
            }
        }
        UIManager.Instance.UpdateRemainingCards();
        CardPops();
        cc += 1;
    }
    void CardPops()
    {
        //If player only has 1 deck
        if (!secondDeckEnabled)
        {
            GameObject d1FirstCard = deck1.GetChild(deck1.childCount - 1).gameObject;
            d1FirstCard.transform.position = deck1CurrentCard.position;
            //If Global Cooldown is not in effect
            if (SpawnBlockerBarManager.onCooldown == false)
            {
                d1FirstCard.GetComponent<Button>().interactable = true;
            }
            //Otherwise . . .
            else if (SpawnBlockerBarManager.onCooldown == true)
            {
                Invoke("UnlockFirstCard", SpawnBlockerBarManager.Instance.cdTime);
            }
        }

        else if (secondDeckEnabled)
        {
            GameObject d1FirstCard = deck1.GetChild(deck1.childCount - 1).gameObject;
            d1FirstCard.transform.position = deck1CurrentCard.position;
            if (SpawnBlockerBarManager.onCooldown == false)
            {
                d1FirstCard.GetComponent<Button>().interactable = true;
            }
            else if (SpawnBlockerBarManager.onCooldown == true)
            {
                d1FirstCard.GetComponent<Button>().interactable = false;
            }


            if (deck2.childCount > 1)
            {
                GameObject d2FirstCard = deck2.GetChild(deck2.childCount - 1).gameObject;
                d2FirstCard.transform.position = deck2CurrentCard.position;
                if (SpawnBlockerBarManager.onCooldown == false)
                {
                    d2FirstCard.GetComponent<Button>().interactable = true;
                }
                else if (SpawnBlockerBarManager.onCooldown == true)
                {
                    d2FirstCard.GetComponent<Button>().interactable = false;
                }
            }


            if (SpawnBlockerBarManager.onCooldown == true)
                Invoke("UnlockFirstCard", SpawnBlockerBarManager.Instance.cdTime);
        }
    }

    void DeductDeck1Count()
    {
        deck1Cards -= 1;
        if (deck1Cards <= 0)
        {
            deck1Cards = 0;
        }
    }
    void DeductDeck2Count()
    {
        deck2Cards -= 1;
        if (deck2Cards <= 0)
            deck2Cards = 0;
    }

    void UnlockFirstCard()
    {
        deck1.GetChild(deck1.childCount - 1).gameObject.GetComponent<Button>().interactable = true;
        if (secondDeckEnabled)
        {
            if (deck2.childCount > 1)
                deck2.GetChild(deck2.childCount - 1).gameObject.GetComponent<Button>().interactable = true;
        }
    }

    void DisableAllCards()
    {
        for (int u = 0; u < troopsArrangement.Count; u++)
        {
            troopsArrangement[u].gameObject.SetActive(true);
            troopsArrangement[u].GetComponent<Button>().interactable = false;
        }
    }
    void ReshuffleCards()
    {
        if (!secondDeckEnabled)
        {
            for (int y = 0; y < deck1.childCount; y++)
            {
                deck1.GetChild(y).gameObject.SetActive(true);
                deck1.GetChild(y).transform.SetSiblingIndex(UnityEngine.Random.Range(0, deck1.childCount));
            }
        }
        else if (secondDeckEnabled)
        {
            for (int x = 0; x < troopsArrangement.Count; x++)
            {
                //Set Active all cards first
                troopsArrangement[x].gameObject.SetActive(true);
            }
            for (int i = 0; i < troopsArrangement.Count; i++)
            {
                GameObject tmp = troopsArrangement[i];
                int r = UnityEngine.Random.Range(i, troopsArrangement.Count);
                troopsArrangement[i] = troopsArrangement[r];
                troopsArrangement[r] = tmp;
            }
        }
    }

    public void RefreshDeckCooldown(float refreshTiming)
    {
        CancelInvoke("WaitForDeckCooldown");
        shufflingTimer = refreshTiming;
        Invoke("WaitForDeckCooldown", refreshTiming);
    }

    void WaitForDeckCooldown()
    {
        DisableAllCards();
        ShuffleTroops();
        DistributeTroops();
        ResetDeckCounts();
        UIManager.Instance.HideDeckBack();
        UIManager.Instance.FakeCardsLaying();
        isShuffling = false;
        Set_Shuffle_Timer();
        Update_Shuffling_Time();
        UnlockFirstCard();
        DisplaySecondCard();

    }

    public void IncreaseMedalsCharge()
    {
        //To increase the medal charge
        soulCharges += 1;
        if (GameManager.soulEnhanced)
            soulCharges += GameManager.soulChargeValue;
        if (soulCharges >= soulChargeCap)
            soulCharges = soulChargeCap;
        UIManager.Instance.Display_Soul_Charges();
        SoulChargeAltered();
    }
    public void Decrease_Soul_Charges(int soulsDeduct)
    {
        for (int x = 0; x < soulsDeduct; x++)
        {
            if (soulCharges >= 0)
            {
                UIManager.Instance.Hide_Soul_Charges();
                soulCharges -= 1;
            }
        }
        SoulChargeAltered();
    }



    // ========= GETTING LOOTS ========== \\
    public void NewUpdateRaids(int gold)
    {
        if (tutorial >= tutorialCap)
        {
            NewGetLoot(gold, castleHealth);
        }
        else if (tutorial == 5)
        {
            NewGetLoot(2000, 0);
        }
    }

    public void NewGetLoot(int goldAmount, int diamondReward)
    {
        DataManager.gold += goldAmount;

        if (DataManager.gold >= treasury_Storage_Cap)
            DataManager.gold = treasury_Storage_Cap;
        else if (DataManager.gold < 0)
            DataManager.gold = 0;

        LimitGold();
        MissionHolder.Instance.UpdateMission("gold", goldAmount);
        int itemToIgnore = UnityEngine.Random.Range(0, 2);

        if (tutorial >= tutorialCap)
        {
            DataManager.diamond += diamondReward;

            int timer3Get = 1;
            for (int x = 0; x < 9; x++)
            {
                int randT3 = UnityEngine.Random.Range(0, 100);
                if (randT3 > 90)
                    timer3Get += 1;
            }
            DataManager.timer3Amount += timer3Get;

            int timer15Get = 0;
            if (itemToIgnore != 0)
            {
                for (int x = 0; x < 8; x++)
                {
                    int randT15 = UnityEngine.Random.Range(0, 100);
                    if (randT15 <= 64)
                        timer15Get += 1;
                }
            }
            DataManager.timer15Amount += timer15Get;

            int timer30Get = 0;
            if (itemToIgnore != 1)
            {
                for (int x = 0; x < 5; x++)
                {
                    int randT30 = UnityEngine.Random.Range(0, 100);
                    if (randT30 <= 36)
                        timer30Get += 1;
                }
            }
            DataManager.timer30Amount += timer30Get;

            int timer60Get = 0;
            if (itemToIgnore != 2)
            {
                for (int x = 0; x < 3; x++)
                {
                    int randT60 = UnityEngine.Random.Range(0, 100);
                    if (randT60 <= 9)
                        randT60 += 1;
                }
            }
            DataManager.timer60Amount += timer60Get;

            UIManager.Instance.Display_Obtained_Rewards(diamondReward, goldAmount, timer3Get, timer15Get, timer30Get, timer60Get);

        }
        else
        {
            UIManager.Instance.Display_Obtained_Rewards(0, goldAmount, 0, 0, 0, 0);
        }
    }


    // ---------------------------------- HOME SCENE ---------------------------------------- \\

    // ==== PLAYER CURRENCIES HANDLER ==== \\
    public void AddGold(int gainAmount)
    {
        gold += gainAmount;
        LimitGold();
        UIManager.Instance.UpdateCurrencyInfo();
    }
    public void SpendGold(int spendAmount)
    {
        gold -= spendAmount;
        LimitGold();
        UIManager.Instance.UpdateCurrencyInfo();
    }
    public void LimitGold()
    {
        if (gold > treasury_Storage_Cap)
            gold = treasury_Storage_Cap;
        if (gold < 0)
            gold = 0;
    }

    public void GainDiamond(int diamondAmount)
    {
        diamond += diamondAmount;
        UIManager.Instance.UpdateCurrencyInfo();
    }
    public void SpendDiamond(int spendAmount)
    {
        if (diamond >= spendAmount)
            diamond -= spendAmount;

        UIManager.Instance.UpdateCurrencyInfo();
        if (accountType == "Guest")
            SavePrefs();
    }

    public void SpendExcalipoints(int amount)
    {
        excalipoints -= amount;
    }
    public void AddExcalipoints(int amountToAdd)
    {
        excalipoints += amountToAdd;

        DataManager.Instance.PrepareDataForSparksPushing();
        GameSparksManager.Instance.SavePlayerExpendables(GameSparksManager.playerId);
    }
    public void ExcaliPoint_Calculator(bool toAdd, int valueToAlter)
    {
        if (toAdd)
            excalipoints += valueToAlter;
        else
            excalipoints -= valueToAlter;
    }
    public int excalipoint_Value()
    {
        return excalipoints;
    }

    public void SpendEvolutionStones(string stoneType, int stoneAmount)
    {
        if (stoneType == "square")
            squareStone -= stoneAmount;

        if (stoneType == "circle")
            circleStone -= stoneAmount;

        if (stoneType == "triangle")
            triangleStone -= stoneAmount;

        if (accountType == "Guest")
            SavePrefs();
    }

    public void UseEnergy(int usageValue)
    {
        playerCurrentEnergy -= usageValue;
        DataManager.energy -= usageValue;

        if (playerCurrentEnergy == maxEnergy - 1)
            // only happens if the player WAS a full energy before using it
            currentEnergyRechargeTime = energyRechargeTime;

        DataManager.Instance.PrepareDataForSparksPushing();
        GameSparksManager.Instance.SavePlayerData(GameSparksManager.playerId);
    }
    public void AddEnergPack(int packsToAdd)
    {
        energyPacks += packsToAdd;

        DataManager.Instance.PrepareDataForSparksPushing();
        GameSparksManager.Instance.SaveInventoryEnergyPacks();
    }
    public void RechargeEnergy(int value)
    {
        playerCurrentEnergy += value;
        if (playerCurrentEnergy > maxEnergy)
            playerCurrentEnergy = maxEnergy;
        UIManager.Instance.Update_Energy();

        DataManager.Instance.PrepareDataForSparksPushing();
        GameSparksManager.Instance.SavePlayerData(GameSparksManager.playerId);
    }

    public void AlterRepValue(bool toAdd, int valueToAlter)
    {
        if (toAdd)
            reputation += valueToAlter;
        else
            reputation -= valueToAlter;

        CheckRepValue();
        Check_If_Castle_Upgradable();
    }
    public void RepValue(bool battleOutcome)
    {
        if (battleOutcome == true)          //If players win battle
        {
            reputation += 3;                    // ....they gain rep
            MissionHolder.Instance.UpdateMission("reputation", 3);
            my_Victories += 1;
            my_Matches += 1;
        }
        if (battleOutcome == false)         //Else if the lose
        {
            reputation -= 3;                    // ....they lose rep
            my_Losses += 1;
            my_Matches += 1;
        }
        CheckRepValue();                  //and value tallied to min/max cap

        DataManager.battles = my_Matches;
        DataManager.losses = my_Losses;
        DataManager.victories = my_Victories;
        DataManager.reputation = reputation;

        //DataManager.Instance.PrepareDataForSparksPushing();
        GameSparksManager.Instance.SavePlayerBattleLog();
        GameSparksManager.Instance.SavePlayerData(GameSparksManager.playerId);

    }
    public void CheckRepValue()
    {
        if (reputation <= 100)
            reputation = 100;
    }

    public void AddStone(string stoneType)
    {
        if (stoneType.Contains("0"))
        {
            squareStone += 1;
        }
        if (stoneType.Contains("1"))
        {
            triangleStone += 1;
        }
        if (stoneType.Contains("2"))
        {
            circleStone += 1;
        }

        if (accountType == "Guest")
            SavePrefs();

        DataManager.Instance.PrepareDataForSparksPushing();
        GameSparksManager.Instance.SaveInventoryStones();
    }

    public int TotalPacks()
    {
        int totalPacks = jackPack + queenPack + kingPack;
        return totalPacks;
    }
    public int TotalChests()
    {
        int totalChests = bronzeChest + silverChest + goldChest;
        return totalChests;
    }
    public void DeductLoot(string lootToDeduct)
    {
        switch (lootToDeduct)
        {
            case "Jack":
                jackPack -= 1;
                break;
            case "Queen":
                queenPack -= 1;
                break;
            case "King":
                kingPack -= 1;
                break;
            case "Bronze":
                bronzeChest -= 1;
                break;
            case "Silver":
                silverChest -= 1;
                break;
            case "Gold":
                goldChest -= 1;
                break;
        }
        UIManager.Instance.UpdateAvailableLoot();
        if (accountType == "Guest")
            SavePrefs();

        DataManager.Instance.PrepareDataForSparksPushing();
        GameSparksManager.Instance.SaveInventoryPacksInfo();
    }


    // ==== PLAYER CURRENCIES HANDLER END ==== \\



    // ==== STRUCTURE UPGRADES ==== \\
    public void Store_The_Last_RSS_Generator_Player_Tapped(int generatorNo, string generatorType)
    {
        mine_That_Player_Last_Tapped_On = generatorNo;
    }

    public int upgradeCost(int buildingNo)
    {
        if (buildingNo == 0)
            return 1000;
        if (buildingNo == 1)
            return townHallCost;
        if (buildingNo == 2)
            return bunkerCost;
        if (buildingNo == 3)
            return rangeCost;
        if (buildingNo == 4)
            return guildCost;
        if (buildingNo == 5)
            return treasuryCost;
        if (buildingNo == 6)
            return minesCost[0];
        if (buildingNo == 7)
            return minesCost[1];
        if (buildingNo == 8)
            return minesCost[2];
        if (buildingNo == 9)
            return minesCost[3];
        if (buildingNo == 10)
            return minesCost[4];
        if (buildingNo == 11)
            return forge_UpgradeCost;
        return 0;
    }

    public void UpgradeTownHall()
    {
        townHallLevel += 1;
        townHallCost = Mathf.FloorToInt(townHallCost * townHallMultiplier);
        townHall_Upgrade_Duration = BuildingManager.Instance.DurationRequest(townHallLevel);
    }
    public void UpgradeRecruitmentHall()
    {
        recruitmentHall_Lvl += 1;
        recruitmentCost = Mathf.RoundToInt(recruitmentCost * 1.27f);
    }
    public void UpgradeBunker()
    {
        //Adding 1 level to Knight Training Ground
        bunkerLv += 1;
        bunkerCost = Mathf.RoundToInt(bunkerCost * barrackMultiplier);
        bunker_Upgrade_Duration = BuildingManager.Instance.DurationRequest(bunkerLv);
    }
    public void UpgradeRange()
    {
        rangeLv += 1;
        rangeCost = Mathf.RoundToInt(rangeCost * barrackMultiplier);
        range_Upgrade_Duration = BuildingManager.Instance.DurationRequest(rangeLv);
    }
    public void UpgradeGuild()
    {
        guildLv += 1;
        guildCost = Mathf.RoundToInt(guildCost * barrackMultiplier);
        guild_Upgrade_Duration = BuildingManager.Instance.DurationRequest(guildLv);
    }
    public void UpgradeTreasury()
    {
        treasuryLvl += 1;
        treasuryCost = Mathf.FloorToInt(treasuryCost * treasuryMultiplier);
        treasury_Storage_Cap = Mathf.FloorToInt(treasury_Storage_Cap * treasury_Storage_Multiplier);
        treasury_Upgrade_Duration = BuildingManager.Instance.DurationRequest(treasuryLvl);
        UIManager.Instance.UpdateCurrencyInfo();
    }
    public void UpgradeMine()
    {
        if (mines[mine_That_Player_Last_Tapped_On] == 0)
        {
            minesBuilt += 1;
        }
        mines[mine_Upgrading_Now] += 1;

        //Check if the current level is 1
        if (mines[mine_Upgrading_Now] >= 1 && mines[mine_Upgrading_Now] <= 6)
            mines_Upgrade_Duration[mine_Upgrading_Now] = mines[mine_Upgrading_Now] * 1500;
        else if (mines[mine_Upgrading_Now] >= 7 && mines[mine_Upgrading_Now] <= 15)
            mines_Upgrade_Duration[mine_Upgrading_Now] = mines[mine_Upgrading_Now] * 4500;
        else if (mines[mine_Upgrading_Now] >= 16 && mines[mine_Upgrading_Now] <= 22)
            mines_Upgrade_Duration[mine_Upgrading_Now] = mines[mine_Upgrading_Now] * 13500;
        else if (mines[mine_Upgrading_Now] >= 23 && mines[mine_Upgrading_Now] <= 29)
            mines_Upgrade_Duration[mine_Upgrading_Now] = mines[mine_Upgrading_Now] * 22500;
        if (mines[mine_That_Player_Last_Tapped_On] == 1)
        {
            minesRate[mine_Upgrading_Now] += 950;
            minesCap[mine_Upgrading_Now] = minesRate[mine_That_Player_Last_Tapped_On] * fillUpRateMultiplier;
            minesCost[mine_Upgrading_Now] = 1000;
        }
        else
        {
            minesRate[mine_Upgrading_Now] = Mathf.RoundToInt(minesRate[mine_Upgrading_Now] * rssGenRateMultiplier);
            minesCap[mine_Upgrading_Now] = Mathf.RoundToInt(minesRate[mine_Upgrading_Now] * fillUpRateMultiplier);
            minesCost[mine_Upgrading_Now] = Mathf.FloorToInt(minesCost[mine_Upgrading_Now] * rssGenMultiplier);
        }
    }
    public void UpgradeForge()
    {
        forgeLevel += 1;
    }

    public void Check_If_Castle_Upgradable()
    {
        UIManager.Instance.ClanToggleDisplay();
        UIManager.Instance.SetSUnitToggleDisplay();
        if (castleLevel >= 0)
        {
            if ((castleLevel * 3 + 3) <= townHallLevel && reputation >= repRequired[castleLevel])
                Upgrade_Castle_Level();
        }
    }
    public void Upgrade_Castle_Level()
    {
        if (castleLevel < castleMaxLevel)
        {
            levelUp(castleLevel);
            castleLevel += 1;
            UIManager.Instance.UpdatePlayerInfo();
            UIManager.Instance.ClanToggleDisplay();
            UIManager.Instance.SetSUnitToggleDisplay();
        }
    }

    public float UpgradingBuildingLevel(string buildingName)
    {
        if (buildingName.ToLower().Contains("range"))
        {
            BuildTimer.Instance.rangesBuildingTimer = range_Upgrade_Duration;
            return range_Upgrade_Duration;
        }
        if (buildingName.ToLower().Contains("townhall"))
        {
            BuildTimer.Instance.tHallBuildingTimer = townHall_Upgrade_Duration;
            return townHall_Upgrade_Duration;
        }
        if (buildingName.ToLower().Contains("bunker"))
        {
            BuildTimer.Instance.bunkerBuildingTimer = bunker_Upgrade_Duration;
            return bunker_Upgrade_Duration;
        }
        if (buildingName.ToLower().Contains("treasury"))
        {
            BuildTimer.Instance.treasuryBuildingTimer = treasury_Upgrade_Duration;
            return treasury_Upgrade_Duration;
        }
        if (buildingName.ToLower().Contains("guild"))
        {
            BuildTimer.Instance.guildBuildingTimer = guild_Upgrade_Duration;
            return guild_Upgrade_Duration;
        }
        if (buildingName.ToLower().Contains("forge"))
        {
            BuildTimer.Instance.forgeBuildingTimer = forge_UpgradeDuration;
            return forge_UpgradeDuration;
        }
        if (buildingName.ToLower().Contains("recruitmenthall"))
        {
            BuildTimer.Instance.rHallBuildingTimer = recruitment_Upgrade_Duration;
            return recruitment_Upgrade_Duration;
        }
        if (buildingName.ToLower().Contains("mine"))
        {
            BuildTimer.Instance.mineBuildingTimer = mines_Upgrade_Duration[mine_Upgrading_Now];
            return mines_Upgrade_Duration[mine_Upgrading_Now];
        }
        return 0;
    }

    // ==== STRUCTURES UPGRADES END ==== \\



    // ==== UNIT ASSIGNMENT HANDLERS ==== \\
    public int PointsLeft()
    {
        return available_Assign_Points;
    }

    public void CheckAssignmentPoints()
    {
        int usedPts = TroopAssign_Info.Instance.UsedAssignmentPoints();

        available_Assign_Points = assign_Points_Limit - usedPts;

        if (available_Assign_Points < 0)
            assignUnitButton.onClick.Invoke();
    }
    public void CheckAssignmentPointResolve()
    {
        if (available_Assign_Points >= 0)
            assingmentPointsResolved();
    }

    // ===== HEROES ASSIGNMENT LIMIT ===== \\
    public void CheckAssignedUnits()
    {
        bool limitExceed = TroopAssign_Info.Instance.Check_For_Troop_Limit();
        if (limitExceed)
            assignUnitButton.onClick.Invoke();
    }
    public void CheckAssignUnitResolved()
    {
        if (TroopAssign_Info.Instance.Check_For_Troop_Limit() == false)
            unitLimitResolved();
    }
    public void Calculate_Number_Of_Troops()
    {
        //reset value of "number_Of_Troops_Assigned"
        number_Of_Troops_Assigned = 0;

        for (int x = 0; x < assignedTroops.Length; x++)
        {
            if (assignedTroops[x] != 250)
            {
                number_Of_Troops_Assigned += 1;
            }
        }
    }

    // ==Old Unit Assignments == \\

    public void CheckGroupLimit()
    {
        if (castleLevel >= 1)
        {
            groupLimit = 2;
            if (castleLevel >= 4)
            {
                groupLimit += 1;
                if (castleLevel >= 8)
                {
                    groupLimit += 1;
                    if (castleLevel >= 10)
                    {
                        groupLimit += 1;
                    }
                }
            }
        }
    }
    public void Check_Group_Value()
    {
        if (myGroups <= 0)
        {
            myGroups = 0;
        }
        else if (myGroups >= 99)
        {
            myGroups = 99;
        }
    }
    public void Check_SUnits_Value()
    {
        if (landmines <= 0)
        {
            landmines = 0;
        }
        else if (landmines >= 99)
        {
            landmines = 99;
        }
        if (dragons <= 0)
        {
            dragons = 0;
        }
        else if (dragons >= 99)
        {
            dragons = 99;
        }
        if (seige <= 0)
        {
            seige = 0;
        }
        else if (seige >= 99)
        {
            seige = 99;
        }
    }
    public void CheckHeroSlots()
    {
        if (castleLevel >= 2)
        {
            heroSlots = 1;
            if (castleLevel >= 5)
            {
                heroSlots = 2;
                if (castleLevel >= 7)
                {
                    heroSlots = 3;
                }
            }
        }
    }

    public void sUnit_LandMines_Equip(bool newValue)
    {
        if (newValue == true)
            equippedSupports[0] = 1;
        else
            equippedSupports[0] = 0;

        PlayerPrefs.SetInt("BringMines", equippedSupports[0]);
    }
    public void sUnit_Dragon_Equip(bool newValue)
    {
        if (newValue == true)
            equippedSupports[1] = 1;
        else
            equippedSupports[1] = 0;
        PlayerPrefs.SetInt("BringDragons", equippedSupports[1]);
    }
    public void sUnit_Seige_Equip(bool newValue)
    {
        if (newValue == true)
            equippedSupports[2] = 1;
        else
            equippedSupports[2] = 0;
        PlayerPrefs.SetInt("BringSeige", equippedSupports[2]);
    }

    //==== UNIT ASSIGNMENT HANDLERS END ==== \\


    // ==== UPDATING EVENTS ==== \\
    public void UpdateEvents()
    {

    }


    // ==== UPDATING EVENTS END ==== \\
    public void UpdatePlayerEnergy()
    {
        float timeDifference = TimeMaster.Instance.CalculateTimeDifference(DataManager.initialLoginServerTime, DataManager.lastLogout);

        if (currentEnergyRechargeTime > energyRechargeTime)
            currentEnergyRechargeTime = energyRechargeTime;

        int energyObtained = Mathf.FloorToInt(timeDifference / energyRechargeTime);

        RechargeEnergy(energyObtained);

        if (playerCurrentEnergy < maxEnergy)
        {
            float timeLeft = timeDifference - (energyObtained * energyRechargeTime);

            currentEnergyRechargeTime -= timeLeft;
        }
    }

    public void UpdateTutorialValue()
    {
        tutorial += 1;
        if (tutorial >= tutorialCap)
        {
            UIManager.Instance.ChatCanvasDisplay();
            GameSparksManager.Instance.InsertPlayerIntoSearchableList();
        }
    }

    public void UpdatePlayerMission()
    {
        MissionHolder.Instance.InitMissionsInt();
        for (int x = 0; x < 3; x++)
        {
            MissionHolder.Instance.DelegateMission(x, playerMission[x], playerMissionProgress[x], playerMissionState[x], playerMissionRefreshTime[x], isInit);
        }
        MissionHolder.Instance.AssignMissionToButtons();
        MissionHolder.Instance.BeginMissionCountdown();
    }

    public string RepRequiredToNextCastleLevel()
    {
        if (castleLevel < castleMaxLevel)
            return repRequired[castleLevel].ToString();
        return "\n<color=#FF0000>(Castle at MAX LEVEL)</color>";
    }

    public void AssignPotraitID(string id)
    {
        potraitID = id;
    }

    public float victoryPercentage()
    {
        float matches = my_Matches;

        float victory = my_Victories;
        return ((my_Victories * 100f) / my_Matches);
    }

    public float Treasury_Filled_Perc()
    {
        return ((gold * 100f / treasury_Storage_Cap * 1.0f));
    }

    public void Battles_Entered_Count(bool victoryOutcome)
    {
        //check if the troop entered the fight or not
        for (int x = 0; x < assignedTroops.Length; x++)
        {
            if (assignedTroops[x] == 100 && !squareAdded)
            {
                DataManager.meleeBattleCount += 1;
                if (squareBattles >= evolvementRequiredBattles)	//TODO uncomment these
                    squareBattles = evolvementRequiredBattles;

                MissionHolder.Instance.UpdateMission("melee", 1);
                squareAdded = true;
            }
            if (assignedTroops[x] == 101 && !triangleAdded)
            {
                DataManager.rangeBattleCount += 1;
                if (triangleBattles >= evolvementRequiredBattles)
                    triangleBattles = evolvementRequiredBattles;

                MissionHolder.Instance.UpdateMission("range", 1);
                triangleAdded = true;
            }
            if (assignedTroops[x] == 102 && !circleAdded)
            {
                DataManager.casterBattleCount += 1;
                if (circleBattles >= evolvementRequiredBattles)
                    circleBattles = evolvementRequiredBattles;

                MissionHolder.Instance.UpdateMission("caster", 1);
                circleAdded = true;
            }
        }
        if (squareAdded && circleAdded && triangleAdded)
        {
            MissionHolder.Instance.UpdateMission("for victory", 1);
        }
        if (squareAdded && circleAdded)
        {
            MissionHolder.Instance.UpdateMission("might and magic", 1);
        }
        if (squareAdded && triangleAdded)
        {
            MissionHolder.Instance.UpdateMission("so near yet so far", 1);
        }
        if (circleAdded && triangleAdded)
        {
            MissionHolder.Instance.UpdateMission("behind the lines", 1);
        }

        UpdatePlayerBattleMissions(victoryOutcome);
    }
    public void UpdatePlayerBattleMissions(bool victoryOutcome)
    {
        MissionHolder.Instance.UpdateMission("enter battle", 1);

        if (victoryOutcome)
            MissionHolder.Instance.UpdateMission("win battle", 1);
    }



    public void StoreAllData()
    {
        DataManager.Instance.PrepareDataForSparksPushing();
        GameSparksManager.Instance.StoreAllData();
    }

    void Reset()
    {
        reputation = 100;
        tutorial = 0;
        playerUniqueID = "";

        race = "human";        // to be keyed in lowercase

        decksAvailable = 1;
        groupLimit = 10;
        heroSlots = 0;
        my_Matches = 0;
        my_Victories = 0;
        my_Losses = 0;
        my_Win_Percentage = 0f;

        earthStoneBuildSlot = 1;
        windStoneBuildSlots = 1;
        flameStoneBuildSlot = 1;

        playerCurrentEnergy = maxEnergy;
        currentEnergyRechargeTime = 0;

        for (int x = 0; x < 3; x++)
        {
            playerMission[x] = 0;
            playerMissionProgress[x] = 0;
            playerMissionState[x] = 0;
            playerMissionRefreshTime[x] = -100;
        }

        jackPack = 0;
        queenPack = 0;
        kingPack = 0;

        bronzeChest = 0;
        silverChest = 0;
        goldChest = 0;

        townHallLevel = 0;
        townHallCost = 3000;
        townHall_Upgrade_Duration = 30f;

        castleLevel = 1;
        castleMaxLevel = 10;
        castleMaxHealth = 3;
        castleHealth = 3;

        treasuryLvl = 0;
        treasuryLvlCap = 30;
        treasuryCost = 2000;
        treasury_Upgrade_Duration = 30f;
        treasury_Storage_Cap = 20000;
        treasury_Protection_Value = 6;

        timer3 = 0;
        timer15 = 0;
        timer30 = 0;
        timer60 = 0;

        bunkerLv = 0;
        bunkerCost = 2000;
        bunker_Upgrade_Duration = 30f;
        rangeLv = 0;
        rangeCost = 2000;
        range_Upgrade_Duration = 30f;
        guildLv = 0;
        guildCost = 2000;
        guild_Upgrade_Duration = 30f;

        stoneBuildDuration = 28800f;
        squareStone = 0;
        squareBattles = 0;
        triangleStone = 0;
        triangleBattles = 0;
        circleStone = 0;
        circleBattles = 0;
        evolvementRequiredBattles = 10;

        recruitmentHallLvlCap = 30;
        recruitmentHall_Lvl = 0;
        recruitment_Limit = 10;
        recruitmentHall_Cost = 1000;
        recruitment_Upgrade_Duration = 5f;
        assigned_Assign_Points = 0;           //Check for how many assign points are used
        available_Assign_Points = 100;          //Check for how many assign points are left
        assign_Points_Limit = 100;                  //Max value of points player will have ; used to check if number is max when removing units

        forgeLevel = 0;
        forge_UpgradeCost = 250000;
        forge_Built_Limit = 20;
        forge_UpgradeDuration = 5f;

        tavernLevel = 0;

        heroTimer15 = 0;
        heroTimer30 = 0;
        heroTimer60 = 0;
        heroTimer120 = 0;

        heroAmount = 17;
        heroLevels = new int[17];
        heroCardAmounts = new int[17];
        heroUnlocked = new int[17];

        assignedTroops = defaultAssignedTroops;

        resourceBuildingsUnlockedCounter = 1;

        minesBuilt = 0;
        mines = new int[5];
        minesRate = new int[] { 650, 650, 650, 650, 650 };
        minesCap = new int[] { 0, 0, 0, 0, 0 };
        minesCost = new int[] { 1000, 1000, 1000, 1000, 1000 };
        minesCurrentValue = new int[5];
        mines_Upgrade_Duration = new float[5];
        mine_Upgrading_Now = 255;

        gold = 18000;
        diamond = 200;
        tokens = 25;

        excalipoints = 0;
        firstTimePlaying = 0;

        MissionHolder.Instance.RemoveAssignedMission();
        MissionHolder.Instance.ResetAllMissions();

        UnitManager.Instance.ResetHeroInfo();

        DataManager.Instance.Reset();
    }

    // ====================================== HOME SCENE END \\

    // ========= // ========= PULLING FROM SERVER // ========= // ========= 


    // ---------- BATTLE SCENE ------------ \\

    public void Damage(int dmgAmt)
    {
        castleHealth -= dmgAmt;
        playerHit();

        if (castleHealth < 0)
            return;
        if (castleHealth == 0)
        {
            Enemy.Instance.CancelSpawning();
            AudioManager.Instance.DefeatJingle();
            GameManager.Instance.BattleOutcome(false);
            GameManager.Instance.gameOverScreen.SetActive(true);
            RepValue(false);
            Battles_Entered_Count(false);
            onCastleDestroy();
            playerDead();
            if (accountType == "Guest")
            {
                SavePrefs();
            }
        }
    }

    // When a unit reaches a castle
    void OnTriggerEnter(Collider other)
    {
        if (name.Contains("Castle") & stillUnderAttack == false)
        {
            // ENEMY CASTLE DAMAGE
            //			if (other.GetComponent<BaseUnit> ().isFriendlyUnit == true & this.gameObject.GetComponent<BaseUnit> ().isFriendlyUnit == false)
            //			{
            //				if (SceneManager.GetActiveScene ().name == "3_Battle") 
            //				{
            //					this.stillUnderAttack = true;
            //					Damage (1);
            //					Enemy enemyCastle = this.GetComponent<Enemy> ();
            //					if (enemyCastle != null)
            //					{
            //						enemyCastle.flags [enemyCastle.castleHealth].gameObject.SetActive (false);
            //					}
            //					this.stillUnderAttack = false;
            //				}
            //			}
            // PLAYER CASTLE DAMAGE
            if (other.GetComponent<BaseUnit>().isFriendlyUnit == false & gameObject.GetComponent<BaseUnit>().isFriendlyUnit == true)
            {
                stillUnderAttack = true;
                Damage(1);
                Player myCastle = GetComponent<Player>();

                if (myCastle != null)
                {
                    UIManager.Instance.HideFlag();
                }

                Hero hero = other.GetComponent<Hero>();
                if (hero != null)
                {
                    //if (hero.heroType == "Healer")
                    //{
                    //    hero.HealerAction();
                    //}
                    if (hero.heroType == "Speeder")
                    {
                        Damage(1);
                        if (myCastle != null)
                        {
                            UIManager.Instance.HideFlag();
                        }
                    }
                    //						else if (hero.hero
                }
                stillUnderAttack = false;
            }
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (GetComponent<BaseUnit>().isFriendlyUnit != other.GetComponent<Unit>().isFriendlyUnit)
        {
            stillUnderAttack = false;
            other.GetComponent<Unit>().FadingOutUnit(2.2f);
        }
    }

    public void CheckMyHealth()
    {
        // To contain Castle Health between 0 - 5
        if (castleHealth >= castleMaxHealth)
        {
            castleHealth = castleMaxHealth;
        }
        if (castleHealth <= 0)
        {
            castleHealth = 0;
        }
    }

    // ====== BATTLE SCENE END


    #region Temporary Save (Player Prefs)

    public static void SavePrefs()
    {
        PlayerPrefs.SetInt("MyTotalGroups", myGroups);
        PlayerPrefs.SetInt("MyMines", landmines);
        PlayerPrefs.SetInt("MyDragons", dragons);
        PlayerPrefs.SetInt("MySeige", seige);
        PlayerPrefs.SetInt("MyReputation", reputation);
        PlayerPrefs.SetInt("MyDiamonds", diamond);

        PlayerPrefs.SetInt("MyCurrentGold", gold);
        PlayerPrefs.SetInt("MyCurrentLumber", gold);
        PlayerPrefs.SetInt("GoldCap", goldCap);
        PlayerPrefs.SetInt("CastleLV", castleLevel);

        PlayerPrefs.SetInt("MySquareStones", squareStone);
        PlayerPrefs.SetInt("MySquareBattles", squareBattles);
        PlayerPrefs.SetInt("MyCircleStones", circleStone);
        PlayerPrefs.SetInt("MyCircleBattles", circleBattles);
        PlayerPrefs.SetInt("MyTriangleStones", triangleStone);
        PlayerPrefs.SetInt("MyTriangleBattles", triangleBattles);
        PlayerPrefs.SetInt("MyStoneBuildSlots", earthStoneBuildSlot);

        PlayerPrefs.SetInt("MinesBuilt", minesBuilt);

        PlayerPrefs.SetInt("Timers3", timer3);
        PlayerPrefs.SetInt("Timers15", timer15);
        PlayerPrefs.SetInt("Timers30", timer30);
        PlayerPrefs.SetInt("Timers60", timer60);

        PlayerPrefs.SetInt("JackPack", jackPack);
        PlayerPrefs.SetInt("QueenPack", queenPack);
        PlayerPrefs.SetInt("KingPack", kingPack);
    }

    public static void SaveBuildingInfo()
    {
        for (int x = 0; x < mines.Length; x++)
        {
            PlayerPrefs.SetInt("MyMinesLevel" + x, mines[x]);
            PlayerPrefs.SetInt("MyMinesGenRate" + x, minesRate[x]);
            PlayerPrefs.SetInt("MyMinesCap" + x, minesCap[x]);
            PlayerPrefs.SetInt("MyMinesCost" + x, minesCost[x]);
            PlayerPrefs.SetFloat("MyMinesUpgradeTime" + x, mines_Upgrade_Duration[x]);
        }
        PlayerPrefs.SetInt("BunkerLV", Player.bunkerLv);
        PlayerPrefs.SetFloat("BunkerUpDuration", bunker_Upgrade_Duration);
        PlayerPrefs.SetInt("BunkerCost", bunkerCost);
        PlayerPrefs.SetInt("RangeLV", Player.rangeLv);
        PlayerPrefs.SetFloat("RangeUpDuration", range_Upgrade_Duration);
        PlayerPrefs.SetInt("RangeCost", rangeCost);
        PlayerPrefs.SetInt("GuildLV", Player.guildLv);
        PlayerPrefs.SetFloat("GuildUpDuration", guild_Upgrade_Duration);
        PlayerPrefs.SetInt("GuildCost", guildCost);

        PlayerPrefs.SetInt("RecruitmentHallLV", Player.recruitmentHall_Lvl);
        PlayerPrefs.SetInt("RecruitmentHallCost", recruitmentHall_Cost);
        PlayerPrefs.SetInt("Recruitment_Limit", recruitment_Limit);
        PlayerPrefs.SetFloat("ShuffleTimer", shufflingTime);

        PlayerPrefs.SetInt("TownHallLV", townHallLevel);
        PlayerPrefs.SetInt("TownHall_Cost", townHallCost);
        PlayerPrefs.SetFloat("TownHall_Duration", townHall_Upgrade_Duration);

        PlayerPrefs.SetInt("TreasuryLv", treasuryLvl);
        PlayerPrefs.SetInt("Treasury_Cap", treasury_Storage_Cap);
        PlayerPrefs.SetInt("Treasury_Cost", treasuryCost);
        PlayerPrefs.SetFloat("Treasury_Protection", treasury_Protection_Value);
        PlayerPrefs.SetFloat("Treasure_Up_Duration", treasury_Upgrade_Duration);

        PlayerPrefs.SetInt("ForgeLv", forgeLevel);
        PlayerPrefs.SetInt("ForgeCost", forge_UpgradeCost);
        PlayerPrefs.SetFloat("ForgeDuration", forge_UpgradeDuration);
    }

    public static void LoadPrefs()
    {
        myGroups = PlayerPrefs.GetInt("MyTotalGroups");
        landmines = PlayerPrefs.GetInt("MyMines");
        dragons = PlayerPrefs.GetInt("MyDragons");
        seige = PlayerPrefs.GetInt("MySeige");
        reputation = PlayerPrefs.GetInt("MyReputation");
        diamond = PlayerPrefs.GetInt("MyDiamonds");

        gold = PlayerPrefs.GetInt("MyCurrentGold");
        gold = PlayerPrefs.GetInt("MyCurrentLumber");
        goldCap = PlayerPrefs.GetInt("GoldCap");
        castleLevel = PlayerPrefs.GetInt("CastleLV");

        squareStone = PlayerPrefs.GetInt("MySquareStones");
        squareBattles = PlayerPrefs.GetInt("MySquareBattles");
        circleStone = PlayerPrefs.GetInt("MyCircleStones");
        circleBattles = PlayerPrefs.GetInt("MyCircleBattles");
        triangleStone = PlayerPrefs.GetInt("MyTriangleStones");
        triangleBattles = PlayerPrefs.GetInt("MyTriangleBattles");
        earthStoneBuildSlot = PlayerPrefs.GetInt("MyStoneBuildSlots");
        assigned_Assign_Points = PlayerPrefs.GetInt("MyAssignedPoints");

        minesBuilt = PlayerPrefs.GetInt("MinesBuilt");

        timer3 = PlayerPrefs.GetInt("Timers3");
        timer15 = PlayerPrefs.GetInt("Timers15");
        timer30 = PlayerPrefs.GetInt("Timers30");
        timer60 = PlayerPrefs.GetInt("Timers60");

        jackPack = PlayerPrefs.GetInt("JackPack");
        queenPack = PlayerPrefs.GetInt("QueenPack");
        kingPack = PlayerPrefs.GetInt("KingPack");
    }

    public static void LoadBuildingInfo()
    {
        for (int x = 0; x < mines.Length; x++)
        {
            mines[x] = PlayerPrefs.GetInt("MyMinesLevel" + x);
            minesRate[x] = PlayerPrefs.GetInt("MyMinesGenRate" + x);
            minesCap[x] = PlayerPrefs.GetInt("MyMinesCap" + x);
            minesCost[x] = PlayerPrefs.GetInt("MyMinesCost" + x);
            mines_Upgrade_Duration[x] = PlayerPrefs.GetFloat("MyMinesUpgradeTime" + x);
        }

        bunkerLv = PlayerPrefs.GetInt("BunkerLV");
        bunker_Upgrade_Duration = PlayerPrefs.GetFloat("BunkerUpDuration");
        bunkerCost = PlayerPrefs.GetInt("BunkerCost");
        rangeLv = PlayerPrefs.GetInt("RangeLV");
        rangeCost = PlayerPrefs.GetInt("RangeCost");
        range_Upgrade_Duration = PlayerPrefs.GetFloat("RangeUpDuration");
        guildLv = PlayerPrefs.GetInt("GuildLV");
        guildCost = PlayerPrefs.GetInt("GuildCost");
        guild_Upgrade_Duration = PlayerPrefs.GetFloat("GuildUpDuration");

        recruitmentHall_Lvl = PlayerPrefs.GetInt("RecruitmentHallLV");
        recruitmentHall_Cost = PlayerPrefs.GetInt("RecruitmentHallCost");
        recruitment_Limit = PlayerPrefs.GetInt("Recruitment_Limit");
        shufflingTime = PlayerPrefs.GetFloat("ShuffleTimer");

        townHallLevel = PlayerPrefs.GetInt("TownHallLV");
        townHallCost = PlayerPrefs.GetInt("TownHall_Cost");
        townHall_Upgrade_Duration = PlayerPrefs.GetFloat("TownHall_Duration");

        treasuryLvl = PlayerPrefs.GetInt("TreasuryLv");
        treasury_Storage_Cap = PlayerPrefs.GetInt("Treasury_Cap");
        treasuryCost = PlayerPrefs.GetInt("Treasury_Cost");
        treasury_Protection_Value = PlayerPrefs.GetFloat("Treasury_Protection");
        treasury_Upgrade_Duration = PlayerPrefs.GetFloat("Treasure_Up_Duration");

        forgeLevel = PlayerPrefs.GetInt("ForgeLv");
        forge_UpgradeCost = PlayerPrefs.GetInt("ForgeCost");
        forge_UpgradeDuration = PlayerPrefs.GetFloat("ForgeDuration");
    }

    public static void Save_Player_Assigned_Slots()
    {
        for (int x = 0; x < assignedTroops.Length; x++)
        {
            PlayerPrefs.SetInt("_myAssignedTroop" + x, assignedTroops[x]);
        }
        PlayerPrefs.SetInt("MyAssignedPoints", assigned_Assign_Points);
    }
    public static void Load_Player_Assigned_Slots()
    {
        for (int x = 0; x < assignedTroops.Length; x++)
        {
            assignedTroops[x] = PlayerPrefs.GetInt("_myAssignedTroop" + x);
        }
    }


    #endregion

    public void TrainGroup()
    {
        //Add value to player available groups
        myGroups += 1;
    }

    public void TrainSupport(string supportBuilt)
    {
        if (supportBuilt.Contains("Mines"))
        { landmines += 1; }
        else if (supportBuilt.Contains("Dragon"))
        { dragons += 1; }
        else if (supportBuilt.Contains("Seige"))
        { seige += 1; }
        UIManager.Instance.Update_Building_LevelInfo();
        SavePrefs();
    }

    public void UpgradeMill()
    {
        //Adding 1 level to the mill that is upgraded
        mills[mill_Upgrading_Now] += 1;
        //Check if the current level is 1
        if (mills[mill_Upgrading_Now] >= 1 && mills[mill_Upgrading_Now] <= 6)
        {
            millsRate[mill_Upgrading_Now] += 5;
            millsCap[mill_Upgrading_Now] += 1080;
            millsCost[mill_Upgrading_Now] += 300;
            mills_Upgrade_Duration[mill_Upgrading_Now] = mills[mill_Upgrading_Now] * 1200;
        }
        else if (mills[mill_Upgrading_Now] >= 7 && mills[mill_Upgrading_Now] <= 15)
        {
            millsRate[mill_Upgrading_Now] += 15;
            millsCap[mill_Upgrading_Now] += 3240;
            millsCost[mill_Upgrading_Now] += 1500;
            mills_Upgrade_Duration[mill_Upgrading_Now] = mills[mill_Upgrading_Now] * 3600;
        }
        else if (mills[mill_Upgrading_Now] >= 16 && mills[mill_Upgrading_Now] <= 22)
        {
            //Increasing the generation rate of that building
            millsRate[mill_Upgrading_Now] += 30;
            millsCap[mill_Upgrading_Now] += 6480;
            millsCost[mill_Upgrading_Now] += 4500;
            mills_Upgrade_Duration[mill_Upgrading_Now] = mills[mill_Upgrading_Now] * 10800;
        }
        else if (mills[mill_Upgrading_Now] >= 23 && mills[mill_Upgrading_Now] <= 29)
        {
            //Increasing the generation rate of that building
            millsRate[mill_Upgrading_Now] += 50;
            millsCap[mill_Upgrading_Now] += 12960;
            millsCost[mill_Upgrading_Now] += 6000;
            mills_Upgrade_Duration[mill_Upgrading_Now] = mills[mill_Upgrading_Now] * 18000;
        }
    }
}