﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


public class Hero : MonoBehaviour, IPointerClickHandler
{
    public delegate void skillUsed(int useValue);
    public event skillUsed SkillUsage;

    public string heroBehavior;
    public string heroType;
    int heroIDNo;

    public int heroLevel;
    public int abilityLevel;                    //To determine the hero's ability level upon summon

    public int buffCounter;             //Used for Tanker health and abilities that can trigger multiple times

    //For tanker
    public int healthLeft;

    // For haste and snail and blocker
    public int targetLane;

    // For ability heroes
    public bool abilityActivated;

    //Stop players from triggering activie abilities if Hero is engaged
    bool isEngaged;

    // For Panther fades
    public Shader fadeShader;
    public Renderer rend;
    Renderer[] rends;

    Unit thisUnit;
    HeroMeshDisplay hmdisplay;

    // -------------------------------

    //Prepare hero abilities and info before they are spawned
    void Awake() 
	{
        thisUnit = GetComponent<Unit>();
        if (GetComponent<HeroMeshDisplay>() != null)
            hmdisplay = GetComponent<HeroMeshDisplay>();
	}
	// ====  ====  ====  ====  ====  ====  ====  ==== //

									//Hero UPDATES//

	// ====  ====  ====  ====  ====  ====  ====  ==== //
	public void MyHeroIdentityNumber(int idNo)	//To allow heroes to know what their abilities are
	{
		heroIDNo = idNo;
		MyHeroInfo();
        if (hmdisplay != null)
        {
            hmdisplay.HideAllLevelUpMeshes();
            hmdisplay.UpdateDisplayOnMesh(heroLevel);
        }
    }
	void MyHeroInfo()
	{
			for (int x = 0; x < UnitManager.Instance.heroInfomation.Count;x++)
			{
				if (heroIDNo == UnitManager.Instance.heroInfomation[x].heroNo)
				{
					heroLevel = UnitManager.Instance.heroInfomation[x].heroLvl;
					break;
				}
			}
//		}
		
		Verify_Whether_Hero();
	}
	public void Assign_Enemy_Hero_ID_And_Level(int heroID, int enemyHeroLevel)
	{
		heroIDNo = heroID;
		heroLevel = enemyHeroLevel;

		Verify_Whether_Hero();
	}

	void Verify_Whether_Hero()
	{
		if (heroLevel >= 3)
		{
			abilityLevel = heroLevel - 2;
			GetComponent<Unit> ().isHero = true;
		}
		else 
		{
			abilityLevel = 0;
			buffCounter = 0;
		}

		if (heroLevel <= 1)
		{
			GetComponent<Unit> ().isHero = false;
		}

		buffCounter = abilityLevel;
	}
	

    // ====  ====  ====  ====  ====  ====  ====  ==== //f

									//Hero Interaction//

	// ====  ====  ====  ====  ====  ====  ====  ==== //
	public void OnPointerClick(PointerEventData eventData)
	{
		if (heroBehavior == "Ability" & !isEngaged)
		{
			HeroAbility ();
		}
	}
	// ====  ====  ====  ====  ====  ====  ====  ==== //

								//HERO ABILITY CALLS//

	// ====  ====  ====  ====  ====  ====  ====  ==== //
	public void HeroSummon(int laneIndex)
	{
		//If heroes are on low level, their ability does not get triggered
		if(heroLevel <= 2)
		{
			return;
		}

		targetLane = laneIndex;
		switch (heroType)
		{
		// Buff Heroes
		case ("Refresher"):
			RefresherAction ();
			break;
		case ("Tanker"):
                TankerAction(laneIndex);
			break;

		//Passive skill Heroes
		case ("Blocker"):
			BlockerAction (laneIndex);
			break;
        case ("Shielder"):
            PriestessAction(laneIndex);
            break;
		case ("Haste1"):
			HasteAction (laneIndex);
			break;
		case ("Snail1"):
			SnailAction (laneIndex);
			break;
		case ("Haste3"):
			HastetresAction ();
			break;
		case ("Snail3"):
			SnailtresAction ();
			break;
		case ("Cooldowner"):
			CooldownerAction ();
			break;
		case ("Ninja"):
			NinjaAction(laneIndex);
			break;
		case ("Speeder"):
			SpeederAction();
			break;
		case ("SoulCharger"):
			SoulChargerAction();
			break;
		case ("Freezer"):
			FreezerAction(laneIndex);
			break;


		//Active skills
		case ("LaneChanger"):
			MyLane(laneIndex);
			break;
		}
	}
	public void HeroCombat(string type)
	{
		if(heroLevel <= 2)
			return;
		// Fighter type heroes abilities are activated upon combat
		if (type == "Fighter")
		{
//			switch (heroType)
//			{
//
//			}
		}

		// Aura type heroes will lose its effect after it is destroyed upon combat
		else if (type == "Aura")
		{
			switch (heroType)
			{
			case ("Blocker"):
				BlockerDebuff ();
				break;
			case ("Haste"):
				HasteDebuff ();
				break;
			case ("Snail"):
				SnailDebuff ();
				break;
			case ("Haste3"):
				HastetresDebuff ();
				break;
			case ("Snail3"):
				SnailtresDebuff ();
				break;
			case ("Cooldowner"):
				CooldownerDebuff ();
				break;
			case ("SoulCharger"):
				SoulChargeDeactivate ();
				break;
			case ("Ninja"):
				NinjaDebuff();
				break;
			}
		}
	}
	//Called when player taps on hero
	public void HeroAbility()
	{
		if(heroLevel <= 2)
			return;
		switch (heroType)
		{
		case("Teleporter"):
			TeleporterAction ();
			break;
		case("LaneChanger"):
			LaneChangerAction ();
			break;
		}
	}
	// ====  ====  ====  ====  ====  ====  ====  ====  ====  ==== //

							//SECTION FOR ALL HEROES ABILITIES //

	// ====  ====  ====  ====  ====  ====  ====  ====  ====  ==== //

    // ----- Buff upon Hero spawn ----- //
    public void TankerAction(int laneIndex)
	{
        buffCounter = abilityLevel + 1;
		healthLeft = buffCounter;

        if (abilityLevel >= 1)
            GameManager.Instance.MinosBlock(laneIndex, thisUnit.isFriendlyUnit);
	}
	public void BlockerAction(int laneIndex)
	{
        GameManager.Instance.Block_Lane(laneIndex, thisUnit.isFriendlyUnit, abilityLevel);
	}
	public void CooldownerAction()
	{
		float shuffle_Time_To_Add = (1.5f * (abilityLevel));
//		if (thisUnit.isFriendlyUnit)
//			Enemy.Instance.shuffleTimeForDisplay += shuffle_Time_To_Add;
		if (!thisUnit.isFriendlyUnit)
		{
			Player.shuffleIncrement += shuffle_Time_To_Add;
			if (!Player.isShuffling)
				UIManager.Instance.Player_Shuffling_Timer_Display(Player.Instance.CurrentShufflingTimer());
		}
	}
	public void HasteAction(int laneIndex)
	{
        GameManager.Instance.Haste_Lane(laneIndex,thisUnit.isFriendlyUnit,abilityLevel);
	}
	public void SnailAction(int laneIndex)
	{
        GameManager.Instance.Slow_Lane(laneIndex, thisUnit.isFriendlyUnit, abilityLevel);
	}
    public void PriestessAction(int laneIndex)
    {
        GameManager.Instance.ShieldUnitAhead(laneIndex, thisUnit.isFriendlyUnit, abilityLevel);
    }
	public void HastetresAction()
	{
        for (int x = 0; x < 3; x++)
        {
            GameManager.Instance.Haste_Lane(x, thisUnit.isFriendlyUnit, abilityLevel);
        }
	}
	public void SnailtresAction()
	{
        for (int x = 0; x < 3; x++)
        {
            GameManager.Instance.Slow_Lane(x, thisUnit.isFriendlyUnit, abilityLevel);
        }
	}
	public void RefresherAction()
	{
		if (thisUnit.isFriendlyUnit)
		{
//			float refreshTimer = 7 - (2 * this.abilityLevel);
//			Player.Instance.RefreshDeckCooldown(refreshTimer);
			Player.shuffleReduction = 1 * abilityLevel;
		}
		else if (!thisUnit.isFriendlyUnit)
		{
			Enemy.shuffleReduction = 1 * abilityLevel;
			Enemy.Instance.SetNewShuffleTime();
		}
		abilityActivated = true;
	}
	public void TeleporterAction()
	{
		if (thisUnit.isFriendlyUnit)
		{
            if (buffCounter > 0)
            {
                // play the effect here
                GameManager.Instance.PlayBlinkFromEffect(transform);

                // bring teleporter up
                Vector3 temp = transform.position;
                temp.y = temp.y + 50000;
                transform.position = temp;

                abilityActivated = true;
                SkillUsage(1);
                buffCounter -= abilityLevel;

                Invoke("TeleportDownAction", 0.25f);
			}
		}
	}
    void TeleportDownAction()
    {
        // Get the jump distance
        int jumpDist = (abilityLevel * 550) + 100;
        Vector3 temp = transform.position;
        temp.x = temp.x + jumpDist;
        temp.y = temp.y - 50000;
        transform.position = temp;
        // play the effect here
        GameManager.Instance.PlayBlinkToEffect(transform);
    }
	public void MyLane(int laneIndex)
	{
		targetLane = laneIndex;
	}
	public void LaneChangerAction()
	{
		if (thisUnit.isFriendlyUnit)
		{
            if (buffCounter > 0)
            {
                GameManager.Instance.PlayGenieFX(transform);
				int roll = Random.Range(1,3);
				targetLane += roll;
				if (targetLane > 2)
				{
					targetLane -= 3;
				}
				transform.SetParent (GameManager.Instance.playerLocs [targetLane].transform);
				transform.localPosition = new Vector3 (transform.localPosition.x, transform.localPosition.y, 0);
                SkillUsage(1);
                buffCounter -= 1;
			}
		}
	}
	public void NinjaAction(int laneIndex)
	{
		//Spawn on other 2 lanes using this formula
		if (thisUnit.isFriendlyUnit)
		{
			for (int x = 0; x < 2; x++)
			{
				//Get the troop that in inactive in the hierarchy
				for (int y = 0; y < TroopManager.Instance.all_Players_Units.Count; y++)
				{
					if (TroopManager.Instance.all_Players_Units[y].name.ToLower().Contains("illusion"))
					{
						if (!TroopManager.Instance.all_Players_Units[y].gameObject.activeInHierarchy)
						{
							int locPos = laneIndex + x + 1;
							if (locPos >= 3)
								locPos -= 3;
							Transform ninIllu = TroopManager.Instance.all_Players_Units[y];
							ninIllu.SetParent(GameManager.Instance.playerLocs[locPos].transform);
							ninIllu.position = GameManager.Instance.playerLocs[locPos].transform.position;
							ninIllu.gameObject.SetActive(true);
							break;
						}
					}
				}
			}
		}
		else if (!thisUnit.isFriendlyUnit)
		{
			for (int x = 0; x < 2; x++)
			{
				//Get the troop that in inactive in the hierarchy
				for (int y = 0; y < TroopManager.Instance.all_Enemy_Units.Count; y++)
				{
					if (TroopManager.Instance.all_Enemy_Units[y].name.ToLower().Contains("illusion"))
					{
						if (!TroopManager.Instance.all_Enemy_Units[y].gameObject.activeInHierarchy)
						{
							int locPos = laneIndex + x + 1;
							if (locPos >= 3)
								locPos -= 3;
							Transform ninIllu = TroopManager.Instance.all_Enemy_Units[y];
							ninIllu.SetParent(GameManager.Instance.enemyLocs[locPos].transform);
							ninIllu.position = GameManager.Instance.enemyLocs[locPos].transform.position;
							ninIllu.gameObject.SetActive(true);
							break;
						}
					}
				}
			}
		}
	}
	public void NinjaIllusionAction(float abilityLvl)
	{
		float timer = (abilityLvl * 6);
		Invoke ("IllusionDestroy", timer);
	}
	void IllusionDestroy()
	{
        TroopManager.Instance.KillUnits(transform, GetComponent<Unit>().myLoc);
	}
	public void SpeederAction()
	{
		GetComponent<Unit>().speed += (10 * abilityLevel);
    }
	public void SoulChargerAction()
	{
		GameManager.soulEnhanced = true;
		GameManager.soulChargeValue = abilityLevel;
	}
	public void FreezerAction(int laneIndex)
	{
		if (thisUnit.isFriendlyUnit)
		{
			Unit[] frozenUnits = GameManager.Instance.enemyLocs[laneIndex].GetComponentsInChildren <Unit>();
			GameManager.eFrozenLane[laneIndex] = 1;
			foreach (Unit frozenUnit in frozenUnits)
				frozenUnit.Frozen(laneIndex);
		}
		else if (!thisUnit.isFriendlyUnit)
		{
			Unit[] frozenUnits = GameManager.Instance.playerLocs[laneIndex].GetComponentsInChildren <Unit>();
			GameManager.pFrozenLane[laneIndex] = 1;
			foreach (Unit frozenUnit in frozenUnits)
				frozenUnit.Frozen(laneIndex);
		}

        float duration = heroLevel * 2f;

        Invoke("FreezerDebuff", duration);
	}
	void BreakInvisibility()
	{
	}
	// ----- Upon entering enemy's Castle ----- //
	public void HealerAction()
	{
		if (thisUnit.isFriendlyUnit)
		{
			Player.castleHealth += abilityLevel;
			Player.Instance.CheckMyHealth();
			UIManager.Instance.DisplayFlag();
		}
		else if (!thisUnit.isFriendlyUnit)
		{
			Enemy.Instance.castleHealth += abilityLevel;
			Enemy.Instance.Check_Health();
            UIManager.Instance.DisplayFlag();
        }
        GameManager.Instance.HealVisual(thisUnit.isFriendlyUnit, thisUnit.myLoc);
	}

	// ----- Debuffing upon Hero's death ----- \\
	public void TankerDebuff()
    {
        GameManager.Instance.MinosUnblock(targetLane, thisUnit.isFriendlyUnit);
    }
    public void BlockerDebuff()
	{
		//if (thisUnit.isFriendlyUnit)
  //          GameManager.eBlocked[GameManager.eBlocked[targetLane]] -= 1;
		//else if (!thisUnit.isFriendlyUnit)
        //    GameManager.pBlocked[GameManager.pBlocked[targetLane]] -= 1;
        
        //GameManager.Instance.BlockVisual(targetLane, thisUnit.isFriendlyUnit, false);
        //CancelInvoke("BlockerDebuff");
	}
	public void CooldownerDebuff()
	{
	
	}
	public void HasteDebuff()
	{
        GameManager.Instance.UnHaste_Lane(targetLane,thisUnit.isFriendlyUnit);
        GameManager.Instance.Reapply_Haste(thisUnit.isFriendlyUnit, this);
	}
	public void SnailDebuff()
    {
        GameManager.Instance.UnSlow_Lane(targetLane, thisUnit.isFriendlyUnit);
        GameManager.Instance.Reapply_Slow(thisUnit.isFriendlyUnit, this);
	}
	public void HastetresDebuff()
	{
        for (int x = 0; x < 3; x++)
        {
            GameManager.Instance.UnHaste_Lane(x, thisUnit.isFriendlyUnit);
        }
        GameManager.Instance.Reapply_Haste(thisUnit.isFriendlyUnit, this);
	}
	public void SnailtresDebuff()
    {
        for (int x = 0; x < 3; x++)
        {
            GameManager.Instance.UnSlow_Lane(x, thisUnit.isFriendlyUnit);
        }
        GameManager.Instance.Reapply_Slow(thisUnit.isFriendlyUnit, this);
	}
	public void NinjaDebuff()
	{
		if (thisUnit.isFriendlyUnit)
		{
			//destroy illusions
			List <Transform> illusions = new List <Transform>();
			for (int i = 0 ; i < GameManager.Instance.playerLocs.Length ; i++)
			{
				for (int j = 0; j < GameManager.Instance.playerLocs[i].transform.childCount; j++)
				{
					if (GameManager.Instance.playerLocs[i].transform.GetChild(j).name.ToLower().Contains("illusion"))
					{
						GameManager.Instance.playerLocs[i].transform.GetChild(j).GetComponent<Hero>().IllusionDestroy();
						break;
					}
				}
			}
		}
		else if (!thisUnit.isFriendlyUnit)
		{
			//destroy illusions
			List <Transform> illusions = new List <Transform>();
			for (int i = 0 ; i < GameManager.Instance.enemyLocs.Length ; i++)
			{
				for (int j = 0; j < GameManager.Instance.enemyLocs[i].transform.childCount; j++)
				{
					if (GameManager.Instance.enemyLocs[i].transform.GetChild(j).name.ToLower().Contains("illusion"))
					{
						GameManager.Instance.enemyLocs[i].transform.GetChild(j).GetComponent<Hero>().IllusionDestroy();
						break;
					}
				}
			}
		}
	}

    public void TeleporterDebuff()
    {
        buffCounter = abilityLevel;
    }
    public void LaneChangeDebuff()
    {
        buffCounter = abilityLevel;
    }

	public void SoulChargeDeactivate()
	{
		GameManager.soulEnhanced = false;
		GameManager.soulChargeValue = 0;
	}
	public void FreezerDebuff()
	{
        CancelInvoke("FreezerDebuff");

        int laneIndex = targetLane;

        if (thisUnit.isFriendlyUnit)
        {
            Unit[] frozenUnits = GameManager.Instance.enemyLocs[laneIndex].GetComponentsInChildren<Unit>();
            GameManager.eFrozenLane[laneIndex] = 0;
            foreach (Unit frozenUnit in frozenUnits)
                frozenUnit.Unfrozen(laneIndex);
        }
        if (!thisUnit.isFriendlyUnit)
        {
            Unit[] frozenUnits = GameManager.Instance.playerLocs[laneIndex].GetComponentsInChildren<Unit>();
            GameManager.pFrozenLane[laneIndex] = 0;
            foreach (Unit frozenUnit in frozenUnits)
                frozenUnit.Unfrozen(laneIndex);
        }
	}

	// ----- HERO already fighting ----- //
	public void HeroEngaged()
	{
		if (!isEngaged)
			isEngaged = true;
	}
	public void InvokeDisengage()
	{
		Invoke("HeroDisengaged",1.2f);
	}
	public void HeroDisengaged()
	{
		if (isEngaged)
			isEngaged = false;
	}

	public void DeactivateAbility()
	{
		abilityActivated = false;
	}
}
