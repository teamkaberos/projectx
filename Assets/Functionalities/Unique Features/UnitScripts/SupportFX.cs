﻿using UnityEngine;

public class SupportFX : MonoBehaviour 
{

    public string allegianceTrigger;  //to determine which way the flame plays
	int start = 0;

	void OnEnable()
	{
		InvokeRepeating("FlameOut",0.01f,0.05f);
		Invoke ("DisableMe",3);
	}

	public void FlameOut()
	{
        if (start < transform.childCount)
        {
            transform.GetChild(start).gameObject.SetActive(true);
            if (name.Contains("Ballista"))
            {
                AudioManager.Instance.SeigeBoomz();
            }
            start += 1;
        }
        else if (start >= transform.childCount)
        {
            CancelInvoke("FlameOut");
        }
	}

	void DisableMe()
	{
		HideFlames();
		start = 0;
		gameObject.SetActive(false);
	}

	void HideFlames()
	{
		for (int x = 0;x<transform.childCount;x++)
		{
			transform.GetChild(x).gameObject.SetActive(false);
		}
	}
}
