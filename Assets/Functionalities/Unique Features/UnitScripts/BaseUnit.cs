﻿using UnityEngine;

public class BaseUnit : MonoBehaviour 
{
	[Header("BaseUnit")]
	//DEFAULT 100
	public float speed;
	public int unitRank;

	public string unitClass;
	public int unitIndex;
	public bool isFriendlyUnit;
	public bool isImmovable;
	public bool isHero;

	public string counterUnit;

	protected virtual void Start()
	{
		Player.onCastleDestroy += StopMoving;
		Enemy.onCastleDestroy += StopMoving;
        StageTimer.roundOver += StopMoving;
        BackButton.retreated += StopMoving;
	}

	public void StopMoving()
	{
		speed = 0f;
	}

	void OnDestroy()
	{
		Player.onCastleDestroy -= StopMoving;
        Enemy.onCastleDestroy -= StopMoving;
        StageTimer.roundOver -= StopMoving;
        BackButton.retreated -= StopMoving;
	}
}
