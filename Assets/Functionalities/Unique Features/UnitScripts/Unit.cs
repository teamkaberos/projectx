﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Unit : BaseUnit
{
    public delegate void UnitKilled();
    public static event UnitKilled killTriggered;
    public static event UnitKilled enemyKilled;

    public delegate void BuffCounterUsed(int countValue);
    public event BuffCounterUsed buffUsed;

    public string colliderPosIndex;

    //FOR ANIMATION PURPOSES
    //To prevent a unit on performing attack animation on a enemy that is still playing its death animation
    public bool isDead;
    public bool isHasted;
    public bool isSlowed;
    public bool isFrozen;

    public int myLoc;

    public float baseSpeedValue;
    public int hasteValue;
    public int slowValue;

    public float rotateAxis;

    public Shader fadeShader;
    public Renderer rend;
    Renderer[] rends;

    Animator anim;

    public FXPlay[] attackFX;                   // for dual hand effects
    public FXPlay attackSecondaryFX;

    // For storing information of the enemy unit that this unit encounters
    public Unit myEnemy;
    public Hero myEnemyHero;


    // Shield area
    public bool shielded;
    public int shieldCount;
    public GameObject shieldedCanvas;      // the canvas gameobject
    Text shieldAmount;
    Vector3 friendlyCanvasRotation = new Vector3(5f, 130f, 0);
    Vector3 enemyCanvasRotation = new Vector3(5f, 310f, 0);

    Animator animator;

    Vector3 colliderCenterPos;

    AudioSource aSource;
    public AudioClip attackSFX;




    void Awake()
    {
        anim = GetComponent<Animator>();
        rends = new Renderer[0];
        rends = GetComponentsInChildren<Renderer>();
        fadeShader = Shader.Find("Transparent/Diffuse");

        animator = GetComponent<Animator>();

        if (GetComponent<AudioSource>() != null)
            aSource = GetComponent<AudioSource>();

        shieldedCanvas = transform.Find("<ShieldCanvas>").gameObject;
        shieldAmount = shieldedCanvas.transform.Find("<ShieldAmount>").GetComponent<Text>();

        if (Player.tutorial == 4)
        {
            speed = 220f;
            baseSpeedValue = 220f;
        }

        GetColliderPos();
    }
    void GetColliderPos()
    {
        colliderCenterPos = GetComponent<BoxCollider>().center;
    }
    public void SetCollider()
    {
        GetComponent<BoxCollider>().center = colliderCenterPos;
    }


    // CORE MECHANICS OF THE GAME
    // The calculation that happens when units meet
    // Called on the ColliderUnit script
    public void ScissorPaperStone(GameObject player, GameObject enemy)
    {
        //check if its an illusion
        if (player.name.Contains("Illusion"))
        {
            TroopManager.Instance.KillUnits(player.transform, myLoc);
            return;
        }
        if (enemy.name.Contains("Illusion"))
        {
            TroopManager.Instance.KillUnits(enemy.transform, myLoc);
            return;
        }

        //Check here first to see if the unit is a Speeder in which case checks if he has a shield
        if (player.name.Contains("Speeder"))
        {
            if (player.GetComponent<Unit>().shielded == true)
            {
                if (player.GetComponent<Unit>().shieldCount >= 1)
                {
                    // he walks through here
                    player.GetComponent<Unit>().DropShieldCount();
                    return;
                }
                // goblin dies here
                DestroyPlayer(player, enemy);
                return;
            }
            if (player.GetComponent<Unit>().shielded == false)
            {
                DestroyPlayer(player, enemy);
                return;
            }
        }
        if (enemy.name.Contains("Speeder"))
        {
            if (enemy.GetComponent<Unit>().shielded == true)
            {
                if (enemy.GetComponent<Unit>().shieldCount >= 1)
                {
                    // he walks through here
                    enemy.GetComponent<Unit>().DropShieldCount();
                    return;
                }
                // goblin dies here
                DestroyEnemy(player, enemy);
                return;
            }
            if (enemy.GetComponent<Unit>().shielded == false)
            {
                DestroyEnemy(player, enemy);
                return;
            }
        }

        //when 2 troops meet
        if (player != null & enemy != null)
        {
            if (colliderPosIndex == enemy.GetComponent<Unit>().colliderPosIndex)
            {
                // Stores the information of the enemy unit upon encounter
                myEnemy = enemy.GetComponent<Unit>();
                if (myEnemy.isHero == true)
                    myEnemyHero = myEnemy.GetComponent<Hero>();

                // ---------- Hero Lv2 and Above Calculation ---------- \\

                // Trigger the Hero Commands from the Hero script
                if (isHero == true)     //If my unit is a hero
                {
                    Hero hero = player.GetComponent<Hero>();
                    hero.HeroEngaged();

                    // The action performed depends on the heroBehavior ("Fighter" or "Aura")
                    // After performing their actions, destroy each other
                    // Check the comment below for more information regarding the if statement!
                    if (hero.heroType != "Tanker")
                    {
                        // === NON TANKER (PLAYER) VS TANKER (ENEMY) === \\
                        if (myEnemy.isHero == true && myEnemyHero.heroType == "Tanker")
                        {
                            // IF Enemy Tankers have only 1 HP
                            if (myEnemyHero.healthLeft == 1)
                            {
                                // non tanking hero meets low level tanker
                                if (shielded)
                                {
                                    ClearShield();
                                    hero.InvokeDisengage();
                                    DestroyEnemy(player, enemy);
                                }
                                else
                                {
                                    DestroyEachOther(player, enemy);
                                }

                                //if (!shielded && myEnemy.shielded == false)
                                //    DestroyEachOther(player, enemy);
                                //else if (shielded && myEnemy.shielded == false)
                                //{
                                //    ClearShield();
                                //    hero.InvokeDisengage();
                                //    DestroyEnemy(player, enemy);
                                //}
                                //else if (!shielded & myEnemy.shielded == true)
                                //{
                                //    myEnemy.ClearShield();
                                //    DestroyPlayer(player, enemy);
                                //}
                                //else if (shielded && myEnemy.shielded == true)
                                //DestroyEachOther(player, enemy);

                            }
                            else if (myEnemyHero.healthLeft > 1)
                            {
                                // if the enemy has more than 1 hp left
                                if (shielded)
                                    DestroyEachOther(player, enemy);
                                else
                                {
                                    myEnemyHero.healthLeft -= 1;
                                    DestroyPlayer(player, enemy);
                                }
                            }
                        }
                        // === NON TANKER (PLAYER) VS NON TANKER (ENEMY) === \\
                        else if (myEnemy.isHero == true && myEnemyHero.heroType != "Tanker")
                        {
                            //If enemy Hero counters Player Hero
                            if (counterUnit == myEnemy.unitClass)
                            {

                                if (!shielded && myEnemy.shielded == false)
                                {
                                    hero.HeroCombat(hero.heroBehavior);
                                    myEnemyHero.InvokeDisengage();
                                    DestroyPlayer(player, enemy);
                                }
                                else if (shielded && myEnemy.shielded == false)
                                {
                                    hero.HeroCombat(hero.heroBehavior);
                                    myEnemyHero.HeroCombat(myEnemyHero.heroBehavior);
                                    DestroyEachOther(player, enemy);
                                }
                                else if (!shielded && myEnemy.shielded == true)
                                {
                                    hero.HeroCombat(hero.heroBehavior);
                                    myEnemy.DropShieldCount();
                                    DestroyPlayer(player, enemy);

                                }
                                if (shielded && myEnemy.shielded == true)
                                {
                                    hero.HeroCombat(hero.heroBehavior);
                                    myEnemyHero.HeroCombat(myEnemyHero.heroBehavior);
                                    DestroyEachOther(player, enemy);
                                }

                            }
                            //Or if the enemy is same class as me
                            else if (unitClass == myEnemy.unitClass & myEnemy.isDead == false)
                            {
                                if (!shielded && myEnemy.shielded == false)
                                {
                                    hero.HeroCombat(hero.heroBehavior);
                                    myEnemyHero.HeroCombat(myEnemyHero.heroBehavior);
                                    DestroyEachOther(player, enemy);
                                }
                                else if (shielded && myEnemy.shielded == false)
                                {
                                    myEnemyHero.HeroCombat(myEnemyHero.heroBehavior);
                                    hero.InvokeDisengage();
                                    DropShieldCount();
                                    DestroyEnemy(player, enemy);
                                }
                                else if (!shielded && myEnemy.shielded == true)
                                {
                                    hero.HeroCombat(hero.heroBehavior);
                                    myEnemy.DropShieldCount();
                                    DestroyPlayer(player, enemy);
                                }
                                else if (shielded & myEnemy.shielded == true)
                                {
                                    hero.HeroCombat(hero.heroBehavior);
                                    myEnemyHero.HeroCombat(myEnemyHero.heroBehavior);
                                    DestroyEachOther(player, enemy);
                                }
                            }
                            //other than that, we'll just kill everything else
                            else
                            {
                                // if both are not shielded
                                if (!shielded && myEnemy.shielded == false)
                                {
                                    hero.InvokeDisengage();
                                    myEnemyHero.HeroCombat(myEnemyHero.heroBehavior);
                                    DestroyEnemy(player, enemy);
                                }
                                else if (shielded && myEnemy.shielded == false)
                                {
                                    hero.InvokeDisengage();
                                    myEnemyHero.HeroCombat(myEnemyHero.heroBehavior);
                                    DropShieldCount();
                                    DestroyEnemy(player, enemy);
                                }
                                else if (!shielded && myEnemy.shielded == true)
                                {
                                    hero.HeroCombat(hero.heroBehavior);
                                    myEnemyHero.HeroCombat(myEnemyHero.heroBehavior);
                                    DestroyEachOther(player, enemy);
                                }
                                else if (shielded & myEnemy.shielded == true)
                                {
                                    hero.HeroCombat(hero.heroBehavior);
                                    myEnemyHero.HeroCombat(myEnemyHero.heroBehavior);
                                    DestroyEachOther(player, enemy);
                                }
                            }
                        }
                        // === NON TANKER (PLAYER) VS BASE UNITS (ENEMY) === \\
                        else if (myEnemy.isHero == false)
                        {
                            // if both units are the same class
                            if (myEnemy.unitClass == unitClass & myEnemy.isDead == false)
                            {
                                if (!shielded & myEnemy.shielded == false)
                                {
                                    hero.HeroCombat(hero.heroBehavior);
                                    DestroyEachOther(player, enemy);
                                }
                                else if (shielded & myEnemy.shielded == false)
                                {
                                    hero.InvokeDisengage();
                                    DropShieldCount();
                                    DestroyEnemy(player, enemy);
                                }
                                else if (!shielded & myEnemy.shielded == true)
                                {
                                    hero.HeroCombat(hero.heroBehavior);
                                    myEnemy.ClearShield();
                                    DestroyPlayer(player, enemy);
                                }
                                else if (shielded & myEnemy.shielded == true)
                                {
                                    hero.HeroCombat(hero.heroBehavior);
                                    DestroyEachOther(player, enemy);
                                }
                            }
                            // if Enemy counters Player
                            else if (myEnemy.unitClass == counterUnit)
                            {
                                if (!shielded & myEnemy.shielded == false)
                                {
                                    hero.HeroCombat(hero.heroBehavior);
                                    DestroyEachOther(player, enemy);
                                }
                                else if (shielded & myEnemy.shielded == false)
                                {
                                    hero.InvokeDisengage();
                                    DropShieldCount();
                                    DestroyEnemy(player, enemy);
                                }
                                else if (!shielded & myEnemy.shielded == true)
                                {
                                    hero.HeroCombat(hero.heroBehavior);
                                    myEnemy.ClearShield();
                                    DestroyPlayer(player, enemy);
                                }
                                else if (shielded & myEnemy.shielded == true)
                                {
                                    hero.HeroCombat(hero.heroBehavior);
                                    DestroyEachOther(player, enemy);
                                }
                            }
                            // if Player counters Enemy
                            else if (myEnemy.counterUnit == unitClass)
                            {
                                if (!shielded & myEnemy.shielded == false)
                                {
                                    hero.InvokeDisengage();
                                    DestroyEnemy(player, enemy);
                                }
                                else if (shielded & myEnemy.shielded == false)
                                {
                                    hero.InvokeDisengage();
                                    DropShieldCount();
                                    DestroyEnemy(player, enemy);
                                }
                                else if (!shielded & myEnemy.shielded == true)
                                {
                                    hero.HeroCombat(hero.heroBehavior);
                                    DestroyEachOther(player, enemy);
                                }
                                else if (shielded & myEnemy.shielded == true)
                                {
                                    ClearShield();
                                    hero.HeroCombat(hero.heroBehavior);
                                    DestroyEnemy(player, enemy);
                                }
                            }
                        }
                    }
                    // Tanker is the only unique case where he got more than one health, so cannot destroy right away
                    else if (hero.heroType == "Tanker")
                    {
                        if (myEnemy.isHero == true && myEnemyHero.heroType == "Tanker")
                        {
                            // ==== TANKER VS TANKER AREA ==== \\
                            if (hero.abilityLevel < 1)
                            {
                                if (myEnemyHero.abilityLevel < 1)
                                {
                                    if (!shielded & myEnemy.shielded == false)
                                    {
                                        DestroyEachOther(player, enemy);
                                    }
                                    else if (shielded & myEnemy.shielded == false)
                                    {
                                        ClearShield();
                                        DestroyEnemy(player, enemy);
                                    }
                                    else if (!shielded & myEnemy.shielded == true)
                                    {
                                        myEnemy.ClearShield();
                                        DestroyPlayer(player, enemy);
                                    }
                                    else if (shielded & myEnemy.shielded == true)
                                    {
                                        DestroyEachOther(player, enemy);
                                    }
                                }
                                else if (myEnemyHero.abilityLevel >= 1)
                                {
                                    if (shielded)
                                    {
                                        DestroyEachOther(player, enemy);
                                    }
                                    else if (!shielded)
                                    {
                                        myEnemy.buffUsed(myEnemyHero.healthLeft);
                                        DestroyPlayer(player, enemy);
                                    }
                                }
                            }
                            else if (hero.abilityLevel >= 1)
                            {
                                if (myEnemyHero.abilityLevel < 1)
                                {
                                    if (myEnemy.shielded == false)
                                    {
                                        buffUsed(hero.healthLeft);
                                        DestroyEnemy(player, enemy);
                                    }
                                    else if (myEnemy.shielded == true)
                                    {
                                        DestroyEachOther(player, enemy);
                                    }
                                }
                                else if (myEnemyHero.abilityLevel >= 1)
                                {
                                    DestroyEachOther(player, enemy);
                                }
                            }
                        }
                        // ==== TANKER (PLAYER) VS NON TANK HERO (ENEMY) ==== \\
                        else if (myEnemy.isHero == true && myEnemyHero.heroType != "Tanker")
                        {
                            if (hero.heroLevel < 1)   // Using level to check here as 
                            {
                                // if ENEMY HERO SHAPE COUNTERS ME
                                if (myEnemy.unitClass == counterUnit)
                                {
                                    if (!shielded & myEnemy.shielded == false)
                                    {
                                        DestroyPlayer(player, enemy);
                                    }
                                    else if (shielded & myEnemy.shielded == false)
                                    {
                                        DestroyEachOther(player, enemy);
                                    }
                                    else if (!shielded & myEnemy.shielded == true)
                                    {
                                        myEnemy.DropShieldCount();
                                        DestroyPlayer(player, enemy);
                                    }
                                    else if (shielded & myEnemy.shielded == true)
                                    {
                                        myEnemy.ClearShield();
                                        DestroyPlayer(player, enemy);
                                    }
                                }
                                // if PLAYER COUNTERS ENEMY HERO
                                else if (unitClass == myEnemy.counterUnit)
                                {
                                    if (!shielded & myEnemy.shielded == false)
                                    {
                                        DestroyEnemy(player, enemy);
                                    }
                                    else if (shielded & myEnemy.shielded == false)
                                    {
                                        DropShieldCount();
                                        DestroyEnemy(player, enemy);
                                    }
                                    else if (!shielded & myEnemy.shielded == true)
                                    {
                                        DestroyEachOther(player, enemy);
                                    }
                                    else if (shielded & myEnemy.shielded == true)
                                    {
                                        ClearShield();
                                        DestroyEnemy(player, enemy);
                                    }
                                }
                                // if BOTH UNITS ARE SAME SHAPE
                                else if (myEnemy.unitClass == unitClass)
                                {
                                    if (!shielded & myEnemy.shielded == false)
                                    {
                                        DestroyEachOther(player, enemy);
                                    }
                                    else if (shielded & myEnemy.shielded == false)
                                    {
                                        ClearShield();
                                        DestroyEnemy(player, enemy);
                                    }
                                    else if (!shielded & myEnemy.shielded == true)
                                    {
                                        myEnemy.ClearShield();
                                        DestroyEnemy(player, enemy);
                                    }
                                    else if (shielded & myEnemy.shielded == true)
                                    {
                                        DestroyEachOther(player, enemy);
                                    }
                                }
                            }
                            else if (hero.abilityLevel >= 1)
                            {
                                if (myEnemy.shielded == true)
                                {
                                    buffUsed(hero.healthLeft);
                                    DestroyEachOther(player, enemy);
                                }
                                else if (myEnemy.shielded == false)
                                {
                                    if (hero.healthLeft > 1 & myEnemy.isDead == false)
                                    {
                                        buffUsed(1);
                                        hero.healthLeft -= 1;
                                        DestroyEnemy(player, enemy);
                                    }
                                    else if (hero.healthLeft == 1)
                                    {
                                        buffUsed(1);
                                        DestroyEachOther(player, enemy);
                                    }
                                }
                            }
                        }
                        // ==== TANKER (PLAYER) VS NON TANK HERO (ENEMY) ==== \\
                        else if (myEnemy.isHero == false)
                        {
                            // TANKER IS CONSIDERED A HERO TYPE
                            if (hero.abilityLevel < 1)
                            {
                                // IF ENEMY COUNTERS PLAYER
                                if (myEnemy.unitClass == counterUnit)
                                {
                                    if (!shielded & myEnemy.shielded == false)
                                    {
                                        DestroyEachOther(player, enemy);
                                    }
                                    else if (shielded & myEnemy.shielded == false)
                                    {
                                        ClearShield();
                                        DestroyEnemy(player, enemy);
                                    }
                                    else if (!shielded & myEnemy.shielded == true)
                                    {
                                        myEnemy.ClearShield();
                                        DestroyPlayer(player, enemy);
                                    }
                                    else if (shielded & myEnemy.shielded == true)
                                    {
                                        DestroyEachOther(player, enemy);
                                    }
                                }
                                // if PLAYER COUNTERS ENEMY UNIT
                                else if (unitClass == myEnemy.counterUnit)
                                {
                                    if (!shielded & myEnemy.shielded == false)
                                    {
                                        DestroyEnemy(player, enemy);
                                    }
                                    else if (shielded & myEnemy.shielded == false)
                                    {
                                        DropShieldCount();
                                        DestroyEnemy(player, enemy);
                                    }
                                    else if (!shielded & myEnemy.shielded == true)
                                    {
                                        ClearShield();
                                        DestroyEnemy(player, enemy);
                                    }
                                    else if (shielded & myEnemy.shielded == true)
                                    {
                                        ClearShield();
                                        DestroyEnemy(player, enemy);
                                    }
                                }
                                // if BOTH UNITS ARE SAME SHAPE
                                else if (myEnemy.unitClass == unitClass)
                                {
                                    if (!shielded & myEnemy.shielded == false)
                                    {
                                        DestroyEachOther(player, enemy);
                                    }
                                    else if (shielded & myEnemy.shielded == false)
                                    {
                                        ClearShield();
                                        DestroyEnemy(player, enemy);
                                    }
                                    else if (!shielded & myEnemy.shielded == true)
                                    {
                                        myEnemy.ClearShield();
                                        DestroyEnemy(player, enemy);
                                    }
                                    else if (shielded & myEnemy.shielded == true)
                                    {
                                        DestroyEachOther(player, enemy);
                                    }
                                }
                            }
                            // TANKER HAS NO TYPE
                            else if (hero.abilityLevel >= 1)
                            {
                                // no counter class for Tanker lv3 and above
                                if (myEnemy.shielded == false)
                                {
                                    if (hero.healthLeft > 1 & myEnemy.isDead == false)
                                    {
                                        buffUsed(1);
                                        hero.healthLeft -= 1;
                                        DestroyEnemy(player, enemy);
                                    }
                                    else if (hero.healthLeft == 1)
                                    {
                                        buffUsed(1);
                                        DestroyEachOther(player, enemy);
                                    }
                                }
                                else if (myEnemy.shielded == true)
                                {
                                    DestroyEachOther(player, enemy);
                                }
                            }
                        }
                    }
                }
                // ----------============ UNIT CALCULATIONS ============---------- \\
                else if (isHero == false)
                {
                    //if the enemy is a hero
                    if (myEnemy.isHero == true)
                    {
                        // Player regular units meets Enemy Tanker
                        if (myEnemyHero.heroType == "Tanker")
                        {
                            if (myEnemyHero.abilityLevel > 1)
                            {
                                if (myEnemyHero.healthLeft > 1 && isDead == false)
                                {
                                    myEnemyHero.healthLeft -= 1;
                                    DestroyPlayer(player, enemy);
                                }
                                else
                                    DestroyEachOther(player, enemy);
                            }
                            else
                            {
                                if (!shielded & myEnemy.shielded == false)
                                {
                                    DestroyEachOther(player, enemy);
                                }
                                else if (shielded & myEnemy.shielded == false)
                                {
                                    DestroyEachOther(player, enemy);
                                }
                                else if (!shielded & myEnemy.shielded == true)
                                {
                                    myEnemy.DropShieldCount();
                                    DestroyPlayer(player, enemy);
                                }
                                else if (shielded & myEnemy.shielded == true)
                                {
                                    DestroyEachOther(player, enemy);
                                }
                            }
                        }
                        // Player(nonhero) vs Enemy(hero,nontanker)
                        else
                        {
                            //If enemy counters me
                            if (myEnemy.counterUnit == unitClass)
                            {
                                if (!shielded & myEnemy.shielded == false)
                                {
                                    //we'll destroy each other
                                    myEnemyHero.HeroCombat(myEnemyHero.heroBehavior);
                                    DestroyEachOther(player, enemy);
                                }
                                else if (shielded & myEnemy.shielded == false)
                                {
                                    DropShieldCount();
                                    myEnemyHero.HeroCombat(myEnemyHero.heroType);
                                    DestroyEnemy(player, enemy);
                                }
                                else if (!shielded & myEnemy.shielded == true)
                                {
                                    myEnemyHero.InvokeDisengage();
                                    DestroyPlayer(player, enemy);
                                }
                                else if (shielded & myEnemy.shielded == true)
                                {
                                    myEnemyHero.InvokeDisengage();
                                    myEnemy.DropShieldCount();
                                    DestroyPlayer(player, enemy);
                                }
                            }
                            //Or if the enemy is same class as me
                            else if (myEnemy.unitClass == unitClass & isDead == false)
                            {
                                if (!shielded & myEnemy.shielded == false)
                                {
                                    //we'll destroy each other
                                    myEnemyHero.HeroCombat(myEnemyHero.heroBehavior);
                                    DestroyEachOther(player, enemy);
                                }
                                else if (shielded & myEnemy.shielded == false)
                                {
                                    DropShieldCount();
                                    myEnemyHero.HeroCombat(myEnemyHero.heroType);
                                    DestroyEnemy(player, enemy);
                                }
                                else if (!shielded & myEnemy.shielded == true)
                                {
                                    myEnemyHero.InvokeDisengage();
                                    myEnemy.DropShieldCount();
                                    DestroyPlayer(player, enemy);
                                }
                                else if (shielded & myEnemy.shielded == true)
                                {
                                    myEnemyHero.InvokeDisengage();
                                    myEnemy.DropShieldCount();
                                    DestroyPlayer(player, enemy);
                                }
                            }
                            //other than that, if Player counters enemy Hero, we'll kill each other
                            else
                            {
                                if (!shielded & myEnemy.shielded == false)
                                {
                                    //we'll destroy each other
                                    myEnemyHero.HeroCombat(myEnemyHero.heroBehavior);
                                    DestroyEachOther(player, enemy);
                                }
                                else if (shielded & myEnemy.shielded == false)
                                {
                                    DropShieldCount();
                                    myEnemyHero.HeroCombat(myEnemyHero.heroType);
                                    DestroyEnemy(player, enemy);
                                }
                                else if (!shielded & myEnemy.shielded == true)
                                {
                                    myEnemyHero.InvokeDisengage();
                                    myEnemy.DropShieldCount();
                                    DestroyPlayer(player, enemy);
                                }
                                else if (shielded & myEnemy.shielded == true)
                                {
                                    myEnemyHero.InvokeDisengage();
                                    DestroyEachOther(player, enemy);
                                }
                            }
                        }
                    }
                    else
                    {
                        // If both units that meet are of the same CLASS
                        if (unitClass == myEnemy.unitClass & myEnemy.isDead == false)
                        {
                            if (!shielded & myEnemy.shielded == false || shielded & myEnemy.shielded == true)
                                DestroyEachOther(player, enemy);
                            else if (shielded & myEnemy.shielded == false)
                            {
                                DropShieldCount();
                                DestroyEnemy(player, enemy);
                            }
                            else if (!shielded & myEnemy.shielded == true)
                            {
                                myEnemy.DropShieldCount();
                                DestroyPlayer(player, enemy);
                            }
                        }
                        // If player's Unit Counters enemy's Unit...
                        else if (unitClass == myEnemy.counterUnit)
                        {
                            if (!shielded & myEnemy.shielded == false)
                                DestroyEnemy(player, enemy);
                            else if (shielded & myEnemy.shielded == false)
                            {
                                DropShieldCount();
                                DestroyEnemy(player, enemy);
                            }
                            else if (!shielded & myEnemy.shielded == true)
                            {
                                DestroyEachOther(player, enemy);
                            }
                            else if (shielded & myEnemy.shielded == true)
                            {
                                DropShieldCount();
                                DestroyEnemy(player, enemy);
                            }
                        }
                        // If enemy's Unit Counters player's Unit...
                        else if (counterUnit == myEnemy.unitClass)
                        {
                            if (!shielded & myEnemy.shielded == false)
                                DestroyPlayer(player, enemy);
                            else if (shielded & myEnemy.shielded == false)
                            {
                                DropShieldCount();
                                DestroyEnemy(player, enemy);
                            }
                            else if (!shielded & myEnemy.shielded == true)
                            {
                                myEnemy.DropShieldCount();
                                DestroyPlayer(player, enemy);
                            }
                            else if (shielded & myEnemy.shielded == true)
                            {
                                myEnemy.DropShieldCount();
                                DestroyPlayer(player, enemy);
                            }
                        }
                    }
                }
            }
        }
    }

    //Setting the ATTACKING FX
    void AttackingAnim(GameObject player, GameObject enemy)
    {
        SetAttackingAnimation(player);
        SetDefendingAnimation(enemy, 2.1f);
        SetDeathAnimation(enemy);
    }
    //Setting the DEFENDING FX
    void DefendingAnim(GameObject player, GameObject enemy)
    {
        SetAttackingAnimation(enemy);
        SetDefendingAnimation(player, 2.1f);
        SetDeathAnimation(player);
    }

    // ANIMATION CONTROLLER  -------------------------------------------------------------------

    // *** For PLAY ATTACK EDITOR SCRIPT *** \\
    public void SetAnimatior()
    {
        anim = GetComponent<Animator>();
    }
    //Setting the ATTACKING ANIMATION
    public void SetAttacking()
    {
        anim.SetBool("Attacking", true);
        speed = 0f;
        Invoke("SetWalk", 0.2f);
    }
    public void SetWalk()
    {
        anim.SetBool("Attacking", false);
    }
    // *** For PLAY ATTACK EDITOR SCRIPT END *** \\


    public void SetAttackingAnimation(GameObject playerUnit)
    {
        playerUnit.GetComponent<Unit>().anim.SetBool("Attacking", true);
        playerUnit.GetComponent<Unit>().speed = 0f;
    }
    //Setting the DEFENDING ANIMATION
    public void SetDefendingAnimation(GameObject unit, float time)
    {
        unit.GetComponent<Unit>().anim.SetBool("Defending", true);
        unit.GetComponent<Unit>().speed = 0f;
    }
    //Setting the DYING ANIMATION
    public void SetDeathAnimation(GameObject playerUnit)
    {
        playerUnit.GetComponent<Unit>().isDead = true;
        playerUnit.GetComponent<Unit>().anim.SetBool("Dying", true);
        playerUnit.GetComponent<Unit>().speed = 0f;
        playerUnit.GetComponent<BoxCollider>().center = new Vector3(0, 500, 0);
        playerUnit.GetComponent<Unit>().FadingOutUnit(3.1f);
    }
    //Setting the DOUBLE ANIMATION
    public void DoubleDeathAnimation(GameObject player, GameObject enemy)
    {
        player.GetComponent<Unit>().anim.SetBool("Dying", true);
        player.GetComponent<Unit>().speed = 0f;
        enemy.GetComponent<Unit>().anim.SetBool("Dying", true);
        enemy.GetComponent<Unit>().speed = 0f;
        player.GetComponent<BoxCollider>().center = new Vector3(0, 500, 0);
        enemy.GetComponent<BoxCollider>().center = new Vector3(0, 500, 0);
        player.GetComponent<Unit>().FadingOutUnit(4f);
        enemy.GetComponent<Unit>().FadingOutUnit(4f);
    }

    IEnumerator SetWalkingAnimation(GameObject unit, float delayTime)
    {
        yield return new WaitForSeconds(delayTime);
        unit.GetComponent<Unit>().anim.SetBool("Attacking", false);
        unit.GetComponent<Unit>().CheckMovementSpeed(this.myLoc);
    }

    public void FadingOutUnit(float destroyTime)
    {
        Unhasted(myLoc);
        Unslowed(myLoc);
        Invoke("ReturnUnitsWhereTheyBelong", destroyTime);
    }

    public void ReturnUnitsWhereTheyBelong()
    {
        TroopManager.Instance.Reset_Hero_Ability(transform);
        TroopManager.Instance.KillUnits(transform, myLoc);
    }

    IEnumerator FadeOut(float duration)
    {
        yield return new WaitForSeconds(1.1f);
        foreach (Renderer r in rends)
        {
            Color newColor = r.material.color;
            r.material.shader = fadeShader;

            while (r.material.color.a > 0)
            {
                newColor.a -= Time.deltaTime / duration;
                r.material.color = newColor;
            }

            yield return new WaitForSeconds(0.07f);
        }
    }

    public void PlayAttackFX()
    {
        if (attackFX.Length >= 1)
        {
            foreach (FXPlay effect in attackFX)
            {
                effect.PlayEffect();
            }
        }
        if (attackSFX != null)
            aSource.PlayOneShot(attackSFX);
    }
    public void StopAttackFX()
    {
        if (attackFX.Length >= 1)
        {
            foreach (FXPlay effect in attackFX)
            {
                effect.StopEffect();
            }
        }
    }

    public void PlaySecondaryAttackFX()
    {
        if (attackSecondaryFX != null)
        {
            attackSecondaryFX.PlayEffect();
        }
    }
    public void StopSecondaryAttackFX()
    {
        if (attackSecondaryFX != null)
        {
            attackSecondaryFX.StopEffect();
        }
    }

    public void PlayWalkFX1()
    {
        if (attackFX.Length >= 1)
        {
            attackFX[0].PlayEffect();
        }
    }
    public void PlayWalkFX2()
    {
        if (attackFX.Length >= 1)
        {
            attackFX[1].PlayEffect();
        }
    }
    // ----- For Scissors-Papers-Stone ----- \\


    public void DestroyEachOther(GameObject player, GameObject enemy)
    {
        // IF BOTH UNITS KILL EACH OTHER
        player.GetComponent<Unit>().isDead = true;
        enemy.GetComponent<Unit>().isDead = true;
        SetAttackingAnimation(player);
        SetAttackingAnimation(enemy);
        DoubleDeathAnimation(player, enemy);
        killTriggered();
        Invoke("EnemyKilled", 1.8f);
        Player.Instance.IncreaseMedalsCharge();
    }

    public void DestroyEnemy(GameObject player, GameObject enemy)
    {
        // IF THE ENEMY IS KILLED
        enemy.GetComponent<Unit>().isDead = true;
        killTriggered();
        Invoke("EnemyKilled", 1.8f);
        AttackingAnim(player, enemy);
        StartCoroutine(SetWalkingAnimation(player, 0.5f));
        Player.Instance.IncreaseMedalsCharge();
        //Enemy.Instance.IncreaseSouls();
    }

    public void DestroyPlayer(GameObject player, GameObject enemy)
    {
        // IF THE PLAYER UNIT IS KILLED

        //Enemy.Instance.IncreaseSouls();
        DefendingAnim(player, enemy);
        StartCoroutine(SetWalkingAnimation(enemy, 0.5f));
    }

    void EnemyKilled()
    {
        enemyKilled();
    }

    // ====================================== \\

    public void Panther_Invis(bool isFriendly)
    {
        anim.SetBool("Hero", true);
        PantherFriendly(isFriendly);
    }
    public void PantherHero(bool isHero)
    {
        anim.SetBool("Hero", isHero);
    }
    void PantherFriendly(bool isFriendly)
    {
        anim.SetBool("Friendly", isFriendly);
    }

    // ---- Movement Buffs and Debuffs ----//
    public void Hasted(int laneIndex)
    {
        if (!isHasted)
        {
            isHasted = true;
            //FXManager.Instance.Haste(transform);
        }
        CheckMovementSpeed(laneIndex);
    }
    public void Unhasted(int laneIndex)
    {
        if (isHasted)
        {
            isHasted = false;
        }
        if (!isDead)
            CheckMovementSpeed(laneIndex);
    }
    public void Slowed(int laneIndex)
    {
        if (!isSlowed)
        {
            isSlowed = true;
            //FXManager.Instance.Slooww(transform);
        }
        CheckMovementSpeed(laneIndex);
    }
    public void Unslowed(int laneIndex)
    {
        if (isSlowed)
        {
            isSlowed = false;
        }
        if (!isDead)
            CheckMovementSpeed(laneIndex);
    }

    public void Shielded(int shieldValue)
    {
        //set as shielded
        shielded = true;
        //insert the shield values
        shieldCount = shieldValue;

        // only check if it is a hero
        if (unitIndex < 100)
            CheckAllegiance();
        // update the shielded amount text
        UpdateShieldedText();
        //set active the shield canvas
        ToggleShieldedCanvas(true);
    }
    void DropShieldCount()
    {
        shieldCount -= 1;
        UpdateShieldedText();

        if (shieldCount < 1)
        {
            Unshield();
            ToggleShieldedCanvas(false);
        }
    }
    public void ClearShield()
    {
        ResetShieldCount();
        Unshield();
        ToggleShieldedCanvas(false);
    }
    void ResetShieldCount()
    {
        shieldCount = 0;
    }
    void Unshield()
    {
        shielded = false;
    }
    void ToggleShieldedCanvas(bool isShielded)
    {
        shieldedCanvas.SetActive(isShielded);
    }
    void UpdateShieldedText()
    {
        if (shieldCount == 3)
            shieldAmount.text = "III";
        else if (shieldCount == 2)
            shieldAmount.text = "II";
        else if (shieldCount == 1)
            shieldAmount.text = "I";
    }
    void CheckAllegiance()
    {
        if (!isFriendlyUnit)
        {
            shieldedCanvas.transform.eulerAngles = new Vector3(0, 90f, 0);
        }
    }


    public void Frozen(int laneIndex)
    {
        if (!isFrozen)
            isFrozen = true;

        CheckMovementSpeed(laneIndex);
    }
    public void Unfrozen(int laneIndex)
    {
        if (isFrozen)
            isFrozen = false;

        CheckMovementSpeed(laneIndex);
    }

    public void CheckMovementSpeed(int myLoc)
    {
        if (isFriendlyUnit)
        {
            if (!isFrozen)
                speed = baseSpeedValue + GameManager.pHasted[myLoc] - GameManager.pSlowed[myLoc];
            else if (isFrozen)
                speed = 0;
            else if (isDead)
                speed = 0;
        }
        else if (!isFriendlyUnit)
        {
            if (!isFrozen)
                speed = baseSpeedValue + GameManager.eHasted[myLoc] - GameManager.eSlowed[myLoc];
            else if (isFrozen)
                speed = 0;
            else if (isDead)
                speed = 0;
        }


        float movementRatio = ((speed - baseSpeedValue) / 80f);

        Animate_Speed(movementRatio);

        // this function triggers after the player finishes attacking
        //		if (isFriendlyUnit)
        //		{
        //			if(isHasted)
        //			{
        //				this.speed = 130+ GameManager.pHasted[myLoc];
        //			}
        //			else if (isSlowed)
        //			{
        //				this.speed = 130- GameManager.pSlowed[myLoc];
        //			}
        //			else if (isSlowed & isHasted)
        //			{
        //				this.speed = 130 + GameManager.pHasted[myLoc] - GameManager.pSlowed[myLoc];
        //			}
        //			else
        //			{
        //				this.speed = 130f;
        //			}
        //		}
        //		else
        //		{
        //			if(isHasted)
        //			{
        //				this.speed = 130+ GameManager.eHasted[myLoc];
        //			}
        //			else if (isSlowed)
        //			{
        //				this.speed = 130- GameManager.eSlowed[myLoc];
        //			}
        //			else if (isSlowed & isHasted)
        //			{
        //				this.speed = 130 + GameManager.eHasted[myLoc] - GameManager.eSlowed[myLoc];
        //			}
        //			else
        //			{
        //				this.speed = 130f;
        //			}
        //		}
    }

    // ------------------------------------------------------------------------------------------


    // ===== Mechanim ===== \\
    void Animate_Speed(float movementSpeed_Multiplier)
    {
        if (!isDead && !isFrozen)
            animator.speed = 1 + movementSpeed_Multiplier;
    }

    // =======

    void Update()
    {
        // FOR MOVEMENT ---------------------------------
        if (!isDead)
        {
            if (isFriendlyUnit)
                transform.rotation = Quaternion.Euler(new Vector3(0, 90, 0));
            if (!isFriendlyUnit)
                transform.rotation = Quaternion.Euler(new Vector3(0, -90, 0));
            transform.Translate(Vector3.forward * Time.deltaTime * speed);
        }
        //  ---------------------------------------------
    }


    void TankerLoseHealth(GameObject player)
    {
        GameObject healthIndicator = player.transform.GetChild(0).gameObject;

        for (int x = 0; x < healthIndicator.transform.childCount; x++)
        {
            if (healthIndicator.transform.GetChild(x).gameObject.activeInHierarchy)
            {
                healthIndicator.transform.GetChild(x).gameObject.SetActive(false);
                break;
            }
        }
    }

}