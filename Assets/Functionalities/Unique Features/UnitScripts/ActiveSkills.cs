﻿using UnityEngine;

public class ActiveSkills : MonoBehaviour 
{
    public GameObject skillUsageCanvas;

    Unit unit;
    Hero hero;

    UsageCounter usageCounter;

    Transform canvasLoc;

    Quaternion rotation = Quaternion.Euler(0, 270, 0);

    int usageValue;

    public GameObject[] abilityUsageDisplay;

    void Start()
    {
        hero = GetComponent<Hero>();
        unit = GetComponent<Unit>();

        canvasLoc = transform.Find("<UsageLoc>").transform;

        if (unit.isFriendlyUnit && hero.buffCounter > 0)
        {
            GameObject usageCanvas = Instantiate(skillUsageCanvas, canvasLoc.position, rotation, canvasLoc);
            usageCounter = usageCanvas.GetComponent<UsageCounter>();

            usageValue = hero.buffCounter;
            usageCounter.usageValue = usageValue;
            usageCounter.abilityUsageDisplay = abilityUsageDisplay;

            if (unit.name.Contains("Tanker"))
            {
                unit.buffUsed += DropCount;
            }
            else if (unit.name.Contains("LaneChange"))
            {
                hero.SkillUsage += DropCount;
            }
            else if (unit.name.Contains("Teleporter"))
            {
                hero.SkillUsage += DropCount;
            }

            ResetCounter();
        }
    }

    public void ResetCounter()
    {
        if (hero.heroLevel <= 2)
            return;

        if (unit.isFriendlyUnit)
        {
            if (unit.name.Contains("Tanker"))
                usageCounter.DisplayCounter(usageValue-1);
            else if (unit.name.Contains("LaneChange"))
                usageCounter.DisplayCounter(usageValue);
            else if (unit.name.Contains("Teleporter"))
                usageCounter.DisplayCounter(1);
        }
    }


    void DropCount(int dropValue)
    {
        usageCounter.HideCounter(dropValue);
    }

}
