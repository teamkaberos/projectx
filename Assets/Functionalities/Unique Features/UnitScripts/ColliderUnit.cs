﻿using UnityEngine;
using UnityEngine.EventSystems;


public class ColliderUnit : MonoBehaviour, IPointerClickHandler
{
    // The Grid ID
    public string colliderIndex;

    // Basically how many units are inside a grid
    // Increases whenever a unit steps on a grid
    public int numberOfUnits;

    public GameObject playerUnit;
    public GameObject enemyUnit;

    void Start()
    {
        // Basically assign all the Grid IDs at the start of the scene
        colliderIndex = gameObject.name;
    }

    // This gets called when a unit steps on a grid
    void OnTriggerEnter(Collider other)
    {
        numberOfUnits += 1;
        // If it is a player unit..
        if (other.GetComponent<Unit>().isFriendlyUnit == true)
        {
            // ..stores it here.
            playerUnit = other.gameObject;
        }
        // But if it is an enemy unit..
        else if (other.GetComponent<Unit>().isFriendlyUnit != true)
        {
            // ..stores here instead.
            enemyUnit = other.gameObject;
        }
        // Sets the limit of how many units information can be stored inside a grid
        // In case there are two player / enemy units and one other unit in the same grid
        if (numberOfUnits > 2)
        {
            numberOfUnits = 2;
        }
        UnitsMeet();
    }

    // Basically tells the script that a unit has stepped out of a grid
    void OnTriggerExit(Collider other)
    {
        numberOfUnits -= 1;
        // If player unit dies
        if (other.GetComponent<Unit>().isFriendlyUnit == true & playerUnit != null)
        {
            playerUnit = null;
            return;
        }
        // If enemy unit dies
        if (other.GetComponent<Unit>().isFriendlyUnit != true & enemyUnit != null)
        {
            enemyUnit = null;
            return;
        }
        // If the surviving unit walks out
        ResetGridInfo();
    }

    // Resets every information stored inside a grid if there are no units on it
    // An extra step to prevent bugs
    void ResetGridInfo()
    {
        playerUnit = null;
        enemyUnit = null;
        numberOfUnits = 0;

        //		if (this.numberOfUnits == 0) 
        //		{
        //			this.playerUnit = null;
        //			this.enemyUnit = null;
        //		}
        //
        //		if (this.playerUnit == null & this.enemyUnit == null)
        //		{
        //			this.numberOfUnits = 0;
        //		}
    }

    // Called when there are two units on a grid
    void UnitsMeet()
    {
        // If the grid where the units meet is not a safezone..
        if (!colliderIndex.Contains("Safezone"))
        {
            // ..and if one of them is player unit, with the other one being an enemy unit
            if (numberOfUnits == 2 & playerUnit != null & enemyUnit != null)
            {
                // SOME FUN STUFF HAPPENS OVER HERE
                // OUR GAME'S CORE MECHANICS
                if (playerUnit.GetComponent<Unit>().isDead == false)
                {
                    playerUnit.GetComponent<Unit>().ScissorPaperStone(playerUnit, enemyUnit);
                    // For destroying lane hints whenever a unit gets destroyed
                    //					int laneID = 0;
                    //					if (this.colliderIndex.Substring (0, 1) == "A") 
                    //					{
                    //						laneID = 0;
                    //					}
                    //					if (this.colliderIndex.Substring (0, 1) == "B") 
                    //					{
                    //						laneID = 1;
                    //					}
                    //					if (this.colliderIndex.Substring (0, 1) == "C") 
                    //					{
                    //						laneID = 2;
                    //					}
                    //					print ("laneID: " + laneID);
                    //					int columnID = int.Parse(this.colliderIndex.Substring (1, 1));
                    //					if (GameManager.grids [laneID] [columnID].gameObject.GetComponent<ColliderUnit>().enemyUnit == null) 
                    //					{
                    //						int lastChild = GameManager.Instance.laneHints [laneID].transform.childCount;
                    //						print ("LastChild: " + lastChild);
                    //						if (GameManager.Instance.laneHints [laneID].transform.GetChild (lastChild) != null) 
                    //						{
                    //							print (GameManager.Instance.laneHints [laneID].transform.GetChild(lastChild).name);
                    //							Destroy (GameManager.Instance.laneHints [laneID].transform.GetChild(lastChild));
                    //						}
                    //					}
                }
            }

        }
    }

    // -------------------- SUPPORT UNIT -------------------- \\

    public void OnPointerClick(PointerEventData eventData)
    {
        string index = colliderIndex;
        if (GameManager.storedSupportIndex != null)
        {
            SummonSupportUnit(index, "friendly");
        }
    }

    public void SummonSupportUnit(string gridIndex, string isFriendly)
    {
        if (isFriendly == "friendly")
        {
            SupportUnitButton selected = DeploySupportUnits.sUnitSelected.GetComponent<SupportUnitButton>();
            if (selected.unitType == SUnitType.SUnit.Mines)
            {
                AudioManager.Instance.LandMines();

                if (selected.sUnitLevel == 1)
                    SummonSingleGarrison(gridIndex, Player.minesRequire[0], 1, isFriendly);
                else if (selected.sUnitLevel == 2)
                    SummonSingleGarrison(gridIndex, Player.minesRequire[1], 2, isFriendly);
                else if (selected.sUnitLevel == 3)
                    SummonSingleGarrison(gridIndex, Player.minesRequire[2], 3, isFriendly);
            }
            else if (selected.unitType == SUnitType.SUnit.Flames)
            {
                AudioManager.Instance.DragonFire();
                AudioManager.Instance.DragonGrowl();

                if (selected.sUnitLevel == 1)
                    SummonDragon(gridIndex, Player.dragsRequire[0], 1, isFriendly);
                else if (selected.sUnitLevel == 2)
                    SummonDragon(gridIndex, Player.dragsRequire[1], 2, isFriendly);
                else if (selected.sUnitLevel == 3)
                    SummonDragon(gridIndex, Player.dragsRequire[2], 3, isFriendly);
            }
        }
        else if (isFriendly == "enemy")
        {
            if (gridIndex.Contains("m"))
            {
                if (gridIndex.Contains("0"))
                    SummonSingleGarrison(gridIndex, Player.minesRequire[0], 1, isFriendly);
                else if (gridIndex.Contains("1"))
                    SummonSingleGarrison(gridIndex, Player.minesRequire[1], 2, isFriendly);
                else if (gridIndex.Contains("2"))
                    SummonSingleGarrison(gridIndex, Player.minesRequire[2], 3, isFriendly);
            }
        }
    }

    public void SummonGarrison(string gridIndex, int medalCost)
    {
        int targetRow = 0;
        targetRow = int.Parse(gridIndex.Substring(1, 2));
        // -1, as the array starts counting at 0, while the grid starts at 1
        // e.g: gridsA[0] = A1, gridsA[1] = A2, and so on.
        if (targetRow <= 32 & Player.soulCharges >= medalCost)
        {
            Transform targetGrid = GameManager.grids[1][targetRow - 1].transform;
            GameObject knightsGarrison = Instantiate(GameManager.Instance.garrison, new Vector3(targetGrid.position.x, targetGrid.position.y, targetGrid.position.z), Quaternion.identity);
            Player.Instance.Decrease_Soul_Charges(medalCost);
        }
    }

    public void SummonSingleGarrison(string gridIndex, int medalCost, int supportLevel, string isFriendly)
    {
        string targetLane = gridIndex.Substring(0, 1);                      //This gets the Letter which the mine is going to be deployed on
        int targetGrid = int.Parse(gridIndex.Substring(1, 2));            //This gets the grid number which the mine is going to be deployed on
        Transform spawnPoint = null;												//First spawn for mine

        if (isFriendly == "friendly")
        {
            if (Player.soulCharges >= medalCost)
            {
                if (targetGrid <= 32)
                {
                    if (supportLevel == 2)
                    {
                        if (targetLane.Contains("A"))
                            targetLane = "AB";
                        else if (targetLane.Contains("B"))
                            targetLane = "BC";
                        else if (targetLane.Contains("C"))
                            targetLane = "AC";
                    }
                    else if (supportLevel == 3)
                        targetLane = "ABC";
                }
            }
        }
        else if (isFriendly == "enemy")
        {
            if (Enemy.Instance.mySouls >= medalCost)
            {
                if (targetGrid <= 32)
                {
                    if (supportLevel == 2)
                    {
                        if (targetLane.Contains("A"))
                            targetLane = "AB";
                        else if (targetLane.Contains("B"))
                            targetLane = "BC";
                        else if (targetLane.Contains("C"))
                            targetLane = "AC";
                    }
                    else if (supportLevel == 3)
                        targetLane = "ABC";
                }
            }
        }

        if (targetLane.Contains("A"))
        {
            spawnPoint = GameManager.Instance.laneAGrids[targetGrid - 1].transform;
            GameObject knightsGarrison = Instantiate(GameManager.Instance.singleGarrison, new Vector3(spawnPoint.position.x, spawnPoint.position.y, spawnPoint.position.z), Quaternion.identity);
            knightsGarrison.GetComponentInChildren<Garrison>().allegiance = isFriendly;
        }
        if (targetLane.Contains("B"))
        {
            spawnPoint = GameManager.Instance.laneBGrids[targetGrid - 1].transform;
            GameObject knightsGarrison = Instantiate(GameManager.Instance.singleGarrison, new Vector3(spawnPoint.position.x, spawnPoint.position.y, spawnPoint.position.z), Quaternion.identity);
            knightsGarrison.GetComponentInChildren<Garrison>().allegiance = isFriendly;
        }
        if (targetLane.Contains("C"))
        {
            spawnPoint = GameManager.Instance.laneCGrids[targetGrid - 1].transform;
            GameObject knightsGarrison = Instantiate(GameManager.Instance.singleGarrison, new Vector3(spawnPoint.position.x, spawnPoint.position.y, spawnPoint.position.z), Quaternion.identity);
            knightsGarrison.GetComponentInChildren<Garrison>().allegiance = isFriendly;
        }

        // ===== UPDATE ON THE PLAYERS STUFF ===== \\
        if (isFriendly == "friendly")
        {
            Player.Instance.Decrease_Soul_Charges(medalCost);
        }
    }

    public void SummonDragon(string gridIndex, int medalCost, int supportLevel, string isFriendly)
    {
        string targetLane = "";
        if (isFriendly == "friendly")
        {
            if (Player.soulCharges >= medalCost)
            {
                targetLane = gridIndex.Substring(0, 1);

                if (supportLevel == 2)
                {
                    if (targetLane.Contains("A"))
                        targetLane = "AB";
                    else if (targetLane.Contains("B"))
                        targetLane = "BC";
                    else if (targetLane.Contains("C"))
                        targetLane = "AC";
                }
                else if (supportLevel == 3)
                    targetLane = "ABC";
            }
        }
        else if (isFriendly == "enemy")
        {
            targetLane = gridIndex.Substring(0, 1);

            if (supportLevel == 2)
            {
                if (targetLane.Contains("A"))
                    targetLane = "AB";
                else if (targetLane.Contains("B"))
                    targetLane = "BC";
                else if (targetLane.Contains("C"))
                    targetLane = "AC";
            }
            else if (supportLevel == 3)
                targetLane = "ABC";
        }

        if (targetLane.Contains("A"))
        {
            if (isFriendly == "friendly")
            {
                GameManager.Instance.playerLaneFlames[0].SetActive(true);
                foreach (GameObject units in GameManager.Instance.laneAGrids)
                {
                    if (units != null)
                    {
                        ColliderUnit u = units.GetComponent<ColliderUnit>();
                        if (u.enemyUnit != null)
                        {
                            u.enemyUnit.GetComponent<Unit>().speed = 0f;
                            u.enemyUnit.GetComponent<Unit>().SetDeathAnimation(u.enemyUnit);
                        }
                    }
                }
            }
            else if (isFriendly == "enemy")
            {
                GameManager.Instance.enemyLaneFlames[0].SetActive(true);
                foreach (GameObject units in GameManager.Instance.laneAGrids)
                {
                    if (units != null)
                    {
                        ColliderUnit u = units.GetComponent<ColliderUnit>();
                        if (u.playerUnit != null)
                        {
                            u.playerUnit.GetComponent<Unit>().speed = 0f;
                            u.playerUnit.GetComponent<Unit>().SetDeathAnimation(u.playerUnit);
                        }
                    }
                }
            }
        }
        if (targetLane.Contains("B"))
        {
            if (isFriendly == "friendly")
            {
                GameManager.Instance.playerLaneFlames[1].SetActive(true);
                foreach (GameObject units in GameManager.Instance.laneBGrids)
                {
                    if (units != null)
                    {
                        ColliderUnit u = units.GetComponent<ColliderUnit>();
                        if (u.enemyUnit != null)
                        {
                            u.enemyUnit.GetComponent<Unit>().speed = 0f;
                            u.enemyUnit.GetComponent<Unit>().SetDeathAnimation(u.enemyUnit);
                        }
                    }
                }
            }
            else if (isFriendly == "enemy")
            {
                GameManager.Instance.enemyLaneFlames[1].SetActive(true);
                foreach (GameObject units in GameManager.Instance.laneBGrids)
                {
                    if (units != null)
                    {
                        ColliderUnit u = units.GetComponent<ColliderUnit>();
                        if (u.playerUnit != null)
                        {
                            u.playerUnit.GetComponent<Unit>().speed = 0f;
                            u.playerUnit.GetComponent<Unit>().SetDeathAnimation(u.playerUnit);
                        }
                    }
                }
            }
        }
        if (targetLane.Contains("C"))
        {
            if (isFriendly == "friendly")
            {
                GameManager.Instance.playerLaneFlames[2].SetActive(true);
                foreach (GameObject units in GameManager.Instance.laneCGrids)
                {
                    if (units != null)
                    {
                        ColliderUnit u = units.GetComponent<ColliderUnit>();
                        if (u.enemyUnit != null)
                        {
                            u.enemyUnit.GetComponent<Unit>().speed = 0f;
                            u.enemyUnit.GetComponent<Unit>().SetDeathAnimation(u.enemyUnit);
                        }
                    }
                }
            }
            else if (isFriendly == "enemy")
            {
                GameManager.Instance.enemyLaneFlames[2].SetActive(true);
                foreach (GameObject units in GameManager.Instance.laneCGrids)
                {
                    if (units != null)
                    {
                        ColliderUnit u = units.GetComponent<ColliderUnit>();
                        if (u.playerUnit != null)
                        {
                            u.playerUnit.GetComponent<Unit>().speed = 0f;
                            u.playerUnit.GetComponent<Unit>().SetDeathAnimation(u.playerUnit);
                        }
                    }
                }
            }
        }
        if (isFriendly == "friendly")
        {
            Player.Instance.Decrease_Soul_Charges(medalCost);
        }
    }

}