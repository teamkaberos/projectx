﻿using UnityEngine;

public class HeroMeshDisplay : MonoBehaviour
{
    [System.Serializable]
    public class EachLevelOfMeshObject
    {
        public GameObject[] meshesInThisLevel;
    }

    public EachLevelOfMeshObject[] levelsOfMeshes;

    public void UpdateDisplayOnMesh(int heroLevel)
    {
        if (heroLevel > 1)
        {
            for (int x = heroLevel - 2; x < 2; x++)
            {
                foreach (GameObject meshToShow in levelsOfMeshes[x].meshesInThisLevel)
                {
                    meshToShow.SetActive(true);
                }
            }
        }
    }
    public void HideAllLevelUpMeshes()
    {
        for (int x = 0; x < levelsOfMeshes.Length; x++)
        {
            foreach (GameObject meshToShow in levelsOfMeshes[x].meshesInThisLevel)
            {
                meshToShow.SetActive(false);
            }
        }
    }
}
