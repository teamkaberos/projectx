﻿using UnityEngine;
using UnityEditor;

[CanEditMultipleObjects]
[CustomEditor(typeof(Unit))]
public class PlayAttack : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        Unit unit = (Unit)target;

        GUILayout.BeginHorizontal("box");


        if (GUILayout.Button("SetAnim", GUILayout.Width(90), GUILayout.Height(40)))
            unit.SetAnimatior();


        if (GUILayout.Button("Walk", GUILayout.Width(90), GUILayout.Height(40)))
            unit.SetWalk();

        if (GUILayout.Button("Attack", GUILayout.Width(90), GUILayout.Height(40)))
        {
            unit.SetAttacking();
        }

            GUILayout.EndHorizontal();
    }
}
