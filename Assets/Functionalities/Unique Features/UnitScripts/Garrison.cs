﻿using UnityEngine;

public class Garrison : MonoBehaviour 
{
	public static GameObject garrisonSummoned;
	public static int garrisonLv = 3;
	public static int totalHP;
	public int hits = 1;
    public string allegiance;
	bool stillUnderAttack;

	void Start()
	{
		totalHP = garrisonLv;
		garrisonSummoned = gameObject.transform.parent.gameObject;
	}

	void OnTriggerEnter(Collider other)
	{
		if (stillUnderAttack == false) 
		{
            // === IF THE SPAWNED LAND MIND BELONGS TO THE PLAYER === \\
            if (allegiance == "friendly")
            {
                if (other.GetComponent<BaseUnit>().isFriendlyUnit == false)
                {
                    stillUnderAttack = true;
                    hits -= 1;
                    totalHP -= 1;
                    other.GetComponent<Unit>().SetDeathAnimation(other.gameObject);
                    other.GetComponent<Unit>().isDead = true;
                    GameObject lmfx = Instantiate(GameManager.Instance.landMineFX, transform.position, Quaternion.identity);
                    stillUnderAttack = false;
                }
                else if (other.GetComponent<BaseUnit>().isFriendlyUnit == true)
                {
                    return;
                }
                if (hits <= 0)
                {
                    Destroy(gameObject);
                    if (totalHP <= 0)
                    {
                        Destroy(garrisonSummoned);
                    }
                }
            }

            // === IF THE SPAWNED LAND MIND BELONGS TO THE ENEMY === \\ if (isFriendlyUnit)
            else if (allegiance == "enemy")
            {
                if (other.GetComponent<BaseUnit>().isFriendlyUnit == true)
                {
                    stillUnderAttack = true;
                    hits -= 1;
                    totalHP -= 1;
                    other.GetComponent<Unit>().SetDeathAnimation(other.gameObject);
                    other.GetComponent<Unit>().isDead = true;
                    GameObject lmfx = Instantiate(GameManager.Instance.landMineFX, transform.position, Quaternion.identity);
                    stillUnderAttack = false;
                }
                else if (other.GetComponent<BaseUnit>().isFriendlyUnit == true)
                {
                    return;
                }
                if (hits <= 0)
                {
                    Destroy(gameObject);
                    if (totalHP <= 0)
                    {
                        Destroy(garrisonSummoned);
                    }
                }
            }
		}
	}
}
