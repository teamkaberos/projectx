﻿using UnityEngine;
using UnityEngine.UI;

public class SpawnBlockerBarManager : MonoBehaviour 
{
	public GameObject[] LoadingBar;
	public GameObject deck1LoadingBar;
	public GameObject deck2LoadingBar;

	public float cdTime;
	public float currentAmount;

	public static bool onCooldown = false;

	public static bool unitSpawned;
	public static int unitCount;

	private static SpawnBlockerBarManager _instance;
	public static SpawnBlockerBarManager Instance
	{
		get
		{
			return _instance;
		}
	}

	void Awake()
	{
		if (_instance == null)
		{
			_instance = this;
		}
		onCooldown = true;
		SpawnChecker();
	}

	void Start()
	{
		unitCount = 0;		//Reset this value to stop blocker from showing up on wrong unit spawns
//		cdTime = 1.5f;
//		currentAmount = cdTime;
	}

	void Update()
	{
		if (currentAmount <= cdTime & onCooldown == true) 
		{
			// For timer to start running
			currentAmount -= Time.deltaTime;

			for (int x = 0; x < LoadingBar.Length; x++)
			{
				deck1LoadingBar.GetComponent<Image> ().fillAmount = currentAmount / cdTime;
				deck2LoadingBar.GetComponent<Image> ().fillAmount = currentAmount / cdTime;
			}
		}
		if (currentAmount < 0)
		{
			currentAmount = 0f;
			onCooldown = false;
			for (int x = 0; x < LoadingBar.Length; x++)
			{
				deck2LoadingBar.SetActive(false);
				deck1LoadingBar.SetActive(false);
			}
			unitSpawned = false;
		}
	}

	public void AddUnitCount()
	{
		// For keeping track of spawned units
		// instead of adding it even when player spawns hero units
		unitCount += 1;
	}

	public void SpawnChecker()
	{
		unitSpawned = true;
	}

	public void Hide_Blocker_Bars()
	{
		if (Player.deck1Cards <= 0)	deck1LoadingBar.SetActive(false);

		if (Player.secondDeckEnabled && Player.deck2Cards <= 0)deck2LoadingBar.SetActive(false);
	}

	//public void Display_Blocker_Bars()
	//{
	//	deck1LoadingBar.SetActive(true);
		
	//	if (Player.secondDeckEnabled)
	//		deck2LoadingBar.SetActive(true);
	//}

	public void CheckDecks()
	{
		if (Player.deck1Cards >= 1)
		{
            deck1LoadingBar.SetActive(true);
		}
		if (Player.secondDeckEnabled)
		{
			if (Player.deck2Cards >= 1)
			{
                deck2LoadingBar.SetActive(true);
			}
		}
	}

	public void BlockCards()
	{
		//If the player has a unit spawned
		if (unitSpawned)
		{
			//If on cooldown and stored unit/hero index is null
			if (onCooldown == false & GameManager.storedUnitIndex != null || onCooldown == false & GameManager.storedHeroID != null)
			{
				//Causes the CD bar to appear and begin the countdown
				if (unitCount < (Player.number_Of_Troops_Assigned))
				{
					currentAmount = cdTime;
					onCooldown = true;
				}

                // reset this number when the unit count is the same as assigned numbers
				if (unitCount == Player.number_Of_Troops_Assigned)
				{
					unitCount = 0;
				}
                // if it is lesser, set active the blocker
				else if (unitCount < Player.number_Of_Troops_Assigned)
				{
					for (int y = 0; y <LoadingBar.Length; y++)
					{
						LoadingBar[y].SetActive(true);
					}
				}
			}
		}
	}
}
