﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SpawnButtonCooldownBar : MonoBehaviour 
{
	public Transform LoadingBar;

	public float cdTime;
	public float currentAmount;

	void Start()
	{
		cdTime = SpawnButton.laneCooldown;
		currentAmount = cdTime;
	}

	void Update()
	{
		if (currentAmount < cdTime) 
		{
			currentAmount += Time.deltaTime;
		}
		else if (currentAmount >= cdTime)
		{
			currentAmount = cdTime;
		}
		LoadingBar.GetComponent<Image> ().fillAmount = currentAmount / cdTime;
	}

	public void SetCooldown()
	{
		if (this.gameObject.GetComponent<SpawnButton>().onCooldown == false & GameManager.storedUnitIndex != null)
			currentAmount = 0;
	}


}