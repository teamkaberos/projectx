﻿using UnityEngine;
using UnityEngine.UI;

public class SupportUnitButton : MonoBehaviour
{
    public SUnitType.SUnit unitType;
    [HideInInspector] public int sUnitLevel;
    [HideInInspector] public int requiredSouls;

    public float cooldownTime = 4;
    [HideInInspector] float currentCooldownTime;

    bool onCooldown;
    public GameObject cooldownPanel;
    public Text cooldownDisplayText;
    [HideInInspector] public Toggle thisToggle;


    void Start()
    {
        thisToggle = GetComponent<Toggle>();
        thisToggle.onValueChanged.AddListener(ToggleSelectedDisplay);

        if (unitType == SUnitType.SUnit.Ballista)
            thisToggle.onValueChanged.AddListener(SummonBallista);
        else
            thisToggle.onValueChanged.AddListener(ToggleSelectedDisplay);

        DeploySupportUnits.OnSUnitSummoned += GlobalCooldown;
        Player.SoulChargeAltered += UsuableCheck;
        Player.SUnitAltered += UsuableCheck;
        GlobalCooldown();
    }
    void OnDestroy()
    {
        DeploySupportUnits.OnSUnitSummoned -= GlobalCooldown;
        Player.SoulChargeAltered -= UsuableCheck;
        Player.SUnitAltered -= UsuableCheck;
    }


    public void SetValues(int lv, int souls)
    {
        sUnitLevel = lv;
        requiredSouls = souls;
    }
    void UsuableCheck()
    {
        if (Player.SUnitAnount(unitType) >= 1)
            thisToggle.interactable = Player.soulCharges > requiredSouls;
        else
            thisToggle.interactable = false;
    }


    void SummonBallista(bool ballistaSummon)
    {
        if (ballistaSummon)
        {
            int chance = Random.Range(0, 100);
            if (sUnitLevel == 1)
            {
                if (chance <= 33)
                {
                    Enemy.Instance.Damage(1);
                    UIManager.Instance.HideEnemyFlag();
                }
            }
            else if (sUnitLevel == 2)
            {
                if (chance <= 66)
                {
                    Enemy.Instance.Damage(1);
                    UIManager.Instance.HideEnemyFlag();
                }
            }
            else if (sUnitLevel == 3)
            {
                Enemy.Instance.Damage(1);
                UIManager.Instance.HideEnemyFlag();
            }
            AudioManager.Instance.SeigeAttack();
            FXManager.Instance.BallistaBoomz();
            Player.Instance.Decrease_Soul_Charges(requiredSouls);
            GameSparksManager.Instance.UpdateSUnitAmount(-1, SUnitType.SUnit.Ballista);
            DeploySupportUnits.UnitSummoned();
            thisToggle.onValueChanged.Invoke(false);
        }
        else
        {
            DeploySupportUnits.SetSUnitSummoned(null);
        }
    }


    public void GlobalCooldown()
    {
        currentCooldownTime = cooldownTime;
        thisToggle.interactable = false;
        cooldownDisplayText.text = "" + (int)currentCooldownTime;
        cooldownPanel.SetActive(true);
        onCooldown = true;
    }
    void Update()
    {
        if (onCooldown)
        {
            currentCooldownTime -= Time.deltaTime;
            cooldownDisplayText.text = "" + Mathf.CeilToInt(currentCooldownTime);
            if (currentCooldownTime <= 0)
            {
                currentCooldownTime = 0;
                onCooldown = false;
                cooldownPanel.SetActive(false);
                UsuableCheck();
            }
        }
    }


    public void ToggleSelectedDisplay(bool isSelected)
    {
        if (isSelected)
            gameObject.transform.localScale = new Vector3(1.2f, 1.2f, 1.2f);
        else
            gameObject.transform.localScale = new Vector3(1, 1, 1);
    }

}
