﻿using UnityEngine;
using UnityEngine.UI;

// Attached to the unit buttons
// Not to be confused with SpawnButton, which is attached to the lane buttons
public class UnitButton : MonoBehaviour
{
    // For identifying which unit to spawn
    public string unitCode;
    public bool spawned;

    // The actual unit cooldown that is used to reset back whenever the unit cooldown reaches 0
    public float baseCooldown;
    // Variable for calculation
    public float cooldownTime = 0;
    public Text cooldownDisplay;
    public Text heroLevel;

    //Control for displaying image on battle scene
    public bool turnedOn = false;
    public GameObject thePressedSprite;
    public GameObject ray;

    // The background color of the button, not to be mistaken with the unit type icon
    //	public GameObject buttonColor;

    // For radial cooldown
    public GameObject loadingBar;
    public GameObject glowIndicator;

    #region ButtonBehaviour	//What happens when you tap on a unit button

    // Episode 2 of GetCode(string), separated because this method gets called from a button
    // Gets the unit code of the pressed button and pass it over to GetCode(), allowing it to be stored inside SpawnManager
    // After that, calls a function to put the button on cooldown
    public void GetCodeContinued()
    {
        //Function performs same thing whether it is selected or not
        Toggling();
        DeSelectPreviousUnit("");

        GetCode(unitCode);

        ThisButtonWillBeDisabled();
    }

    public void GetHeroCode()
    {
        Toggling();
        DeSelectPreviousUnit("");

        GetHeroCodeContinued();

        ThisButtonWillBeDisabled();
    }

    //Removes stored info on the gameManager if player selects different unit
    public void DeSelectPreviousUnit(string index)
    {
        //If button to be disabled contains a Unit, Hero or Troop	
        if (GameManager.toBeDisabled != null)
        {
            // check if the button tapped is THIS
            if (GameManager.toBeDisabled != this)
            {
                // if not, disable it
                GameManager.toBeDisabled.Toggling();
                //				GameManager.toBeDisabled = null;
            }
            if (GameManager.storedUnitIndex != null)
            {
                GameManager.storedUnitIndex = null;
            }

            if (GameManager.storedHeroID != null)
            {
                GameManager.storedHeroID = null;
            }

            if (GameManager.storedSupportIndex != null)
            {
                GameManager.storedSupportIndex = null;
            }
        }
    }

    // This function is called when you press the unit buttons
    // Stores the index (which is passed from the function GetCodeContinued()) 
    //of the unit into the SpawnManager to queue the unit spawning
    void GetCode(string index)
    {
        //Basically if the button hasn't been chosen yet
        //tells gamemanager to store this button's unit index
        if (turnedOn == true)
        {
            GameManager.storedUnitIndex = index;
        }
        //Basically if the button is already selected
        //returns gamemanager's selected unit to null
        //so that units cannot spawn if spawn is pressed
        else if (!turnedOn)
        {
            GameManager.storedUnitIndex = null;
        }
    }

    void GetHeroCodeContinued()
    {
        if (turnedOn)
        {
            string code = unitCode;
            GameManager.storedHeroID = code;
        }
        else
        {
            GameManager.storedHeroID = null;
        }
    }

    public virtual void ThisButtonWillBeDisabled()
    {
        // Assign this instance to GameManager, in which it will be disabled after spawning unit later
        // Used by SpawnButton
        // Basically to check if the button is already selected or not pressed
        // If not selected, tells gameManager to hold this unitButton to be disabled
        if (turnedOn)
        {
            GameManager.toBeDisabled = this;
        }
        // If selected, tells gameManager to remove this unitButton
        else if (!turnedOn)
        {
            GameManager.toBeDisabled = null;
        }
    }

    public void Toggling()
    {
        if (!turnedOn)
        {
            gameObject.transform.localScale = new Vector3(1.25f, 1.25f, 1f);
            turnedOn = true;
            AudioManager.Instance.SelectionSFX();
        }
        else if (turnedOn)
        {
            gameObject.transform.localScale = Vector3.one;
            turnedOn = false;
        }
    }

    #endregion

    public void CardLevel(int troopLevel)
    {
        heroLevel = transform.Find("Level").GetComponent<Text>();
        heroLevel.text = "Level " + troopLevel;
    }

    // Called when the cooldown starts
    public void SpawnBoolToTrue()
    {
        spawned = true;
        cooldownTime = baseCooldown;
        gameObject.GetComponent<Button>().interactable = false;
        glowIndicator.SetActive(false);
    }

    public virtual void Cooldown()
    {
        // If this unit has been spawned
        if (spawned == true)
        {
            // Runs the countdown
            cooldownTime -= Time.deltaTime;
            loadingBar.GetComponent<Image>().fillAmount = cooldownTime / baseCooldown;

            if (cooldownTime < 0)
            {
                RefreshCooldown();
            }
        }
    }

    public void RefreshCooldown()
    {
        cooldownTime = 0;
        loadingBar.GetComponent<Image>().fillAmount = cooldownTime / baseCooldown;
        spawned = false;
        if (SpawnBlockerBarManager.onCooldown == false)
        {
            GetComponent<Button>().interactable = true;
        }
    }

    //Allow for GLOWING FX to show cards ready
    public void GlowEnable()
    {
        if (turnedOn)
        {
            if (gameObject.GetComponent<Button>().interactable == true)
            {
                if (SpawnBlockerBarManager.onCooldown == false)
                {
                    //this.GetComponent<Button> ().interactable = true;
                    glowIndicator.SetActive(true);
                }
                else if (SpawnBlockerBarManager.onCooldown == true)
                {
                    //this.GetComponent<Button> ().interactable = false;
                    glowIndicator.SetActive(false);
                }
            }
        }
        //To disable to glowing if units get spawned
        if (gameObject.GetComponent<Button>().interactable == false || turnedOn == false)
        {
            glowIndicator.SetActive(false);
        }
    }

    //Push card to the back of deck
    public void MoveButtonBack(GameObject button)
    {
        gameObject.SetActive(false);
        button.transform.SetAsFirstSibling();
        Player.Instance.DisplaySecondCard();
    }

    public virtual void Update()
    {
        Cooldown();
        //		GlowEnable();
    }

    #region heroButton
    public void InitialLockHeroButton()
    {
        cooldownTime = baseCooldown;
        spawned = true;
        HeroSpawned();
        RevokeSpawnning(baseCooldown);
    }

    public void RevokeSpawnning(float waitingTime)
    {
        Invoke("UnlockHeroButton", waitingTime);
    }
    public void HeroSpawned()
    {
        //happens only during start of battle
        //And if player spawns a hero unit
        Invoke("UnspawnHero", baseCooldown);
        Invoke("UnlockHeroButton", baseCooldown);
    }
    public void UnspawnHero()
    {
        spawned = false;
    }
    public void UnlockHeroButton()
    {
        if (SpawnBlockerBarManager.onCooldown == true || spawned == true)
        {
            return;
        }
        GetComponent<Button>().interactable = true;
    }
    #endregion

    /*
	public void Highlight()
	{
		if (GameManager.previousUnitButton == null) 
		{
//			GameManager.previousUnitButton = this.buttonColor;
		}
		if (GameManager.previousUnitButton != null)
		{
			GameManager.previousUnitButton.GetComponent<Image>().color = GameManager.defaultGray;
//			GameManager.previousUnitButton = this.buttonColor;
		}
//		this.buttonColor.GetComponent<Image>().color = Color.yellow;
	}
	*/
}
