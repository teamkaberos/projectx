﻿using UnityEngine;
using UnityEngine.UI;

// Attached to the lane buttons
public class SpawnButton : MonoBehaviour 
{
    public delegate void TroopDeployed();
    public static event TroopDeployed TroopDropped;

	// Basically to identify which lane the unit is spawning at
	public int locIndex;
	public bool onCooldown;
	public static float laneCooldown = 1f;
	float cooldownDuration = laneCooldown;

	public TroopManager tManager;
	public SpawnBlockerBarManager sBBManager;
	public UIManager uiManager;

	void Start()
	{
		//Storing Instances value once to optimise performance
		tManager = TroopManager.Instance;
		sBBManager = SpawnBlockerBarManager.Instance;
		uiManager = UIManager.Instance;
	}

	// ONE OF THE MAIN FUNCTIONS OF THE GAME
	// This is called when you press the lane buttons
	public void SpawnUnit()
	{
		if (GameManager.storedUnitIndex == null && GameManager.storedHeroID == null && GameManager.storedSupportIndex == null)
			return;

        // IF the PLAYER'S LANE IS BLOCKED BY AN OPPOSING BLOCKER
        if (GameManager.Instance.LaneIsBlocked(locIndex,true))
            return;
        // IF THE PLAYER"S LANE IS BLOCKED BY MINOTAUR
        if (GameManager.Instance.LaneIsMinosBlocked(locIndex, true))
            return;

		// FOR SPAWNNING REGULAR K,W,A
		if (GameManager.storedUnitIndex != null && GameManager.storedHeroID == null)
		{
			tManager.CheckSpawnTroopAndLoc(locIndex);					//sending the lane to be spawned
			tManager.New_SpawnUnitsOnLane();													//the actual spawning
			// =========== Disabling and moving card back =============//
			GameManager.toBeDisabled.gameObject.GetComponent<UnitButton>().Toggling ();
            GameManager.toBeDisabled.gameObject.GetComponent<UnitButton>().MoveButtonBack (GameManager.toBeDisabled.gameObject);
            // =========== To apply cooldown on cards ============//
            sBBManager.AddUnitCount();
            sBBManager.SpawnChecker();
            sBBManager.BlockCards();
            sBBManager.CheckDecks();
            sBBManager.Hide_Blocker_Bars();
			// =========== Thinning deck and disabling hero ============//
			uiManager.DeckThinning();
			uiManager.GlobalCooldown_HeroButton_Disasble();
			// =========== Disable THIS spawn button and clearing stored index ========//
			GameManager.storedUnitIndex = null;						// Set the stored info to null to prevent respawning unit before pressing the unit button again
            if (Player.tutorial < Player.tutorialCap)
                TroopDropped();
            DisableButton();
		}

		// FOR SPAWNNING HEROES
		else if (GameManager.storedUnitIndex == null && GameManager.storedHeroID != null)
        {
			// =========== Getting info for the unit to be spawned =========== //
            tManager.CheckSpawnHeroAndLoc(locIndex);
            tManager.New_SpawnHeroOnLane();
			// =========== Disabling card =============//
			GameManager.toBeDisabled.gameObject.GetComponent<UnitButton> ().Toggling ();
            GameManager.toBeDisabled.gameObject.GetComponent<UnitButton>().MoveButtonBack (GameManager.toBeDisabled.gameObject);
            // =========== To apply cooldown on cards ============//
            sBBManager.AddUnitCount();
            sBBManager.SpawnChecker();
            sBBManager.BlockCards();
            sBBManager.CheckDecks();
            sBBManager.Hide_Blocker_Bars();
			// =========== Thinning deck and disabling hero ============//
			uiManager.DeckThinning();
			uiManager.GlobalCooldown_HeroButton_Disasble();
			// ========== Locking all hero cards ============= //
//			uiManager.LockAllHeroCards();
//			uiManager.GlobalCooldown_HeroButton_Disasble();		//For Locking hero buttons
			// =========== Disable THIS spawn button and clearing stored index ========//
			GameManager.storedHeroID = null;     	// Set the stored info to null to prevent respawning unit before pressing the unit button again
		}
		GameManager.toBeDisabled = null;			//Remove any stored button
		DisableButton();
	}
		
	/* NOT USING ANYMORE, CONVERTED TO OBJECT POOLING
	// Basically handles all the extra steps of instantiating a unit
	// Setparenting it, setting the scale to 1, etc.
//	void JustTransformStuffs(Transform spawnie)
//	{
//		spawnie.SetParent (Player.Instance.unitLocs [Player.Instance.getLocIndex]);
////		spawnie.transform.localScale = new Vector3 (spawnie.transform.localScale.x, spawnie.transform.localScale.y, spawnie.transform.localScale.z);
//		if (spawnie.gameObject.GetComponent<Unit> ().unitClass == "Wizard")
//		{
//			spawnie.transform.localScale = new Vector3 (1f, 1f, 1f);
//		}
//		if (spawnie.gameObject.GetComponent<Unit> ().unitClass == "Archer")
//		{
//			spawnie.transform.localScale = new Vector3 (1.4f, 1.4f, 1.4f);
//		}
//		if (spawnie.gameObject.GetComponent<Unit> ().unitClass == "Knight")
//		{
//			spawnie.transform.localScale = new Vector3 (1.2f, 1.2f, 1.2f);
//		}
//		spawnie.gameObject.GetComponent<Unit> ().isFriendlyUnit = true;
//		// Because we need rigidbody for OnTriggerFunction, but at the same time we don't need physics for our game
//		spawnie.gameObject.AddComponent<Rigidbody> ().useGravity = false;
//	}
*/

	void Update()
	{
		Cooldown ();
	}

	public void DisableButton()
	{
		this.gameObject.GetComponent<Button> ().interactable = false;
		onCooldown = true;
	}

	public void Cooldown()
	{
		if (onCooldown == true) 
		{
			cooldownDuration -= Time.deltaTime;
			// More specifically, if the cooldown is 0.9, sets it to 0
			if (cooldownDuration <= 0f)
			{
				cooldownDuration = 0;
				onCooldown = false;
				gameObject.GetComponent<Button> ().interactable = true;
				cooldownDuration = laneCooldown;
			}
		}
	}

	//	void CheckIfCountered(int getIndex, int laneIndex)
	//	{
	//		string counterIndex = CheckCounter (getIndex).ToString ();
	//		int numberOfChild = GameManager.Instance.laneHints [laneIndex].transform.childCount;
	//		if (GameManager.Instance.laneHints [laneIndex] != null) 
	//		{
	//			Transform toBeChecked = GameManager.Instance.laneHints [laneIndex].transform.GetChild (numberOfChild - 1);
	//			if (toBeChecked.gameObject.tag == counterIndex || toBeChecked.gameObject.tag == getIndex.ToString ()) 
	//			{
	//				Destroy (toBeChecked.gameObject);
	//			}
	//		}
	//	}
	//
	//	int CheckCounter(int getIndex)
	//	{
	//		// just a random number, to prevent unassigned local variable error
	//		int counterIndex = 5;
	//		if (getIndex == 0) 
	//		{
	//			counterIndex = 1;
	//		}
	//		if (getIndex == 1) 
	//		{
	//			counterIndex = 2;
	//		}
	//		if (getIndex == 2) 
	//		{
	//			counterIndex = 0;
	//		}
	//		return counterIndex;
	//	}
		
}
