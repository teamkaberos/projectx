﻿using UnityEngine;

public class DeploySupportUnits : MonoBehaviour
{
    public delegate void OnEventTriggered();
    public static event OnEventTriggered OnSUnitSummoned;

    public static bool supportUnitSummoned;
    public SupportUnitButton[] deployMines;
    public SupportUnitButton[] deployDrags;
    public SupportUnitButton[] deployBallista;

    public static SupportUnitButton sUnitSelected;

    private void Start()
    {
        supportUnitSummoned = false;

        SetRequiredSouls();
    }


    public void SetRequiredSouls()
    {
        for (int x = 0; x < deployMines.Length; x++)
            deployMines[x].SetValues(x + 1, Player.minesRequire[x]);
        for (int x = 0; x < deployDrags.Length; x++)
            deployDrags[x].SetValues(x + 1, Player.dragsRequire[x]);
        for (int x = 0; x < deployBallista.Length; x++)
            deployBallista[x].SetValues(x + 1, Player.seigeRequire[x]);
    }



    public static void SetSUnitSummoned(SupportUnitButton selected_)
    {
        sUnitSelected = selected_;
    }
    public static void UnitSummoned()
    {
        OnSUnitSummoned();
    }



}
