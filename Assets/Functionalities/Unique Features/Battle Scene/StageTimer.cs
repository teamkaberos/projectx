﻿using UnityEngine;
using UnityEngine.UI;

public class StageTimer : MonoBehaviour
{
    public delegate void TimeUp();
    public static event TimeUp roundOver;

    [HideInInspector] public float timer = 151;
    public float frenzyTime;
	public Text timerText;

    bool isCounting = true;

	public static bool frenzyMode;

    public Animator fxControl;

	void Start()
	{
        isCounting = true;
        frenzyMode = false;
        Time.timeScale = 1f;
        Enemy.onCastleDestroy += StopTimer;
        Player.onCastleDestroy += StopTimer;
	}

	void FrenzyMode()
	{
        FrenzyAnimation();
        PlayFrenzyNotify();
        timerText.color = Color.red;
		Time.timeScale = 1.5f;
	}

    void DisplayTime()
    {
        timerText.text = "" + (int)timer;
    }

    public void StopTimer()
    {
        isCounting = false;
    }

	void Update () 
	{
        if (timer <= frenzyTime && frenzyMode != true)
		{
			frenzyMode = true;
			FrenzyMode();
        }

        if (isCounting)
        {
            if (Time.timeScale > 0)
                timer -= Time.deltaTime / Time.timeScale;
        }
        DisplayTime();

		if (timer <= 0) 
		{
			timer = 0;
            StopTimer();
            roundOver();
            Enemy.Instance.CancelSpawning();
            AudioManager.Instance.DefeatJingle();
            GameManager.Instance.BattleOutcome(false);
			GameManager.Instance.gameOverScreen.SetActive (true);
		}
	}

    void OnDestroy()
    {
        Enemy.onCastleDestroy -= StopTimer;
        Player.onCastleDestroy -= StopTimer;
    }



    // FOR FRENZY FX
    public FXPlay frenzyFX;
    public GameObject frenzyNotify;


    void FrenzyAnimation()
    {
        fxControl.SetBool("Frenzy", true);
    }
    void PlayFrenzyNotify()
    {
        frenzyFX.PlayEffect();
    }
}
