﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShuffleBlocker : MonoBehaviour 
{
	public Image cooldownDial;

	float playerShuffleTime;
	public float shufflingTimer;

	public Text shuffleDisplay;

	// Use this for initialization
	void OnEnable () 
	{
		playerShuffleTime = Player.Instance.CurrentShufflingTimer();		//Total shuffle time
		shufflingTimer  =playerShuffleTime;				//Current time left
	}
	
	// Update is called once per frame
	void Update () 
	{
		shufflingTimer -= Time.deltaTime;
		cooldownDial.fillAmount = shufflingTimer/playerShuffleTime;

		if (shufflingTimer <= 0)
		{
			shufflingTimer = playerShuffleTime;
			this.gameObject.SetActive(false);
		}
	}
}
