﻿using UnityEngine;
using UnityEngine.UI;

public class Hero_Info_Assign : MonoBehaviour
{
    #region singleton
    private static Hero_Info_Assign _instance;
	public static Hero_Info_Assign Instance
	{
		get
		{
			return _instance;
		}
	}
    #endregion

    int heroNoToDisplay;

	public Hero_Evolution_Info[] hero_Evolution_Infos;
	[Space(10)]
	public Text heroName;
	public Text heroClass;
	public Text cardsRequired;
	public Text stonesRequired;
	public Text heroDescription;
    public Text currentlyOwnedExcalipoints;

	public Image heroIcon;
	public Image heroStoneNeeded;

	public GameObject heroModel;
    
	public Button excalipoint_Exchange_Button;
    public Button excalipoint_Exchange10_Button;

	public HeroModels heroModelController;

    public Transform[] heroCards;


    public Image[] baseShape;

    public Image[] skillIcon;

    public GameObject[] levelUpObj;
    public GameObject heroStatusIcon;   // the star obj that shows if its a hero

    public Text[] heroSkillInfo;
    string[] lockedText = {"-","Unlocks at LVL 2",
        "Unlocks at LVL 3",
        "Unlocks at LVL 4",
        "Unlocks at LVL 5"};
    public Text[] exchangeWithExcalipoint;

    public Transform toMoveHeroModels;
    public Transform locatorForHeroModels;

    public GameObject chatCanvas;
    public GameObject InfoBar;

    public CheckTime lightTimerCheck;



	void Awake()
	{ 
		_instance = this;
        heroModelController.LoadModels();

    }

    void OnEnable()
    {
        //toMoveHeroModels.transform.position = locatorForHeroModels.transform.position;
        UpdateExcalipointsAmount();
        lightTimerCheck.ToggleOffAllLights();
        chatCanvas.SetActive(false);
        InfoBar.SetActive(false);
    }

    public void UpdateExcalipointsAmount()
    {
        currentlyOwnedExcalipoints.text = string.Format("You have : {0:n0}", Player.Instance.excalipoint_Value());
    }


	public void Recieve_Hero_Number(int hero_No_I_Need_To_Display, bool isOn, int herosLevel)
	{
		heroNoToDisplay = hero_No_I_Need_To_Display;
        Display_ThisHero_Info(isOn, herosLevel);
	}

    void Display_ThisHero_Info(bool isOn, int herosLevel)
	{
        //find the hero info that is supposed to be displayed
        for (int x = 0; x < hero_Evolution_Infos.Length; x++)
        {
            if (hero_Evolution_Infos[x].heroNo == heroNoToDisplay)
            {
                heroName.text = hero_Evolution_Infos[x].heroName;
                heroClass.text = hero_Evolution_Infos[x].heroClass;
                heroDescription.text = hero_Evolution_Infos[x].heroDescription;


                // *** Shape updating *** \\
                for (int y = 0; y < baseShape.Length; y++)
                {
                    baseShape[y].sprite = hero_Evolution_Infos[x].baseShape;
                    skillIcon[y].sprite = hero_Evolution_Infos[x].skillIcon;
                }
                // *** Skill Text updating *** \\
                for (int y = 0; y < lockedText.Length; y++)
                {
                    if (y < herosLevel)
                        heroSkillInfo[y].text = hero_Evolution_Infos[x].skillText[y];
                    else
                        heroSkillInfo[y].text = lockedText[y];
                }

                if (isOn)
                    heroModelController.DisplayHero(hero_Evolution_Infos[x].heroClass, herosLevel);
                else
                    heroModelController.HideModels();

                heroIcon.sprite = hero_Evolution_Infos[x].heroIcon;
                heroStoneNeeded.sprite = hero_Evolution_Infos[x].stoneRequired;

                if (heroName.text.Contains("Goblin"))
                {
                    // display only 3 levels

                    LevelUpObjToggle(true);
                    baseShape[0].sprite = hero_Evolution_Infos[x].skillIcon;
                    skillIcon[1].sprite = hero_Evolution_Infos[x].skillIcon;
                }
                else
                {
                    // if selected hero isnt the goblin
                    LevelUpObjToggle(false);
                    skillIcon[1].sprite = hero_Evolution_Infos[x].baseShape;
				}
				break;
			}
		}
	}
    void LevelUpObjToggle(bool isGoblin)
    {
        heroStoneNeeded.gameObject.SetActive(!isGoblin);

        levelUpObj[3].SetActive(!isGoblin);
        levelUpObj[4].SetActive(!isGoblin);
        heroStatusIcon.SetActive(isGoblin);
        skillIcon[0].gameObject.SetActive(isGoblin);
    }



    public void Display_Card_Cost_Values(int cardCostValue)
    {
        exchangeWithExcalipoint[0].text = string.Format("{0} points\n<color=#FFFF00>1 card</color>", cardCostValue);

        exchangeWithExcalipoint[1].text = string.Format("{0}0 points\n<color=#FFFF00>10 card</color>", cardCostValue);
    }

	public void Check_ExcaliPoints_Button(int cardCostValue)
	{
        if (Player.excalipoints < cardCostValue)
            excalipoint_Exchange_Button.interactable = false;
		else
			excalipoint_Exchange_Button.interactable = true;


        if (Player.excalipoints < cardCostValue*10)
            excalipoint_Exchange10_Button.interactable = false;
        else
            excalipoint_Exchange10_Button.interactable = true;
        
	}

    public void Check_If_Hero_Has_EnoughCards(bool hasEnoughCards)
    {
        excalipoint_Exchange_Button.gameObject.SetActive(hasEnoughCards);
    }

    public void untoggleHeroCards()
    {
        for (int x = 0; x < heroCards.Length; x++)
        {
            for (int y = 0; y < heroCards[x].childCount; y++)
            {
                if (heroCards[x].GetChild(y).GetComponent<Toggle>().isOn)
                    heroCards[x].GetChild(y).GetComponent<Toggle>().isOn = false;
            }
        }
    }

	void OnDisable()
    {
        if (lightTimerCheck != null)
            lightTimerCheck.CheckTimeOfTheDay();

        chatCanvas.SetActive(true);
        InfoBar.SetActive(true);
        if (heroModelController != null)
		{
			heroModelController.HideModels();
        }
    }
}