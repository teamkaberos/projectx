﻿using UnityEngine;

[CreateAssetMenu(fileName = "newHeroInfo", menuName = "HeroInformation")]
public class Hero_Evolution_Info : ScriptableObject
{
	public string heroName;
	public string heroClass;

	public int heroNo;

	[TextArea(3,10)]
	public string heroDescription;

	public Sprite heroIcon;
	public Sprite stoneRequired;

	public GameObject heroModel;

    public Sprite baseShape;
    public Sprite skillIcon;

    [TextArea(2,10)]
    public string[] skillText;
}
