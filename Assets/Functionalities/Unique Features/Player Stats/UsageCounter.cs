﻿using UnityEngine;
using UnityEngine.UI;

public class UsageCounter : MonoBehaviour
{
    public Text counterDisplay;

    [HideInInspector]
    public int usageValue;      //amounts of use left

    public GameObject[] abilityUsageDisplay;


    void Start()
    {
        counterDisplay.gameObject.SetActive(false);
    }

    public void DisplayCounter(int usages)
    {
        usageValue = usages;
        //counterDisplay.text = usages + "";
        DisplayOBJ();
    }
    void DisplayOBJ()
    {
        for (int x = 0; x < abilityUsageDisplay.Length; x++)
        {
            abilityUsageDisplay[x].SetActive(false);
        }
        if (usageValue >= 1)
            abilityUsageDisplay[usageValue - 1].SetActive(true);
    }



    public void HideCounter(int toHide)
    {
        usageValue -= toHide;
        DisplayCounter(usageValue);
    }
}
