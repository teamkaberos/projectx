﻿using UnityEngine;
using UnityEngine.UI;

public class TroopAssign_Info : MonoBehaviour
{
    static TroopAssign_Info _instance;
    public static TroopAssign_Info Instance
    {
        get
        {
            return _instance;
        }
    }

    public GameObject[] DropSlots;


    public string[] infoAndLevels;

    int usedAssignmentPoints;

    int troopLimit = 2;
    int troopLimitCounter;

    public Button[] sendTroopAssignmentButton;



    void Awake()
    {
        _instance = this;
    }

    void Start()
    {
        for (int x = 0; x < sendTroopAssignmentButton.Length; x++)
        {
            sendTroopAssignmentButton[x].onClick.AddListener(SendTroops);
        }
    }


    public void Reassign(int heroID)
    {
        for (int x = 0; x < DropSlots.Length; x++)
        {
            if (DropSlots[x].GetComponent<AssignSlot>().item != null)
            {
                // if the player has a hero that is assigned here
                if (DropSlots[x].GetComponent<AssignSlot>().assignedTroopNo == heroID)
                {
                    DropSlots[x].GetComponent<AssignSlot>().item.GetComponent<DragAndDrop>().Assign();
                }
            }
        }
    }

    public void New_AssignTroop()   //After merging DB
    {
        if (Player.tutorial >= 4)
        {
            for (int x = 0; x < DropSlots.Length; x++)
            {
                DropSlots[x].GetComponent<AssignSlot>().assignedTroopNo = Player.assignedTroops[x];
                for (int y = 0; y < UnitManager.Instance.heroInfomation.Count; y++)
                {
                    if (DropSlots[x].GetComponent<AssignSlot>().assignedTroopNo == UnitManager.Instance.heroInfomation[y].heroNo)
                    {
                        if (DropSlots[x].GetComponent<AssignSlot>().assignedTroopNo < 100)
                        {
                            DropSlots[x].GetComponent<AssignSlot>().assignedTroopLevel = UnitManager.Instance.heroInfomation[y].heroLvl;
                        }
                    }
                }
                DropSlots[x].GetComponent<AssignSlot>().Create_Troop_Cards();
            }
        }
        else
        {
            Player.assignedTroops[0] = 100;
            Player.assignedTroops[1] = 101;
            Player.assignedTroops[2] = 102;

            DropSlots[0].GetComponent<AssignSlot>().assignedTroopNo = 100;
            DropSlots[1].GetComponent<AssignSlot>().assignedTroopNo = 101;
            DropSlots[2].GetComponent<AssignSlot>().assignedTroopNo = 102;

            for (int x = 3; x < DropSlots.Length; x++)
            {
                Player.assignedTroops[x] = 250;
                DropSlots[x].GetComponent<AssignSlot>().assignedTroopNo = 250;
            }
            for (int x = 0; x < DropSlots.Length; x++)
            {
                DropSlots[x].GetComponent<AssignSlot>().assignedTroopLevel =0;
                DropSlots[x].GetComponent<AssignSlot>().Create_Troop_Cards();
            }
        }

        Assign_Now();
        CheckLimit();

        //Invoke("Assign_Now", 0.04f);    // to perform Assignment point calculation
        //Invoke("CheckLimit", 0.04f);    // to perform Assignment point check
    }

    public void SaveTroop()
    {
        SendTroops();
    }

    public bool CheckHowManyOfThisUnitThereAre(int heroNoToCheck)
    {
        for (int x = 0; x < DropSlots.Length; x++)
        {
            if (DropSlots[x].GetComponent<AssignSlot>().assignedTroopNo < 100 && DropSlots[x].GetComponent<AssignSlot>().assignedTroopNo == heroNoToCheck)
            {
                troopLimitCounter += 1;
                if (troopLimitCounter >= troopLimit)
                {
                    break;
                }
            }
        }
        if (troopLimitCounter >= troopLimit)
        {
            troopLimitCounter = 0;
            return true;
        }
        troopLimitCounter = 0;
        return false;
    }

    public void UpdateAssignedTroopValues()
    {
        for (int x = 0; x < DropSlots.Length; x++)
        {
            if (DropSlots[x].GetComponent<AssignSlot>().item != null)
            {
                DropSlots[x].GetComponent<AssignSlot>().item.GetComponent<DragAndDrop>().UpdateDisplayInfo();
            }
        }
    }

    public int UsedAssignmentPoints()
    {
        usedAssignmentPoints = 0;

        for (int x = 0; x < DropSlots.Length; x++)
        {
            if (DropSlots[x].GetComponent<AssignSlot>().item != null)
            {
                usedAssignmentPoints += DropSlots[x].GetComponent<AssignSlot>().item.GetComponent<DragAndDrop>().assignmentCost;
            }
        }

        return usedAssignmentPoints;
    }

    public void Assign_Points_Used()
    {
        Invoke("Assign_Now", 0.01f);
    }

    public void Assign_Now()
    {
        int totalPointsUsed = 0;

        for (int x = 0; x < DropSlots.Length; x++)
        {
            if (DropSlots[x].transform.childCount > 1)  //As there is a glow gameobject
            {
                if (DropSlots[x].GetComponent<AssignSlot>().assignedTroopNo != 250)
                {
                    totalPointsUsed += DropSlots[x].transform.GetChild(1).GetComponent<DragAndDrop>().assignmentCost;

                }
            }
        }
        Player.assigned_Assign_Points = totalPointsUsed;
        Player.available_Assign_Points = Player.assign_Points_Limit - Player.assigned_Assign_Points;

        UIManager.Instance.UpdateAvailableAssignPoints();

        if (!GetComponent<DragAndDrop_Transform_Organizer>().draggingParent.gameObject.activeInHierarchy)
            Player.Instance.CheckAssignmentPoints();
        else
            Player.Instance.CheckAssignmentPointResolve();
    }

    void CheckLimit()
    {
        Player.Instance.CheckAssignedUnits();
    }
    //Will check if the player has exceeded the limit count
    public bool Check_For_Troop_Limit()
    {
        //First assigned unit loop
        for (int x = 0; x < DropSlots.Length; x++)
        {
            if (DropSlots[x].GetComponent<AssignSlot>().assignedTroopNo < 100)  //Make sure it is a hero
            {
                int assigned = 0;
                //Compare it with the others that are assigned
                for (int y = 0; y < DropSlots.Length; y++)
                {
                    if (y != x) //Make sure not to check the same slot
                    {
                        if (DropSlots[y].GetComponent<AssignSlot>().assignedTroopNo < 100) //make sure this slot is also a hero
                        {
                            if (DropSlots[x].GetComponent<AssignSlot>().assignedTroopNo == DropSlots[y].GetComponent<AssignSlot>().assignedTroopNo)
                            {
                                assigned += 1;
                                if (assigned == 2)
                                {
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    void SendTroops()
    {
        for (int i = 0; i < 3; i++)
        {
            DataManager.assignedUnitsSlot[0, i] = DropSlots[i].GetComponent<AssignSlot>().assignedTroopNo;
            DataManager.assignedUnitsLevel[0, i] = DropSlots[i].GetComponent<AssignSlot>().assignedTroopLevel;
        }
        for (int i = 0; i < 3; i++)
        {
            DataManager.assignedUnitsSlot[1, i] = DropSlots[i + 3].GetComponent<AssignSlot>().assignedTroopNo;
            DataManager.assignedUnitsLevel[1, i] = DropSlots[i + 3].GetComponent<AssignSlot>().assignedTroopLevel;
        }
        for (int i = 0; i < 3; i++)
        {
            DataManager.assignedUnitsSlot[2, i] = DropSlots[i + 6].GetComponent<AssignSlot>().assignedTroopNo;
            DataManager.assignedUnitsLevel[2, i] = DropSlots[i + 6].GetComponent<AssignSlot>().assignedTroopLevel;
        }
        GameSparksManager.Instance.SaveAllAssignedUnits(GameSparksManager.playerId);
    }


    public void ResetTroopAssigned()
    {
        for (int i = 0; i < 3; i++)
        {
            DataManager.assignedUnitsSlot[0, i] = 100+i;
            DataManager.assignedUnitsLevel[0, i] = -100;
        }
        for (int i = 0; i < 3; i++)
        {
            DataManager.assignedUnitsSlot[1, i] = 0;
            DataManager.assignedUnitsLevel[1, i] = 0;
        }
        for (int i = 0; i < 3; i++)
        {
            DataManager.assignedUnitsSlot[2, i] = 0;
            DataManager.assignedUnitsLevel[2, i] = 0;
        }
    }


	#region William's Badlands

	//    void Retrieve_Assign_Troop_Info()
	//    {
	//        StartCoroutine("_retrieve_Assign_Troop_Info");
	//    }
	//
	//    IEnumerator _retrieve_Assign_Troop_Info()
	//    {
	//        WWWForm form = new WWWForm();
	//        form.AddField("playerID", Player.playerUniqueID);
	//
	//        // This function is the one where it executes the data sending
	//        WWW www = new WWW(troopAssignInfoURL, form);
	//        yield return www;
	//
	//        Debug.Log(www.text);
	//        infoAndLevels = www.text.Split('|');
	//
	//        //		AssignTroops();
	//    }

	//	void AssignTroops()
	//	{
	//		for (int x = 0; x < DropSlots.Length ; x++)
	//		{
	//			Player.assignedTroops[x] = int.Parse(infoAndLevels[x]);
	//			DropSlots[x].GetComponent<AssignSlot>().assignedTroopNo = int.Parse(infoAndLevels[x]);
	//			//and obtain the level from the player's own info
	//		}
	//	}

	#endregion
}
