﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StonePurchase : MonoBehaviour
{
    [Header("Ones")]
    public Button addOnesB;
    public Button deductOnesB;
    public Text onesT;
    int ones = 1;

    [Space(20)]
    [Header("Tens")]
    public Button addTensB;
    public Button deductTensB;
    public Text tensT;
    int tens = 0;

    [Space(20)]
    [Header("Hundreds")]
    public Button addHundredsB;
    public Button deductHundredsB;
    public Text hundredsT;
    int hundreds = 0;

    [Space(20)]
	public int stoneCostD;

    [Space(20)]
    public Button purchaseStones;

    [Space(20)]
    public static string stoneToPurchase;

    [Space(20)]
    public GameObject stoneBoughtDisplay;
    public Text stoneBought;
    public Text stoneCostDisplayText;
    public Text currentAvailableDiamondsText;

    [Space(20)]
    string toBeReplacedText;    // the text that will be initialized
    public Text displayConfirmation;


    /*
	*/

    void Start()
    {
        toBeReplacedText = displayConfirmation.text;
    }

    void OnEnable()
    {
        DisplayCost();
        DisplayDiamonds();
    }

	void OnDisable()
	{
        ResetValues();
	}

	void ResetValues()
    {
        ones = 1;
        tens = 0;
        hundreds = 0;

        DisplayOnes();
        DisplayTens();
        DisplayHundreds();

        if (addOnesB != null & addTensB != null & addHundredsB != null)
        {
            addOnesB.interactable = true;
            addTensB.interactable = true;
            addHundredsB.interactable = true;
        }

        if (deductOnesB != null & deductTensB != null & deductHundredsB != null)
        {
            deductOnesB.interactable = true;
            deductTensB.interactable = false;
            deductHundredsB.interactable = false;
        }

        CheckIfAffordable();
    }

    public void StoneToBePurchased(string stoneBeingSelected)
    {
        stoneToPurchase = stoneBeingSelected;
    }


    // ==== ADDING STONE AMOUNTS UI UPDATE ==== \\
    public void AddOnes()
    {
        deductOnesB.interactable = true;

        if (ones < 9)
        {
            ones += 1;
            if (ones == 9)
                addOnesB.interactable = false;
            else
                addOnesB.interactable = true;
        }

        DisplayCost();
        DisplayOnes();
    }
    public void DeductOnes()
    {
        addOnesB.interactable = true;

        if (ones > 0)
        {
            ones -= 1;
            if (ones == 0)
                deductOnesB.interactable = false;
            else
                deductOnesB.interactable = true;
        }

        DisplayOnes();
    }
    void DisplayOnes()
    {
        if (onesT != null)
        {
            onesT.text = ones.ToString();
            CheckIfAffordable();
        }
    }

    public void AddTens()
    {
        deductTensB.interactable = true;

        if (tens < 9)
        {
            tens += 1;
            if (tens == 9)
                addTensB.interactable = false;
            else
                addTensB.interactable = true;
        }

        DisplayCost();
        DisplayTens();
    }
    public void DeductTens()
    {
        addTensB.interactable = true;

        if (tens > 0)
        {
            tens -= 1;
            if (tens == 0)
                deductTensB.interactable = false;
            else
                deductTensB.interactable = true;
        }

        DisplayTens();
    }
    void DisplayTens()
    {
        if (tensT != null)
        {
            tensT.text = tens.ToString();
            CheckIfAffordable();
        }
    }

    public void AddHundreds()
    {
        deductHundredsB.interactable = true;

        if (hundreds < 9)
        {
            hundreds += 1;
            if (hundreds == 9)
                addHundredsB.interactable = false;
            else
                addHundredsB.interactable = true;
        }

        DisplayCost();
        DisplayHundreds();
    }
    public void DeductHundreds()
    {
        addHundredsB.interactable = true;

        if (hundreds > 0)
        {
            hundreds -= 1;
            if (hundreds == 0)
                deductHundredsB.interactable = false;
            else
                deductHundredsB.interactable = true;
        }

        DisplayHundreds();
    }
    void DisplayHundreds()
    {
        if (hundredsT != null)
        {
            hundredsT.text = hundreds.ToString();
            CheckIfAffordable();
        }
    }
    // ==== ADDING STONE AMOUNTS UI UPDATE END ==== \\


    int PurchaseAmount()
    {
        int amount = (hundreds * 100) + (tens * 10) + ones;
        return amount;
    }
	int DiamondCost()
    {
        int costPrice = PurchaseAmount() * stoneCostD;
        return costPrice;
    }
    void DisplayCost()
    {
        stoneCostDisplayText.text = string.Format("Cost : {0:n0} Diamonds ", DiamondCost());
    }
    void DisplayDiamonds()
    {
        currentAvailableDiamondsText.text = string.Format("Currently Available Diamonds : {0:n0} ", Player.diamond);
    }

    public void DisplayPurchaseConfirmation()
    {
        displayConfirmation.text = toBeReplacedText;

        displayConfirmation.text = displayConfirmation.text.Replace("purchase_product", PurchaseAmount() + PurchasingStones());
        displayConfirmation.text = displayConfirmation.text.Replace("product_cost", DiamondCost() + " DIAMONDS");
    }
    string PurchasingStones()
    {
        if (stoneToPurchase == "square")
        {
            return " EARTH STONES";
        }
        if (stoneToPurchase == "triangle")
        {
            return " WIND STONES";
        }
        if (stoneToPurchase == "circle")
        {
            return " FLAME STONES";
        }
        return "Null";
    }

    void CheckIfAffordable()
    {
        if (DiamondCost() == 0)
        {
            purchaseStones.interactable = false;
            return;
        }

        if (DiamondCost() >= Player.diamond)
            purchaseStones.interactable = false;
        else
            purchaseStones.interactable = true;
    }

    public void BuyStones()
    {
        if (stoneToPurchase == "square")
        {
            Player.squareStone += PurchaseAmount();
            stoneBought.text = PurchaseAmount() + " EARTH stones obtained";
        }
        else if (stoneToPurchase == "triangle")
        {
            Player.triangleStone += PurchaseAmount();
            stoneBought.text = PurchaseAmount() + " WIND stones obtained";
        }
        else if (stoneToPurchase == "circle")
        {
            Player.circleStone += PurchaseAmount();
            stoneBought.text = PurchaseAmount() + " FLAME stones obtained";
        }

        stoneBoughtDisplay.SetActive(true);

        Player.Instance.SpendDiamond(DiamondCost());
        DataManager.Instance.PrepareDataForSparksPushing();
        GameSparksManager.Instance.SavePlayerExpendables(GameSparksManager.playerId);
        GameSparksManager.Instance.SaveInventoryStones();

        ResetValues();
        DisplayCost();
        DisplayDiamonds();

        UIManager.Instance.UpdateTavernInfos();
    }
}
