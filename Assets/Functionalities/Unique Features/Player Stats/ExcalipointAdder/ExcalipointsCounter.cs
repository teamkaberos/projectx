﻿using UnityEngine;
using UnityEngine.UI;

public class ExcalipointsCounter : MonoBehaviour
{
	public Text pointsDisplay;		//the display of how many points are attained
	int points = 0;

	public Text[] pointsObtainedDisplay;

	public GameObject[] addPoint;    //the +1 that will appear

	void Start()
	{
		//listen to every kill
		Unit.killTriggered += AddPoint;
		Unit.enemyKilled += DisplayPointAdd;

		//listen to point adding
		for (int x = 0; x < addPoint.Length; x++)
		{
			addPoint[x].GetComponent<AddPoint>().pointReached += UpdatePoints;
		}

		//listen to outcome of battle
		Player.playerDead += AddToTotal;
		Enemy.enemyDead += AddToTotal;
	}


    public void DisplayPointAdd()
    {
        for (int x = 0; x < addPoint.Length; x++)
        {
            if (!addPoint[x].activeInHierarchy)
            {
                addPoint[x].SetActive(true);
                break;
            }
        }
    }

	void AddPoint()
	{
        points+=1;
	}
   	void UpdateText()
	{
		pointsDisplay.text = points + "";
	}

    void UpdateBattleOutcomeDisplay()
	{
		pointsObtainedDisplay[0].text = string.Format("+{0} EXCALIPOINTS" , points);
		pointsObtainedDisplay[1].text = string.Format("+{0} EXCALIPOINTS" , points);
	}

	public void AddToTotal()
	{
        Player.Instance.ExcaliPoint_Calculator(true,points);
        DataManager.excalipoints += points;
        MissionHolder.Instance.UpdateMission("excalipoint", points);
	}

    void UpdatePoints()
	{
		UpdateText();
		UpdateBattleOutcomeDisplay();
	}

	void OnDestroy()
	{
		Unit.killTriggered -= AddPoint;
		Unit.enemyKilled -= DisplayPointAdd;

		for (int x = 0; x < addPoint.Length; x++)
        {
			addPoint[x].GetComponent<AddPoint>().pointReached -= UpdatePoints;
        }

		Player.playerDead -= AddToTotal;  
		Enemy.enemyDead -= AddToTotal;
	}
}
