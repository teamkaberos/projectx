﻿using UnityEngine;

public class AddPoint : MonoBehaviour
{
	public delegate void OnPointReached();
	public event OnPointReached pointReached;

    void TriggerPoint()
	{
		AddThePoint();
		Hide();
	}

	void AddThePoint()
	{
		pointReached();
	}

	void Hide()
	{
		gameObject.SetActive(false);
	}
}
