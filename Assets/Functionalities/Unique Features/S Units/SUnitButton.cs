﻿using UnityEngine;
using UnityEngine.UI;

public class SUnitButton : MonoBehaviour
{
    public SUnitType.SUnit thisSUnit;
    [HideInInspector] public int thisSUnitAmount;

    public Text currentAmountText;      // shows how many of the support units there are

    public Transform animRoot;
    Animator anim;
    public Button challengeButton;
    Button thisSUnitFarmButton;

    public int requiredLvToUnlock;

    void Start()
    {
        thisSUnitFarmButton = GetComponent<Button>();

        DataManager.DataDistributed += SetUnlock;
    }
    private void OnDestroy()
    {
        DataManager.DataDistributed -= SetUnlock;
    }
    void SetUnlock()
    {
        thisSUnitFarmButton.interactable = Player.castleLevel >= requiredLvToUnlock;
    }

    public void PrepareSUnitButton()
    {
        anim = animRoot.GetComponentInChildren<Animator>();
        ToggleAnimation(false);
    }

    public void GetSUnitsAmount()
    {
        GameSparksManager.Instance.GetSUnitsAmount(this);
    }

    public void PrepareBeginChallengeButton()
    {
        challengeButton.onClick.AddListener(delegate { LevelManager.Instance.SUnitFarm(thisSUnit); });
    }


    public void SUnitValueUpdate(int sUnitAmount)
    {
        thisSUnitAmount = sUnitAmount;
        Player.SetSUnitValues(sUnitAmount, thisSUnit);
        UpdateSUnitsAmount();
    }


    void UpdateSUnitsAmount()
    {
        currentAmountText.text = string.Format("{1} Amount : {0:n0}", Player.SUnitAnount(thisSUnit), thisSUnit.ToString().ToUpper());
    }

    public void ToggleAnimation(bool isToggled)
    {
        anim.enabled = isToggled;
    }
    public void ToggleBeginChallenge(bool isToggled)
    {
        challengeButton.interactable = isToggled;
    }
}
