﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileDamage : MonoBehaviour
{
    public GameObject explosion;
    public int attackDamage = 20;
    Health health;
 

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Boundary")
        {
            return;
        }

        other.GetComponent<Health>().TakeDamage(attackDamage);        
        Instantiate(explosion, transform.position, transform.rotation);

    }



    void Attack()
    {
        if (health.currentHealth > 0)
        {
            health.TakeDamage(attackDamage);
        }
    }
}


// Destroy everything that enters the trigger
/*    void OnTriggerEnter(Collider other)
 *    {
 *      Destroy(other.gameObject);
 *      Destroy(gameObject);
 *    }
 */
