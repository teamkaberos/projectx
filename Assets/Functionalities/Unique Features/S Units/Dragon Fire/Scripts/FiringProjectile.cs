﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Boundary
{
    public float xMin, xMax, yMin, yMax;
}

public class FiringProjectile : MonoBehaviour
{
    Rigidbody rb;
    public Boundary boundary;    
    public GameObject fireball;
    public Transform fireballSpawn;
    public float fireRate;

    // ---to use with Directional Control with Keyboard---
    //public float speed;
    //public float tilt;

    private float nextFire;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }


    void Update()
    {
        if (Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            Instantiate (fireball, fireballSpawn.position, fireballSpawn.rotation);
        }
    }
}

//For Directional Control usage with Keyboard
/* void FixedUpdate()
 {
     float moveHorizontal = Input.GetAxis ("Horizontal");
     float moveVertical = Input.GetAxis ("Vertical");

     Vector3 movement = new Vector3 (moveHorizontal, moveVertical, 0.0f);
     rb.velocity = movement * speed;

     rb.position = new Vector3
     (
         Mathf.Clamp (rb.position.x, boundary.xMin, boundary.xMax),
         Mathf.Clamp (rb.position.y, boundary.yMin, boundary.yMax),
         0.0f
     );

     rb.rotation = Quaternion.Euler (0.0f, 0.0f, rb.velocity.x * -tilt);             
 }*/



