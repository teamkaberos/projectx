﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class DestroyByContact : MonoBehaviour
{
    public GameObject explosion;
    public int attackDamage = 20;

    GameObject Wizard;
    Health playerHealth;


    private void Awake()
    {
        Wizard = GameObject.FindGameObjectWithTag("Wizard");
        playerHealth = Wizard.GetComponent<Health>();
    }


    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Boundary")
        {
            return;
        }

        Attack();
        Instantiate (explosion, transform.position, transform.rotation);
        
    }

    /*private void Update()
    {
        Attack();
    }*/


    void Attack()
    {
        if (playerHealth.currentHealth > 0)
        {
            playerHealth.TakeDamage(attackDamage);         
        }
    }
}


// Destroy everything that enters the trigger
/*    void OnTriggerEnter(Collider other)
 *    {
 *      Destroy(other.gameObject);
 *      Destroy(gameObject);
 *    }
 */