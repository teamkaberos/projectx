﻿using UnityEngine.UI;
using UnityEngine;



public class Health : MonoBehaviour
{
    public int startingHealth;
    public int currentHealth;
    public Slider healthSlider; //using UnityEngine.UI
    public Image damageImage;
    public AudioClip deathClip;
    public float flashSpeed = 5f;
    public Color flashColour = new Color(1f, 0f, 0f, 0.1f);

    Animator anim;
    AudioSource playerAudio;    
    TouchManager playerMovement;
    Mover playerShooting;
    //PlayerShooting playShooting;
    bool isDead;
    bool damaged;
  


    private void Awake()
    {
        anim = GetComponent<Animator>();       
        playerAudio = GetComponent<AudioSource>();
        playerMovement = GetComponent<TouchManager>();
        currentHealth = startingHealth;
    }

    
    private void Update()
    {
        if (damaged)
        {
            damageImage.color = flashColour;
        }

        else
        {
            damageImage.color = Color.Lerp(damageImage.color, Color.clear, flashSpeed * Time.deltaTime);
        }
        damaged = false;
    }



    public void TakeDamage(int amount)
    {
        damaged = true;
        currentHealth -= amount;       

        if (currentHealth <= 0 && !isDead)
        {
            Death();
        }

        if (name.Contains("Dragon"))
        {
            return;
        }

        else
        {
            healthSlider.value = currentHealth;
        }
    }


    void Death()
    {
        isDead = true;
        //playerShooting.DisableEffects ();

        anim.SetTrigger("Die");

        playerAudio.clip = deathClip;
        playerAudio.Play();

        playerMovement.enabled = false;
        playerShooting.enabled = false;
    }
}
