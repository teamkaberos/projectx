﻿using UnityEngine;
using UnityEngine.UI;

public class SUnitChallenge : MonoBehaviour
{
    public SUnitButton[] sUnitButtons;

    public static int sUnitAmountCap = 999;
    public static SUnitType.SUnit sUnitToAdd;

    // Start is called before the first frame update
    void Start()
    {
        PrepareAllSUnitsButtons();
        ForceSelect();
    }



    void PrepareAllSUnitsButtons()
    {
        for (int x = 0; x < sUnitButtons.Length; x++)
        {
            sUnitButtons[x].GetSUnitsAmount();
            sUnitButtons[x].PrepareSUnitButton();
            sUnitButtons[x].PrepareBeginChallengeButton();
        }
    }



    // "SelectMines" is to ensure the Mines challenge is selected whenever the menu is closed and reopened
    public void ForceSelect()
    {
        for (int x = 0; x < sUnitButtons.Length; x++)
        {
            sUnitButtons[x].GetComponent<Toggle>().isOn = false;
        }

        for (int x = 0; x < sUnitButtons.Length; x++)
        {
            if (sUnitButtons[x].thisSUnit == SUnitType.SUnit.Ballista)
            {
                sUnitButtons[x].GetComponent<Toggle>().isOn = true;
            }
        }
    }


    public int CurrentAmount(SUnitType.SUnit sUnitType)
    {
        int currentAmount = 0;
        for (int x = 0; x < sUnitButtons.Length; x++)
        {
            if (sUnitButtons[x].thisSUnit == sUnitType)
            {
                currentAmount = sUnitButtons[x].thisSUnitAmount;
                break;
            }
        }
        return currentAmount;
    }

}


public class SUnitType : MonoBehaviour
{
    public enum SUnit
    {
        Mines,
        Flames,
        Ballista
    }
}
