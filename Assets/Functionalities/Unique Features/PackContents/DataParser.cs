﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DataParser : MonoBehaviour
{
    public Transform textHolder;
    List<Text> itemName = new List<Text>();
    List<Text> itemDropRate = new List<Text>();

    public LootManager lootManager;

    float totalWeght;

    void Start()
    {
        for (int x = 0; x < lootManager.dropTable.Count; x++)
        {
            itemName.Add(textHolder.GetChild(x).GetComponent<Text>());
            totalWeght += lootManager.dropTable[x].itemWeight;
        }

        Display_DropRate();
    }

    void Display_DropRate()
    {
        for (int x = 0; x < lootManager.dropTable.Count; x++)
        {
            itemName[x].text = string.Format("<color={1}>{0}</color>", 
                                             lootManager.dropTable[x].name, /*((lootManager.dropTable[x].itemWeight / totalWeght)*100)*/
                                            lootManager.dropTable[x].textColor );
            Check_Whether_To_Display(lootManager.dropTable[x].itemWeight, itemName[x].gameObject);
        }
    }

    void Check_Whether_To_Display(int itemWeight, GameObject textObject)
    {
        if (itemWeight <= 0)
            textObject.SetActive(false);
    }

}
