﻿using UnityEngine;
using UnityEngine.UI;

public class NewItemObtained : MonoBehaviour
{
    public GameObject newItemObtainedDisplayCanvas;

    public Text itemObtainedDisplayText;
    public Image itemObtainImage;

    public Button tapToClose;


    void Start()
    {
        PlayOpenCard.NewCardReceived += DisplayReward;
        UnitManager.NewUnitEvolvedWasObtained += DisplayReward;
        LootManager.NewHeroFromIntervalReward += DisplayReward;
    }

    void OnDestroy()
    {
        PlayOpenCard.NewCardReceived -= DisplayReward;
        UnitManager.NewUnitEvolvedWasObtained -= DisplayReward;
        LootManager.NewHeroFromIntervalReward -= DisplayReward;
    }



    public void ListenToResumeOpening(LootCardHandler lootCardHandle)
    {
        tapToClose.onClick.AddListener(lootCardHandle.ResumeOpening);
    }


    public void DisplayReward(string heroName, Sprite newObject)
    {
        itemObtainedDisplayText.text = ParseText(heroName);
        ChangeDisplaySprite(newObject);
        ToggleNewItemObtainedDisplayCanvas(true);
    }


    void ChangeDisplaySprite(Sprite newObjectImage)
    {
        itemObtainImage.sprite = newObjectImage;
    }
    string ParseText(string newHeroName)
    {
        return "Congratulations! You've unlocked " + newHeroName.ToUpper();
    }
    void ToggleNewItemObtainedDisplayCanvas(bool toSet)
    {
        newItemObtainedDisplayCanvas.SetActive(toSet);
    }
}
