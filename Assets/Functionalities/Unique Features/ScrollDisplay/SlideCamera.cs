﻿using UnityEngine;
using System.Collections.Generic;

public class SlideCamera : MonoBehaviour
{
    protected new Camera camera;
    public float countAsDraggingTimer = 0.3f;

    [SerializeField] public static bool popup_Blocked;  // used to check if  there is any popup active

    [Header("Left/Right Limit Positions")]
    public Transform camPosLeftEnd;
    public Transform camPosRightEnd;

    [SerializeField] [Tooltip("Time for damping duration")] protected float smoothTime;

    [Space(15)]
    protected Vector3 mouseStartPos;
    protected Vector3 camStartPos;
    protected Vector3 camEndPos;
    protected Transform cameraPos;
    protected Vector3 targetPos;

    [SerializeField] public float dampFloat;

    [Space(15)]
    [Header("Lerp Var")]
    public float lerpTime = 1f;
    public float lerpSpeed;

    protected bool isLerping;
    protected bool isDamping;
    protected bool lerpingTowards;

    [Space(15)]
    public static bool dragging;

    protected float counter;
    protected Vector3 velocity = Vector3.zero;




    protected virtual void Start()
    {
        camera = GameObject.Find("Main Camera").GetComponent<Camera>();
        cameraPos = camera.gameObject.transform;
    }

    public bool IsDragging()
    {
        return popup_Blocked;
    }

    void StartDragging()
    {
        //If the player is not already touching the screen
        if (!dragging)
        {
            //Record the initial position of the touch input
            mouseStartPos = Input.mousePosition;
            //Keep camera's current position
            camStartPos = cameraPos.localPosition;
            //Stores player touching info
            dragging = true;
        }
    }


    //void Update()
    //{
    //    if (Input)
    //}
    protected virtual void Update()
    {
        if (Input.GetMouseButton(0))
        {
            counter += Time.deltaTime;
            if (counter >= countAsDraggingTimer)
            {
                popup_Blocked = true;
            }
        }

        if (isDamping)
        {
            cameraPos.localPosition = Vector3.SmoothDamp(cameraPos.localPosition, targetPos, ref velocity, smoothTime);
            if (cameraPos.localPosition == targetPos)
            {
                isDamping = false;
            }
        }
    }

    void LateUpdate()
    {
        if (Input.GetMouseButtonUp(0))
        {
            dragging = false;
            counter = 0;
            popup_Blocked = false;
        }

        if (!Input.GetMouseButton(0)) return;
        {
            if (Input.touchCount > 1)
                return;

            StartDragging();
            if (dragging)
            {
                //difference between start position and current position
                //from the player's first touch to current point
                //&& storing it in vector3
                Vector3 t = mouseStartPos - Input.mousePosition;

                targetPos = new Vector3(Mathf.Clamp((camStartPos.x + (t.x * dampFloat)), camPosLeftEnd.position.x, camPosRightEnd.position.x)
                                        , camStartPos.y, cameraPos.position.z);

                isDamping = true;
            }
        }
    }



}

