﻿using UnityEngine;
using UnityEngine.UI;

public class InfoDisplay : MonoBehaviour
{
    [Header("UI TEXT DISPLAYS")]
    public Text heroName;
    public Text heroLevel;
    public Text heroClass;
    public Text heroDescrip;
    public Text heroAbility;

    public void FormatInfoForDisplay(Hero_Evolution_Info evoInfo)
    {
        heroName.text = string.Format("HERO NAME : {0}",evoInfo.heroName);
        heroLevel.text = string.Format("HERO LEVEL : {0}", "10");
        heroClass.text = string.Format("HERO CLASS : {0}", evoInfo.heroClass);
        heroDescrip.text = string.Format("HERO DESCRIP : {0}", evoInfo.heroDescription);
        heroAbility.text = string.Format("HERO ABILITY : {0}", evoInfo.skillText[4]);
    }
}
