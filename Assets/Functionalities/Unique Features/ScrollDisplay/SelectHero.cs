﻿using UnityEngine;
using UnityEngine.UI;

public class SelectHero : MonoBehaviour
{
    public Hero_Evolution_Info thisHero;    // the obj that holds necessary info of this Hero
    Button selectHero;
    public InfoDisplay infoDisplayPanel;

    void Start()
    {
        selectHero = GetComponent<Button>();
        selectHero.onClick.AddListener(DisplayInfo);
    }

    void DisplayInfo()
    {
        infoDisplayPanel.FormatInfoForDisplay(thisHero);
    }

    string FormatNecessary()
    {
        return string.Format("HeroName > {0}", thisHero.heroName);
    }
}
