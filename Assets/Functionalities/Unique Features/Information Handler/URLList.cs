﻿public static class URLList
{
	// ----- CONNECTION ----- \\

	public static string check_Connectivity_URL = "http://159.89.207.43/Connectivity_Check.php";
	public static string LoginURL = "http://159.89.207.43/Login.php";
	public static string CreateUserURL = "http://159.89.207.43/InsertUser.php";
	public static string logOutURL = "http://159.89.207.43/Logout.php";

	// ====================== \\



	// -------- TIME -------- \\

	public static string GetTimeURL = "http://159.89.207.43/GetTime.php";
	public static string GetServerTimeURL = "http://159.89.207.43/GetTime.php";
	public static string Get_Structure_StampedTimeURL ="http://159.89.207.43/GetStampedTime.php";
	public static string SendExitTimeURL = "http://159.89.207.43/StampExitTime.php";
	public static string Login_Logout_TimeStampURL = "http://159.89.207.43/Login_And_Logout_Stamp.php";
	public static string timeStampURL = "http://159.89.207.43/TimeStamp.php";
	public static string allUpgradingURL = "http://159.89.207.43/AllUpgradesTimeStamp.php";
	public static string clearTimeStampURL = "http://159.89.207.43/ClearTimeStamp.php";
	public static string stoneTimeStampURL = "http://159.89.207.43/StoneTimeStamp.php";
	public static string stone1TimeStampURL = "http://159.89.207.43/Stone1TimeStamp.php";
	public static string stone2TimeStampURL = "http://159.89.207.43/Stone2TimeStamp.php";

	// ====================== \\



	// -------- MAIL -------- \\

	public static string verifyingMailSenderURL = "http://159.89.207.43/Verify_Mail_Sender.php";
	public static string deleteMailURL = "http://159.89.207.43/RemoveMail.php";
	public static string send_Mail_URL ="http://159.89.207.43/Send_Mail.php";
	public static string send_Mail_To_Players = "http://159.89.207.43/Send_Mail.php";

	// ====================== \\



	// ------- REWARD ------- \\

	public static string get_Player_Rewards_URL = "http://159.89.207.43/Player_Reward_Update.php";
	public static string Claim_Interval_Reward_TimeURL = "http://159.89.207.43/ClaimIntervalReward.php";

	// ====================== \\



	// ------ MISSION ------- \\

	public static string GetMissionsURL = "http://159.89.207.43/GetMission.php";
	public static string SendMissionsURL = "http://159.89.207.43/SendMission.php";

	// ====================== \\



	// ------- TROOPS ------- \\

	public static string store_Hero_ValuesURL = "http://159.89.207.43/Player_Troops_Info_Send.php";

	// ====================== \\



	// ------- BATTLE ------- \\

	public static string troopAssignmentSlotURL = "http://159.89.207.43/UnitAssign.php";
	public static string get_Player_Info_URL = "http://159.89.207.43/Player_Info_Update.php";
	public static string attainSeigedEnemyDataURL = "http://159.89.207.43/GetRevengeEnemyInfo.php";
	public static string enemyDataLoaderURL = "http://159.89.207.43/LoadEnemyInfo.php";
	public static string update_Enemy_Raided_Resources = "http://159.89.207.43/Raided_Enemy_Outcome.php";
	public static string update_Tutorial_State_URL = "http://159.89.207.43/Tutorial_State.php";
	public static string getNewSeigesURL = "http://159.89.207.43/StructUpgrade.php";

	// ====================== \\



	// ----- STRUCTURES ----- \\

	public static string get_Structure_Lvls = "http://159.89.207.43/GetStructureLvl.php";
	public static string strcture_Update_Send_URL = "http://159.89.207.43/Player_Structure_Send.php";
	public static string expandables_Update_Send_URL = "http://159.89.207.43/Player_Expandables_Send.php";
	public static string purchaseStoneBuildSlotURL = "http://159.89.207.43/Purchase_Stone_Unlock_Slot.php";
	public static string newPurchaseStoneBuildSlotURL = "http://159.89.207.43/New_Purchase_StoneSlot.php";
	public static string storeRSSLastValueURL = "http://159.89.207.43/Store_RSS_Values.php";

	// ====================== \\

}
