﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class EnemyLoader : MonoBehaviour
{
    #region Singleton
    static EnemyLoader _instance;
    public static EnemyLoader Instance
    {
        get
        {
            return _instance;
        }
    }
    #endregion
    //Serializable to enable list for holding enemy assigned unit info
    [Serializable]
    public class EnemyAssgnedUnitsInt
    {
        [HideInInspector]
        public string name;
        public List<int> assignedUnits = new List<int>();
        public List<int> assignedUnitsLevels = new List<int>();

        public bool[] thisPlayerUnitChecked = { false, false, false };

        public void AddName(string nameSent)
        {
            name = nameSent;
        }

        public void ClearAssignedUnits()
        {
            if (assignedUnits.Count > 0)
            {
                assignedUnits.Clear();
                assignedUnitsLevels.Clear();
            }
        }
        public void ResetRetrieval()
        {
            for (int x = 0; x < thisPlayerUnitChecked.Length; x++)
                thisPlayerUnitChecked[x] = false;
        }

        public void AddAssignedUnit(int unitValue, int unitLevel)
        {
            //Debug.Log(string.Format("This unit is number {0} and level {1}", unitValue, unitLevel));
            assignedUnits.Add(unitValue);
            assignedUnitsLevels.Add(unitLevel);
        }

        public void SetUnitCheck(int boolValue)
        {
            thisPlayerUnitChecked[boolValue] = true;

            Instance.CheckIfEnemyInfoRetrieved();
        }
    }



    // ==== INFO LIST FOR REGULAR SEARCH ENEMIES ==== \\
    [HideInInspector] public List<string> enemyIDs = new List<string>();
    [HideInInspector] public List<string> enemyNames = new List<string>();
    [HideInInspector] public List<string> enemyRaces = new List<string>();
    [HideInInspector] public List<string> enemyPotrait = new List<string>();
    [HideInInspector] public List<string> enemyClans = new List<string>();
    [HideInInspector] public List<float> enemyShuffleTimes = new List<float>();
    [HideInInspector] public List<int> enemyReputations = new List<int>();
    [HideInInspector] public List<int> enemyGolds = new List<int>();
    [HideInInspector] public List<int> enemyLevels = new List<int>();
    public List<EnemyAssgnedUnitsInt> enemyAssignedUnits = new List<EnemyAssgnedUnitsInt>();

    //  ==== INFO LIST FOR SIEGES ==== \\
    public static List<string> attackerIDList = new List<string>();
    public static List<DateTime> timeOfAttackList = new List<DateTime>();
    public static List<long> goldStolenList = new List<long>();
    public static List<string> attackerNameList = new List<string>();
    public static List<string> attackerPotraitList = new List<string>();
    public static List<string> attackerClanList = new List<string>();
    public static List<string> attackerRaceList = new List<string>();
    public static List<float> attackerShuffleTimeList = new List<float>();
    public static List<int> attackerReputationList = new List<int>();
    public static List<int> attackerGoldList = new List<int>();
    public static List<int> attackerLevelList = new List<int>();
    public List<EnemyAssgnedUnitsInt> attackerAssignedUnitList = new List<EnemyAssgnedUnitsInt>(6);

    public List<string> displayList = new List<string>();

    public static long lastNotificationTimeRecieved;

    // ==== DISPLAY INFO ON REVENGE TAB ==== \\
    public GameObject revengeTargetButton;      //The object to be instantiated holding the revenge target infos
    public Transform siegedInbox;                      //Where the revengeTargets will be held


    public static string selectedEnemyID;               // This will store the ID of who the Player attacks, whether regular or siege
    public static string attackOccurrence;


    string readLine;
    [TextArea(5, 3)]
    [HideInInspector] public string[] type;
    [HideInInspector] public string[][] values;

    int loadEnemyValue = 6;                             //how many enemies do you want to load?

    public ToggleGroup loadedEnemyToggles;  //for switching all toggles off
    public GameObject[] loaded_Enemy_Detail_Buttons;
    public List<int> defaultListIntOrder = new List<int> { 0, 1, 2, 3, 4, 5 };
    public List<int> loadedDetailsButtonOrder = new List<int>();
    public List<int> loadedDetailsButtonOrderOutput = new List<int>();
    LoadedEnemy[] listOfVictims;

    public Button PVP_Button;
    public Button revengeButton;
    public Button refreshList_Button;

    public GameObject loadingScreenPopup;

    public EnemyAssgnedUnitsInt chatboxTargetAssignedUnits;
    int chatboxEnemyTroopCounter = 0;  // used to track if all the enemy troops from the chat box has been loaded

    public Clan clan_;

    int[] pulledAssignedSets = new int[6];
    bool[] retrievalCheck = new bool[6];

    public Text[] displayTextTest;

    public Toggle chatCanvasToggle;


    //public static bool siegeCheckCount = 0;

    void Start()
    {
        _instance = this;

        if (Player.tutorial == 4)
        {
            PVP_Button.onClick.AddListener(LoadOpponentList);
        }

        AssignLoadedEnemy();

        PVP_Button.onClick.AddListener(GetAListOfEnemies);
        PVP_Button.onClick.AddListener(SwitchOffLoadedEneimesToggle);

        refreshList_Button.onClick.AddListener(GetAListOfEnemies);
        refreshList_Button.onClick.AddListener(SwitchOffLoadedEneimesToggle);

        revengeButton.onClick.AddListener(GetAListOfEnemies);
        revengeButton.onClick.AddListener(SwitchOffLoadedEneimesToggle);

        GetAListOfEnemies();
    }

    void AssignLoadedEnemy()
    {
        listOfVictims = new LoadedEnemy[loadEnemyValue];
        for (int x = 0; x < loadEnemyValue; x++)
        {
            listOfVictims[x] = loaded_Enemy_Detail_Buttons[x].GetComponent<LoadedEnemy>();
        }
    }


    // +++ ====== ENEMY LOADER SHUFFLING ====== +++ \\
    void SetToBeShuffledList()
    {
        for (int x = 0; x < loadEnemyValue; x++)
        {
            loadedDetailsButtonOrder.Add(defaultListIntOrder[x]);
        }
    }
    void ClearLoadedDetailsButtonOrderOutput()
    {
        loadedDetailsButtonOrderOutput.Clear();
    }
    void ShuffleOrder()
    {
        // Reset the list of ints to be shuffled
        // Clear the Output list to prepare it for taking in a new order
        for (int x = 0; x < loadEnemyValue; x++)
        {
            int toMove = UnityEngine.Random.Range(0, loadedDetailsButtonOrder.Count - 1);
            loadedDetailsButtonOrderOutput.Add(loadedDetailsButtonOrder[toMove]);
            loadedDetailsButtonOrder.RemoveAt(toMove);
        }
    }
    void RearrangeOBJs()
    {
        for (int x = loadEnemyValue - 1; x >= 0; x--)
        {
            for (int y = 0; y < loadEnemyValue; y++)
            {
                // Find the enemy loader with the new order number from the bottom
                if (listOfVictims[y].orderNo == loadedDetailsButtonOrderOutput[x])
                {
                    loaded_Enemy_Detail_Buttons[y].transform.SetAsLastSibling();
                }
            }
        }
    }


    // +++===== REGULAR TARGETS PULLING ======+++ \\
    public void GetAListOfEnemies()
    {
        SetToBeShuffledList();
        ClearLoadedDetailsButtonOrderOutput();
        ShuffleOrder();
        RearrangeOBJs();

        ResetBoolCheck();
        ResetPulledAssignedUnitsSets();
        ClearAllList();
        GenerateEnemyList();
        GameSparksManager.Instance.GetEnemyList(loadEnemyValue);        // Get the ID of the enemies first (Continued at LoadOpponentList())
    }
    // ===== CLEARS ANY RETAINED INFOMATION THAT IS UNNECESSARY ===== \\
    void ClearAllList()
    {
        //clear the list everytime the player needs to pull, so when the GS replies the number of times 
        enemyIDs.Clear();
        enemyNames.Clear();
        enemyRaces.Clear();
        enemyClans.Clear();
        enemyPotrait.Clear();
        enemyShuffleTimes.Clear();
        enemyReputations.Clear();
        enemyGolds.Clear();
        enemyLevels.Clear();
        ResetEnemyUnitsList();
    }
    void ResetEnemyUnitsList()
    {
        for (int x = 0; x < loadEnemyValue; x++)
        {
            enemyAssignedUnits[x].ClearAssignedUnits();
            enemyAssignedUnits[x].ResetRetrieval();
        }
    }
    void GenerateEnemyList()
    {
        for (int x = 0; x < loadEnemyValue; x++)
        {
            enemyNames.Add("-");
            enemyRaces.Add("-");
            enemyClans.Add("-");
            enemyPotrait.Add("-");
            enemyShuffleTimes.Add(0);
            enemyReputations.Add(0);
            enemyGolds.Add(-100);
            enemyLevels.Add(0);
        }
    }
    void ResetBoolCheck()
    {
        for (int x = 0; x < retrievalCheck.Length; x++)
        {
            retrievalCheck[x] = false;
        }
    }
    void ResetPulledAssignedUnitsSets()
    {
        for (int x = 0; x < pulledAssignedSets.Length; x++)
        {
            // this number gets added when the retrieved enemy set is returned
            pulledAssignedSets[x] = 0;
        }
    }

    public void AddEnemyToList(string eID)
    {
        //Add the pulled opponent from server into the list, and check if the list is filled.
        enemyIDs.Add(eID);

        if (enemyIDs.Count == loadEnemyValue)
            LoadOpponentList();
    }
    void LoadOpponentList()
    {
        if (Player.tutorial == 4)
        {
            Hide_Enemy_Button_For_Tutorial();
            Tutorial_Enemy();
            CloseLoadingScreen();
        }
        else
        {
            PullRawEnemyData();        // Getting the raw values
        }
    }

    public void TroopPullCheck(int queueNo)
    {
        pulledAssignedSets[queueNo] += 1;
        ConfirmPullComplete(queueNo);
    }
    public void ConfirmPullComplete(int queueNo)
    {
        //Check enemy data
        if (EnemyDataRetrieved(queueNo) == false)
            return;
        ////Check enemy details
        if (EnemyDetailRetrieved(queueNo) == false)
            return;
        //// Check if expandebles are retrieved
        if (CheckResourcesPull(queueNo) == false)
            return;
        //// Check if there is at least a minimum of 1 troop
        if (!CheckEnemyAssignedSets(queueNo) == false)
            return;

        DisplayAllLoaded();


        retrievalCheck[queueNo] = true;

        if (ReadyForDistribution())
        {
            DistributeEnemyData();
            CloseLoadingScreen();
        }
    }
    #region Enemy Data Pull Check
    bool EnemyDataRetrieved(int queueNo)
    {
        if (enemyLevels[queueNo] == 0 || enemyReputations[queueNo] == 0 || enemyShuffleTimes[queueNo] == 0)
            return false;
        return true;
    }
    bool EnemyDetailRetrieved(int queueNo)
    {
        if (enemyRaces[queueNo] == "-" || enemyNames[queueNo] == "-")
            return false;
        return true;
    }
    bool CheckResourcesPull(int queueNo)
    {
        if (enemyGolds[queueNo] == -100)
            return false;
        return true;
    }
    bool CheckEnemyAssignedSets(int queueNo)
    {
        if (pulledAssignedSets[queueNo] < 3)
        {
            //Debug.Log()
            return false;
        }
        return true;
    }
    bool ReadyForDistribution()
    {
        for (int x = 0; x < retrievalCheck.Length; x++)
        {
            if (retrievalCheck[x] == false)
                return false;
        }
        return true;
    }
    #endregion


    void DisplayAllLoaded()
    {
        for (int x = 0; x < enemyIDs.Count; x++)
        {
            displayTextTest[x].text = string.Format("{5} : {0} (ID) \n {1} (name) \n {6} (level) \n {7:n0} (reputation) \n {8:n0} (gold) \n {2} (shuffle time) \n {3} (assigned units) \n {4} (assigned units level)",
                enemyIDs[x], enemyNames[x], enemyShuffleTimes[x], EnemyUnitsParse(x), EnemyUnitLVParse(x), x, enemyLevels[x], enemyReputations[x], enemyGolds[x]);
        }
    }

    string EnemyUnitsParse(int enemyno)
    {
        string toReturn = "";

        for (int x = 0; x < enemyAssignedUnits[enemyno].assignedUnits.Count; x++)
        {
            toReturn += enemyAssignedUnits[enemyno].assignedUnits[x] + ", ";
        }

        return toReturn;
    }
    string EnemyUnitLVParse(int enemyno)
    {
        string toReturn = "";

        for (int x = 0; x < enemyAssignedUnits[enemyno].assignedUnitsLevels.Count; x++)
        {
            toReturn += enemyAssignedUnits[enemyno].assignedUnitsLevels[x] + ", ";
        }

        return toReturn;
    }


    void PullRawEnemyData()
    {
        for (int i = 0; i < loadEnemyValue; i++)
        {
            GameSparksManager.Instance.GetEnemyData(enemyIDs[i], i, "regular");
            GameSparksManager.Instance.GetEnemyDetails(enemyIDs[i], i, "regular");
            GameSparksManager.Instance.GetEnemyExpendables(enemyIDs[i], i, "regular");
            GameSparksManager.Instance.GetEnemyAssignedUnits(enemyIDs[i], i, "regular");
        }
    }
    void DistributeEnemyData()
    {
        for (int i = 0; i < loadEnemyValue; i++)
        {
            listOfVictims[i].playerID = enemyIDs[i];
            listOfVictims[i].playerName = enemyNames[i];
            listOfVictims[i].potrait = enemyPotrait[i];
            listOfVictims[i].race = enemyRaces[i];
            listOfVictims[i].clan = enemyClans[i];
            listOfVictims[i].shuffleTiming = enemyShuffleTimes[i];
            listOfVictims[i].reputation = enemyReputations[i];
            listOfVictims[i].gold = enemyGolds[i];
            listOfVictims[i].castleLv = enemyLevels[i];
            listOfVictims[i].enemyAssignedUnitsN = enemyAssignedUnits[i].assignedUnits;
            listOfVictims[i].enemyAssignedUnitsLevelN = enemyAssignedUnits[i].assignedUnitsLevels;
            listOfVictims[i].UpdateOpponentInfo();
        }
    }
    void CloseLoadingScreen()
    {
        loadingScreenPopup.SetActive(false);
    }

    void SwitchOffLoadedEneimesToggle()
    {
        loadedEnemyToggles.SetAllTogglesOff();
    }


    // ======== SIEGED ENEMY LOADER ======= \\

    // ==== OBTAIN INFO FROM CLOUD OF SIEGES THAT HAPPENED WHILE AWAY ==== \\
    public void ClearAttackerList()
    {
        attackerIDList.Clear();
        timeOfAttackList.Clear();
        goldStolenList.Clear();
    }
    public void AddAttackerToList(string attackerID, DateTime timeOfAttack, long goldStolen)
    {
        attackerIDList.Add(attackerID);
        timeOfAttackList.Add(timeOfAttack);
        goldStolenList.Add(goldStolen);
    }
    public void SiegeEnemiesDisplay(string occurrenceTime)
    {
        attackOccurrence = occurrenceTime;
        ClearLists();
        GenerateSiegeList();
        CompleteLoading();
    }
    void ClearLists()
    {
        attackerNameList.Clear();
        attackerClanList.Clear();
        attackerPotraitList.Clear();
        attackerRaceList.Clear();
        attackerShuffleTimeList.Clear();
        attackerReputationList.Clear();
        attackerGoldList.Clear();
        attackerLevelList.Clear();
    }
    void GenerateSiegeList()
    {
        if (attackerIDList.Count > 0)
        {
            for (int x = 0; x < attackerIDList.Count; x++)
            {
                attackerNameList.Add("-");
                attackerClanList.Add("-");
                attackerPotraitList.Add("-");
                attackerRaceList.Add("-");
                attackerShuffleTimeList.Add(-100);
                attackerReputationList.Add(-100);
                attackerGoldList.Add(-100);
                attackerLevelList.Add(-100);
            }
        }
    }
    void CompleteLoading()
    {
        if (attackerIDList.Count > 0)
        {
            for (int i = 0; i < attackerIDList.Count; i++)
            {
                GameSparksManager.Instance.GetEnemyData(attackerIDList[i], i, "siege");
                GameSparksManager.Instance.GetEnemyDetails(attackerIDList[i], i, "siege");
                GameSparksManager.Instance.GetEnemyExpendables(attackerIDList[i], i, "siege");
                GameSparksManager.Instance.GetEnemyAssignedUnits(attackerIDList[i], i, "siege");
            }
        }
    }
    public void CheckIfEnemyInfoRetrieved()
    {
        for (int x = 0; x < attackerIDList.Count; x++)
        {
            if (attackerNameList[x].Contains("-"))
            {
                return;
            }
        }
        for (int x = 0; x < attackerIDList.Count; x++)
        {
            if (attackerShuffleTimeList[x] == -100f)
            {
                return;
            }
        }
        for (int x = 0; x < attackerIDList.Count; x++)
        {
            if (attackerReputationList[x] == -100f)
            {
                return;
            }
        }
        for (int x = 0; x < attackerIDList.Count; x++)
        {
            if (attackerLevelList[x] == -100f)
            {
                return;
            }
        }
        for (int x = 0; x < attackerIDList.Count; x++)
        {
            if (attackerClanList[x].Contains("-"))
            {
                return;
            }
        }
        for (int x = 0; x < attackerIDList.Count; x++)
        {
            if (attackerGoldList[x] == -100f)
            {
                return;
            }
        }
        for (int x = 0; x < attackerIDList.Count; x++)
        {
            if (attackerLevelList[x] == -100)
            {
                return;
            }
        }
        for (int x = 0; x < attackerIDList.Count; x++)
        {
            if (attackerAssignedUnitList[x].thisPlayerUnitChecked[0] == false)
            {
                return;
            }
        }
        for (int x = 0; x < attackerIDList.Count; x++)
        {
            if (attackerAssignedUnitList[x].thisPlayerUnitChecked[1] == false)
            {
                return;
            }
        }
        for (int x = 0; x < attackerIDList.Count; x++)
        {
            if (attackerAssignedUnitList[x].thisPlayerUnitChecked[2] == false)
            {
                return;
            }
        }
        CreateSiegeButtons();
    }
    public void CreateSiegeButtons()
    {
        if (siegedInbox.childCount > 0)
        {
            for (int x = 0; x < siegedInbox.childCount; x++)
            {
                siegedInbox.GetChild(x).GetComponent<Siege>().DestroySeigeItem();
            }
        }
        //Create and transfer the info into the siege button
        for (int x = 0; x < attackerIDList.Count; x++)
        {
            GameObject go = Instantiate(revengeTargetButton, siegedInbox.position, Quaternion.identity, siegedInbox);
            Siege siegeInfo = go.GetComponent<Siege>();
            siegeInfo.clan_ = clan_;
            siegeInfo.playerID = attackerIDList[x];
            siegeInfo.playerName = attackerNameList[x];
            siegeInfo.race = attackerRaceList[x];
            siegeInfo.lostGold = goldStolenList[x];
            siegeInfo.clan = attackerClanList[x];
            siegeInfo.shuffleTiming = attackerShuffleTimeList[x];
            siegeInfo.reputation = attackerReputationList[x];
            siegeInfo.gold = attackerGoldList[x];
            siegeInfo.castleLv = attackerLevelList[x];
            siegeInfo.timeOfAttack = timeOfAttackList[x];
            siegeInfo.enemyAssignedUnitsN = attackerAssignedUnitList[x].assignedUnits;
            siegeInfo.enemyAssignedUnitsLevelN = attackerAssignedUnitList[x].assignedUnitsLevels;
            siegeInfo.UpdateOpponentInfo();
        }
        CheckForNewSiege();
    }
    void CheckForNewSiege()
    {
        if (lastNotificationTimeRecieved < DataManager.lastLogout)
            lastNotificationTimeRecieved = DataManager.lastLogout;
        int timesHitWhileGone = 0;
        long latestTimeHit = lastNotificationTimeRecieved;

        for (int x = 0; x < attackerIDList.Count; x++)
        {
            // then, we check this time of attack
            long thisTimeOfAttack = (long)timeOfAttackList[x].Subtract(new DateTime(1970, 1, 1)).TotalSeconds;

            //Debug.Log(DataManager.lastLogout + " last " + thisTimeOfAttack + " this time ");

            if (thisTimeOfAttack > lastNotificationTimeRecieved)
            {
                timesHitWhileGone += 1;

                Debug.Log(timesHitWhileGone + " is added");

                if (thisTimeOfAttack >= lastNotificationTimeRecieved)
                    latestTimeHit = thisTimeOfAttack;
            }
        }
        lastNotificationTimeRecieved = latestTimeHit;

        UIManager.Instance.DisplaySeiged(timesHitWhileGone, attackOccurrence);
    }



    // ========= CHATBOX ENEMY LOADER ========= \\
    public void GetChatboxTargetAssignedUnits(string playerID)
    {
        GameSparksManager.Instance.GetEnemyAssignedUnits(playerID, 0, "chatbox");
    }
    public void TroopsObtained()
    {
        chatboxEnemyTroopCounter += 1;
        if (chatboxEnemyTroopCounter == 3)
        {
            Enemy.troopsAssigned = chatboxTargetAssignedUnits.assignedUnits;
            Enemy.troopsAssignedLevels = chatboxTargetAssignedUnits.assignedUnitsLevels;

            chatCanvasToggle.onValueChanged.Invoke(false);

            LevelManager.Instance.BattleScene();
        }
    }



    // ======= TUTORIAL ENEMY ======= \\
    public void Tutorial_Enemy()
    {
        listOfVictims[0].playerName = "Faldeut.Meynem";
        listOfVictims[0].clan = "";
        listOfVictims[0].castleLv = 1;
        listOfVictims[0].reputation = 100;
        listOfVictims[0].gold = 2000;
        listOfVictims[0].shuffleTiming = 25f;
        listOfVictims[0].race = "";
        listOfVictims[0].enemyAssignedUnitsN = new List<int> { 100, 101, 102 };
        listOfVictims[0].enemyAssignedUnitsLevelN = new List<int> { -100, -100, -100 };
        listOfVictims[0].UpdateOpponentInfo();
        listOfVictims[0].GetComponent<Toggle>().isOn = false;
        listOfVictims[0].Listen();


        listOfVictims[1].playerName = "Un.Killable";
        listOfVictims[1].clan = "Impregnable";
        listOfVictims[1].castleLv = 8;
        listOfVictims[1].reputation = 2775;
        listOfVictims[1].gold = 6283422;
        listOfVictims[1].shuffleTiming = 12f;
        listOfVictims[1].race = "";
        listOfVictims[1].enemyAssignedUnitsN = new List<int> { 100, 101, 102 };
        listOfVictims[1].enemyAssignedUnitsLevelN = new List<int> { -100, -100, -100 };
        listOfVictims[1].UpdateOpponentInfo();
        //listOfVictims[1].GetComponent<Toggle>().isOn = true;
    }

    public void Hide_Enemy_Button_For_Tutorial()
    {
        for (int x = 1; x < loaded_Enemy_Detail_Buttons.Length; x++)
        {
            loaded_Enemy_Detail_Buttons[x].SetActive(false);
        }
    }

    public void LoadBattleScene()
    {
        LevelManager.Instance.BattleScene();
        LevelManager.Instance.StoreTotalGameTime();
    }
}