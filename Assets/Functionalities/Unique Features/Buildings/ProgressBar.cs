﻿using UnityEngine;
using UnityEngine.UI;
using System;

public class ProgressBar : MonoBehaviour 
{
	public Transform LoadingBar;

	public float cdTime;
	public float currentAmount;

	DateTime currentDate;
	DateTime oldDate;
	public static float timePassed;
	// for identifying which instances will be disabled
	public int index;

	public Text time_Left_For_Building_To_Complete;		//The value shown on the UI

	void Awake()
	{
		if (!PlayerPrefs.HasKey ("TimerIsRunning" + index))
		{
			gameObject.SetActive (false);
		}
		cdTime = PlayerPrefs.GetFloat ("CooldownTime" + index);
	}

	void OnApplicationQuit()
	{
		PlayerPrefs.SetFloat ("TimerAmount" + index, currentAmount);
		PlayerPrefs.SetFloat ("CooldownTime" + index, cdTime);
	}

	void OnDisable()
	{
		PlayerPrefs.SetFloat ("TimerAmount" + index, currentAmount);
		PlayerPrefs.SetFloat ("CooldownTime" + index, cdTime);
	}

	void OnEnable()
	{
		PlayerPrefs.SetString ("TimerIsRunning" + index, "Ahoy");
		currentAmount = PlayerPrefs.GetFloat ("TimerAmount" + index);
		CalculateTimePassed ();
		if (currentAmount > 0)
		{
			UpdateTimeBarUI ();
		}
	}

    void CalculateTimePassed()
	{
		currentDate = DateTime.UtcNow;
		long temp = Convert.ToInt64 (PlayerPrefs.GetString ("sysString"));
		oldDate = DateTime.FromBinary (temp);
		TimeSpan difference = currentDate.Subtract (oldDate);
		timePassed = (float)difference.TotalSeconds;
		print ("Time passed: " + TimeMaster.timePassed);
	}

    void UpdateTimeBarUI()
	{
		currentAmount += TimeMaster.timePassed;
		LoadingBar.GetComponent<Image> ().fillAmount = currentAmount / cdTime;
	}

}