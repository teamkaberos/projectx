﻿using UnityEngine.UI;
using UnityEngine;
public class Barrack_Update : BaseUpdate_Popoup
{
	public Button buildStone_Button;
	public Button evolve_Button;
    public GameObject purchase_Research_Slot;

    public GameObject unableToEvoleDisplayTexts;
    public GameObject ableToEvolveText;

    public string thisName;

    public GameObject[] stoneCompleteNotificationObj;

    //void Awake()
    //{
    //    if (name.Contains("Bunker"))
    //        thisName = "bunker";
    //    else if (name.Contains("Range"))
    //        thisName = "range";
    //    else if (name.Contains("Guild"))
    //        thisName = "guild";

    //    Debug.Log(thisName + " THIS IS THE NAME");
    //}

    public override void GetUpdated()
	{
        StructureInfoDisplay(thisName);
        RequirementCheck(thisName);
        StoneBuildSlotChecker(thisName);
        ToggleStoneCompletNotification();

        if (Player.tutorial < 7)
            TutorialArea();
  	}

    // ==== If Player still in Tutorial state ==== \\
    void TutorialArea()
    {
        buildStone_Button.interactable = false;
        purchase_Research_Slot.GetComponent<Button>().interactable = false;
    }
    void DisableStone_Button()
    {
        buildStone_Button.interactable = false;
    }

    void Assign_Listener(Button button, int methodToAssign, int stoneSLotToBuildOn)
    {
        button.onClick.RemoveAllListeners();
        if (methodToAssign == 2)
        {
            button.onClick.AddListener(delegate { BuildTimer.Instance.StoneQueue(stoneSLotToBuildOn); });
        }
    }

    // ==== Display Name ==== \\
    void StructureInfoDisplay(string buildingName)
    {
        switch (buildingName.ToLower())
        {
            case "bunker":
                currentLvl.text = string.Format("Knight Training Ground Lv{0} ", Player.bunkerLv);
                currentValue.text = string.Format("{0} / {1}", Player.squareBattles, Player.evolvementRequiredBattles);
                break;
            case "range":
                currentLvl.text = string.Format("Archer Training Ground Lv{0}", Player.rangeLv);
                currentValue.text = string.Format("{0} / {1}", Player.triangleBattles, Player.evolvementRequiredBattles);
                break;
            case "guild":
                currentLvl.text = string.Format("Mage Training Ground Lv{0}", Player.guildLv);
                currentValue.text = string.Format("{0} / {1}", Player.circleBattles, Player.evolvementRequiredBattles);
                break;
        }
    }

    // ==== Evolvable Checking ==== \\
    void RequirementCheck(string buildingType)
    {
        bool stoneCheck = false;
        bool battleCheck = false;
        switch (buildingType.ToLower())
        {
            case "bunker":
                if (Player.squareBattles >= Player.evolvementRequiredBattles)
                {
                    battleCheck = true;
                    if (Player.squareStone < 1) currentValue.text = "Requires Earth stones";
                    else stoneCheck = true;
                }
                break;
            case "range":
                if (Player.triangleBattles >= Player.evolvementRequiredBattles)
                {
                    battleCheck = true;
                    if (Player.triangleStone < 1) currentValue.text = "Requires Wind stones";
                    else stoneCheck = true;
                }
                break;
            case "guild":
                if (Player.circleBattles >= Player.evolvementRequiredBattles)
                {
                    battleCheck = true;
                    if (Player.circleStone < 1) currentValue.text = "Requires Flame stones";
                    else stoneCheck = true;
                }
                break;
        }
        EvolvableInfoDisplay(unitEvolvable(stoneCheck, battleCheck));
    }

    // ==== Stone Slot Checker ==== \\
    void StoneBuildSlotChecker(string stoneSlotName)
    {
        switch (stoneSlotName.ToLower())
        {
            case "bunker":
                if (Player.earthStoneBuildSlot == 2)
                    purchase_Research_Slot.SetActive(false);
                break;
            case "range":
                if (Player.windStoneBuildSlots == 2)
                    purchase_Research_Slot.SetActive(false);
                break;
            case "guild":
                if (Player.flameStoneBuildSlot== 2)
                    purchase_Research_Slot.SetActive(false);
                break;
        }
    }

    public void CheckIfThereAreStonesBeingBuilt()
    {
        if(thisName.ToLower().Contains("bunker"))
        {
            if (BuildTimer.earthstone0_Build_Time >= 0)
                BuildingManager.Instance.myHomeBuildings_[2].ToggleNotification(false);
            else
                BuildingManager.Instance.myHomeBuildings_[2].ToggleNotification(true);

            if (Player.earthStoneBuildSlot > 1)
            {
                if (BuildTimer.earthstone1_Build_Time >= 0)
                    BuildingManager.Instance.myHomeBuildings_[2].ToggleNotification(false);
                else
                    BuildingManager.Instance.myHomeBuildings_[2].ToggleNotification(true);
            }
        }
        if (thisName.ToLower().Contains("range"))
        {
            if (BuildTimer.windStone0_Build_Time >=0)
                BuildingManager.Instance.myHomeBuildings_[3].ToggleNotification(false);
            else
                BuildingManager.Instance.myHomeBuildings_[3].ToggleNotification(true);

            if (Player.windStoneBuildSlots > 1)
            {
                if (BuildTimer.windStone1_Build_Time >= 0)
                    BuildingManager.Instance.myHomeBuildings_[3].ToggleNotification(false);
                else
                    BuildingManager.Instance.myHomeBuildings_[3].ToggleNotification(true);
            }
        }
        if (thisName.ToLower().Contains("guild"))
        {
            if (BuildTimer.flameStone0_Build_Time >= 0)
                BuildingManager.Instance.myHomeBuildings_[4].ToggleNotification(false);
            else
                BuildingManager.Instance.myHomeBuildings_[4].ToggleNotification(true);

            if (Player.flameStoneBuildSlot > 1)
            {
                if (BuildTimer.flameStone1_Build_Time >= 0)
                    BuildingManager.Instance.myHomeBuildings_[4].ToggleNotification(false);
                else
                    BuildingManager.Instance.myHomeBuildings_[4].ToggleNotification(true);
            }
        }
            
    }

    public void ToggleStoneCompletNotification()
    {
        if (name.ToLower().Contains("bunker"))
        {
            if (BuildTimer.isBuildingStone[0] == true && BuildTimer.earthstone0_Build_Time <= 0)
                stoneCompleteNotificationObj[0].SetActive(true);
            else
                stoneCompleteNotificationObj[0].SetActive(false);

            if (BuildTimer.isBuildingStone[3] == true && BuildTimer.earthstone1_Build_Time <= 0)
                stoneCompleteNotificationObj[1].SetActive(true);
            else
                stoneCompleteNotificationObj[1].SetActive(false);
        }
        if (name.ToLower().Contains("range"))
        {
            if (BuildTimer.isBuildingStone[1] == true && BuildTimer.windStone0_Build_Time <= 0)
                stoneCompleteNotificationObj[0].SetActive(true);
            else
                stoneCompleteNotificationObj[0].SetActive(false);

            if (BuildTimer.isBuildingStone[4] == true && BuildTimer.windStone1_Build_Time <= 0)
                stoneCompleteNotificationObj[1].SetActive(true);
            else
                stoneCompleteNotificationObj[1].SetActive(false);
        }
        if (name.ToLower().Contains("guild"))
        {
            if (BuildTimer.isBuildingStone[2] == true && BuildTimer.flameStone0_Build_Time <= 0)
                stoneCompleteNotificationObj[0].SetActive(true);
            else
                stoneCompleteNotificationObj[0].SetActive(false);

            if (BuildTimer.isBuildingStone[5] == true && BuildTimer.flameStone1_Build_Time <= 0)
                stoneCompleteNotificationObj[1].SetActive(true);
            else
                stoneCompleteNotificationObj[1].SetActive(false);
        }
    }

    void EvolvableInfoDisplay(bool isEvolvable)
    {
        unableToEvoleDisplayTexts.SetActive(!isEvolvable);
        ableToEvolveText.SetActive(isEvolvable);
        evolve_Button.interactable = isEvolvable;
    }


    bool unitEvolvable(bool stoneCheck, bool battleCheck)
    {
        if (stoneCheck == false || battleCheck == false)
            return false;
        return true;
    }

	public override void SpeedUpOrMoveToBuilding()
	{
		thisBuilding = name;
		base.SpeedUpOrMoveToBuilding();
	}

	
}
