﻿using UnityEngine;
using UnityEngine.UI;

public class UpgradeRequirements : MonoBehaviour
{
    public Text meleeLvl, rangeLvl, magicLvl, treasuryLvl, mine1Lvl, mine2Lvl, mine3Lvl, mine4Lvl, mine5Lvl, prepHalLvl, townHallLvl,
    reputationNeeded;

    private void OnEnable()
    {
        //if the castle is still at lvl 0
        if (Player.castleLevel < 1)
        {
            if (Player.bunkerLv < 6)
                meleeLvl.text = string.Format("<color=#FF0000> {0} / 6</color>", Player.bunkerLv);
            else
                meleeLvl.text = string.Format("<color=#00FF00> {0} / 6</color>", Player.bunkerLv);


            if (Player.rangeLv < 6)
                rangeLvl.text = string.Format("<color=#FF0000> {0} / 6</color>", Player.rangeLv);
            else
                rangeLvl.text = string.Format("<color=#00FF00> {0} / 6</color>", Player.rangeLv);


            if (Player.guildLv < 6)
                magicLvl.text = string.Format("<color=#FF0000> {0} / 6</color>", Player.guildLv);
            else
                magicLvl.text = string.Format("<color=#00FF00> {0} / 6</color>", Player.guildLv);


            if (Player.recruitmentHall_Lvl < 6)
                prepHalLvl.text = string.Format("<color=#FF0000> {0} / 6</color>", Player.recruitmentHall_Lvl);
            else
                prepHalLvl.text = string.Format("<color=#00FF00> {0} / 6</color>", Player.recruitmentHall_Lvl);


            if (Player.mines[4] < 6)
                mine1Lvl.text = string.Format("<color=#FF0000> {0} / 6</color>", Player.mines[4]);
            else
                mine1Lvl.text = string.Format("<color=#00FF00> {0} / 6</color>", Player.mines[4]);

            if (Player.mines[2] == 0)
                mine2Lvl.text = "- Gold Mine Locked -";
            else
            {
                if (Player.mines[2] >= 1)
                    mine2Lvl.text = string.Format("<color=#FF0000> {0} / 6</color>", Player.mines[2]);
                else
                    mine2Lvl.text = string.Format("<color=#00FF00> {0} / 6</color>", Player.mines[2]);
            }

            if (Player.mines[0] == 0)
                mine3Lvl.text = "- Gold Mine Locked -";
            else
            {
                if (Player.mines[0] < 6)
                    mine3Lvl.text = string.Format("<color=#FF0000> {0} / 6</color>", Player.mines[0]);
                else
                    mine3Lvl.text = string.Format("<color=#00FF00> {0} / 6</color>", Player.mines[0]);
            }

            if (Player.mines[1] == 0)
                mine4Lvl.text = "- Gold Mine Locked -";
            else
            {
                if (Player.mines[1] < 6)
                    mine4Lvl.text = string.Format("<color=#FF0000> {0} / 6</color>", Player.mines[1]);
                else
                    mine4Lvl.text = string.Format("<color=#00FF00> {0} / 6</color>", Player.mines[1]);
            }

            if (Player.mines[3] == 0)
                mine5Lvl.text = "- Gold Mine Locked -";
            else
            {
                if (Player.mines[3] < 6)
                    mine5Lvl.text = string.Format("<color=#FF0000> {0} / 6</color>", Player.mines[3]);
                else
                    mine5Lvl.text = string.Format("<color=#00FF00> {0} / 6</color>", Player.mines[3]);
            }


            if (Player.treasuryLvl < 6)
                treasuryLvl.text = string.Format("<color=#FF0000> {0} / 6</color>", Player.treasuryLvl);
            else
                treasuryLvl.text = string.Format("<color=#00FF00> {0} / 6</color>", Player.treasuryLvl);


            if (Player.townHallLevel < 6)
                townHallLvl.text = string.Format("<color=#FF0000> {0} / 6</color>", Player.townHallLevel);
            else
                townHallLvl.text = string.Format("<color=#00FF00> {0} / 6</color>", Player.townHallLevel);

        }
        // if the castle is above level 0
        else if (Player.castleLevel >= 1)
        {
            // but not at the max level yet
            if (Player.castleLevel < Player.castleMaxLevel)
            {
                if (Player.bunkerLv < (Player.castleLevel * 3) + 3)
                    meleeLvl.text = string.Format("<color=#FF0000> {0} / {1}</color>", Player.bunkerLv, (Player.castleLevel * 3) + 3);
                else if (Player.bunkerLv >= (Player.castleLevel * 3) + 3)
                    meleeLvl.text = string.Format("<color=#00FF00> {0} / {1}</color>", Player.bunkerLv, (Player.castleLevel * 3) + 3);


                if (Player.rangeLv < (Player.castleLevel * 3) + 3)
                    rangeLvl.text = string.Format("<color=#FF0000> {0} / {1}</color>", Player.rangeLv, (Player.castleLevel * 3) + 3);
                else
                    rangeLvl.text = string.Format("<color=#00FF00> {0} / {1}</color>", Player.rangeLv, (Player.castleLevel * 3) + 3);


                if (Player.guildLv < (Player.castleLevel * 3) + 3)
                    magicLvl.text = string.Format("<color=#FF0000> {0} / {1}</color>", Player.guildLv, (Player.castleLevel * 3) + 3);
                else
                    magicLvl.text = string.Format("<color=#00FF00> {0} / {1}</color>", Player.guildLv, (Player.castleLevel * 3) + 3);


                if (Player.recruitmentHall_Lvl < (Player.castleLevel * 3) + 3)
                    prepHalLvl.text = string.Format("<color=#FF0000> {0} / {1}</color>", Player.recruitmentHall_Lvl, (Player.castleLevel * 3) + 3);
                else
                    prepHalLvl.text = string.Format("<color=#00FF00> {0} / {1}</color>", Player.recruitmentHall_Lvl, (Player.castleLevel * 3) + 3);


                if (Player.mines[4] < (Player.castleLevel * 3) + 3)
                    mine1Lvl.text = string.Format("<color=#FF0000> {0} / {1}</color>", Player.mines[4], (Player.castleLevel * 3) + 3);
                else
                    mine1Lvl.text = string.Format("<color=#00FF00> {0} / {1}</color>", Player.mines[4], (Player.castleLevel * 3) + 3);

                if (Player.mines[2] == 0)
                    mine2Lvl.text = "- Gold Mine Locked -";
                else
                {
                    if (Player.mines[2] < (Player.castleLevel * 3) + 3)
                        mine2Lvl.text = string.Format("<color=#FF0000> {0} / {1}</color>", Player.mines[2], (Player.castleLevel * 3) + 3);
                    else
                        mine2Lvl.text = string.Format("<color=#00FF00> {0} / {1}</color>", Player.mines[2], (Player.castleLevel * 3) + 3);
                }


                if (Player.mines[0] == 0)
                    mine3Lvl.text = "- Gold Mine Locked -";
                else
                {
                    if (Player.mines[0] < (Player.castleLevel * 3) + 3)
                        mine3Lvl.text = string.Format("<color=#FF0000> {0} / {1}</color>", Player.mines[0], (Player.castleLevel * 3) + 3);
                    else
                        mine3Lvl.text = string.Format("<color=#00FF00> {0} / {1}</color>", Player.mines[0], (Player.castleLevel * 3) + 3);
                }


                if (Player.mines[1] == 0)
                    mine4Lvl.text = "- Gold Mine Locked -";
                else
                {
                    if (Player.mines[1] < (Player.castleLevel * 3) + 3)
                        mine4Lvl.text = string.Format("<color=#FF0000> {0} / {1}</color>", Player.mines[1], (Player.castleLevel * 3) + 3);
                    else
                        mine4Lvl.text = string.Format("<color=#00FF00> {0} / {1}</color>", Player.mines[1], (Player.castleLevel * 3) + 3);
                }


                if (Player.mines[3] == 0)
                    mine5Lvl.text = "- Gold Mine Locked -";
                else
                {
                    if (Player.mines[3] < (Player.castleLevel * 3) + 3)
                        mine5Lvl.text = string.Format("<color=#FF0000> {0} / {1}</color>", Player.mines[3], (Player.castleLevel * 3) + 3);
                    else
                        mine5Lvl.text = string.Format("<color=#00FF00> {0} / {1}</color>", Player.mines[3], (Player.castleLevel * 3) + 3);
                }


                if (Player.treasuryLvl < (Player.castleLevel * 3) + 3)
                    treasuryLvl.text = string.Format("<color=#FF0000> {0} / {1}</color>", Player.treasuryLvl, (Player.castleLevel * 3) + 3);
                else
                    treasuryLvl.text = string.Format("<color=#00FF00> {0} / {1}</color>", Player.treasuryLvl, (Player.castleLevel * 3) + 3);


                if (Player.townHallLevel < (Player.castleLevel * 3) + 3)
                    townHallLvl.text = string.Format("<color=#FF0000> {0} / {1}</color>", Player.townHallLevel, (Player.castleLevel * 3) + 3);
                else
                    townHallLvl.text = string.Format("<color=#00FF00> {0} / {1}</color>", Player.townHallLevel, (Player.castleLevel * 3) + 3);
            }
            // if player hits the castle cap
            else 
            {
                if (Player.bunkerLv < (Player.castleLevel * 3) + 3)
                    meleeLvl.text = string.Format("<color=#FF0000> {0} / MAXED</color>", Player.bunkerLv);
                else if (Player.bunkerLv >= (Player.castleLevel * 3) + 3)
                    meleeLvl.text = string.Format("<color=#00FF00> {0} / MAXED</color>", Player.bunkerLv, (Player.castleLevel * 3) + 3);


                if (Player.rangeLv < (Player.castleLevel * 3) + 3)
                    rangeLvl.text = string.Format("<color=#FF0000> {0} / MAXED</color>", Player.rangeLv, (Player.castleLevel * 3) + 3);
                else
                    rangeLvl.text = string.Format("<color=#00FF00> {0} / MAXED</color>", Player.rangeLv, (Player.castleLevel * 3) + 3);


                if (Player.guildLv < (Player.castleLevel * 3) + 3)
                    magicLvl.text = string.Format("<color=#FF0000> {0} / MAXED</color>", Player.guildLv, (Player.castleLevel * 3) + 3);
                else
                    magicLvl.text = string.Format("<color=#00FF00> {0} / MAXED</color>", Player.guildLv, (Player.castleLevel * 3) + 3);


                if (Player.recruitmentHall_Lvl < (Player.castleLevel * 3) + 3)
                    prepHalLvl.text = string.Format("<color=#FF0000> {0} / MAXED</color>", Player.recruitmentHall_Lvl, (Player.castleLevel * 3) + 3);
                else
                    prepHalLvl.text = string.Format("<color=#00FF00> {0} / MAXED</color>", Player.recruitmentHall_Lvl, (Player.castleLevel * 3) + 3);


                if (Player.mines[4] < (Player.castleLevel * 3) + 3)
                    mine1Lvl.text = string.Format("<color=#FF0000> {0} / MAXED</color>", Player.mines[4], (Player.castleLevel * 3) + 3);
                else
                    mine1Lvl.text = string.Format("<color=#00FF00> {0} / MAXED</color>", Player.mines[4], (Player.castleLevel * 3) + 3);

                if (Player.mines[2] == 0)
                    mine2Lvl.text = "- Gold Mine Locked -";
                else
                {
                    if (Player.mines[2] < (Player.castleLevel * 3) + 3)
                        mine2Lvl.text = string.Format("<color=#FF0000> {0} / MAXED</color>", Player.mines[2], (Player.castleLevel * 3) + 3);
                    else
                        mine2Lvl.text = string.Format("<color=#00FF00> {0} / MAXED</color>", Player.mines[2], (Player.castleLevel * 3) + 3);
                }


                if (Player.mines[0] == 0)
                    mine3Lvl.text = "- Gold Mine Locked -";
                else
                {
                    if (Player.mines[0] < (Player.castleLevel * 3) + 3)
                        mine3Lvl.text = string.Format("<color=#FF0000> {0} / MAXED</color>", Player.mines[0], (Player.castleLevel * 3) + 3);
                    else
                        mine3Lvl.text = string.Format("<color=#00FF00> {0} / MAXED</color>", Player.mines[0], (Player.castleLevel * 3) + 3);
                }


                if (Player.mines[1] == 0)
                    mine4Lvl.text = "- Gold Mine Locked -";
                else
                {
                    if (Player.mines[1] < (Player.castleLevel * 3) + 3)
                        mine4Lvl.text = string.Format("<color=#FF0000> {0} / MAXED</color>", Player.mines[1], (Player.castleLevel * 3) + 3);
                    else
                        mine4Lvl.text = string.Format("<color=#00FF00> {0} / MAXED</color>", Player.mines[1], (Player.castleLevel * 3) + 3);
                }


                if (Player.mines[3] == 0)
                    mine5Lvl.text = "- Gold Mine Locked -";
                else
                {
                    if (Player.mines[3] < (Player.castleLevel * 3) + 3)
                        mine5Lvl.text = string.Format("<color=#FF0000> {0} / MAXED</color>", Player.mines[3], (Player.castleLevel * 3) + 3);
                    else
                        mine5Lvl.text = string.Format("<color=#00FF00> {0} / MAXED</color>", Player.mines[3], (Player.castleLevel * 3) + 3);
                }


                if (Player.treasuryLvl < (Player.castleLevel * 3) + 3)
                    treasuryLvl.text = string.Format("<color=#FF0000> {0} / MAXED</color>", Player.treasuryLvl, (Player.castleLevel * 3) + 3);
                else
                    treasuryLvl.text = string.Format("<color=#00FF00> {0} / MAXED</color>", Player.treasuryLvl, (Player.castleLevel * 3) + 3);


                if (Player.townHallLevel < (Player.castleLevel * 3) + 3)
                    townHallLvl.text = string.Format("<color=#FF0000> {0} / MAXED</color>", Player.townHallLevel, (Player.castleLevel * 3) + 3);
                else
                    townHallLvl.text = string.Format("<color=#00FF00> {0} / MAXED</color>", Player.townHallLevel, (Player.castleLevel * 3) + 3);
            }
        }



        if (Player.castleLevel < Player.castleMaxLevel)
        {
            if (Player.reputation < Player.Instance.repRequired[Player.castleLevel])
                reputationNeeded.text = string.Format("<color=#FF0000> {0} / {1}</color>", Player.reputation, Player.Instance.repRequired[Player.castleLevel]);
            else
                reputationNeeded.text = string.Format("<color=#00FF00> {0} / {1}</color>", Player.reputation, Player.Instance.repRequired[Player.castleLevel]);
        }
        else if (Player.castleLevel == Player.castleMaxLevel)
        {
            reputationNeeded.text = string.Format("Castle Level Maxed");
        }
    }
}
