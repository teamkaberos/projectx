﻿using UnityEngine;
using UnityEngine.UI;

public class RHall_Update : BaseUpdate_Popoup 
{
    //	public Button groupBuildButton;

    public Button assign_Troop_Button;
    bool tutorial_Troop_Assigned;

    public override void OnEnable()
    {
        if (Player.tutorial < 3)
        {
            Disable_Tutorial_Troop_Assign_Button();
        }
        if (Player.tutorial == 3)
        {
            BuildTimer.buildingUnqueued += Enable_Troop_Assign_Button;

            TroopAssign_Descrip.troopsAssigned += Disable_Troop_Assign_Button;
        }

        base.OnEnable();
    }

    public void Enable_Troop_Assign_Button()
    {
        assign_Troop_Button.interactable = true;

		Unsub();
    }

    public void Disable_Troop_Assign_Button()
    {
        assign_Troop_Button.interactable = false;
        tutorial_Troop_Assigned = true;
    }

    public override void GetUpdated()
	{
        currentLvl.text = string.Format("Preparation Hall Lv{0}", Player.recruitmentHall_Lvl);

		// When player has queued max amount possible
		// This is the queued amount display text
		if (BuildTimer.queuedMyGroup.Count == Player.recruitment_Limit)
			generationRate.color = Color.red;
		else
			generationRate.color = Color.white;

		generationRate.text = string.Format("{0} / {1}" , BuildTimer.queuedMyGroup.Count, Player.recruitment_Limit);

		// Check if player has enough gold to build groups
		if (Player.gold < Player.groupCost)
		{
//			groupBuildButton.interactable = false;
			currentValue.text = "Insufficient gold";
			currentValue.color = Color.red;
		}
		else
		{
//			groupBuildButton.interactable = true;
			currentValue.text = string.Format("{0}g / group", Player.groupCost);
			currentValue.color = Color.white;
		}

		// When player has max possible groups in barracks already
		// including those that are queued
		if ((Player.myGroups+BuildTimer.unitMyGroup.Count)  >= 99)
		{
//			groupBuildButton.interactable = false;
			currentValue.color = Color.red;
			currentValue.text = "No more room..";
		}
		else if ((Player.myGroups+BuildTimer.unitMyGroup.Count) < 99)
		{
//			groupBuildButton.interactable = true;
			currentValue.color = Color.white;
			currentValue.text = string.Format("{0}g / group", Player.groupCost);
		}


        if (Player.tutorial == 3 & Player.recruitmentHall_Lvl == 0)
            assign_Troop_Button.interactable = false;

        if (Player.tutorial == 3 & Player.recruitmentHall_Lvl == 1 && tutorial_Troop_Assigned == true)
            assign_Troop_Button.interactable = false;

//		if (BuildTimer.queuedBuilding.Count >= 1)
//		{
//			if (BuildTimer.queuedBuilding[0].Contains("Recruitment"))
//				groupBuildButton.interactable = false;
//			else
//				groupBuildButton.interactable = true;
//		}
	}

    public void Disable_Tutorial_Troop_Assign_Button()
    {
        assign_Troop_Button.interactable = false;
    }

	public override void SpeedUpOrMoveToBuilding()
	{
		thisBuilding = name;
		base.SpeedUpOrMoveToBuilding();
	}

    void Unsub()
    {
        TroopAssign_Descrip.troopsAssigned -= Disable_Troop_Assign_Button;
        BuildTimer.buildingUnqueued -= Enable_Troop_Assign_Button;
    }

    public override void OnDestroy()
    {
        Unsub();
        base.OnDestroy();
    }

}
