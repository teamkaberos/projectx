﻿using UnityEngine;
using UnityEngine.EventSystems;

public class ResourceGenerate : HomeBuildings
{
	public delegate void OnTutorialTapped();
	public static event OnTutorialTapped TutorialBuildingTapped;

    public delegate void OnTutorialTapped_(bool isTapped);
    public static event OnTutorialTapped_ TutorialBuildingTapped_;

    public delegate void OnResourceCollected(string rss, int rssCollected);
    public event OnResourceCollected ResourceAdded;

    public int displayGeneratorNo;          //The number to identify which RSS generator this is for players
	public string generatorType;				//To be derived from PlayerS to determine which generator it is
	public int generatorLvl;							//To determine how much THIS generator can generate and store
    public float generationRate;						//How much resource generated over time
	float generationIntervals;						//For if player stays on screen
	public int generatorCap;							//How much resource can it store up to
    public float currentResourceValue;		//How much resource it has at the current moment

	public int generatorNo;							//for saving purposes;
	public bool isUpgrading;						//use to disable generation when upgrading;							

	public GameObject[] levelToDisplay;

	public GameObject floatingCoin;		//overhead coin display

    public GameObject goblinsHolder;

    void Update()
    {
        //Stop generation when structure is upgrading
        if (!isUpgrading && generatorLvl >= 1)
        {
            // ===--- THE RESOURCE GENERATION PROCESS ---=== //
            generationIntervals += Time.deltaTime;
            if (generationIntervals >= 3)
            {
                Generate();
            }
        }
    }


	public void CollectResources()
	{
		// --- Gold Collection
		int gSpaceLeft = Player.treasury_Storage_Cap - Player.gold;		//Check the amount of space left to store gold
		if (currentResourceValue <= gSpaceLeft)				                    //If there is space
		{
            Player.gold += (int)currentResourceValue;				        	//Add gold
            ResourceAdded("gold", (int)currentResourceValue);
            currentResourceValue = 0;									                //and clear resource in this generator
		}
		else 																					        //other wise
        {
            //if (gSpaceLeft > 0)
                //Player.Instance.StoreAllData();
			currentResourceValue -= gSpaceLeft;					                //current value deducts space left from treasury
            ResourceAdded("gold", gSpaceLeft);
            Player.gold += gSpaceLeft;													//and adds it to gold amount
		}

        
        if (currentResourceValue < (generatorCap/10))
		{
			if (floatingCoin.activeInHierarchy)
				floatingCoin.SetActive(false);
		}

        DataManager.Instance.PrepareDataForSparksPushing();
        GameSparksManager.Instance.SavePlayerExpendables(GameSparksManager.playerId);
        GameSparksManager.Instance.SaveMine(generatorNo);
		UIManager.Instance.UpdateCurrencyInfo();
	}


	public override void OnPointerClick(PointerEventData eventData)
	{
        SelectThisBuilding();
	}
    public override void SelectThisBuilding()
    {
        if (floatingCoin.activeInHierarchy)
        {
            CollectResources();
            if (Player.gold != Player.treasury_Storage_Cap)
                return;
        }

        DataManager.Instance.PrepareDataForSparksPushing();
        Player.Instance.Store_The_Last_RSS_Generator_Player_Tapped(generatorNo, generatorType);
        small_Popup_Info.GetComponent<ResourceUpdate>().GoldValueChanged();

        if (Player.tutorial == 0)
        {
            TutorialBuildingTapped();
            TutorialBuildingTapped_(true);
        }

        base.SelectThisBuilding();
    }


    // ========= VALUE CHECKS ========= \\
    void Check_If_RSS_Generator_Is_Filled()
    {
        if (currentResourceValue >= generatorCap)
            currentResourceValue = generatorCap;
        Show_Hide_FloatingCoin();
    }
    void NegativeValueCheck()
    {
        if (currentResourceValue < 0)
            currentResourceValue = 0;
    }


    // ========= RSS GENERATOR BEHAVIOUR =========\\
    void Generate()
	{
		//Stop generator from adding if generator is maxed out
		if (currentResourceValue <= generatorCap)
		{
			currentResourceValue +=generationRate/1200f;
			Check_If_RSS_Generator_Is_Filled();
			generationIntervals = 0;
			Show_Hide_FloatingCoin();
		}
	}
	void Show_Hide_FloatingCoin()
	{
		if (!floatingCoin.activeInHierarchy && currentResourceValue >= generatorCap*0.1f)
		{
			if (generatorLvl >= 1)
				floatingCoin.SetActive(true);
		}
        if (upgradeInProgress.activeInHierarchy)
        {
            currentResourceValue = 0;
            floatingCoin.SetActive(false);
        }
	}
    public void Update_Current_Value(long timeDifference)
	{
		int valueAdded = Mathf.FloorToInt((Mathf.FloorToInt(timeDifference/3)) * (generationRate/1200f));
		currentResourceValue += valueAdded;

		Check_If_RSS_Generator_Is_Filled();
        NegativeValueCheck();
	}

	public void UpgradeRSSBuilding(bool collectResource)
	{
		isUpgrading = true;
		if(collectResource)
			CollectResources();
		currentResourceValue = 0;
		generationIntervals = 0;
	}
	public void UpgradeCompleteRSSBuilding()
	{
		isUpgrading = false;
	}

    // ========= UPDATE 3D MESH ON HOME SCREEN ========\\
	public void Disable_All_Levels_Of_Mesh()
	{
		for (int x = 0; x < levelToDisplay.Length; x++)
		{
			levelToDisplay[x].SetActive(false);
		}
	}
	public virtual void Update_RSS_Building_Model()
	{
        if (generatorLvl == 0)
            return;
        if (generatorLvl >= 1 & generatorLvl < 3)
        {
            levelToDisplay[0].SetActive(true);
            return;
        }
        //Begin for loop to check what is going to be displayed
        for (int x = 0; x < levelToDisplay.Length; x++)
        {
            // For every 3 levels of this building
            if (generatorLvl >= 3 * (x + 1))
            {
                //Display the corresponding mesh
                levelToDisplay[x].SetActive(true);
            }
        }
	}
    public void DisplayGoblins()
    {
        if (generatorLvl >= 1)
        goblinsHolder.SetActive(true);
    }
    public override void ShowScaffold()
    {
        base.ShowScaffold();

        currentResourceValue = 0;

        floatingCoin.SetActive(false);
    }


    #region Return Values for UI display
    public string CurrentRSSLvl()
    {
        string currentLvl = string.Format("Gold Mine Lv{0}", generatorLvl);
        return currentLvl;
    }
    public string CurrentAvailableAmount()
    {
        if (isUpgrading)
            currentResourceValue = 0;

        string currentAmount = string.Format("{0:n0} / {1:n0}", currentResourceValue, generatorCap);

        if (generatorLvl < 1)
            currentAmount = "0 / 0";

        return currentAmount;
    }
    public int FilledPercentage()
    {
        if (generatorCap == 0)
            return 0;
        
        int fillAmount = (int)((currentResourceValue * 100) / generatorCap);
        return fillAmount;
    }
    public string currentGenerationRate()
    {
        string genRate = "0 / hour";
        if (Player.mines[Player.mine_That_Player_Last_Tapped_On] > 0)
            genRate = string.Format("{0} / hour", generationRate);
        return genRate;
    }
    #endregion


}
