﻿using UnityEngine;

public class BuildingCompleteSpin : MonoBehaviour 
{
    public float speed = 60f;

	void Update()
	{
        Quaternion rotU = Quaternion.AngleAxis(speed * Time.deltaTime, Vector3.up);

        transform.rotation = transform.rotation * rotU;
	}
}
