﻿using UnityEngine;
using UnityEngine.UI;

public class ProgressBarHandler : MonoBehaviour 
{
	delegate void OnBuildingComplete(string buildingType);
	OnBuildingComplete onbuildingComplete;

	public GameObject theProgressBar;							// Hide and Display progress bar
	public Button upgradeButton;									// To be disabled if there is a building upgrading in progress
    public Button speedUpButton;
	public GameObject completeButton;							// Button where the COMPLETE building appears
	public GameObject upgradeBlocker;

	public Text upgrade_in_Progress_Informer;			        //Text to inform players of currently being upgraded building

	public Button unitsAssignButton;                             //For prep halls

    public Text goldMineBuild;

	// Function only called when a player completes building a structure
	public void OnBuildingCompleted(string buildingType)
	{
		onbuildingComplete = Complete_Building;
		onbuildingComplete (buildingType);
		UIManager.Instance.Hide_SpeedUp_PopUp();
		UIManager.Instance.Disable_SpeedUp_Buttons();
	}
	void Complete_Building(string buildType)
	{
		//check first if the current pop up is a RSS building
		{
			// If this pop up is a mine pop up
			if (buildType == "Mine")
			{
				if (Player.mine_That_Player_Last_Tapped_On == Player.mine_Upgrading_Now)
				{
					completeButton.SetActive(true);
					return;
				}
			}
			else
			{
				completeButton.SetActive(true);
				return;
			}
		}
	}
    void TutorialDisable()
    {
        upgradeButton.interactable = false;
        speedUpButton.interactable = false;
        completeButton.GetComponent<Button>().interactable = false;
    }
	public void Check_If_Building_Upgrade_Available()
	{
		//TroopsCheck();

		Inform_Upgrade_In_Progress();

        if (Player.tutorial < Player.tutorialCap)
        {
            TutorialDisable();
            return;
        }

		if (name.Contains("Mill"))
		{
			// IF CASTLE LEVEL REQUIREMENT NOT MET
			if (Player.mills[Player.mill_That_Player_Last_Tapped_On] >= (Player.castleLevel *3) || Player.millsCost[Player.mill_That_Player_Last_Tapped_On] > Player.gold)
			{
				upgradeButton.interactable = false;
				return;
			}
			upgradeButton.interactable = true;
		}
		else if (name.Contains("Mine"))
        {
			//IF CASTLE LEVEL REQUIREMENT NOT MET
            if (Player.tutorial < Player.tutorialCap && Player.mine_That_Player_Last_Tapped_On == 4 && Player.mines[4] <= 1)
            {
                upgradeButton.interactable = false;
                completeButton.GetComponent<Button>().interactable = false;
                return;
            }
            if (Player.mines[Player.mine_That_Player_Last_Tapped_On] >= Player.castleLevel*3+3)
            {
                upgradeButton.interactable = false;
                return;
            }
			if (Player.minesCost[Player.mine_That_Player_Last_Tapped_On] > Player.gold || Player.mines[Player.mine_That_Player_Last_Tapped_On] >= (Player.castleLevel * 3 + 3))
			{
				upgradeButton.interactable = false;
				return;
			}
            if(MineUnlockCounterReached() & Player.mines[Player.mine_That_Player_Last_Tapped_On] <= 0)
			{
				upgradeButton.interactable = false;
				return;
			}	
			if (Player.mines[Player.mine_That_Player_Last_Tapped_On] >= Player.rssBuildingLvCap)
			{
				upgradeButton.interactable = false;
				return;
			}  
			upgradeButton.interactable = true;
		}
		else if (name.Contains("Bunker"))
		{
			if (Player.bunkerCost > Player.gold)
			{
				upgradeButton.interactable = false;
				return;
			}
			if (Player.bunkerLv >= Player.castleLevel * 3 + 3)
			{
				upgradeButton.interactable = false;
				return;
			}
			if (Player.bunkerLv >= Player.barracksLvlCap)
			{
				upgradeButton.interactable = false;
				return;
			}
            if (Player.tutorial == 2 & Player.bunkerLv == 1)
            {
                upgradeButton.interactable = false;
                return;
            }
			upgradeButton.interactable = true;
		}
		else if (name.Contains("Guild"))
		{
			if (Player.guildCost > Player.gold)
			{
				upgradeButton.interactable = false;
				return;
			}
			if (Player.guildLv >= (Player.castleLevel * 3 + 3))
			{
				upgradeButton.interactable = false;
				return;
			}
			if (Player.guildLv >= Player.barracksLvlCap)
			{
				upgradeButton.interactable = false;
				return;
			}
            if (Player.tutorial == 2 && Player.guildLv == 1)
            {
                upgradeButton.interactable = false;
                return;
            }
			upgradeButton.interactable = true;
		}
		else if (name.Contains("Range"))
		{
			if(Player.rangeCost > Player.gold)
			{
				upgradeButton.interactable = false;
				return;
			}
			if(Player.rangeLv >= (Player.castleLevel * 3 + 3))
			{
				upgradeButton.interactable = false;
				return;
			}
			if (Player.rangeLv >= Player.barracksLvlCap)
			{
				upgradeButton.interactable = false;
				return;
			}
            if (Player.tutorial == 2 & Player.rangeLv == 1)
            {
                upgradeButton.interactable = false;
                return;
            }
			upgradeButton.interactable = true;
		}
		else if (name.Contains("RecruitmentHall"))
		{
			//RHall can only be upgraded if all 3 barracks are higher lvl than it
			if (Player.bunkerLv <= Player.recruitmentHall_Lvl)
			{
				this.upgradeButton.interactable = false;
				return;
			}
			if (Player.guildLv <= Player.recruitmentHall_Lvl)
			{
				this.upgradeButton.interactable = false;
				return;
			}
			if (Player.rangeLv <=Player.recruitmentHall_Lvl)
			{
				this.upgradeButton.interactable = false;
				return;
			}

			//or if there are no troops being built at the moment
			if (BuildTimer.queuedMyGroup.Count >= 1)
			{
				this.upgradeButton.interactable = false;
				return;
			}

			if (Player.recruitmentHall_Lvl >= (Player.castleLevel *3 + 3))
			{
				this.upgradeButton.interactable = false;
				return;
			}
			if (Player.recruitmentHall_Cost > Player.gold)
			{
				upgradeButton.interactable = false;
				return;
			}

			//if rhall is at max lvl
			if (Player.recruitmentHall_Lvl >= Player.recruitmentHallLvlCap)
			{
				upgradeButton.interactable = false;
				return;
			}
            if (Player.tutorial == 3 & Player.recruitmentHall_Lvl == 1)
            {
                upgradeButton.interactable = false;
                unitsAssignButton.interactable = false;
                return;
            }
			else 
				this.upgradeButton.interactable = true;
		}
		else if (name.Contains("Treasury"))
		{
            if (Player.tutorial < Player.tutorialCap & Player.treasuryLvl <= 1)
            {
                speedUpButton.interactable = false;
                upgradeButton.interactable = false;
                return;
            }
			if (Player.treasuryLvl >= (Player.castleLevel * 3 + 3))
			{
				upgradeButton.interactable = false;
				return;
			}
			if( Player.treasuryCost > Player.gold)
			{
				upgradeButton.interactable = false;
				return;
			}
			if (Player.treasuryLvl >= Player.treasuryLvlCap)
			{
				upgradeButton.interactable = false;
				return;
			}
			upgradeButton.interactable = true;
		}
		else if (name.Contains("Forge"))
		{
			if (Player.castleLevel < (Player.forgeLevel * 2) +2 )
			{
				upgradeButton.interactable = false;
				return;
			}
			if (Player.forge_UpgradeCost > Player.gold)
			{
				upgradeButton.interactable = false;
				return;
			}
			if (Player.forgeLevel >= Player.forgeCap)
			{
				upgradeButton.interactable = false;
				return;
			}
			upgradeButton.interactable = true;
		}
		else if (name.Contains("TownHall"))
		{
            if (Player.tutorial < Player.tutorialCap)
            {
                upgradeButton.interactable = false;
                return;
            }
			for (int x = 0; x < 5; x++)
			{
				if (Player.mines[x] <= Player.townHallLevel && Player.mines[x] != 0)
				{
					upgradeButton.interactable = false;
					return;
				}
			}
			if (Player.bunkerLv <= Player.townHallLevel)
			{
				upgradeButton.interactable = false;
				return;
			}
			if (Player.rangeLv <= Player.townHallLevel)
			{
				upgradeButton.interactable = false;
				return;
			}
			if (Player.guildLv <= Player.townHallLevel)
			{
				upgradeButton.interactable = false;
				return;
			}
			if (Player.treasuryLvl <= Player.townHallLevel)
			{
				upgradeButton.interactable = false;
				return;
			}
			if (Player.recruitmentHall_Lvl <= Player.townHallLevel)
			{
				upgradeButton.interactable = false;
				return;
			}
			if (Player.townHallLevel >= (Player.castleLevel * 3 + 3))
			{
				upgradeButton.interactable = false;
				return;
			}
			if (Player.townHallCost > Player.gold)
			{
				upgradeButton.interactable = false;
				return;
			}
			if (Player.townHallLevel >= Player.townHallLevelCap)
			{
				upgradeButton.interactable = false;
				return;
			}
			upgradeButton.interactable = true;
			
		}
	}

    bool MineUnlockCounterReached()
    {
        int unlockedNow = 0;
        for (int x = 0; x < Player.mines.Length; x++)
        {
            if (Player.mines[x] >= 1)
                unlockedNow += 1;
        }

        if (unlockedNow >= Player.resourceBuildings_Unlock_Counter[Player.castleLevel])
            return true;
        return false;
    }

	public void Inform_Upgrade_In_Progress()
	{
		if (BuildTimer.queuedBuilding.Count >= 1)
		{
			upgrade_in_Progress_Informer.gameObject.SetActive(true);
		
			// If this isnt the building that is being upgraded
			if(!name.Contains(BuildTimer.queuedBuilding[0]))
			{
                upgrade_in_Progress_Informer.text = NameConverting(BuildTimer.queuedBuilding[0]) + " is currently being upgraded";
			}

			//else if it is the one being upgraded
			else if(name.Contains(BuildTimer.queuedBuilding[0]))
			{
				upgrade_in_Progress_Informer.text = "This building is currently being upgraded";

                //check if its a gold mine
				if (name.Contains("Mine"))
				{
					if (Player.mine_That_Player_Last_Tapped_On != Player.mine_Upgrading_Now)
					{
						upgrade_in_Progress_Informer.text = "Another Mine is currently being upgraded";
					}
				}
			}
		}
			
		else	if (BuildTimer.queuedBuilding.Count < 1)
		{
			upgrade_in_Progress_Informer.gameObject.SetActive(false);
		}
	}

    string NameConverting(string toConvert)
    {
        string toReturn = toConvert;

        if (toReturn.ToLower().Contains("bunker"))
            return "Knight Training Ground";
        if (toReturn.ToLower().Contains("range"))
            return "Archer Training Ground";
        if (toReturn.ToLower().Contains("guild"))
            return "Mage Training Ground";
        if (toReturn.ToLower().Contains("recruitment"))
            return "Preparation hall";
        return toConvert;
    }

	void OnEnable()
	{
		//TroopsCheck();
		//As long as there are no structures being built in progress

		Check_If_Building_Upgrade_Available();

		if (BuildTimer.buildingTimer.Count == 0 )
		{
			upgradeBlocker.SetActive(false);
			//turn off progress bar
			theProgressBar.SetActive(false);
			if (name.Contains("Recruitment") && BuildTimer.queuedMyGroup.Count >= 1)
			{
				//this.upgradeButton.interactable = false;
				upgradeBlocker.SetActive(true);
			}
		}
		// But if there are any buildings that are being built
		else if (BuildTimer.queuedBuilding.Count >= 1)
		{
			// and if this building is the one being upgraded
			if (name.Contains(BuildTimer.queuedBuilding[0]))
				theProgressBar.SetActive(true);
			//disable the upgrade button
			//upgradeButton.interactable = false;

			//if not this building that is being upgraded
			else
				upgradeBlocker.SetActive(true);
		

		//check if the last tapped RSS building is upgrading
		// If the last tap to pop up building is not on upgrade
		// == -- MILL CHECK -- == //
    		if (name.Contains ("Mill"))
    		{
    			if (Player.mill_That_Player_Last_Tapped_On != Player.mill_Upgrading_Now)
    			{
    				// hide the progress bar
    				theProgressBar.SetActive(false);
    				// hide the complete button
    				completeButton.SetActive(false);
    			}
    			// But if the last time is the one that is currently upgrading
    			else if (Player.mill_That_Player_Last_Tapped_On == Player.mill_Upgrading_Now)
    			{
    				//display progress
    				theProgressBar.SetActive(true);
    				// and if the building time reaches 0
    				if (BuildTimer.buildingTimer[0] <=  0)
    				{
    					// allow players to interact with the complete button
    					Complete_Building("Mill");
    				}
    			}
    		}
    		// == -- MINE CHECK -- == //
    		else if (name.Contains ("Mine"))
    		{
    			if (Player.mine_That_Player_Last_Tapped_On != Player.mine_Upgrading_Now)
    			{
    				// hide the progress bar
    				theProgressBar.SetActive(false);
    				// hide the complete button
    				completeButton.SetActive(false);
                    //enable the upgrade blocker            
                    upgradeBlocker.SetActive(true);
    			}
    			// But if the last time is the one that is currently upgrading
    			else if (Player.mine_That_Player_Last_Tapped_On == Player.mine_Upgrading_Now)
    			{
    				//display progress
    				theProgressBar.SetActive(true);
    				// and if the building time reaches 0
    				if (BuildTimer.buildingTimer[0] <=  0)
    				{
    					// allow players to interact with the complete button
    					Complete_Building("Mine");
    				}
    			}
            }
		}
	}

	void OnDisable()
	{
		upgradeBlocker.SetActive(false);
		upgrade_in_Progress_Informer.gameObject.SetActive(false);
	}
}
