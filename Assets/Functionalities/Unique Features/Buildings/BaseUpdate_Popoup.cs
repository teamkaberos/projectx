﻿using UnityEngine;
using UnityEngine.UI;

public class BaseUpdate_Popoup : MonoBehaviour 
{
	public delegate void BuildingComplete();
	public static event BuildingComplete OnBuildingComplete;

	public Text currentLvl;
	public Text currentValue;
	public Text generationRate;
	public Text filledPerc;									//percentage filled for secondary activity of building
	public RectTransform gaugeSize;					//rect transform for secondary progress bar ie troops; RSS
	public Image currentFillAmount;					//progress amount for secondary element
	public Text buildingFilledPerc;						//structure building progress percentage
	public RectTransform buildGaugeSize;		    //rect transform for the building progress bar
	public Image structureBuildingAmount;       //progress amount for structure building
	public Text nextRequirementNotMet;          //displays the reason the upgrade button is not available
    public Text upgradingText;                         // the text on the 'Upgrade' button in the mini popup

	protected string thisBuilding;
	public Button upgradeButton;
	public GameObject speedUp;
	public GameObject moveToUpgradingBuilding;
	public GameObject completeBuilding;

	public Image buildProgression;
	public Text buildTimeLeft;
    public Text buildCost;

	[SerializeField] public float x;

	void Start()
	{
		BuildTimer.buildingCompleted += BuildingCompleted;
        BuildTimer.buildingUnqueued += DisplayCost;

	}
    public virtual void OnDestroy()
    {
        BuildTimer.buildingCompleted -= BuildingCompleted;
        BuildTimer.buildingUnqueued -= DisplayCost;
    }

    public virtual void GetUpdated()
    {
        
    }


    public virtual void OnEnable()
	{
		CancelInvoke("GetUpdated");
		Invoke("GetUpdated", 0.01f);

		if (tag.Contains("Resource"))
			InvokeRepeating("Update_RSS_Gens",0.01f, 3f);

        DisplayCost();

		if(BuildTimer.queuedBuilding.Count >= 1)
		{

			if (name.Contains (BuildTimer.queuedBuilding[0]))
			{
				// if the building one is a mine
				if (BuildTimer.queuedBuilding[0].Contains("Mine"))
				{
					if (Player.mine_That_Player_Last_Tapped_On == Player.mine_Upgrading_Now)
					{
						buildProgression.gameObject.SetActive(true);
					}
					else
					{
						buildProgression.gameObject.SetActive(false);
					}
				}
				else
					buildProgression.gameObject.SetActive(true);

				if (BuildTimer.buildingIsCompleted)
				{
					BuildingCompleted();
				}
			}
		}

		SpeedUpOrMoveToBuilding();
	}

    void DisplayCost()
    {
        if (name.ToLower().Contains("tavern"))
            return;

        if (nextRequirementNotMet.text.ToLower().Contains("max"))
        {
            buildCost.text = "<color=00FF00>Maxed</color>";
            return;
        }

        if (!name.ToLower().Contains("tavern"))
            buildCost.text = string.Format("{0} / {1}", kConversion(Player.gold), kConversion(Player.Instance.upgradeCost(HomeBuildings.buildingSelected)));
    }
	
    void OnDisable()
	{
		speedUp.SetActive(false);
		moveToUpgradingBuilding.SetActive(false);
		CancelInvoke("GetUpdated");
	}

    public void GoldValueChanged()
    {
        DisplayCost();
        Check_If_Building_Upgrade_Available();
    }


	void BuildingCompleted()
	{
		//for mines
		if (name.Contains("Mine"))
		{
			if (Player.mine_That_Player_Last_Tapped_On == Player.mine_Upgrading_Now)
			{
				buildProgression.fillAmount = 1;
				buildTimeLeft.text = "Building Completed";
			}
			else
			{
				buildProgression.fillAmount =0;
				buildTimeLeft.text = "00:00:00";
			}
		}
		else
		{
			if (!name.Contains("Tavern"))
			{
				buildProgression.fillAmount = 1;
				buildTimeLeft.text = "Building Completed";
			}
		}
	}

	public void HideProgressionBar_And_UpdateProgressText()
	{
		buildProgression.fillAmount = 0;
		buildTimeLeft.text = "00:00:00";
	}

    public void Toggle_Upgrade_Button()
    {
        upgradeButton.interactable = true;
    }

    protected string kConversion(int valueToConvert)
    {
        float toConvert = 0;

        if (valueToConvert.ToString().Length >= 4)
        {
            toConvert = valueToConvert / 1000f;

            if (valueToConvert.ToString().Length >= 7)
                toConvert = valueToConvert / 1000000f;
        }

        if (valueToConvert.ToString().Length >= 4 & valueToConvert.ToString().Length < 7)
            return string.Format("{0:n1}K", toConvert);
        if (valueToConvert.ToString().Length >= 7)
            return string.Format("{0:n1}M", toConvert);
        return valueToConvert.ToString();
    }

	void Update()
	{
		if (!name.Contains("Tavern"))
		{
			if(BuildTimer.queuedBuilding.Count >= 1)
			{
				if (BuildTimer.buildingIsCompleted != true)
				{
					//Checking and displaying build progress area
					//If this building is being upgraded right now
					if (name.Contains (BuildTimer.queuedBuilding[0]))
					{
						if (name.Contains("Mine"))
						{
							if (Player.mine_That_Player_Last_Tapped_On == Player.mine_Upgrading_Now)
							{
								buildProgression.gameObject.SetActive(true);
                                buildTimeLeft.text = string.Format("{0} ({1:0}%)", BuildTimer.currentBuildingTimeLeft, Mathf.FloorToInt(BuildTimer.currentBuildingProgress * 100));

								buildProgression.fillAmount = BuildTimer.currentBuildingProgress;
								if ((BuildTimer.currentBuildingProgress * 100)<10)
                                    buildTimeLeft.text = string.Format("{0} ({1:0}%)", BuildTimer.currentBuildingTimeLeft, Mathf.FloorToInt(BuildTimer.currentBuildingProgress * 100));
								else
                                    buildTimeLeft.text = string.Format("{0} ({1:00}%)", BuildTimer.currentBuildingTimeLeft, Mathf.FloorToInt(BuildTimer.currentBuildingProgress * 100));
							}
							else
							{
								buildProgression.gameObject.SetActive(false);
								buildTimeLeft.text =  "00:00:00";
							}
						}
						else
						{
							buildProgression.gameObject.SetActive(true);
                            buildTimeLeft.text = string.Format("{0} ({1:0}%)", BuildTimer.currentBuildingTimeLeft, Mathf.FloorToInt(BuildTimer.currentBuildingProgress * 100));

							buildProgression.fillAmount = BuildTimer.currentBuildingProgress;
							if ((BuildTimer.currentBuildingProgress * 100)<10)
                                buildTimeLeft.text = string.Format("{0} ({1:0}%)", BuildTimer.currentBuildingTimeLeft, Mathf.FloorToInt(BuildTimer.currentBuildingProgress * 100));
							else
                                buildTimeLeft.text = string.Format("{0} ({1:00}%)", BuildTimer.currentBuildingTimeLeft, Mathf.FloorToInt(BuildTimer.currentBuildingProgress * 100));
						}
					}
					else
					{
						buildProgression.gameObject.SetActive(false);
						buildTimeLeft.text = "00:00:00";
					}
				}
				else
				{
					if (name.Contains("Mine"))
					{}
				}
			}
			else
			{
				buildProgression.gameObject.SetActive(false);
				buildTimeLeft.text = "00:00:00";
			}
		}
	}

	public virtual void SpeedUpOrMoveToBuilding()
	{
		//if there is an upgrade going on
		if (BuildTimer.queuedBuilding.Count>=1)
		{			
			//and this pop up is the same as the building being upgraded
			if (thisBuilding.Contains(BuildTimer.queuedBuilding[0]))
			{
				speedUp.SetActive(true);
				if (BuildTimer.buildingTimer[0] <= 0)
				{
					completeBuilding.SetActive(true);
				}
			}
			else
			{
				moveToUpgradingBuilding.SetActive(true);
			}
		}
		else 
		{
			completeBuilding.SetActive(false);
			moveToUpgradingBuilding.SetActive(false);
		}

		Check_If_Building_Upgrade_Available();
	}

	public void Check_If_Building_Upgrade_Available()
    {
        thisBuilding = name;
        if (!thisBuilding.Contains("Tavern"))
            nextRequirementNotMet.gameObject.SetActive(false);
        
        if (thisBuilding.Contains("Mine"))
        {
            if (Player.mines[Player.mine_That_Player_Last_Tapped_On] == 0)
                upgradingText.text = "Build Up";

            if (Player.mines[Player.mine_That_Player_Last_Tapped_On] >= Player.rssBuildingLvCap)
            {
                nextRequirementNotMet.text = UnupgradableReason("max");
                upgradingText.text = string.Format("<color=#FF0000>{0}</color>", UnupgradeableReasonButton("max"));
                DisplayCost();
                upgradeButton.interactable = false;
                return;
            }
            //IF CASTLE LEVEL REQUIREMENT NOT MET
            if (Player.minesCost[Player.mine_That_Player_Last_Tapped_On] > Player.gold)
            {
                nextRequirementNotMet.text = UnupgradableReason("gold");
                upgradingText.text = string.Format("<color=#FF0000>{0}</color>", UnupgradeableReasonButton("gold"));
                upgradeButton.interactable = false;
                return;
            }
            if (Player.mines[Player.mine_That_Player_Last_Tapped_On] >= (Player.castleLevel * 3 + 3))
            {
                nextRequirementNotMet.text = UnupgradableReason("castle");
                upgradingText.text = string.Format("<color=#FF0000>{0}</color>", UnupgradeableReasonButton("castle"));
                upgradeButton.interactable = false;
                return;
            }
            if (Player.tutorial == 0 & Player.mines[4] <= 1)
            {
                upgradeButton.interactable = false;
                return;
            }
            if (Player.mines[Player.mine_That_Player_Last_Tapped_On] >= 1)
            {
                upgradingText.text = "<color=#FFFFFF>Build Up</color>";
            }
        }
        else if (thisBuilding.Contains("Bunker"))
        {
            if (Player.bunkerLv >= Player.barracksLvlCap)
            {
                nextRequirementNotMet.text = UnupgradableReason("max");
                upgradingText.text = string.Format("<color=#FF0000>{0}</color>", UnupgradeableReasonButton("max"));
                DisplayCost();
                upgradeButton.interactable = false;
                return;
            }
            if (Player.bunkerCost > Player.gold)
            {
                nextRequirementNotMet.text = UnupgradableReason("gold");
                upgradingText.text = string.Format("<color=#FF0000>{0}</color>", UnupgradeableReasonButton("gold"));
                upgradeButton.interactable = false;
                return;
            }
            if (Player.bunkerLv >= Player.castleLevel * 3 + 3)
            {
                nextRequirementNotMet.text = UnupgradableReason("castle");
                upgradingText.text = string.Format("<color=#FF0000>{0}</color>", UnupgradeableReasonButton("castle"));
                upgradeButton.interactable = false;
                return;
            }
            if (Player.tutorial == 2 && Player.bunkerLv == 1)
            {
                upgradeButton.interactable = false;
                return;
            }
        }
		else if (thisBuilding.Contains("Guild"))
        {
            if (Player.guildLv >= Player.barracksLvlCap)
            {
                nextRequirementNotMet.text = UnupgradableReason("max");
                upgradingText.text = string.Format("<color=#FF0000>{0}</color>", UnupgradeableReasonButton("max"));
                DisplayCost();
                upgradeButton.interactable = false;
                return;
            }
			if (Player.guildCost > Player.gold)
			{
                nextRequirementNotMet.text = UnupgradableReason("gold");
                upgradingText.text = string.Format("<color=#FF0000>{0}</color>", UnupgradeableReasonButton("gold"));
				upgradeButton.interactable = false;
				return;
			}
			if (Player.guildLv >= (Player.castleLevel * 3 + 3))
			{
                nextRequirementNotMet.text = UnupgradableReason("castle");
                upgradingText.text = string.Format("<color=#FF0000>{0}</color>", UnupgradeableReasonButton("castle"));
				upgradeButton.interactable = false;
				return;
			}
            if (Player.tutorial == 2 && Player.guildLv == 1)
            {
                upgradeButton.interactable = false;
                return;
            }         
		}
		else if (thisBuilding.Contains("Range"))
        {
            if (Player.rangeLv >= Player.barracksLvlCap)
            {
                nextRequirementNotMet.text = UnupgradableReason("max");
                upgradingText.text = string.Format("<color=#FF0000>{0}</color>", UnupgradeableReasonButton("max"));
                DisplayCost();
                upgradeButton.interactable = false;
                return;
            }
			if(Player.rangeCost > Player.gold)
			{
                nextRequirementNotMet.text = UnupgradableReason("gold");
                upgradingText.text = string.Format("<color=#FF0000>{0}</color>", UnupgradeableReasonButton("gold"));
				upgradeButton.interactable = false;
				return;
			}
			if(Player.rangeLv >= (Player.castleLevel * 3 + 3))
			{
                nextRequirementNotMet.text = UnupgradableReason("castle");
                upgradingText.text = string.Format("<color=#FF0000>{0}</color>", UnupgradeableReasonButton("castle"));
				upgradeButton.interactable = false;
				return;
			}
            if (Player.tutorial == 2 && Player.rangeLv == 1)
            {
                upgradeButton.interactable = false;
                return;
			}
		}
		else if (thisBuilding.Contains("RecruitmentHall"))
        { 
            //if rhall is at max lvl
            if (Player.recruitmentHall_Lvl >= Player.recruitmentHallLvlCap)
            {
                nextRequirementNotMet.text = UnupgradableReason("max");
                upgradingText.text = string.Format("<color=#FF0000>{0}</color>", UnupgradeableReasonButton("max"));
                DisplayCost();
                upgradeButton.interactable = false;
                return;
            }     
			//RHall can only be upgraded if all 3 barracks are higher lvl than it
			if (Player.bunkerLv <= Player.recruitmentHall_Lvl)
			{
                nextRequirementNotMet.text = UnupgradableReason("bunker");
                upgradingText.text = string.Format("<color=#FF0000>{0}</color>", UnupgradeableReasonButton("structure"));
				upgradeButton.interactable = false;
				return;
			}
            if (Player.rangeLv <= Player.recruitmentHall_Lvl)
            {
                nextRequirementNotMet.text = UnupgradableReason("range");
                upgradingText.text = string.Format("<color=#FF0000>{0}</color>", UnupgradeableReasonButton("structure"));
                upgradeButton.interactable = false;
                return;
            }
			if (Player.guildLv <= Player.recruitmentHall_Lvl)
			{
                nextRequirementNotMet.text = UnupgradableReason("guild");
                upgradingText.text = string.Format("<color=#FF0000>{0}</color>", UnupgradeableReasonButton("structure"));
				upgradeButton.interactable = false;
				return;
			}

			//or if there are no troops being built at the moment
			if (BuildTimer.queuedMyGroup.Count >= 1)
			{
				upgradeButton.interactable = false;
				return;
			}

			if (Player.recruitmentHall_Lvl >= (Player.castleLevel *3 + 3))
			{
                nextRequirementNotMet.text = UnupgradableReason("castle");
                upgradingText.text = string.Format("<color=#FF0000>{0}</color>", UnupgradeableReasonButton("castle"));
				upgradeButton.interactable = false;
				return;
			}
			if (Player.recruitmentHall_Cost > Player.gold)
			{
                nextRequirementNotMet.text = UnupgradableReason("gold");
                upgradingText.text = string.Format("<color=#FF0000>{0}</color>", UnupgradeableReasonButton("gold"));
				upgradeButton.interactable = false;
				return;
			}
            if (Player.tutorial == 3 & Player.recruitmentHall_Lvl == 1)
            {
                upgradeButton.interactable = false;
                return;
            }
		}
		else if (thisBuilding.Contains("Treasury"))
        {
            if (Player.treasuryLvl >= Player.treasuryLvlCap)
            {
                nextRequirementNotMet.text = UnupgradableReason("max");
                upgradingText.text = string.Format("<color=#FF0000>{0}</color>", UnupgradeableReasonButton("max"));
                DisplayCost();
                upgradeButton.interactable = false;
                return;
            }
            if (Player.treasuryCost > Player.gold)
			{
                nextRequirementNotMet.text = UnupgradableReason("gold");
                upgradingText.text = string.Format("<color=#FF0000>{0}</color>", UnupgradeableReasonButton("gold"));
                upgradeButton.interactable = false;
                return;
            }
			if (Player.treasuryLvl >= (Player.castleLevel * 3 + 3))
			{
                nextRequirementNotMet.text = UnupgradableReason("castle");
                upgradingText.text = string.Format("<color=#FF0000>{0}</color>", UnupgradeableReasonButton("castle"));
				upgradeButton.interactable = false;
				return;
			}
            if (Player.tutorial == 1 && Player.treasuryLvl <= 1)
            {
                upgradeButton.interactable = false;
                return;
			}
		}
		else if (thisBuilding.Contains("Forge"))
        {
            if (Player.forgeLevel >= Player.forgeCap)
            {
                nextRequirementNotMet.text = UnupgradableReason("max");
                upgradingText.text = string.Format("<color=#FF0000>{0}</color>", UnupgradeableReasonButton("max"));
                DisplayCost();
                upgradeButton.interactable = false;
                return;
            }
            if (Player.forge_UpgradeCost > Player.gold)
            {
                nextRequirementNotMet.text = UnupgradableReason("gold");
                upgradingText.text = string.Format("<color=#FF0000>{0}</color>", UnupgradeableReasonButton("gold"));
                upgradeButton.interactable = false;
                return;
            }
			if (Player.castleLevel < (Player.forgeLevel * 2) +2 )
			{
                nextRequirementNotMet.text = UnupgradableReason("castle");
                upgradingText.text = string.Format("<color=#FF0000>{0}</color>", UnupgradeableReasonButton("castle"));
				upgradeButton.interactable = false;
				return;
			}
		}
		else if (thisBuilding.Contains("TownHall"))
        {
            if (Player.townHallLevel >= Player.townHallLevelCap)
            {
                nextRequirementNotMet.text = UnupgradableReason("max");
                upgradingText.text = string.Format("<color=#FF0000>{0}</color>", UnupgradeableReasonButton("max"));
                DisplayCost();
                upgradeButton.interactable = false;
                return;
            }
			for (int x = 0; x < 5; x++)
			{
				if (Player.mines[x] <= Player.townHallLevel && Player.mines[x] != 0)
				{        
                    nextRequirementNotMet.text = UnupgradableReason("mine");
                    upgradingText.text = string.Format("<color=#FF0000>{0}</color>", UnupgradeableReasonButton("structure"));
					upgradeButton.interactable = false;
					return;
				}
			}
            if (Player.recruitmentHall_Lvl <= Player.townHallLevel)
            {
                nextRequirementNotMet.text = UnupgradableReason("recruitment hall");
                upgradingText.text = string.Format("<color=#FF0000>{0}</color>", UnupgradeableReasonButton("structure"));
                upgradeButton.interactable = false;
                return;
            }
			if (Player.treasuryLvl <= Player.townHallLevel)
			{
                nextRequirementNotMet.text = UnupgradableReason("treasury");
                upgradingText.text = string.Format("<color=#FF0000>{0}</color>", UnupgradeableReasonButton("structure"));
				upgradeButton.interactable = false;
				return;
			}
			if (Player.townHallLevel >= (Player.castleLevel * 3 + 3))
			{
                nextRequirementNotMet.text = UnupgradableReason("castle");
                upgradingText.text = string.Format("<color=#FF0000>{0}</color>", UnupgradeableReasonButton("structure"));
				upgradeButton.interactable = false;
				return;
			}
			if (Player.townHallCost > Player.gold)
			{
                nextRequirementNotMet.text = UnupgradableReason("gold");
                upgradingText.text = string.Format("<color=#FF0000>{0}</color>", UnupgradeableReasonButton("gold"));
				upgradeButton.interactable = false;
				return;
			}
        }

        OkToUpgrade();
	}
    void OkToUpgrade()
    {
        if (!name.Contains("Tavern"))
        {
            upgradingText.text = "<color=#FFFFFF>Build Up</color>";
            upgradeButton.interactable = true;
            nextRequirementNotMet.text = "go ahead";
            nextRequirementNotMet.gameObject.SetActive(false);
        }
    }

    public string UnupgradableReason(string reason)
	{
		nextRequirementNotMet.gameObject.SetActive(true);

		if (reason.Contains("gold"))
			return "Insufficient GOLD";
		if (reason.Contains("castle"))
			return "Requires higher PLAYER Level";
		if (reason.Contains("max"))
			return "Building already at MAX LEVEL";
        if (reason.Contains("bunker")|| reason.Contains("range") || reason.Contains("guild") || reason.Contains("townhall") || reason.Contains("recruitment hall"))
            return string.Format("Requires {0} to be upgraded", NameConverting(reason).ToUpper());
        if (reason.Contains("treasury") || reason.Contains("mine"))
			return string.Format("Requires {0} to be upgraded", reason.ToUpper());

        return null;        
	}
    public string UnupgradeableReasonButton(string reason)
    {
        if (reason.Contains("gold"))
            return "Insufficient GOLD";
        if (reason.Contains("castle"))
            return "Insufficient P.Lv";
        if (reason.Contains("max"))
            return "Level Maxed";
        if (reason.Contains("structure"))
            return "Insufficient Structure Lvs";

        return null;        
    }

    string NameConverting(string toConvert)
    {
        switch (toConvert)
        {
            case "bunker":
                return "Knight Training Ground";

            case "range":
                return "Archer Training Ground";

            case "guild":
                return "Mage Training Ground";

            case "townhall":
                return "castle";

            case "recruitment hall":
                return "Preparation hall";

            default:
                return "";
        }

    }
}
