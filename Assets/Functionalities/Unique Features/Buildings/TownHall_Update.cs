﻿public class TownHall_Update : BaseUpdate_Popoup
{
    public delegate void OnTutorialBuildUp();
    public static event OnTutorialBuildUp tutorialBuild;

    public override void GetUpdated()
	{
        currentLvl.text = string.Format("Castle Lv{0}", Player.townHallLevel);
	}

	public override void SpeedUpOrMoveToBuilding()
	{
		thisBuilding = name;
		base.SpeedUpOrMoveToBuilding();
	}


    public void TutorialUpgrade()
    {
        if (Player.tutorial < Player.tutorialCap)
            tutorialBuild();
    }
}
