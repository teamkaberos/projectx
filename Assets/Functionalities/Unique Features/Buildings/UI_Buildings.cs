﻿using UnityEngine;

public class UI_Buildings : ResourceGenerate
{
	public float rotationSpeed;
    public GameObject floor;


	public override void Update_RSS_Building_Model()
	{
        generatorLvl = Player.mines[Player.mine_That_Player_Last_Tapped_On];
        if (name.ToLower().Contains("mine"))
        {
            if (generatorLvl >= 1)
                floor.SetActive(false);
            else
                floor.SetActive(true);
            
            base.Update_RSS_Building_Model();
        }
	}

	void OnEnable()
	{
		Update_RSS_Building_Model();
	}
		
	void OnDisable()
	{
		Disable_All_Levels_Of_Mesh();
	}

	void Update()
	{
		transform.Rotate(Vector3.up * Time.deltaTime * rotationSpeed);
	}
}
