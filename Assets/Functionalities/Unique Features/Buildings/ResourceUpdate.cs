﻿using UnityEngine;
using UnityEngine.UI;

public class ResourceUpdate : BaseUpdate_Popoup
{
    public delegate void OnTutorialUpgrade();
    public static event OnTutorialUpgrade tutorialUpgrading;

	public Button collect_RSS_Button;
	public Button info_Button;
    public Button building_Complete_Button;

    public GameObject speedUpProgression;

    bool continueCheckIfUpgradeAvail;   // If this Class says upgrade is unavailable, stop the check from base class

    public void Toggle_Building_Complete_Button()
    {
        building_Complete_Button.interactable = true;
    }

    public override void GetUpdated()
	{
		string currentLevel = BuildingManager.Instance.goldMine[Player.mine_That_Player_Last_Tapped_On].CurrentRSSLvl();
		currentLvl.text = currentLevel;

		string genRate = BuildingManager.Instance.goldMine[Player.mine_That_Player_Last_Tapped_On].currentGenerationRate();
		generationRate.text = genRate;

        string value = BuildingManager.Instance.goldMine[Player.mine_That_Player_Last_Tapped_On].CurrentAvailableAmount();

        int crv = Mathf.FloorToInt(BuildingManager.Instance.goldMine[Player.mine_That_Player_Last_Tapped_On].currentResourceValue);
        int mrv = BuildingManager.Instance.goldMine[Player.mine_That_Player_Last_Tapped_On].generatorCap;

        if (Player.mines[Player.mine_That_Player_Last_Tapped_On] > 0)
            currentValue.text = string.Format("{0} / {1}", kConversion(crv), kConversion(mrv));
        else
            currentValue.text = string.Format("0 / 0");

        if (BuildingManager.Instance.goldMine[Player.mine_That_Player_Last_Tapped_On].generatorLvl < 1)
            collect_RSS_Button.interactable = false;
		else
			collect_RSS_Button.interactable = true;


        if (BuildingManager.Instance.goldMine[Player.mine_That_Player_Last_Tapped_On].currentResourceValue < 1)
            collect_RSS_Button.interactable = false;
        else
            collect_RSS_Button.interactable = true;

        if (Player.mine_Upgrading_Now == Player.mine_That_Player_Last_Tapped_On)
            speedUpProgression.SetActive(true);
        else
            speedUpProgression.SetActive(false);

        if (MineUnlockCounterReached() & Player.mines[Player.mine_That_Player_Last_Tapped_On] <= 0)
        {
            continueCheckIfUpgradeAvail = false;
            upgradeButton.interactable = false;

            nextRequirementNotMet.text = UnupgradableReason("castle");
        }
        else
        {
            continueCheckIfUpgradeAvail = true;
            upgradeButton.interactable = true;
        }

        if (Player.tutorial < Player.tutorialCap)
        {
            continueCheckIfUpgradeAvail = false;
            upgradeButton.interactable = false;
        }

        if (continueCheckIfUpgradeAvail)
            Check_If_Building_Upgrade_Available();//From base
	}
    bool MineUnlockCounterReached()
    {
        int unlockedNow = 0;
        for (int x = 0; x < Player.mines.Length; x++)
        {
            if (Player.mines[x] >= 1)
                unlockedNow += 1;
        }

        if (unlockedNow >= Player.resourceBuildings_Unlock_Counter[Player.castleLevel])
            return true;
        return false;
    }
	public void Update_RSS_Gens()
	{
		string value = BuildingManager.Instance.goldMine[Player.mine_That_Player_Last_Tapped_On].CurrentAvailableAmount();

        int crv = Mathf.FloorToInt(BuildingManager.Instance.goldMine[Player.mine_That_Player_Last_Tapped_On].currentResourceValue);
        int mrv = BuildingManager.Instance.goldMine[Player.mine_That_Player_Last_Tapped_On].generatorCap;
        if (Player.mines[Player.mine_That_Player_Last_Tapped_On] > 0)
            currentValue.text = string.Format("{0} / {1}", kConversion(crv), kConversion(mrv));
        else
            currentValue.text = string.Format("0 / 0");

		int currentPerc = BuildingManager.Instance.goldMine[Player.mine_That_Player_Last_Tapped_On].FilledPercentage();

        if (BuildingManager.Instance.goldMine[Player.mine_That_Player_Last_Tapped_On].currentResourceValue < 1)
            collect_RSS_Button.interactable = false;
        else
            collect_RSS_Button.interactable = true;

		filledPerc.text = currentPerc + "% full";

		if (currentPerc == 0)
			x = 0;
		else if (currentPerc > 0)
			x = ((currentPerc * gaugeSize.sizeDelta.x) / 100f);

		currentFillAmount.rectTransform.sizeDelta = new Vector2(x,gaugeSize.sizeDelta.y);
	}

	public override void SpeedUpOrMoveToBuilding()
	{
		//registers what building is being tapped on
		thisBuilding = name;

		if (BuildTimer.queuedBuilding.Count>=1)
		{
			//and if the building being upgraded right now is a mine
			if (thisBuilding.Contains(BuildTimer.queuedBuilding[0]))
			{
				if(Player.mine_Upgrading_Now == Player.mine_That_Player_Last_Tapped_On)
				{
					moveToUpgradingBuilding.SetActive(false);
					speedUp.SetActive(true);
					if (BuildTimer.buildingTimer[0] <= 0)
					{
						completeBuilding.SetActive(true);
					}
				}
				else
				{
					completeBuilding.SetActive(false);
					moveToUpgradingBuilding.SetActive(true);
				}
			}
			// if not
			else
			{
				speedUp.SetActive(false);
				completeBuilding.SetActive(false);
				moveToUpgradingBuilding.SetActive(true);
			}
		}

		Check_If_Building_Upgrade_Available();
	}

    public void Disable_Tutorial_Speed_Up()
    {
        if (Player.tutorial == 0)
        {
            speedUp.GetComponent<Button>().interactable = false;
            building_Complete_Button.interactable = false;
            tutorialUpgrading();
        }
        else
        {
            speedUp.GetComponent<Button>().interactable = true;
            building_Complete_Button.interactable = true;
        }
    }
}