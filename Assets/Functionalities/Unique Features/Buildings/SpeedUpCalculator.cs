﻿using UnityEngine;
using UnityEngine.UI;

public class SpeedUpCalculator : MonoBehaviour
{
    public Button[] SpeedupButton;
    public Button cancelButton;
    public Button applySpeedUps;

	void Start()
	{
        BuildTimer.speedUpFilled += DisableAllButton;
        BuildTimer.speedUpTapped += EnableSpeedUp;

		//BuildTimer.Instance.To_Be_Sped_Up("building");
		BuildTimer.Instance.Calculate_SpeedUp_Cost();
        AddApplyListener();
	}

	void OnEnable()
    {
        CheckTimerAmounts();
		BuildTimer.Instance.Calculate_SpeedUp_Cost();
        ToggleApplySpeedUp(false);
	}

    void AddApplyListener()
    {
        for (int x = 0; x < SpeedupButton.Length-1; x++)
        {
            SpeedupButton[x].onClick.AddListener(delegate { ToggleApplySpeedUp(true) ; });
        }
    }

    void CheckTimerAmounts()
    {
        if (Player.timer3 < 1)
            SpeedupButton[0].interactable = false;
        else
            SpeedupButton[0].interactable = true;

        if (Player.timer15 < 1)
            SpeedupButton[1].interactable = false;
        else
            SpeedupButton[1].interactable = true;
        
        if (Player.timer30 < 1)
            SpeedupButton[2].interactable = false;
        else
            SpeedupButton[2].interactable = true;

        if (Player.timer60 < 1)
            SpeedupButton[3].interactable = false;
        else
            SpeedupButton[3].interactable = true;
    }
     void ToggleApplySpeedUp(bool toDisable)
    {
        applySpeedUps.interactable = toDisable;
    }
    void EnableSpeedUp()
    {
        ToggleApplySpeedUp(true);
    }

    void EnableAllButtons()
    {
        for (int x = 0; x < SpeedupButton.Length; x++)
        {
            SpeedupButton[x].interactable = true;
        }
    }

    void DisableAllButton()
    {
        for (int x = 0; x < SpeedupButton.Length; x++)
        {
            SpeedupButton[x].interactable = false;
        }
    }

    void OnDestroy()
    {
        BuildTimer.speedUpFilled -= DisableAllButton;
        BuildTimer.speedUpTapped -= EnableSpeedUp;
    }
}