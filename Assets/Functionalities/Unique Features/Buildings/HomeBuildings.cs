﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class HomeBuildings : MonoBehaviour,IPointerClickHandler
{
    public AudioSource audioSource;
    public AudioClip startingSound, lodgeSound;

    public delegate void OnTutorialTapped();
    public static event OnTutorialTapped TutorialBuildingTapped;

    public delegate void OnTutorialTapped_(bool isTapped);
    public static event OnTutorialTapped_ TutorialBuildingTapped_;

	public GameObject popup;				        // Pop-ups for each respective buildings
	public GameObject tapToClose;		        // Tap to close BG to prevent other pop-ups from opening

	public GameObject BuildingToDisplay;		//the 3d building to be poped up

	public GameObject upgradeInProgress;		//scaffolding to appear when players upgrade
    GameObject upgradeProgressBar;

	public GameObject small_Popup_Info;		//the popup info to be displayed for This building

	CameraMovement camMovement;

    public int buildingNo;

    public static int buildingSelected;

	public GameObject buildingComplete_Pop_Up;

    public GameObject selectedDisplay;
	public bool clicked;

    EventTrigger eventTrigger;

    public GameObject buildingMesh;
    public GameObject barrackCyclone;                               // Only available for the barracks
    public GameObject barrackFullBattlePointNotification;
    public GameObject buildingTroops;

    public GameObject notificationObj;
    public Text notifyText;
    public GameObject progressBarObj;

    public CameraMovement cameraMovement;
    public GameObject bunkerPointer;

    Vector4 yellowCol = new Vector4(1, 1, 0.37f, 1);
    Vector4 blueCol = new Vector4(0, 0.59f, 1, 1);


	public virtual void Start()
	{
		camMovement = GameObject.FindGameObjectWithTag("CamControl").GetComponent<CameraMovement>();

		BuildTimer.buildingCompleted += Building_Has_Been_Completed;
		BuildTimer.buildingUnqueued += HideScaffold;

        if (name.ToLower().Contains("barrack"))
            UnitManager.UnitHasEvolved += CheckFullBattlePointNotification;
            
        eventTrigger = gameObject.GetComponent<EventTrigger>();

        if (!name.ToLower().Contains("tavern"))
        {
            if (tag != "UIBuilding")
                upgradeProgressBar = upgradeInProgress.transform.GetChild(1).gameObject;
        }
	}

	public virtual void OnPointerClick(PointerEventData ped)
	{
        SelectThisBuilding();
	}

    public virtual void SelectThisBuilding()
    {
        // If another pop-up is already active
        // code stops here
        if (tapToClose.activeInHierarchy)
            return;
        if (camMovement.IsDragging() == true)
            return;
        
        // If there is no pop up active
        //Check if current building is a resource generator
        if (clicked == true)
            clicked = false;
        else
            clicked = true;
        
        //Check if player is on tutorial now

        if (Player.tutorial < Player.tutorialCap)
        {
            if (TutorialBuildingTapped != null)
                TutorialBuildingTapped();
            if (TutorialBuildingTapped_ != null)
                TutorialBuildingTapped_(true);

            if (Player.tutorial == 2)
            {
                bunkerPointer.SetActive(false);
                cameraMovement.LerpTowards_Building(LerpCheck("tut"), true);
                if (name.Contains("Barrack"))
                    GetComponent<BoxCollider>().enabled = false;
            }
        }
        else
        {
            camMovement.LerpTowards_Building(LerpCheck(), false);       
        }
        //Bar to slide up from below no matter which building is clicked

        UIManager.Instance.Slide_Up_Bar(clicked);
        //Check if building is selected, if yes it will drop the popup menu

        //Hide the selected display for all buildings
        BuildingManager.Instance.HideBuildingHighlight();

        BuildingManager.Instance.CheckWhichPopUp(this);

        selectedDisplay.SetActive(clicked);

        buildingSelected = buildingNo;

        //Hides any previous popup
        UIManager.Instance.HideMiniInfo();
        //Display the popup for this building
        small_Popup_Info.SetActive(true);

        //used to trigger each individual building's pop up
        BuildingManager.lastSelectedBuillding = gameObject;

        //Identify last popup for scaffolding display
        BuildTimer.lastBuildingPopup = gameObject;
    }
	public void BuildingPopUps()
	{
		//Update all building infos
		UIManager.Instance.Update_Building_LevelInfo();
        // Otherwise, it will open the pop-up and
        // prevent another one from opening
        // Display 3D building on the UI
        if (!name.Contains("Tavern"))
        {
            tapToClose.SetActive(true);
            BuildingToDisplay.SetActive(true);
        }
        else
        {
            audioSource.clip = lodgeSound;
            UnitManager.Instance.DistributeStats();
            if (Player.tutorial == 6)
                return;
        }
		// Display respective Popups
		popup.SetActive(true);
	}

	public void Building_Has_Been_Completed()
	{
		if (!tag.Contains("UI"))
		{
			if (name.Contains(BuildTimer.queuedBuilding[0]))
			{
				if (name.Contains("Mine"))
				{
                    if (name.Contains("Mine"+UIManager.Instance.goldMineConverter(Player.mine_Upgrading_Now).ToString()))
                        buildingComplete_Pop_Up.SetActive(true);
				}
				else 
				buildingComplete_Pop_Up.SetActive(true);
			}
		}
	}

    public void CheckFullBattlePointNotification()
    {
        if (name.ToLower().Contains("bunker"))
        {
            if (Player.squareBattles >= 10 && Player.squareStone >= 1)
            {
                ToggleNotification(true);
                SetNotificationType(1);
            }
            else if(Player.squareBattles < 10)
            {
                ToggleNotification(false);
            }        
        }
        if (name.ToLower().Contains("range"))
        {
            if (Player.triangleBattles >= 10 && Player.triangleStone >= 1)
            {
                ToggleNotification(true);
                SetNotificationType(1);
            }
            else if (Player.triangleBattles < 10)
            {
                ToggleNotification(false);
            }
        }
        if (name.ToLower().Contains("guild"))
        {
            if (Player.circleBattles >= 10 && Player.circleStone >= 1)
            {
                ToggleNotification(true);
                SetNotificationType(1);
            }
            else if (Player.circleBattles < 10)
            {
                ToggleNotification(false);
            }
        }
    }

    public void DisplayBuilding(int buildingToDisplayNo)
    {
        switch (buildingToDisplayNo)
        {
            case 0:
                if (name.ToLower().Contains("recruitment") & Player.recruitmentHall_Lvl >= 1)
                    buildingMesh.SetActive(true);
                break;
            case 1:
                if (name.ToLower().Contains("townhall") & Player.townHallLevel >= 1)
                    buildingMesh.SetActive(true);
                break;
            case 2:
                if (name.ToLower().Contains("bunker") & Player.bunkerLv >= 1)
                {
                    buildingMesh.SetActive(true);
                    barrackCyclone.SetActive(true);
                }
                break;
            case 3:
                if (name.ToLower().Contains("range") & Player.rangeLv >= 1)
                {
                    buildingMesh.SetActive(true);
                    barrackCyclone.SetActive(true);
                }
                break;
            case 4:
                if (name.ToLower().Contains("guild") & Player.guildLv >= 1)
                {
                    buildingMesh.SetActive(true);
                    barrackCyclone.SetActive(true);
                }
                break;
            case 5:
                if (name.ToLower().Contains("treasury") & Player.treasuryLvl >= 1)
                    buildingMesh.SetActive(true);
                break;
            case 11:
                if (name.ToLower().Contains("forge") & Player.forgeLevel >= 1)
                    buildingMesh.SetActive(true);
                break;
            case 12:
                if (name.ToLower().Contains("tavern") & Player.tutorial >= 5)
                    buildingMesh.SetActive(true);
                break;
        }
    }

    string LerpCheck()
    {
        if (name.Contains("RecruitmentHall"))
            return "RecruitmentHall";
        if (name.Contains("TownHall"))
            return "TownHall";
        if (name.Contains("Tavern"))
            return "tavernCamPos";
        if (name.Contains("Bunker"))
            return "Bunker";
        if (name.Contains("Range"))
            return "Range";
        if (name.Contains("Guild"))
            return "Guild";
        if (name.Contains("Treasury"))
            return "Treasury";
        if (name.Contains("Mine1"))
            return "Mine1";
        if (name.Contains("Mine2"))
            return "Mine2";
        if (name.Contains("Mine3"))
            return "Mine3";
        if (name.Contains("Mine4"))
            return "Mine4";
        if (name.Contains("Mine5"))
            return "Mine5";
        return "";
    }
    string LerpCheck(string forTut)
    {
        if (name.Contains("Bunker"))
            return "TempBunker";
        if (name.Contains("Range"))
            return "TempRange";
        if (name.Contains("Guild"))
            return "TempGuild";
        return "";
    }

    public void ToggleNotification(bool toSet)
    {
        if (!tag.Contains("UI"))
        {
            notificationObj.SetActive(toSet);
        }
    }
    public void SetNotificationType(int typeToSet)
    {
        // 0 is used for BATTLE POINTS met, 1 is used for STONE COMPLETION
        if (typeToSet == 0)
            notifyText.color = yellowCol;
        else if (typeToSet == 1)
            notifyText.color = blueCol;
    }

	public virtual void ShowScaffold()
	{
        if (upgradeInProgress != null)
        {
            upgradeInProgress.SetActive(true);
            upgradeProgressBar.SetActive(true);

            if (name.ToLower().Contains("barrack") || name.ToLower().Contains("mine"))
                HideTrainingUnit();
        }
	}
    public void DisplayProgressionBuild()
    {
        progressBarObj.SetActive(true);
    }
    void HideTrainingUnit()
    {
        buildingTroops.SetActive(false);
    }
    void ShowTrainingUnit()
    {
        buildingTroops.SetActive(true);
    }


	void HideScaffold()
	{
		if (!tag.Contains("UI"))
		{
			if (name.Contains(BuildTimer.queuedBuilding[0]))
			{
                if (name.ToLower().Contains("mine"))
				{
					if (name.Contains(Player.mine_Upgrading_Now .ToString()))
					{
						upgradeInProgress.SetActive(false);
                        Player.mine_Upgrading_Now = 255;
                        ShowTrainingUnit();
                    }
                }
                if (name.ToLower().Contains("barrack"))
                    ShowTrainingUnit();
				upgradeInProgress.SetActive(false);
			}
			// return value of the current upgrading mill to 255
			// 255 = default value so the progress bar will not appear
		}
	}

	void OnDestroy()
	{
		BuildTimer.buildingCompleted -= Building_Has_Been_Completed;
        BuildTimer.buildingUnqueued -= HideScaffold;

        if (name.ToLower().Contains("barrack"))
            UnitManager.UnitHasEvolved -= CheckFullBattlePointNotification;
	}
}
