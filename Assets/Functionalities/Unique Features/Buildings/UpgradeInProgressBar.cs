﻿using UnityEngine;
using UnityEngine.UI;

public class UpgradeInProgressBar : MonoBehaviour 
{
    public Image buildProgressImage;
    public Text buildingProgressPercentage;

    public Transform mainCam;
    float clampValueMin = -20;
    float clampValueMax = 10;

    void Start()
    {
        mainCam = GameObject.FindGameObjectWithTag("MainCamera").transform;
    }

	void Update () 
    {
        buildProgressImage.fillAmount = BuildTimer.currentBuildingProgress;

        GetPercentage();

        if (BuildTimer.currentBuildingProgress >= 1)
        {
            gameObject.SetActive(false);
        }
	}

    void LookAtCam()
    {
        transform.LookAt(mainCam.transform);
    }

    void GetPercentage()
    {
        buildingProgressPercentage.text = string.Format("{0} %", Mathf.FloorToInt(BuildTimer.currentBuildingProgress * 100));
    }
}
