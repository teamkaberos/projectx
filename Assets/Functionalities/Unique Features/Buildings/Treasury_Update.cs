﻿using UnityEngine;
using UnityEngine.UI;

public class Treasury_Update : BaseUpdate_Popoup 
{
    public delegate void OnTutorialUpgrade();
    public static event OnTutorialUpgrade tutorialUpgrading;
    
    public Button inventory_Button;
    public Button reward_Button;

    public GameObject speedUp_Tutorial_Popup;
    public GameObject treasury_Tutorial_Popup;

	public Image protectionAmount;

	float y;		//used for capping protection visuals

    public override void GetUpdated()
	{
        currentLvl.text = string.Format("Treasury Lv{0}", Player.treasuryLvl);

        generationRate.text = string.Format("{0} / {1}", kConversion(Player.gold), kConversion(Player.treasury_Storage_Cap));

		#region packs+chests
		//currentValue.text = string.Format("{0} available" ,Player.Instance.TotalPacks());
        buildingFilledPerc.text = string.Format("{0} available" ,Player.Instance.TotalPacks() + Player.Instance.TotalChests());
       

        if (Player.Instance.TotalChests() < 1 && Player.Instance.TotalPacks() < 1)
            reward_Button.interactable = false;
        else
            reward_Button.interactable = true;

        if (Player.tutorial < 5)
            inventory_Button.interactable = false;
        else
            inventory_Button.interactable = true;
		#endregion

		#region protection area
		currentFillAmount.rectTransform.sizeDelta = new Vector2 ((Player.Instance.Treasury_Filled_Perc()*gaugeSize.sizeDelta.x)/100f, gaugeSize.sizeDelta.y);
//		if (x > Player.treasury_Protection_Value)
//			y = Player.treasury_Protection_Value;
//		else if (x <= Player.treasury_Protection_Value)
//			y = x;
		protectionAmount.rectTransform.sizeDelta = new Vector2 ((y*gaugeSize.sizeDelta.x)/100f, gaugeSize.sizeDelta.y);
		filledPerc.text = string.Format("{0:0}% filled",Player.Instance.Treasury_Filled_Perc());
        #endregion
	}

	public override void SpeedUpOrMoveToBuilding()
	{
		thisBuilding = name;
		base.SpeedUpOrMoveToBuilding();
	}

    public void Tutorial_Upgrade()
    {
        if (Player.tutorial == 1)
            tutorialUpgrading();
    }
    public void Tutorial_Speed_Up()
    {
        if (Player.tutorial == 1)
        {
            treasury_Tutorial_Popup.SetActive(false);
            speedUp_Tutorial_Popup.SetActive(true);
        }
    }
}
