﻿using UnityEngine;

public class Forge_Update : BaseUpdate_Popoup 
{
	//	public Button sUnitBuild_Button;

	public override void GetUpdated()
	{
        currentLvl.text = string.Format("Forge Lv{0}", Player.forgeLevel);

		//Cost
		currentValue.text = string.Format("{0} / batch", Player.supportUnits_Cost);

		generationRate.text = string.Format("{0} / {1}",BuildTimer.unitMySupport.Count ,Player.recruitment_Limit);

		//if (Player.gold < Player.supportUnits_Cost)
		//{
		//	sUnitBuild_Button.interactable = false;
		//	currentValue.text = "Insufficient gold";
		//	currentValue.color = Color.red;
		//}
		//else if (Player.gold >= Player.supportUnits_Cost)
		//{
		//	sUnitBuild_Button.interactable = true;
		//	currentValue.text = string.Format("{0}g / group", Player.groupCost);
		//	currentValue.color = Color.white;
		//}

		if (Player.forgeLevel < 1)
		{
			currentValue.text = "Unlock Forge";
			currentValue.color = Color.red;
		}

        Check_If_Building_Upgrade_Available();
	}

//	public void Get_Support_Progress()
//	{
//		if (BuildTimer.queuedMySupport.Count >= 1)
//		{
//			float progressPerc = BuildTimer.Instance.SupportBuildProgress_Perc();
//			x = progressPerc;
//			currentFillAmount.rectTransform.sizeDelta = new Vector2(gaugeSize.sizeDelta.x * x,gaugeSize.sizeDelta.y);
//			filledPerc.text = string.Format("{0}% training in progress . ." , Mathf.FloorToInt(x*100));
//		}
//		else if (BuildTimer.queuedMyGroup.Count < 1)
//		{
//			x = 0;
//			currentFillAmount.rectTransform.sizeDelta = new Vector2(gaugeSize.sizeDelta.x * x,gaugeSize.sizeDelta.y);
//			filledPerc.text = "No training in progress . .";
//		}
//	}

	public override void SpeedUpOrMoveToBuilding()
	{
		thisBuilding = name;
		base.SpeedUpOrMoveToBuilding();
	}
}
