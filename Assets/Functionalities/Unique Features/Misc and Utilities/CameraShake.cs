﻿using UnityEngine;

public class CameraShake : MonoBehaviour
{
    public Vector3 axisShakeMin;
    public Vector3 axisShakeMax;
    public float timeOfShake;
    float timeOfShakeStore;
    bool shake;
    Vector3 startPos;
    // Use this for initialization
    void Start()
    {
        shake = false;
        startPos = transform.position;
        timeOfShakeStore = timeOfShake;

        Player.playerHit += TriggerShake;
        Enemy.enemyHit += TriggerShake;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (shake)
        {
            transform.position = startPos + new Vector3(Random.Range(axisShakeMin.x, axisShakeMax.x), Random.Range(axisShakeMin.y, axisShakeMax.y), Random.Range(axisShakeMin.z, axisShakeMax.z));
            timeOfShake -= Time.deltaTime;
            if (timeOfShake <= 0.0f)
            {
                shake = false;
                transform.position = startPos;
            }
        }
    }

    void TriggerShake()
    {
        ShakeCamera();
    }

    void ShakeCamera(float shakeTime = -1.0f)
    {
        if (shakeTime > 0.0f)
        {
            timeOfShake = shakeTime;
        }
        else
        {
            timeOfShake = timeOfShakeStore;
        }
        shake = true;
    }

    void OnDestroy()
    {
        Player.playerHit -= TriggerShake;
        Enemy.enemyHit -= TriggerShake;
    }
}