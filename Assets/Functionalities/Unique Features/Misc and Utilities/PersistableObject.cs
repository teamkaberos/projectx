﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersistableObject : MonoBehaviour 
{
	void Awake ()
	{
		DontDestroyOnLoad (gameObject);
	}
}
