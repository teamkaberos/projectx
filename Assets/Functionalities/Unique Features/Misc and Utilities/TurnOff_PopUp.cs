﻿using UnityEngine;

public class TurnOff_PopUp : MonoBehaviour 
{
    float timer = 4f;

    private void OnEnable()
    {
        Invoke("TurnOff", timer);
    }

    void TurnOff()
    {
        gameObject.SetActive(false);
    }
}
