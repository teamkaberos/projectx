﻿using System.Collections.Generic;
using UnityEngine;

public class HeroModels : MonoBehaviour
{
    public List<GameObject> heroModels;

    public void LoadModels()
    {
        for (int x = 0; x < transform.childCount; x++)
        {
            heroModels.Add(transform.GetChild(x).gameObject);
        }
    }

    public void HideModels()
    {
        for (int x = 0; x < heroModels.Count; x++)
        {
            heroModels[x].SetActive(false);
        }
    }

    public void DisplayHero(string heroName, int heroLevel)
    {
        for (int x = 0; x < heroModels.Count; x++)
        {
            if (heroModels[x].name.Contains(heroName) & !heroModels[x].activeInHierarchy)
            {
                if (heroModels[x].GetComponent<HeroMeshDisplay>() != null)
                {
                    heroModels[x].GetComponent<HeroMeshDisplay>().HideAllLevelUpMeshes();
                    heroModels[x].GetComponent<HeroMeshDisplay>().UpdateDisplayOnMesh(heroLevel);

                }
                heroModels[x].SetActive(true);
            }
        }
    }
}
