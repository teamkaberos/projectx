﻿using UnityEngine;
using UnityEngine.UI;

public class UnitDescrip : MonoBehaviour
{
	public Text descripText;

    public void DisplayDescrip(string descrip)
	{
		descripText.text = descrip;
	}
}
