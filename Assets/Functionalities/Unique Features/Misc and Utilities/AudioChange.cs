﻿using UnityEngine;

public class AudioChange : MonoBehaviour
{
    protected AudioSource aSource;

    public AudioClip homeBGM;
    public AudioClip lodgeBGM;

    public static bool lodgeOn;

    void Start()
    {
        aSource = GetComponent<AudioSource>();
        aSource.loop = true;
    }


    public void Swap()
    {
        lodgeOn = !lodgeOn;

        aSource.Stop();

        if (lodgeOn)
            aSource.PlayOneShot(lodgeBGM);
        else
            aSource.PlayOneShot(homeBGM);
    }
}
