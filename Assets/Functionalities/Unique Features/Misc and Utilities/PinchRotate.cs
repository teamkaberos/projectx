﻿using UnityEngine;

public class PinchRotate : MonoBehaviour
{
    Vector3 FirstPoint; //change type on Vector3
    Vector3 SecondPoint;
    float xAngle = 0.0f; //angle for axes x for rotation
    float yAngle = 0.0f;
    float xAngTemp = 0.0f; //temp variable for angle
    float yAngTemp = 0.0f;


    void Start()
    {    //Initialization our angles of camera    
        //xAngle = transform.rotation.x;
        //yAngle = transform.rotation.y;
        //transform.rotation = Quaternion.Euler(yAngle, xAngle, 0.0f);       

    }

    void LateUpdate()
    {   //Check count touches 
        if (Input.touchCount >= 2)
        {
            //Touch began, save position
            if (Input.GetTouch(0).phase == TouchPhase.Began)
            {
                FirstPoint = Input.GetTouch(0).position;
                xAngTemp = xAngle;
                //yAngTemp = yAngle;
            }
            //Move finger by screen
            if (Input.GetTouch(1).phase == TouchPhase.Moved)
            {
                SecondPoint = Input.GetTouch(1).position;

                //Mainly, about rotate camera. For example, for Screen.width rotate on 180 degree
                xAngle = xAngTemp + (SecondPoint.x - FirstPoint.x) * 180.0f / Screen.width;
                //yAngle = yAngTemp - (SecondPoint.y - FirstPoint.y) * 90.0f / Screen.height;
                if (xAngle < 0)
                    xAngle += 360;

                if (xAngle > 360)
                    xAngle -= 360;
                
                //Rotate camera
                transform.rotation = Quaternion.Euler(34, xAngle, -2);
            }
        }
    }

    #region parallel finger movement rotation
    //if (Input.touchCount >= 2)
    //{   
    ////Touch began, save position
    //if (Input.GetTouch(0).phase == TouchPhase.Began)    
    //{
    //    FirstPoint = Input.GetTouch(0).position;
    //    xAngTemp = xAngle;
    //    yAngTemp = yAngle;
    //}
    ////Move finger by screen
    //if (Input.GetTouch(0).phase == TouchPhase.Moved)
    //{
    //    SecondPoint = Input.GetTouch(0).position;
    //    //Mainly, about rotate camera. For example, for Screen.width rotate on 180 degree
    //    xAngle = xAngTemp + (SecondPoint.x - FirstPoint.x) * 180.0f / Screen.width;
    //    yAngle = yAngTemp - (SecondPoint.y - FirstPoint.y) * 90.0f / Screen.height;
    //    //Rotate camera
    //    transform.rotation = Quaternion.Euler(yAngle, xAngle, 0.0f);
    //}
#endregion
}
