﻿using UnityEngine;
using UnityEngine.UI;

public class ProgressDisplayPanel : MonoBehaviour
{
    public Text whatsNewText;

    [TextArea(3,15)]
    public string[] theNewStuff;

    public void DisplayNew(string newBuilding)
    {
        whatsNewText.text = theNewStuff[BuildTimer.Instance.UpgradingBuildingNo(newBuilding)];

        ToggleWhatsNew(true);
    }

    void ToggleWhatsNew(bool toDisplay)
    {
        whatsNewText.gameObject.SetActive(toDisplay);
    }

    void OnDisable()
    {
        ToggleWhatsNew(false);
    }
}
