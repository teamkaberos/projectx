﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaitToTapOnEnable : MonoBehaviour
{
	public Button toBeWait;

	void OnEnable()
	{
		Invoke ("EnableButton",2f);
	}
	void OnDisable()
	{
		toBeWait.interactable = false;
	}

	void EnableButton()
	{
		toBeWait.interactable = true;
	}
}
