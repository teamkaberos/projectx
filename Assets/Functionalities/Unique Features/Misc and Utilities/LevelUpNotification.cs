﻿using UnityEngine;
using UnityEngine.UI;

public class LevelUpNotification : MonoBehaviour
{
    public GameObject panelToBeActivated;
    public Text playerLevelText;
    public Text minesInfoText;
    public Text clanAvailText;
    public Text castleHealthText;
    public Text heroLVCapText;
    public Text structureLVCapText;

    void Start()
    {
        Player.levelUp += FormatInfo;
    }
    void OnDestroy()
    {
        Player.levelUp -= FormatInfo;
    }

    void FormatInfo(int playerLV)
    {
        playerLevelText.text = PlayerLevelnfo(playerLV);
        clanAvailText.text = ClanAvailability(playerLV);
        minesInfoText.text = MinesInfo(playerLV);
        castleHealthText.text = CastleHealthInfo(playerLV);
        heroLVCapText.text = HeroLvCap(playerLV);
        structureLVCapText.text = StructureLVCap(playerLV);
        panelToBeActivated.SetActive(true);
    }



    // numbers used are levels before increment eg=> 3 >>4 we are using 3 for these methods
    string PlayerLevelnfo(int level)
    {
        return string.Format("Player Level : {0} {1}", level, IncrementTo(level + 1));
    }
    string MinesInfo(int level)
    {
        int currentUnlocked = Player.resourceBuildings_Unlock_Counter[level];
        int newUnlocked = Player.resourceBuildings_Unlock_Counter[level +1];

        if (currentUnlocked == newUnlocked)
            return string.Format("Mines Unlocked : {0}", currentUnlocked);
        return string.Format("Mines Unlocked : {0} {1}", currentUnlocked, IncrementTo(newUnlocked));
    }
    string ClanAvailability(int level)
    {
        if (level == 1)
            return string.Format("Clans : Unavailable {0} Available", IncrementTo());
        return string.Format("Clans : Available");
    }
    string CastleHealthInfo(int level)
    {
        int currentHealth = Player.castleHealth_[level];
        int newHealth = Player.castleHealth_[level];

        if (currentHealth == newHealth)
            return string.Format("Castle Health : {0}", currentHealth);
        return string.Format("Castle Health : {0} {1}", currentHealth, IncrementTo(newHealth));
    }
    string HeroLvCap (int level)
    {
        int currentCap = Mathf.FloorToInt(level / 2) + 1;
        int newCap = Mathf.FloorToInt((level + 1 )/ 2 + 1);

        if (newCap > 5)
            newCap = 5;

        if (currentCap == newCap)
            return string.Format("Hero LV Cap : {0}", currentCap);
        return string.Format("Hero LV Cap : {0} {1}", currentCap, IncrementTo(newCap));
    }
    string StructureLVCap(int level)
    {
        return string.Format("Structure LV Cap : {0} {1}", (level + 1) * 3, IncrementTo(Mathf.Clamp( ((level + 2) * 3), 0, 30) ) );
    }




    #region GREEN ARROWS
    // === New Green Arrow Value === \\
    string IncrementTo()
    {
        return string.Format("<color=#00FF00>>></color>");
    }
    string IncrementTo(int valueToColor)
    {
        return string.Format("<color=#00FF00>>> {0}</color>", valueToColor);
    }
    // === New Green Arrow Value End === \\
    #endregion
}
