﻿using UnityEngine;
using System;
using UnityEngine.SceneManagement;

public class CheckTime : MonoBehaviour 
{
	DateTime timeCheck;

    DateTime dtevening = new DateTime(2000, 2, 20, 20, 20, 20);
    DateTime dtnight = new DateTime(2000, 2, 20, 22, 20, 20);



	public GameObject sunrise;
	public GameObject afternoon;
	public GameObject sunset;
	public GameObject evening;
    public GameObject night;

    public GameObject[] buildingNightLights;

    public GameObject[] torches;

	void Start () 
	{
        timeCheck = DateTime.Now;
        Off_Lights_First();
		CheckTimeOfTheDay ();
	}

    public void ToggleOffAllLights()
    {
        Off_Lights_First();
    }

    void Off_Lights_First()
    {
        sunrise.SetActive(false);
        afternoon.SetActive(false);
        sunset.SetActive(false);
        evening.SetActive(false);
    }

    void OnTorches()
    {
        for (int x = 0; x < torches.Length; x++)
            torches[x].SetActive(true);
    }

    public void CheckTimeOfTheDay()
    {
        if (timeCheck.Hour >= 0 & timeCheck.Hour < 6)
        {
            night.SetActive(true);

            if (SceneManager.GetActiveScene().name.ToLower().Contains("battle"))
                OnTorches();
        }

        else if (timeCheck.Hour >= 6 & timeCheck.Hour < 12)
        {
            sunrise.SetActive(true);
            ToggleBuildingLights(false);
        }
        else if (timeCheck.Hour >= 12 & timeCheck.Hour < 18)
        {
            afternoon.SetActive(true);
            ToggleBuildingLights(false);
        }
        else if (timeCheck.Hour >= 18 & timeCheck.Hour < 20)
        {
            sunset.SetActive(true);
            ToggleBuildingLights(false);
        }
        else if (timeCheck.Hour >= 20 & timeCheck.Hour < 22)
        {
            evening.SetActive(true);
            ToggleBuildingLights(true);
        }
        else if (timeCheck.Hour >= 22 & timeCheck.Hour < 24)
        {
            night.SetActive(true);

            if (SceneManager.GetActiveScene().name.ToLower().Contains("battle"))
                OnTorches();
        }
    }


    void ToggleBuildingLights(bool turnOn)
    {
        for (int i = 0; i < buildingNightLights.Length; i++)
        {
                buildingNightLights[i].SetActive(turnOn);
        }
    }
}   
