﻿using UnityEngine;

public class BackButton : MonoBehaviour 
{
    public delegate void Retreating();
    public static event Retreating retreated;

	public GameObject backPopUp;
    public StageTimer stageTimer;


	public void GoBack()
	{
		backPopUp.SetActive (true);
	}

	public void Back()
    {
        // USED IN BATTLE SCENE BUTTON TO RETURN TO HOME SCENE
        LevelManager.Instance.HomeScene();
        LevelManager.Instance.StoreTotalGameTime();
	}


    public void Retreat()
    {
        if (retreated != null)
            retreated();
        Enemy.Instance.CancelSpawning();
        stageTimer.StopTimer();
        AudioManager.Instance.DefeatJingle();

        GameManager.Instance.gameOverScreen.SetActive(true);
        Player.Instance.UpdatePlayerBattleMissions(false);
        Player.Instance.RepValue(false);
        GameManager.Instance.BattleOutcome(false);
    }
}
