﻿using UnityEngine;
using System;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class CameraMovement : SlideCamera
{
	Vector3 selectedBuilding;
	Vector3 dragOrigin;

    [Space(15)]
    public Transform TopLeftMaxPost;
    public Transform TopRightMaxPost;
    public Transform BottomLeftMaxPost;
    public Transform BottomRightMaxPost;

	public float homeMinXScroll;
	public float homeMaxXScroll;
	public float homeMinZScroll;
    public float homeMaxZScroll;
    public float homeMinYScroll;
    public float homeMaxYScroll;
	public float battleMinScroll;
	public float battleMaxScroll;

    //public Vector3 forgeCamPos;
    [Space(15)]
    public Transform forgeCamPost;
    public Transform townHallCamPost;
    public Transform bunkerCamPost;
    public Transform rangeCamPost;
    public Transform guildCamPost;
    public Transform treasuryCamPost;
    public Transform prepHallCamPost;	
    public Transform tavernCamPost;

    public Transform goldMineAllCamPost;
    public Transform goldMineCamPost0;
    public Transform goldMineCamPost1;
    public Transform goldMineCamPost2;
    public Transform goldMineCamPost3;
    public Transform goldMineCamPost4;

    public Transform tutGuildCamPost;
    public Transform tutRangeCamPost;
    public Transform tutBunkerCamPost;
    public Transform tutTavernPost;
    public Transform tutPrepHallPost;
    public Transform tutTownHallPost;
    public Transform tutTreasuryPost;

	/*wills
	RuntimePlatform platform;
	Vector3 currentPos;
	Vector3 lastPos;
	Vector3 deltaPos;
	*/
    [Space(15)]
    public GameObject tapToClose;		// Basically to check if there are non-UI pop-ups in Home scene
	public GameObject troopAssign;		//If troops assignment is on
	public GameObject popUpTap;			// If player has any UI pop-ups open
	public GameObject playerTab;			// If the player tab is on
	public GameObject lootPopup;			// If the player opens the loot tab
	public GameObject tavernPopup;	// Popup for Tavern
	public GameObject intervalRewardsPopup;
	public GameObject tutorialBlocker;
	public GameObject tutorialTextBlocker;
	public GameObject seigedPopup;
	public GameObject[] focusBlocker;
	public GameObject speedupPopup;
    public GameObject inventoryPopup;

    public Transform cameraStartPoint;

	public GameObject[] buildingsToLerpTo;

    int[] tempMinesLevel = new int[5];


    Camera UICamera;

	RaycastHit hit;


	//	public float perspectiveZoomSpeed = 0.5f;        // The rate of change of the field of view in perspective mode.
	//	public float orthoZoomSpeed = 0.5f;      			  // The rate of change of the orthographic size in orthographic mode.

    protected override void Start()
	{
        //platform = Application.platform;
        // If the current scene is HOME

        base.Start();

        UICamera = transform.Find("UICamera").GetComponent<Camera>();
        //SetCameraCenter();
        camera.transform.position = cameraStartPoint.position;

        cameraPos = camera.gameObject.transform;
	}
    void SetCameraCenter()
    {
        Vector3 minValue = new Vector3(homeMinXScroll, camera.transform.position.y, homeMinXScroll);
        Vector3 maxValue = new Vector3(homeMaxXScroll, camera.transform.position.y, homeMaxZScroll);

        Vector3 middle = Vector3.Lerp(minValue , maxValue , 0.5f);

        camera.transform.position = middle;
    }

	void StartDragging()
	{
		//If the player is not already touching the screen
		if (!dragging)
		{
			//Record the initial position of the touch input
			mouseStartPos = Input.mousePosition;
			//Keep camera's current position
            camStartPos = cameraPos.localPosition;
			//Stores player touching info
			dragging = true;
		}
	}

	public void LerpTowards_UpgradingBuilding()
	{
		Invoke ("InvokedLerp",0.1f);
	}
	public void InvokedLerp()
	{
        if (!BuildTimer.queuedBuilding[0].Contains("Mine"))
            LerpTowards_Building(BuildTimer.queuedBuilding[0],true);
        else
        {
            string buildingToGoTo = BuildTimer.queuedBuilding[0] + UIManager.Instance.goldMineConverter(Player.mine_Upgrading_Now);
            LerpTowards_Building(buildingToGoTo, true);
        }
	}

	bool Check_For_Popup()
	{
		for (int x = 0 ; x < focusBlocker.Length; x++)
		{
			if (focusBlocker[x].activeInHierarchy)
				return true;
		}
		return false;
	}
    //public void LerpTowards_Building(int buttonNo)
    //{
    //    LerpTowards_Building(buttonNo);
    //}
    public void LerpTowards_Building(string buildingToLerpTo)
    {
        LerpTowards_Building(buildingToLerpTo,false);
    }
	public void LerpTowards_Building(string toLerpTo, bool isToLerp)
	{
        UIManager.Instance.CheckWhetherToSlide();
		if (!isDamping)
		{
			isDamping = true;
        }
      
		lerpingTowards = true;
        switch (toLerpTo)
        {
            case "Forge":
                targetPos = forgeCamPost.position;
                break;
            case "TownHall":
                targetPos = townHallCamPost.position;
                break;
            case "Bunker":
                targetPos = bunkerCamPost.position;
                break;
            case "Range":
                targetPos = rangeCamPost.position;
                break;
            case "Guild":
                targetPos = guildCamPost.position;
                break;
            case "Treasury":
                targetPos = treasuryCamPost.position;
                break;
            case "RecruitmentHall":
                targetPos = prepHallCamPost.position;
                break;
            case "tavernCamPos":
                targetPos = tavernCamPost.position;
                break;
            case "Mine1":
                targetPos = goldMineCamPost0.position;
                break;
            case "Mine2":
                targetPos = goldMineCamPost1.position;
                break;
            case "Mine3":
                targetPos = goldMineCamPost2.position;
                break;
            case "Mine4":
                targetPos = goldMineCamPost3.position;
                break;
            case "Mine5":
                targetPos = goldMineCamPost4.position;
                break;

            #region tutorial Lerps
            case "TempBunker":
                targetPos = tutBunkerCamPost.position;
                break;
            case "TempRange":
                targetPos = tutRangeCamPost.position;
                break;
            case "TempGuild":
                targetPos = tutGuildCamPost.position;
                break;
            case "TempHeroLodge":
                targetPos = tutTavernPost.position;
                break;
            case "TempTownHall":
                targetPos = tutTownHallPost.position;
                break;
            case "TempPrep":
                targetPos = tutPrepHallPost.position;
                break;
            case "TempTreasury":
                targetPos = tutTreasuryPost.position;
                break;
            #endregion
        }
        if (isToLerp)
        {
            Invoke("StopLerpingToUpgrade", lerpTime);

            Assign_Last_Selected_Building(toLerpTo);
        }
	}
    public void LerpTowards_Building(int buttonNo_)
    {
        UIManager.Instance.CheckWhetherToSlide();
        if (!isDamping)
        {
            isDamping = true;
        }
        targetPos = goldMineAllCamPost.position;
     
        Invoke("StopLerpingToUpgrade", lerpTime);

        Assign_Last_Selected_Building(buttonNo_);
    }

	public void Assign_Last_Selected_Building(string lerpingTowards)
	{
		BuildingManager.Instance.Switch_Last_Selected();
		for (int x = 0; x < buildingsToLerpTo.Length; x++)
		{
            if (buildingsToLerpTo[x].name.Contains(lerpingTowards))
			{
				BuildingManager.lastSelectedBuillding = buildingsToLerpTo[x];
                if (lerpingTowards.Contains("Mine"))
                {
                    lerpingTowards = lerpingTowards.Substring(lerpingTowards.Length - 1);
                    Player.mine_That_Player_Last_Tapped_On = int.Parse(lerpingTowards);
                }
				break;
			}
		}
    }
    public void Assign_Last_Selected_Building(int buttonNo)
    {
        BuildingManager.Instance.Switch_Last_Selected();

        tempMinesLevel = Player.mines;
        Array.Sort(tempMinesLevel);
        Array.Reverse(tempMinesLevel);
        int tracker = 0;
        int tempButtonNo = buttonNo;

        for (int x = 0; x < UIManager.Instance.goldMineButton_.Length; x++)
        {
            if (x <= tempButtonNo)
            {
                if (UIManager.Instance.goldMineButton_[x].IsInteractable() == false)
                    buttonNo -= 1;
                if (tempButtonNo == x)
                    break;
            }
        }

        for (int x = 0; x < Player.mines.Length; x++)
        {
            if (Player.mines[x] >= (Player.castleLevel * 3 + 3))
                continue;

            if (tracker == buttonNo)
            {
                int value = x + 1;
                LerpTowards_Building("Mine" + value);
                break;
            }
            tracker += 1;
        }
    }


	public void StopLerpingToUpgrade()
	{
        cameraPos.position = targetPos;
		lerpingTowards = false;
		UIManager.Instance.HideMiniInfo();
        UIManager.Instance.Lerp_To_Building_MiniPopup();
        if (Player.tutorial >= Player.tutorialCap)
        {
            if (!BuildingManager.lastSelectedBuillding.name.Contains("Mine"))
                BuildingManager.lastSelectedBuillding.GetComponent<HomeBuildings>().SelectThisBuilding();
            else
                BuildingManager.lastSelectedBuillding.GetComponent<ResourceGenerate>().SelectThisBuilding();  
        }
	}

    void CancelLerp()
    {
        if (lerpingTowards)
            lerpingTowards = false;
    }

	void LateUpdate () 
	{
        CancelLerp();
        CancelInvoke("StopLerpingToUpgrade");
        
        //FOV40-70

		//If player releases touch
		if (Input.GetMouseButtonUp(0))
		{
			dragging = false;
			counter = 0;
			popup_Blocked = false;
        }

		//If there is a touch input
		if (Input.GetMouseButton(0))
		{
            //Disallow dragging if the Player is still in the tutorial
           if (Player.tutorial < Player.tutorialCap)
                return;

            RaycastHit hit;
            Ray ray = UICamera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit))
            {
                //Debug.Log(hit.transform.name + hit.transform.GetType());
                if (hit.transform.name == "Scroll_Blocking")
                    return;
            }

			if (tapToClose.activeInHierarchy || popUpTap.activeInHierarchy || playerTab.activeInHierarchy || 
				lootPopup.activeInHierarchy || tavernPopup.activeInHierarchy || troopAssign.activeInHierarchy || 
			    intervalRewardsPopup.activeInHierarchy || tutorialBlocker.activeInHierarchy || speedupPopup.activeInHierarchy
                || seigedPopup.activeInHierarchy || inventoryPopup.activeInHierarchy || Check_For_Popup() == true)
			{
				return;
			}
		}

		if (!Input.GetMouseButton(0)) return;
		{
            if (Input.touchCount > 1)
                return;
            
			//wait for a while before assuming drag
			StartDragging();
			//record inital touch position
			//if finger is still on screen
			if (dragging)
			{
				//difference between start position and current position
				//from the player's first touch to current point
				//&& storing it in vector3
				Vector3 t = mouseStartPos - Input.mousePosition;
				//Checks current scene name
				if (SceneManager.GetActiveScene().name.Contains("Home"))
				{
//					transform.localPosition = new Vector3 (Mathf.Clamp((camStartPos.x + (t.x/2)) ,homeMinXScroll, homeMaxXScroll)
//													, transform.localPosition.y, 
//													Mathf.Clamp((camStartPos.z + (t.y/2)),homeMinZScroll, homeMaxZScroll));

					targetPos = new Vector3 (Mathf.Clamp ( (camStartPos.x + (t.x * dampFloat))  ,homeMinXScroll, homeMaxXScroll )
                                             , camStartPos.y, 
					Mathf.Clamp((camStartPos.z + (t.y * dampFloat)),homeMinZScroll, homeMaxZScroll));
					
					isDamping = true;
				}
			}
		}
	// PINCH ZOOM
//		// If there are two touches on the device...
//		if (Input.touchCount == 2)
//		{
//			// Store both touches.
//			Touch touchZero = Input.GetTouch(0);
//			Touch touchOne = Input.GetTouch(1);
//
//			// Find the position in the previous frame of each touch.
//			Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
//			Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;
//
//			// Find the magnitude of the vector (the distance) between the touches in each frame.
//			float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
//			float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;
//
//			// Find the difference in the distances between each frame.
//			float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;
//
//			// If the camera is orthographic...
//			if (camera.isOrthoGraphic)
//			{
//				// ... change the orthographic size based on the change in distance between the touches.
//				camera.orthographicSize += deltaMagnitudeDiff * orthoZoomSpeed;
//
//				// Make sure the orthographic size never drops below zero.
//				camera.orthographicSize = Mathf.Max(camera.orthographicSize, 0.1f);
//			}
//			else
//			{
				// Otherwise change the field of view based on the change in distance between the touches.
//
//				camera.fieldOfView += deltaMagnitudeDiff * perspectiveZoomSpeed;
//
//				// Clamp the field of view to make sure it's between 0 and 80.
//				camera.fieldOfView = Mathf.Clamp(camera.fieldOfView, 10.1f, 89.9f);
//
//				cameraPos.position = new Vector3 (cameraPos.position.x, cameraPos.position.y += deltaMagnitudeDiff, cameraPos.position.z -= deltaMagnitudeDiff);
//			}
//		}
	}

    protected override void Update()
	{		
		if (Input.GetMouseButton(0))
		{
			counter += Time.deltaTime;
			if (counter>= countAsDraggingTimer)
			{
				popup_Blocked = true;
			}
		}

		if (isDamping)
		{
            cameraPos.localPosition = Vector3.SmoothDamp(cameraPos.localPosition,targetPos,ref velocity,smoothTime);
            if (cameraPos.localPosition == targetPos)
			{
				isDamping = false;
			}
		}
	}



//	void Update()
//	{
//		if (Input.GetMoIncreaseFlamesButtonDown(0))
//		{
//			if (tapToClose != null && tapToClose.gameObject.activeInHierarchy)
//			{
//				return;
//			}
//
//			dragOrigin = Input.moIncreaseFlamesPosition;
//			return;
//		}
//
//		if (!Input.GetMoIncreaseFlamesButton(0)) return;
//
//		Vector3 pos = Camera.main.ScreenToViewportPoint(Input.moIncreaseFlamesPosition - dragOrigin);
//		Vector3 move = new Vector3(pos.x * dragSpeed, 0, 0);
//
//		transform.Translate(move, Space.World);  
//	}
/*
	// Will
	void Update () 
	{
		if (platform != RuntimePlatform.Android || platform != RuntimePlatform.IPhonePlayer) 
		{
			if (Input.GetMoIncreaseFlamesButton (0)) {
				// If there is a pop-up object and is active
				// camera will not move
				if (tapToClose != null && tapToClose.gameObject.activeInHierarchy) {
					return;
				}
				float h = horizontalSpeed * Input.GetAxis ("MoIncreaseFlames X");
				transform.Translate (h, 0, 0);


				// Camera movement restriction

				if (SceneManager.GetActiveScene ().name == "2_Home") {
					transform.position = new Vector3 (
						Mathf.Clamp (transform.position.x, -75, 75),
						Mathf.Clamp (transform.position.y, 0, 0),
						Mathf.Clamp (transform.position.z, -75, 75)
					);
				} else if (SceneManager.GetActiveScene ().name == "3_Battle") {
					transform.position = new Vector3 (
						Mathf.Clamp (transform.position.x, -800, 800),
						Mathf.Clamp (transform.position.y, 600, 600),
						Mathf.Clamp (transform.position.z, -2100, 2100)
					);
				}
			}
		}

		if (platform == RuntimePlatform.Android) 
		{
			if (Input.touchCount > 0) 
			{
				currentPos = Input.GetTouch (0).position;
				currentPos = Input.moIncreaseFlamesPosition;
				deltaPos = currentPos - lastPos;
				lastPos = currentPos;

				transform.Translate (deltaPos);
			}
			if (SceneManager.GetActiveScene ().name == "2_Home") {
				transform.position = new Vector3 (
					Mathf.Clamp (transform.position.x, -75, 75),
					Mathf.Clamp (transform.position.y, 0, 0),
					Mathf.Clamp (transform.position.z, -75, 75)
				);
			} else if (SceneManager.GetActiveScene ().name == "3_Battle") {
				transform.position = new Vector3 (
					Mathf.Clamp (transform.position.x, -800, 800),
					Mathf.Clamp (transform.position.y, 600, 600),
					Mathf.Clamp (transform.position.z, -700, -700)
				);
			}
		}

	}
	*/

}