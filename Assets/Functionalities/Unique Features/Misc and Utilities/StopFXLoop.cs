﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopFXLoop : MonoBehaviour 
{
	public GameObject fxToStop;

    public void StopLoop()
	{
		fxToStop.GetComponent<ParticleSystem>().loop = false;
		
		for (int x = 0; x < fxToStop.transform.childCount; x++)
		{
			fxToStop.transform.GetChild(x).GetComponent<ParticleSystem>().loop = false;
		}
	}

}
