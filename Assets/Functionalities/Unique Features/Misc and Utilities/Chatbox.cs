﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chatbox : MonoBehaviour
{	
	public GameObject chatbox;			// ChatBox gameObject
	public GameObject tapToClose;		// TapToClose BG

	bool isOpened;								// Checking if chatbox is opened;

	public void ToggleChatBox()
	{
		if (!isOpened)
		{
			chatbox.transform.localPosition = new Vector2 (chatbox.transform.localPosition.x , -150);
			chatbox.transform.SetAsLastSibling();
			tapToClose.SetActive(true);
			isOpened = true;
		}
		else
		{
			CloseChatBox();
		}
	}
	// Function to close Chat Box in case TapToClose
	// is opened by another pop-up
	public void CloseChatBox()
	{
		chatbox.transform.localPosition = new Vector2 (chatbox.transform.localPosition.x , -1050);
		chatbox.transform.SetAsFirstSibling();
		tapToClose.SetActive(false);
		isOpened = false;
	}
}
