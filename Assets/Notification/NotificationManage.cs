﻿using System;
using UnityEngine.UI;
using UnityEngine;

public class NotificationManage : MonoBehaviour
{
    public Toggle openNotificationToggle;
    public GameObject notificationDisplayPanel;
    [Space(20)]
    public Toggle[] pageToggle;
    public GameObject[] notificationItems;
    [Space(20)]
    public Button nextPage;
    public Button previousPage;

    int pageNo;

    private void Start()
    {
        openNotificationToggle.onValueChanged.AddListener(DisplayNotificationPanel);

        nextPage.onClick.AddListener(delegate { ScrollPage(1); });
        previousPage.onClick.AddListener(delegate { ScrollPage(-1); });

        for (int x = 0; x < pageToggle.Length; x++)
        {
            pageToggle[x].onValueChanged.AddListener(ToggleItemCheck);
        }
        if (!PlayerPrefs.HasKey("SUnitNotification"))
            DataManager.DataDistributed += AutoToggle;
    }

    void AutoToggle()
    {
        PlayerPrefs.SetInt("SUnitNotification", 1);
        openNotificationToggle.isOn = true;
        DataManager.DataDistributed -= AutoToggle;
    }
    void Display()
    {
        DisplayNotificationPanel(true);
    }

    void ToggleItemCheck(bool isOn_)
    {
        for (int x = 0; x < pageToggle.Length; x++)
        {
            if (pageToggle[x].isOn)
            {
                pageNo = x;
                TogglePage(x);
                break;
            }
        }
    }


    void DisplayNotificationPanel(bool toOn)
    {
        TogglePage(0);
        notificationDisplayPanel.SetActive(toOn);
    }


    void ScrollPage(int toModify)
    {
        pageNo += toModify;
        if (pageNo > notificationItems.Length - 1)
            pageNo = 0;
        else if (pageNo < 0)
            pageNo = notificationItems.Length - 1;

        pageToggle[pageNo].isOn = true;
    }

    void TogglePage(int pageNo_)
    {
        for (int x = 0; x < notificationItems.Length; x++)
        {
            notificationItems[x].SetActive(x == pageNo_);
        }
    }
}