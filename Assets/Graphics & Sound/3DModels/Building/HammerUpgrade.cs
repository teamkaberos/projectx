﻿using UnityEngine;

public class HammerUpgrade : MonoBehaviour
{
    public FXPlay hammerSparkle;

    public void PlaySparkle()
    {
        hammerSparkle.PlayEffect();
    }
}
