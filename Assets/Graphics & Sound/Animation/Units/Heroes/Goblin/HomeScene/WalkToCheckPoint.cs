﻿using UnityEngine;
using UnityEngine.AI;

public class WalkToCheckPoint : MonoBehaviour
{
    public bool toTreasury = true;
    public Transform[] checkPoints;     // 0 = treasury , 1 = mine , 2 = hidden
    NavMeshAgent thisUnit;
    public bool toHide;

    void Start()
    {
        thisUnit = GetComponent<NavMeshAgent>();
        thisUnit.SetDestination(SwapCheckPoint().position);
    }

    void Update()
    {
        if (thisUnit.transform.position == thisUnit.destination && gameObject.activeInHierarchy)
        {
            if (toHide)
            {
                MoveAway();
                return;
            }

            toTreasury = !toTreasury;
            thisUnit.SetDestination(SwapCheckPoint().position);
        }
    }

    Transform SwapCheckPoint()
    {
        if (toTreasury)
        {
            return checkPoints[0];
        }
        toHide = true;
        return checkPoints[1];
    }

    void MoveAway()
    {
        toHide = false;
        thisUnit.transform.position = checkPoints[2].position;
        Invoke("Return", 2f);
        gameObject.SetActive(false);
    }
    void Return()
    {
        thisUnit.transform.position = checkPoints[1].position;
        gameObject.SetActive(true);
    }
}