﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Text_Fade : MonoBehaviour 
{
	Text thisText;
	Vector4 startColor = new Vector4 (1,1,1,1);

	public float duration = 1.5f;

	float fadeFloat = 1;

	void Awake()
	{
		thisText = GetComponent<Text>();

	}

	void OnEnable()
	{
		thisText.color = startColor;
	}

	void OnDisable()
	{
		fadeFloat = 1;
	}

	void Fade()
	{
		thisText.color = new Vector4 (1,1,1,fadeFloat);
	}

	void Update()
	{
		fadeFloat -= (Time.deltaTime/duration);
		Fade();
		if (fadeFloat <= 0)
		{
			this.gameObject.SetActive(false);
		}
	}
}
