﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtText : MonoBehaviour 
{
	Transform lookAtMe;

	void OnEnable()
	{
		lookAtMe = GameObject.Find("Main Camera").transform;
	}

	void Update()
	{
		this.transform.LookAt(lookAtMe);
	}
}
