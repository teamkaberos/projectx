﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DelegateTest : MonoBehaviour 
{
	public delegate void OnChangeTime(int timer);
	public OnChangeTime timeChanged;

	void Start () 
	{
		timeChanged += ChangeTime;
	}

	void ChangeTime(int myNewTime)
	{
		print (myNewTime + " time");
	}


}
