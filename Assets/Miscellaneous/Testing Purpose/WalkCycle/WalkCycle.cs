﻿using UnityEngine;

public class WalkCycle : MonoBehaviour
{
    public Transform toGoTo;

    void OnTriggerEnter(Collider col)
    {
        col.gameObject.transform.position = toGoTo.transform.position;
    }

}
