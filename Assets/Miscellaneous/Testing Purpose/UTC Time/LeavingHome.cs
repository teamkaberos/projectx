﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;

public class LeavingHome : MonoBehaviour {

	TimeSpan result;

	public void TrainUnits ()
	{
		result = GlobalCountdown.totalTime - (DateTime.UtcNow - GlobalCountdown.timeStarted);
		if ((float)result.TotalSeconds < 0) {
			GlobalCountdown.StartCountdown (TimeSpan.FromSeconds (10));
			print ("Training");
		} 
		else 
		{
			CheckTimeLeft ();
		}
	}

	public void CheckTimeLeft()
	{
		print ("Time Left: " + result);
	}

	public void Return()
	{
		SceneManager.LoadSceneAsync ("TimeTest");
	}

	void Update()
	{
		
	}
}
