﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public static class GlobalCountdown
{
	public static DateTime timeStarted;
	public static TimeSpan totalTime;

	public static void StartCountdown(TimeSpan time)
	{
		timeStarted = DateTime.UtcNow;
		totalTime = time;
	}

//	public static TimeSpan TimeLeft
//	{
//		get
//		{
//			var result = totalTime - (DateTime.UtcNow - timeStarted);
//			if (result.TotalSeconds <= 0) 
//			{
//				Test.troops += 1;
//				Debug.Log ("Now you have: " + Test.troops + " troops.");
//				return TimeSpan.Zero;
//			} else 
//			{
//				Debug.Log ("Still running countdown");
//			}
//			return result;
//		}
//	}

}
