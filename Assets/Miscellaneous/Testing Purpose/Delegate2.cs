﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Delegate2 : MonoBehaviour 
{
	DelegateTest dgt;

	// Use this for initialization
	void Start () 
	{
		dgt = GetComponent<DelegateTest>();
		dgt.timeChanged += TimeChanger;
	}

	void TimeChanger(int myTimer)
	{
		int newTime = myTimer * 2;
		print (			newTime);
	}

	void Update()
	{
		if (Input.GetKey(KeyCode.G))
		{
			dgt.timeChanged(55);
		}
		if (Input.GetKey(KeyCode.Y))
		{
			dgt.timeChanged(25);
		}
	}
}
