﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Attached to the lane buttons
public class SpawnButtonDefender : MonoBehaviour {

	// Basically to identify which lane the unit is spawning at
	public int locIndex;
	bool onCooldown;
	static float laneCooldown = 4;
	float cooldownDuration = laneCooldown;
	public Text cooldownDisplay;


	public void SpawnUnit()
	{
		if (GameManager.storedUnitIndexDefender != null)
		{
			// Parses the stored unit information into a temp variable
			int unitIndex = int.Parse(GameManager.storedUnitIndexDefender);
			string race = (string)Player2.Instance.GetType ().GetField ("race").GetValue (Player2.Instance);
			GameManager gameManager = GameManager.Instance;
			Transform[] unitSet = new Transform[3];
			unitSet = (Transform[])gameManager.GetType ().GetField (race + "Units").GetValue (gameManager);
			Player2.Instance.GetUnitLoc (this.locIndex);
			// Put the instance of UnitButton that was assigned previously on cooldown
			GameManager.toBeDisabledDefender.gameObject.GetComponent<UnitButtonDefender> ().SpawnBoolToTrue (0);
			Transform spawnies = Instantiate (unitSet [unitIndex], new Vector3 (Player2.Instance.unitLocs[Player2.Instance.getLocIndex].position.x, Player2.Instance.unitLocs[Player2.Instance.getLocIndex].position.y, Player2.Instance.unitLocs[Player2.Instance.getLocIndex].position.z), Quaternion.identity) as Transform;
			JustTransformStuffs(spawnies);
			Player2.Instance.CheckCooldown ();
			// Set the stored info to null to prevent respawning unit before pressing the unit button again
			GameManager.storedUnitIndexDefender = null;
			// To put the selected lane button on cooldown
			DisableButton ();
		}
	}
		
	void JustTransformStuffs(Transform spawnie)
	{
		spawnie.SetParent (Player2.Instance.unitLocs [Player2.Instance.getLocIndex]);
		spawnie.transform.localScale = Vector3.one;
		if (spawnie.gameObject.GetComponent<Unit> ().unitClass == "Archer") 
		{
			spawnie.transform.localScale = new Vector3 (1.3f, 1.3f, 1.3f);
		}
		if (spawnie.gameObject.GetComponent<Unit> ().unitClass == "Knight") 
		{
			spawnie.transform.localScale = new Vector3 (1.3f, 1.3f, 1.3f);
		}
		spawnie.gameObject.GetComponent<Unit> ().isFriendlyUnit = false;
		// Because we need rigidbody for OnTriggerFunction, but at the same time we don't need physics for our game
		spawnie.gameObject.AddComponent<Rigidbody> ().useGravity = false;
	}
		
	void Update()
	{
		Cooldown ();
	}

	public void DisableButton()
	{
		this.gameObject.GetComponent<Button> ().interactable = false;
		onCooldown = true;
	}

	public void Cooldown()
	{
		if (onCooldown == true) 
		{
			cooldownDuration -= Time.deltaTime;
			cooldownDisplay.text = "" + (int)cooldownDuration;
			// More specifically, if the cooldown is 0.9, sets it to 0
			if (cooldownDuration <= 1)
			{
				cooldownDuration = 0;
				cooldownDisplay.text = null;
				onCooldown = false;
				this.gameObject.GetComponent<Button> ().interactable = true;
				cooldownDuration = laneCooldown;
			}
		}
	}
		
}
