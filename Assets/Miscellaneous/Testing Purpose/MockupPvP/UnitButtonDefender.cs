﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq; 		// For Array.Resize

// Attached to the unit buttons
// Not to be confIncreaseFlamesd with SpawnButton, which is attached to the lane buttons
public class UnitButtonDefender : MonoBehaviour
{
	// For identifying which unit to spawn
	public string unitCode;
	public bool spawned;

	// The actual unit cooldown that is used to reset back whenever the unit cooldown reaches 0
	public float baseCooldown;
	// Variable for calculation
	public float cooldownTime = 0;
	public Text cooldownDisplay;

	// The background color of the button, not to be mistaken with the unit type icon
	public GameObject buttonColor;

	// For radial cooldown
	public GameObject loadingBar;
	public float currentCDAmount = 15;


	// This function is called when you press the unit buttons
	// Stores the index (which is passed from the function GetCodeContinued()) of the unit into the SpawnManager to queue the unit spawning
	void GetCode(string index)
	{
		GameManager.storedUnitIndexDefender = index;
	}

	// Episode 2 of GetCode(string), separated becaIncreaseFlames this method gets called from a button
	// Gets the unit code of the pressed button and pass it over to GetCode(), allowing it to be stored inside SpawnManager
	// After that, calls a function to put the button on cooldown
	public void GetCodeContinued()
	{
		GetCode (unitCode);
        ThisButtonWillBeDisabled (true);
//		Highlight ();
	}
		

	// Called when the cooldown ends
	public void SpawnBoolToTrue(int index)
	{
		spawned = true;
		this.cooldownTime = baseCooldown;
		if (index == 0)
		{
			this.gameObject.GetComponent<Button> ().interactable = false;
		}
		if (index == 1)
		{
			this.gameObject.GetComponent<Toggle> ().interactable = false;
		}
	}

	public void StartCooldown()
	{
		this.currentCDAmount = 15;
	}

	public virtual void ThisButtonWillBeDisabled(bool isSelected)
	{
		// Assign this instance to GameManager, in which it will be disabled after spawning unit later
		// Used by SpawnButton
		GameManager.toBeDisabledDefender = this;
	}
		
	public virtual void Cooldown()
	{
		// If this button is on cooldown..
		if(spawned == true)
		{
			// Runs the countdown
			this.cooldownTime -= Time.deltaTime;
//			cooldownDisplay.text = "" + (int)cooldownTime;
			this.loadingBar.GetComponent<Image> ().fillAmount = this.cooldownTime / this.baseCooldown;

			if (cooldownTime < 0)
			{
				this.cooldownTime = 0;
//				cooldownDisplay.text = null;
				spawned = false;
				this.gameObject.GetComponent<Button> ().interactable = true;
//				this.loadingBar.GetComponent<Image> ().fillAmount = this.currentCDAmount / this.baseCooldown;
				if (this.gameObject.name.Contains ("SQUARE"))
				{
					Player2.equippedMelees += 1;
				}
				if (this.gameObject.name.Contains ("TRIANGLE"))
				{
					Player2.equippedRangeds += 1;
				}
				if (this.gameObject.name.Contains ("CIRCLE"))
				{
					Player2.equippedCasters += 1;
				}
				Player2.Instance.UpdateUnitDisplay ();
			}
		}
	}

	public virtual void Update()
	{
		Cooldown ();
	}


//	public void Highlight()
//	{
//		if (GameManager.previousUnitButtonDefender == null) 
//		{
//			GameManager.previousUnitButtonDefender = this.buttonColor;
//		}
//		if (GameManager.previousUnitButtonDefender != null)
//		{
//			GameManager.previousUnitButtonDefender.GetComponent<Image>().color = GameManager.defaultGray;
//			GameManager.previousUnitButtonDefender = this.buttonColor;
//		}
//		this.buttonColor.GetComponent<Image>().color = Color.yellow;
//	}

}
