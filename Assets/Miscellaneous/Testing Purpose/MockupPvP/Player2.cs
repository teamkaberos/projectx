﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using System.Linq;
//using System.Reflection;
using System;
using UnityEngine.SceneManagement;

public class Player2 : MonoBehaviour {

	#region Singleton
	private static Player2 _instance;
	public static Player2 Instance
	{
		get
		{
			if (_instance == null) 
			{
				GameObject player2 = new GameObject ("EnemyCastle");
				player2.AddComponent<Player2> ();
			}
		return _instance;
		}
	}
	#endregion

	// ----------------------------------- VARIABLES ------------------------------------ \\

	#region variables: Home Screen

	public Text castleBuildingLv;

	public static int recruitmentHallLv = 1;
	public Text recruitmentHallLvDisplay;

	public static int bunkerLv = 1;
	public Text bunkerLvDisplay;
	public static int rangeLv = 1;
	public Text rangeLvDisplay;
	public static int guildLv = 1;
	public Text guildLvDisplay;

	public static int Melees;
	public static int Rangeds;
	public static int Casters;
	public Text MeleesDisplay;
	public Text RangedsDisplay;
	public Text CastersDisplay;

	public static int gold = 100;
	public static int lumber = 100;
	public static int diamond = 50;
	public Text goldDisplay;
	public Text lumberDisplay;
	public Text diamondDisplay;

	public static int goldCap = 250;
	public static int lumberCap = 250;

	#endregion

	#region variables: Battle Scene

	//for SpawnButton
	public Transform[] unitLocs;
	public int getLocIndex;

	public static int equippedMelees = 2;
	public static int equippedRangeds = 2;
	public static int equippedCasters = 2;
	public Text equippedMeleesDisplay;
	public Text equippedRangedsDisplay;
	public Text equippedCastersDisplay;

	public Text meleesAmountBattle;
	public Text rangedsAmountBattle;
	public Text castersAmountBattle;

	public static int equippedUnits;

	public GameObject[] squareButtons;
	public GameObject[] triangleButtons;
	public GameObject[] circleButtons;

	public float[] squareCooldowns;
	public float[] triangleCooldowns;
	public float[] circleCooldowns;

	public static int medalCharges;
	public GameObject[] medals;


	float minCooldown;
	GameObject withMinCooldown;

	public static int unitCap = 8;


	#endregion

	#region For Testing

	public float squareCDFormula;
	public float triangleCDFormula;
	public float circleCDFormula;

	#endregion

	// ====================================================================== VARIABLES END \\

	#region MonoBehavior

	void Awake()
	{
		// -- Singleton Initialization -- \\
		if (_instance == null)
		{
			_instance = this;
		}
		equippedUnits = equippedMelees + equippedRangeds + equippedCasters;
		UpdateUnitDisplay ();
		if (SceneManager.GetActiveScene().name == "MockupPvP")
		{
			equippedMelees = 2;
			equippedRangeds = 2;
			equippedCasters = 2;
			CooldownLvCalculation ();
//			InitializeCooldowns (Player.Instance.squareCDFormula, Player.Instance.triangleCDFormula, Player.Instance.circleCDFormula);
			InitializeUnitButtons ();

//			IncreaseMedalsDisplay ();

			meleesAmountBattle.text = "" + equippedMelees;
			rangedsAmountBattle.text = "" + equippedRangeds;
			castersAmountBattle.text = "" + equippedCasters;
		}
	}

	#endregion

	// -------------------------------- BATTLE SCENE -------------------------------------- \\

	public void GetUnitLoc(int i)
	{
		getLocIndex = i;
	}

	void InitializeUnitButtons()
	{
		for (int i = 0; i < equippedMelees; i++) 
		{
			squareButtons [i].gameObject.SetActive (true);
		}
		for (int i = 0; i < equippedRangeds; i++) 
		{
			triangleButtons [i].gameObject.SetActive (true);
		}
		for (int i = 0; i < equippedCasters; i++) 
		{
			circleButtons [i].gameObject.SetActive (true);
		}
		Array.Resize (ref squareButtons, equippedMelees);
		Array.Resize (ref triangleButtons, equippedRangeds);
		Array.Resize (ref circleButtons, equippedCasters);
	}

	public void CheckCooldown()
	{
		string getUnitClass = GameManager.storedUnitIndexDefender;
		if (getUnitClass == "0") 
		{
			getUnitClass = "square";
			equippedMelees -= 1;
		}
		if (getUnitClass == "1")
		{
			getUnitClass = "triangle";
			equippedRangeds -= 1;
		}
		if (getUnitClass == "2") 
		{
			equippedCasters -= 1;
			getUnitClass = "circle";
		}
		UpdateUnitDisplay();
		Player2 playerS = Player2.Instance;

		GameObject[] getArrayOfClass = new GameObject[12];
		getArrayOfClass = (GameObject[])playerS.GetType().GetField(getUnitClass + "Buttons").GetValue (playerS);

		UnitButtonDefender[] getAllUnitButtons = new UnitButtonDefender[getArrayOfClass.Length];

		for (int i = 0; i < getArrayOfClass.Length; i++)
		{
			getAllUnitButtons [i] = getArrayOfClass [i].GetComponentInChildren<UnitButtonDefender> ();
		}
		BringObjectToFront (getAllUnitButtons);
	}

	public void BringObjectToFront(UnitButtonDefender[] allUnitButtons)
	{
		MinCooldown (allUnitButtons).transform.SetAsLastSibling();
	}
		
	public GameObject MinCooldown(UnitButtonDefender[] unitButtons)
	{
		GameObject go = null;
		float minCooldown = 15;
		foreach (UnitButtonDefender ub in unitButtons) 
		{
			float cd = ub.cooldownTime;
			if (cd < minCooldown || cd == 0)
			{
				minCooldown = cd;
				go = ub.gameObject;
			}
		}
		return go;
	}
   
	void CooldownLvCalculation()
	{
		squareCDFormula = 15 - ((bunkerLv - 1) * 0.05f);
		triangleCDFormula = 15 - ((rangeLv - 1) * 0.05f);
		circleCDFormula = 15 - ((guildLv - 1) * 0.05f);
	}

//	void InitializeCooldowns(float squareFormula, float triangleFormula, float circleFormula)
//	{
//		Player2.Instance.squareCooldowns = new float[Player2.equippedMelees];
//		for (int i = 0; i < Player2.Instance.squareCooldowns.Length; i++) 
//		{
//			Player2.Instance.squareCooldowns [i] = squareFormula;
//			squareButtons [i].GetComponent<UnitButtonDefender> ().baseCooldown = squareCooldowns [i];
//		}
//		Player2.Instance.triangleCooldowns = new float[Player2.equippedRangeds];
//		for (int i = 0; i < Player.Instance.triangleCooldowns.Length; i++) 
//		{
//			Player2.Instance.triangleCooldowns [i] = triangleFormula;
//			triangleButtons [i].GetComponent<UnitButtonDefender> ().baseCooldown = triangleCooldowns [i];
//		}
//		Player2.Instance.circleCooldowns = new float[Player2.equippedCasters];
//		for (int i = 0; i < Player.Instance.circleCooldowns.Length; i++) 
//		{
//			Player2.Instance.circleCooldowns [i] = circleFormula;
//			circleButtons [i].GetComponent<UnitButtonDefender> ().baseCooldown = circleCooldowns [i];
//		}
//	}

//	public void UseMedals(int deductedAmt)
//	{
//		int previousCharge = medalCharges;
//		medalCharges = previousCharge - deductedAmt;
//		for (int i = previousCharge; i > medalCharges; i--) 
//		{
//			medals [i - 1].SetActive (false);
//		}
//	}
//
//	public void IncreaseMedalsCharge()
//	{
//		if (medalCharges < 9) 
//		{
//			medalCharges += 1;
//		}
//	}
//
//	public void IncreaseMedalsDisplay()
//	{
//		for (int i = 0; i < medalCharges; i++) 
//		{
//			medals [i].SetActive (true);
//		}
//	}
		

	public void UpdateUnitDisplay()
	{
		equippedMeleesDisplay.text = "" + equippedMelees;
		equippedRangedsDisplay.text = "" + equippedRangeds;
		equippedCastersDisplay.text = "" + equippedCasters;
	}
		

	#region From BaseManager

	// --- PLAYERDATA STUFFS --- \\

	public string playerName;
	public Text nameDisplay;

	public int reputation;
	public Text reputationDisplay;

	public string clan;


	// =========================== \\

	public string race;

	public Transform[] flags;

	public int castleLevel = 1;
	public int castleHealth = 5;
	public Text castleLevelDisplay;

	bool stillUnderAttack;


	void Start () 
	{
		if (SceneManager.GetActiveScene ().name == "3_Battle" || SceneManager.GetActiveScene().name == "MockupPvP")
			AssignFlags ();
	}

	// -------------------------------- BATTLE SCENE -------------------------------- \\

	public void Damage(int dmgAmt)
	{
		this.castleHealth -= dmgAmt;
		if (this.castleHealth <= 0) 
		{
			Destroy (gameObject);
			if (this.GetComponent<BaseUnit>().isFriendlyUnit == true)
				GameManager.Instance.gameOverScreen.SetActive (true);
			else if (this.GetComponent<BaseUnit>().isFriendlyUnit == false)
				//GameManager.Instance.victoryScreen.SetActive (true);
			Time.timeScale = 0;

//			Player.UnitExpended ();
		}
	}

	void AssignFlags()
	{
		for (int i = 0; i < this.castleHealth; i++) 
		{
			flags [i].gameObject.SetActive (true);
		}
	}

	// When a unit reaches a castle
	void OnTriggerEnter(Collider other)
	{
		if (this.name.Contains ("Castle") & stillUnderAttack == false) 
		{
			// ENEMY CASTLE DAMAGE
			if (other.GetComponent<BaseUnit> ().isFriendlyUnit == true & this.gameObject.GetComponent<BaseUnit> ().isFriendlyUnit == false) {
				if (SceneManager.GetActiveScene ().name == "3_Battle") 
				{
					Damage (1);
					Enemy enemyCastle = this.GetComponent<Enemy> ();
					if (enemyCastle != null)
						enemyCastle.flags [enemyCastle.castleHealth].gameObject.SetActive (false);

					this.stillUnderAttack = true;
				}

				// For Mockup PvP
				else if (SceneManager.GetActiveScene().name == "MockupPvP")
				{
					Damage (1);
					Player2 defenderCastle = this.GetComponent<Player2> ();
					if (defenderCastle != null)
						defenderCastle.flags [defenderCastle.castleHealth].gameObject.SetActive (false);

					this.stillUnderAttack = true;
				}
			}

			// PLAYER CASTLE DAMAGE
			else if (other.GetComponent<BaseUnit>().isFriendlyUnit == false & this.gameObject.GetComponent<BaseUnit>().isFriendlyUnit == true)
			{
				Damage (1);
				Player myCastle = this.GetComponent<Player>();
				if (myCastle != null) 
				{
					UIManager.Instance.HideFlag();
				}
				this.stillUnderAttack = true;
			}
		}
	}

	void OnTriggerExit(Collider other)
	{
		if (this.GetComponent<BaseUnit> ().isFriendlyUnit != other.GetComponent<Unit> ().isFriendlyUnit)
		{
			this.stillUnderAttack = false;
			Destroy (other.gameObject);
		}
	}

	// ================================================================= BATTLE SCENE END

	#endregion


}