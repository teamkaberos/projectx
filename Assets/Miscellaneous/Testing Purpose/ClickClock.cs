﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickClock : MonoBehaviour 
{
	public void One()
	{
		print ("One");
	}

	public void Two()
	{
		print ("Two");
	}

	public void Three()
	{
		print ("Three!");
	}

}
