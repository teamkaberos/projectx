﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Forey : MonoBehaviour {

	public Xerox[] purints;

	// Use this for initialization
	void Start () 
	{
//		StartCoroutine ("Xorex", 2f);
	}

	IEnumerator Xorex ()
	{
		foreach (Xerox x in purints) 
		{
			while (x.fadeValue > 0) 
			{
				x.fadeValue -= Time.deltaTime;
				print ((int)x.fadeValue);
				yield return new WaitForSeconds (3f);
			}
		}
	}

}
