﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CheckLaneSpeed : MonoBehaviour 
{
	public Text[] hasteV;
	public Text[] slowedV;

	void Start()
	{
		GameManager.whenBuffValueChange += UpdateValues;
	}

	void UpdateValues()
	{
		for (int x = 0; x < 3; x++)
		{
			hasteV[x].text = "Haste: "+GameManager.pHasted[x];
			slowedV[x].text = "Slowed: "+GameManager.eSlowed[x];
		}
	}

	void OnDestory()
	{
		GameManager.whenBuffValueChange -= UpdateValues;
	}
}
