﻿using UnityEngine;
using UnityEngine.UI;

public class SetAnim : MonoBehaviour 
{
	Animator[] anims;
    Unit[] units;

	public Button setAttack;
	public Button setDeath;

	// Use this for initialization
	void Start () 
	{
		anims =  GetComponentsInChildren<Animator>();
        //units = GetComponentsInChildren<Unit>();
	}

	public void SetAttackAnim()
	{
		CancelInvoke("SetWalk");
		for (int x = 0;x < anims.Length; x++)
		{
			anims[x].SetBool("Attacking",true);
            //units[x].StopMoving();
		}
		Invoke ("SetWalk",0.01f);
	}

	void SetWalk()
	{
		for (int x = 0;x < anims.Length; x++)
		{
			anims[x].SetBool("Attacking",false);
		}
	}

	public void SetDeathAnim()
	{
		for (int x = 0;x < anims.Length; x++)
		{
			anims[x].SetBool("Dying",true);
		}
	}
}
