﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Foo : MonoBehaviour {

	public string name1 = "Holla";
	public string name2 = "Hallo";

	void Start()
	{
		Bar bark = GameObject.Find ("Bar").GetComponent<Bar> ();
		string test = (string)bark.GetType ().GetField ("name" + gameObject.name).GetValue (bark);
		print (test);
	}

}