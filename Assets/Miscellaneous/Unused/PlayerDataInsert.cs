﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using UnityEngine.UI;
//
//public class PlayerDataInsert : MonoBehaviour 
//{
//	public InputField Username;
//	public InputField Password;
//	public InputField ConfirmPassword;
//	public InputField Email;
//
//	public GameObject infoPopup;
//	public Text infoText;
//
//	public GameObject loadingCanvas;
//
//	bool userCreated;
//
//	string CreateUserURL = "http://dispersed-maximum.000webhostapp.com/InsertUser.php";
//
//	public void CreateUser()
//	{
//		bool infoFilled = InfoFilled();
//		bool nameFilled = NameFilled();
//
//		if (infoFilled && nameFilled)
//		{
//			Player.playerName = Username.text;
//			PlayerPrefs.SetString("_myPlayerName", Username.text);
//			PlayerPrefs.SetString("_myLoginPassword", Password.text);
//
//			Debug.Log ("Checking");
//
//			loadingCanvas.SetActive(true);
//
//			StartCoroutine(InsertUserToDatabase(Username.text,Password.text,Email.text));
//		}
//		else
//			infoPopup.SetActive(true);
//	}
//
//	public void CreateGuest()
//	{
//		bool nameFilled = NameFilled();
//
//		if (nameFilled)
//		{
//			Player.playerName = Username.text;
//
//			PlayerPrefs.SetInt("_myGuestAcc",1);
//
//			LevelManager.Instance.HomeScene();
//		}
//		else
//			infoPopup.SetActive(true);
//	}
//
//	public bool NameFilled()
//	{
//		if(Username.text == "")
//		{
//			infoText.text = "Username field cannot be empty";
//			return false;
//		}
//		else
//			return true;
//	}
//
//	public bool InfoFilled()
//	{
//		if (Username.text.Length <= 4)
//		{
//			infoText.text = "Username must be 5 - 24 characters long";
//			return false;
//		}
//		else if (Password.text == "")
//		{
//			infoText.text = "Please key in password";
//			return false;
//		}
//		else if (Password.text .Length <= 5)
//		{
//			infoText.text = "Password too short";
//			return false;
//		}
//		else if (Password.text != ConfirmPassword.text)
//		{
//			infoText.text = "Passwords do not match";
//			return false;
//		}
//		else if (Email.text == "")
//		{
//			infoText.text = "Please insert valid email";
//			return false;
//		}
//		else
//			return true;
//  	}
//
//	IEnumerator InsertUserToDatabase(string _username, string _password, string _email)
//	{
//		WWWForm form = new WWWForm();
//		form.AddField("usernamePost", _username);
//		form.AddField("passwordPost", _password);
//		form.AddField("emailPost", _email);
//
//		WWW www  = new WWW(CreateUserURL,form);
////		WWW response = new WWW(CreateUserURL);
//
//		yield return www;
//
//		if (www.text.Contains("Success"))
//		{
//			LevelManager.Instance.HomeScene();
//		}
//	}
//}
