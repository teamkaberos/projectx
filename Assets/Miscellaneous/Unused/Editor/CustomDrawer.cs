﻿using UnityEditor;
using UnityEngine;

//[CustomPropertyDrawer(typeof(UnitManager.Unit_Info))]
//public class CustomDrawer : PropertyDrawer
//{
//    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
//    {
//        // Using BeginProperty / EndProperty on the parent property means that
//        // prefab override logic works on the entire property.
//        EditorGUI.BeginProperty(position, label, property);

//        // Draw label
//        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);



//        // Don't make child fields be indented
//        var indent = EditorGUI.indentLevel;
//        EditorGUI.indentLevel = 0;

//        // Calculate rects
//        var nameRect = new Rect(position.x, position.y, 150, position.height);
//        var classRect = new Rect(position.x + 155, position.y, 80, position.height);
//        var heroNoRect = new Rect(position.x + 245 , position.y,30, position.height);

//        var heroIsUnlockedRect = new Rect(position.x, nameRect.y + nameRect.height, 150, position.height);
//        //var classRect = new Rect(position.x + 155, position.y, 80, position.height);
//        //var heroNoRect = new Rect(position.x + 245, position.y, 30, position.height);


//        //// Draw fields - passs GUIContent.none to each so they are drawn without labels
//        EditorGUI.PropertyField(nameRect, property.FindPropertyRelative("heroName"), GUIContent.none);
//        EditorGUI.PropertyField(classRect, property.FindPropertyRelative("heroRace"), GUIContent.none);
//        EditorGUI.PropertyField(heroNoRect, property.FindPropertyRelative("heroNo"), GUIContent.none);

//        EditorGUI.PropertyField(heroIsUnlockedRect, property.FindPropertyRelative("heroIsUnlocked"), GUIContent.none);

//        //EditorGUI.PropertyField(amountRect1, property.FindPropertyRelative("amount"), GUIContent.none);

//        //EditorGUI.PropertyField(amountRect2, property.FindPropertyRelative("amount"), GUIContent.none);

//        // Set indent back to what it was
//        EditorGUI.indentLevel = indent;

//        EditorGUI.EndProperty();
//    }
//}
