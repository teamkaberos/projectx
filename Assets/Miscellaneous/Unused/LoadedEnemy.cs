﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class LoadedEnemy : MonoBehaviour 
{
    public delegate void tutorial_Enemy_Selection_NextStep();
    public static event tutorial_Enemy_Selection_NextStep enemySelected;

	// ---------- For pre-loading enemy before battle begins ---------- \\
	
    // ===== INFO HOLDER =====\\
    [HideInInspector]
    public string playerID, playerName ,race, clan, potrait;
    [HideInInspector]
    public float shuffleTiming;
    [HideInInspector]
    public int reputation, gold, castleLv;
    //[HideInInspector]
    public List<int> enemyAssignedUnitsN;
    //[HideInInspector]
    public List<int> enemyAssignedUnitsLevelN;
    [HideInInspector]
    public int orderNo;

    public Clan clan_;       //checks if THIS opponent is in a CLAN player is currently at war with
    public Image toggleImage;

    int raidableGold;

	public Text nameDisplay;
	public Text castleLvDisplay;
	public Text raceDisplay;
	public Text reputationDisplay;
	public Text clanDisplay;
	public Text goldDisplay;
	//public Text lumberDisplay;

    public Button embarkToWar;

	// ================================================================= \\

    void Start()
    {
        for (int x = 0; x < transform.parent.childCount - 1; x++)
        {
            if (transform.parent.GetChild(x) == transform)
                orderNo = x;
        }
    }

    void OnDestroy()
    {
        ToBattle_Descrip.selectedEnemy -= EnableMe;
    }

	public virtual void UpdateOpponentInfo()
	{
        CalculateRaidable();

		nameDisplay.text = playerName;
		castleLvDisplay.text = "Lv. " + castleLv;
		reputationDisplay.text = "" + reputation;

        if (clan != "<currently_not_in_any_clan_yet>")
            clanDisplay.text = clan;
        else
            clanDisplay.text = "";
        
		raceDisplay.text = string.Format(race).ToUpper();
        goldDisplay.text = string.Format("{0} <color=#00FF00>({1})</color>", string.Format("{0:n0}",gold), string.Format("{0:n0}",raidableGold));

        //CheckIfCurrentlyAtWar();
	}
    void CalculateRaidable()
    {
        raidableGold = Mathf.FloorToInt(gold * 0.5f);
    }

    public void SelectOpponent(bool newValue)
    {
        embarkToWar.interactable = newValue;
        StoreEnemyInfo();
        if (Player.tutorial == 4)
            enemySelected();
    }

    public virtual void StoreEnemyInfo()
    {
        EnemyLoader.selectedEnemyID = playerID;
        Enemy.nameLoaded = playerName;
        Enemy.castleLevel = castleLv;
        Enemy.reputation = reputation;
        Enemy.clan = clan;
        Enemy.race = race;
        Enemy.troopsAssigned = enemyAssignedUnitsN;
        Enemy.troopsAssignedLevels = enemyAssignedUnitsLevelN;
        Enemy.gold = gold;
        Enemy.shuffleTiming = shuffleTiming;

        for (int x = 0; x < clan_.ongoingWarTargetClanID.Count; x++)
        {
            if (clan == clan_.ongoingWarTargetClanID[x])
            {
                //change color
                Clan.atWarEnemy = true;
                break;
            }
            Clan.atWarEnemy = false;
        }
    }

    void CheckIfCurrentlyAtWar()
    {
        if (Clan.clanID != "<currently_not_in_any_clan_yet>")
        {
            if (clan_.ongoingWarTargetClanID.Count > 0)
            {
                for (int x = 0; x < clan_.ongoingWarTargetClanID.Count; x++)
                {
                    if (clan == clan_.ongoingWarTargetClanID[x] && clan_.ongoingWarsLoaded[x].status == "running")
                    {
                        // THIS IS TO NOTIFY PLAYERS THAT THEIR CLAN HAS A WAR WITH THIS TARGET
                        //change color
                        Clan.atWarEnemy = true;
                        toggleImage.color = Color.red;
                        break;
                    }
                    Clan.atWarEnemy = false;
                    toggleImage.color = Color.white;
                }
            }
        }
        else
        {
            Clan.atWarEnemy = false;
            toggleImage.color = Color.white;
        }
    }

    public void ChatboxTargetTroop()
    {
        Enemy.troopsAssigned = enemyAssignedUnitsN;
        Enemy.troopsAssignedLevels = enemyAssignedUnitsLevelN;
    }

    // For tutorial
    public void Listen()
    {
        ToBattle_Descrip.selectedEnemy += EnableMe;
    }
    void EnableMe()
    {
        GetComponent<Toggle>().interactable = true;
    }
}
