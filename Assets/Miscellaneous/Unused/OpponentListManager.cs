﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using UnityEngine.UI;
//
//public class OpponentListManager : MonoBehaviour 
//{
//	public static OpponentListManager Instance;
//
//	public LoadedEnemy[] listOfVictims;
//
//	public static string tempPlayername;
//	public static int tempCastleLV;
//	public static string tempClanName;
//	public static int tempReputationValue;
//	public static float tempShuffleTiming;
//	public static int tempGroups;
//	public static float tempShufflingTime;
//	public static string tempEnemyRace;
//	public static int floatingLumber;
//	public static  int floatingGold;
//	public static string tempuuid;
//
//	// ---------- For PvE ---------- \\
//
//	public static string gameMode;
//	public static string farmType;
//
//	// ============================== \\
//
//	void Awake()
//	{
//		Instance = this;
//	}
//		
//	public void Distribution(int i)
//	{
//		listOfVictims [i].playerName = tempPlayername;
//		listOfVictims [i].castleLv = tempCastleLV;
//		listOfVictims [i].shuffleTiming = tempShufflingTime;
//		listOfVictims [i].reputation = tempReputationValue;
//		listOfVictims [i].clan = tempClanName;
//		listOfVictims [i].squares = tempGroups;
//		listOfVictims [i].circles = tempGroups;
//		listOfVictims [i].triangles = tempGroups;
//		listOfVictims[i].race = tempEnemyRace;
//		listOfVictims[i].uuid = tempuuid;
//		listOfVictims[i].gold = Mathf.FloorToInt(floatingGold*0.8f);
//		listOfVictims [i].UpdateOpponentInfo ();
//	}
//
//	public void CloseEnemies()
//	{
//		for (int x = 0; x<listOfVictims.Length; x++)
//		{
//			listOfVictims[x].gameObject.GetComponent<Toggle>().isOn = false;
//		}
//	}
//		
//	#region Misc
//
//	public void PvPMode()
//	{
//		gameMode = "PvP";
//	}
//
//	public void PvEMode()
//	{
//		gameMode = "PvE";
//	}
//
//	public void FarmingLandmines()
//	{
//		farmType = "Landmines";
//	}
//
//	public void FarmingBallistas()
//	{
//		farmType = "Ballistas";
//	}
//
//	public void FarmingDragons()
//	{
//		farmType = "Dragons";
//	}
//
//
//	#endregion
//}
