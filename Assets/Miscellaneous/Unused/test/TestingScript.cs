﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestingScript : MonoBehaviour
{
	Transform mainCam;

	void Start()
	{
		mainCam = GameObject.FindGameObjectWithTag("MainCamera").transform;
	}

	void LateUpdate()
	{
		transform.LookAt(mainCam);
	}
}
