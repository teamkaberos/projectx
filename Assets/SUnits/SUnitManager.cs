﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SUnitManager : GameOverManager
{
    [Header("S-Unit MANAGER Stuff")]
	public SUnitTimerTier sUnitTimer;

    public BaseSUnit player;
    public BaseSUnit enemy;

    [Header("Outcome Necessities")]
    public Text outcomeHeaderText;


    public override void VictoryCheck(BaseSUnit sUnit)
    {
        if (sUnit == player)
        {
            PlayerLose();
        }
        else if (sUnit == enemy)
        {
            PlayerWin();
        }
    }



    protected override void UpdateSUnit()
    {
        base.UpdateSUnit();

        if (SceneManager.GetActiveScene().name.Contains("BallistaFarm"))
        {
            if (GameSparksManager.Instance != null)
                GameSparksManager.Instance.SaveSUnitsAmount(SUnitTierValueCheck(sUnitTimer.Tier));
        }
    }
    int SUnitTierValueCheck(int currentTier)
    {
        if (currentTier == 0)
            return 3;
        if (currentTier == 1)
            return 2;
        if (currentTier == 2)
            return 1;
        return 0;
    }


    protected override void VictoryOutcome(int sUnitsAdded)
    {
        outcomeHeaderText.text = "VICTORIOUS";

        base.VictoryOutcome(sUnitsAdded);
    }


    protected override void DefeatOutcome()
    {
        outcomeHeaderText.text = "DEFEATED";

        outcomeBodyText.text = "You were defeated";

        base.DefeatOutcome();
    }
}
