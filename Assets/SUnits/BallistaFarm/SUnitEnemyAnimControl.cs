﻿using UnityEngine;

public class SUnitEnemyAnimControl : SUnitBaseAnimControl
{
    public delegate void StateChange(BaseSUnit.UnitState unitState);
    public static event StateChange UnitStateHasChanged;


    void Start()
    {
        anim = GetComponent<Animator>();
    }


    public override void SetAttacking()
    {
        base.SetAttacking();
        UnitStateHasChanged(BaseSUnit.UnitState.attack);
    }
    public override void SetNotAttacking()
    {
        base.SetNotAttacking();
        UnitStateHasChanged(BaseSUnit.UnitState.idle);
    }

    public override void SetFlinching()
    {
        base.SetFlinching();
        UnitStateHasChanged(BaseSUnit.UnitState.flinch);
    }
    public override void SetNotFlinching()
    {
        base.SetNotFlinching();
        if (anim.GetBool("Attacking") != true && anim.GetBool("Dying") != true)
            UnitStateHasChanged(BaseSUnit.UnitState.idle);
    }


    public override void SetVictorious()
    {
        base.SetVictorious();
        anim.SetBool("Victorious", true);
        UnitStateHasChanged(BaseSUnit.UnitState.victory);
    }


    public override void SetDying()
    {
        base.SetDying();
        anim.SetBool("Dying", true);
        UnitStateHasChanged(BaseSUnit.UnitState.death);
    }
}
