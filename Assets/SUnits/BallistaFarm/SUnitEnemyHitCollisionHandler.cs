﻿using UnityEngine;

public class SUnitEnemyHitCollisionHandler : MonoBehaviour
{
    public SUnitEnemy sUnitEnemy;

    public int damageModifier;
    [Tooltip("Does this hit the player?")]

    public bool hitBox = true;   // Decides if this collider will cause damage to this unit
    public bool attackBox;       // Decides if this collider does damage to the player

    void OnTriggerEnter2D(Collider2D col)
    {
        if (attackBox)
        {
            if (col.tag == "Player")
            {
                col.GetComponent<SUnitPlayer>().TakeDamage(sUnitEnemy.Damage);
            }
        }

        if (hitBox)
        {
            if (col.tag == "Projectile")
            {
                Projectile pjInfo = col.GetComponentInParent<Projectile>();
                if (pjInfo != null)
                {
                    if (pjInfo.HasHit == false & pjInfo.IsFired == true)
                    {
                        sUnitEnemy.TakeDamage(pjInfo.damageValue + damageModifier);
                        pjInfo.ToggleHasHit(true);
                    }
                }
            }
        }
    }
}
