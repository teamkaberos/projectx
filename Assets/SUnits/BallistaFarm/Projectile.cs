﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Projectile : MonoBehaviour
{
    public delegate void ProjectileFired();
    public static event ProjectileFired ShotFired;      // happens right when player releases
    public static event ProjectileFired ShotReleased;

    public delegate void ProjectileStatus(SUnitPlayer.ArrowHelpStatus currentStatus);
    public static event ProjectileStatus ProjectileStatusUpdate;


    public ProjectileCollisionHandler pColHandler;

    public int damageValue;

    public static float maxDragDistance = 2f;
    public static float currentDragDistance;
    public static float returnableDistance = 0.7f;
    public static bool shootable;

    public GameObject projectileMeshDisplay;

    public SpringJoint2D sJoint;

    Rigidbody2D pivotTransform;
    Rigidbody2D projectileRB;
    public BoxCollider2D projectileCollider;
    public static bool isPressed;
    bool isFired;
    public bool IsFired
    {
        get
        {
            return isFired;
        }
    }
    bool hasHit;
    public bool HasHit
    {
        get
        {
            return hasHit;
        }
    }

    public static float releaseTime = 0.08f;

    public static bool isLastShot;

    public static float normalizedDistance;

    public Image shotImage;     // to show the arrow trajectory


    public static float arrowAngle;
    float sign = 1;
    float offset;

    public FXPlay onHitEffect;



    void Start()
    {
        pColHandler.ArrowHasHit += HitNotify;
        pColHandler.SetProjectile(this);
    }
    void OnDestroy()
    {
        pColHandler.ArrowHasHit -= HitNotify;
    }

    void ToggleProjectileVisibility(bool toBeSet)
    {
        projectileMeshDisplay.SetActive(toBeSet);
    }

    public void InitializeProjectile(int damageAmount, Transform toGoTo)
    {
        damageValue = damageAmount;
        projectileRB = GetComponent<Rigidbody2D>();
        pivotTransform = toGoTo.GetComponent<Rigidbody2D>();
        sJoint.connectedBody = pivotTransform;
    }

    public void ReadyForFire()
    {
        projectileMeshDisplay.transform.eulerAngles = new Vector3(0, 90, 0);
        ToggleHasHit(false);
        ToggleProjectile(true);
        ToggleProjectileVisibility(true);
        ToggleCollider(true);
    }
    public void ReturnHome()
    {
        ResetProjectile();
        MoveToPivot();
        ToggleProjectile(false);
        ToggleCollider(true);
        isFired = false;
    }

    void ResetProjectile()
    {
        sJoint.enabled = true;
    }
    void MoveToPivot()
    {
        transform.position = pivotTransform.position;
    }
    void ToggleProjectile(bool toSet)
    {
        gameObject.SetActive(toSet);
    }
    public void ToggleHasHit(bool toSet)
    {
        hasHit = toSet;
    }
    void ToggleCollider(bool isTouchable)
    {
        projectileCollider.enabled = isTouchable;
    }

    void HitNotify()
    {
        onHitEffect.PlayEffect();
        ToggleProjectileVisibility(false);
    }

    void Update()
    {
        if (isPressed && !isFired && !SUnitPlayer.isFlinching && !SUnitPlayer.isDead)
        {
            Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            if (Vector3.Distance(mousePos, pivotTransform.position) > maxDragDistance)
            {
                projectileRB.position = pivotTransform.position + (mousePos - pivotTransform.position).normalized * maxDragDistance;
            }
            else
            {
                projectileRB.position = mousePos;
                if (Vector3.Distance(mousePos, pivotTransform.position) > returnableDistance)
                {
                    shootable = true;
                    ProjectileStatusUpdate(SUnitPlayer.ArrowHelpStatus.release);
                }
                else
                {
                    shootable = false;
                    ProjectileStatusUpdate(SUnitPlayer.ArrowHelpStatus.drag);
                }
            }

            // how far away is the arrow from the pivot
            normalizedDistance = Vector3.Distance(pivotTransform.position, projectileRB.position) / maxDragDistance;

            // arrow Rotation Display
            sign = (transform.position.y < 0) ? -1 : 1;
            offset = (sign > 0) ? 0 : 360;
            arrowAngle = Vector2.Angle(Vector2.left, projectileMeshDisplay.transform.position);
            projectileMeshDisplay.transform.eulerAngles = new Vector3((arrowAngle * sign) + offset, 90, 0);

            // display trajectory imaging
            if (normalizedDistance >= returnableDistance)
                shotImage.color = new Vector4(1, 1, 1, (normalizedDistance - 0.5f) * 1.5f);
            else
                shotImage.color = new Vector4(1, 1, 1, 0);
        }
        else if (SUnitPlayer.isFlinching || SUnitPlayer.isDead)
        {
            // set normalized distance to 0
            normalizedDistance = 0;
            // reset mesh display to default rotation
            projectileMeshDisplay.transform.eulerAngles = new Vector3(0, 90, 0);

            // set unshootable, unpressed and trajectory image back to invisible
            shootable = false;
            isPressed = false;
            shotImage.color = new Vector4(1, 1, 1, 0);

            // disable kinematic and reset the arrow help status display
            projectileRB.isKinematic = false;
            ProjectileStatusUpdate?.Invoke(SUnitPlayer.ArrowHelpStatus.drag);
        }
    }

    void OnMouseDown()
    {
        if (!SUnitPlayer.isFlinching)
        {
            isPressed = true;
            projectileRB.isKinematic = true;

            ProjectileStatusUpdate?.Invoke(SUnitPlayer.ArrowHelpStatus.drag);
        }
    }
    void OnMouseUp()
    {
        isPressed = false;
        projectileRB.isKinematic = false;
        ProjectileStatusUpdate?.Invoke(SUnitPlayer.ArrowHelpStatus.tap);

        if (shootable)
        {
            shotImage.color = new Vector4(1, 1, 1, 0);
            StartCoroutine(Release());
        }
        else
        {
            projectileMeshDisplay.transform.eulerAngles = new Vector3(0, 90, 0);
        }
    }

    IEnumerator Release()
    {
        ShotFired?.Invoke();
        ToggleCollider(false);

        yield return new WaitForSeconds(releaseTime);

        shootable = false;
        isFired = true;
        sJoint.enabled = false;

        if (!isLastShot)
            yield return new WaitForSeconds(RechargeTimer.rechargeTime - releaseTime);
        else
            yield return new WaitForSeconds(RechargeTimer.refillTime - releaseTime);

        ShotReleased?.Invoke();
        yield return new WaitForSeconds(2.5f);
        ReturnHome();
    }
}
