﻿using UnityEngine;

public class SUnitBaseAnimControl : MonoBehaviour
{
    protected Animator anim;



    public virtual void SetAttacking()
    {
        anim.SetBool("Attacking", true);
    }
    public virtual void SetNotAttacking()
    {
        anim.SetBool("Attacking", false);
    }

    public virtual void SetFlinching()
    {
        anim.SetBool("Flinching", true);
    }
    public virtual void SetNotFlinching()
    {
        anim.SetBool("Flinching", false);
    }


    public virtual void SetVictorious()
    {
        anim.SetBool("Victorious", true);
    }

    public virtual void SetDying()
    {
        anim.SetBool("Dying", true);
    }
}
