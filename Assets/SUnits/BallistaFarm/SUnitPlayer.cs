﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SUnitPlayer : BaseSUnit
{
    [Header("Anim Controller")]
    public SUnitPlayerAnimControl anim;

    [Header("Flinch Info")]
    public static bool isFlinching;
    public static bool isDead;
    public float flinchDuration;
    float currentFlinchDuration;
    public Image flichDisplayImage;
    public Text flinchDurationTextDisplay;

    [Header("Projectile OBJs")]
    public Transform arrowDragLoc;
    public GameObject projectilePrefab;

    [HideInInspector] public List<Projectile> projectileList = new List<Projectile>();


    [Header("Arrow Status")]
    public Text arrowStatusDisplayText;
    public enum ArrowHelpStatus
    {
        tap,
        drag,
        release
    }
    protected ArrowHelpStatus arrowStatus;

    protected override void Start()
    {
        base.Start();

        ResetStatics();

        flinchDurationTextDisplay.text = "0.0";

        UpdateStatusText(ArrowHelpStatus.drag);

        ChangeState(UnitState.idle);

        CreateProjectile(RechargeTimer.totalShotsCount);
        ReadyProjectile();

        SUnitPlayerAnimControl.UnitStateHasChanged += ChangeState;
        Projectile.ShotReleased += ReadyProjectile;
        Projectile.ProjectileStatusUpdate += UpdateStatusText;
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        SUnitPlayerAnimControl.UnitStateHasChanged -= ChangeState;
        Projectile.ShotReleased -= ReadyProjectile;
        Projectile.ProjectileStatusUpdate -= UpdateStatusText;
    }


    void ResetStatics()
    {
        isFlinching = false;
        isDead = false;
    }


    void Update()
    {
        if (isFlinching)
        {
            currentFlinchDuration -= Time.deltaTime;

            if (currentFlinchDuration <= 0)
            {
                currentFlinchDuration = 0;
                Unflinch();
            }
            flichDisplayImage.fillAmount = currentFlinchDuration / flinchDuration;
            flinchDurationTextDisplay.text = string.Format("{0:0.0}", currentFlinchDuration);
        }
    }


    // ===== PROJECTILES ISSUES ===== \\

    void CreateProjectile(int projectilesToCreate)
    {
        for (int x = 0; x < projectilesToCreate; x++)
        {
            // Create the projectile
            GameObject projectile = Instantiate(projectilePrefab,
            arrowDragLoc.position, Quaternion.identity, arrowDragLoc);
            // Get Projectile class to initialize
            Projectile pj = projectile.GetComponent<Projectile>();
            pj.InitializeProjectile(damage, arrowDragLoc);
            projectileList.Add(pj);
            projectile.SetActive(false);
        }
    }

    void ReadyProjectile()
    {
        if (!AllFired())
        {
            for (int x = 0; x < projectileList.Count; x++)
            {
                if (!projectileList[x].gameObject.activeInHierarchy)
                {
                    projectileList[x].ReadyForFire();
                    //if (isFlinching)
                    //    isFlinching = false;
                    break;
                }
            }
        }
        else
        {
            for (int x = 0; x < projectileList.Count; x++)
            {
                projectileList[x].ReturnHome();
            }
            projectileList[0].ReadyForFire();
        }
    }
    bool AllFired()
    {
        for (int x = 0; x < projectileList.Count; x++)
        {
            if (projectileList[x].IsFired == false)
                return false;
        }
        return true;
    }

    void HideAllProjectiles()
    {
        for (int x = 0; x < arrowDragLoc.childCount; x++)
        {
            if (arrowDragLoc.GetChild(x).gameObject.activeInHierarchy)
            {
                arrowDragLoc.GetChild(x).gameObject.SetActive(false);
            }
        }
    }

    void UpdateStatusText(ArrowHelpStatus helpStatus)
    {
        arrowStatusDisplayText.text = ArrowStatusText(helpStatus);
    }
    string ArrowStatusText(ArrowHelpStatus helpStatus)
    {
        if (helpStatus == ArrowHelpStatus.tap)
            return "Tap to draw";
        if (helpStatus == ArrowHelpStatus.drag)
            return "Drag to pull";
        if (helpStatus == ArrowHelpStatus.release)
            return "Release to shoot";
        return "";
    }

    // ===== PROJECTILES ISSUES END ===== \\

    public override void TakeDamage(int damageAmount)
    {
        base.TakeDamage(damageAmount);

        if (currentHitPoints > 0)
        {
            anim.SetFlinching();
            isFlinching = true;
            currentFlinchDuration = flinchDuration;
        }
        else
        {
            anim.SetDying();
            isDead = true;
        }
    }
    void Unflinch()
    {
        anim.SetNotFlinching();
        isFlinching = false;
    }


    protected override void UnitVictoryBehaviour(BaseSUnit baseSUnit)
    {
        base.UnitVictoryBehaviour(baseSUnit);

        HideAllProjectiles();

        if (baseSUnit.transform != transform)
        {
            anim.SetVictorious();
        }
    }

}
