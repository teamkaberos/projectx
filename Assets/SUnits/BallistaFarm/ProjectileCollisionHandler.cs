﻿using UnityEngine;

public class ProjectileCollisionHandler : MonoBehaviour
{
    public delegate void OnHit();
    public event OnHit ArrowHasHit;

    Projectile projectile;


    public void SetProjectile(Projectile pj)
    {
        projectile = pj;
    }
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Enemy")
        {
            if (projectile.IsFired)
            {
                ArrowHasHit();
            }
        }
    }
}
