﻿using UnityEngine;

public class SUnitPlayerAnimControl : SUnitBaseAnimControl
{
    public SUnitPlayer sUnitPlayer;


    public delegate void StateChange(BaseSUnit.UnitState unitState);
    public static event StateChange UnitStateHasChanged;

    float startFromFrame = 15;      // this is the frame where the arrow begins being pulled

    float drawBowAnimationLength;



    void Start()
    {
        if (GetComponent<Animator>() != null)
        {
            anim = GetComponent<Animator>();
        }
    }


    public void SetDrawing()
    {
        if (anim.GetBool("Flinching") != true || anim.GetBool("Drawing") != true)
        {
            anim.SetBool("Drawing", true);
            UnitStateHasChanged?.Invoke(BaseSUnit.UnitState.draw);
        }
    }
    public void SetUndrawing()
    {
        if (anim.GetBool("Flinching") != true & anim.GetBool("Drawing") == true)
        {
            anim.SetBool("Drawing", false);
            UnitStateHasChanged?.Invoke(BaseSUnit.UnitState.idle);
        }
    }


    public override void SetFlinching()
    {
        base.SetFlinching();
        SetUndrawing();
        UnitStateHasChanged?.Invoke(BaseSUnit.UnitState.flinch);
    }
    public override void SetNotFlinching()
    {
        base.SetNotFlinching();
        UnitStateHasChanged?.Invoke(BaseSUnit.UnitState.idle);
    }


    public override void SetVictorious()
    {
        base.SetVictorious();
        UnitStateHasChanged?.Invoke(BaseSUnit.UnitState.victory);
    }


    public override void SetDying()
    {
        base.SetDying();
        UnitStateHasChanged?.Invoke(BaseSUnit.UnitState.death);
    }



    void Update()
    {
        if (Input.GetMouseButton(0) && Projectile.isPressed)
        {
            if (!SUnitPlayer.isFlinching && !SUnitPlayer.isDead)
            {
                SetDrawing();

                if (anim.GetCurrentAnimatorStateInfo(0).IsName("Archer_Anim_Draw"))
                {
                    anim.Play("Archer_Anim_Draw", 0, Projectile.normalizedDistance);
                }
            }
        }
        else if (Input.GetMouseButtonUp(0))
        {
            SetUndrawing();
            if (anim.GetCurrentAnimatorStateInfo(0).IsName("Archer_Anim_Draw"))
            {
            }
            if (!Projectile.isPressed)
            {
            }
        }
        else if (!Projectile.isPressed)
        {
            SetUndrawing();
        }
    }
}
