﻿using UnityEngine;
using UnityEngine.UI;

public class HPBar : MonoBehaviour
{
    public Image currentHitPointsDisplay;
    public Text hitPointsDisplayText;

    public void DisplayHitPoints(int current, int total)
    {
        currentHitPointsDisplay.fillAmount = current/(float)total;
        hitPointsDisplayText.text = string.Format("{0:n0} / {1:n0}", current, total);
    }
}
