﻿using UnityEngine;

public class SUnitEnemy : BaseSUnit
{
    public SUnitEnemyAnimControl anim;

    [Header("Collision Handlers")]
    public SUnitEnemyHitCollisionHandler[] collisionHandler;


    protected override void Start()
    {
        base.Start();

        // listen to animation playing
        SUnitEnemyAnimControl.UnitStateHasChanged += ChangeState;

        InvokeRepeating("AttackPlayer", 11f, 12f);
    }
    protected override void OnDestroy()
    {
        base.OnDestroy();
        SUnitEnemyAnimControl.UnitStateHasChanged -= ChangeState;
    }
    protected override void GetRigidbody()
    {
    }

    public override void TakeDamage(int damageAmount)
    {
        base.TakeDamage(damageAmount);
        if (currentUnitState != UnitState.attack)
        {
            ToggleFlinchingAnim(true);
        }

        if (currentHitPoints <= 0)
        {
            //Death to the ENEMY
            ToggleDyingAnim();
        }
    }

    void AttackPlayer()
    {
        if (currentUnitState != UnitState.death && currentUnitState != UnitState.victory)
        {
            ToggleAttackAnim(true);
            ChangeState(UnitState.attack);
        }
    }

    void ToggleAttackAnim(bool toSet)
    {
        if (toSet == true)
            anim.SetAttacking();
    }
    void ToggleFlinchingAnim(bool toSet)
    {
        if (toSet == true)
            anim.SetFlinching();
    }
    void ToggleDyingAnim()
    {
        anim.SetDying();
    }


    protected override void UnitVictoryBehaviour(BaseSUnit baseSUnit)
    {
        base.UnitVictoryBehaviour(baseSUnit);

        if (baseSUnit != null)
        {
            if (baseSUnit.transform != transform)
            {
                anim.SetVictorious();
                CancelInvoke("AttackPlayer");
            }
        }
    }

}
