﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOverManager : MonoBehaviour
{
    /*  === NOT IN USE RESTART ANYMORE === 
    public float restartDelay = 5f;
    float restartTimer;
    */

    public GameObject victoryCanvas;
    public Text outcomeBodyText;
    public GameObject defeatCanvas;

    void Start()
    {
        if (SceneManager.GetActiveScene().name.Contains("MinesFarm"))
        {
            PlayerHealth.PlayerLost += PlayerLose;
            EnemyHealth.EnemyLost += PlayerWin;
        }
        else if (SceneManager.GetActiveScene().name.Contains("BallistaFarm"))
        {
            BaseSUnit.IsNowDead += VictoryCheck;
        }

        GameSparksManager.SUnitsAmountUpdated += VictoryOutcome;
    }


    void OnDestroy()
    {
        if (SceneManager.GetActiveScene().name.Contains("MinesFarm"))
        {
            PlayerHealth.PlayerLost -= PlayerLose;
            EnemyHealth.EnemyLost -= PlayerWin;
        }
        else if (SceneManager.GetActiveScene().name.Contains("BallistaFarm"))
        {
            BaseSUnit.IsNowDead -= VictoryCheck;
        }

        GameSparksManager.SUnitsAmountUpdated -= VictoryOutcome;

    }

    public virtual void VictoryCheck(BaseSUnit sUnit)
    {

    }


    protected void PlayerWin()
    {
        //anim.SetTrigger("YouWin");
        UpdateSUnit();
    }
    protected void PlayerLose()
    {
        //anim.SetTrigger("GameOver");
        DefeatOutcome();
    }

    protected virtual void VictoryOutcome(int sUnitsAdded)
    {
        // Parse text
        outcomeBodyText.text = string.Format("Obtained {0} {1}", sUnitsAdded, SUnitType(SUnitChallenge.sUnitToAdd));
        // Display popup
        victoryCanvas.SetActive(true);
    }
    string SUnitType(SUnitType.SUnit typeOfUnit)
    {
        if (typeOfUnit == global::SUnitType.SUnit.Mines)
            return "Mine(s)";
        if (typeOfUnit == global::SUnitType.SUnit.Ballista)
            return "Ballista(s)";
        if (typeOfUnit == global::SUnitType.SUnit.Flames)
            return "Flame(s)";
        return "";
    }
    protected virtual void DefeatOutcome()
    {
        // Display popup
        defeatCanvas.SetActive(true);
    }


    protected virtual void UpdateSUnit()
    {
        if (SceneManager.GetActiveScene().name.Contains("MinesFarm"))
        {
            if (GameSparksManager.Instance != null)
                GameSparksManager.Instance.SaveSUnitsAmount(CountdownTimer.RewardTier());
        }
    }

    void GoBackHome()
    {
        LevelManager.Instance.HomeScene();
    }

    //void Update()
    //{
    //    if (playerHealth.currentHealth <= 0)
    //    {

    //        restartTimer += Time.deltaTime;

    //        if (restartTimer >= restartDelay)
    //        {
    //            SceneManager.LoadScene(0);
    //        }
    //    }

    //    if (enemyHealth.currentHealth <= 0)
    //    {

    //        restartTimer += Time.deltaTime;

    //        if (restartTimer >= restartDelay)
    //        {
    //            SceneManager.LoadScene(0);
    //        }
    //    }
    //}

}