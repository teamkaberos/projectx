﻿using UnityEngine;
using UnityEngine.UI;

public class BaseSUnitTimer : MonoBehaviour
{
    public delegate void TimeUp();
    public static event TimeUp RoundOver;

    public float roundTime;
    public Text roundTimeText;

    protected bool roundOngoing = true;

    protected virtual void Start()
    {
        BaseSUnit.IsNowDead += StopRoundTime;
    }
    void StopRoundTime(BaseSUnit sUnit)
    {
        roundOngoing = false;
    }
    void OnDestroy()
    {
        BaseSUnit.IsNowDead -= StopRoundTime;
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        if (roundOngoing)
        {
            DeductTime();
            UpdateTimeText();
        }
    }

    void DeductTime()
    {
        roundTime -= Time.deltaTime;
        roundOngoing &= roundTime > 0;
    }
    void UpdateTimeText()
    {
        roundTimeText.text = Mathf.CeilToInt(roundTime).ToString();
    }
}
