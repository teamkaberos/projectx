﻿using UnityEngine;

public class SUnitTimerTier : BaseSUnitTimer
{
    int tier;
    public int Tier
    {
        get
        {
            return tier;
        }
    }
    bool tierRewardAvailable = true;

    public float[] timerTiers;
    public GameObject[] tierDisplays;

    protected override void Start()
    {
        base.Start();

        for (int x = 0; x < tierDisplays.Length; x++)
        {
            tierDisplays[x].SetActive(true);
        }
    }

    protected override void Update()
    {
        base.Update();

        if (tierRewardAvailable)
        {
            if (roundTime <= timerTiers[tier])
            {
                tierDisplays[tier].SetActive(false);
                tier += 1;
                if (tier >= tierDisplays.Length)
                    tierRewardAvailable = false;
            }
        }
    }
}
