﻿using UnityEngine;

public class Bomb : MonoBehaviour
{
    public float startForce = 1f;
    public float spinX = 0f;
    public float spinY = 0f;
    public float spinZ = 0f;
    
    public int attackDamage = 10;

    public GameObject bombExplosion;
    public GameObject bombExplosionCyclops;

    Rigidbody2D rb;
    GameObject knight;
    GameObject cyclops;
    PlayerHealth playerHealth;
    EnemyHealth enemyHealth;    

    public int subtractScore = 50;
    public int bonusScore = 100;


    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        rb.AddForce(transform.up * startForce, ForceMode2D.Impulse);

        knight = GameObject.FindGameObjectWithTag("Knight");
        playerHealth = knight.GetComponent<PlayerHealth>();

        cyclops = GameObject.FindGameObjectWithTag("Cyclops");
        enemyHealth = cyclops.GetComponent<EnemyHealth>();
    }


    void Update()
    {
        transform.Rotate(spinX, spinY, spinZ);
    }

    

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Blade")
        {            
            Instantiate(bombExplosion, transform.position, transform.rotation);
            AttackKnight();
            Destroy(gameObject);

            ScoreText.score -= ScoreText.score;
        }


        if (col.tag == "Knight")
        {

            Instantiate(bombExplosion, transform.position, transform.rotation);
            AttackKnight();
            Destroy(gameObject);

            ScoreText.score -= ScoreText.score;
        }

        if (col.tag == "Cyclops")
        {
            Instantiate(bombExplosionCyclops, transform.position, transform.rotation);
            AttackCyclops();
            Destroy(gameObject);

            ScoreText.score += bonusScore;

        }

        if (col.tag == "DeflectHitbox")
        {
            GetComponent<Collider2D>().isTrigger = false;
        }
    }


    void OnTrigger2DExit(Collider2D col)
    {
        if (col.tag == "DeflectHitbox")
        {
            GetComponent<Collider2D>().isTrigger = true;
        }
    }


    void AttackKnight()
    {
        if (playerHealth.currentHealth > 0)
        {
            playerHealth.KnightHurt(attackDamage);
        }
    }


    void AttackCyclops()
    {
        if (enemyHealth.currentHealth > 0)
        {
                enemyHealth.CyclopsHurt(attackDamage);
        }
    }
}
