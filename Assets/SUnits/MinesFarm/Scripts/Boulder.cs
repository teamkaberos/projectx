﻿using UnityEngine;

public class Boulder : MonoBehaviour
{
    public GameObject boulderSlicedPrefab;    
    public float startForce = 0f;
    public float Spin;
    
    public int scorePoints = 10;
    Rigidbody2D rb;




    void Start()
    {
        rb = GetComponent <Rigidbody2D>();        
        rb.AddForce (transform.up * startForce, ForceMode2D.Impulse);
        rb.angularVelocity = Spin;
    }



    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Blade")
        {
            Vector3 direction = (col.transform.position - transform.position).normalized;

            Quaternion rotation = Quaternion.LookRotation(direction);

            GameObject slicedBoulder = Instantiate (boulderSlicedPrefab, transform.position, rotation);
            Destroy(slicedBoulder, 3f);
            Destroy (gameObject);

            ScoreText.score += scorePoints;   
        }
    }    
}
