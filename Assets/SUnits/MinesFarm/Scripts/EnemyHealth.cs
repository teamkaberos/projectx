﻿using UnityEngine;
using UnityEngine.UI;


public class EnemyHealth : MonoBehaviour
{
    public delegate void HealthDepleted();
    public static event HealthDepleted EnemyLost;

    public int startingHealth = 100;
    public int currentHealth;
    public Slider healthSlider;     
    public Image cyclopsHurtImage;
    public float flashSpeed = 5f;
    public Color flashColour = new Color(0f, 0f, 0f, 0.1f);

    public Animator anim;

    AudioSource cyclopsAudio;
    public AudioClip deadSFX;

    GameObject knight;
    PlayerHealth playerHealth;

    bool cyclopsDead;
    bool damaged;



    void Start ()
    {
        cyclopsAudio = GetComponent <AudioSource> ();   
        currentHealth = startingHealth;
        
        knight = GameObject.FindGameObjectWithTag("Knight");
        playerHealth = knight.GetComponent<PlayerHealth>();

        PlayerHealth.PlayerLost += Victory;
    }



    void Update()
    {
        anim.SetBool("IsAttacking", true);

        if (damaged)
        {
            cyclopsHurtImage.color = flashColour;
            anim.SetBool("CyclopsHurt", true);
            cyclopsAudio.Play();
        }

        else
        {
            cyclopsHurtImage.color = Color.Lerp(cyclopsHurtImage.color, Color.clear, flashSpeed * Time.deltaTime);
            anim.SetBool("CyclopsHurt", false);
        }

        damaged = false;
    }



    //void FixedUpdate()
    //{
    //    if (playerHealth.currentHealth <= 0)
    //    {
    //        anim.SetBool("CyclopsWin", true);
    //    }
    //}

    void Victory()
    {
        anim.SetBool("CyclopsWin", true);
    }


    public void CyclopsHurt (int amount)
    {
        damaged = true;
        currentHealth -= amount;
        healthSlider.value = currentHealth;
        cyclopsAudio.Play();

        if (currentHealth <= 0 && !cyclopsDead)
        {
            Death();
        }

    }

    void Death()
    {
        EnemyLost();
        cyclopsDead = true;
        anim.SetTrigger("CyclopsDead");
        cyclopsAudio.clip = deadSFX;
    }
}