﻿using UnityEngine;
using UnityEngine.UI;


public class PlayerHealth : MonoBehaviour
{
    public delegate void HealthDepleted();
    public static event HealthDepleted PlayerLost;

    public int startingHealth = 100;
    public int currentHealth;
    public Slider healthSlider;
    public Image knightHurtImage;
    public Color flashColour = new Color(0f, 0f, 0f, 0.1f);
    public float flashSpeed = 5f;

    public Animator anim;

    AudioSource knightAudio;
    public AudioClip deadSFX;
           
    bool isDead;
    bool damaged;
    //bool isStaggered; <---not necessary since it's calling from public Animator


    void Start ()
    {
        knightAudio = GetComponent <AudioSource> ();
        currentHealth = startingHealth;
    }


    void Update()
    {
        if (damaged)
        {
            knightHurtImage.color = flashColour;
            anim.SetBool("IsStaggered", true);
            knightAudio.Play();
        }

        else
        {
            knightHurtImage.color = Color.Lerp (knightHurtImage.color, Color.clear, flashSpeed * Time.deltaTime);
            anim.SetBool("IsStaggered", false);
        }

        damaged = false;
    }


     public void KnightHurt (int amount)
     {
         damaged = true;               
         currentHealth -= amount;
         healthSlider.value = currentHealth;
         knightAudio.Play();

         if (currentHealth <= 0 && !isDead)
         {
            PlayerLost();
            Death ();
         }
     }


    void Death ()
    {
        isDead = true;
        anim.SetTrigger ("IsDead");
        knightAudio.clip = deadSFX;
    }
}
