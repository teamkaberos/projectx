﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombSpawn : MonoBehaviour
{
    public GameObject bombPrefab;
    public float spawnTime = 5f;
    public Transform[] spawnPointsBomb;
    
    PlayerHealth playerHealth;
    GameObject knight;
    EnemyHealth enemyHealth;
    GameObject cyclops;
    

    void Start ()
    {
        InvokeRepeating ("Spawn", spawnTime,/*<--wait time to start*/ spawnTime/*<--wait time to repeat*/);

        knight = GameObject.FindGameObjectWithTag("Knight");
        cyclops = GameObject.FindGameObjectWithTag("Cyclops");

        playerHealth = knight.GetComponent<PlayerHealth>();
        enemyHealth = cyclops.GetComponent<EnemyHealth>();
    }


    void Spawn()
    {
        if (enemyHealth.currentHealth <= 0)
        {
            return;
        }

        if (playerHealth.currentHealth <= 0)
        {
            return;
        }

        int spawnPointIndex = Random.Range(0, spawnPointsBomb.Length);
        {
            GameObject spawnedBomb = Instantiate (bombPrefab, spawnPointsBomb[spawnPointIndex].position, spawnPointsBomb[spawnPointIndex].rotation);
            Destroy(spawnedBomb, 3f);
        }
    }
}

