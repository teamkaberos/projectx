﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DeflectHitbox : MonoBehaviour
{
    public TimeManager timeManager;    
    public Image deflectPromptImage;

    GameObject bomb;


    void Start()
    {
        bomb = GameObject.FindGameObjectWithTag("Bomb");
    }


    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Bomb")
        {
            timeManager.DoSlowmotion();

            deflectPromptImage.enabled = true;
        }
        
        else
        {
            deflectPromptImage.enabled = false;
        }
    }
}
