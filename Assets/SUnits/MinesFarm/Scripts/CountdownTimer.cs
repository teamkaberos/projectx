﻿using UnityEngine;
using UnityEngine.UI;


public class CountdownTimer : MonoBehaviour
{
    [SerializeField] Text uiText;
    [SerializeField] float mainTimer;
    
    static float timer;
    bool canCount = true;
    bool doOnce;

    public PlayerHealth playerhealth;
    public EnemyHealth enemyHealth;


    [Space(12)]
    public int[] timerTiers;
    public GameObject[] tierDisplays;
    int tier;
    bool tierRewardAvailable = true;


    private void Start()
    {
        timer = mainTimer;

        for (int x = 0; x < tierDisplays.Length; x++)
        {
            tierDisplays[x].SetActive(true);
        }
    }

    void Update()
    {
        if (timer >= 0.0f && canCount)
        {
            timer -= Time.deltaTime;
            uiText.text = timer.ToString("F");
        }
        else if (timer <= 0.0f && !doOnce)
        {
            canCount = false;
            doOnce = true;
            uiText.text = "0.0";
            timer = 0.0f;
        }


        if (tierRewardAvailable)
        {
            if (timer <= timerTiers[tier])
            {
                tierDisplays[tier].SetActive(false);
                tier += 1;
                if (tier >= tierDisplays.Length)
                    tierRewardAvailable = false;
            }
        }

    }

    private void FixedUpdate()
    {
        if (playerhealth.currentHealth <= 0)
        {
            canCount = false;
        }

        if (enemyHealth.currentHealth <= 0)
        {
            canCount = false;
        }

    }

    public static int RewardTier()
    {
        if (timer > 60)
        {
            if (timer > 80)
            {
                if (timer > 100)
                {
                    return 3;
                }
                return 2;
            }
            return 1;
        }
        return 0;
    }

}
