﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoulderSpawn : MonoBehaviour
{
    public float minDelay = 0.1f;
    public float maxDelay = 1f;
    public GameObject boulderPrefab;
    public Transform[] spawnPoints;

    PlayerHealth playerHealth;
    EnemyHealth enemyHealth;
    GameObject knight;
    GameObject cyclops;
    

    void Start ()
    {
        StartCoroutine (SpawnBoulders());
        knight = GameObject.FindGameObjectWithTag("Knight");
        cyclops = GameObject.FindGameObjectWithTag("Cyclops");
        playerHealth = knight.GetComponent<PlayerHealth>();
        enemyHealth = cyclops.GetComponent<EnemyHealth>();
    }


    IEnumerator SpawnBoulders()
    {   //while (true) statement - loops into infinity - MUST add delay command
        while (true)
        {
            float delay = Random.Range(minDelay, maxDelay);
            yield return new WaitForSeconds(delay);

            if (enemyHealth.currentHealth <= 0)
            {
                break;
            }

            if (playerHealth.currentHealth <= 0)
            {
                break;
            }

            int spawnIndex = Random.Range(0, spawnPoints.Length);
            Transform spawnPoint = spawnPoints[spawnIndex];

            GameObject spawnedBoulder = Instantiate(boulderPrefab, spawnPoint.position, spawnPoint.rotation);
            Destroy(spawnedBoulder, 3f);
        }       
    }
}