﻿using UnityEngine;

public class Blade : MonoBehaviour
{
    public GameObject bladeTrailPrefab;
    public float minCuttingVelocity = 1f;
    
    bool isCutting = false;
    Vector2 previousPosition;

    GameObject currentBladeTrail;    
    Rigidbody2D rb;
    Camera cam;
    CircleCollider2D circleCollider;
    AudioSource cuttingAudio;

    public Animator anim;
    //bool isSlashing; <---not necessary since it's calling from public Animator


    void Start ()
    {
        cam = Camera.main;
        rb = GetComponent <Rigidbody2D>();
        circleCollider = GetComponent <CircleCollider2D>();
        cuttingAudio = GetComponent <AudioSource>();
    }


    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            StartCutting();
            anim.SetBool("IsSlashing", true);
        }

        else if (Input.GetMouseButtonUp(0))
        {
            StopCutting();
            anim.SetBool("IsSlashing", false);
        }

        if (isCutting)
        {
            UpdateCut();
        }
    }


    void UpdateCut ()
    {
        Vector2 newPosition = cam.ScreenToWorldPoint(Input.mousePosition);
        rb.position = newPosition;

        float velocity = (newPosition - previousPosition).magnitude * Time.deltaTime;
        if (velocity > minCuttingVelocity)
        {
            circleCollider.enabled = true;
            cuttingAudio.Play();
        }

        else
        {
            circleCollider.enabled = false;
        }

        previousPosition = newPosition;
    }
      
      
    void StartCutting ()
    {
        isCutting = true;
        currentBladeTrail = Instantiate(bladeTrailPrefab, transform);
        previousPosition = cam.ScreenToWorldPoint(Input.mousePosition);
        circleCollider.enabled = false;
    }


    void StopCutting ()
    {
        isCutting = false;
        currentBladeTrail.transform.SetParent(null);
        Destroy(currentBladeTrail, 1f);
        circleCollider.enabled = false;
    }
}
