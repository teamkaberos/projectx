﻿using UnityEngine;
using UnityEngine.UI;

public class RechargeTimer : MonoBehaviour
{
    enum ShotState
    {
        hold,           // waiting for player input to shoot
        recharge,       // shot fired, getting next arrow
        refill          // shots depleted, refilling ammo
    }
    ShotState currentState = ShotState.hold;        // set to wait in the beginning

    // **** SHOTS INFO **** \\
    // the original full amount of shots available
    public static int totalShotsCount = 5;
    int currentShotsLeft;
    public int CurrentShotsLeft
    {
        get
        {
            return currentShotsLeft;
        }
    }
    public Button[] projectilesLeftImage;

    // **** RECHARGE TIME INFO **** \\
    public static float rechargeTime = 0.5f;
    public static float refillTime = 3f;
    protected float currentRechargeTime;
    protected float currentRefillTime;
    public Image rechargeTimeRemaining;
    public Text shotStateLeftText;



    void Start()
    {
        DisplayAllShots();
        DisplayShotStateDurations();

        // we need to know when single shot is fired
        Projectile.ShotFired += CheckRemaining;
        // and we also need to know when the shots are out for reload
    }
    private void OnDestroy()
    {
        Projectile.ShotFired -= CheckRemaining;
    }
    // Update is called once per frame
    void Update()
    {
        if (currentState == ShotState.recharge)
        {
            currentRechargeTime -= Time.deltaTime;
            if (currentRechargeTime <= 0)
            {
                currentRechargeTime = 0;
                DisplayShotStateDurations();
                currentState = ShotState.hold;
            }
            DisplayShotStateDurations();
        }
        else if (currentState == ShotState.refill)
        {
            currentRefillTime -= Time.deltaTime;
            if (currentRefillTime <= 0)
            {
                currentRefillTime = 0;
                DisplayShotStateDurations();
                DisplayAllShots();
                currentState = ShotState.hold;
            }
            DisplayShotStateDurations();
        }
    }

    void DisplayAllShots()
    {
        currentShotsLeft = totalShotsCount;
        for (int x = 0; x < currentShotsLeft; x++)
        {
            projectilesLeftImage[x].gameObject.SetActive(true);
            projectilesLeftImage[x].interactable = true;
        }
    }
    void DisplayShotStateDurations()
    {
        if (currentState == ShotState.hold)
        {
            currentRechargeTime = rechargeTime;
            currentRefillTime = refillTime;
            shotStateLeftText.text = string.Format("{0}", currentRechargeTime);
        }
        else if (currentState == ShotState.recharge)
        {
            rechargeTimeRemaining.fillAmount = 1 - (currentRechargeTime / rechargeTime);
            shotStateLeftText.text = string.Format("{0:0.00}", currentRechargeTime);
        }
        else if (currentState == ShotState.refill)
        {
            rechargeTimeRemaining.fillAmount = 1 - (currentRefillTime / refillTime);
            shotStateLeftText.text = string.Format("{0:0.00}", currentRefillTime);
        }
    }

    void CheckRemaining()
    {
        CheckShotsNumberLeft();
    }
    void CheckShotsNumberLeft()
    {
        projectilesLeftImage[currentShotsLeft - 1].interactable = false;
        if (currentShotsLeft >= 1)
        {
            currentShotsLeft -= 1;

            if (currentShotsLeft > 0)
            {
                Projectile.isLastShot = false;
                currentState = ShotState.recharge;
            }
            else if (currentShotsLeft == 0)
            {
                Projectile.isLastShot = true;
                currentState = ShotState.refill;
            }
        }
    }
}
