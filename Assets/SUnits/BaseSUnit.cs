﻿using UnityEngine;
using UnityEngine.UI;

public class BaseSUnit : MonoBehaviour
{
    public delegate void HasBeenKilled(BaseSUnit sUnitKilled);
    public static event HasBeenKilled IsNowDead;

    public enum UnitState
    {
        idle,
        attack,
        flinch,
        draw,
        death,
        victory
    }
    //[HideInInspector]
    protected UnitState currentUnitState;
    public UnitState CurrentUnitState
    {
        get { return currentUnitState; }
    }

    [Header("Health")]
    [SerializeField] protected int totalHitPoints;
    protected int currentHitPoints;
    [HideInInspector]
    public int CurrentHitPoints
    {
        get
        {
            return currentHitPoints;
        }
    }
    public HPBar sUnitHPBar;

    [Header("Hit")]
    [SerializeField] protected int damage;
    [HideInInspector]
    public int Damage
    {
        get
        {
            return damage;
        }
    }

    protected Rigidbody unitRB;

    [Header("Base SUnit State Display")]
    public Text stateUpdate;

    protected virtual void Start()
    {
        ResetHitPoints();
        GetRigidbody();

        ChangeState(UnitState.idle);

        // Make sure all inherited Unit classes listens to the death of another unit
        IsNowDead += UnitVictoryBehaviour;
    }
    protected virtual void OnDestroy()
    {
        IsNowDead -= UnitVictoryBehaviour;
    }

    void ResetHitPoints()
    {
        currentHitPoints = totalHitPoints;
        DisplayHitPoints();
    }
    protected virtual void GetRigidbody()
    {
        unitRB = GetComponent<Rigidbody>();
    }


    protected virtual void ChangeState(UnitState unitState)
    {
        currentUnitState = unitState;

        stateUpdate.text = currentUnitState.ToString();
    }


    public virtual void TakeDamage(int damageAmount)
    {
        GetDamage(damageAmount);   // receive the damage taken, so inherited class will react with new hp value
        DisplayHitPoints();                 // update the UI on the damage received
    }


    void GetDamage(int damageTaken)
    {
        currentHitPoints -= damageTaken;
        if (currentHitPoints <= 0)
        {
            currentHitPoints = 0;
            IsNowDead?.Invoke(this);
        }
    }
    void DisplayHitPoints()
    {
        sUnitHPBar.DisplayHitPoints(currentHitPoints, totalHitPoints);
    }


    protected virtual void UnitVictoryBehaviour(BaseSUnit baseSUnit)
    {
    }
}

