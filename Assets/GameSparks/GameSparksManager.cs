﻿using System;
using System.Collections.Generic;
using GameSparks.Api.Messages;
using GameSparks.Api.Requests;
using GameSparks.Api.Responses;
using GameSparks.Core;
using GameSparks.RT;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameSparksManager : MonoBehaviour
{
    public delegate void MailReceived(string senderID, string senderName, string subject, string content, long timeSent, string avatar, string clan);
    public static event MailReceived MailIncoming;
    public delegate void TargetedMailReceived(string objid, string rewardType, int rewardValue);
    public static event TargetedMailReceived TargetedMailIncoming;


    public delegate void RequiredInfoObtained();
    public static event RequiredInfoObtained InformationsReceived;
    public static event RequiredInfoObtained SiegeReceived;

    public delegate void ClanNotificationReceived(string memberID);
    public static event ClanNotificationReceived PromotionNotice;
    public static event ClanNotificationReceived DemotionNotice;

    public delegate void ClanMemberNotificationReceived(string memberID, string memberName);
    public static event ClanMemberNotificationReceived MemberRemovalNotice;
    public static event ClanMemberNotificationReceived MemberJoined;

    public delegate void ClanTreasuryNotificationReceived(string recepientID, int goldDifference, int goldLeft, string senderID);
    public static event ClanTreasuryNotificationReceived GoldIncreased; // handles player deposits
    public static event ClanTreasuryNotificationReceived GoldDropped;   // handles withdrawals

    public delegate void ClanNotificationChanged();
    public static event ClanNotificationChanged ClaNotificationHasChanged;

    public delegate void ClanLeaderChanged(string newLeaderID);
    public static event ClanLeaderChanged ClanLeaderHasChanged;

    public delegate void ClanFlagUpdated(int newFlagNo);
    public static event ClanFlagUpdated ClanFlagChanged;

    public delegate void ClanWarOngoing();
    public static event ClanWarOngoing WarDeclared;

    public delegate void ClanWarPointUpdate(string targetClan, string playerID);
    public static event ClanWarPointUpdate WarPointsChanged;

    public delegate void SUnitsSaved(int amountUpdated);
    public static event SUnitsSaved SUnitsAmountUpdated;


    #region Variables Control Panel

    public static int releasedHeroesAmount = 17;

    #endregion

    // Should be converted to local variable (but check GetPlayerID() comments!)
    public static string playerId;  // Immediately usable as it is initialized in the Start()
    public static string displayName;

    #region singleton
    static GameSparksManager _instance;
    public static GameSparksManager Instance
    {
        get
        {
            return _instance;
        }
    }
    #endregion

    public ChatBoxContent chatBoxContentSmall;
    public ChatBoxContent chatboxContentGlobalChat;
    public ChatBoxContent chatboxContentClanChat;
    public ChatBoxContent chatBoxContentSystemChat;

    public ReceiveManager receiveManager;

    GameSparksRTUnity gameSparksRTUnity;
    public GameSparksRTUnity GetRTSession()
    {
        return gameSparksRTUnity;
    }

    RTSessionInfo sessionInfo;                           //Used whenever there is a real - time event going on                    //Used to trigger any event
    LogChallengeEventRequest logChallengeEventRequest;   //Used to trigger log challeng events
    AcceptChallengeRequest acceptChallengeRequest;
    DeclineChallengeRequest declineChallengeRequest;

    public RTSessionInfo GetSessionInfo()
    {
        return sessionInfo;
    }

    List<ListTeamChatResponse._ChatMessage> globalChatHistory = new List<ListTeamChatResponse._ChatMessage>();
    List<ListTeamChatResponse._ChatMessage> clanChatHistory = new List<ListTeamChatResponse._ChatMessage>();
    bool globalChatLoaded;
    bool clanChatLoaded;
    List<string> chatHistory = new List<string>();
    long millisecsAdd;
    DateTime millisecsAdded;

    //public delegate void OnChallengeBeingIssued(float responseDuration);
    //public static event OnChallengeBeingIssued challengeIsIssued;

    static public bool newPlayer;
    static bool subscribedToSessionTerminate;

    public GameObject sessionTimeOutPopup;
    bool returningHome;
    float returningHomeTime;
    public Text returnHomeTimeDisplayText;
    public Button backToHome;

    public GameObject serverMessagePopup;
    public Text serverMessageText;

    public GameObject disconnectedCanvas;
    public Text timeLeftText;

    static string authTokenString;

    // To Check whether the game should proceed if any of these infos are not obtained
    static public bool clanInfoObtained, clanResourcesObtained, playerClanInfoObtained,

    battleCountObtained, playerDataObtained, playerDetailsObtained, playerExpenablesObtained, battleLogsObtained,

    inventoryChestsObtained, inventoryTimersObtained, inventoryEnergyPacksObtained, inventoryCardPacksObtained, inventoryStonesObtained,

    barracksInfoObtained, minesInfoObtained, townHallInfoObtained, treasuryInfoObtained, forgeInfoObtained, preparationHallInfoObtained,

    heroInfosObtained, assignedUnitsObtained,

    missionsObtained,

    intervalRewardRefreshTimeObtained,

    buildingUnderUpgradeObtained, stoneUnderUpgradeObtained,

    siegeObtained,

    allObtained;

    public static bool isSiege;

    readonly float restartCounterTime = 8;
    float restartCounter;
    bool startCheck;
    bool restarted;

    public float timeOutDurationInMillis = 1000;




    void Awake()
    {
        _instance = this;
        //DontDestroyOnLoad(this);

        //On game start, check if gameSparks is initialized
        GS.GameSparksAvailable = (isAvailable) =>
        {
            if (isAvailable)
            {
                if (SceneManager.GetActiveScene().name.Contains("Login"))
                {
                    Login.Instance.Connectivity(true);
                }
            }
            else
            {
                if (SceneManager.GetActiveScene().name.Contains("Login"))
                {
                    Login.Instance.Connectivity(false);
                }
            }
        };
        GS.GameSparksAvailable = GSConnectivityHandler;
    }
    void GSConnectivityHandler(bool isConnected)
    {
        Debug.Log("GS connection is " + isConnected);
        if (isConnected)
        {
            if (!SceneManager.GetActiveScene().name.Contains("Login"))
            {
                CanvasPanelToggle(false);
            }
            else if (SceneManager.GetActiveScene().name.Contains("Login"))
            {
                Login.Instance.Connectivity(true);
            }
        }
        else if (!isConnected)
        {
            if (!SceneManager.GetActiveScene().name.Contains("Login"))
            {
                CanvasPanelToggle(true);
                ReturnToHomeTimerDisplay(restartCounter);
            }
            else if (SceneManager.GetActiveScene().name.Contains("Login"))
            {
                Login.Instance.Connectivity(false);
            }
        }
    }
    void Start()
    {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;

        restartCounter = restartCounterTime;
        if (SceneManager.GetActiveScene().name.Contains("Login"))
        {
            startCheck = false;
            ResetInfoChecks();
        }
        if (SceneManager.GetActiveScene().name.Contains("Home"))
        {
            Join_Global_Chat_Team();

            GetMyTeams(false, new List<string>(1) { "CLAN" }, "create");

            gameSparksRTUnity = GetComponent<GameSparksRTUnity>();

            startCheck = true;

            TeamMessageListener();
            ScriptMessageListener();

            if (!subscribedToSessionTerminate)
            {
                SessionTermiatedListener();
            }

            logChallengeEventRequest = new LogChallengeEventRequest();

            Invoke("TimeOutCheck", 10f);

            //this is for old players
            if (newPlayer == false && allObtained == false)
                LoadAllData();
            else if (newPlayer == true && allObtained == false)
                CancelInvoke("TimeOutCheck");
        }
        if (SceneManager.GetActiveScene().name.Contains("Battle"))
        {
            startCheck = true;

            if (!subscribedToSessionTerminate)
            {
                SessionTermiatedListener();
            }
        }
        if (SceneManager.GetActiveScene().name.Contains("Farm"))
        {
            startCheck = true;

            if (!subscribedToSessionTerminate)
            {
                SessionTermiatedListener();
            }
        }
    }

    void ReturnHome()
    {
        returningHome = true;
        returningHomeTime = 3.5f;
    }
    public bool AmReturningHome()
    {
        return returningHome;
    }

    void Update()
    {
        if (startCheck)
            CheckConnectivity();

        if (returningHome)
        {
            returningHomeTime -= Time.deltaTime;

            returnHomeTimeDisplayText.text = string.Format("Session Timed out. \n Loading login screen in {0}", Mathf.CeilToInt(returningHomeTime));

            if (returningHomeTime <= 0)
            {
                returningHome = false;
                if (BuildingManager.Instance != null)
                {
                    BuildingManager.Instance.LogOutReset();
                    MissionHolder.Instance.LogoutReset();
                }
                LevelManager.Instance.LoginScene();
            }
        }
    }
    void CheckConnectivity()
    {
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            restartCounter -= Time.deltaTime;
            CanvasPanelToggle(true);
            ReturnToHomeTimerDisplay(restartCounter);
            if (restartCounter <= 0 && !restarted)
            {
                restarted = true;
                LevelManager.Instance.LoginScene();
            }
        }
        else
        {
            restartCounter = restartCounterTime;
            restarted = false;
            CanvasPanelToggle(false);
        }
    }

    void OnDestroy()
    {
        TeamChatMessage.Listener -= TeamMessageRecieved;
    }

    public void Reconnect()
    {
        GS.Reconnect();
    }


    // ====== CONNECTIVITY CHECK ====== \\
    public void CanvasPanelToggle(bool isDisconnected)
    {
        disconnectedCanvas.SetActive(isDisconnected);
    }
    public void ReturnToHomeTimerDisplay(float timeLeft)
    {
        timeLeftText.text = string.Format("Disconnecting ({0})", Mathf.CeilToInt(timeLeft));
    }




    //this can be used to create multiple channels for chatting
    void CreateGlobalChat()
    {
        new CreateTeamRequest()
            .SetTeamId("GlobalChat")
            .SetTeamName("GlobalChat")
            .SetTeamType("GLOBAL")
            .Send((response) =>
            {
                if (!response.HasErrors)
                {
                    Debug.Log("GC created");
                }
                else
                {
                    Debug.Log(response.Errors.JSON);
                }
            });
    }

    public void Notify_Chatbox_Init(string type, ChatBoxContent chatBox)
    {
        if (type == "small")
            chatBoxContentSmall = chatBox;
        else if (type == "popup")
            chatboxContentGlobalChat = chatBox;
    }
    public void Join_Global_Chat_Team()
    {
        new JoinTeamRequest()
        .SetTeamId("globalchat")
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                var owner = response.Owner;
                GSData scriptData = response.ScriptData;
                string teamId = response.TeamId;
                string teamName = response.TeamName;
                string teamType = response.TeamType;
                //chatBoxContentPopup.DisplayMessageInChatbox(string.Format("Joined <color=#00FF00>{0} GLOBAL CHAT</color>", teamType));
            }
            GetGlobalChatMessages();
        });
    }
    void Join_Server_Notification()
    {

    }
    void GetGlobalChatMessages()
    {
        globalChatHistory.Clear();
        new ListTeamChatRequest()
        .SetEntryCount(80)
        .SetTeamId("globalchat")
        .SetTeamType("GLOBAL")
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                foreach (ListTeamChatResponse._ChatMessage chatMessages in response.Messages)
                    globalChatHistory.Add(chatMessages);

                for (int x = globalChatHistory.Count - 1; x >= 0; x--)
                {
                    object ts = null;
                    long timeSent;

                    DateTime newSendTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                    globalChatHistory[x].JSONData.TryGetValue("when", out ts);
                    if (ts != null)
                    {
                        timeSent = Convert.ToInt64(ts);
                        newSendTime = newSendTime.AddMilliseconds(timeSent);
                    }
                    chatboxContentGlobalChat.AssignSenderID(globalChatHistory[x].FromId, globalChatHistory[x].Who);      // Assign chat target ID and Name
                    GetEnemyDetails(globalChatHistory[x].FromId, Mathf.Abs(globalChatHistory.Count - 1 - x), "chatboxGlobal");
                    chatboxContentGlobalChat.DisplayMessageInChatbox(string.Format("<color=#0023E5>{0}</color> : {1}", globalChatHistory[x].Who, globalChatHistory[x].Message),
                                                                                                            newSendTime);
                    chatBoxContentSmall.DisplayMessageInChatbox(string.Format("[Global] <color=#0023E5>{0}</color> : {1}", globalChatHistory[x].Who, globalChatHistory[x].Message),
                                                                                                   newSendTime);
                }

                globalChatLoaded = true;
                // Inform the loading screen to disappear
                TimeMaster.Instance.ChatLoaded();
            }
            else
            {
                chatboxContentGlobalChat.DisplayMessageInChatbox(string.Format("Failed to retrive messages. {0}", response.Errors), new DateTime(0));
            }
        });
    }
    void GetClanChatHistory()
    {
        clanChatLoaded = false;
        clanChatHistory.Clear();
        new ListTeamChatRequest()
        .SetEntryCount(80)
        .SetTeamId(Clan.clanID)
        .SetTeamType("CLAN")
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                foreach (ListTeamChatResponse._ChatMessage chatMessages in response.Messages)
                {
                    if (!chatMessages.Message.StartsWith(" ", 0))
                        clanChatHistory.Add(chatMessages);
                }

                for (int x = clanChatHistory.Count - 1; x >= 0; x--)
                {
                    object ts = null;
                    long timeSent;

                    DateTime newSendTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                    clanChatHistory[x].JSONData.TryGetValue("when", out ts);
                    if (ts != null)
                    {
                        timeSent = Convert.ToInt64(ts);
                        newSendTime = newSendTime.AddMilliseconds(timeSent);
                    }

                    chatboxContentClanChat.AssignSenderID(clanChatHistory[x].FromId, clanChatHistory[x].Who);      // Assign chat target ID and Name
                    GetEnemyDetails(clanChatHistory[x].FromId, Mathf.Abs(clanChatHistory.Count - 1 - x), "chatboxClan");
                    chatboxContentClanChat.DisplayMessageInChatbox(string.Format("<color=#0023E5>{0}</color> : {1}", clanChatHistory[x].Who, clanChatHistory[x].Message),
                                                                                                        newSendTime);
                    chatBoxContentSmall.DisplayMessageInChatbox(string.Format("[Clan] <color=#0023E5>{0}</color> : {1}", clanChatHistory[x].Who, clanChatHistory[x].Message),
                                                                                                 newSendTime);
                }
                // Inform the loading screen to disappear
                ClanChatRecieved();
            }
            else
            {
                chatboxContentClanChat.DisplayMessageInChatbox(string.Format("Failed to retrive messages. {0}", response.Errors), new DateTime(0));
            }
        });
    }
    public void ClanChatRecieved()
    {
        clanChatLoaded = true;
        AllChatsRetrieved();
    }
    void AllChatsRetrieved()
    {
        if (clanChatLoaded == true && globalChatLoaded == true)
            TimeMaster.Instance.ChatLoaded();
    }
    // ====== TEAM MESSAGES ====== \\
    void TeamMessageListener()
    {
        TeamChatMessage.Listener = TeamMessageRecieved;
    }
    void TeamMessageRecieved(TeamChatMessage chatMessage)
    {
        if (NotificationCheck(chatMessage.Message))
        {
            isSiege = false;
            string[] notificationMessage = NotificationParse(chatMessage.Message);
            NotificationTrigger(notificationMessage, chatMessage.FromId);
        }
        else
        {
            long timeSent = chatMessage.ScriptData.GetLong("sentTime").GetValueOrDefault(-404);
            DateTime sendTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddMilliseconds(timeSent * 1000);

            if (chatMessage.TeamType == "GLOBAL")
            {
                // GLOBAL CHAT MESSAGES
                string message = string.Format("<color=#0023E5>{0}</color> : {1}", chatMessage.Who, chatMessage.Message);

                chatboxContentGlobalChat.AssignSenderID(chatMessage.FromId, chatMessage.Who);
                chatBoxContentSmall.DisplayMessageInChatbox("[Global] " + message, sendTime);

                GetChatPlayerDetails(chatMessage.FromId, Mathf.Abs(chatboxContentGlobalChat.chatMessageChallenges.Count), message, "global", sendTime);
            }
            else if (chatMessage.TeamType == "CLAN")
            {
                // CLAN CHAT MESSAGES
                string message = string.Format("<color=#0023E5>{0}</color> : {1}", chatMessage.Who, chatMessage.Message);

                chatboxContentClanChat.AssignSenderID(chatMessage.FromId, chatMessage.Who);
                chatBoxContentSmall.DisplayMessageInChatbox("[Clan] " + message, sendTime);

                GetChatPlayerDetails(chatMessage.FromId, Mathf.Abs(chatboxContentClanChat.chatMessageChallenges.Count), message, "clan", sendTime);
            }
        }
    }
    bool NotificationCheck(string stringToCheck)
    {
        if (stringToCheck.Substring(0, 1) == " ")
            return true;
        return false;
    }
    string[] NotificationParse(string messageToParse)
    {
        messageToParse.Trim();
        return messageToParse.Split(' ');
    }
    void NotificationTrigger(string[] notificationMessage, string senderID)
    {
        switch (notificationMessage[1])
        {
            case "MemberPromotion":
                PromotionNotice(notificationMessage[2]);
                break;
            case "MemberDemotion":
                DemotionNotice(notificationMessage[2]);
                break;
            case "GoldSend":
                GoldDropped(notificationMessage[2], int.Parse(notificationMessage[3]), int.Parse(notificationMessage[4]), senderID);
                break;
            case "GoldDeposit":
                GoldIncreased(notificationMessage[2], int.Parse(notificationMessage[3]), int.Parse(notificationMessage[4]), senderID);
                break;
            case "MemberRemoval":
                MemberRemovalNotice(notificationMessage[2], notificationMessage[3]);
                break;
            case "MemberJoined":
                if (notificationMessage[2] == playerId)
                    return;
                MemberJoined(notificationMessage[2], notificationMessage[3]);
                break;
            case "ClanNotificationUpdate":
                ClaNotificationHasChanged();
                break;
            case "ClanLeaderChange":
                ClanLeaderHasChanged(notificationMessage[2]);
                break;
            case "WarDeclared":
                WarDeclared();
                break;
            case "ClanWarPointChanged":
                WarPointsChanged(notificationMessage[2], notificationMessage[3]);
                break;
            case "FlagChange":
                ClanFlagChanged(int.Parse(notificationMessage[2]));
                break;
        }
    }
    // ====== TEAM MESSAGES END ====== \\


    void ServerMessage(string serverMessage)
    {
        CancelInvoke("HideServerMessage");
        serverMessageText.text = serverMessage;
        serverMessagePopup.SetActive(true);

        chatBoxContentSystemChat.DisplayMessageInChatbox(serverMessage);
        Invoke("HideServerMessage", 15f);
    }
    void HideServerMessage()
    {
        serverMessagePopup.SetActive(false);
    }


    void SessionTermiatedListener()
    {
        subscribedToSessionTerminate = true;
        SessionTerminatedMessage.Listener += PlayerHasDisconnected;
    }
    void ScriptMessageListener()
    {
        ScriptMessage.Listener = (ScriptMessage message) =>
        {
            switch (message.ExtCode)
            {
                case "siegeNotification":
                    if (SceneManager.GetActiveScene().name.Contains("Home"))
                    {
                        playerExpenablesObtained = false;
                        playerDataObtained = false;
                        siegeObtained = false;
                        isSiege = true;

                        TimeMaster.Instance.BlockerToggle(true);

                        GetPlayerExpendables(playerId);     // Obtaining updated expendables
                        GetPlayerData(playerId);            // Obtaining updated data
                        GetSeige("home");
                    }
                    break;
                case "mailReceived":
                    if (SceneManager.GetActiveScene().name.Contains("Home"))
                    {
                        GSData data = message.Data.GetGSData("Mail");

                        string sender = data.GetString("SenderName");
                        string senderID = data.GetString("SenderID");
                        string senderPortrait = data.GetString("SenderAvatar");
                        string clanID = data.GetString("SenderClan");
                        string subject = data.GetString("Subject");
                        string content = data.GetString("Content");
                        long timeSent = data.GetLong("TimeSent").GetValueOrDefault(-404);

                        MailIncoming(senderID, sender, subject, content, timeSent, senderPortrait, clanID);
                    }
                    break;
                case "targetedGiftReceived":
                    if (SceneManager.GetActiveScene().name.Contains("Home"))
                    {
                        GSData data = message.Data.GetGSData("TargetMail");

                        string rewardType = data.GetString("rewardTypeSent");
                        string objID = data.GetString("objID");
                        int rewardValue = data.GetInt("rewardValueSent").GetValueOrDefault(-404);

                        TargetedMailIncoming(objID, rewardType, rewardValue);
                    }
                    break;
                case "toggleLiveEvent":
                    if (SceneManager.GetActiveScene().name.Contains("Home"))
                    {
                        string eventActivity = message.Data.GetString("EventActivityStatus");

                        string liveActivity = message.Data.GetString("LiveEventStatus");

                        EventHolderManager.Instance.SetEventActivityFromServer(eventActivity, liveActivity);
                    }
                    break;
                case "serverShutdown":
                    {
                        string data = message.Data.GetString("Notice");

                        ServerMessage(data);
                    }
                    break;
            }
        };
    }


    public void StartNewRTSession(RTSessionInfo _info)
    {
        sessionInfo = _info;
        //gameSparksRTUnity = gameObject.AddComponent<GameSparksRTUnity>(); // Adds the RT script to the game
        // In order to create a new RT game we need a 'FindMatchResponse' //
        // This would usually come from the server directly after a successful MatchmakingRequest //
        // However, in our case, we want the game to be created only when the first player decides using a button //
        // therefore, the details from the response is passed in from the gameInfo and a mock-up of a FindMatchResponse //
        // is passed in. //
        GSRequestData mockedResponse = new GSRequestData()
                                            .AddNumber("port", (double)_info.GetPortID())
                                            .AddString("host", _info.GetHostURL())
                                            .AddString("accessToken", _info.GetAccessToken()); // construct a dataset from the game-details

        FindMatchResponse response = new FindMatchResponse(mockedResponse); // create a match-response from that data and pass it into the game-config
        // So in the game-config method we pass in the response which gives the instance its connection settings //
        // In this example, I use a lambda expression to pass in actions for 
        // OnPlayerConnect, OnPlayerDisconnect, OnReady and OnPacket actions //
        // These methods are self-explanatory, but the important one is the OnPacket Method //
        // this gets called when a packet is received //

        gameSparksRTUnity.Configure(response,
            (peerId) => { OnPlayerConnectedToGame(peerId); },
            (peerId) => { OnPlayerDisconnected(peerId); },
            (ready) => { OnRTReady(ready); },
            (packet) => { OnPacketReceived(packet); });
        gameSparksRTUnity.Connect(); // when the config is set, connect the game

    }

    //When a player connects to the game
    void OnPlayerConnectedToGame(int _peerId)
    {
        Debug.Log("GSM| Player Connected, " + _peerId);
    }
    //When a player gets disconnected
    void OnPlayerDisconnected(int _peerId)
    {
        Debug.Log("GSM| Player Disconnected, " + _peerId);
    }
    //Means the real-time session match is ready
    void OnRTReady(bool _isReady)
    {
        if (_isReady)
        {
            Debug.Log("GSM| RT Session Connected . . .");
            Debug.Log("Loading Battle Scene . . .");
            Debug.Log("Match Started !");
        }
    }

    void OnPacketReceived(RTPacket _packet)
    {
        switch (_packet.OpCode)
        {
            // op-code 1 refers to any chat-messages being received by a player //
            // from here, we'll send them to the chat-manager //
            case 1:
                Debug.Log("Recieved . . ."); // send the whole packet to the chat-manager
                break;

        }
    }
    //    ==THE TYPES OF INFO THAT CAN BE RECIEVED WITH RT_PACKET==
    //    int myInt = (int)_packet.Data.GetInt(1); // get int at key 1
    //    float myFloat = (float)_packet.Data.GetFloat(2); // get float at key 2                        
    //    double myDouble = (double)_packet.Data.GetDouble(3); // get double at key 3                  
    //    long myLong = (long)_packet.Data.GetLong(4); // get long at key 4                         
    //    string myString = _packet.Data.GetString(5); // get string at key 5    
    //    Vector2 myVector2 = (Vector2)_packet.Data.GetVector2(6); // get vector2 at key 6
    //    Vector3 myVector3 = (Vector3)_packet.Data.GetVector3(7); // get vector3 at key 7
    //    Vector4 myVector4 = (Vector4)_packet.Data.GetVector4(8); // get vector4 at key 8
    //    RTVector myRTVector = (RTVector)_packet.Data.GetRTVector(9); // get RTVector at key 9
    //    int myNestedInt = (int)_packet.Data.GetData(10).GetInt(1); // get nested data at key 10
    //    StreamReader reader = new StreamReader(_packet.Stream);
    //    string message = reader.ReadToEnd();
    //    reader.Close();

    //When the player gets disconnect
    public void PlayerHasDisconnected(SessionTerminatedMessage _message)
    {
        ReturnHome();
        sessionTimeOutPopup.SetActive(true);
        authTokenString = _message.AuthToken;
    }

    void DisconnectPlayer()
    {
        new LogEventRequest()
                       .SetEventKey("PLAYER_LOG_OUT")
                       .Send((response) =>
                        {
                            if (!response.HasErrors)
                                Debug.Log("Logged out successfully");
                            else
                                Debug.Log(response.Errors.JSON);
                        });
    }

    void OnApplicationQuit()
    {
        PlayerLogout();
    }
    public void PlayerLogout()
    {
        Leave_Global_Chat();
    }
    void Leave_Global_Chat()
    {

    }


    // ===== VERSIONING CHECK ===== \\
    public void CheckGameVersion()
    {
        new LogEventRequest().SetEventKey("gameVersionCheck")
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                string version = response.ScriptData.GetGSData("version").GetString("version");
                string minversion = response.ScriptData.GetGSData("version").GetString("minversion");
                string maintenance = response.ScriptData.GetGSData("version").GetString("maintenance");

                // if the game is not under maintenance
                if (maintenance.ToLower() == "false")
                {
                    // check for the game version here (if player's versions corresponds)
                    if (Player.gameVersion == minversion || Player.gameVersion == version)
                    {
                        // CORRECT VERSION AND CONNECTED
                        // For Testing purpose Login Check
                        if (Login.Instance.forTestLogin)
                        {
                            Login.Instance.AutoLogin(Login.Instance.usernameintput, Login.Instance.passwordinput);
                            Login.Instance.RememberMe(Login.Instance.usernameintput, Login.Instance.passwordinput);
                        }
                        else
                        {
                            if (PlayerPrefs.HasKey("lastPlayerUsername"))
                            {
                                Login.Instance.AutoLogin();
                            }
                            else
                            {
                                Login.Instance.LoadingPopupMessage("", false);
                                Login.Instance.Connectivity(true);
                            }
                        }
                    }
                    // if game version does not tally
                    else
                    {
                        Login.Instance.PopupMessage("Game client out of date. Please update.", true);
                        Login.Instance.LoadingPopupMessage("", false);
                        Login.Instance.VersionIsWrong();
                        Login.versionChecked = false;
                    }
                }
                //  else if the game is under maintenance
                else if (maintenance.ToLower() == "true")
                {
                    // Allow game for testing here

                    if (Login.Instance.forTestLogin == true)
                    {
                        Login.Instance.AutoLogin(Login.Instance.usernameintput, Login.Instance.passwordinput);
                    }
                    else
                    {
                        Login.Instance.PopupMessage("Server under going maintenance. Please check back in a while.", true);
                        Login.Instance.LoadingPopupMessage("", false);
                        Login.Instance.Maintainance();
                        Login.versionChecked = false;
                    }
                }
            }
            else if (response.Errors.JSON.Contains("timeout"))
            {
                Login.Instance.PopupMessage("Connection timed out. Please check your internet connection", true);
                Login.Instance.Connectivity(false);
                Login.Instance.ToggleLoginButtons(false);
                Login.Instance.ToggleRetryConnectionButton(true);
                Login.Instance.LoadingPopupMessage("", false);
                Login.versionChecked = false;
            }
            else
            {
                // If there are errors
                VersionCheckLogin();
                //Debug.Log(response.Errors.JSON);
            }
        });
    }


    // ------- PLAYER'S ACCOUNT DETAILS STUFF -------- //
    public void VersionCheckLogin()
    {
        new AuthenticationRequest()
        .SetUserName("versioncheck")
        .SetPassword("versioncheck")
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                CheckGameVersion();
            }
            else if (response.Errors.JSON.Contains("timeout"))
            {
                // timeout means unable to reach server
                Login.Instance.PopupMessage("Connection timed out. Please check your internet connection", true);
                Login.Instance.Connectivity(false);
                Login.Instance.ToggleLoginButtons(false);
                Login.Instance.ToggleRetryConnectionButton(true);
                Login.Instance.LoadingPopupMessage("", false);
            }
            else
            {
                Login.Instance.PopupMessage(LoginErrorResponseParse(response.Errors.JSON), true);
                Login.Instance.ToggleLoginButtons(false);
                Login.Instance.ToggleRetryConnectionButton(true);
                Login.Instance.LoadingPopupMessage("", false);
            }
        });
    }

    public void GameSparksLogin()
    {
        //		// Getting the values from the InputFields
        string inputName = Login.Instance.loginUsernameInput.text;
        string inputPassword = Login.Instance.loginPasswordInput.text;

        new AuthenticationRequest().SetUserName(inputName)
       .SetPassword(inputPassword)
       .Send((response) =>
       {
           if (!response.HasErrors)
           {
               Player.playerName = response.DisplayName;
               Player.email = response.ScriptData.GetString("email");
               playerId = response.UserId;

               Login.Instance.RememberMe(inputName, inputPassword);
               LevelManager.Instance.HomeScene();

               GetInitialLoginServerTime();
               GetLastLogout(playerId);
           }
           else
           {
               Debug.Log(response.Errors.JSON);

               Login.Instance.LoadingPopupMessage("", false);
               Login.Instance.ReenableLoginButtons();
               Login.Instance.PopupMessage(LoginErrorResponseParse(response.Errors.JSON), true);

           }
       });
    }
    // Overriden Method, used for user that have logged in previously.
    public void GameSparksLogin(string locallySavedName, string locallySavedPassword)	// Passes the login details saved in PlayerPrefs
    {
        new AuthenticationRequest().SetUserName(locallySavedName)
       .SetPassword(locallySavedPassword)
       .Send((response) =>
       {
           if (!response.HasErrors)
           {
               Player.playerName = locallySavedName;
               Player.email = response.ScriptData.GetString("email");
               playerId = response.UserId;
               Player.playerName = response.DisplayName;
               LevelManager.Instance.HomeScene();

               GetInitialLoginServerTime();
               GetLastLogout(playerId);
           }
           else
           {
               if (response.Errors.JSON.Contains("timeout"))
               {
                   Login.Instance.PopupMessage("Connection timed out. Please check your internet connection", true);
                   Login.Instance.ToggleLoginButtons(false);
               }
               else
               {
                   Login.Instance.PopupMessage(LoginErrorResponseParse(response.Errors.JSON), true);

               }
               Login.Instance.ToggleLoginButtons(false);
               Login.Instance.ToggleRetryConnectionButton(true);
               Login.Instance.LoadingPopupMessage("", false);
           }
       });
    }

    string LoginErrorResponseParse(string errorNote)
    {
        if (errorNote.ToLower().Contains("unrecognised"))
            return "Username and password do not match.";
        if (errorNote.ToLower().Contains("locked"))
            return "Account locked. Please contact admin.";

        return "";
    }

    public void ForTestGetPlayerDetails()
    {
        new LogEventRequest()
        .SetEventKey("getPlayerDetails")            // Pass the event name to Sparks server here
            .SetEventAttribute("PLAYER_ID", "5b688974b3bd4b04e7b31ddb")   // List all the attributes
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                GSData data = response.ScriptData;   // The JSON file

                //string a = data.GetGSData(playerId).GetString("portrait");
                //string b = data.GetGSData(playerId).GetInt("tutorial").GetValueOrDefault(-404);
                //string c = data.GetGSData(playerId).GetString("clanID");
                //playerDetailsObtained = true;

                //Debug.Log("My Clan = " + Clan.clanID + " My Portrait = " + Player.potraitID + " My tutorial = " + Player.tutorial);
            }
            else if (response.Errors.GetString("ERROR").Contains("No results found"))
            {
            }
            else
                print(response.Errors.JSON);
        });
    }
    public void ForTestGSLogin2(string name, string password, string color, SendMessage msgSend)
    {
        new AuthenticationRequest().SetUserName(name)
        .SetPassword(password)
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                Player.playerName = response.UserId;
                msgSend.FillInInfo(response.UserId, response.DisplayName, color);
            }
        });
    }
    public void ForEventTestGSLogin()
    {
        new AuthenticationRequest()
        .SetUserName("RylaiCrestmaiden")
        .SetPassword("1234")
        .Send((response) =>
            {
                if (!response.HasErrors)
                {
                    playerId = response.UserId;
                    GetInitialLoginServerTime();
                    Debug.Log("Connected");
                }
                else
                    Debug.Log(response.Errors.JSON);
            });
    }

    public void GameSparksRegister()
    {
        new RegistrationRequest()
         .SetDisplayName(Login.Instance.Username.text)
         .SetPassword(Login.Instance.Password.text)
         .SetUserName(Login.Instance.Username.text)
         .Send((response) =>
         {
             if (!response.HasErrors)
             {
                 SaveEmail(Login.Instance.Email.text);
                 newPlayer = response.NewPlayer.GetValueOrDefault(true);
                 Login.Instance.RememberMe(Login.Instance.Username.text, Login.Instance.Password.text);
                 GetSparksPlayerID();
                 GetInitialLoginServerTime();
                 Player.playerName = Login.Instance.Username.text;
                 LevelManager.Instance.HomeScene();
             }
             else
             {
                 Login.Instance.LoadingPopupMessage("", false);
                 Login.Instance.ReenableRegistrationButtons();
                 Login.Instance.PopupMessage("Username already taken", true);
             }
         });
    }
    void SaveEmail(string emailadd)
    {
        new LogEventRequest()
        .SetEventKey("saveEmail")
        .SetEventAttribute("emailAddress", emailadd)
        .Send((response) =>
        {
            if (response.HasErrors)
            {
                Player.email = emailadd;
            }
        });
    }

    public void ChangePlayerDetails(string newUsername, string oldPassword, string newPassword, AccountDetails acct)
    {
        if (newUsername != "")
        {
            new ChangeUserDetailsRequest()
                .SetUserName(newUsername)
                .SetDisplayName(newUsername)
                .SetOldPassword(oldPassword)
                .SetNewPassword(newPassword)
                .Send((response) =>
                {
                    if (!response.HasErrors)
                    {
                        // in the case where the account detail update is successfull
                        acct.MessagePopup("Account successfully updated", true);
                        Login.Instance.RememberMe(newUsername, newPassword);
                        Player.playerName = newUsername;
                        UIManager.Instance.UpdatePlayerInfo();
                        UIManager.Instance.UpdatePlayerTabInfo();
                    }
                    else
                        acct.MessagePopup(UserDetailsResponse(response.Errors.JSON), false);
                });
        }
        else if (newUsername == "")
        {
            new ChangeUserDetailsRequest()
                .SetOldPassword(oldPassword)
                .SetNewPassword(newPassword)
                .Send((response) =>
                {
                    if (!response.HasErrors)
                    {
                        // in the case where the account detail update is successfull
                        acct.MessagePopup("Account successfully updated", true);
                        Login.Instance.RememberMe(Player.playerName, newPassword);
                    }
                    else
                        acct.MessagePopup(UserDetailsResponse(response.Errors.JSON), false);
                });
        }
    }

    string UserDetailsResponse(string errorNote)
    {
        if (errorNote.ToLower().Contains("unrecognised"))
            return "Passwords did not match.";
        return "Username already taken.";
    }

    #region Encapsulated Methods
    public void GetSparksPlayerID()
    {
        new LogEventRequest()
            .SetEventKey("getPlayerID")
            .Send((response) =>
           {
               if (!response.HasErrors)
               {
                   GSData data = response.ScriptData;
                   if (response.ScriptData == null)
                       print("Empty script data");
                   else
                   {
                       playerId = data.GetString("playerId");
                       //SavePlayerClanInfo(playerId);
                   }
               }
               else
                   Debug.Log(response.Errors.JSON);
           });
    }

    public void SaveMine(int mineNo)
    {
        new LogEventRequest()
            .SetEventKey("saveStructureMines" + mineNo)
            .SetEventAttribute("level", Player.mines[mineNo])
            .SetEventAttribute("value", (long)BuildingManager.Instance.goldMine[mineNo].currentResourceValue)
            .SetEventAttribute("capacity", (long)BuildingManager.Instance.goldMine[mineNo].generatorCap)
            .SetEventAttribute("generation", (long)BuildingManager.Instance.goldMine[mineNo].generationRate)
            .Send((response) =>
           {
               if (response.HasErrors)
                   Debug.Log(response.Errors.JSON);
           });
    }
    public void GetMine(int mineNo)
    {
        new LogEventRequest()
            .SetEventKey("getStructureMines" + mineNo)          // Pass the event name to Sparks server here
            .SetEventAttribute("PLAYER_ID", playerId)   // List all the attributes
            .Send((response) =>
           {
               if (!response.HasErrors)
               {
                   GSData data = response.ScriptData;  // The JSON file

                   if (response.ScriptData == null)
                   {
                       DataManager.minesLv[mineNo] = 0;
                   }

                   int level = data.GetGSData(playerId).GetInt("level").GetValueOrDefault(-404);    // In this case, the persistor is the entry name 'playerId'
                   int value = data.GetGSData(playerId).GetInt("value").GetValueOrDefault(-404);   // Get the Script data that is saved using the persistor name.

                   DataManager.minesLv[mineNo] = level;
                   DataManager.minesValue[mineNo] = value;
                   if (mineNo == 4)
                   {
                       minesInfoObtained = true;
                       InfoReceivedCheck();
                   }
               }
               else if (response.Errors.GetString("ERROR").Contains("No results found"))
               {
                   minesInfoObtained = true;
                   InfoReceivedCheck();
               }
               else
                   Debug.Log(response.Errors.JSON);
           });
    }

    public void SaveHeroInfo(int heroID)
    {
        string heroName = HeroNameChecker(heroID);
        new LogEventRequest()
            .SetEventKey("saveHeroInfo")
            .SetEventAttribute("name", heroName)
            .SetEventAttribute("ID", heroID.ToString())
            .SetEventAttribute("level", UnitManager.heroLevels[heroID])
            .SetEventAttribute("cardAmount", UnitManager.heroCardsAmount[heroID])
            .SetEventAttribute("isUnlocked", UnitManager.heroUnlocked[heroID])
            .Send((response) =>
           {
               if (!response.HasErrors)
               { }
               else
                   Debug.Log(response.Errors.JSON);
           }
            );
    }
    public void GetHeroInfo(int heroID)
    {
        string heroName = HeroNameChecker(heroID);

        new LogEventRequest()
            .SetEventKey("getHeroInfo")         // Pass the event name to Sparks server here
            .SetEventAttribute("PLAYER_ID", playerId)   // List all the attributes
            .Send((response) =>
           {
               if (!response.HasErrors)
               {
                   GSData data = response.ScriptData;   // The JSON file

                   if (data.GetGSData(playerId + heroName) != null)
                   {
                       string name = data.GetGSData(playerId + heroName).GetString("name");
                       string ID = data.GetGSData(playerId + heroName).GetString("ID");
                       UnitManager.heroLevels[heroID] = data.GetGSData(playerId + heroName).GetInt("level").GetValueOrDefault(-404);
                       UnitManager.heroCardsAmount[heroID] = data.GetGSData(playerId + heroName).GetInt("cardAmount").GetValueOrDefault(-404);
                       UnitManager.heroUnlocked[heroID] = data.GetGSData(playerId + heroName).GetInt("isUnlocked").GetValueOrDefault(-404);
                       if (heroID == releasedHeroesAmount - 1)
                       {
                           heroInfosObtained = true;
                           InfoReceivedCheck();
                       }
                   }
                   else
                   {
                       heroInfosObtained = true;
                       InfoReceivedCheck();
                   }
               }
               else if (response.Errors.GetString("ERROR").Contains("No results found"))
               {
                   heroInfosObtained = true;
                   InfoReceivedCheck();
               }
               else
                   Debug.Log(response.Errors.JSON);
           });
    }

    // Convert Hero's ID to name
    public string HeroNameChecker(int heroID)
    {
        string heroName = "";
        switch (heroID)
        {
            case 0:
                heroName = "Tanker";
                break;
            case 1:
                heroName = "Haste";
                break;
            case 2:
                heroName = "Haste3";
                break;
            case 3:
                heroName = "Snail";
                break;
            case 4:
                heroName = "Snail3";
                break;
            case 5:
                heroName = "Healer";
                break;
            case 6:
                heroName = "Blocker";
                break;
            case 7:
                heroName = "Refresher";
                break;
            case 8:
                heroName = "Teleporter";
                break;
            case 9:
                heroName = "LaneChanger";
                break;
            case 10:
                heroName = "Cooldowner";
                break;
            case 11:
                heroName = "SoulCharger";
                break;
            case 12:
                heroName = "Ninja";
                break;
            case 13:
                heroName = "Speeder";
                break;
            case 14:
                heroName = "Morpher";
                break;
            case 15:
                heroName = "Freezer";
                break;
            case 16:
                heroName = "Invisibility";
                break;
            default:
                break;
        }

        return heroName;
    }

    // Passes a code to identify which barracks to get
    public void SaveBarracks(int code)
    {
        if (Player.tutorial < 3)
            return;
        string barrackType = "";
        switch (code)
        {
            case 0:
                if (DataManager.guildStoneSlot < 1)
                    DataManager.guildStoneSlot = 1;
                barrackType = "Caster";
                new LogEventRequest()
                .SetEventKey("saveStructure" + barrackType + "Barracks")
                .SetEventAttribute("level", DataManager.guildLv)
                .SetEventAttribute("slot0TimeLeft", DataManager.stonesUnderUpgrade[2])
                .SetEventAttribute("slot1TimeLeft", DataManager.stonesUnderUpgrade[5])
                .SetEventAttribute("stoneResearchSlot", DataManager.guildStoneSlot)
                .Send((response) =>
               {
                   if (!response.HasErrors) { }
                   else
                       Debug.Log(response.Errors.JSON);
               }
                );
                break;
            case 1:
                if (DataManager.bunkerStoneSlot < 1)
                    DataManager.bunkerStoneSlot = 1;
                barrackType = "Melee";
                new LogEventRequest()
                .SetEventKey("saveStructure" + barrackType + "Barracks")
                .SetEventAttribute("level", DataManager.bunkerLv)
                .SetEventAttribute("slot0TimeLeft", DataManager.stonesUnderUpgrade[0])
                .SetEventAttribute("slot1TimeLeft", DataManager.stonesUnderUpgrade[3])
                .SetEventAttribute("stoneResearchSlot", DataManager.bunkerStoneSlot)
                .Send((response) =>
                {
                    if (!response.HasErrors) { }
                    else
                        Debug.Log(response.Errors.JSON);
                }
                );
                break;
            case 2:
                if (DataManager.rangeStoneSlot < 1)
                    DataManager.rangeStoneSlot = 1;
                barrackType = "Range";
                new LogEventRequest()
                .SetEventKey("saveStructure" + barrackType + "Barracks")
                .SetEventAttribute("level", DataManager.rangeLv)
                .SetEventAttribute("slot0TimeLeft", DataManager.stonesUnderUpgrade[1])
                .SetEventAttribute("slot1TimeLeft", DataManager.stonesUnderUpgrade[4])
                .SetEventAttribute("stoneResearchSlot", DataManager.rangeStoneSlot)
                .Send((response) =>
                {
                    if (!response.HasErrors) { }
                    else
                        Debug.Log(response.Errors.JSON);
                }
                );
                break;
            default:
                print("Invalid paramater passed (only 0-2 allowed)");
                break;
        }
        //		new LogEventRequest ()
        //			.SetEventKey ("saveStructure" + barrackType + "Barracks")
        //			.SetEventAttribute ("level", 11)
        //			.SetEventAttribute ("slot0TimeLeft", 11)
        //			.SetEventAttribute ("slot1TimeLeft", 11)
        //			.SetEventAttribute ("stoneResearchSlot", 11)
        //			.Send ((response)=>
        //			{
        //				if (!response.HasErrors)
        //					print("Successfully saved");
        //				else
        //					print("Failed to save data");
        //			}
        //			);
    }
    public void GetBarracks(int code)
    {
        string barrackType = "";
        switch (code)
        {
            case 0:
                barrackType = "Caster";
                break;
            case 1:
                barrackType = "Melee";
                break;
            case 2:
                barrackType = "Range";
                break;
            default:
                print("Invalid paramater passed (only 0-2 allowed)");
                break;
        }
        new LogEventRequest()
            .SetEventKey("getStructure" + barrackType + "Barracks")         // Pass the event name to Sparks server here
            .SetEventAttribute("PLAYER_ID", playerId)   // List all the attributes
            .Send((response) =>
           {
               if (!response.HasErrors)
               {
                   GSData data = response.ScriptData;  // The JSON file

                   if (response.ScriptData == null)
                       print("Empty script data");

                   int level = data.GetGSData(playerId).GetInt("level").GetValueOrDefault(-404);   // In this case, the persistor is the entry name 'playerId'
                   int slot0TimeLeft = data.GetGSData(playerId).GetInt("slot0TimeLeft").GetValueOrDefault(-404);   // Get the Script data that is saved using the persistor name.
                   int slot1TimeLeft = data.GetGSData(playerId).GetInt("slot1TimeLeft").GetValueOrDefault(-404);   // Get the Script data that is saved using the persistor name.
                   int stoneResearchSlot = data.GetGSData(playerId).GetInt("stoneResearchSlot").GetValueOrDefault(-404);   // Get the Script data that is saved using the persistor name.
                   if (stoneResearchSlot >= 2)
                       stoneResearchSlot = 2;

                   switch (code)
                   {
                       case 0:
                           //
                           DataManager.guildLv = level;
                           DataManager.stonesUnderUpgrade[2] = slot0TimeLeft;
                           DataManager.stonesUnderUpgrade[5] = slot1TimeLeft;
                           DataManager.guildStoneSlot = stoneResearchSlot;
                           break;
                       case 1:
                           DataManager.bunkerLv = level;
                           DataManager.stonesUnderUpgrade[0] = slot0TimeLeft;
                           DataManager.stonesUnderUpgrade[3] = slot1TimeLeft;
                           DataManager.bunkerStoneSlot = stoneResearchSlot;
                           break;
                       case 2:
                           DataManager.rangeLv = level;
                           DataManager.stonesUnderUpgrade[1] = slot0TimeLeft;
                           DataManager.stonesUnderUpgrade[4] = slot1TimeLeft;
                           DataManager.rangeStoneSlot = stoneResearchSlot;
                           barracksInfoObtained = true;
                           InfoReceivedCheck();
                           break;
                   }
               }
               else if (response.Errors.GetString("ERROR").Contains("No results found"))
               {
                   barracksInfoObtained = true;
                   InfoReceivedCheck();
               }
               else
                   print("Failed to load data");
           });
    }

    public void SaveAssignedUnitsSet(int setNo, string accessedPlayerID)
    {
        new LogEventRequest()
        .SetEventKey("saveAssignedUnitsSet" + setNo)
        .SetEventAttribute("slot0", DataManager.assignedUnitsSlot[(setNo - 1), 0])
        .SetEventAttribute("slot1", DataManager.assignedUnitsSlot[(setNo - 1), 1])
        .SetEventAttribute("slot2", DataManager.assignedUnitsSlot[(setNo - 1), 2])
        .SetEventAttribute("slot0level", GetUnitLevel(DataManager.assignedUnitsSlot[(setNo - 1), 0]))
        .SetEventAttribute("slot1level", GetUnitLevel(DataManager.assignedUnitsSlot[(setNo - 1), 1]))
        .SetEventAttribute("slot2level", GetUnitLevel(DataManager.assignedUnitsSlot[(setNo - 1), 2]))
        .SetEventAttribute("accessedID", accessedPlayerID)
        .Send((response) =>
       {
           if (response.HasErrors)
               Debug.Log(response.Errors.JSON);
       }
        );
    }
    int GetUnitLevel(int unitID)
    {
        int level = 0;
        for (int x = 0; x < UnitManager.Instance.heroInfomation.Count; x++)
        {
            if (x == unitID)
            {
                level = UnitManager.Instance.heroInfomation[x].heroLvl;
                break;
            }
        }
        return level;
    }
    public void GetAssignedUnitsSet(int setNo, string accessedPlayerID)
    {
        new LogEventRequest()
        .SetEventKey("getAssignedUnitsSet" + setNo)         // Pass the event name to Sparks server here
        .SetEventAttribute("PLAYER_ID", accessedPlayerID)   // List all the attributes
        .Send((response) =>
       {
           if (!response.HasErrors)
           {
               GSData data = response.ScriptData;   // The JSON file

               if (response.ScriptData == null)
                   print("Empty script data");

               DataManager.assignedUnitsSlot[(setNo - 1), 0] = data.GetGSData(playerId).GetInt("slot0").GetValueOrDefault(-404);
               DataManager.assignedUnitsSlot[(setNo - 1), 1] = data.GetGSData(playerId).GetInt("slot1").GetValueOrDefault(-404);
               DataManager.assignedUnitsSlot[(setNo - 1), 2] = data.GetGSData(playerId).GetInt("slot2").GetValueOrDefault(-404);

               if (setNo == 3)
               {
                   assignedUnitsObtained = true;
                   InfoReceivedCheck();
               }

               //print ("set  " + (setNo - 1) + DataManager.assignedUnitsSlot[(setNo - 1), 0]); 
               //print ("set  " + (setNo - 1) + DataManager.assignedUnitsSlot[(setNo - 1), 1]); 
               //print ("set  " + (setNo - 1) + DataManager.assignedUnitsSlot[(setNo - 1), 2]); 
           }
           else if (response.Errors.GetString("ERROR").Contains("No results found"))
           {
               assignedUnitsObtained = true;
               InfoReceivedCheck();
           }
           else
               Debug.Log(response.Errors.JSON);
       });
    }

    public void SaveMission(int missionSlotNo)
    {
        new LogEventRequest()
            .SetEventKey("saveMission" + missionSlotNo)
            .SetEventAttribute("ID", DataManager.missionIDs[missionSlotNo])
            .SetEventAttribute("state", DataManager.missionStates[missionSlotNo])
            .SetEventAttribute("refreshTime", (long)DataManager.missionRefreshTimes[missionSlotNo])
            .SetEventAttribute("progress", DataManager.missionProgresses[missionSlotNo])
            .Send((response) =>
           {
               if (response.HasErrors)
                   Debug.Log(response.Errors.JSON);
           });
    }
    public void GetMission(int missionSlotNo)
    {
        new LogEventRequest()
        .SetEventKey("getMission" + missionSlotNo)          // Pass the event name to Sparks server here
        .SetEventAttribute("PLAYER_ID", playerId)   // List all the attributes
        .Send((response) =>
       {
           if (!response.HasErrors)
           {
               GSData data = response.ScriptData;   // The JSON file
               DataManager.missionIDs[missionSlotNo] = data.GetGSData(playerId).GetInt("ID").GetValueOrDefault(-404);
               DataManager.missionStates[missionSlotNo] = data.GetGSData(playerId).GetInt("state").GetValueOrDefault(-404); // Get the Script data that is saved using the persistor name.
               DataManager.missionProgresses[missionSlotNo] = data.GetGSData(playerId).GetInt("progress").GetValueOrDefault(-404);
               DataManager.missionRefreshTimes[missionSlotNo] = data.GetGSData(playerId).GetInt("refreshTime").GetValueOrDefault(-404);

               if (missionSlotNo == 2)
               {
                   missionsObtained = true;
                   InfoReceivedCheck();
               }
           }
           else if (response.Errors.GetString("ERROR").Contains("No results found"))
           {
               missionsObtained = true;
               InfoReceivedCheck();
           }
           else
               Debug.Log(response.Errors.JSON);
       });
    }
    #endregion

    // =================== CONTROL PANEL ======================== \\

    // To bring user back to home scene if connection time out
    void TimeOutCheck()
    {
        if (allObtained == false)
            sessionTimeOutPopup.SetActive(true);
    }
    // Check if the information was successfully pulled
    public void InfoReceivedCheck()
    {
        if (battleCountObtained == false)
        {
            //Debug.Log("BattleCount failed");
            return;
        }
        if (playerDataObtained == false)
        {
            //Debug.Log("playerData failed");
            return;
        }
        if (playerDetailsObtained == false)
        {
            //Debug.Log("playerDetails failed");
            return;
        }
        if (playerExpenablesObtained == false)
        {
            //Debug.Log("playerExpenable failed");
            return;
        }
        if (battleLogsObtained == false)
        {
            //Debug.Log("battleLogs failed");
            return;
        }
        if (inventoryChestsObtained == false)
        {
            //Debug.Log("inventoryChests failed");
            return;
        }
        if (inventoryTimersObtained == false)
        {
            //Debug.Log("inventoryTimers failed");
            return;
        }
        if (inventoryEnergyPacksObtained == false)
        {
            //Debug.Log("inventoryEnergyPacks failed");
            return;
        }
        if (inventoryCardPacksObtained == false)
        {
            //Debug.Log("inventoryCardPacksObtained failed");
            return;
        }
        if (inventoryStonesObtained == false)
        {
            //Debug.Log("inventoryStonesObtained failed");
            return;
        }
        if (barracksInfoObtained == false)
        {
            //Debug.Log("barracksInfoObtained failed");
            return;
        }
        if (minesInfoObtained == false)
        {
            //Debug.Log("minesInfoObtained failed");
            return;
        }
        if (townHallInfoObtained == false)
        {
            //Debug.Log("townHallInfoObtained failed");
            return;
        }
        if (treasuryInfoObtained == false)
        {
            //Debug.Log("treasuryInfoObtained failed");
            return;
        }
        if (forgeInfoObtained == false)
        {
            //Debug.Log("forgeInfoObtained failed");
            return;
        }
        if (preparationHallInfoObtained == false)
        {
            //Debug.Log("preparationHallInfoObtained failed");
            return;
        }
        if (heroInfosObtained == false)
        {
            //Debug.Log("heroInfosObtained failed");
            return;
        }
        if (assignedUnitsObtained == false)
        {
            //Debug.Log("assignedUnitsObtained failed");
            return;
        }
        if (missionsObtained == false)
        {
            //Debug.Log("missionsObtained failed");
            return;
        }
        if (buildingUnderUpgradeObtained == false)
        {
            //Debug.Log("buildingUnderUpgradeObtained failed");
            return;
        }
        if (stoneUnderUpgradeObtained == false)
        {
            //Debug.Log("stoneUnderUpgradeObtained failed");
            return;
        }
        if (intervalRewardRefreshTimeObtained == false)
        {
            //Debug.Log("intervalRewardRefreshTimeObtained failed");
            return;
        }
        if (siegeObtained == false)
        {
            //Debug.Log("siegeObtained failed");
            return;
        }
        if (clanInfoObtained == false)
        {
            //Debug.Log("siegeObtained failed");
            return;
        }
        if (playerClanInfoObtained == false)
        {
            //Debug.Log("siegeObtained failed");
            return;
        }
        if (clanResourcesObtained == false)
        {
            //Debug.Log("siegeObtained failed");
            return;
        }

        if (allObtained == false)
        {
            allObtained = true;
            CancelInvoke("TimeOutCheck");
            InformationsReceived();
        }
        else if (allObtained == true)
        {
            if (isSiege)
            {
                // this happens if the player needs to be update upon sieged
                SiegeReceived();
            }
        }
    }
    // Reset the values back to false in the case where Player Logs out
    public void ResetInfoChecks()
    {
        //clanInfoObtained = false;                     // uncomment when clan implemented
        //clanResourcesObtained = false;            // uncomment when clan implemented
        //playerClanInfoObtained = false;           // uncomment when clan implemented
        battleCountObtained = false;
        playerDataObtained = false;
        playerDetailsObtained = false;
        playerExpenablesObtained = false;
        battleLogsObtained = false;

        inventoryChestsObtained = false;
        inventoryTimersObtained = false;
        inventoryEnergyPacksObtained = false;
        inventoryCardPacksObtained = false;
        inventoryStonesObtained = false;

        barracksInfoObtained = false;
        minesInfoObtained = false;
        townHallInfoObtained = false;
        treasuryInfoObtained = false;
        forgeInfoObtained = false;
        preparationHallInfoObtained = false;

        heroInfosObtained = false;
        assignedUnitsObtained = false;
        missionsObtained = false;

        intervalRewardRefreshTimeObtained = false;
        buildingUnderUpgradeObtained = false;
        stoneUnderUpgradeObtained = false;
        siegeObtained = false;

        playerClanInfoObtained = false;
        clanInfoObtained = false;
        clanResourcesObtained = false;

        allObtained = false;
    }
    // Load data from Sparks and assign it to the Manager Classes
    public void LoadAllData()
    {
        //GetLastLogout();
        GetUnitBattleCount();

        GetStarterPack();

        GetPlayerData(playerId);            //For loading the Player's own data
        GetPlayerDetails(playerId);
        GetPlayerExpendables(playerId);

        GetPlayerBattleLog();

        GetInventoryChestInfo();
        GetInventoryEnergyPacks();
        GetInventoryPacksInfo();
        GetInventoryStones();
        GetInventoryTimers();

        GetStructureTownHall();
        GetStructureTreasury();
        GetStructurePreparationHall();
        GetAllStructureMines();
        GetStructureAllUnitBuildings();
        GetStructureForge();

        GetBuildingUnderUpgrade();
        GetStonesUnderUpgrade();

        GetAllHeroInfos();
        GetSeige("away");

        GetAllAssignedUnits(playerId);
        GetAllMissions();
        GetIntervalReward();
        //		GetMail ();
    }
    // Same as the method above, but this one is to save
    public void StoreAllData()
    {
        // if this is not a new player, and all the necessary data was successfully pulled
        if (newPlayer == false && allObtained == false)
            return;
        SaveUnitBattleCount();
        SavePlayerData(playerId);
        SavePlayerDetails();
        SavePlayerExpendables(playerId);
        SavePlayerBattleLog();

        SaveInventoryChestInfo();
        SaveInventoryEnergyPacks();
        SaveInventoryPacksInfo();
        SaveInventoryStones();
        SaveInventoryTimers();

        SaveStructureTownHall();
        SaveStructureTreasury();
        SaveStructurePreparationHall();
        SaveAllStructureMines();
        SaveStructureAllUnitBuildings();
        SaveStructureForge();

        SaveAllHeroInfos();

        SaveAllAssignedUnits(playerId);

        SaveStonesUnderUpgrade();
        SaveAllMissions();
        SaveBuildingUnderUpgrade();

        SaveIntervalReward(false);
        //SaveMail ();
    }
    public void StorePackRewards()
    {
        SavePlayerData(playerId);
        SavePlayerExpendables(playerId);

        SaveInventoryEnergyPacks();
        SaveInventoryStones();
        SaveInventoryTimers();

        SaveAllHeroInfos();
    }

    // ====================================================== \\



    // ================ LIVE EVENTS AREA ================= \\
    public void RetrieveCurrentEvents()
    {
        new LogEventRequest()
       .SetEventKey("getEvent")
       .Send((response) =>
       {
           if (!response.HasErrors)
           {
               GSData scriptData = response.ScriptData.GetGSData("Event");

               int eventSeason = scriptData.GetInt("eventWeek").GetValueOrDefault(-404);
               int eventType = scriptData.GetInt("eventType").GetValueOrDefault(-404);
               string eventActivity = scriptData.GetString("eventActivity");
               int eventDay = scriptData.GetInt("eventDay").GetValueOrDefault(-404);
               string eventLiveStatus = scriptData.GetString("eventLiveStatus");

               RetrieveExcaliburHolder();
               EventHolderManager.Instance.CurrentEventsRetrieved(eventType, eventSeason, eventActivity, eventDay, eventLiveStatus);
           }
           else
               Debug.Log(response.Errors.JSON);
       }
       );
    }

    public void RetrieveExcaliburHolder()
    {
        new LogEventRequest()
        .SetEventKey("getXcaliburHolder")
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                GSData scriptData = response.ScriptData;

                if (scriptData != null)
                {
                    // this event means there is at least 1 event with an Xcalibur holder

                    // so we'll clear of the List
                    EventHolderManager.Instance.ClearOldList();

                    for (int x = 0; x < 10; x++)
                    {
                        if (scriptData.GetGSData("XcaliburHistory " + x) != null)
                        {
                            GSData historyObj = scriptData.GetGSData("XcaliburHistory " + x);

                            string clan = historyObj.GetString("clanID");
                            int objNo = historyObj.GetInt("objNo").GetValueOrDefault(-100);
                            int points = historyObj.GetInt("points").GetValueOrDefault(-100);
                            string isCanceled = "false";
                            if (historyObj.GetString("isCanceled") != null)
                                isCanceled = historyObj.GetString("isCanceled");

                            EventHolderManager.Instance.AddToXcaliburHistoryList(clan, objNo, points, isCanceled);
                        }
                        else
                            break;
                    }

                    // Update the display here
                    EventHolderManager.Instance.UpdateLastXcaliburHolder();
                }
                else
                {
                    EventHolderManager.Instance.NoPreviousHolder();
                }
            }
            else
                Debug.Log(response.Errors.JSON);
        });
    }

    // ==== Event contributions ==== \\
    public void RetrieveEventContributionPoints()
    {
        new LogEventRequest()
        .SetEventKey("getEventProgression")
        .SetEventAttribute("season", EventHolderManager.currentEventSeason)
        .SetEventAttribute("eventType", EventHolderManager.currentActiveEvent)
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                GSData scriptData = response.ScriptData;
                GSData eventProgress = scriptData.GetGSData("EventProgress");
                int progress = eventProgress.GetInt("progress").GetValueOrDefault(-404);
                if (progress <= -1)
                {
                    SaveEventProgress();
                }
                else
                {
                    string clanID = eventProgress.GetString("clanID");
                    string[] rewards = eventProgress.GetString("rewards").Split(',');
                    string[] clanRewards = eventProgress.GetString("clanRewards").Split(',');
                    EventHolderManager.Instance.PlayersProgressionRetrieved(progress, clanID, rewards, clanRewards);
                }
            }
            else if (response.HasErrors)
            {
                Debug.Log(response.Errors.JSON);
            }
        });
    }
    public void RetrieveClanEventContributionPoints()
    {
        new LogEventRequest()
        .SetEventKey("getClanEventProgression")
        .SetEventAttribute("season", EventHolderManager.currentEventSeason)
        .SetEventAttribute("eventType", EventHolderManager.currentActiveEvent)
        .SetEventAttribute("clanID", Clan.clanID)
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                GSData eventProgress = response.ScriptData.GetGSData("EventProgress");
                int progress = eventProgress.GetInt("progress").GetValueOrDefault(-404);
                if (progress <= -1)
                    SaveClanEventProgress();
                else
                    EventHolderManager.Instance.ClanProgressRetrieved(progress);
            }
            else if (response.HasErrors)
            {
                if (response.Errors.GetString("ERROR").ToLower().Contains("does not exist"))
                {
                    EventHolderManager.Instance.ClanProgressRetrieved(0);
                    if (EventHolderManager.currentEventIsActive == true)
                        SaveClanEventProgress();
                }
                else
                    Debug.Log(response.Errors.JSON);
            }
        });
    }

    public void SaveEventProgress()
    {
        new LogEventRequest()
        .SetEventKey("updateEventProgression")
        .SetEventAttribute("eventType", EventHolderManager.currentActiveEvent)
        .SetEventAttribute("eventSeason", EventHolderManager.currentEventSeason)
        .SetEventAttribute("rewardsObtained", EventHolderManager.myObtainedRewards.RedemptionToBeSaved())
        .SetEventAttribute("clanRewardsObtained", EventHolderManager.myObtainedClanRewards.RedemptionToBeSaved())
        .SetEventAttribute("progressValue", EventHolderManager.pointsToAdd)
        .SetEventAttribute("clanID", Clan.clanID)
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                GSData eventProgress = response.ScriptData.GetGSData("EventProgress");

                int progress = eventProgress.GetInt("progress").GetValueOrDefault(-404);
                string clanID = eventProgress.GetString("clanID");
                string[] rewards = eventProgress.GetString("rewards").Split(',');
                string[] clanRewards = eventProgress.GetString("clanRewards").Split(',');

                EventHolderManager.Instance.PlayersProgressionRetrieved(progress, clanID, rewards, clanRewards);
            }
            else
                Debug.Log(response.Errors.JSON);
        });
    }
    public void SaveEventProgress(RewardButton rewardButton)
    {
        new LogEventRequest()
        .SetEventKey("updateEventProgression")
        .SetEventAttribute("eventType", EventHolderManager.currentActiveEvent)
        .SetEventAttribute("eventSeason", EventHolderManager.currentEventSeason)
        .SetEventAttribute("rewardsObtained", EventHolderManager.myObtainedRewards.RedemptionToBeSaved())
        .SetEventAttribute("clanRewardsObtained", EventHolderManager.myObtainedClanRewards.RedemptionToBeSaved())
        .SetEventAttribute("progressValue", 0)
        .SetEventAttribute("clanID", Clan.clanID)
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                GSData eventProgress = response.ScriptData.GetGSData("EventProgress");

                int progress = eventProgress.GetInt("progress").GetValueOrDefault(-404);
                string clanID = eventProgress.GetString("clanID");
                string[] rewards = eventProgress.GetString("rewards").Split(',');
                string[] clanRewards = eventProgress.GetString("clanRewards").Split(',');

                EventHolderManager.Instance.PlayersProgressionRetrieved(progress, clanID, rewards, clanRewards);

                rewardButton.DisplayClaimedReward();
            }
            else
            {
                Debug.Log(response.Errors.JSON);
                EventHolderManager.Instance.RedemptionOfRewardFailure(rewardButton, "Redemption failed.");
            }
        });
    }

    public void SaveClanEventProgress()
    {
        new LogEventRequest()
        .SetEventKey("updateClanEventProgress")
        .SetEventAttribute("eventType", EventHolderManager.currentActiveEvent)
        .SetEventAttribute("season", EventHolderManager.currentEventSeason)
        .SetEventAttribute("progressValue", EventHolderManager.pointsToAdd)
        .SetEventAttribute("clanID", Clan.clanID)
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                GSData eventProgress = response.ScriptData.GetGSData("ClanEventProgress");
                if (eventProgress != null)
                {
                    int progress = eventProgress.GetInt("progress").GetValueOrDefault(-404);
                    EventHolderManager.Instance.ClanProgressRetrieved(progress);
                }
            }
            else
                Debug.Log(response.Errors.JSON);
        });
    }
    // ==== Event contributions end ==== \\


    // ==== TOP CLANS ISSUES ==== \\
    public void GetTopClans(int numberToGet, LeaderBoard lb)
    {
        new LogEventRequest()
        .SetEventKey("getTopClans")
        .SetEventAttribute("nValue", numberToGet)
        .SetEventAttribute("eventType", EventHolderManager.currentActiveEvent + " " + EventHolderManager.currentEventSeason)
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                int r = 1;
                foreach (GSData nClan in response.ScriptData.GetGSDataList("TopNClans"))
                {
                    int contributionPts_ = nClan.GetInt("progress").GetValueOrDefault(-404);
                    string clanID_ = nClan.GetString("clanID");
                    string clanLeaderID_ = nClan.GetString("leaderID");
                    int memberCount_ = nClan.GetInt("members").GetValueOrDefault(-404);
                    int flagInt_ = nClan.GetInt("flag").GetValueOrDefault(0);

                    lb.InsertInfo(new LeaderBoard.LeaderboardInfo
                    {
                        rankInt_ = r,
                        clanID = clanID_,
                        contributionPoints = contributionPts_,
                        leaderID = clanLeaderID_,
                        clanMemberCount = memberCount_,
                        flagInt = flagInt_
                    });

                    r++;
                }
                lb.ArrangeAccordingToRank();
            }
            else
            {
                Debug.Log(response.Errors.JSON);
            }
        });
    }


    // ==== Xcalibur Event Rewards ==== \\
    string CreateRewardArray(int rewardAmount)
    {
        string rewards = "";
        for (int x = 0; x < rewardAmount; x++)
        {
            if (x == rewardAmount - 1)
            {
                rewards += "0";
                break;
            }
            rewards += "0,";
        }
        return rewards;
    }

    void SavePlayerXcaliPrizeInfo(XcaliburEvent xcalievent, string prizeList)
    {
        new LogEventRequest()
        .SetEventKey("updateXcaliprize")
        .SetEventAttribute("eventType", EventHolderManager.currentActiveEvent)
        .SetEventAttribute("season", EventHolderManager.currentEventSeason)
        .SetEventAttribute("rewards", prizeList)
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                GSData xcaliRewards = response.ScriptData.GetGSData("XcaliRewards");

                string rewards = xcaliRewards.GetString("rewards");

                xcalievent.PlayerExcaliprizeRetrieved(rewards);
            }
            else
                Debug.Log(response.Errors.JSON);
        });
    }
    public void RetrievePlayerXcaliprizeInfo(XcaliburEvent xcalievent)
    {
        new LogEventRequest()
        .SetEventKey("retrieveXcaliprize")
        .SetEventAttribute("eventType", EventHolderManager.currentActiveEvent)
        .SetEventAttribute("season", EventHolderManager.currentEventSeason)
        .Send((response) =>
        {
            // If things go smoothly and the response comes back without Errors
            if (!response.HasErrors)
            {
                // we get the scriptData
                GSData xcaliRewards = response.ScriptData.GetGSData("XcaliRewards");
                string rewardsRetrieved = xcaliRewards.GetString("rewards");
                xcalievent.PlayerExcaliprizeRetrieved(rewardsRetrieved);
            }
            // but if it comes back with an Error of Does not exist
            else if (response.Errors.GetString("ERROR").ToLower().Contains("does not exist"))
            {
                // we will attempt to create an OBJ
                SavePlayerXcaliPrizeInfo(xcalievent, CreateRewardArray(xcalievent.currentXcaliprize.xcalirewards.Length));
            }
            else
                Debug.Log(response.Errors.JSON);
        });
    }

    public void ClaimXcaliPrize(XcaliburEvent xcalievent, string rewardString, XcaliRewardButton xcalirewardButton)
    {
        new LogEventRequest()
        .SetEventKey("updateXcaliprize")
        .SetEventAttribute("eventType", EventHolderManager.currentActiveEvent)
        .SetEventAttribute("season", EventHolderManager.currentEventSeason)
        .SetEventAttribute("rewards", rewardString)
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                GSData xcaliRewards = response.ScriptData.GetGSData("XcaliRewards");

                string rewards = xcaliRewards.GetString("rewards");

                xcalievent.SuccessfulRewardClaim(xcalirewardButton);
                xcalievent.PlayerExcaliprizeRetrieved(rewards);
            }
            else
            {
                xcalirewardButton.SetRewardAvailable(true);
                Debug.Log(response.Errors.JSON);
                xcalievent.ResponsePanel("Failed to update Xcaliprize.");
            }
        });
    }
    // ==== Xcalibur Event Rewards End ==== \\



    // ==== Giveaway Events Area ==== \\

    public void ClaimGiveaway(ReceiveManager rm, Reward reward)
    {
        new LogEventRequest()
        .SetEventKey("claimGiveaway")
        .SetEventAttribute("rewardID", reward.rewardID)
        .Send((response) =>
        {
            if (!response.HasErrors)
                rm.GiveawayReceivedSuccess(reward);
            else
                rm.ResponsePopup(response.Errors.JSON);
        });
    }

    public void ClaimTargetedGift(ReceiveManager rm, Reward reward)
    {
        new LogEventRequest()
        .SetEventKey("collectPlayerSentOBJ")
        .SetEventAttribute("objID", reward.rewardID)
        .Send((response) =>
        {
            if (response.HasErrors)
            {
                rm.ResponsePopup(response.Errors.JSON);
            }
            else
                reward.ClaimThisTargetedGift();
        });
    }

    // ==== Giveaway Events Area ==== \\



    // ================ LIVE EVENTS AREA END ================= \\



    // ================ CLANS INFO RETRIEVAL ============== \\
    public void GetMyTeams(bool ownTeam, List<string> teamTypes, string requestType)
    {
        new GetMyTeamsRequest()
        .SetOwnedOnly(ownTeam)
        .SetTeamTypes(teamTypes)
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                int u = 0;

                foreach (var item in response.Teams)
                {
                    //fill in the Player's CLAN info
                    Clan.clanID = item.TeamName;
                    Clan.leaderID = item.Owner.Id;
                    Clan.leaderName = item.Owner.DisplayName;
                    foreach (var memberInfo in item.Members)
                    {
                        Clan.clanMembersID.Add(memberInfo.Id);
                        Clan.clanMembersName.Add(memberInfo.DisplayName);
                    }
                    u++;
                }

                // if  u > 0, it means the player is in a clan
                if (u > 0)
                {
                    UIManager.Instance.SetClanChatToggle(true);
                    ClanManager.Instance.ClanInfoObtained(requestType);
                    GetClanChatHistory();
                }
                else
                {
                    ClanManager.Instance.ClanPanelToggle(false);
                    clanInfoObtained = true;
                    playerClanInfoObtained = true;
                    clanResourcesObtained = true;
                    InfoReceivedCheck();
                }
            }
            else
                Debug.Log(response.Errors.JSON);
        });
    }

    public void AcknowledgeRemovalFromClan(ClanManager clanManager)
    {
        new LogEventRequest()
        .SetEventKey("acknowledgeClanDrop")
        .Send(response =>
        {
            if (response.HasErrors)
            {
                clanManager.InfoPopup("Failed to acknowledge removal " + response.Errors.JSON);
            }
        });
    }

    // ==== Clan creation events ==== \\
    public void CreateClanRequest()
    {
        new CreateTeamRequest()
        .SetTeamId(ClanManager.Instance.clanNameInput.text)
        .SetTeamName(ClanManager.Instance.clanNameInput.text)
        .SetTeamType("CLAN")
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                var owner = response.Owner;
                GSData scriptData = response.ScriptData;

                // Store all this stuffs in the Player S
                string clanID = response.TeamId;
                string clanName = response.TeamName;

                Clan.clanID = clanID;
                ClanManager.Instance.ToggleCreateClan(false);

                SavePlayerDetails();

                CreateClanInfo(clanID);
            }
            else
                ClanManager.Instance.InfoPopup(CreateClanErrorMessage(response.Errors.JSON));
        });
    }
    string CreateClanErrorMessage(string errorMessage)
    {
        if (errorMessage.ToLower().Contains("max_owned_reached"))
            return "You are already a Clan Leader.";
        if (errorMessage.ToLower().Contains("max_teams_reached"))
            return "You are already in a Clan.";
        return "A Clan with that name already exists.";
    }

    public void CreateClanInfo(string clanID)
    {
        new LogEventRequest()
            .SetEventKey("saveClanInfo")
            .SetEventAttribute("clanID", clanID)
            .SetEventAttribute("clanFlag", Clan.clanFlag)
            .SetEventAttribute("clanNotice", "")
            .SetEventAttribute("clanMemberLimit", ClanManager.Instance.clanMaxMemberCount)
            .SetEventAttribute("clanWarsCount", 0)
            .SetEventAttribute("clanWarsVictories", 0)
            // ADD THE FLAG IMAGE HERE
            .Send((response) =>
            {
                if (!response.HasErrors)
                    CreateClanResource(clanID);
                else
                    ClanManager.Instance.InfoPopup("Error in saving Clan info.");
            }
            );
    }
    public void CreateClanResource(string clanID)
    {
        new LogEventRequest()
        .SetEventKey("saveClanResources")
        .SetEventAttribute("clanID", clanID)
        .SetEventAttribute("clanAvailableGold", 0)
        .SetEventAttribute("clanAvailableGoldLimit", 10000000)
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                CreateClanPoints(clanID);
            }
            else
            {
                Debug.Log(response.Errors.JSON);
                ClanManager.Instance.InfoPopup("Error in saving Clan resources.");
            }
        });
    }
    public void CreateClanPoints(string clanID)
    {
        new LogEventRequest()
        .SetEventKey("saveClanPoints")
        .SetEventAttribute("clanID", clanID)
        .SetEventAttribute("clanPoints", 0)
        .Send((response) =>
        {
            if (!response.HasErrors)
                CreatePlayerClanInfo(playerId, "create");
            else
            {
                Debug.Log(response.Errors.JSON);
                ClanManager.Instance.InfoPopup("Error in saving Clan Points.");
            }
        });
    }
    public void CreateClanWars(string clanID)
    {
        new LogEventRequest()
        .SetEventKey("saveClanWars")
        .SetEventAttribute("clanID", clanID)
        .SetEventAttribute("clanWars", 0)
        .SetEventAttribute("clanWarsVictories", 0)
        .Send((response) =>
        {
            if (!response.HasErrors)
                CreatePlayerClanInfo(playerId, "create");
            else
            {
                Debug.Log(response.Errors.JSON);
                ClanManager.Instance.InfoPopup("Error in saving Clan Wars.");
            }
        });
    }
    public void CreatePlayerClanInfo(string playerID, string createType)
    {
        int rankInt = -100;

        // == Create Rank Here == \\
        if (createType == "join") rankInt = 3;
        else if (createType == "create") rankInt = 0;

        new LogEventRequest()
        .SetEventKey("savePlayerClanInfo")          // Pass the event name to Sparks server here
        .SetEventAttribute("playerID", playerID)   // List all the attributes
        .SetEventAttribute("contributionPoints", 0)
        .SetEventAttribute("rank", rankInt)
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                GetMyTeams(false, new List<string>(1) { "CLAN" }, createType);
                //SaveClanEventProgress();
            }
            else
            {
                Debug.Log(response.Errors.JSON);
                ClanManager.Instance.InfoPopup("Error in saving Clan Info.");
            }
        });
    }

    // ==== STANDALONE CREATIONS ==== \\
    public void CreateNewClanPointsOBJ(string clanID)
    {
        new LogEventRequest()
        .SetEventKey("saveClanPoints")
        .SetEventAttribute("clanID", clanID)
        .SetEventAttribute("clanPoints", 0)
        .Send((response) =>
        {
            if (!response.HasErrors)
                Debug.Log("Created ClanPoints OBJ for Clan");
            else
            {
                Debug.Log(response.Errors.JSON);
                ClanManager.Instance.InfoPopup("Error in saving Clan Points.");
            }
        });
    }
    public void CreateNewClanWarsOBJ(string clanID)
    {
        new LogEventRequest()
        .SetEventKey("saveClanWarCount")
        .SetEventAttribute("clanID", clanID)
        .SetEventAttribute("clanWarCount", 0)
        .SetEventAttribute("clanPoints", 0)
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
            }
            else
            {
                Debug.Log(response.Errors.JSON);
                ClanManager.Instance.InfoPopup("Error in saving Clan Wars.");
            }
        });
    }

    // ==== PLAYER ACTIONS ==== \\
    public void JoinClanRequest(Clan clan, string clanID, string leaderID)
    {
        new JoinTeamRequest()
        .SetTeamId(clanID)
        .SetOwnerId(leaderID)
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                // IF the player successfully joins the CLAN
                clan.CreateClanInfoAsMember();
                CreatePlayerClanInfo(playerId, "join");
            }
            else
                ClanManager.Instance.InfoPopup(JoinTeamResponseError(response.Errors.JSON));
        });
    }
    string JoinTeamResponseError(string errorNote)
    {
        if (errorNote.ToLower().Contains("invalid"))
            return "Clan does not exist";
        if (errorNote.ToLower().Contains("already_joined"))
            return "You are already part of the Clan";
        if (errorNote.ToLower().Contains("max_members_reached"))
            return "Clan members count already at limit.";
        return "";
    }
    public void RemoveFromClan(string clanID, string memberID, string memberName, Clan clan)
    {
        new LogEventRequest()
        .SetEventKey("removeFromClan")
        .SetEventAttribute("clanID", clanID)
        .SetEventAttribute("memberID", memberID)
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                ClanManager.Instance.InfoPopup("Member successfully removed");
                clan.CompleteRemoval(memberID);
            }
            else
            {
                Debug.Log(response.Errors.JSON);
                ClanManager.Instance.InfoPopup("Failed to remove member.");
            }
        });
    }
    public void LeaveClanRequest(string clanID, string leaderID, Clan playerClan)
    {
        new LeaveTeamRequest()
        .SetTeamId(clanID)
        .SetOwnerId(leaderID)
        .SetMaxResponseTimeInMillis(1000)
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                ClanManager.Instance.InfoPopup("Left " + clanID);
                UIManager.Instance.SetClanChatToggle(false);
                ClanManager.Instance.ToggleLeaveClanButton(false);
                playerClan.LeftClan();
            }
            else
            {
                Debug.Log(response.Errors.JSON);
                ClanManager.Instance.InfoPopup("Failed to remove member.");
            }
        });
    }
    public void DropClanRequest(string clanID, string leaderID, Clan playerClan)
    {
        new DropTeamRequest()
        .SetTeamId(clanID)
        .SetOwnerId(leaderID)
        .SetTeamType("CLAN")
        .SetMaxResponseTimeInMillis(1000)
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                ClanManager.Instance.InfoPopup("Left " + clanID);
                ClanManager.Instance.SetPopupInteractable(true);    // allow for popup to close
                ClanManager.Instance.ToggleLeaveClanButton(false);
                UIManager.Instance.SetClanChatToggle(false);          // hide clan chat
                playerClan.LeftClan();
            }
            else
            {
                Debug.Log(response.Errors.JSON);
                ClanManager.Instance.InfoPopup("Failed to abolish Clan.");
            }
        });
    }
    string LeaveTeamErrorMessage(string errorNote)
    {
        if (errorNote.ToLower().Contains("invalid"))
            return "Clan cound not be found.";
        if (errorNote.ToLower().Contains("not_member"))
            return "You are not part of the Clan.";
        if (errorNote.ToLower().Contains("not_unique"))
            return "The Leader and Clan name combination isn't unique.";
        return "";
    }


    // ==== Leaders and Officers only events ==== \\
    public void PromoteClanMember(string memberID, int contributionPts, int memberRank, Clan clan)
    {
        new LogEventRequest()
        .SetEventKey("savePlayerClanInfo")
        .SetEventAttribute("playerID", memberID)   // List all the attributes
        .SetEventAttribute("contributionPoints", contributionPts)
        .SetEventAttribute("rank", memberRank)
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                SendPromotionNotification(clan);
                clan.CompletePromotion(memberID);
            }
            else
            {
                Debug.Log(response.Errors.JSON);
                ClanManager.Instance.InfoPopup("Failed to promote member.");
            }
        });
    }
    public void DemoteClanMember(string memberID, int contributionPts, int memberRank, Clan clan)
    {
        new LogEventRequest()
       .SetEventKey("savePlayerClanInfo")
       .SetEventAttribute("playerID", memberID)   // List all the attributes
       .SetEventAttribute("contributionPoints", contributionPts)
       .SetEventAttribute("rank", memberRank)
       .Send((response) =>
       {
           if (!response.HasErrors)
           {
               SendDemotionNotification(clan);
               clan.CompleteDemotion(memberID);
           }
           else
           {
               Debug.Log(response.Errors.JSON);
               ClanManager.Instance.InfoPopup("Failed to promote member.");
           }
       });
    }
    public void TransferLeadership(string memberToBePromotedID, Clan clan)
    {

        new LogEventRequest()
       .SetEventKey("transferLeadership")
       .SetEventAttribute("newLeaderID", memberToBePromotedID)   // List all the attributes
       .SetEventAttribute("clanID", Clan.clanID)
       .Send((response) =>
       {
           if (!response.HasErrors)
           {
               GSData scriptData = response.ScriptData;

               string newLeaderID = scriptData.GetString("NewLeader");

               SendClanLeadershipTransferNotification(newLeaderID, clan);
           }
           else
           {
               Debug.Log(response.Errors.JSON);
               ClanManager.Instance.InfoPopup("Failed to promote member.");
           }
       });
    }

    public void SaveClanMessage(string clanMessage, Clan clan)
    {
        // This will only be accessible by higher ranks
        new LogEventRequest()
        .SetEventKey("saveClanInfo")
        .SetEventAttribute("clanID", Clan.clanID)
        .SetEventAttribute("clanFlag", Clan.clanFlag)
        .SetEventAttribute("clanNotice", clanMessage.Trim())
        .SetEventAttribute("clanMemberLimit", ClanManager.Instance.clanMaxMemberCount)
        .SetEventAttribute("clanWarsCount", Clan.clanWarsCount)
        .SetEventAttribute("clanWarsVictories", Clan.clanWarsVictories)
        .Send((response) =>
        {
            if (response.HasErrors)
                ClanManager.Instance.InfoPopup(string.Format("Failed to update Clan info. {0}", response.Errors.JSON));
            else
                SendClanMessageUpdateNotification(clan);
        });
    }

    public void UpdateClanFlag(Clan clan)
    {
        new LogEventRequest()
        .SetEventKey("saveClanInfo")
        .SetEventAttribute("clanID", Clan.clanID)
        .SetEventAttribute("clanFlag", clan.tempFlagNo)
        .SetEventAttribute("clanNotice", Clan.clanNotice)
        .SetEventAttribute("clanMemberLimit", ClanManager.Instance.clanMaxMemberCount)
        .SetEventAttribute("clanWarsCount", Clan.clanWarsCount)
        .SetEventAttribute("clanWarsVictories", Clan.clanWarsVictories)
        .Send((response) =>
        {
            if (!response.HasErrors)
                SendFlagChangeNotification(clan.tempFlagNo);
            else
            {
                Debug.Log(response.Errors.JSON);
                ClanManager.Instance.InfoPopup("Failed to update Clan info.");
            }
        });
    }

    public void DeclareClanWar(TargetClan targetClan, Clan clan)
    {
        new LogEventRequest()
        .SetEventKey("createClanWars")
        .SetEventAttribute("clanID", Clan.clanID)
        .SetEventAttribute("targetclanID", targetClan.clanName)
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                // Create the OBJ in the pending wars
                clan.InsertChallengedClanInfo(targetClan);
                clan.WarConfirmationPopupToggle(false);
                WarDeclarationNotification(targetClan, clan);
            }
            else
            {
                Debug.Log(response.Errors.JSON);
                ClanManager.Instance.InfoPopup("Failed to declare War.");
            }
        });
    }

    // Only pulls guild name and owner name 
    public void ClanListRequest()
    {
        new ListTeamsRequest()
        .SetEntryCount(ClanManager.Instance.clanToLoadCount)
        .SetOffset(ClanManager.Instance.clanToLoadOffset)
        .SetTeamTypeFilter("CLAN")
        .SetTeamNameFilter(ClanManager.Instance.clanNameFilter.text)
        .Send((response) =>
        {
            if (response.HasErrors)
                Debug.Log(response.Errors.JSON);
            else if (!response.HasErrors)
            {
                GSData scriptData = response.ScriptData;
                int i = 0;
                foreach (var item in response.Teams)
                {
                    ClanManager.Instance.loadedClanList[i].clanName = item.TeamName;
                    ClanManager.Instance.loadedClanList[i].clanLeaderID = item.Owner.Id;
                    ClanManager.Instance.loadedClanList[i].clanLeaderName = item.Owner.DisplayName;
                    GetClanInfo(item.TeamName, i);
                    i++;
                }
                ClanManager.Instance.ClanListObtained(i);
            }
        });
    }
    public void ClanMemberCountRequest(string clanID, int loadedClanInt)
    {
        new LogEventRequest()
           .SetEventKey("getTeamCount")
           .SetEventAttribute("clanID", clanID)
           .Send((response) =>
           {
               if (response.HasErrors)
                   Debug.Log(response.Errors.JSON);
               else if (!response.HasErrors)
               {
                   ClanManager.Instance.loadedClanList[loadedClanInt].clanMembers = response.ScriptData.GetInt("teamCount").GetValueOrDefault(-404);
                   //Inform clan manager that the member count has returned   
                   ClanManager.Instance.ClanMemeberCountObtained();
               }
           });
    }
    void GetClanInfo(string clanID, int clanQueue)
    {
        new LogEventRequest()
        .SetEventKey("getClanInfo")          // Pass the event name to Sparks server here
        .SetEventAttribute("clanID", clanID)   // List all the attributes
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                GSData data = response.ScriptData;   // The JSON file

                ClanManager.Instance.loadedClanList[clanQueue].clanFlagID = data.GetGSData(clanID).GetInt("clanFlag").GetValueOrDefault(0);   // Get the Script data that is saved using the persistor name.
            }
            else
            {
                Debug.Log(response.Errors.JSON);
                ClanManager.Instance.InfoPopup("Failed to retrieve Clan info.");
            }
        });
    }

    public void GetClanInfo(string clanID, Clan clan)
    {
        new LogEventRequest()
        .SetEventKey("getClanInfo")          // Pass the event name to Sparks server here
        .SetEventAttribute("clanID", clanID)   // List all the attributes
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                GSData data = response.ScriptData;   // The JSON file

                Clan.clanFlag = data.GetGSData(clanID).GetInt("clanFlag").GetValueOrDefault(0);   // Get the Script data that is saved using the persistor name.
                Clan.clanNotice = data.GetGSData(clanID).GetString("clanNotice");
                Clan.clanMemberLimit = data.GetGSData(clanID).GetInt("clanMemberLimit").GetValueOrDefault(-404);
                Clan.clanWarsCount = data.GetGSData(clanID).GetInt("clanWarsCount").GetValueOrDefault(-404);
                Clan.clanWarsVictories = data.GetGSData(clanID).GetInt("clanWarsVictories").GetValueOrDefault(-404);
                clan.SecondaryInfoObtained();
                clanInfoObtained = true;
                InfoReceivedCheck();
            }
            else if (response.Errors.GetString("ERROR").Contains("No results found"))
            {
                clanInfoObtained = true;
                InfoReceivedCheck();
            }
            else
            {
                Debug.Log(response.Errors.JSON);
                ClanManager.Instance.InfoPopup("Failed to retrieve Clan info.");
            }
        });
    }
    public void GetClanMessage(Clan clan)
    {
        new LogEventRequest()
       .SetEventKey("getClanInfo")          // Pass the event name to Sparks server here
       .SetEventAttribute("clanID", Clan.clanID)   // List all the attributes
       .Send((response) =>
       {
           if (!response.HasErrors)
           {
               GSData data = response.ScriptData;   // The JSON file
               Clan.clanNotice = data.GetGSData(Clan.clanID).GetString("clanNotice");

               clan.SecondaryInfoObtained();
           }
           else
               ClanManager.Instance.InfoPopup("Fail to retrieve Clan Message");
       });
    }
    public void GetClanPoints(string clanID, Clan clan)
    {
        new LogEventRequest()
        .SetEventKey("getClanPoints")
        .SetEventAttribute("clanID", clanID)
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                GSData data = response.ScriptData;

                Clan.clanPoints = data.GetGSData("ClanPoints").GetInt("clanPoints").GetValueOrDefault(-404);
                clan.ClanPointsObtained();
            }
            else if (response.Errors.GetString("ERROR").Contains("No results found"))
            {
                CreateNewClanPointsOBJ(clanID);
                Clan.clanPoints = 0;
                clan.ClanPointsObtained();
            }
            else
            {
                Debug.Log(response.Errors.JSON);
                ClanManager.Instance.InfoPopup("Failed to retrieve Clan info.");
            }
        });
    }

    public void UpdateClanPoints(string clanID, int value)
    {
        new LogEventRequest()
        .SetEventKey("saveClanPoints")
        .SetEventAttribute("clanID", clanID)
        .SetEventAttribute("clanPoints", value)
        .SetDurable(true)
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                Clan.clanPoints += value;
            }
            else
            {
                Debug.Log(response.Errors.JSON);
                ClanManager.Instance.InfoPopup("Failed to update Clan info.");
            }
        });
    }

    public void SaveClanResources(int valueDifference, int updateValue, int saveType, Clan clan)
    {
        new LogEventRequest()
        .SetEventKey("saveClanResources")
        .SetEventAttribute("clanID", Clan.clanID)
        .SetEventAttribute("clanAvailableGold", updateValue)
        .SetEventAttribute("clanAvailableGoldLimit", Clan.clanAvailableGoldLimit)
        .SetDurable(true)
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                int newGold = response.ScriptData.GetInt("GOLD").GetValueOrDefault(-404);
                int newGoldLimit = response.ScriptData.GetInt("GOLDLIMIT").GetValueOrDefault(-404);

                clan.UpdateDone(newGold, newGoldLimit);

                if (saveType == 0)
                {
                    // IF any player stores GOLD in the CLAN treasury
                    clan.ToggleStoreGoldPopup(false);
                    DepositGoldNotification(valueDifference, clan);
                }
                else if (saveType == 1)
                {
                    clan.ToggleSendGoldPopup(false);
                    SendGoldSentNotification(valueDifference, clan, "memberSend");
                }
                else if (saveType == 2)
                {
                    clan.ToggleWithdrawGoldPopup(false);
                    SendGoldSentNotification(valueDifference, clan, "withdraw");
                }
            }
            else
            {
                clan.ToggleStoreGoldPopup(false);
                ClanManager.Instance.InfoPopup(response.Errors.JSON);
            }
        });
    }
    public void GetClanResources(string clanID, Clan clan)
    {
        new LogEventRequest()
        .SetEventKey("getClanResources")          // Pass the event name to Sparks server here
        .SetEventAttribute("clanID", clanID)   // List all the attributes
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                GSData data = response.ScriptData;   // The JSON file

                Clan.clanAvailableGold = data.GetGSData(Clan.clanID).GetInt("clanAvailableGold").GetValueOrDefault(-404);
                Clan.clanAvailableGoldLimit = data.GetGSData(Clan.clanID).GetInt("clanAvailableGoldLimit").GetValueOrDefault(-404);
                clan.ClanResourcesObtained();
                clanResourcesObtained = true;
                InfoReceivedCheck();
            }
            else if (response.Errors.GetString("ERROR").Contains("No results found"))
            {
                clanResourcesObtained = true;
                InfoReceivedCheck();
            }
            else
            {
                Debug.Log(response.Errors.JSON);
                ClanManager.Instance.InfoPopup("Failed to retrieve Clan info");
            }
        });
    }
    public void CheckClanResource(string clanID, int changeType, Clan clan)
    {
        new LogEventRequest()
        .SetEventKey("getClanResources")          // Pass the event name to Sparks server here
        .SetEventAttribute("clanID", clanID)   // List all the attributes
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                GSData data = response.ScriptData;   // The JSON file
                Clan.clanAvailableGold = data.GetGSData(Clan.clanID).GetInt("clanAvailableGold").GetValueOrDefault(-404);
                Clan.clanAvailableGoldLimit = data.GetGSData(Clan.clanID).GetInt("clanAvailableGoldLimit").GetValueOrDefault(-404);
                clan.ChangeClanGoldValue(changeType);
                clanResourcesObtained = true;
                InfoReceivedCheck();
            }
            else if (response.Errors.GetString("ERROR").Contains("No results found"))
            {
                clanResourcesObtained = true;
                InfoReceivedCheck();
            }
            else
            {
                Debug.Log(response.Errors.JSON);
                ClanManager.Instance.InfoPopup("Failed to retrieve Clan info");
            }
        });
    }

    public void UpdatePlayerClanContribution(int pointsToUpdate)
    {
        new LogEventRequest()
       .SetEventKey("updatePlayerClanInfo")          // Pass the event name to Sparks server here
       .SetEventAttribute("progressValue", pointsToUpdate)
       .SetDurable(true)
       .Send((response) =>
       {
           if (!response.HasErrors)
           {
               Clan.contributionPoints = response.ScriptData.GetGSData("playerClanInfo").GetInt("contributionPoints").GetValueOrDefault(0);

               ClanManager.Instance.UpdatingClanMemberInfo(false);

               ClanManager.Instance.playerClanInfo.UpdateClanPoints();
           }
           else
           {
               ClanManager.Instance.InfoPopup("Failed to update contribution points");
               ClanManager.Instance.TogglePopup(true);
           }
       });
    }
    public void SavePlayerClanInfo(string playerID)
    {
        if (Clan.rank == 3)
            Clan.rank = 2;
        new LogEventRequest()
        .SetEventKey("savePlayerClanInfo")          // Pass the event name to Sparks server here
        .SetEventAttribute("playerID", playerID)   // List all the attributes
        .SetEventAttribute("contributionPoints", Clan.contributionPoints)
        .SetEventAttribute("rank", Clan.rank)
        .SetDurable(true)
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                Debug.Log(response.ScriptData.JSON);
            }
            else
            {
                Debug.Log(response.Errors.JSON);
                ClanManager.Instance.InfoPopup("Failed to retrieve Clan info");
            }
        });
    }
    public void GetPlayerClanInfo(string playerID, int clanMemInt, Clan clan)
    {
        new LogEventRequest()
        .SetEventKey("getPlayerClanInfo")          // Pass the event name to Sparks server here
        .SetEventAttribute("playerID", playerID)   // List all the attributes
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                GSData data = response.ScriptData;   // The JSON file

                int clanMemContributionPt = data.GetGSData(playerID).GetInt("contributionPoints").GetValueOrDefault(-404);
                int clanMemRank = (int)data.GetGSData(playerID).GetLong("rank").GetValueOrDefault(-404);
                if (IsAccessingOwnData(playerID))
                {
                    Clan.contributionPoints = data.GetGSData(playerID).GetInt("contributionPoints").GetValueOrDefault(-404);
                    Clan.rank = data.GetGSData(playerID).GetInt("rank").GetValueOrDefault(-404);
                }

                clan.InsertPlayerInfo(clanMemInt, clanMemRank, clanMemContributionPt);
                playerClanInfoObtained = true;
                InfoReceivedCheck();
            }
            else if (response.Errors.GetString("ERROR").Contains("No results found"))
            {
                playerClanInfoObtained = true;
                InfoReceivedCheck();
            }
            else
            {
                Debug.Log(response.Errors.JSON);
            }
        });
    }

    // ==== Clan Wars Necessaries ==== \\
    public void ObtainPotentialClanemies(Clan clan)
    {
        new ListTeamsRequest()
        .SetEntryCount(clan.clanemiesToLoad)    // Adds 1 in the case request returns the Player's own team
        .SetOffset(0)
        .SetTeamTypeFilter("CLAN")
        .SetTeamNameFilter(clan.clanTargetFilter.text)
        .Send((response) =>
        {
            if (response.HasErrors)
            {
                Debug.Log(response.Errors.JSON);
                ClanManager.Instance.InfoPopup("Failed to retrieve Clan info");
            }
            else if (!response.HasErrors)
            {
                GSData scriptData = response.ScriptData;
                int i = 0;
                foreach (var item in response.Teams)
                {
                    if (item.TeamId != Clan.clanID && clan.TargetClanIDCheck(item.TeamId) == false)
                    {
                        clan.targetClanNameList.Add(item.TeamName);
                        clan.targetClanLeadersList.Add(item.Owner.DisplayName);
                    }
                    i++;
                }
                clan.RetrieveClanemiesInfo();
            }
        });
    }
    public void GetClanemyClanInfo(string clanID, int clanQueueInt, Clan clan)
    {
        new LogEventRequest()
        .SetEventKey("getClanInfo")          // Pass the event name to Sparks server here
        .SetEventAttribute("clanID", clanID)   // List all the attributes
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                GSData data = response.ScriptData;   // The JSON file

                if (response.ScriptData == null)
                    print("Empty script data");

                string clanIcon = data.GetGSData(clanID).GetString("clanIcon");   // Get the Script data that is saved using the persistor name.
                int clanWarsCount = data.GetGSData(clanID).GetInt("clanWarsCount").GetValueOrDefault(-404);
                int clanWarsVictories = data.GetGSData(clanID).GetInt("clanWarsVictories").GetValueOrDefault(-404);

                clan.StoreRetrievedClanemiesClanInfo(clanQueueInt, clanIcon, clanWarsCount, clanWarsVictories);
            }
            else if (response.Errors.GetString("ERROR").Contains("No results found"))
            {
                clanInfoObtained = true;
                InfoReceivedCheck();
            }
            else
            {
                Debug.Log(response.Errors.JSON);
                ClanManager.Instance.InfoPopup("Failed to retrieve Clan info");
            }
        });
    }
    public void GetClanemyClanPoints(string clanID, int clanQueueInt, Clan clan)
    {
        new LogEventRequest()
        .SetEventKey("getClanPoints")
        .SetEventAttribute("clanID", clanID)
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                GSData data = response.ScriptData;

                int clanPoints = data.GetGSData(clanID).GetInt("clanPoints").GetValueOrDefault(-404);
                clan.StoreRetrievedClanemiesClanPoints(clanQueueInt, clanPoints);
            }
            else if (response.Errors.GetString("ERROR").Contains("No results found"))
            {
                CreateNewClanPointsOBJ(clanID);
            }
            else
            {
                Debug.Log(response.Errors.JSON);
                ClanManager.Instance.InfoPopup("Failed to retrieve Clan info");
            }
        });
    }
    public void GetClanemyMemebers(string clanID, int clanQueue, Clan clan)
    {
        new GetTeamRequest()
        .SetTeamId(clanID)
        .SetTeamType("CLAN")
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                //Do stuff here
                List<string> membersNameToSend = new List<string>();
                List<string> membersIDToSend = new List<string>();
                foreach (var teams in response.Teams)
                {
                    foreach (var members in teams.Members)
                    {
                        membersNameToSend.Add(members.DisplayName);
                        membersIDToSend.Add(members.Id);
                    }
                }
                clan.StoreRetrievedClanemiesClanMembers(clanQueue, membersNameToSend, membersIDToSend);
            }
            else
            {
                Debug.Log(response.Errors.JSON);
                ClanManager.Instance.InfoPopup("Failed to retrieve Clan info");
            }
        });
    }
    // ==== Clan Wars Necessaries End ==== \\

    // ==== Clan Wars Ongoing ==== \\
    public void GetOnGoingWars(string clanID, Clan clan)
    {
        new LogEventRequest()
        .SetEventKey("getOngoingWars")
        .SetEventAttribute("clanID", clanID)
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                GSData scriptData = response.ScriptData;
                //Send stuff to the Clan class
                if (response.ScriptData == null)
                    Debug.Log("Empty Clan War History");
                else
                {
                    for (int x = 0; x < 50; x++)
                    {
                        if (scriptData.GetGSData("OngoingWar" + x) != null)
                        {
                            List<string> clanMemberParticipation = new List<string>();
                            List<string> targetClanMemberParticipation = new List<string>();

                            GSData newWar = scriptData.GetGSData("OngoingWar" + x);

                            if (newWar.GetString("clanID") == Clan.clanID)
                            {
                                //Add the ENEMY Clan
                                clan.ongoingWarTargetClanID.Add(newWar.GetString("targetClanID"));

                                // Player Clan Participation and Points accumulated
                                clanMemberParticipation = newWar.GetStringList("clanParticipation");

                                // Target Clan Participation and Points accumulated
                                targetClanMemberParticipation = newWar.GetStringList("targClanParticipation");

                                clan.ongoingWarsStatusList.Add(newWar.GetString("status"));

                                clan.InsertParticipationMemberList(clanMemberParticipation, targetClanMemberParticipation);
                            }
                            else if (newWar.GetString("targetClanID") == Clan.clanID)
                            {
                                //Add the ENEMY Clan
                                clan.ongoingWarTargetClanID.Add(newWar.GetString("clanID"));

                                // Player Clan Participation and Points accumulated
                                clanMemberParticipation = newWar.GetStringList("targClanParticipation");

                                // Target Clan Participation and Points accumulated
                                targetClanMemberParticipation = newWar.GetStringList("clanParticipation");

                                clan.ongoingWarsStatusList.Add(newWar.GetString("status"));

                                clan.InsertParticipationMemberList(targetClanMemberParticipation, clanMemberParticipation);
                            }
                        }
                        else
                            break;
                    }
                }
                clan.OngoingClanWarsInfoObtained();
            }
            else
            {
                Debug.Log(response.Errors.JSON);
                ClanManager.Instance.InfoPopup("Failed to retrieve Clan info");
            }
        });
    }
    public void GetOngoingWarsClanMembers(string clanID, int clanQueue, Clan clan)
    {
        new GetTeamRequest()
            .SetTeamId(clanID)
            .SetTeamType("CLAN")
            .Send((response) =>
            {
                if (!response.HasErrors)
                {
                    //Do stuff here
                    List<string> membersNameToSend = new List<string>();
                    List<string> membersIDToSend = new List<string>();
                    foreach (var teams in response.Teams)
                    {
                        foreach (var members in teams.Members)
                        {
                            membersNameToSend.Add(members.DisplayName);
                            membersIDToSend.Add(members.Id);
                        }
                        clan.InsertOngoingWarsTargetClanLeader(clanQueue, teams.Owner.Id, teams.Owner.DisplayName);
                    }

                    clan.InsertOngoingWarsTargetClanMembers(clanQueue, membersNameToSend, membersIDToSend);
                }
                else
                {
                    Debug.Log(response.Errors.JSON);
                    ClanManager.Instance.InfoPopup("Failed to retrieve Clan info");
                }
            });
    }
    public void GetClanPointsForOngoingWarClans(string clanID, int clanQueue, Clan clan)
    {
        new LogEventRequest()
       .SetEventKey("getClanPoints")
       .SetEventAttribute("clanID", clanID)
       .Send((response) =>
       {
           if (!response.HasErrors)
           {
               GSData data = response.ScriptData;

               clan.InsertOngoingWarClanPoints(clanQueue, data.GetGSData(clanID).GetInt("clanPoints").GetValueOrDefault(-404));
           }
           else
               Debug.Log(response.Errors.JSON);
       });
    }
    public void GetOngoingWarsClanBattleStats(string clanID, int clanQueue, Clan clan)
    {
        new LogEventRequest()
            .SetEventKey("getClanInfo")          // Pass the event name to Sparks server here
            .SetEventAttribute("clanID", clanID)   // List all the attributes
            .Send((response) =>
            {
                if (!response.HasErrors)
                {
                    GSData data = response.ScriptData;   // The JSON file

                    if (response.ScriptData == null)
                        print("Empty script data");

                    string clanIcon = data.GetGSData(clanID).GetString("clanIcon");   // Get the Script data that is saved using the persistor name.
                    string clanNotice = data.GetGSData(clanID).GetString("clanNotice");
                    int clanWarsCount = data.GetGSData(clanID).GetInt("clanWarsCount").GetValueOrDefault(-404);
                    int clanWarsVictories = data.GetGSData(clanID).GetInt("clanWarsVictories").GetValueOrDefault(-404);

                    clan.InsertOngoingWarClanBattleStats(clanQueue, clanWarsCount, clanWarsVictories);
                }
                else
                    print(response.Errors.JSON);
            });
    }
    public void UpdateParticipation(string targetClanID, Clan clan, string targetClanLeaderID)
    {
        new LogEventRequest()
        .SetEventKey("updateClanWarParticipation")
        .SetEventAttribute("clanID", Clan.clanID)
        .SetEventAttribute("targetClanID", targetClanID)
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                // Inform Clan class about the new participation update
                //clan.MemberParticipationUpdated(playerId, Enemy.clan);
                //Clan members notification
                ClanWarPointsChange(playerId);
            }
        });
    }
    // ==== Clan Wars Ongoing End ==== \\

    // ==== Clan Wars History ==== \\
    public void GetClanWarsHistory(string clanID, Clan clan)
    {
        new LogEventRequest()
        .SetEventKey("getClanWarHistory")
        .SetEventAttribute("clanID", clanID)
        .Send((response) =>
        {
            if (!response.HasErrors)
            {

                GSData scriptData = response.ScriptData;
                if (scriptData != null)
                {

                    for (int x = 0; x < 50; x++)
                    {
                        if (scriptData.GetGSData("warHistory" + x) != null)
                        {
                            List<string> clanMemberParticipation = new List<string>();
                            List<string> targetClanMemberParticipation = new List<string>();

                            GSData newWarHistory = scriptData.GetGSData("warHistory" + x);

                            if (newWarHistory.GetString("clanID") == Clan.clanID)
                            {
                                //Add the ENEMY Clan
                                clan.endedClanWarTargetClanID.Add(newWarHistory.GetString("targetClanID"));
                                // Add the number of target clan members during the war declaration
                                clan.endedClanWarTargetClanMemberCountDuringWar.Add(newWarHistory.GetInt("targetClanMemberCount").GetValueOrDefault(-404));
                                // Add the number of clan members during the war
                                clan.endedClanWarClanMemberCountDuringWar.Add(newWarHistory.GetInt("clanMemberCount").GetValueOrDefault(-404));

                                // Player Clan Participation and Points accumulated
                                clanMemberParticipation = newWarHistory.GetStringList("clanParticipation");

                                // Target Clan Participation and Points accumulated
                                targetClanMemberParticipation = newWarHistory.GetStringList("targClanParticipation");

                                clan.InsertWarHistoryParticipationList(clanMemberParticipation, targetClanMemberParticipation);
                            }
                            else if (newWarHistory.GetString("targetClanID") == Clan.clanID)
                            {
                                //Add the ENEMY Clan
                                clan.endedClanWarTargetClanID.Add(newWarHistory.GetString("clanID"));
                                // Add the number of target clan members during the war declaration
                                clan.endedClanWarClanMemberCountDuringWar.Add(newWarHistory.GetInt("targetClanMemberCount").GetValueOrDefault(-404));
                                // Add the number of clan members during the war
                                clan.endedClanWarTargetClanMemberCountDuringWar.Add(newWarHistory.GetInt("clanMemberCount").GetValueOrDefault(-404));

                                // Player Clan Participation and Points accumulated
                                clanMemberParticipation = newWarHistory.GetStringList("targClanParticipation");

                                // Target Clan Participation and Points accumulated
                                targetClanMemberParticipation = newWarHistory.GetStringList("clanParticipation");

                                clan.InsertWarHistoryParticipationList(targetClanMemberParticipation, clanMemberParticipation);
                            }
                        }
                        else
                            break;
                    }
                }
                clan.ClanWarHistoryInfoObtained();
            }
            else
                Debug.Log(response.Errors.JSON);
        });
    }
    // ==== Clan Wars History End ==== \\

    // ==== Clan happenings notification ==== \\
    void SendFlagChangeNotification(int flagNo)
    {
        new SendTeamChatMessageRequest()
        .SetMessage(" FlagChange " + flagNo)
        .SetTeamId(Clan.clanID)
        .SetTeamType("CLAN")
        .Send((response) =>
        {
            if (!response.HasErrors)
                Debug.Log("Notified");
        });
    }
    void SendClanMessageUpdateNotification(Clan clan)
    {
        new SendTeamChatMessageRequest()
       .SetMessage(" ClanNotificationUpdate ")
       .SetOwnerId(Clan.leaderID)
       .SetTeamId(Clan.clanID)
       .SetTeamType("CLAN")
       .Send((response) =>
       {
           if (!response.HasErrors)
               clan.MessageUpdateSuccess();
       });
    }
    void SendClanLeadershipTransferNotification(string leaderID, Clan clan)
    {
        new SendTeamChatMessageRequest()
       .SetMessage(" ClanLeaderChange " + leaderID)
       .SetOwnerId(leaderID)
       .SetTeamId(Clan.clanID)
       .SetTeamType("CLAN")
       .Send((response) =>
       {
           if (!response.HasErrors)
           {
               Clan.rank = 1;
               ClanManager.Instance.InfoPopup("Successfully transfered Clan ownership.");
               clan.ToggleLeadershipTransferPopup(false);
               clan.TogglePopup(false);
           }
       });
    }

    public void SendJoinClanNotification()
    {
        new SendTeamChatMessageRequest()
            .SetMessage(" MemberJoined " + playerId + " " + Player.playerName)
            .SetTeamId(Clan.clanID)
            .SetTeamType("CLAN")
            .Send((response) =>
            {
                if (!response.HasErrors)
                {
                    ClanManager.Instance.InfoPopup("Successfully join CLAN");
                    SavePlayerDetails();
                }
                else
                    ClanManager.Instance.InfoPopup("Failed to join CLAN" + response.Errors.JSON);
            });
    }
    public void SendPromotionNotification(Clan clan)
    {
        new SendTeamChatMessageRequest()
        .SetMessage(" MemberPromotion " + clan.memberOnDisplay)
        .SetTeamId(Clan.clanID)
        .SetTeamType("CLAN")
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                ClanManager.Instance.InfoPopup(clan.memberOnDisplayName + " has been successfully promoted");
            }
            else
                ClanManager.Instance.InfoPopup("Failed to Promote" + response.Errors.JSON);
        });
    }
    public void SendDemotionNotification(Clan clan)
    {
        new SendTeamChatMessageRequest()
       .SetMessage(" MemberDemotion " + clan.memberOnDisplay)
       .SetTeamId(Clan.clanID)
       .SetTeamType("CLAN")
       .Send((response) =>
       {
           if (!response.HasErrors)
           {
               ClanManager.Instance.InfoPopup(clan.memberOnDisplayName + " has been successfully demoted");
           }
           else
               ClanManager.Instance.InfoPopup("Failed to Demote" + response.Errors.JSON);
       });
    }
    public void SendGoldSentNotification(int valueDifference, Clan clan, string type)
    {
        string newMessage = "";

        if (type == "memberSend")
            newMessage = " GoldSend " + clan.memberOnDisplay + " " + valueDifference + " " + Clan.clanAvailableGold + " ";
        else if (type == "withdraw")
            newMessage = " GoldSend " + playerId + " " + valueDifference + " " + Clan.clanAvailableGold + " ";


        new SendTeamChatMessageRequest()
        .SetMessage(newMessage)
        .SetOwnerId(Clan.leaderID)
        .SetTeamId(Clan.clanID)
        .SetTeamType("CLAN")
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                if (type == "memberSend")
                    ClanManager.Instance.InfoPopup("Gold successfully sent to MEMBER");
                else if (type == "withdraw")
                    ClanManager.Instance.InfoPopup(valueDifference + " Gold successfully withdrawn");
            }
            else
                ClanManager.Instance.InfoPopup("Failed to alter GOLD" + response.Errors.JSON);
        });
    }
    public void DepositGoldNotification(int valueDifference, Clan clan)
    {
        new SendTeamChatMessageRequest()
        .SetMessage(" GoldDeposit " + playerId + " " + valueDifference + " " + Clan.clanAvailableGold)
        .SetOwnerId(Clan.leaderID)
        .SetTeamId(Clan.clanID)
        .SetTeamType("CLAN")
        .Send((response) =>
        {
            if (response.HasErrors)
                ClanManager.Instance.InfoPopup("Failed to Store GOLD" + response.Errors.JSON);
        });
    }
    public void RemoveMemberNotification(string clanID, string memberID, string memberName, string exitType, Clan clan)
    {
        new SendTeamChatMessageRequest()
        .SetMessage(" MemberRemoval " + memberID + " " + memberName)
        .SetOwnerId(Clan.leaderID)
        .SetTeamId(Clan.clanID)
        .SetTeamType("CLAN")
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                if (exitType == "kicked")
                    RemoveFromClan(clanID, memberID, memberName, clan);
                else if (exitType == "leave")
                    LeaveClanRequest(clanID, Clan.leaderID, clan);
            }
            else
                ClanManager.Instance.InfoPopup("Failed to kick member" + response.Errors.JSON);
        });
    }
    // ADD NOTIFICATION WHEN CLAN HAS DECLARED WAR
    public void WarDeclarationNotification(TargetClan targetClan, Clan clan)
    {
        //Only available to Leader and Second in Commands
        new SendTeamChatMessageRequest()
        .SetMessage(" WarDeclared " + targetClan)
        .SetOwnerId(Clan.leaderID)
        .SetTeamId(Clan.clanID)
        .SetTeamType("CLAN")
        .Send((response) =>
        {
            if (!response.HasErrors)
                ClanManager.Instance.InfoPopup(targetClan.clanName + " has been Challenged");
            else
                ClanManager.Instance.InfoPopup("Failed to declare war " + response.Errors.JSON);
        });
    }
    public void NotifyTargetClanOfWarDeclaration(TargetClan targetClan)
    {
        //Only available to Leader and Second in Commands
        new SendTeamChatMessageRequest()
        .SetMessage(" WarDeclared " + targetClan.clan)
        .SetOwnerId(targetClan.leaderID)
        .SetTeamId(targetClan.clanName)
        .SetTeamType("CLAN")
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                Debug.Log("Enemy Clan notified");
            }
            else
                ClanManager.Instance.InfoPopup("Failed to declare war " + response.Errors.JSON);
        });
    }
    // ADD NOTIFICATION WHEN MEMBERS TAKE PARTS IN ANY CLAN WARS AND WINS
    public void ClanWarPointsChange(string playerID)
    {
        // UPDATE MY CLAN ABOUT THE POINT CHANGE
        new SendTeamChatMessageRequest()
            .SetMessage(" ClanWarPointChanged " + playerID + " " + Enemy.clan)
            .SetOwnerId(Clan.leaderID)
            .SetTeamId(Clan.clanID)
           .SetTeamType("CLAN")
           .Send((response) =>
           {
               if (!response.HasErrors)
                   Debug.Log("Clan notified");
               else
                   ClanManager.Instance.InfoPopup("Failed to update clan war point " + response.Errors.JSON);
           });
    }
    // === ******* THIS METHOD NOT USABLE BECAUSE NOT IN THE SAME TEAM ******** === \\
    public void TargetClanWarPointsChange(string playerID, string targetClanLeaderID)
    {
        // UPDATE TARGET CLAN ABOUT THE POINT CHANGE
        new SendTeamChatMessageRequest()
            .SetMessage(" ClanWarPointChanged " + playerID + " " + Clan.clanID)
            .SetOwnerId(targetClanLeaderID)
            .SetTeamId(Enemy.clan)
           .SetTeamType("CLAN")
           .Send((response) =>
           {
               if (!response.HasErrors)
                   Debug.Log("TargetClan Notified");
               else
                   ClanManager.Instance.InfoPopup("Failed to update clan war point " + response.Errors.JSON);
           });
    }
    // ================ CLANS INFO RETRIEVAL END ============== \\


    // ============= CLAN TOURNAMENTS ================\\
    public void RetrieveOngoingTournamentInfo()
    {
        new LogEventRequest()
            .SetEventKey("getTournament")
            .Send((response) =>
            {
                if (!response.HasErrors)
                {
                    GSData scriptData = response.ScriptData;
                    foreach (GSData item in scriptData.GetGSDataList("Tournaments"))
                    {
                        Tournament.Instance.CreateTournamentTemp(
                            item.GetString("tournamentName"),
                            item.GetInt("queueNo").GetValueOrDefault(-404),
                            item.GetLong("startTime").GetValueOrDefault(-404),
                            item.GetLong("endTime").GetValueOrDefault(-404),
                            item.GetString("status"),
                            item.GetInt("season").GetValueOrDefault(-404));
                    }
                    Tournament.Instance.TournamentsInit();
                }
                else
                    Debug.Log(response.Errors);
            });
    }

    public void SaveClanTournamentInfo(string clanID)
    {
    }
    public void RetrieveClanTournament(string clanID)
    {
    }
    // ============= CLAN TOURNAMENTS END ================\\

    // Player Data, Expendables, and AssignedUnits pass through playerID params, because it is also used to load enemy's data. So ID is required to differentiate between the two.
    public void GetSUnitsAmount(SUnitButton sUnitButton)
    {
        new LogEventRequest()
        .SetEventKey("getSUnitAmount")
        .SetEventAttribute("sUnitType", sUnitButton.thisSUnit.ToString())
        .Send((response) =>
        {
            if (response.HasErrors)
                Debug.Log(response.Errors.JSON);
            else
            {
                int unitAmount = response.ScriptData.GetInt(sUnitButton.thisSUnit.ToString()).GetValueOrDefault(0);
                sUnitButton.SUnitValueUpdate(unitAmount);
            }
        });
    }
    public void SaveSUnitsAmount(int rewardTier)
    {
        new LogEventRequest()
           .SetEventKey("saveSUnitAmount")
           .SetEventAttribute("sUnitType", SUnitChallenge.sUnitToAdd.ToString())
           .SetEventAttribute("rewardTier", rewardTier)
           .SetDurable(true)
           .Send((response) =>
           {
               if (response.HasErrors)
                   Debug.Log(response.Errors.JSON);
               else
                   SUnitsAmountUpdated?.Invoke(rewardTier);
           });
    }
    public void UpdateSUnitAmount(int valToAlter, SUnitType.SUnit typeToUpdate)
    {
        new LogEventRequest()
        .SetEventKey("saveSUnitAmount")
        .SetEventAttribute("sUnitType", typeToUpdate.ToString())
        .SetEventAttribute("rewardTier", valToAlter)
        .Send((response) =>
        {
            if (response.HasErrors)
                Debug.Log(response.Errors.JSON);
            else
                Player.UpdateSUnitsAmount(typeToUpdate);
        });
    }
    public void SaveUnitBattleCount()
    {
        new LogEventRequest()
        .SetEventKey("saveUnitBattleCount")
        .SetEventAttribute("casterCount", DataManager.casterBattleCount)
        .SetEventAttribute("meleeCount", DataManager.meleeBattleCount)
        .SetEventAttribute("rangeCount", DataManager.rangeBattleCount)
        .Send((response) =>
        {
            if (response.HasErrors)
                print(response.Errors.JSON);
        }
        );
    }
    public void GetUnitBattleCount()
    {
        // This keyword is to call a custom event
        new LogEventRequest()
            .SetEventKey("getUnitBattleCount")          // Pass the event name to Sparks server here
            .SetEventAttribute("PLAYER_ID", playerId)   // List all the attributes
            .Send((response) =>
           {
               if (!response.HasErrors)
               {
                   GSData data = response.ScriptData;   // The JSON file

                   if (response.ScriptData == null)
                       print("Empty script data");

                   DataManager.casterBattleCount = data.GetGSData(playerId).GetInt("cC").GetValueOrDefault(-404);   // Get the Script data that is saved using the persistor name.
                   DataManager.meleeBattleCount = data.GetGSData(playerId).GetInt("mC").GetValueOrDefault(-404);   // In this case, the persistor is the entry name 'playerId'
                   DataManager.rangeBattleCount = data.GetGSData(playerId).GetInt("rC").GetValueOrDefault(-404);
                   battleCountObtained = true;
                   InfoReceivedCheck();
               }
               else if (response.Errors.GetString("ERROR").Contains("No results found"))
               {
                   DataManager.rangeBattleCount = 0;
                   DataManager.casterBattleCount = 0;
                   DataManager.meleeBattleCount = 0;
                   battleCountObtained = true;
                   InfoReceivedCheck();
               }
               else
                   print(response.Errors.JSON);
           });
    }

    public void GetStarterPack()
    {
        new LogEventRequest()
         .SetEventKey("getStarterPack")
         .Send((response) =>
         {
             if (response.HasErrors)
             {
                 Debug.Log(response.Errors.JSON);
                 Player.starterPackPurchase = 0;
             }
             else
             {
                 GSData sData = response.ScriptData;
                 Player.starterPackPurchase = 1;
             }
         });
    }
    public void SaveStarterPackPurchase()
    {
        new LogEventRequest()
          .SetEventKey("starterPackPurchase")
          .Send((response) =>
          {
              if (response.HasErrors)
                  Debug.Log(response.Errors.JSON);
          });
    }

    public void SavePlayerData(string accessedPlayerID)
    {
        if (IsAccessingOwnData(accessedPlayerID))
        {
            new LogEventRequest()
            .SetEventKey("savePlayerData")
            .SetEventAttribute("energy", (long)DataManager.energy)
            .SetEventAttribute("level", (long)DataManager.castleLv)
            .SetEventAttribute("reputation", (long)DataManager.reputation)
            .SetEventAttribute("shuffleTime", (long)DataManager.shuffleTime * 100)
            .SetEventAttribute("energyRefreshTime", (long)DataManager.energyRefreshTime)
            .SetEventAttribute("accessedID", accessedPlayerID)
            .Send((response) =>
            {
                if (response.HasErrors)
                    print(response.Errors.JSON);
            });
        }
        else
        {
            new LogEventRequest()
            .SetEventKey("savePlayerData")
            .SetEventAttribute("energy", (long)Enemy.Instance.energy)
            .SetEventAttribute("level", (long)Enemy.castleLevel)
            .SetEventAttribute("reputation", (long)Enemy.reputation)
            .SetEventAttribute("shuffleTime", (long)Enemy.shuffleTiming * 100)
            .SetEventAttribute("energyRefreshTime", (long)Enemy.Instance.energyRefreshTime)
            .SetEventAttribute("accessedID", accessedPlayerID)
            .Send((response) =>
            {
                if (response.HasErrors)
                    print(response.Errors.JSON);
            });
        }
    }
    public void GetPlayerData(string accessedPlayerID)
    {
        new LogEventRequest()
            .SetEventKey("getPlayerData")           // Pass the event name to Sparks server here
            .SetEventAttribute("PLAYER_ID", accessedPlayerID)   // List all the attributes
            .Send((response) =>
           {
               if (!response.HasErrors)
               {
                   GSData data = response.ScriptData;   // The JSON file

                   if (response.ScriptData == null)
                       print("Empty script data");

                   int energyN = Convert.ToInt32(data.GetGSData(accessedPlayerID).GetInt("energy").GetValueOrDefault(-404));
                   int energyRFTimeN = Convert.ToInt32(data.GetGSData(accessedPlayerID).GetInt("energyRefreshTime").GetValueOrDefault(-404));
                   int castleLevelN = Convert.ToInt32(data.GetGSData(accessedPlayerID).GetInt("level").GetValueOrDefault(-404)); // In this case, the persistor is the entry name 'playerId'
                   int reputationN = Convert.ToInt32(data.GetGSData(accessedPlayerID).GetInt("reputation").GetValueOrDefault(-404));// Get the Script data that is saved using the persistor name.
                   float shuffletimeN = Convert.ToInt32(data.GetGSData(accessedPlayerID).GetInt("shuffleTime").GetValueOrDefault(-404));// Get the Script data that is saved using the persistor name.

                   if (shuffletimeN > 100)
                       shuffletimeN = shuffletimeN / 100;

                   if (IsAccessingOwnData(accessedPlayerID))
                   {
                       DataManager.energy = energyN;
                       DataManager.energyRefreshTime = energyRFTimeN;
                       DataManager.castleLv = castleLevelN;
                       DataManager.reputation = reputationN;
                       DataManager.shuffleTime = shuffletimeN;
                       playerDataObtained = true;
                       InfoReceivedCheck();
                   }
                   else
                   {
                       Enemy.Instance.energy = energyN;
                       Enemy.Instance.energyRefreshTime = energyRFTimeN;
                       Enemy.reputation = reputationN;
                   }
               }
               else if (response.Errors.GetString("ERROR").Contains("No results found"))
               {
                   playerDataObtained = true;
                   InfoReceivedCheck();
               }
               else
                   print(response.Errors.JSON);
           });
    }
    public void GetClanMemberData(string playerID, int memberInt, Clan clan)
    {
        new LogEventRequest()
            .SetEventKey("getPlayerData")           // Pass the event name to Sparks server here
            .SetEventAttribute("PLAYER_ID", playerID)   // List all the attributes
            .Send((response) =>
            {
                if (!response.HasErrors)
                {
                    GSData data = response.ScriptData;   // The JSON file
                    int repValue = data.GetGSData(playerID).GetInt("reputation").GetValueOrDefault(-404);
                    int memLv = data.GetGSData(playerID).GetInt("level").GetValueOrDefault(-404);
                    //Debug.Log(repValue + " obtained rep value here");
                    clan.InsertPlayerDataInfo(memberInt, repValue, memLv);
                }
                else
                    Debug.Log(response.Errors);
            });
    }
    public void GetEnemyData(string accessedPlayerID, int enemyInt, string type)
    {
        new LogEventRequest()
        .SetEventKey("getPlayerData")           // Pass the event name to Sparks server here
        .SetEventAttribute("PLAYER_ID", accessedPlayerID)   // List all the attributes
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                GSData data = response.ScriptData;  // The JSON file

                int levelnew = Convert.ToInt32(data.GetGSData(accessedPlayerID).GetInt("level").GetValueOrDefault(-404));
                int repnew = Convert.ToInt32(data.GetGSData(accessedPlayerID).GetInt("reputation").GetValueOrDefault(-404));
                float shufflenew = Convert.ToInt32(data.GetGSData(accessedPlayerID).GetInt("shuffleTime").GetValueOrDefault(-404));

                if (shufflenew > 100)
                    shufflenew = shufflenew / 100;

                if (type == "regular")
                {
                    EnemyLoader.Instance.enemyLevels[enemyInt] = levelnew;                  // In this case, the persistor is the entry name 'playerId'
                    EnemyLoader.Instance.enemyReputations[enemyInt] = repnew;           // Get the Script data that is saved using the persistor name.
                    EnemyLoader.Instance.enemyShuffleTimes[enemyInt] = shufflenew;      // Get the Script data that is saved using the persistor name.
                    EnemyLoader.Instance.ConfirmPullComplete(enemyInt);

                }
                else if (type == "siege")
                {
                    EnemyLoader.attackerLevelList[enemyInt] = levelnew;
                    EnemyLoader.attackerReputationList[enemyInt] = repnew;
                    EnemyLoader.attackerShuffleTimeList[enemyInt] = shufflenew;
                    EnemyLoader.Instance.CheckIfEnemyInfoRetrieved();
                }
                else if (type == "chatbox")
                {
                    chatboxContentGlobalChat.RetrieveEnemyData(levelnew, repnew, shufflenew);
                    if (ChatMessageChallenge.isChallenging == true)
                    {
                        chatboxContentGlobalChat.chatMessageChallenges[enemyInt].DataObtained();
                    }
                }
                else if (type == "chatboxGlobal")
                {
                    if (ChatMessageChallenge.isChallenging == true)
                    {
                        chatboxContentGlobalChat.chatMessageChallenges[enemyInt].DataObtained();
                        chatboxContentGlobalChat.RetrieveEnemyData(levelnew, repnew, shufflenew);
                    }
                }
                else if (type == "chatboxClan")
                {
                    chatboxContentClanChat.chatMessageChallenges[enemyInt].DataObtained();
                    chatboxContentClanChat.RetrieveEnemyData(levelnew, repnew, shufflenew);
                }
            }
            else
                Debug.Log(response.Errors.JSON);
        });
    }

    public void SavePlayerDetails()
    {
        new LogEventRequest()
            .SetEventKey("savePlayerDetails")
            .SetEventAttribute("clanID", Clan.clanID)
            .SetEventAttribute("portrait", Player.potraitID)
            .SetEventAttribute("tutorial", Player.tutorial)
            .Send((response) =>
        {
            if (response.HasErrors)
                Debug.Log(response.Errors.JSON);
            else
            {
                GSData data = response.ScriptData;

                if (Clan.clanJoinedTimeStampCheck == false)
                {
                    Clan.clanJoinedTimeStamp = data.GetGSData(playerId).GetLong("clanIDChangedTime").GetValueOrDefault(1558742399);
                    Clan.clanJoinedTimeStampCheck = true;
                    EventHolderManager.Instance.UpdateLastXcaliburHolder();
                }
            }
        });
    }
    public void GetPlayerDetails(string accessedPlayerID)
    {
        new LogEventRequest()
        .SetEventKey("getPlayerDetails")                                   // Pass the event name to Sparks server here
        .SetEventAttribute("PLAYER_ID", accessedPlayerID)       // List all the attributes
        .Send((response) =>
       {
           if (!response.HasErrors)
           {
               GSData data = response.ScriptData;   // The JSON file

               DataManager.portrait = data.GetGSData(playerId).GetString("portrait");
               DataManager.tutorialState = data.GetGSData(playerId).GetInt("tutorial").GetValueOrDefault(-404);
               Clan.clanID = data.GetGSData(playerId).GetString("clanID");

               if (data.GetGSData(playerId).GetString("clanDropped") != null)
               {
                   string oldClan = data.GetGSData(playerId).GetString("clanDropped");
                   ClanManager.clanRemovalNotificaion = ClanManager.RemoveFromClan.disband;
                   ClanManager.oldClan = oldClan;
               }
               if (data.GetGSData(playerId).GetString("removalNotice") != null)
               {
                   string oldClan = data.GetGSData(playerId).GetString("clanRemoved");
                   ClanManager.clanRemovalNotificaion = ClanManager.RemoveFromClan.kick;
                   ClanManager.oldClan = oldClan;
               }

               playerDetailsObtained = true;
               InfoReceivedCheck();
           }
           else if (response.Errors.GetString("ERROR").Contains("No results found"))
           {
               playerDetailsObtained = true;
               InfoReceivedCheck();
           }
           else
               print(response.Errors.JSON);
       });
    }
    public void GetEnemyDetails(string accessedPlayerID, int enemyInt, string type)
    {
        new LogEventRequest()
        .SetEventKey("getPlayerDetails")            // Pass the event name to Sparks server here
        .SetEventAttribute("PLAYER_ID", accessedPlayerID)   // List all the attributes
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                GSData data = response.ScriptData;  // The JSON file
                string racenew = data.GetGSData(accessedPlayerID).GetString("race");
                string guildnew = data.GetGSData(accessedPlayerID).GetString("clanID");
                string portrait = data.GetGSData(accessedPlayerID).GetString("portrait");
                string pName = data.GetGSData(accessedPlayerID).GetString("playerName");
                if (type == "regular")
                {
                    EnemyLoader.Instance.enemyRaces[enemyInt] = racenew;
                    EnemyLoader.Instance.enemyClans[enemyInt] = guildnew;
                    EnemyLoader.Instance.enemyPotrait[enemyInt] = portrait;
                    EnemyLoader.Instance.enemyNames[enemyInt] = pName;
                    EnemyLoader.Instance.enemyAssignedUnits[enemyInt].AddName(pName);
                    EnemyLoader.Instance.ConfirmPullComplete(enemyInt);
                }
                else if (type == "siege")
                {
                    EnemyLoader.attackerRaceList[enemyInt] = racenew;
                    EnemyLoader.attackerClanList[enemyInt] = guildnew;
                    EnemyLoader.attackerPotraitList[enemyInt] = portrait;
                    EnemyLoader.attackerNameList[enemyInt] = pName;
                    EnemyLoader.Instance.attackerAssignedUnitList[enemyInt].AddName(pName);
                    EnemyLoader.Instance.CheckIfEnemyInfoRetrieved();
                }
                else if (type == "chatboxGlobal")
                {
                    chatboxContentGlobalChat.senderAvatar[enemyInt] = portrait;
                    chatboxContentGlobalChat.senderGuild[enemyInt] = guildnew;
                    chatboxContentGlobalChat.senderRace[enemyInt] = racenew;
                    chatboxContentGlobalChat.AssignDetails(enemyInt, guildnew, racenew, portrait);

                    if (ChatMessageChallenge.isChallenging == true)
                    {
                        chatboxContentGlobalChat.chatMessageChallenges[enemyInt].DetailObtained();
                        chatboxContentGlobalChat.AssignDetails(enemyInt, guildnew, racenew, portrait);
                    }
                }
                else if (type == "chatboxClan")
                {
                    chatboxContentClanChat.senderAvatar[enemyInt] = portrait;
                    chatboxContentClanChat.senderGuild[enemyInt] = guildnew;
                    chatboxContentClanChat.senderRace[enemyInt] = racenew;
                    chatboxContentClanChat.chatMessageChallenges[enemyInt].DetailObtained();
                    chatboxContentClanChat.AssignDetails(enemyInt, guildnew, racenew, portrait);
                }
                else if (type == "mail")
                {
                    MailManager.Instance.NewPlayerDetailsRetrieved(accessedPlayerID, pName, guildnew, portrait);
                }
            }
        });
    }
    public void GetChatPlayerDetails(string accessedPlayerID, int enemyInt, string message, string type, DateTime sendTime)
    {
        new LogEventRequest()
        .SetEventKey("getPlayerDetails")            // Pass the event name to Sparks server here
        .SetEventAttribute("PLAYER_ID", accessedPlayerID)   // List all the attributes
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                GSData data = response.ScriptData;  // The JSON file

                string racenew = data.GetGSData(accessedPlayerID).GetString("race");
                string guildnew = data.GetGSData(accessedPlayerID).GetString("clanID");
                string portrait = data.GetGSData(accessedPlayerID).GetString("portrait");
                string pName = data.GetGSData(accessedPlayerID).GetString("playerName");


                if (type == "global")
                {
                    chatboxContentGlobalChat.DisplayMessageInChatbox(message, sendTime);
                    chatboxContentGlobalChat.senderAvatar[enemyInt] = portrait;
                    chatboxContentGlobalChat.senderGuild[enemyInt] = guildnew;
                    chatboxContentGlobalChat.senderRace[enemyInt] = racenew;
                    chatboxContentGlobalChat.AssignDetails(enemyInt, guildnew, racenew, portrait);
                }
                else if (type == "clan")
                {
                    chatboxContentClanChat.DisplayMessageInChatbox(message, sendTime);
                    chatboxContentClanChat.senderAvatar[enemyInt] = portrait;
                    chatboxContentClanChat.senderGuild[enemyInt] = guildnew;
                    chatboxContentClanChat.senderRace[enemyInt] = racenew;
                    chatboxContentClanChat.AssignDetails(enemyInt, guildnew, racenew, portrait);
                }
            }
            else
                Debug.Log(response.Errors.JSON);
        });
    }
    public void GetClanMemberDetails(string memberID, int memberInt, Clan clan)
    {
        new LogEventRequest()
           .SetEventKey("getPlayerDetails")            // Pass the event name to Sparks server here
            .SetEventAttribute("PLAYER_ID", memberID)   // List all the attributes
           .Send((response) =>
           {
               if (!response.HasErrors)
               {
                   GSData data = response.ScriptData;  // The JSON file
                   string portrait = data.GetGSData(memberID).GetString("portrait");

                   clan.InsertPlayerInfo(memberInt, portrait);
               }
               else
                   print(response.Errors.JSON);
           });
    }
    public void GetClanMembersLastLogout(string memberID, int memberInt, Clan clan)
    {
        new LogEventRequest()
             .SetEventKey("getLastLogout")           // Pass the event name to Sparks server here
            .SetEventAttribute("PLAYER_ID", memberID)
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                GSData data = response.ScriptData;
                long lastLog = (data.GetGSData(memberID).GetLong("unixTimestamp").GetValueOrDefault(-404));
                clan.InsertPlayerLastLogout(memberInt, lastLog);
            }
            else
                Debug.Log("Failed to get last logout time " + response.Errors.JSON);
        });
    }

    public void AttemptShopPurchase(ShopManager smanager, ShopManager.PurchaseItem purchaseType, int value)
    {
        new LogEventRequest()
        .SetEventKey("savePlayerExpendables")
        .SetEventAttribute("diamond", DataManager.diamond)
        .SetEventAttribute("excalipoints", DataManager.excalipoints)
        .SetEventAttribute("gold", DataManager.gold)
        .SetEventAttribute("accessedID", playerId)
        .SetDurable(true)
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                if (purchaseType == ShopManager.PurchaseItem.diamond)
                {
                    smanager.DiamondExchangeOutcome(value);
                }
            }
            else
                Debug.Log(response.Errors.JSON);
        }
        );
    }

    public void SavePlayerExpendables(string accessedPlayerID)
    {
        if (IsAccessingOwnData(accessedPlayerID))
        {
            if (Player.tutorial >= Player.tutorialCap)
            {
                new LogEventRequest()
                    .SetEventKey("savePlayerExpendables")
                    .SetEventAttribute("diamond", DataManager.diamond)
                    .SetEventAttribute("excalipoints", DataManager.excalipoints)
                    .SetEventAttribute("gold", DataManager.gold)
                    .SetEventAttribute("accessedID", accessedPlayerID)
                    .Send((response) =>
                    {
                        if (response.HasErrors)
                            Debug.Log(response.Errors.JSON);
                    }
                    );
            }
            else
            {
                new LogEventRequest()
                    .SetEventKey("savePlayerExpendables")
                    .SetEventAttribute("diamond", DataManager.diamond)
                    .SetEventAttribute("excalipoints", DataManager.excalipoints)
                    .SetEventAttribute("gold", DataManager.gold)
                    .SetEventAttribute("accessedID", accessedPlayerID)
                    .Send((response) =>
                    {
                        if (response.HasErrors)
                            Debug.Log(response.Errors.JSON);
                    }
                    );
            }
        }
    }
    public void GetPlayerExpendables(string accessedPlayerID)
    {
        if (IsAccessingOwnData(accessedPlayerID))
        {
            new LogEventRequest()
                .SetEventKey("getPlayerExpendables")            // Pass the event name to Sparks server here
                .SetEventAttribute("PLAYER_ID", accessedPlayerID)   // List all the attributes
                .Send((response) =>
               {
                   if (!response.HasErrors)
                   {
                       GSData data = response.ScriptData;
                       DataManager.gold = data.GetGSData(playerId).GetInt("gold").GetValueOrDefault(-404);
                       DataManager.diamond = data.GetGSData(playerId).GetInt("diamond").GetValueOrDefault(-404);
                       DataManager.excalipoints = data.GetGSData(playerId).GetInt("excalipoints").GetValueOrDefault(-404);
                       playerExpenablesObtained = true;
                       InfoReceivedCheck();
                   }
                   else if (response.Errors.GetString("ERROR").Contains("No results found"))
                   {
                       playerExpenablesObtained = true;
                       InfoReceivedCheck();
                   }
                   else
                       print(response.Errors.JSON);
               });
        }
        else
        {
            new LogEventRequest()
               .SetEventKey("getPlayerExpendables")            // Pass the event name to Sparks server here
               .SetEventAttribute("PLAYER_ID", accessedPlayerID)   // List all the attributes
               .Send((response) =>
               {
                   if (!response.HasErrors)
                   {
                       GSData data = response.ScriptData;
                       Enemy.gold = data.GetGSData(accessedPlayerID).GetInt("gold").GetValueOrDefault(-404);
                       Enemy.Instance.diamond = data.GetGSData(accessedPlayerID).GetInt("diamond").GetValueOrDefault(-404);
                       Enemy.Instance.excalipoints = data.GetGSData(accessedPlayerID).GetInt("excalipoints").GetValueOrDefault(-404);
                   }
                   else
                       print(response.Errors.JSON);
               });
        }
    }
    public void GetPlayerExpendables(string enemyID, GameManager gmanager)
    {
        new LogEventRequest()
           .SetEventKey("getPlayerExpendables")            // Pass the event name to Sparks server here
           .SetEventAttribute("PLAYER_ID", enemyID)   // List all the attributes
           .Send((response) =>
           {
               if (!response.HasErrors)
               {
                   GSData data = response.ScriptData;
                   Enemy.gold = data.GetGSData(enemyID).GetInt("gold").GetValueOrDefault(-404);
                   Enemy.Instance.diamond = data.GetGSData(enemyID).GetInt("diamond").GetValueOrDefault(-404);
                   Enemy.Instance.excalipoints = data.GetGSData(enemyID).GetInt("excalipoints").GetValueOrDefault(-404);

                   gmanager.UpdateRaidedInfo();
               }
               else
                   print(response.Errors.JSON);
           });
    }
    public void GetEnemyExpendables(string accessedPlayerID, int enemyInt, string type)
    {
        new LogEventRequest()
        .SetEventKey("getPlayerExpendables")                           // Pass the event name to Sparks server here
        .SetEventAttribute("PLAYER_ID", accessedPlayerID)         // List all the attributes
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                GSData data = response.ScriptData;  // The JSON file
                int goldnew = data.GetGSData(accessedPlayerID).GetInt("gold").GetValueOrDefault(-404);

                if (type == "regular")
                {
                    EnemyLoader.Instance.enemyGolds[enemyInt] = goldnew;
                    EnemyLoader.Instance.ConfirmPullComplete(enemyInt);
                }
                else if (type == "siege")
                {
                    EnemyLoader.attackerGoldList[enemyInt] = goldnew;
                    EnemyLoader.Instance.CheckIfEnemyInfoRetrieved();
                }
                else if (type == "chatbox")
                {
                    if (ChatMessageChallenge.isChallenging == true)
                    {
                        chatboxContentGlobalChat.chatMessageChallenges[enemyInt].CurrencyObtained();
                    }
                }
                else if (type == "chatboxGlobal")
                {
                    if (ChatMessageChallenge.isChallenging == true)
                    {
                        chatboxContentGlobalChat.RetrieveEnemyGold(goldnew);
                        chatboxContentGlobalChat.chatMessageChallenges[enemyInt].CurrencyObtained();
                    }
                }
                else if (type == "chatboxClan")
                {
                    chatboxContentClanChat.RetrieveEnemyGold(goldnew);
                    chatboxContentClanChat.chatMessageChallenges[enemyInt].CurrencyObtained();
                }
            }
            else
                print(response.Errors.JSON + " ENEMY EXPANDABLES");
        });
    }
    public void GetClanMemberGoldInfo(string memberID, Clan clan)
    {
        new LogEventRequest()
               .SetEventKey("getPlayerExpendables")            // Pass the event name to Sparks server here
            .SetEventAttribute("PLAYER_ID", memberID)   // List all the attributes
               .Send((response) =>
               {
                   if (!response.HasErrors)
                   {
                       GSData data = response.ScriptData;
                       int gold = data.GetGSData(memberID).GetInt("gold").GetValueOrDefault(-404);

                       clan.MemberGoldRetrieved(gold);
                   }
                   else
                   {
                       ClanManager.Instance.InfoPopup(response.Errors.JSON);
                   }
               });
    }

    public void SavePlayerBattleLog()
    {
        new LogEventRequest()
            .SetEventKey("savePlayerBattleLog")
            .SetEventAttribute("battles", DataManager.battles)
            .SetEventAttribute("losses", DataManager.losses)
            .SetEventAttribute("victories", DataManager.victories)
            .SetDurable(true)
            .Send((response) =>
           {
               if (response.HasErrors)
                   print(response.Errors.JSON);
           });
    }
    public void GetPlayerBattleLog()
    {
        new LogEventRequest()
            .SetEventKey("getPlayerBattleLog")          // Pass the event name to Sparks server here
            .SetEventAttribute("PLAYER_ID", playerId)   // List all the attributes
            .Send((response) =>
           {
               if (!response.HasErrors)
               {
                   GSData data = response.ScriptData;  // The JSON file

                   DataManager.battles = data.GetGSData(playerId).GetInt("battles").GetValueOrDefault(-404);
                   DataManager.losses = data.GetGSData(playerId).GetInt("losses").GetValueOrDefault(-404);
                   DataManager.victories = data.GetGSData(playerId).GetInt("victories").GetValueOrDefault(-404);
                   battleLogsObtained = true;
                   InfoReceivedCheck();
               }
               else if (response.Errors.GetString("ERROR").Contains("No results found"))
               {
                   DataManager.battles = 0;
                   DataManager.losses = 0;
                   DataManager.victories = 0;
                   battleLogsObtained = true;
                   InfoReceivedCheck();
               }
               else
                   print(response.Errors.JSON);
           });
    }
    public void GetClanMemberBattleLog(string memberID, int playerQueue, Clan clan)
    {
        new LogEventRequest()
            .SetEventKey("getPlayerBattleLog")          // Pass the event name to Sparks server here
            .SetEventAttribute("PLAYER_ID", memberID)   // List all the attributes
            .Send((response) =>
            {
                if (!response.HasErrors)
                {
                    GSData data = response.ScriptData;  // The JSON file

                    int battles = data.GetGSData(memberID).GetInt("battles").GetValueOrDefault(-404);
                    int losses = data.GetGSData(memberID).GetInt("losses").GetValueOrDefault(-404);
                    int victories = data.GetGSData(memberID).GetInt("victories").GetValueOrDefault(-404);

                    clan.InsertPlayerInfo(playerQueue, battles, losses, victories);
                }
                else
                {
                    ClanManager.Instance.InfoPopup(response.Errors.JSON);
                }
            });
    }

    public void SaveInventoryChestInfo()
    {
        new LogEventRequest()
            .SetEventKey("saveInventoryChestInfo")
            .SetEventAttribute("bronze", DataManager.bronzeChest)
            .SetEventAttribute("gold", DataManager.silverChest)
            .SetEventAttribute("silver", DataManager.goldChest)
            .SetDurable(true)
            .Send((response) =>
            {
                if (response.HasErrors)
                    print(response.Errors.JSON);
            }
            );
    }
    public void GetInventoryChestInfo()
    {
        new LogEventRequest()
            .SetEventKey("getInventoryChestInfo")           // Pass the event name to Sparks server here
            .SetEventAttribute("PLAYER_ID", playerId)   // List all the attributes
            .Send((response) =>
           {
               if (!response.HasErrors)
               {
                   GSData data = response.ScriptData;   // The JSON file

                   //if (response.ScriptData == null)
                   //	print("Empty script data");
                   //else
                   //print("Successfully retrieved script data");

                   DataManager.bronzeChest = data.GetGSData(playerId).GetInt("bronze").GetValueOrDefault(-404);
                   DataManager.goldChest = data.GetGSData(playerId).GetInt("gold").GetValueOrDefault(-404);
                   DataManager.silverChest = data.GetGSData(playerId).GetInt("silver").GetValueOrDefault(-404);
                   inventoryChestsObtained = true;
                   InfoReceivedCheck();
               }
               else if (response.Errors.GetString("ERROR").Contains("No results found"))
               {
                   inventoryChestsObtained = true;
                   InfoReceivedCheck();
               }
               else
                   print("Failed to load data");
           });
    }

    public void SaveInventoryEnergyPacks()
    {
        new LogEventRequest()
            .SetEventKey("saveInventoryEnergyPacks")
            .SetEventAttribute("energyPacks", DataManager.energyPacks)
            .SetDurable(true)
            .Send((response) =>
       {
           if (response.HasErrors)
               print(response.Errors.JSON);
       }
            );
    }
    public void GetInventoryEnergyPacks()
    {
        new LogEventRequest()
            .SetEventKey("getInventoryEnergyPacks")         // Pass the event name to Sparks server here
            .SetEventAttribute("PLAYER_ID", playerId)   // List all the attributes
            .Send((response) =>
           {
               if (!response.HasErrors)
               {
                   GSData data = response.ScriptData;

                   DataManager.energyPacks = data.GetGSData(playerId).GetInt("energyPacks").GetValueOrDefault(-404);
                   inventoryEnergyPacksObtained = true;
                   InfoReceivedCheck();
               }
               else if (response.Errors.GetString("ERROR").Contains("No results found"))
               {
                   inventoryEnergyPacksObtained = true;
                   InfoReceivedCheck();
               }
               else
                   print("Failed to load data");
           });
    }

    public void SaveInventoryPacksInfo()
    {
        new LogEventRequest()
            .SetEventKey("saveInventoryPacksInfo")
            .SetEventAttribute("jack", DataManager.jackPack)
            .SetEventAttribute("queen", DataManager.queenPack)
            .SetEventAttribute("king", DataManager.kingPack)
            .SetDurable(true)
            .Send((response) =>
           {

               if (response.HasErrors)
                   print(response.Errors.JSON);
           }
            );
    }
    public void GetInventoryPacksInfo()
    {
        new LogEventRequest()
            .SetEventKey("getInventoryPacksInfo")           // Pass the event name to Sparks server here
            .SetEventAttribute("PLAYER_ID", playerId)   // List all the attributes
            .Send((response) =>
           {
               if (!response.HasErrors)
               {
                   GSData data = response.ScriptData;   // The JSON file

                   DataManager.jackPack = data.GetGSData(playerId).GetInt("jack").GetValueOrDefault(-404);
                   DataManager.queenPack = data.GetGSData(playerId).GetInt("queen").GetValueOrDefault(-404);
                   DataManager.kingPack = data.GetGSData(playerId).GetInt("king").GetValueOrDefault(-404);
                   inventoryCardPacksObtained = true;
                   InfoReceivedCheck();

               }
               else if (response.Errors.GetString("ERROR").Contains("No results found"))
               {
                   inventoryCardPacksObtained = true;
                   InfoReceivedCheck();
               }
               else
                   print("Failed to load data");
           });
    }

    public void SaveInventoryStones()
    {
        new LogEventRequest()
            .SetEventKey("saveInventoryStones")
            .SetEventAttribute("circle", DataManager.circleStones)
            .SetEventAttribute("square", DataManager.squareStones)
            .SetEventAttribute("triangle", DataManager.triangleStones)
            .SetDurable(true)
            .Send((response) =>
           {

               if (response.HasErrors)
                   print(response.Errors.JSON);
           }
            );
    }
    public void GetInventoryStones()
    {
        new LogEventRequest()
            .SetEventKey("getInventoryStones")          // Pass the event name to Sparks server here
            .SetEventAttribute("PLAYER_ID", playerId)   // List all the attributes
            .Send((response) =>
           {
               if (!response.HasErrors)
               {
                   GSData data = response.ScriptData;  // The JSON file

                   //if (response.ScriptData == null)
                   //	print("Empty script data");
                   //else
                   //print("Successfully retrieved script data");

                   DataManager.circleStones = data.GetGSData(playerId).GetInt("circle").GetValueOrDefault(-404);
                   DataManager.squareStones = data.GetGSData(playerId).GetInt("square").GetValueOrDefault(-404);
                   DataManager.triangleStones = data.GetGSData(playerId).GetInt("triangle").GetValueOrDefault(-404);
                   inventoryStonesObtained = true;
                   InfoReceivedCheck();

                   //					print ("circle => " + circle); 
                   //					print ("square => " + square); 
                   //					print ("triangle => " + triangle);
               }
               else if (response.Errors.GetString("ERROR").Contains("No results found"))
               {
                   DataManager.circleStones = 0;
                   DataManager.squareStones = 0;
                   DataManager.triangleStones = 0;
                   inventoryStonesObtained = true;
                   InfoReceivedCheck();
               }
               else
                   print("Failed to load data");
           });
    }

    public void SaveInventoryTimers()
    {
        new LogEventRequest()
            .SetEventKey("saveInventoryTimers")
            .SetEventAttribute("timer3", DataManager.timer3Amount)
            .SetEventAttribute("timer15", DataManager.timer15Amount)
            .SetEventAttribute("timer30", DataManager.timer30Amount)
            .SetEventAttribute("timer60", DataManager.timer60Amount)
            .SetDurable(true)
            .Send((response) =>
           {
               //if (!response.HasErrors)
               //	print("Successfully saved");
               //else
               //print("Failed to save data");
           }
            );
    }
    public void GetInventoryTimers()
    {
        new LogEventRequest()
            .SetEventKey("getInventoryTimers")          // Pass the event name to Sparks server here
            .SetEventAttribute("PLAYER_ID", playerId)   // List all the attributes
            .Send((response) =>
           {
               if (!response.HasErrors)
               {
                   GSData data = response.ScriptData;  // The JSON file

                   //if (response.ScriptData == null)
                   //	print("Empty script data");
                   //else
                   //print("Successfully retrieved script data");

                   DataManager.timer3Amount = data.GetGSData(playerId).GetInt("timer3").GetValueOrDefault(-404);
                   DataManager.timer15Amount = data.GetGSData(playerId).GetInt("timer15").GetValueOrDefault(-404);
                   DataManager.timer30Amount = data.GetGSData(playerId).GetInt("timer30").GetValueOrDefault(-404);
                   DataManager.timer60Amount = data.GetGSData(playerId).GetInt("timer60").GetValueOrDefault(-404);
                   inventoryTimersObtained = true;
                   InfoReceivedCheck();

                   //					print ("timer3 => " + timer3); 
                   //					print ("timer15 => " + timer15); 
                   //					print ("timer30 => " + timer30);
                   //					print ("timer60 => " + timer60);
               }
               else if (response.Errors.GetString("ERROR").Contains("No results found"))
               {
                   DataManager.timer3Amount = 0;
                   DataManager.timer15Amount = 0;
                   DataManager.timer30Amount = 0;
                   DataManager.timer60Amount = 0;
                   inventoryTimersObtained = true;
                   InfoReceivedCheck();
               }
               else
                   print("Failed to load data");
           });
    }

    public void SaveStructureAllUnitBuildings()
    {
        for (int i = 0; i < 3; i++)
        {
            SaveBarracks(i);// Encapsulated Method to access a single barracks according to its no
        }                   // In this case, using for loop will access all the barracks available
    }
    public void GetStructureAllUnitBuildings()
    {
        for (int i = 0; i < 3; i++)
        {
            GetBarracks(i);// Encapsulated Method to access a single barracks according to its no
        }                   // In this case, using for loop will access all the barracks available
    }

    public void SaveStructureTownHall()
    {
        new LogEventRequest()
            .SetEventKey("saveStructureTownHall")
            .SetEventAttribute("level", DataManager.townHallLv)
            .SetDurable(true)
            .Send((response) =>
           {
               if (response.HasErrors)
                   print(response.Errors.JSON);
           }
            );
    }
    public void GetStructureTownHall()
    {
        new LogEventRequest()
            .SetEventKey("getStructureTownHall")            // Pass the event name to Sparks server here
            .SetEventAttribute("PLAYER_ID", playerId)   // List all the attributes
            .Send((response) =>
           {
               if (!response.HasErrors)
               {
                   GSData data = response.ScriptData;   // The JSON file

                   DataManager.townHallLv = data.GetGSData(playerId).GetInt("level").GetValueOrDefault(-404);
                   townHallInfoObtained = true;
                   InfoReceivedCheck();
               }
               else if (response.Errors.GetString("ERROR").Contains("No results found"))
               {
                   townHallInfoObtained = true;
                   InfoReceivedCheck();
               }
               else
                   print("Failed to load data");
           });
    }

    public void SaveStructureTreasury()
    {
        new LogEventRequest()
            .SetEventKey("saveStructureTreasury")
            .SetEventAttribute("level", DataManager.treasuryLv)
            .SetEventAttribute("storageLimit", DataManager.treasuryStorageLimit)
            .SetDurable(true)
            .Send((response) =>
       {
           if (response.HasErrors)
               print(response.Errors.JSON);
       });
    }
    public void GetStructureTreasury()
    {
        new LogEventRequest()
            .SetEventKey("getStructureTreasury")            // Pass the event name to Sparks server here
            .SetEventAttribute("PLAYER_ID", playerId)   // List all the attributes
            .Send((response) =>
           {
               if (!response.HasErrors)
               {
                   GSData data = response.ScriptData;   // The JSON file

                   //if (response.ScriptData == null)
                   //	print("Empty script data");
                   //else
                   //print("Successfully retrieved script data");

                   DataManager.treasuryLv = data.GetGSData(playerId).GetInt("level").GetValueOrDefault(-404);
                   treasuryInfoObtained = true;
                   InfoReceivedCheck();
               }
               else if (response.Errors.GetString("ERROR").Contains("No results found"))
               {
                   treasuryInfoObtained = true;
                   InfoReceivedCheck();
               }
               else
                   print("Failed to load data");
           });
    }
    public void GetClanMemberTreasury(string memberID, Clan clan)
    {
        new LogEventRequest()
        .SetEventKey("getStructureTreasury")                // Pass the event name to Sparks server here
        .SetEventAttribute("PLAYER_ID", memberID)      // List all the attributes
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                GSData data = response.ScriptData;          // The JSON file

                long treasuryLimit = data.GetGSData(memberID).GetLong("storageLimit").GetValueOrDefault(-404);
                clan.MemberGoldLimitRetrieved(treasuryLimit);
            }
            else
                ClanManager.Instance.InfoPopup(response.Errors.JSON);
        });
    }

    public void SaveStructureForge()
    {
        new LogEventRequest()
            .SetEventKey("saveStructureForge")
            .SetEventAttribute("level", DataManager.forgeLv)
            .SetDurable(true)
            .Send((response) =>
            {
                if (response.HasErrors)
                    print(response.Errors.JSON);
            }
            );
    }
    public void GetStructureForge()
    {
        new LogEventRequest()
            .SetEventKey("getStructureForge")            // Pass the event name to Sparks server here
            .SetEventAttribute("PLAYER_ID", playerId)   // List all the attributes
            .Send((response) =>
            {
                if (!response.HasErrors)
                {
                    GSData data = response.ScriptData;  // The JSON file

                    DataManager.forgeLv = data.GetGSData(playerId).GetInt("level").GetValueOrDefault(-404);
                    forgeInfoObtained = true;
                    InfoReceivedCheck();
                }
                else if (response.Errors.GetString("ERROR").Contains("No results found"))
                {
                    forgeInfoObtained = true;
                    InfoReceivedCheck();
                }
                else
                    Debug.Log(response.Errors);
            });
    }

    public void SaveStructurePreparationHall()
    {
        new LogEventRequest()
            .SetEventKey("saveStructurePreparationHall")
            .SetEventAttribute("level", DataManager.preparationHallLv)
            .SetDurable(true)
            .Send((response) =>
           {
               if (response.HasErrors)
                   print(response.Errors.JSON);
           }
            );
    }
    public void GetStructurePreparationHall()
    {
        new LogEventRequest()
            .SetEventKey("getStructurePreparationHall")         // Pass the event name to Sparks server here
            .SetEventAttribute("PLAYER_ID", playerId)               // List all the attributes
            .Send((response) =>
           {
               if (!response.HasErrors)
               {
                   GSData data = response.ScriptData;               // The JSON file

                   //if (response.ScriptData == null)
                   //	print("Empty script data");
                   //else
                   //print("Successfully retrieved script data");

                   DataManager.preparationHallLv = data.GetGSData(playerId).GetInt("level").GetValueOrDefault(-404);
                   preparationHallInfoObtained = true;
                   InfoReceivedCheck();
               }
               else if (response.Errors.GetString("ERROR").Contains("No results found"))
               {
                   preparationHallInfoObtained = true;
                   InfoReceivedCheck();
               }
               else
                   print("Failed to load data");
           });
    }

    public void SaveAllStructureMines()
    {
        for (int i = 0; i < 5; i++)
        {
            SaveMine(i);    // Encapsulated Method to access a single mine according to its no
        }                   // In this case, using for loop will access all the mines available
    }
    public void GetAllStructureMines()
    {
        for (int i = 0; i < 5; i++)
        {
            GetMine(i);
        }
    }

    public void SaveAllHeroInfos()
    {
        for (int i = 0; i < releasedHeroesAmount; i++)
        {
            SaveHeroInfo(i);
        }
    }
    public void GetAllHeroInfos()
    {
        for (int i = 0; i < releasedHeroesAmount; i++)
        {
            GetHeroInfo(i);
        }
    }

    public void SaveAllAssignedUnits(string accessedPlayerID)
    {
        for (int i = 1; i < 4; i++)
        {
            SaveAssignedUnitsSet(i, accessedPlayerID);
        }
    }
    public void GetAllAssignedUnits(string accessedPlayerID)
    {
        if (IsAccessingOwnData(accessedPlayerID))
        {
            for (int i = 1; i < 4; i++)
            {
                GetAssignedUnitsSet(i, accessedPlayerID);
            }
        }
        else
        {
            new LogEventRequest()
                .SetEventKey("getAssignedUnitsSet1")            // Pass the event name to Sparks server here
                .SetEventAttribute("PLAYER_ID", accessedPlayerID)   // List all the attributes
                .Send((response) =>
               {
                   if (!response.HasErrors)
                   {
                       GSData data = response.ScriptData;   // The JSON file

                       for (int i = 0; i < 3; i++)
                       {
                           print("Troop: " + data.GetGSData(accessedPlayerID).GetInt("slot" + i).GetValueOrDefault(-404));
                           Enemy.troopsAssigned[i] = data.GetGSData(accessedPlayerID).GetInt("slot" + i).GetValueOrDefault(-404);
                       }
                   }
                   else
                       print("Failed to load data");
               });
            new LogEventRequest()
                .SetEventKey("getAssignedUnitsSet2")            // Pass the event name to Sparks server here
                .SetEventAttribute("PLAYER_ID", accessedPlayerID)   // List all the attributes
                .Send((response) =>
               {
                   if (!response.HasErrors)
                   {
                       GSData data = response.ScriptData;   // The JSON file

                       for (int i = 0; i < 3; i++)
                       {
                           print("Troop: " + data.GetGSData(accessedPlayerID).GetInt("slot" + i).GetValueOrDefault(-404));
                           Enemy.troopsAssigned[i + 3] = data.GetGSData(accessedPlayerID).GetInt("slot" + i).GetValueOrDefault(-404);
                       }
                   }
                   else
                       print("Failed to load data");
               });
            new LogEventRequest()
                .SetEventKey("getAssignedUnitsSet3")            // Pass the event name to Sparks server here
                .SetEventAttribute("PLAYER_ID", accessedPlayerID)   // List all the attributes
                .Send((response) =>
               {
                   if (!response.HasErrors)
                   {
                       GSData data = response.ScriptData;   // The JSON file

                       for (int i = 0; i < 3; i++)
                       {
                           print("Troop: " + data.GetGSData(accessedPlayerID).GetInt("slot" + i).GetValueOrDefault(-404));
                           Enemy.troopsAssigned[i + 6] = data.GetGSData(accessedPlayerID).GetInt("slot" + i).GetValueOrDefault(-404);
                       }
                   }
                   else
                       print("Failed to load data");
               });
        }
    }
    public void GetEnemyAssignedUnits(string accessedPlayerID, int enemyInt, string type)
    {
        new LogEventRequest()
        .SetEventKey("getAssignedUnitsSet1")            // Pass the event name to Sparks server here
        .SetEventAttribute("PLAYER_ID", accessedPlayerID)   // List all the attributes
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                GSData data = response.ScriptData;  // The JSON file

                for (int i = 0; i < 3; i++)
                {
                    //Check the ID of the unit saved
                    int unitInt = data.GetGSData(accessedPlayerID).GetInt("slot" + i).GetValueOrDefault(-404);
                    int unitLevel = data.GetGSData(accessedPlayerID).GetInt("slot" + i + "level").GetValueOrDefault(-404);
                    //if it is a unit
                    if (type == "regular")
                    {
                        if (unitInt != 250)
                        {
                            //check if its a hero
                            if (unitInt > 100) EnemyLoader.Instance.enemyAssignedUnits[enemyInt].AddAssignedUnit(unitInt, 0);
                            else EnemyLoader.Instance.enemyAssignedUnits[enemyInt].AddAssignedUnit(unitInt, unitLevel);
                        }
                    }
                    else if (type == "siege")
                    {
                        if (unitInt != 250)
                        {
                            //check if its a hero
                            if (unitInt > 100) EnemyLoader.Instance.attackerAssignedUnitList[enemyInt].AddAssignedUnit(unitInt, 0);
                            else EnemyLoader.Instance.attackerAssignedUnitList[enemyInt].AddAssignedUnit(unitInt, unitLevel);
                        }
                        if (i == 2)
                            EnemyLoader.Instance.attackerAssignedUnitList[enemyInt].SetUnitCheck(0);
                    }
                    else if (type == "chatbox")
                    {
                        if (unitInt != 250)
                        {
                            //check if its a hero
                            if (unitInt > 100) EnemyLoader.Instance.chatboxTargetAssignedUnits.AddAssignedUnit(unitInt, 0);
                            else EnemyLoader.Instance.chatboxTargetAssignedUnits.AddAssignedUnit(unitInt, unitLevel);
                        }
                        if (i == 2)
                        {
                            EnemyLoader.Instance.TroopsObtained();
                        }
                    }
                }

                if (type == "regular")
                    EnemyLoader.Instance.TroopPullCheck(enemyInt);
            }
        });
        new LogEventRequest()
        .SetEventKey("getAssignedUnitsSet2")            // Pass the event name to Sparks server here
        .SetEventAttribute("PLAYER_ID", accessedPlayerID)   // List all the attributes
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                GSData data = response.ScriptData;  // The JSON file

                for (int i = 0; i < 3; i++)
                {
                    //Check the ID of the unit saved
                    int unitInt = data.GetGSData(accessedPlayerID).GetInt("slot" + i).GetValueOrDefault(-404);
                    int unitLevel = data.GetGSData(accessedPlayerID).GetInt("slot" + i + "level").GetValueOrDefault(-404);
                    //if it is a unit
                    if (type == "regular")
                    {
                        if (unitInt != 250)
                        {
                            //check if its a hero
                            if (unitInt > 100) EnemyLoader.Instance.enemyAssignedUnits[enemyInt].AddAssignedUnit(unitInt, 0);
                            else EnemyLoader.Instance.enemyAssignedUnits[enemyInt].AddAssignedUnit(unitInt, unitLevel);
                        }
                    }
                    else if (type == "siege")
                    {
                        if (unitInt != 250)
                        {
                            //check if its a hero
                            if (unitInt > 100) EnemyLoader.Instance.attackerAssignedUnitList[enemyInt].AddAssignedUnit(unitInt, 0);
                            else EnemyLoader.Instance.attackerAssignedUnitList[enemyInt].AddAssignedUnit(unitInt, unitLevel);
                        }
                        if (i == 2)
                            EnemyLoader.Instance.attackerAssignedUnitList[enemyInt].SetUnitCheck(1);
                    }
                    else if (type == "chatbox")
                    {
                        if (unitInt != 250)
                        {
                            //check if its a hero
                            if (unitInt > 100) EnemyLoader.Instance.chatboxTargetAssignedUnits.AddAssignedUnit(unitInt, 0);
                            else EnemyLoader.Instance.chatboxTargetAssignedUnits.AddAssignedUnit(unitInt, unitLevel);
                        }
                        if (i == 2)
                        {
                            EnemyLoader.Instance.TroopsObtained();
                        }
                    }
                }
                if (type == "regular")
                    EnemyLoader.Instance.TroopPullCheck(enemyInt);
            }
        });
        new LogEventRequest()
        .SetEventKey("getAssignedUnitsSet3")            // Pass the event name to Sparks server here
        .SetEventAttribute("PLAYER_ID", accessedPlayerID)   // List all the attributes
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                GSData data = response.ScriptData;  // The JSON file

                for (int i = 0; i < 3; i++)
                {
                    //Check the ID of the unit saved
                    int unitInt = data.GetGSData(accessedPlayerID).GetInt("slot" + i).GetValueOrDefault(-404);
                    int unitLevel = data.GetGSData(accessedPlayerID).GetInt("slot" + i + "level").GetValueOrDefault(-404);
                    //if it is a unit
                    if (type == "regular")
                    {
                        if (unitInt != 250)
                        {
                            //check if its a hero
                            if (unitInt > 100) EnemyLoader.Instance.enemyAssignedUnits[enemyInt].AddAssignedUnit(unitInt, 0);
                            else EnemyLoader.Instance.enemyAssignedUnits[enemyInt].AddAssignedUnit(unitInt, unitLevel);
                        }
                    }
                    else if (type == "siege")
                    {
                        if (unitInt != 250)
                        {
                            //check if its a hero
                            if (unitInt > 100) EnemyLoader.Instance.attackerAssignedUnitList[enemyInt].AddAssignedUnit(unitInt, 0);
                            else EnemyLoader.Instance.attackerAssignedUnitList[enemyInt].AddAssignedUnit(unitInt, unitLevel);
                        }

                        if (i == 2)
                            EnemyLoader.Instance.attackerAssignedUnitList[enemyInt].SetUnitCheck(2);
                    }
                    else if (type == "chatbox")
                    {
                        if (unitInt != 250)
                        {
                            //check if its a hero
                            if (unitInt > 100) EnemyLoader.Instance.chatboxTargetAssignedUnits.AddAssignedUnit(unitInt, 0);
                            else EnemyLoader.Instance.chatboxTargetAssignedUnits.AddAssignedUnit(unitInt, unitLevel);
                        }
                        if (i == 2)
                        {
                            EnemyLoader.Instance.TroopsObtained();
                        }
                    }
                }
                if (type == "regular")
                    EnemyLoader.Instance.TroopPullCheck(enemyInt);
            }
        });
    }

    public void SaveRevengeList()
    {
        new LogEventRequest()
            .SetEventKey("saveRevengeList")
            .SetEventAttribute("attackerID", 11)
            .SetEventAttribute("defenderID", 11)
            .SetEventAttribute("goldLost", 11)
            .SetEventAttribute("timeOfAttack", 11)
            .Send((response) =>
       {
           if (response.HasErrors)
               print(response.Errors.JSON);
       }
            );
    }
    public void GetRevengeList()
    {
        new LogEventRequest()
            .SetEventKey("getRevengeList")          // Pass the event name to Sparks server here
            .SetEventAttribute("PLAYER_ID", playerId)   // List all the attributes
            .Send((response) =>
           {
               if (!response.HasErrors)
               {
                   GSData data = response.ScriptData;   // The JSON file

                   int attackerID = data.GetGSData(playerId).GetInt("attackerID").GetValueOrDefault(-404);
                   int defenderID = data.GetGSData(playerId).GetInt("defenderID").GetValueOrDefault(-404);
                   int goldLost = data.GetGSData(playerId).GetInt("goldLost").GetValueOrDefault(-404);
                   int timeOfAttack = data.GetGSData(playerId).GetInt("timeOfAttack").GetValueOrDefault(-404);

                   print("attackerID => " + attackerID);
                   print("defenderID => " + defenderID);
                   print("goldLost => " + goldLost);
                   print("timeOfAttack => " + timeOfAttack);
               }
               else
                   print("Failed to load data");
           });
    }

    public void SaveAllMissions()
    {
        for (int i = 0; i < 3; i++)
            SaveMission(i);
    }
    public void GetAllMissions()
    {
        for (int i = 0; i < 3; i++)
            GetMission(i);
    }

    public void CheckIfUsernameExists(string recipientName, string subject, string content)
    {
        new LogEventRequest()
        .SetEventKey("getIDFromUsername")
        .SetEventAttribute("userName", recipientName)
        .Send((response) =>
        {
            if (response.HasErrors)
            {
                Debug.Log(response.Errors.JSON);

                MailManager.Instance.DisplayEventHandling("User does not exist", true, true);
            }
            else
            {
                GSData scriptData = response.ScriptData;

                string userID = scriptData.GetGSData("FOUND").GetGSData("_id").GetString("$oid");

                SendMail(userID, subject, content);
            }
        });
    }

    public void SendMail(string recipientID, string subject, string content)
    {
        new LogEventRequest()
        .SetEventKey("sendMail")
        .SetEventAttribute("recipientID", recipientID)
        .SetEventAttribute("subject", subject)
        .SetEventAttribute("content", content)
        .Send((response) =>
       {
           if (!response.HasErrors)
           {
               GSData mailData = response.ScriptData.GetGSData("SENT");

               if (mailData != null)
               {
                   string recipientName = mailData.GetString("RecipientName");
                   string recipientAvatar = mailData.GetString("RecipientAvatar");
                   string recipientClan = mailData.GetString("RecipientClan");
                   long utcTime = mailData.GetLong("Time Sent").GetValueOrDefault(-404);

                   MailManager.Instance.DisplayEventHandling("Mail successfully sent", true, true);
                   MailManager.Instance.AddLoadedPlayer(recipientID, recipientName, recipientClan, recipientAvatar);
                   MailManager.Instance.MailSuccessfullySent(recipientName, recipientID, recipientAvatar, utcTime);
               }
           }
           else
           {
               if (response.Errors.GetString("NOT FOUND") != null)
               {
                   MailManager.Instance.DisplayEventHandling("User not found", true, true);
               }
               Debug.Log(response.Errors.JSON);
           }
       }
        );
    }
    public void GetMail()
    {
        new LogEventRequest()
        .SetEventKey("getMail")                     // Pass the event name to Sparks server here
        .SetEventAttribute("PLAYER_ID", playerId)   // List all the attributes
        .Send((response) =>
       {
           if (!response.HasErrors)
           {
               GSData data = response.ScriptData;   // The JSON file

               if (data.GetGSDataList("Sent Mails") != null)
               {
                   foreach (GSData sentMail in data.GetGSDataList("Sent Mails"))
                   {
                       string recepID = sentMail.GetString("recipientID");
                       string subject = sentMail.GetString("subject");
                       string content = sentMail.GetString("content");
                       long timeSent = sentMail.GetLong("sendTime").GetValueOrDefault(-404);

                       MailManager.Instance.SentMailsRawInfoUpdate(recepID, subject, content, timeSent);
                   }
                   // after all raw info injected, start checking for the Players names
                   MailManager.Instance.GetRecipientDetails();
               }

               if (data.GetGSDataList("Received Mails") != null)
               {
                   foreach (GSData receivedMail in data.GetGSDataList("Received Mails"))
                   {
                       string recepID = receivedMail.GetString("recipientID");
                       string senderID = receivedMail.GetString("senderID");
                       string subject = receivedMail.GetString("subject");
                       string content = receivedMail.GetString("content");
                       long timeSent = receivedMail.GetLong("sendTime").GetValueOrDefault(-404);

                       MailManager.Instance.ReceivedMailsRawInfoUpdate(recepID, subject, content, timeSent, senderID);
                   }
                   // after all raw info injected, start checking for the Players names
                   MailManager.Instance.GetSenderDetails();
               }
           }
       });
    }
    public void DeleteMail(string senderID, long timeStamp)
    {
        new LogEventRequest()
        .SetEventKey("deleteMail")
        .SetEventAttribute("timeStamp", timeStamp)
        .SetEventAttribute("senderID", senderID)
        .Send((response) =>
        {
            if (response.HasErrors)
                Debug.Log(response.Errors.JSON);
            else
            {
                MailManager.Instance.MailDeleteResponse(senderID, timeStamp);
            }
        });
    }
    public void DeleteExpiredMail(string senderID, long timeStamp, BaseMail expiredMail)
    {
        new LogEventRequest()
          .SetEventKey("deleteMail")
          .SetEventAttribute("timeStamp", timeStamp)
          .SetEventAttribute("senderID", senderID)
          .Send((response) =>
          {
              if (response.HasErrors)
                  Debug.Log(response.Errors.JSON);
              else
              {
                  if (senderID == playerId)
                      expiredMail.OutboxMailExpired();
                  else
                      expiredMail.InboxMailExpired();
              }
          });
    }

    public void CheckGiveaways(ReceiveManager receiveManager)
    {
        new LogEventRequest()
        .SetEventKey("retrieveGMGiveaways")
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                GSData scriptData = response.ScriptData;
                foreach (GSData info in scriptData.GetGSDataList("Giveaways"))
                {
                    string rewardReason = info.GetString("rewardReason");
                    int rewardValue = info.GetInt("rewardValue").GetValueOrDefault(-100);
                    int rewardDaysLeft = info.GetInt("rewardDuration").GetValueOrDefault(-100);
                    string rewardType = info.GetString("rewardType");
                    string rewardID = info.GetLong("rewardID").ToString();

                    receiveManager.AddCurrentGiveaways(rewardReason, rewardType, rewardValue, rewardID, rewardDaysLeft);
                }
                ReceiveManager.gmGiveawaysRetrieved = true;
                receiveManager.GiveawaysRetrieved();
            }
            else if (response.Errors.GetString("ERROR").Contains("No results found"))
            {
            }
        });
    }
    public void CheckClaimedGiveaways(ReceiveManager receiveManager, string rewardID_)
    {
        new LogEventRequest()
        .SetEventKey("retrieveClaimedGiveaways")
        .SetEventAttribute("rewardID", rewardID_)
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                string giveawayStatus = response.ScriptData.GetString("Giveaway");

                if (giveawayStatus == "Claimed")
                    receiveManager.StoreRetrievedRewardsID(rewardID_);
                else if (giveawayStatus == "Unclaimed")
                    receiveManager.CreateRewardOBJ(rewardID_);
                else
                    Debug.Log(giveawayStatus + " GiveawayClaim");
            }
            else
                receiveManager.ResponsePopup(response.Errors.JSON);
        });
    }

    public void GetTargetedRewards(ReceiveManager receiveManager)
    {
        new LogEventRequest()
        .SetEventKey("retrieveSentCurrency")
        .Send((response) =>
       {
           if (!response.HasErrors)
           {
               GSData scriptData = response.ScriptData;

               if (scriptData.GetGSDataList("SentItems") != null)
               {
                   foreach (GSData info in scriptData.GetGSDataList("SentItems"))
                   {
                       int rewardValue = info.GetInt("rewardValueSent").GetValueOrDefault(-100);
                       string rewardType = info.GetString("rewardTypeSent");
                       string rewardID = info.GetLong("objID").ToString();

                       receiveManager.CreateRetrievedTargetedGiveaways(rewardID, rewardType, rewardValue);
                   }
                   ReceiveManager.targetedGiveawaysRetrieved = true;
                   receiveManager.GiveawaysRetrieved();
               }
           }
           else if (response.Errors.GetString("ERROR").Contains("No results found"))
           {
               Debug.Log("No Giveaways");
           }
       });
    }

    public void SaveIntervalReward(bool onCollection)
    {
        new LogEventRequest()
        .SetEventKey("saveIntervalReward")
        .SetEventAttribute("refreshTime", (long)Interval_Rewards.timeTillNextReward)
        .Send((response) =>
       {
           if (!response.HasErrors)
           {
               if (onCollection)
                   Interval_Rewards.Instance.IntervalRewardResultHandler(true);
           }
       });
    }
    public void GetIntervalReward()
    {
        new LogEventRequest()
        .SetEventKey("getIntervalReward")               // Pass the event name to Sparks server here
        .SetEventAttribute("PLAYER_ID", playerId)   // List all the attributes
        .Send((response) =>
       {
           if (!response.HasErrors)
           {
               GSData data = response.ScriptData;  // The JSON file

               DataManager.intervalRewardRefreshTime = data.GetGSData(playerId).GetInt("refreshTime").GetValueOrDefault(-404);
               intervalRewardRefreshTimeObtained = true;

               InfoReceivedCheck();
           }
           else if (response.Errors.GetString("ERROR").Contains("No results found"))
           {
               // when there is no record on the GS server trigger initial reward
               SaveInitialReward();
               intervalRewardRefreshTimeObtained = true;
               InfoReceivedCheck();
           }
           else
               Debug.Log(response.Errors.JSON);
       });
    }
    public void SaveInitialReward()
    {
        //this will happen for old users who have tutorial over 8 and trigger interval rewards
        new LogEventRequest()
        .SetEventKey("saveIntervalReward")
        .SetEventAttribute("refreshTime", (long)5f)
        .SetDurable(true)
        .Send((response) =>
        {
            if (!response.HasErrors)
                GetIntervalReward();
            else
                Debug.Log(response.Errors.JSON);
        });
    }

    public void SaveBuildingUnderUpgrade()
    {
        new LogEventRequest()
        .SetEventKey("saveBuildingUnderUpgrade")
        .SetEventAttribute("unixTimestamp", (long)DataManager.buildingTimeLeft)
        .SetEventAttribute("ID", DataManager.buildingUnderUpgrade)
        .Send((response) =>
       {
           if (response.HasErrors)
               print(response.Errors.JSON);
       }
        );
    }
    // Also used for removing building from list after its upgrade is complete
    public void SaveBuildingUnderUpgrade(int buildingID, long unixTimestamp)
    {
        new LogEventRequest()
        .SetEventKey("saveBuildingUnderUpgrade")
        .SetEventAttribute("unixTimestamp", unixTimestamp)
        .SetEventAttribute("ID", buildingID)
        .Send((response) =>
       {
           if (response.HasErrors)
               Debug.Log(response.Errors.JSON);
       }
        );
    }
    public void GetBuildingUnderUpgrade()
    {
        new LogEventRequest()
        .SetEventKey("getBuildingUnderUpgrade")         // Pass the event name to Sparks server here
        .SetEventAttribute("PLAYER_ID", playerId)   // List all the attributes
        .Send((response) =>
       {
           if (!response.HasErrors)
           {
               GSData data = response.ScriptData;  // The JSON file

               //Debug.Log("BUILDING TIMERS " + data.JSON);

               DataManager.buildingUnderUpgrade = data.GetGSData(playerId).GetInt("ID").GetValueOrDefault(-404);
               DataManager.buildingTimeLeft = data.GetGSData(playerId).GetFloat("unixTimestamp").GetValueOrDefault(-404);
               buildingUnderUpgradeObtained = true;

               InfoReceivedCheck();
           }
           else if (response.Errors.GetString("ERROR").Contains("No results found"))
           {
               buildingUnderUpgradeObtained = true;
               InfoReceivedCheck();
           }
           else
               print("Failed to load data");
       });
    }

    public void SaveStonesUnderUpgrade()
    {
        //Debug.Log(string.Format("Saving the stones now 1:{0}    2:{1}    3:{2}", BuildTimer.earthstone0_Build_Time, BuildTimer.windStone0_Build_Time, BuildTimer.flameStone0_Build_Time));
        new LogEventRequest()
        .SetEventKey("saveStonesUnderUpgrade")
        .SetEventAttribute("slot0", (long)BuildTimer.earthstone0_Build_Time)
        .SetEventAttribute("slot1", (long)BuildTimer.windStone0_Build_Time)
        .SetEventAttribute("slot2", (long)BuildTimer.flameStone0_Build_Time)
        .SetEventAttribute("slot3", (long)BuildTimer.earthstone1_Build_Time)
        .SetEventAttribute("slot4", (long)BuildTimer.windStone1_Build_Time)
        .SetEventAttribute("slot5", (long)BuildTimer.flameStone1_Build_Time)
        //.SetDurable(true)
        .Send((response) =>
       {
           if (response.HasErrors)
           {
               Debug.Log(response.Errors);
           }
       }
        );
    }
    public void GetStonesUnderUpgrade()
    {
        new LogEventRequest()
        .SetEventKey("getStonesUnderUpgrade")           // Pass the event name to Sparks server here
        .SetEventAttribute("PLAYER_ID", playerId)           // List all the attributes
        .Send((response) =>
       {
           if (!response.HasErrors)
           {
               GSData data = response.ScriptData;  // The JSON file

               //Debug.Log("STONE TIMERS  " + data.JSON);

               BuildTimer.earthstone0_Build_Time = data.GetGSData(playerId).GetInt("slot0").GetValueOrDefault(-404);
               BuildTimer.windStone0_Build_Time = data.GetGSData(playerId).GetInt("slot1").GetValueOrDefault(-404);
               BuildTimer.flameStone0_Build_Time = data.GetGSData(playerId).GetInt("slot2").GetValueOrDefault(-404);
               BuildTimer.earthstone1_Build_Time = data.GetGSData(playerId).GetInt("slot3").GetValueOrDefault(-404);
               BuildTimer.windStone1_Build_Time = data.GetGSData(playerId).GetInt("slot4").GetValueOrDefault(-404);
               BuildTimer.flameStone1_Build_Time = data.GetGSData(playerId).GetInt("slot5").GetValueOrDefault(-404);

               BuildTimer.Instance.CapStoneResearchDuration();
               stoneUnderUpgradeObtained = true;
               InfoReceivedCheck();
           }
           else if (response.Errors.GetString("ERROR").Contains("No results found"))
           {
               stoneUnderUpgradeObtained = true;
               InfoReceivedCheck();
           }
           else
               print("Failed to load data");
       });
    }

    // ======= SIEGES =======\\
    public void SaveSeige(int goldRaided)
    {
        new LogEventRequest().SetEventKey("saveSiegedDetail")
        .SetEventAttribute("siegedGold", goldRaided)
        .SetEventAttribute("targetID", EnemyLoader.selectedEnemyID)
         .Send((response) =>
        {
            if (!response.HasErrors)
            {
                PlayerSieged(EnemyLoader.selectedEnemyID);
            }
            else
                Debug.Log(response.Errors.JSON);
        });
    }
    public void GetSeige(string occurrenceTime)
    {
        //occurenceTime is for Checking if the player was attacked while away or duing gamePlay
        new LogEventRequest().SetEventKey("getSiegedDetail")
        .SetEventAttribute("PLAYER_ID", playerId)
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                GSData data = response.ScriptData; // The JSON file
                EnemyLoader.Instance.ClearAttackerList();

                for (int x = 0; x < 10; x++)
                {
                    string searchField = playerId + x;
                    if (data.GetGSData(searchField) != null)
                    {
                        string attackerID = data.GetGSData(searchField).GetString("attackerID");

                        long timeOfAttack = (long)data.GetGSData(searchField).GetLong("timeOfAttack");
                        long goldLost = (long)data.GetGSData(searchField).GetLong("goldLost");

                        DateTime attackTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddMilliseconds(timeOfAttack * 1000);

                        EnemyLoader.Instance.AddAttackerToList(attackerID, attackTime, goldLost);
                        if (x == 9)
                        {
                            EnemyLoader.Instance.SiegeEnemiesDisplay(occurrenceTime);
                            siegeObtained = true;
                            Instance.InfoReceivedCheck();
                            break;
                        }
                    }
                    else
                    {
                        EnemyLoader.Instance.SiegeEnemiesDisplay(occurrenceTime);

                        siegeObtained = true;
                        Instance.InfoReceivedCheck();
                        break;
                    }
                }
            }
            else if (response.Errors.GetString("ERROR").Contains("No results found"))
            {
                siegeObtained = true;
                InfoReceivedCheck();
            }
            else
                Debug.Log(response.Errors.JSON);
        });
    }

    public void PlayerSieged(string targetID)
    {
        new LogEventRequest()
            .SetEventKey("notifySiege")
            .SetEventAttribute("player_Id", targetID)
            .Send((response) =>
            {
                if (!response.HasErrors)
                    UIManager.Instance.ToggleBackToHomeSceneButton();
                else
                    Debug.Log(response.Errors.JSON);
            });
    }

    public void UpdateRaidedEnemyInfo(string targetEnemyID, int expectedGold, bool battleOutcome)
    {
        new LogEventRequest()
          .SetEventKey("updateRaided")
          .SetEventAttribute("playerID", targetEnemyID)
          .SetEventAttribute("battleOutcome", battleOutcome.ToString())
          .Send((response) =>
          {
              if (response.HasErrors)
                  Debug.Log(response.Errors.JSON);
              else
              {
                  if (battleOutcome == true)
                  {
                      int goldRaided = response.ScriptData.GetInt("goldRaided").GetValueOrDefault(expectedGold);

                      Player.Instance.NewUpdateRaids(goldRaided);
                      SaveSeige(goldRaided);
                  }
                  else
                  {
                      UIManager.Instance.ToggleBackToHomeSceneButton();
                  }
              }
          }
          );
    }

    // ---------- TIME ---------- \\
    public void GetInitialLoginServerTime()
    {
        new LogEventRequest()
        .SetEventKey("getServerTime")
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                GSData data = response.ScriptData;
                DataManager.initialLoginServerTime = data.GetInt("serverTime").GetValueOrDefault(-404);
            }
            else
                print("Failed to get server time " + response.Errors);
        });
    }
    public void GetServerTime()
    {
        new LogEventRequest()
        .SetEventKey("getServerTime")           // Pass the event name to Sparks server here
        .Send((response) =>
       {
           if (!response.HasErrors)
           {
               GSData data = response.ScriptData;   // The JSON file

               DataManager.currentServerTime = data.GetInt("serverTime").GetValueOrDefault(-404);
           }
           else
               print("Failed to get server time " + response.Errors);
       });
    }
    public void SaveLastLogout()
    {
        if (DataManager.initialLoginServerTime == -100)
            return;

        new LogEventRequest()
        .SetEventKey("saveLastLogout")
        .Send((response) =>
       {
           if (response.HasErrors)
               Debug.Log(response.Errors.JSON);
       });
    }
    public void GetLastLogout(string idToUse)
    {
        new LogEventRequest()
        .SetEventKey("getLastLogout")           // Pass the event name to Sparks server here
        .SetEventAttribute("PLAYER_ID", idToUse)
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                GSData data = response.ScriptData;
                DataManager.lastLogout = data.GetGSData(idToUse).GetLong("unixTimestamp").GetValueOrDefault(-404);
                EnemyLoader.lastNotificationTimeRecieved = DataManager.lastLogout;
                SaveLastLogout();
            }
            else
                Debug.Log("Failed to get last logout time " + response.Errors.JSON);
        });
    }

    // --------- BATTLE ----------- \\

    // For loading Enemy IDs
    public void GetEnemyList(int count)
    {
        new LogEventRequest()
        .SetEventKey("searchRandomPlayer")         // Pass the event name to Sparks server here
        .SetEventAttribute("playerLevel", Player.castleLevel)
        .SetEventAttribute("count", 6)
        .SetEventAttribute("clanID", Clan.clanID)
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                GSData data = response.ScriptData;  // The JSON file

                foreach (GSData info in data.GetGSDataList("Target"))
                {
                    string getID = info.GetGSData("data").GetString("playerId");
                    EnemyLoader.Instance.AddEnemyToList(getID);
                }
            }
            else
                print("Failed to get player IDs " + response.Errors);
        });
    }
    public void GetSingleEnemyName(string IDtoSearch, int enemyInt, string type)
    {
        new LogEventRequest()
        .SetEventKey("searchTarget")         // Pass the event name to Sparks server here
        .SetEventAttribute("PLAYER_ID", IDtoSearch)
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                GSData data = response.ScriptData;  // The JSON file

                if (data.GetGSData("PlayerSearchableList") != null)
                {
                    string getID = data.GetGSData("PlayerSearchableList").GetString("id");

                    if (type == "siege")
                    {
                        EnemyLoader.attackerIDList[enemyInt] = getID;
                    }
                }
            }
            else
                print("Failed to get player IDs " + response.Errors);
        });
    }

    // --------- MISC ----------- \\
    public void InsertPlayerIntoSearchableList()
    {
        new LogEventRequest()
        .SetEventKey("insertPlayerIntoSearchableList")              // Pass the event name to Sparks server here
        .Send((response) =>
       {
           if (response.HasErrors)
               Debug.Log(response.Errors.JSON);
       });
    }

    public DateTime TimeDifferenceToTomorrowUTC()
    {
        DateTime now = DateTime.UtcNow;

        int hrDiff = DateTime.UtcNow.Hour;
        int minDiff = DateTime.UtcNow.Minute;
        int secDiff = DateTime.UtcNow.Second;

        //Debug.Log(string.Format("{0} hour {1} mins {2} sec", hrDiff, minDiff, secDiff));

        DateTime tomorrow = now.AddDays(1).AddHours(-hrDiff).AddMinutes(-minDiff).AddSeconds(-secDiff);

        return tomorrow;
    }
    public long UnixTime(DateTime timeToConvert)
    {
        DateTime sTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        return (long)(timeToConvert - sTime).TotalSeconds;
    }
    // ======= MISC END ======== \\


    // =======================================================

    #region Misc (Non-Sparks methods)

    bool IsAccessingOwnData(string IDBeingUsed)
    {
        return IDBeingUsed == playerId;
    }

    #endregion

    // ================================ \\
}


public class RTSessionInfo
{
    string hostURL;
    public string GetHostURL() { return hostURL; }
    string acccessToken;
    public string GetAccessToken() { return acccessToken; }
    int portID;
    public int GetPortID() { return portID; }
    string matchID;
    public string GetMatchID() { return matchID; }

    List<RTPlayer> playerList = new List<RTPlayer>();
    public List<RTPlayer> GetPlayerList()
    {
        return playerList;
    }

    /// <param name="_message">Message.</param>
    public RTSessionInfo(MatchFoundMessage _message)
    {
        portID = (int)_message.Port;
        hostURL = _message.Host;
        acccessToken = _message.AccessToken;
        matchID = _message.MatchId;
        // we loop through each participant and get their peerId and display name //
        foreach (MatchFoundMessage._Participant p in _message.Participants)
        {
            playerList.Add(new RTPlayer(p.DisplayName, p.Id, (int)p.PeerId));
        }
    }

    public class RTPlayer
    {
        public RTPlayer(string _displayName, string _id, int _peerId)
        {
            displayName = _displayName;
            id = _id;
            peerId = _peerId;
        }

        public string displayName;
        public string id;
        public int peerId;
        public bool isOnline;
    }
}
