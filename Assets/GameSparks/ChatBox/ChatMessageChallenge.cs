﻿using UnityEngine;
using UnityEngine.UI;

public class ChatMessageChallenge : ChatMessage
{
    [Space(15)]
    [Header("Challenge Options Info")]
    public Image avatarImage;
    public Button challengePlayer;      // this is the PLAYER AVATER icon in their own chat boxes

    public static bool isChallenging;
    public bool dataReceieved;
    public bool detailReceived;
    public bool currencyReceived;

    public void TriggerListener()
    {
        if (!senderName.Contains("(Admin) "))
        {
            challengePlayer.onClick.AddListener(delegate { SetIsChallenging(true); });
            challengePlayer.onClick.AddListener(chatboxContent.ResetEnemy);
            challengePlayer.onClick.AddListener(chatboxContent.BlockWhenLoadingInfoPanel);
            challengePlayer.onClick.AddListener(delegate { chatboxContent.ViewChallengeInfo(senderID, childID, chatboxType); });
        }
        else
            challengePlayer.interactable = false;
    }
    

    void SetIsChallenging(bool toSet)
    {
        isChallenging = toSet;
        dataReceieved = false;
        detailReceived = false;
        currencyReceived = false;
    }

    public void DataObtained()
    {
        dataReceieved = true;
        SendInfo();
    }
    public void DetailObtained()
    {
        detailReceived = true;
        SendInfo();
    }
    public void CurrencyObtained()
    {
        currencyReceived = true;
        SendInfo();
    }
    
    void SendInfo()
    {
        if (currencyReceived == false || detailReceived == false || dataReceieved == false)
            return;

        chatboxContent.targetIDC = senderID;
        chatboxContent.nameC = senderName;
        chatboxContent.clanC = senderClan;
        chatboxContent.raceC = senderRace;
        chatboxContent.challengeAvatarDisplay.sprite = avatarImage.sprite;

        chatboxContent.CheckIfChallengable();
        chatboxContent.FillInEnemyInfo();

        isChallenging = false;
    }


    public void DestoryMessage()
    {
        Destroy(gameObject, 0.1f);
    }
}
