﻿using System;
using System.Collections.Generic;
using GameSparks.RT;
using GameSparks.Api.Requests;
using UnityEngine;
using UnityEngine.UI;
using GameSparks.Core;

public class ChatBoxContent : MonoBehaviour
{
    [Range(2, 100)]
    public int maxMessageCount = 80;

    public Dropdown recipientOptions;

    public GameObject chatPanel, textObject;
    public InputField chatBox;

    public Animator animator;

    public string chatboxType;

    [SerializeField]
    public List<GameObject> chatObjects = new List<GameObject>();
    public List<ChatMessage> chatMessages = new List<ChatMessage>();
    public List<ChatMessageChallenge> chatMessageChallenges = new List<ChatMessageChallenge>();

    public List<string> senderName = new List<string>();
    public List<string> senderID = new List<string>();
    public List<string> senderAvatar = new List<string>();
    public List<string> senderGuild = new List<string>();
    public List<string> senderRace = new List<string>();

    bool colorInt;

    [Space(15)]
    [Header(" *** Global Chat Color Bars ***")]
    public Color globalChatMessageColor1;
    public Color globalChatMessageColor2;
    Vector4 brown_1 = new Vector4(0.77f, 0.69f, 0.42f, 1);
    Vector4 brown_2 = new Vector4(0.57f, 0.35f, 0.22f, 1);

    [Space(15)]
    [Header(" *** Clan Chat Color Bars ***")]
    public Color clanChatMessageColor1;
    public Color clanChatMessageColor2;

    // ==== CHALLENGE POPUP ==== \\
    [Space(12)]
    [Header("View Enemy Detail")]
    // since there will always only be one, we only need to store it once
    public GameObject blockWhenLoadingInfoPanel;
    public GameObject popupPanel;
    public Image challengeAvatarDisplay;
    public Text nameC_, repC_, levelC_, goldC_, clanC_;
    public string targetIDC, nameC, clanC, raceC;
    int goldC, repC, levelC;
    float shuffleTimeC;
    public GameObject challengeButton;      // this is to Set the button inactive if selected player is self
    // ==== CHALLENGE POPUP END ==== \\

    // ==== MESSAGE POPUP ==== \\
    [Space(12)]
    [Header("Response Messages")]
    public GameObject msgPopup;
    public Text responseMsg;

    public static int smallChatCounter, globalChatCounter, clanChatCounter, systemChatCounter;



    void Start()
    {
        smallChatCounter = 0;
        globalChatCounter = 0;
        clanChatCounter = 0;
    }
    void OnEnable()
    {
        ChangeMyAvatarInChatDisplay(Player.potraitID);
    }



    public void SendGlobalessageButton()
    {
        if (chatBox.text != "")
        {
            SendGlobalMessage(chatBox.text);
            //chatBox.ActivateInputField();
            chatBox.text = "";
        }
    }
    public void SendClanMessageButton()
    {
        if (chatBox.text != "")
        {
            SendClanMessage(chatBox.text);
            //chatBox.ActivateInputField();
            chatBox.text = "";
        }
    }


    void SendGlobalMessage(string MessageToSend)
    {
        using (RTData data = RTData.Get())
        {
            data.SetString(1, MessageToSend); // we add the message data to the RTPacket at key '1', so we know how to key it when the packet is receieved
            data.SetString(2, DateTime.Now.ToString()); // we are also going to send the time at which the user sent this message

            //This area contains info for sending messages to the WORLD CHAT
            new SendTeamChatMessageRequest()
            .SetMessage(MessageToSend.Trim())
            .SetOwnerId("5b3ca9a46f217904e31ba8b7")
            .SetTeamId("globalchat")
            .SetTeamType("GLOBAL")
            .Send((response) =>
            {
              if (!response.HasErrors)
              {
                  GSData scriptData = response.ScriptData;
              }
              else
                  Debug.Log(response.Errors.JSON);
          });
        }
    }
    void SendClanMessage(string MessageToSend)
    {
        new SendTeamChatMessageRequest()
        .SetMessage(MessageToSend.Trim())
        .SetOwnerId(Clan.leaderID)
        .SetTeamId(Clan.clanID)
        .SetTeamType("CLAN")
        .Send((response) =>
        {
            if (!response.HasErrors)
            {
                GSData scriptData = response.ScriptData;
            }
            else
                Debug.Log(response.Errors.JSON);
        });
    }


    public void ToggleInputField(bool toSet)
    {
        chatBox.gameObject.SetActive(!toSet);
    }

    // ========== MESSAGE INSERTION =========== \\

    // The PROCESS FOR PUTTING ANY MESSAGES RECIEVED/SENT INTO THE CHATBOX
    public void DisplayMessageInChatbox(string MessageToSend ,DateTime sentTime)
    {
        if (name.ToLower().Contains("small"))
        {
            GameObject newMessage = Instantiate(textObject, chatPanel.transform);

            ChatMessage chatMsg = newMessage.GetComponent<ChatMessage>();
            chatMsg.Text = MessageToSend;
            chatMsg.textObject.text = MessageToSend;

            chatMsg.chatboxContent = this;
            chatMsg.chatboxType = chatboxType;

            //chatMsg.textObject.color = SwitchFontColor();
            //chatMsg.chatPanelImage.color = GlobalChatSwitchColor();
            chatMsg.childID = smallChatCounter;         //start the value from 0
            chatMsg.SetDisplayTime(sentTime);

            SmallChatboxTimeRearrange(chatMsg, newMessage);

            smallChatCounter += 1;
        }
        else if (name.Contains("GLOBAL"))
        {
            if (chatMessages.Count >= maxMessageCount)
            {
                Destroy(chatObjects[0]);
                chatObjects.RemoveAt(0);
                chatMessageChallenges.RemoveAt(0);
            }

            GameObject newMessage = Instantiate(textObject, chatPanel.transform);

            ChatMessageChallenge chatMsg = newMessage.GetComponent<ChatMessageChallenge>();
            chatMsg.Text = MessageToSend;
            chatMsg.textObject.text = MessageToSend;

            chatMsg.chatboxContent = this;
            chatMsg.chatboxType = chatboxType;

            chatMsg.textObject.color = SwitchFontColor();
            chatMsg.chatPanelImage.color = GlobalChatSwitchColor();

            chatMsg.senderName = senderName[senderName.Count - 1];
            chatMsg.senderID = senderID[senderID.Count-1];
            chatMsg.childID = globalChatCounter;     // also start this counter from 0
            chatMsg.SetDisplayTime(sentTime);

            chatMsg.TriggerListener();

            chatMessageChallenges.Add(chatMsg);
            chatObjects.Add(newMessage);

            globalChatCounter += 1;
        }
        else if (name.Contains("CLAN"))
        {
            if (chatMessages.Count >= maxMessageCount)
            {
                Destroy(chatObjects[0]);
                chatObjects.RemoveAt(0);
                chatMessageChallenges.RemoveAt(0);
            }

            GameObject newMessage = Instantiate(textObject, chatPanel.transform);

            ChatMessageChallenge chatMsg = newMessage.GetComponent<ChatMessageChallenge>();
            chatMsg.Text = MessageToSend;
            chatMsg.textObject.text = MessageToSend;

            chatMsg.chatboxContent = this;
            chatMsg.chatboxType = chatboxType;

            chatMsg.textObject.color = SwitchFontColor();
            chatMsg.chatPanelImage.color = ClanChatSwitchColor();

            chatMsg.senderName = senderName[senderName.Count - 1];
            chatMsg.senderID = senderID[senderID.Count - 1];
            chatMsg.childID = clanChatCounter;     // also start this counter from 0
            chatMsg.SetDisplayTime(sentTime);

            chatMsg.TriggerListener();

            chatMessageChallenges.Add(chatMsg);
            chatObjects.Add(newMessage);

            clanChatCounter += 1;
        }
        else if (name.Contains("SYSTEM"))
        {
            // message removal
            if (chatMessages.Count >= maxMessageCount)
            {
                Destroy(chatObjects[0]);
                chatObjects.RemoveAt(0);
                chatMessageChallenges.RemoveAt(0);
            }

            GameObject newMessage = Instantiate(textObject, chatPanel.transform);

            ChatMessage chatMsg = newMessage.GetComponent<ChatMessage>();
            chatMsg.Text = MessageToSend;
            chatMsg.textObject.text = chatMsg.textObject.text.Replace("_text_", MessageToSend);

            chatMsg.chatboxContent = this;
            chatMsg.chatboxType = chatboxType;

            chatMsg.childID = clanChatCounter;     // also start this counter from 0

            chatObjects.Add(newMessage);

            clanChatCounter += 1;
        }
    }
    public void DisplayMessageInChatbox(string MessageToSend)
    {
        if (name.Contains("SYSTEM"))
        {
            // message removal
            if (chatMessages.Count >= maxMessageCount)
            {
                Destroy(chatObjects[0]);
                chatObjects.RemoveAt(0);
                chatMessageChallenges.RemoveAt(0);
            }

            GameObject newMessage = Instantiate(textObject, chatPanel.transform);

            ChatMessage chatMsg = newMessage.GetComponent<ChatMessage>();
            chatMsg.Text = MessageToSend;
            chatMsg.textObject.text = chatMsg.textObject.text.Replace("_text_", MessageToSend);

            chatMsg.chatboxContent = this;
            chatMsg.chatboxType = chatboxType;

            chatMsg.childID = clanChatCounter;     // also start this counter from 0

            chatObjects.Add(newMessage);

            clanChatCounter += 1;
        }
    }
    public void AssignSenderID(string senderID_, string senderName_)
    {
        if (senderID.Count >= maxMessageCount && senderName.Count >= maxMessageCount)
        {
            chatMessageChallenges.RemoveAt(0);
            senderName.RemoveAt(0);
            senderID.RemoveAt(0);
            senderAvatar.RemoveAt(0);
            senderGuild.RemoveAt(0);
            senderRace.RemoveAt(0);
        }
        senderName.Add(senderName_);
        senderID.Add(senderID_);
        senderAvatar.Add("-");
        senderGuild.Add("-");
        senderRace.Add("-");
    }
    public void AssignDetails(int playerInt, string clan_, string race_, string portrait)
    {
        if (senderAvatar[playerInt] == "-" || senderGuild[playerInt] == "-" || senderRace[playerInt] == "-")
            return;

        chatMessageChallenges[playerInt].avatarImage.sprite = PotraitManager.Instance.PortraitSprite(portrait);
        chatMessageChallenges[playerInt].senderClan = clan_;
        chatMessageChallenges[playerInt].senderRace = race_;
    }

    void SmallChatboxTimeRearrange(ChatMessage chatMsg, GameObject newMsg)
    {
        // if there is more than the message count max, we need to find out if this new item deserves a slot
        if (chatMessages.Count >= maxMessageCount)
        {
            for (int x = chatMessages.Count - 1; x >= 0 ; x--)
            {
                if (chatMsg.timeStampSent >= chatMessages[x].timeStampSent)
                {
                    chatMsg.transform.SetSiblingIndex(chatMessages[x].transform.GetSiblingIndex()+1);

                    chatMessages.Insert(x + 1, chatMsg);
                    chatObjects.Insert(x + 1, newMsg);

                    Destroy(chatObjects[0]);
                    chatMessages.RemoveAt(0);
                    chatObjects.RemoveAt(0);
                    break;
                }

                if (x == 0)
                    Destroy(newMsg);
            }
        }
        else if(chatMessages.Count < maxMessageCount)
        {
            chatMessages.Add(chatMsg);
            chatObjects.Add(newMsg);
        }
    }

    Color GlobalChatSwitchColor()
    {
        colorInt = !colorInt;
        if (colorInt)
            return globalChatMessageColor1;
        return globalChatMessageColor2;
    }
    Color SwitchFontColor()
    {
        if (colorInt)
            return Color.white;
        return Color.black;
    }
    Color ClanChatSwitchColor()
    {
        colorInt = !colorInt;
        if (colorInt)
            return clanChatMessageColor1;
        return clanChatMessageColor2;
    }

    // ========== MESSAGE INSERTION END =========== \\



    // ========== RESPONSE HANDLING POPUP ============ \\
    public void ViewChallengeInfo(string playerIDtoSearch, int childID, string chatboxType)
    {
        //Get the info and let the response handle the popup
        GameSparksManager.Instance.GetEnemyExpendables(playerIDtoSearch, childID, chatboxType);
        GameSparksManager.Instance.GetEnemyData(playerIDtoSearch, childID, chatboxType);
        GameSparksManager.Instance.GetEnemyDetails(playerIDtoSearch, childID, chatboxType);
    }
    public void BlockWhenLoadingInfoPanel()
    {
        blockWhenLoadingInfoPanel.SetActive(true);
    }
    public void ResetEnemy()
    {
        // Triggered everytime a Player hits a target from the chatbox
        // in order to perform an "All Info Recieved" when pulling
        goldC = -100;
        levelC = -100;
        repC = -100;
        shuffleTimeC = -100;
    }
    void InvokeEnemyPullTimeOut()
    {
        Invoke("TimeOutCheck", 10f);
    } 
    public void RetrieveEnemyGold(int gold)
    {
        goldC = gold;
        FillInEnemyInfo();
    }
    public void RetrieveEnemyData(int level, int rep, float shuffleTime)
    {
        levelC = level;
        repC = rep;
        shuffleTimeC = shuffleTime;

        FillInEnemyInfo();
    }
    public void FillInEnemyInfo()
    {
        if (goldC == -100 || levelC == -100 || repC == -100 || shuffleTimeC == -100)
            return;

        goldC_.text = string.Format("Gold : {0:n0} <color=#00FF00>({1:n0})</color>", goldC, goldC/2);
        repC_.text = string.Format("Rep : {0:n0}", repC);
        levelC_.text = string.Format("Level : {0:n0}", levelC);
        nameC_.text = "Name : " + nameC;

        if (clanC == "" || clanC == "<currently_not_in_any_clan_yet>")
            clanC_.text = "Clan : Not yet joined any";
        else
            clanC_.text = "Clan : " + clanC;

        PopupDisplay();
    }
    void PopupDisplay()
    {
        popupPanel.SetActive(true);
    }
    void TimeOutCheck()
    {
        PopupMessage("Failed to retrieve required info.");
    }

    // === RESPONSE MESSAGES POPUP === \\
    public void PopupMessage(string message)
    {
        SetMessage(message);
        DisplayPopup(true);
    }
    void SetMessage(string message)
    {
        responseMsg.text = message;
    }
    void DisplayPopup(bool isActive)
    {
        msgPopup.SetActive(isActive);
    }
    // ========== RESPONSE HANDLING POPUP END ============ \\



    // ========== CHATBOX TARGET CHALLENGE ============= \\
    public void ChallengeTarget()
    {
        StoreEnemyInfo();
        ObtainTargetAssignedUnits();
    }
    public void CheckIfChallengable()
    {
        // If selected is PLAYER
        if (targetIDC == GameSparksManager.playerId)
            challengeButton.SetActive(false);
        else
            challengeButton.SetActive(true);

        // if Player is in a CLAN and the selected is in the same clan
        if (Clan.clanID != "<currently_not_in_any_clan_yet>")
        {
            if (clanC == Clan.clanID)
                challengeButton.SetActive(false);
            else
                challengeButton.SetActive(true);
        }
    }
    void StoreEnemyInfo()
    {
        EnemyLoader.selectedEnemyID = targetIDC;
        Enemy.nameLoaded = nameC;
        Enemy.castleLevel = levelC;
        Enemy.reputation = repC;
        Enemy.clan = clanC;
        Enemy.race = raceC;
        Enemy.gold = goldC;
        Enemy.shuffleTiming = shuffleTimeC;
    }
    void ObtainTargetAssignedUnits()
    {
        EnemyLoader.Instance.GetChatboxTargetAssignedUnits(targetIDC);
    }



    //To be put into start on new scene to load whoever is in the battle
    public void Display_Recipients()
    {
        foreach (RTSessionInfo.RTPlayer _player in GameSparksManager.Instance.GetSessionInfo().GetPlayerList())
        if (GameSparksManager.Instance.GetSessionInfo() != null)
        {
        recipientOptions.options.Add(new Dropdown.OptionData { text = _player.displayName });
        } 	
    }



    // Remove of data from the Lists
    public void ClearAllLists()
    {
        chatObjects.Clear();
        chatMessages.Clear();
        senderName.Clear();
        senderID.Clear();
        senderAvatar.Clear();
        senderGuild.Clear();
        senderRace.Clear();
    }
    public void ClearChatObjs()
    {
        for (int x = 0; x < chatMessageChallenges.Count; x++)
        {
            chatMessageChallenges[x].DestoryMessage();
        }
        chatMessageChallenges.Clear();
    }



    public void ToggleChat(bool isToggled)
    {
        animator.SetBool("Open", isToggled);
    }

    public void ChangeMyAvatarInChatDisplay(string portraitID)
    { 
        for(int x = 0; x < chatMessageChallenges.Count; x++)
        {
            if (chatMessageChallenges[x].senderID == GameSparksManager.playerId)
            {
                chatMessageChallenges[x].avatarImage.sprite = PotraitManager.Instance.PortraitSprite(portraitID);
            }
        }
    }


    // View the type of data that can be sent
    // Index the info that is to be sent before the value
    public void Send_RT_Data()
    {
        using (RTData data = RTData.Get())
        {
            data.SetInt(1, 10); // add int at index 1
            data.SetFloat(2, 20f); // add float at index 2                        
            data.SetDouble(3, 30d); // add double at index 3                  
            data.SetLong(4, 4294967296L); // add long at index 4                         
            data.SetString(5, "Hello World!"); // add string at index 5    
            data.SetVector2(6, new Vector2(1f, 2f)); // add vector2 at index 6
            data.SetVector3(7, new Vector3(1f, 2f, 3f)); // add vector3 at index 7
            data.SetVector4(8, new Vector4(1f, 2f, 3f, 4f)); // add vector4 at index 8
            data.SetRTVector(9, new RTVector(1f, 2f, 3f, 4f)); // add RTVector at index 9
            data.SetData(10, RTData.Get().SetInt(1, 1)); // add nested data at index 10
        }  
    }
}