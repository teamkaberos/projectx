﻿using UnityEngine;
using UnityEngine.UI;
//How to resize view when touchscreen keyboard is visible on Android with Unity?

public class Input_Resize : MonoBehaviour
{

    // Assign panel here in order to adjust its height when TouchScreenKeyboard is shown
    public GameObject panel;

     InputField inputField;
     RectTransform panelRectTrans;
     Vector2 panelOffsetMinOriginal;
    float initialKeyboardHeightRatio = 1;
     float panelHeightOriginal;
     float currentKeyboardHeightRatio;

    public static bool chatFocus;


    public void Start()
    {
        inputField = transform.GetComponent<InputField>();

        panelRectTrans = panel.GetComponent<RectTransform>();

        panelOffsetMinOriginal = panelRectTrans.offsetMin;
        panelHeightOriginal = panelRectTrans.rect.height;
    }

    public void LateUpdate ()
    {
        if (inputField.isFocused)
        {
            chatFocus = true;

            float newKeyboardHeightRatio = GetKeyboardHeightRatio();
            if (currentKeyboardHeightRatio != newKeyboardHeightRatio)
            {
                currentKeyboardHeightRatio = newKeyboardHeightRatio;
                panelRectTrans.offsetMin = new Vector2(panelOffsetMinOriginal.x, panelHeightOriginal * currentKeyboardHeightRatio);
            }
        }
        else if (!inputField.isFocused)
        {
            //if (inputField.text == "")
            //{
                panelRectTrans.offsetMin = new Vector2(panelOffsetMinOriginal.x, panelHeightOriginal * panelOffsetMinOriginal.y * currentKeyboardHeightRatio);
                currentKeyboardHeightRatio = initialKeyboardHeightRatio;
            //}

            chatFocus = false;
        }
    }

    private float GetKeyboardHeightRatio() 
    {
        if (Application.isEditor)
            return 0.4f; // fake TouchScreenKeyboard height ratio for debug in editor 

        #if UNITY_ANDROID        
        using (AndroidJavaClass UnityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer")) {
            AndroidJavaObject View = UnityClass.GetStatic<AndroidJavaObject>("currentActivity").Get<AndroidJavaObject>("mUnityPlayer").Call<AndroidJavaObject>("getView");
            using (AndroidJavaObject rect = new AndroidJavaObject("android.graphics.Rect")) {
                View.Call("getWindowVisibleDisplayFrame", rect);
                return (float)(Screen.height - rect.Call<int>("height")) / Screen.height;
            }
        }
        #else
        return (float)TouchScreenKeyboard.area.height / Screen.height;
        #endif
    }

}