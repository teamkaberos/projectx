﻿using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ChatMessage : MonoBehaviour 
{
    public enum ChatType
    {
        global,
        clan 
    }

    // ==== Actual Info ==== \\
    [Header("Info Area")]
    public string Text;         
    public string senderID;
    public string senderName;
    public string senderClan;
    public string senderRace;
    public long timeStampSent;
    DateTime timeMsgSent;

    public int childID;     //number increases as long as the player stays in game

    // ==== UI Stuff ==== \\
    [Space(15)]
    [Header("Info Area")]
    public Text textObject;
    public Text timeSentText;
    public Image chatPanelImage;

    [Space(15)]
    [Header("Chatbox reference")]
    public ChatBoxContent chatboxContent;
    public string chatboxType;


    void OnEnable()
    {
        //Time.timeSinceLevelLoad
        TimeDifference();
    }

    public void SetDisplayTime(DateTime timeSent)
    {
        timeStampSent = timeSent.Ticks;
        timeMsgSent = timeSent;

        TimeDifference();
    }

    public void TimeDifference()
    {
        DateTime nowTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddMilliseconds(DataManager.initialLoginServerTime * 1000).AddSeconds(Time.timeSinceLevelLoad);
        TimeSpan diff = nowTime.Subtract(timeMsgSent);
        string timeSentText_ = "";

        if (Mathf.FloorToInt(diff.Days) >= 1)
        {
            timeSentText_ = Mathf.FloorToInt(diff.Days) + " day";
            if (Mathf.FloorToInt(diff.Days) > 1)
                timeSentText_ += "s";
        }
        else if (Mathf.FloorToInt(diff.Hours) >= 1)
        {
            timeSentText_ = Mathf.FloorToInt(diff.Hours) + " hr";
            if (Mathf.FloorToInt(diff.Hours) > 1)
                timeSentText_ += "s";
        }
        else if (Mathf.FloorToInt(diff.Minutes) >= 1)
        {
            timeSentText_ = Mathf.FloorToInt(diff.Minutes) + " min";
            if (Mathf.FloorToInt(diff.Minutes) > 1)
                timeSentText_ += "s";
        }
        else if (Mathf.FloorToInt(diff.Minutes) < 1)
        {
            timeSentText_ = "Less than a min";
        }

        timeSentText.text = string.Format("{0} ago", timeSentText_);
    }
}