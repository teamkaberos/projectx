﻿using UnityEngine;

public class Reorder_Canvas : MonoBehaviour
{
    Canvas chatCanvas;

    void Start()
    {
        chatCanvas = GetComponent<Canvas>();
    }


    public void Reorder_Layer(bool isOn)
    {
        if (isOn)
            chatCanvas.sortingOrder = 1;
        else
            chatCanvas.sortingOrder = -3;
    }
}
